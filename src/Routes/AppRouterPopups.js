import React, { useContext } from "react";
import { PopupContext } from "./AppRouter";

const PasswordChanged = React.lazy(() => import("Screens/PasswordChanged"));
const NewPassword = React.lazy(() => import("Screens/NewPassword"));
const SelectWorkTime = React.lazy(() => import("Screens/SelectWorkTime"));
const SetHourlyRate = React.lazy(() => import("Screens/SetHourlyRate"));
const AlertDialog = React.lazy(() => import("Components/AlertDialog"));
const RejectRequest = React.lazy(() => import("Screens/RejectRequest"));
const PaymentConfirmation = React.lazy(() =>
  import("Screens/PaymentConfirmation")
);
const ActionConfirmation = React.lazy(() =>
  import("Screens/ActionConfirmation")
);
const ContactSuccess = React.lazy(() => import("Screens/ContactSuccess"));
const ShareDiscusssion = React.lazy(() => import("Screens/ShareDiscusssion"));
const Login = React.lazy(() => import("Screens/Login"));
const AskQuestion = React.lazy(() => import("Screens/AskQuestion"));
const EmailVerification = React.lazy(() => import("Screens/EmailVerification"));
const ForgotPassword = React.lazy(() => import("Screens/ForgotPassword"));
const ChangePassword = React.lazy(() => import("Screens/ChangePassword"));
const JobPreview = React.lazy(() => import("Screens/JobPreview"));
const AddDegree = React.lazy(() => import("Screens/AddDegree"));
const AddCertifications = React.lazy(() => import("Screens/AddCertifications"));
const AddSkills = React.lazy(() => import("Screens/AddSkills"));
const AddWorkExperiance = React.lazy(() => import("Screens/AddWorkExperiance"));
const EditDegree = React.lazy(() => import("Screens/EditDegree"));
const EditCertifications = React.lazy(() =>
  import("Screens/EditCertifications")
);
const EditSkills = React.lazy(() => import("Screens/EditSkills"));
const EditWorkExperiance = React.lazy(() =>
  import("Screens/EditWorkExperiance")
);
const UploadProject = React.lazy(() => import("Screens/UploadProject"));
const EditProject = React.lazy(() => import("Screens/EditProject"));
const Message = React.lazy(() => import("Screens/Message"));
const RequestVideoCall = React.lazy(() => import("Screens/RequestVideoCall"));
const AddSocials = React.lazy(() => import("Screens/AddSocials"));
const EditSocials = React.lazy(() => import("Screens/EditSocials"));
const CustomizedRequirments = React.lazy(() =>
  import("Screens/CustomizedRequirments")
);
const Invoice = React.lazy(() => import("Screens/Invoice"));
const ApplyForJob = React.lazy(() => import("Screens/ApplyForJob"));

export default function AppRouterPopups({
  redirectURLFromLogin,
  setHeadingAndTextForAlertDialog,
  getAskQuestionData,
  dataAfterPaymentConfirmation,
  isContactSuccess,
  setIsContactSuccess,
  isShareDiscusssion,
  setIsShareDiscusssion,
  isAddConfirmation,
  setIsAddConfirmation,
  isDeleteConfirmation,
  setIsDeleteConfirmationResponse,
  text,
  editDegree,
  editCertificationData,
  selectedSkills,
  selectedHourlyRate,
  addWorkExperianceEditData,
  selectedProjectData,
  userData,
  videoCallUserData,
  videoCallUpdate,
  setVideoCallUpdateSuccess,
  isRejectRequest,
  setIsRejectRequest,
  isApplyForJob,
  setIsApplyForJob,
  setIsApplyForJobProject,
  isJobPreviewData,
  setDataAfterPaymentConfirmation,
}) {
  const {
    isLoginOpen,
    setIsLoginOpen,
    isForgotPasswordOpen,
    setIsForgotPasswordOpen,
    isFromForgotPassword,
    setIsFromForgotPassword,
    isAskQuestionOpen,
    setIsAskQuestionOpen,
    isNewPasswordOpen,
    setIsNewPasswordOpen,
    isEmailVerificationOpen,
    setIsEmailVerificationOpen,
    isPasswordChangedOpen,
    setIsPasswordChangedOpen,
    alertDialogVisibility,
    setAlertDialogVisibility,
    isPaymentConfirmation,
    setIsPaymentConfirmation,
    isMessageOpen,
    setIsMessageOpen,
    isEditDegreeOpen,
    setIsEditDegreeOpen,
    setIsDeleteConfirmation,
    isInvoiceOpen,
    setIsInvoiceOpen,
    isCustomizedRequirmentsOpen,
    setIsCustomizedRequirmentsOpen,
    isEditSocialsOpen,
    setIsEditSocialsOpen,
    isAddSocialsOpen,
    setIsRequestVideoCallOpen,
    isRequestVideoCallOpen,
    setIsAddSocialsOpen,
    isEditWorkExperianceOpen,
    setIsEditWorkExperianceOpen,
    isUploadProjectOpen,
    setIsUploadProjectOpen,
    isEditProjectOpen,
    setIsEditProjectOpen,
    isEditSkillsOpen,
    setIsEditSkillsOpen,
    isEditCertificationsOpen,
    setIsEditCertificationsOpen,
    isAddWorkExperianceOpen,
    setIsAddWorkExperianceOpen,
    isChangePasswordOpen,
    setIsChangePasswordOpen,
    isAddSkillsOpen,
    setIsAddSkillsOpen,
    isSetHourlyRateOpen,
    setIsSetHourlyRateOpen,
    isSelectWorkTimeOpen,
    setIsSelectWorkTimeOpen,
    isAddCertificationsOpen,
    setIsAddCertificationsOpen,
    isJobPreviewOpen,
    setIsJobPreviewOpen,
    isAddDegreeOpen,
    setIsAddDegreeOpen,
    isEditConfirmation,
    setIsEditConfirmation,
  } = useContext(PopupContext);
  return (
    <>
      {isLoginOpen ? (
        <Login
          setIsLoginOpen={setIsLoginOpen}
          redirectURLFromLogin={redirectURLFromLogin}
          setIsForgotPasswordOpen={setIsForgotPasswordOpen}
          setIsEmailVerificationOpen={setIsEmailVerificationOpen}
          setAlertDialogVisibility={setAlertDialogVisibility}
          setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
        />
      ) : null}
      {isAskQuestionOpen ? (
        <AskQuestion
          setIsAskQuestionOpen={setIsAskQuestionOpen}
          getAskQuestionData={getAskQuestionData}
        />
      ) : null}
      {isPaymentConfirmation ? (
        <PaymentConfirmation
          setIsPaymentConfirmation={setIsPaymentConfirmation}
          setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
        />
      ) : null}
      {isContactSuccess ? (
        <ContactSuccess onClose={setIsContactSuccess} />
      ) : null}
      {isShareDiscusssion ? (
        <ShareDiscusssion onClose={setIsShareDiscusssion} />
      ) : null}
      {isAddConfirmation ? (
        <ActionConfirmation
          onClose={setIsAddConfirmation}
          heading="Do you want to add"
          onConfirm={() => { }}
          onDecline={() => { }}
        />
      ) : null}
      {isEditConfirmation ? (
        <ActionConfirmation
          onClose={setIsEditConfirmation}
          heading="Do you want to edit"
          text=""
          onConfirm={() => { }}
          onDecline={() => { }}
        />
      ) : null}
      {isDeleteConfirmation ? (
        <ActionConfirmation
          onClose={setIsDeleteConfirmation}
          heading="Do you want to delete?"
          text=""
          onConfirm={() => {
            setIsDeleteConfirmationResponse(Math.random());
            setIsDeleteConfirmation(false);
          }}
          onDecline={() => {
            setIsDeleteConfirmationResponse(false);
            setIsDeleteConfirmation(false);
          }}
        />
      ) : null}
      {alertDialogVisibility ? (
        <AlertDialog
          onClose={setAlertDialogVisibility}
          heading={"AIDApro"}
          text={text}
          onConfirm={() => { }}
          onDecline={() => { }}
        />
      ) : null}
      {isForgotPasswordOpen ? (
        <ForgotPassword
          setIsForgotPasswordOpen={setIsForgotPasswordOpen}
          setIsEmailVerificationOpen={setIsEmailVerificationOpen}
          setIsLoginOpen={setIsLoginOpen}
          setIsFromForgotPassword={setIsFromForgotPassword}
        />
      ) : null}
      {isEmailVerificationOpen ? (
        <EmailVerification
          setIsEmailVerificationOpen={setIsEmailVerificationOpen}
          setIsChangePasswordOpen={setIsChangePasswordOpen}
          setAlertDialogVisibility={setAlertDialogVisibility}
          setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
          setIsForgotPasswordOpen={setIsForgotPasswordOpen}
          setIsNewPasswordOpen={setIsNewPasswordOpen}
          setIsFromForgotPassword={setIsFromForgotPassword}
          isFromForgotPassword={isFromForgotPassword}
        />
      ) : null}
      {isNewPasswordOpen ? (
        <NewPassword
          setIsNewPasswordOpen={setIsNewPasswordOpen}
          setIsChangePasswordOpen={setIsChangePasswordOpen}
          setAlertDialogVisibility={setAlertDialogVisibility}
          setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
          setIsForgotPasswordOpen={setIsForgotPasswordOpen}
          setIsPasswordChangedOpen={setIsPasswordChangedOpen}
        />
      ) : null}
      {isPasswordChangedOpen ? (
        <PasswordChanged
          setIsPasswordChangedOpen={setIsPasswordChangedOpen}
          setIsChangePasswordOpen={setIsChangePasswordOpen}
          setAlertDialogVisibility={setAlertDialogVisibility}
          setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
          setIsForgotPasswordOpen={setIsForgotPasswordOpen}
        />
      ) : null}
      {isChangePasswordOpen ? (
        <ChangePassword setIsChangePasswordOpen={setIsChangePasswordOpen} />
      ) : null}
      {isJobPreviewOpen ? (
        <JobPreview
          setIsJobPreviewOpen={setIsJobPreviewOpen}
          data={isJobPreviewData}
        />
      ) : null}
      {isAddDegreeOpen ? (
        <AddDegree
          setIsAddDegreeOpen={setIsAddDegreeOpen}
          editDegree={editDegree}
        />
      ) : null}
      {isAddCertificationsOpen ? (
        <AddCertifications
          setIsAddCertificationsOpen={setIsAddCertificationsOpen}
          editCertificationData={editCertificationData}
        />
      ) : null}
      {isAddSkillsOpen ? (
        <AddSkills
          setIsAddSkillsOpen={setIsAddSkillsOpen}
          selectedSkill={selectedSkills}
        />
      ) : null}
      {isSetHourlyRateOpen ? (
        <SetHourlyRate
          setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
          selectedHourlyRate={selectedHourlyRate}
        />
      ) : null}
      {isSelectWorkTimeOpen ? (
        <SelectWorkTime setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen} />
      ) : null}
      {isAddWorkExperianceOpen ? (
        <AddWorkExperiance
          setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
          addWorkExperianceEditData={addWorkExperianceEditData}
        />
      ) : null}
      {isEditDegreeOpen ? (
        <EditDegree setIsEditDegreeOpen={setIsEditDegreeOpen} />
      ) : null}
      {isEditCertificationsOpen ? (
        <EditCertifications
          setIsEditCertificationsOpen={setIsEditCertificationsOpen}
          editCertificationData={editCertificationData}
        />
      ) : null}
      {isEditSkillsOpen ? (
        <EditSkills setIsEditSkillsOpen={setIsEditSkillsOpen} />
      ) : null}
      {isEditWorkExperianceOpen ? (
        <EditWorkExperiance
          setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
        />
      ) : null}
      {isUploadProjectOpen ? (
        <UploadProject setIsUploadProjectOpen={setIsUploadProjectOpen} />
      ) : null}
      {isEditProjectOpen ? (
        <EditProject
          setIsEditProjectOpen={setIsEditProjectOpen}
          selectedProjectData={selectedProjectData}
        />
      ) : null}
      {isMessageOpen ? (
        <Message userData={userData} setIsMessageOpen={setIsMessageOpen} />
      ) : null}
      {isRequestVideoCallOpen ? (
        <RequestVideoCall
          setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
          videoCallUserData={videoCallUserData}
          videoCallUpdate={videoCallUpdate}
          setVideoCallUpdateSuccess={setVideoCallUpdateSuccess}
        />
      ) : null}
      {isRejectRequest ? (
        <RejectRequest setIsRejectRequest={setIsRejectRequest} />
      ) : null}
      {isAddSocialsOpen ? (
        <AddSocials setIsAddSocialsOpen={setIsAddSocialsOpen} />
      ) : null}
      {isEditSocialsOpen ? (
        <EditSocials setIsEditSocialsOpen={setIsEditSocialsOpen} />
      ) : null}
      {isCustomizedRequirmentsOpen ? (
        <CustomizedRequirments
          setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
        />
      ) : null}
      {isInvoiceOpen ? <Invoice setIsInvoiceOpen={setIsInvoiceOpen} /> : null}
      {isApplyForJob ? (
        <ApplyForJob
          setIsApplyForJob={setIsApplyForJob}
          setIsApplyForJobProject={setIsApplyForJobProject}
        />
      ) : null}
    </>
  );
}
