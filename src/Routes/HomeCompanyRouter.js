import React, { useContext } from "react";
import { Router } from "@reach/router";
import { PageNotFound } from "Screens/404";
import Invoices from "Screens/Invoices";
import { PopupContext } from "./AppRouter";
import {
  Billing,
  DashboardCompany,
  Draft,
  Messenger,
  PersonalDetails,
  PersonalDetailsPreview,
  PersonelDetailPreviewMessengerUser,
  Plan,
  PlannedVideoCalls,
  Posting,
  PostingDetails,
  PostJob,
  PostProject,
} from "Screens";

export const HomeCompanyAdminRouter = ({
  setDontShowSidebar,
  setIsJobPreviewData,
  setVideoCallUserData,
  setIsRejectRequest,
  filteredDataCall,
  title,
  searchBy,
  searchByFreelancer,
  searchByUser,
  keywords,
  setUserData,
  latitude,
  longitude,
  mapLocation,
  selectedRangeValue,
  setVideoCallUpdate,
  videoCallUpdate,
  videoCallUpdateSuccess,
  isDeleteConfirmationResponse,
  setIsDeleteConfirmationResponse,
  jobSeekers,
  setIsFilterOpen,
  isFilterOpen,
  dataAfterPaymentConfirmation,
}) => {
  const {
    setIsPaymentConfirmation,
    setIsMessageOpen,
    setIsDeleteConfirmation,
    setIsInvoiceOpen,
    setIsCustomizedRequirmentsOpen,
    setIsRequestVideoCallOpen,
    setIsJobPreviewOpen,
  } = useContext(PopupContext);
  window.localStorage.setItem("isUserProfile", false);
  return (
    <Router>
      <DashboardCompany
        path="/"
        setIsFilterOpen={setIsFilterOpen}
        isFilterOpen={isFilterOpen}
        filterCall={filteredDataCall}
        title={title}
        searchBy={searchBy}
        searchByFreelancer={searchByFreelancer}
        searchByUser={searchByUser}
        keywords={keywords}
        latitude={latitude}
        longitude={longitude}
        mapLocation={mapLocation}
        selectedRangeValue={selectedRangeValue}
        jobSeekers={jobSeekers}
      />
      <Messenger
        setIsPaymentConfirmation={setIsPaymentConfirmation}
        path="/messenger"
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
        setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
        setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
        setVideoCallUserData={setVideoCallUserData}
      />
      <Posting path="/posting" setDontShowSidebar={setDontShowSidebar} />
      <PostingDetails
        path="/posting/details"
        setDontShowSidebar={setDontShowSidebar}
        setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
        setVideoCallUserData={setVideoCallUserData}
        setIsRejectRequest={setIsRejectRequest}
        setIsMessageOpen={setIsMessageOpen}
        isRandom={false}
        setUserData={setUserData}
        setIsPaymentConfirmation={setIsPaymentConfirmation}
        dataAfterPaymentConfirmation={dataAfterPaymentConfirmation}
      />
      <PostJob
        path="/post-job"
        setIsJobPreviewOpen={setIsJobPreviewOpen}
        previewData={setIsJobPreviewData}
      />
      <Draft
        path="/job-draft"
        name="Saved Jobs"
        setIsJobPreviewOpen={setIsJobPreviewOpen}
      />
      <PostProject
        path="/post-project"
        setIsJobPreviewOpen={setIsJobPreviewOpen}
        previewData={setIsJobPreviewData}
      />
      <Draft
        path="/project-draft"
        name="Saved Projects"
        setIsJobPreviewOpen={setIsJobPreviewOpen}
      />
      <Plan
        path="/plan"
        setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
      />
      <Billing
        path="/billing"
        setIsInvoiceOpen={setIsInvoiceOpen}
        setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
      />
      <Invoices
        path="/invoices"
        setIsInvoiceOpen={setIsInvoiceOpen}
        setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
      />
      <PlannedVideoCalls
        path="/planned-video-calls"
        isOn="company"
        setIsMessageOpen={setIsMessageOpen}
        setVideoCallUpdate={setVideoCallUpdate}
        setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
        videoCallUpdate={videoCallUpdate}
        videoCallUpdateSuccess={videoCallUpdateSuccess}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
        setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
      />
      <PersonalDetails
        path="/profile-edit"
        isOn="company"
        setDontShowSidebar={setDontShowSidebar}
      />
      <PersonalDetailsPreview
        path="/profile"
        isOn="company"
        setDontShowSidebar={setDontShowSidebar}
      />
      <PersonelDetailPreviewMessengerUser
        setDontShowSidebar={setDontShowSidebar}
        from="profile"
        path="/messenger-user-profile"
        isOn="company"
      />
      <PageNotFound path="*" />
    </Router>
  );
};
