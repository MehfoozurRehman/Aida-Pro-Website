import React, {
  useContext,
  Suspense,
  useState,
  useEffect,
  createContext,
} from "react";
import { updateNotification } from "Redux/Actions/AppActions";
import { changeStateOfUserOffline } from "Utils/common";
import { NotificationMessage } from "Screens/Toasts";
import { LoadScript } from "@react-google-maps/api";
import { LoadingMask } from "Screens/LoadingMask";
import AppRouterRoutes from "./AppRouterRoutes";
import AppRouterPopups from "./AppRouterPopups";
import UserContext from "Context/UserContext";
import { LIBRARIES } from "Utils/Constants";
import firebase from "firebase";
import { store } from "index";
import { Footer } from "Components";
import Gallery from "Components/Gallery";

export const PopupContext = createContext();

export const AppRouter = () => {
  const [isLoginOpen, setIsLoginOpen] = useState(false);
  const [isAskQuestionOpen, setIsAskQuestionOpen] = useState(false);
  const [isForgotPasswordOpen, setIsForgotPasswordOpen] = useState(false);
  const [isFromForgotPassword, setIsFromForgotPassword] = useState(false);
  const [isChangePasswordOpen, setIsChangePasswordOpen] = useState(false);
  const [isJobPreviewOpen, setIsJobPreviewOpen] = useState(false);
  const [isJobPreviewData, setIsJobPreviewData] = useState();
  const [isEmailVerificationOpen, setIsEmailVerificationOpen] = useState(false);
  const [isNewPasswordOpen, setIsNewPasswordOpen] = useState(false);
  const [isPasswordChangedOpen, setIsPasswordChangedOpen] = useState(false);
  const [isAddDegreeOpen, setIsAddDegreeOpen] = useState(false);
  const [editDegree, setEditDegree] = useState(null);
  const [isAddCertificationsOpen, setIsAddCertificationsOpen] = useState(false);
  const [editCertificationData, setEditCertificationData] = useState(null);
  const [isAddSkillsOpen, setIsAddSkillsOpen] = useState(false);
  const [isAddWorkExperianceOpen, setIsAddWorkExperianceOpen] = useState(false);
  const [addWorkExperianceEditData, setAddWorkExperianceEditData] =
    useState(null);
  const [isEditDegreeOpen, setIsEditDegreeOpen] = useState(false);
  const [isEditCertificationsOpen, setIsEditCertificationsOpen] =
    useState(false);
  const [isEditSkillsOpen, setIsEditSkillsOpen] = useState(false);
  const [isEditWorkExperianceOpen, setIsEditWorkExperianceOpen] =
    useState(false);
  const [isUploadProjectOpen, setIsUploadProjectOpen] = useState(false);
  const [isEditProjectOpen, setIsEditProjectOpen] = useState(false);
  const [isMessageOpen, setIsMessageOpen] = useState(false);
  const [isRequestVideoCallOpen, setIsRequestVideoCallOpen] = useState(false);
  const [isRejectRequest, setIsRejectRequest] = useState(false);
  const [isAddSocialsOpen, setIsAddSocialsOpen] = useState(false);
  const [isEditSocialsOpen, setIsEditSocialsOpen] = useState(false);
  const [isInvoiceOpen, setIsInvoiceOpen] = useState(false);
  const [userData, setUserData] = useState(null);
  const [askQuestionSuccess, setAskQuestionSuccess] = useState(false);

  const [isCustomizedRequirmentsOpen, setIsCustomizedRequirmentsOpen] =
    useState(false);
  const [isApplyForJob, setIsApplyForJob] = useState(false);

  const [redirectURLFromLogin, setRedirectURLFromLogin] =
    useState("/home-company");
  const [isPaymentConfirmation, setIsPaymentConfirmation] = useState(false);
  const [dataAfterPaymentConfirmation, setDataAfterPaymentConfirmation] =
    useState(false);
  const [isAddConfirmation, setIsAddConfirmation] = useState(false);
  const [isEditConfirmation, setIsEditConfirmation] = useState(false);
  const [isDeleteConfirmation, setIsDeleteConfirmation] = useState(false);
  const [isDeleteConfirmationResponse, setIsDeleteConfirmationResponse] =
    useState(false);
  const [isContactSuccess, setIsContactSuccess] = useState(false);
  const [isShareDiscusssion, setIsShareDiscusssion] = useState(false);
  const [isApplyForJobProject, setIsApplyForJobProject] = useState(false);
  const [selectedProjectData, setSelectedProjectData] = useState(null);
  const [selectedSkills, setSelectedSkills] = useState([]);
  const [alertDialogVisibility, setAlertDialogVisibility] = useState(false);
  const [isSetHourlyRateOpen, setIsSetHourlyRateOpen] = useState(false);
  const [isSelectWorkTimeOpen, setIsSelectWorkTimeOpen] = useState(false);
  const [selectedHourlyRate, setSelectedHourlyRate] = useState("");
  const [videoCallUserData, setVideoCallUserData] = useState(null);
  const [showFooter, setShowFooter] = useState(true);
  const [videoCallUpdate, setVideoCallUpdate] = useState(null);
  const [isGalleryOpen, setIsGalleryOpen] = useState(false);
  const [videoCallUpdateSuccess, setVideoCallUpdateSuccess] = useState(null);
  const [text, setText] = useState("");

  const user = useContext(UserContext);

  const setPreviewData = (dataObject) => {
    setIsJobPreviewData(dataObject);
  };

  const setUserDataSelect = (value) => {
    setUserData(value);
  };

  const getAskQuestionData = (value) => {
    setAskQuestionSuccess(value);
  };

  const getSelectedProjectData = (value) => {
    setSelectedProjectData(value);
  };

  const setHeadingAndTextForAlertDialog = (text) => {
    setText(text);
  };

  const handleEvent = () => {
    if (isLoginOpen) setIsLoginOpen(false);
  };

  useEffect(() => {
    window.addEventListener("beforeunload", handleUnload);
  });

  const handleUnload = (e) => {
    var message = "o/";
    // console.log("message", message);
    changeStateOfUserOffline(user.FirebaseId);
    // (e || window.event).returnValue = message; //Gecko + IE
    // return message;
  };

  useEffect(() => {
    if (firebase.messaging.isSupported()) {
      const messaging = firebase.messaging();
      messaging
        .getToken({
          vapidKey:
            "BIhXfmJaG-BO4Yx6Z5F6fSrgCFuLXywZV0h3J0gg2in0foj2NChS_DRntWhZxusH47CXAUm3_rl0kie7jEchObQ",
        })
        .then((currentToken) => {
          if (currentToken) {
            // console.log(currentToken, "currentToken");
            onMessageListener();
            window.localStorage.setItem("ft", currentToken);
          } else
            console.log(
              "No registration token available. Request permission to generate one."
            );
        })
        .catch((err) => {
          // console.log("An error occurred while retrieving token. ", err);
        });
    }
    window.addEventListener("popstate", handleEvent);
    return () => window.removeEventListener("popstate", handleEvent);
  });

  const onMessageListener = () =>
    new Promise((resolve) => {
      if (firebase.messaging.isSupported()) {
        firebase.messaging().onMessage((payload) => {
          resolve(payload);
          NotificationMessage(payload, setIsLoginOpen);
          store.dispatch(updateNotification(payload));
        });
      }
    });

  return (
    <Suspense fallback={<LoadingMask />}>
      <LoadScript
        id="google-map-script-loader"
        googleMapsApiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
        // language="English"
        libraries={LIBRARIES}
      >
        <PopupContext.Provider
          value={{
            isLoginOpen,
            setIsLoginOpen,
            isForgotPasswordOpen,
            setIsForgotPasswordOpen,
            isFromForgotPassword,
            setIsFromForgotPassword,
            isAskQuestionOpen,
            setIsAskQuestionOpen,
            isNewPasswordOpen,
            setIsNewPasswordOpen,
            isEmailVerificationOpen,
            setIsEmailVerificationOpen,
            isPasswordChangedOpen,
            setIsPasswordChangedOpen,
            alertDialogVisibility,
            setAlertDialogVisibility,
            isPaymentConfirmation,
            setIsPaymentConfirmation,
            isMessageOpen,
            setIsMessageOpen,
            isEditDegreeOpen,
            setIsEditDegreeOpen,
            setIsDeleteConfirmation,
            isInvoiceOpen,
            setIsInvoiceOpen,
            isCustomizedRequirmentsOpen,
            setIsCustomizedRequirmentsOpen,
            isEditSocialsOpen,
            setIsEditSocialsOpen,
            isAddSocialsOpen,
            setIsRequestVideoCallOpen,
            isRequestVideoCallOpen,
            setIsAddSocialsOpen,
            isEditWorkExperianceOpen,
            setIsEditWorkExperianceOpen,
            isUploadProjectOpen,
            setIsUploadProjectOpen,
            isEditProjectOpen,
            setIsEditProjectOpen,
            isEditSkillsOpen,
            setIsEditSkillsOpen,
            isEditCertificationsOpen,
            setIsEditCertificationsOpen,
            isAddWorkExperianceOpen,
            setIsAddWorkExperianceOpen,
            isChangePasswordOpen,
            setIsChangePasswordOpen,
            isAddSkillsOpen,
            setIsAddSkillsOpen,
            isSetHourlyRateOpen,
            setIsSetHourlyRateOpen,
            isSelectWorkTimeOpen,
            setIsSelectWorkTimeOpen,
            isAddCertificationsOpen,
            setIsAddCertificationsOpen,
            isJobPreviewOpen,
            setIsJobPreviewOpen,
            isAddDegreeOpen,
            setIsAddDegreeOpen,
            isEditConfirmation,
            setIsEditConfirmation,
            isGalleryOpen,
            setIsGalleryOpen,
          }}
        >
          {isGalleryOpen ? <Gallery onClose={setIsGalleryOpen} /> : null}
          <AppRouterPopups
            redirectURLFromLogin={redirectURLFromLogin}
            setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
            getAskQuestionData={getAskQuestionData}
            dataAfterPaymentConfirmation={dataAfterPaymentConfirmation}
            setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
            isContactSuccess={isContactSuccess}
            setIsContactSuccess={setIsContactSuccess}
            isShareDiscusssion={isShareDiscusssion}
            setIsShareDiscusssion={setIsShareDiscusssion}
            isAddConfirmation={isAddConfirmation}
            setIsAddConfirmation={setIsAddConfirmation}
            isDeleteConfirmation={isDeleteConfirmation}
            setIsDeleteConfirmation={setIsDeleteConfirmation}
            setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
            text={text}
            isJobPreviewData={isJobPreviewData}
            editDegree={editDegree}
            editCertificationData={editCertificationData}
            selectedSkills={selectedSkills}
            selectedHourlyRate={selectedHourlyRate}
            addWorkExperianceEditData={addWorkExperianceEditData}
            selectedProjectData={selectedProjectData}
            userData={userData}
            videoCallUserData={videoCallUserData}
            videoCallUpdate={videoCallUpdate}
            setVideoCallUpdateSuccess={setVideoCallUpdateSuccess}
            isRejectRequest={isRejectRequest}
            setIsRejectRequest={setIsRejectRequest}
            isApplyForJob={isApplyForJob}
            setIsApplyForJob={setIsApplyForJob}
            setIsApplyForJobProject={setIsApplyForJobProject}
          />
          <AppRouterRoutes
            setRedirectURLFromLogin={setRedirectURLFromLogin}
            setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
            dataAfterPaymentConfirmation={dataAfterPaymentConfirmation}
            setIsApplyForJob={setIsApplyForJob}
            setAlertDialogVisibility={setAlertDialogVisibility}
            setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
            setIsContactSuccess={setIsContactSuccess}
            askQuestionSuccess={askQuestionSuccess}
            setIsShareDiscusssion={setIsShareDiscusssion}
            setPreviewData={setPreviewData}
            setVideoCallUserData={setVideoCallUserData}
            setIsRejectRequest={setIsRejectRequest}
            setUserDataSelect={setUserDataSelect}
            setIsPaymentConfirmation={setIsPaymentConfirmation}
            setVideoCallUpdate={setVideoCallUpdate}
            videoCallUpdate={videoCallUpdate}
            videoCallUpdateSuccess={videoCallUpdateSuccess}
            setEditDegree={setEditDegree}
            setEditCertificationData={setEditCertificationData}
            getSelectedProjectData={getSelectedProjectData}
            setSelectedSkills={setSelectedSkills}
            setIsDeleteConfirmation={setIsDeleteConfirmation}
            setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
            isDeleteConfirmationResponse={isDeleteConfirmationResponse}
            setSelectedHourlyRate={setSelectedHourlyRate}
            setIsApplyForJobProject={setIsApplyForJobProject}
            isApplyForJobProject={isApplyForJobProject}
            setShowFooter={setShowFooter}
            setAddWorkExperianceEditData={setAddWorkExperianceEditData}
          />
          {showFooter ? <Footer /> : null}
        </PopupContext.Provider>
      </LoadScript>
    </Suspense>
  );
};
