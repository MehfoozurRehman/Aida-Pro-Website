import React, { useContext } from "react";
import { Router, Redirect } from "@reach/router";
import UserContext from "Context/UserContext";
import { PageNotFound } from "Screens/404";
import { PopupContext } from "./AppRouter";

const Homepage = React.lazy(() => import("Screens/HomePage"));
const FaqDetails = React.lazy(() => import("Screens/FaqDetails"));
const HomepageCompany = React.lazy(() => import("Screens/HomepageCompany"));
const HomeCompany = React.lazy(() => import("Screens/HomeCompany"));
const HomeEmployment = React.lazy(() => import("Screens/HomeEmployment"));
const HomeFreelancer = React.lazy(() => import("Screens/HomeFreelancer"));
const CoffeeCorner = React.lazy(() => import("Screens/CoffeeCorner"));
const BlogArtical = React.lazy(() => import("Screens/BlogArtical"));
const Blog = React.lazy(() => import("Screens/Blog"));
const SignUp = React.lazy(() => import("Screens/SignUp"));
const AboutUs = React.lazy(() => import("Screens/AboutUs"));
const Contact = React.lazy(() => import("Screens/Contact"));
const TermsConditions = React.lazy(() => import("Screens/TermsConditions"));
const PrivacyPolicy = React.lazy(() => import("Screens/PrivacyPolicy"));
const Faq = React.lazy(() => import("Screens/Faq"));
const Login = React.lazy(() => import("Screens/Login"));
const CoffeeCornerDiscussion = React.lazy(() =>
  import("Screens/CoffeeCornerDiscussion")
);
const JobsProjectsDetailsCoffeeCorner = React.lazy(() =>
  import("Screens/JobProjectDetailsCoffeeCorner")
);
const HomepageResearcher = React.lazy(() =>
  import("Screens/HomepageEmployment")
);
const HomePageFreelancer = React.lazy(() =>
  import("Screens/HomepageFreelancer")
);
const PaymentProcessingScreen = React.lazy(() =>
  import("Screens/PaymentProcessingScreen")
);
const JobsProjectsDetails = React.lazy(() =>
  import("Screens/JobsProjectsDetails")
);
const VideoChatContainerMainScreen = React.lazy(() =>
  import("Screens/MainScreen/MainAppVideo")
);

export const appRoles = {
  cm: "Company",
  em: "JobSekker",
  fr: "Freelancer",
};

const AuthConsumer = ({ children }) => {
  const user = useContext(UserContext);
  return children({ user: user, isAuthenticated: user.Role !== undefined });
};

const LoginRoute = ({ component: Component, ...rest }) => (
  <AuthConsumer>
    {({ user, isAuthenticated }) => {
      if (isAuthenticated) {
        if (user.Role.Title === appRoles.cm) {
          return <Redirect from="" to="/home-company" noThrow />;
        } else if (user.Role.Title === appRoles.em) {
          return <Redirect from="" to="/home-professional" noThrow />;
        } else if (user.Role.Title === appRoles.fr) {
          return <Redirect from="" to="/home-freelancer" noThrow />;
        }
      } else {
        return <Component {...rest} />;
      }
    }}
  </AuthConsumer>
);

export default function AppRouterRoutes({
  setRedirectURLFromLogin,
  setDataAfterPaymentConfirmation,
  setIsApplyForJob,
  setHeadingAndTextForAlertDialog,
  setIsContactSuccess,
  askQuestionSuccess,
  setIsShareDiscusssion,
  setPreviewData,
  setVideoCallUserData,
  setIsRejectRequest,
  setUserDataSelect,
  setVideoCallUpdate,
  videoCallUpdate,
  videoCallUpdateSuccess,
  setEditDegree,
  setAddWorkExperianceEditData,
  setEditCertificationData,
  getSelectedProjectData,
  setSelectedSkills,
  isDeleteConfirmationResponse,
  setIsDeleteConfirmationResponse,
  setSelectedHourlyRate,
  setIsApplyForJobProject,
  isApplyForJobProject,
  setShowFooter,
  dataAfterPaymentConfirmation,
}) {
  const {
    setIsLoginOpen,
    setIsAskQuestionOpen,
    setIsEmailVerificationOpen,
    setAlertDialogVisibility,
    setIsPaymentConfirmation,
    setIsMessageOpen,
    setIsEditDegreeOpen,
    setIsDeleteConfirmation,
    setIsInvoiceOpen,
    setIsCustomizedRequirmentsOpen,
    setIsRequestVideoCallOpen,
    setIsAddSocialsOpen,
    setIsEditWorkExperianceOpen,
    setIsUploadProjectOpen,
    setIsEditProjectOpen,
    setIsEditSkillsOpen,
    setIsEditCertificationsOpen,
    setIsAddWorkExperianceOpen,
    setIsAddSkillsOpen,
    setIsSetHourlyRateOpen,
    setIsSelectWorkTimeOpen,
    setIsAddCertificationsOpen,
    setIsJobPreviewOpen,
    setIsAddDegreeOpen,
  } = useContext(PopupContext);
  return (
    <Router>
      <Homepage path="/" exact setIsLoginOpen={setIsLoginOpen} />
      <HomepageCompany
        path="/company"
        setIsLoginOpen={setIsLoginOpen}
        setRedirectURLFromLogin={setRedirectURLFromLogin}
        setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
      />
      <HomePageFreelancer
        path="/freelancer"
        setIsLoginOpen={setIsLoginOpen}
        setRedirectURLFromLogin={setRedirectURLFromLogin}
        setIsApplyForJob={setIsApplyForJob}
      />
      <HomepageResearcher
        path="/professional"
        setIsLoginOpen={setIsLoginOpen}
        setRedirectURLFromLogin={setRedirectURLFromLogin}
        setIsApplyForJob={setIsApplyForJob}
      />

      <SignUp
        path="/sign-up"
        setIsLoginOpen={setIsLoginOpen}
        redirectedFrom="company"
        setIsEmailVerificationOpen={setIsEmailVerificationOpen}
        setAlertDialogVisibility={setAlertDialogVisibility}
        setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
      />

      <AboutUs path="/about-us" setIsLoginOpen={setIsLoginOpen} />
      <Contact
        path="/contact"
        setIsLoginOpen={setIsLoginOpen}
        setIsContactSuccess={setIsContactSuccess}
      />
      <PaymentProcessingScreen
        path="/PaymentProcessing"
        setIsLoginOpen={setIsLoginOpen}
      />
      <CoffeeCorner
        path="/coffee-corner"
        setIsLoginOpen={setIsLoginOpen}
        setIsAskQuestionOpen={setIsAskQuestionOpen}
        askQuestionSuccess={askQuestionSuccess}
        setIsShareDiscusssion={setIsShareDiscusssion}
      />
      <CoffeeCornerDiscussion
        path="/coffee-corner-discussion/:Id"
        setIsLoginOpen={setIsLoginOpen}
        setIsAskQuestionOpen={setIsAskQuestionOpen}
        askQuestionSuccess={askQuestionSuccess}
        setIsShareDiscusssion={setIsShareDiscusssion}
      />
      <TermsConditions
        path="/terms-conditions"
        setIsLoginOpen={setIsLoginOpen}
      />
      <PrivacyPolicy path="/privacy-policy" setIsLoginOpen={setIsLoginOpen} />
      <Blog path="/blog" setIsLoginOpen={setIsLoginOpen} />
      <HomeCompany
        path="/home-company/*"
        setIsJobPreviewOpen={setIsJobPreviewOpen}
        setIsJobPreviewData={setPreviewData}
        setIsMessageOpen={setIsMessageOpen}
        setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
        setVideoCallUserData={setVideoCallUserData}
        setIsRejectRequest={setIsRejectRequest}
        setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
        setIsInvoiceOpen={setIsInvoiceOpen}
        setUserData={setUserDataSelect}
        setIsPaymentConfirmation={setIsPaymentConfirmation}
        setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
        setVideoCallUpdate={setVideoCallUpdate}
        videoCallUpdate={videoCallUpdate}
        videoCallUpdateSuccess={videoCallUpdateSuccess}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
        setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
        dataAfterPaymentConfirmation={dataAfterPaymentConfirmation}
      />
      <HomeEmployment
        path="/home-professional/*"
        setIsAddDegreeOpen={setIsAddDegreeOpen}
        setEditDegree={setEditDegree}
        setIsAddCertificationsOpen={setIsAddCertificationsOpen}
        setIsAddSkillsOpen={setIsAddSkillsOpen}
        setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
        setAddWorkExperianceEditData={setAddWorkExperianceEditData}
        setIsEditDegreeOpen={setIsEditDegreeOpen}
        setIsEditCertificationsOpen={setIsEditCertificationsOpen}
        setEditCertificationData={setEditCertificationData}
        setIsEditSkillsOpen={setIsEditSkillsOpen}
        setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
        setIsAddSocialsOpen={setIsAddSocialsOpen}
        setIsEditProjectOpen={setIsEditProjectOpen}
        getSelectedProjectData={getSelectedProjectData}
        setIsEditSocialsOpen={setIsAddSocialsOpen}
        setIsUploadProjectOpen={setIsUploadProjectOpen}
        setSelectedSkills={setSelectedSkills}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
        setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
        setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
        setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
        setSelectedHourlyRate={setSelectedHourlyRate}
        setIsMessageOpen={setIsMessageOpen}
        setUserData={setUserDataSelect}
        setVideoCallUpdate={setVideoCallUpdate}
      />
      <HomeFreelancer
        path="/home-freelancer/*"
        setIsAddDegreeOpen={setIsAddDegreeOpen}
        setEditDegree={setEditDegree}
        setIsAddCertificationsOpen={setIsAddCertificationsOpen}
        setIsAddSkillsOpen={setIsAddSkillsOpen}
        setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
        setAddWorkExperianceEditData={setAddWorkExperianceEditData}
        setIsEditDegreeOpen={setIsEditDegreeOpen}
        setIsEditCertificationsOpen={setIsEditCertificationsOpen}
        setEditCertificationData={setEditCertificationData}
        setIsEditSkillsOpen={setIsEditSkillsOpen}
        setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
        setIsUploadProjectOpen={setIsUploadProjectOpen}
        setIsEditProjectOpen={setIsEditProjectOpen}
        getSelectedProjectData={getSelectedProjectData}
        setIsAddSocialsOpen={setIsAddSocialsOpen}
        setIsEditSocialsOpen={setIsAddSocialsOpen}
        setSelectedSkills={setSelectedSkills}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
        setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
        setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
        setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
        setSelectedHourlyRate={setSelectedHourlyRate}
        setIsMessageOpen={setIsMessageOpen}
        setUserData={setUserDataSelect}
        setVideoCallUpdate={setVideoCallUpdate}
      />

      {/* Login Route */}
      <LoginRoute
        component={Login}
        path="/login"
        setIsLoginOpen={setIsLoginOpen}
      />

      <JobsProjectsDetails
        path="/project-details"
        setIsApplyForJob={setIsApplyForJob}
        setIsLoginOpen={setIsLoginOpen}
        setIsApplyForJobProject={setIsApplyForJobProject}
        isApplyForJobProject={isApplyForJobProject}
        setAlertDialogVisibility={setAlertDialogVisibility}
        setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
        isRandom
      />
      <JobsProjectsDetails
        path="/job-details"
        setIsApplyForJob={setIsApplyForJob}
        setIsLoginOpen={setIsLoginOpen}
        setIsApplyForJobProject={setIsApplyForJobProject}
        isApplyForJobProject={isApplyForJobProject}
        setAlertDialogVisibility={setAlertDialogVisibility}
        setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
        isRandom
      />
      <JobsProjectsDetailsCoffeeCorner
        path="/job-project-details"
        setIsApplyForJob={setIsApplyForJob}
        setIsLoginOpen={setIsLoginOpen}
        setIsApplyForJobProject={setIsApplyForJobProject}
        isApplyForJobProject={isApplyForJobProject}
        setAlertDialogVisibility={setAlertDialogVisibility}
        setHeadingAndTextForAlertDialog={setHeadingAndTextForAlertDialog}
        isRandom
      />
      <JobsProjectsDetails
        path="/posting-details"
        setIsLoginOpen={setIsLoginOpen}
        setDontShowSidebar={false}
        setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
        setIsRejectRequest={setIsRejectRequest}
        setIsMessageOpen={setIsMessageOpen}
        isRandom
        fromHomeCompany={true}
      />
      {/* <JobsProjectsDetails
     path="/job-details"
     setIsLoginOpen={setIsLoginOpen}
    /> */}
      <BlogArtical path="/blog-artical/" setIsLoginOpen={setIsLoginOpen} />
      <Faq setIsLoginOpen={setIsLoginOpen} path="/faq" />
      <FaqDetails setIsLoginOpen={setIsLoginOpen} path="/faq-details" />

      {/* <VideoChatContainer setShowFooter={setShowFooter} path="/videoChat/:Id" /> */}

      <VideoChatContainerMainScreen
        setShowFooter={setShowFooter}
        path="/videoChat/:Id"
      />

      <PageNotFound path="*" />
    </Router>
  );
}
