import React from "react";
import { SidebarLink } from "Components";
import Img from "react-cool-img";
import {
  appliedActive,
  appliedInActive,
  chatNotSelected,
  chatSelected,
  facetimeButtonActive,
  facetimeButtonInActive,
  homeNotSelected,
  homeSelected,
  interestedNotSelected,
  interestedSelected,
  myProjectNotSelected,
  postingNotSelected,
  postingSelected,
  postProjectSelected,
  profileNotSelected,
  profileSelected,
} from "Assets";

export default function HomeFreelancerSidebar({}) {
  return (
    <div className="dashboard__container__sidebar">
      <SidebarLink
        name="Home"
        path="/home-freelancer"
        defaultChecked={true}
        selectedImg={
          <Img loading="lazy" src={homeSelected} alt="homeSelected" />
        }
        notSelectedImg={
          <Img loading="lazy" src={homeNotSelected} alt="homeNotSelected" />
        }
      />
      <SidebarLink
        name="Messenger"
        path="/home-freelancer/messenger"
        selectedImg={
          <Img loading="lazy" src={chatSelected} alt="chatSelected" />
        }
        notSelectedImg={
          <Img loading="lazy" src={chatNotSelected} alt="chatNotSelected" />
        }
      />
      <SidebarLink
        name="Personal details"
        path="/home-freelancer/personal-details-preview"
        selectedImg={
          <Img loading="lazy" src={profileSelected} alt="profileSelected" />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={profileNotSelected}
            alt="profileNotSelected"
          />
        }
      />
      <SidebarLink
        name="Professional details"
        path="/home-freelancer/professional-details"
        selectedImg={
          <Img loading="lazy" src={postingSelected} alt="postingSelected" />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={postingNotSelected}
            alt="postingNotSelected"
          />
        }
      />
      <SidebarLink
        name="My projects"
        path="/home-freelancer/project"
        selectedImg={
          <Img
            loading="lazy"
            src={postProjectSelected}
            alt="myProjectSelected"
          />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={myProjectNotSelected}
            alt="myProjectNotSelected"
          />
        }
      />
      <SidebarLink
        name="Applied"
        path="/home-freelancer/applied"
        selectedImg={
          <Img loading="lazy" src={appliedActive} alt="appliedActive" />
        }
        notSelectedImg={
          <Img loading="lazy" src={appliedInActive} alt="appliedInActive" />
        }
      />
      <SidebarLink
        name="Interested"
        path="/home-freelancer/interested"
        selectedImg={
          <Img
            loading="lazy"
            src={interestedSelected}
            alt="interestedSelected"
          />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={interestedNotSelected}
            alt="interestedNotSelected"
          />
        }
      />
      <SidebarLink
        name="Video meetings"
        path="/home-freelancer/planned-video-calls"
        selectedImg={
          <Img
            loading="lazy"
            src={facetimeButtonActive}
            alt="interestedSelected"
          />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={facetimeButtonInActive}
            alt="interestedNotSelected"
          />
        }
      />
    </div>
  );
}
