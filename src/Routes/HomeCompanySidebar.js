import React from "react";
import { SidebarLink } from "Components";
import Img from "react-cool-img";
import {
  BalanceNotSelected,
  BalanceSelected,
  chatNotSelected,
  chatSelected,
  facetimeButtonActive,
  facetimeButtonInActive,
  homeNotSelected,
  homeSelected,
  PlanNotSelected,
  PlanSelected,
  postingNotSelected,
  postingSelected,
  postJobNotSelected,
  postJobSelected,
  postProjectNotSelected,
  postProjectSelected,
} from "Assets";

export default function HomeCompanySidebar() {
  return (
    <div className="dashboard__container__sidebar">
      <SidebarLink
        name="Home"
        path="/home-company"
        defaultChecked={true}
        selectedImg={
          <Img loading="lazy" src={homeSelected} alt="homeSelected" />
        }
        notSelectedImg={
          <Img loading="lazy" src={homeNotSelected} alt="homeNotSelected" />
        }
      />
      <SidebarLink
        name="Messenger"
        path="/home-company/messenger"
        selectedImg={
          <Img loading="lazy" src={chatSelected} alt="chatSelected" />
        }
        notSelectedImg={
          <Img loading="lazy" src={chatNotSelected} alt="chatNotSelected" />
        }
      />
      <SidebarLink
        name="Postings"
        path="/home-company/posting"
        selectedImg={
          <Img loading="lazy" src={postingSelected} alt="postingSelected" />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={postingNotSelected}
            alt="postingNotSelected"
          />
        }
      />
      <SidebarLink
        name="Post a job"
        path="/home-company/post-job"
        selectedImg={
          <Img loading="lazy" src={postJobSelected} alt="postJobSelected" />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={postJobNotSelected}
            alt="postJobNotSelected"
          />
        }
      />
      <SidebarLink
        name="Post a project"
        path="/home-company/post-project"
        selectedImg={
          <Img
            loading="lazy"
            src={postProjectSelected}
            alt="postProjectSelected"
          />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={postProjectNotSelected}
            alt="postJobNotSelected"
          />
        }
      />
      <SidebarLink
        name="Plans & pricing"
        path="/home-company/plan"
        selectedImg={
          <Img loading="lazy" src={PlanSelected} alt="PlanSelected" />
        }
        notSelectedImg={
          <Img loading="lazy" src={PlanNotSelected} alt="PlanNotSelected" />
        }
      />
      <SidebarLink
        name="Balance"
        path="/home-company/billing"
        selectedImg={
          <Img loading="lazy" src={BalanceSelected} alt="BalanceSelected" />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={BalanceNotSelected}
            alt="BalanceNotSelected"
          />
        }
      />
      <SidebarLink
        name="Video meetings"
        path="/home-company/planned-video-calls"
        selectedImg={
          <Img
            loading="lazy"
            src={facetimeButtonActive}
            alt="BalanceSelected"
          />
        }
        notSelectedImg={
          <Img
            loading="lazy"
            src={facetimeButtonInActive}
            alt="BalanceNotSelected"
          />
        }
      />
    </div>
  );
}
