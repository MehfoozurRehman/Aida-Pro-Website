import React, { useContext } from "react";
import { Router } from "@reach/router";
import { PopupContext } from "./AppRouter";
import {
  DashboardEmployment,
  Interested,
  JobsProjectsApplied,
  Messenger,
  PersonalDetails,
  PersonalDetailsPreview,
  PlannedVideoCalls,
  ProfessionalDetails,
  Profile,
  UserProfile,
  UserProjectPreview,
  UserProjects,
} from "Screens";

export const HomeEmployeeRouter = ({
  setIsFilterOpen,
  isFilterOpen,
  setEditDegree,
  setAddWorkExperianceEditData,
  setEditCertificationData,
  filteredDataCall,
  title,
  keywords,
  userName,
  getSelectedProjectData,
  setSelectedSkills,
  latitude,
  longitude,
  mapLocation,
  isDeleteConfirmationResponse,
  setSelectedHourlyRate,
  selectedRangeValue,
  setUserData,
  setVideoCallUpdate,
}) => {
  const {
    setIsMessageOpen,
    setIsEditDegreeOpen,
    setIsDeleteConfirmation,
    setIsEditSocialsOpen,
    setIsAddSocialsOpen,
    setIsEditWorkExperianceOpen,
    setIsUploadProjectOpen,
    setIsEditProjectOpen,
    setIsEditSkillsOpen,
    setIsEditCertificationsOpen,
    setIsAddWorkExperianceOpen,
    setIsAddSkillsOpen,
    setIsSetHourlyRateOpen,
    setIsSelectWorkTimeOpen,
    setIsAddCertificationsOpen,
    setIsAddDegreeOpen,
  } = useContext(PopupContext);
  window.localStorage.setItem("isJobView", false);

  let uName = "";

  if (userName) {
    uName = userName.split(" ");
    uName = uName[0];
  }

  return (
    <Router>
      <DashboardEmployment
        path="/"
        setIsFilterOpen={setIsFilterOpen}
        isFilterOpen={isFilterOpen}
        filterCall={filteredDataCall}
        title={title}
        keywords={keywords}
        userName={uName}
        latitude={latitude}
        longitude={longitude}
        mapLocation={mapLocation}
        selectedRangeValue={selectedRangeValue}
      />
      <Messenger
        path="/messenger"
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse} />
      <PersonalDetails path="/personal-details" isOn="professional" />
      <PersonalDetailsPreview
        path="/personal-details-preview"
        isOn="professional"
      />
      <ProfessionalDetails
        path="/professional-details"
        type="professional"
        setIsAddDegreeOpen={setIsAddDegreeOpen}
        setEditDegree={setEditDegree}
        setIsAddCertificationsOpen={setIsAddCertificationsOpen}
        setIsAddSkillsOpen={setIsAddSkillsOpen}
        setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
        setAddWorkExperianceEditData={setAddWorkExperianceEditData}
        setIsEditDegreeOpen={setIsEditDegreeOpen}
        setIsEditCertificationsOpen={setIsEditCertificationsOpen}
        setEditCertificationData={setEditCertificationData}
        setIsEditSkillsOpen={setIsEditSkillsOpen}
        setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
        setIsAddSocialsOpen={setIsAddSocialsOpen}
        setIsEditSocialsOpen={setIsEditSocialsOpen}
        setSelectedSkills={setSelectedSkills}
        setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
        setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
        setSelectedHourlyRate={setSelectedHourlyRate}
      />
      <UserProjects
        path="/project"
        setIsUploadProjectOpen={setIsUploadProjectOpen}
      />
      <UserProjectPreview
        path="/project-details"
        setIsEditProjectOpen={setIsEditProjectOpen}
        getSelectedProjectData={getSelectedProjectData}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
      />
      <PlannedVideoCalls
        path="/planned-video-calls"
        isOn="professional"
        setIsMessageOpen={setIsMessageOpen}
        setUserData={setUserData}
        setVideoCallUpdate={setVideoCallUpdate}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
      />
      <UserProfile path="/profile" from="profile" isOn="professional" />
      <Profile path="/change-password" />
      <JobsProjectsApplied path="/applied" />
      <Interested path="/interested" />
    </Router>
  );
};
