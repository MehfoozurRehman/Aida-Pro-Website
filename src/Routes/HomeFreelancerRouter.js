import React, { useContext } from "react";
import { Router } from "@reach/router";
import {
  DashboardFreelancer,
  Interested,
  JobsProjectsApplied,
  Messenger,
  PersonalDetails,
  PersonalDetailsPreview,
  PlannedVideoCalls,
  ProfessionalDetails,
  Profile,
  UserProfile,
  UserProjectPreview,
  UserProjects,
} from "Screens";
import { PopupContext } from "./AppRouter";

export const HomeFreelancerRouter = ({
  setIsFilterOpen,
  isFilterOpen,
  setAddWorkExperianceEditData,
  setEditCertificationData,
  filteredDataCall,
  title,
  keywords,
  userName,
  getSelectedProjectData,
  setSelectedSkills,
  latitude,
  longitude,
  mapLocation,
  isDeleteConfirmationResponse,
  selectedRangeValue,
  setSelectedHourlyRate,
  setEditDegree,
  setUserData,
  setVideoCallUpdate,
}) => {
  const {
    setIsMessageOpen,
    setIsEditDegreeOpen,
    setIsDeleteConfirmation,
    setIsEditSocialsOpen,
    setIsAddSocialsOpen,
    setIsEditWorkExperianceOpen,
    setIsUploadProjectOpen,
    setIsEditProjectOpen,
    setIsEditSkillsOpen,
    setIsEditCertificationsOpen,
    setIsAddWorkExperianceOpen,
    setIsAddSkillsOpen,
    setIsSetHourlyRateOpen,
    setIsSelectWorkTimeOpen,
    setIsAddCertificationsOpen,
    setIsAddDegreeOpen,
  } = useContext(PopupContext);

  window.localStorage.setItem("isProjectView", false);
  localStorage.setItem("isOn", "freelancer");

  let uName = "";

  if (userName) {
    uName = userName.split(" ");
    uName = uName[0];
  }

  return (
    <Router>
      <DashboardFreelancer
        path="/"
        setIsFilterOpen={setIsFilterOpen}
        isFilterOpen={isFilterOpen}
        filterCall={filteredDataCall}
        title={title}
        keywords={keywords}
        userName={uName}
        latitude={latitude}
        longitude={longitude}
        mapLocation={mapLocation}
        selectedRangeValue={selectedRangeValue}
      />
      <Messenger
        path="/messenger"
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
      />
      <PersonalDetails path="/personal-details" isOn="freelancer" />
      <PersonalDetailsPreview
        path="/personal-details-preview"
        isOn="freelancer"
      />
      <ProfessionalDetails
        path="/professional-details"
        type="freelancer"
        setIsAddDegreeOpen={setIsAddDegreeOpen}
        setIsAddCertificationsOpen={setIsAddCertificationsOpen}
        setIsAddSkillsOpen={setIsAddSkillsOpen}
        setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
        setAddWorkExperianceEditData={setAddWorkExperianceEditData}
        setIsEditDegreeOpen={setIsEditDegreeOpen}
        setIsEditCertificationsOpen={setIsEditCertificationsOpen}
        setEditCertificationData={setEditCertificationData}
        setIsEditSkillsOpen={setIsEditSkillsOpen}
        setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
        setIsAddSocialsOpen={setIsAddSocialsOpen}
        setIsEditSocialsOpen={setIsEditSocialsOpen}
        setSelectedSkills={setSelectedSkills}
        setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
        setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
        setSelectedHourlyRate={setSelectedHourlyRate}
        setEditDegree={setEditDegree}
      />
      <UserProjects
        path="/project"
        setIsUploadProjectOpen={setIsUploadProjectOpen}
      />
      <UserProjectPreview
        path="/project-details"
        setIsEditProjectOpen={setIsEditProjectOpen}
        getSelectedProjectData={getSelectedProjectData}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
      />
      <PlannedVideoCalls
        path="/planned-video-calls"
        isOn="freelancer"
        setIsMessageOpen={setIsMessageOpen}
        setUserData={setUserData}
        setVideoCallUpdate={setVideoCallUpdate}
        setIsDeleteConfirmation={setIsDeleteConfirmation}
        isDeleteConfirmationResponse={isDeleteConfirmationResponse}
      />
      <UserProfile path="/profile" from="profile" isOn="freelancer" />
      <Profile path="/change-password" />
      <JobsProjectsApplied path="/applied" />
      <Interested path="/interested" />
    </Router>
  );
};
