import axios from "axios";

export const getUserDetailById = (id) => {
  return axios.get(
    process.env.REACT_APP_BASEURL + `/api/user/GetById?id=${id}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getUserDetailByUserId = (UserId, Type, CompanyId) => {
  return axios.get(
    process.env.REACT_APP_BASEURL +
    `/api/User/GetUserDetailById?UserId=${UserId}&Type=${Type}&companyProfileId=${CompanyId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getUserInfoByUserId = (UserId, Type) => {
  return axios.get(
    process.env.REACT_APP_BASEURL +
    `/api/user/GetUserInfoById?UserId=${UserId}&Type=${Type}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const verifyUser = (data) => {
  return axios.post(
    process.env.REACT_APP_BASEURL + "/api/user/VerifiyUser",
    data,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const forgetPassword = (email) => {
  return axios.post(
    process.env.REACT_APP_BASEURL + "/api/user/ForgotPassword?email=" + email,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const resetPassword = (data) => {
  return axios.put(
    process.env.REACT_APP_BASEURL + "/api/user/ResetPassword",
    data,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const validateCode = (data) => {
  return axios.put(
    process.env.REACT_APP_BASEURL + "/api/user/ValidateCode",
    data,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const changePassword = (data) => {
  return axios.put(
    process.env.REACT_APP_BASEURL + "/api/user/ChangePassword",
    data,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};
