import axios from "axios";

export const employeeSignUp = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/JobSeeker/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerGetById = (id) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/JobSeeker/GetById?Id=${id}`,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerPersonalDetailsUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseeker/PersonalDetail`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerProfessionalDetailsUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseeker/UpdateMainProfessionalDetail`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

// export const jobSekkerSkillUpdate = (data) => {
//   return axios.post(
//     `${process.env.REACT_APP_BASEURL}api/jobseekerSkill/Post`,
//     data,
//     {
//       ["axios-retry"]: {
//         retries: 5,
//       },
//     }
//   );
// };

export const jobSekkerSkillUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/jobseekerSkill/MultiPost`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobseekerSkillDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerSkill/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerQualificationUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/jobseekerQualification/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerQualificationEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerQualification/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobseekerQualificationDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerQualification/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerCertificateUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/jobseekerCertificate/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerCertificateEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerCertificate/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobseekerCertificateDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerCertificate/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerExperienceUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/jobseekerExperience/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerExperienceEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerExperience/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerExperienceDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/jobseekerExperience/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerProjectPortfolio = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/JobSeekerPortfolio/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSeekerProjectPortfolioUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/JobSeekerPortfolio/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSeekerProjectPortfolioDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/JobSeekerPortfolio/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerRecentJobs = (jobSeekId, page, limit) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/JobSeekerPortfolio/Search?limit=${limit}&page=${page}&JobSeekerId=${jobSeekId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobSekkerJobs = ({
  page,
  limit,
  Title,
  jobType,
  jobLocation,
  jobLocationLat,
  jobLocationLng,
  dateOfPosting,
  salaryEstimate,
  fieldWork,
  salaryFrom,
  salaryTo,
  selectedRadius,
}) => {
  const params = {};
  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (Title) params.Title = Title;
  if (jobLocation) params.jobLocation = jobLocation;
  if (dateOfPosting) params.dateOfPosting = dateOfPosting;
  if (jobType.length > 0) params.jobType = jobType.toString();
  // if (salaryEstimate) params.salaryEstimate = salaryEstimate;
  if (fieldWork.length > 0) params.fieldWork = fieldWork.toString();
  if (salaryFrom) params.salaryFrom = salaryFrom;
  if (salaryTo) params.salaryTo = salaryTo;
  if (selectedRadius) params.radius = selectedRadius;
  if (jobLocationLat) params.Latitude = jobLocationLat;
  if (jobLocationLng) params.Longitude = jobLocationLng;

  return axios.get(`${process.env.REACT_APP_BASEURL}api/job/Search`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const jobSekkerJobsForHomePage = (
  page,
  limit,
  title,
  location,
  range
) => {
  const params = {};
  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (title) params.title = title;
  if (location) params.location = location;
  if (range) params.range = range;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/job/Search`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllInterestedJobByJobseekerId = (id, page, limit) => {
  const params = {};
  if (id) params.Id = id;
  if (page) params.page = page;
  if (limit) params.limit = limit;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/JobDetail/GetAllInterestedJobByJobseekerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllAppliedJobByJobseekerId = (id, page, limit) => {
  const params = {};
  if (id) params.Id = id;
  if (page) params.page = page;
  if (limit) params.limit = limit;
  return axios.get(`${process.env.REACT_APP_BASEURL}/api/JobDetail/GetAllAppliedJobByJobseekerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};