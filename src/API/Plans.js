import axios from "axios";

export const getPlans = () => {
  return axios.get(`${process.env.REACT_APP_BASEURL}api/plan/GetAll`, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const createPayment = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/PaymentRequest/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const updatePaymentStatus = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/PaymentRequest/UpdateStatusByTransactionId`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getPaymentRequestByCompanyId = (CompanyId) => {
  const params = {
    CompanyId
  };
  return axios.get(
    `${process.env.REACT_APP_BASEURL}/api/PaymentRequest/GetByCompanyId`,
    {
      params,
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};