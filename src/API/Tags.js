import axios from "axios";

export const getTopTags = (limit) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Tag/GetTopTags?limit=` + limit,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getTags = () => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Tag/GetAll`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};