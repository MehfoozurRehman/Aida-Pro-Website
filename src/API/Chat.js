import axios from "axios";
const queryString = require('query-string');

export const getChatUsersOfCompany = (user) => {
  let requestURL = process.env.REACT_APP_BASEURL.concat(
    "/api/Chat/GetChatByUserId?"
  );

  if (user.CompanyId !== null)
    requestURL = requestURL.concat("companyProfileId=").concat(user.CompanyId);
  else if (user.JobSeekerId !== null)
    requestURL = requestURL.concat("jobseekerId=").concat(user.JobSeekerId);
  else
    requestURL = requestURL.concat("freelancerId=").concat(user.FreelancerId);

  return axios.get(requestURL, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getChatUsersOfJobseeker = (id) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}:443/api/Chat/GetChatByUserId?jobseekerId=` +
    id,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getChatUsersOfFreelancer = (id) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}:443/api/Chat/GetChatByUserId?freelancerId=` +
    id,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getChatNodeName = (companyId, jobseekerId, freelancerId) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}:443/api/Chat/GetChatNodeName?companyProfileId=` +
    companyId +
    `&jobseekerId=` +
    jobseekerId +
    `&freelancerId=` +
    freelancerId,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const chatRequestPost = (
  companyId,
  freelancerId,
  jobSeekerid,
  message,
  companyName
) => {
  // let date = new Date();
  // const utcDate1 = new Date(Date.UTC(date.getUTCFullYear(), date.getMonth(), date.getDay(), date.getHours(), date.getMinutes(), date.getMilliseconds()));

  let intializeChatObject = {
    chat: {
      CompanyProfileId: companyId,
      FreelancerId: freelancerId,
      JobSeekerId: jobSeekerid,
      IsVideo: false,
      CompanyProfile: {
        CompanyName: companyName,
      },
    },
    message: {
      Body: message,
      Title: "New message",
    },
  };

  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/Chat/Post`,
    intializeChatObject,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const approveChat = (object) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/Chat/UpdateState`,
    object,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const deleteChat = (object) => {
  return axios.put(`${process.env.REACT_APP_BASEURL}api/Chat/Delete`, object, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const uploadFile = async (model, media) => {
  var bodyFormData = new FormData();
  bodyFormData.append("model", JSON.stringify(model));
  for (let index = 0; index < media.length; index++) {
    const element = media[index];
    bodyFormData.append("media", element, element.name)
  }

  return axios.post(`${process.env.REACT_APP_BASEURL}api/ChatStorage/MultiPart`, bodyFormData, {
    headers: {
      'content-type': 'multipart/form-data'
    },
    ["axios-retry"]: {
      retries: 5,
    },
  });
};
