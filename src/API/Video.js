import axios from "axios";

export const videoRequest = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const videoRequestUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const videoRequestUpdateStatus = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/RequestStatus`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllVideoRequest = (
  companyProfileId,
  jobseekerId,
  freelancerId,
  page,
  limit
) => {
  const params = {
    companyProfileId,
    jobseekerId,
    freelancerId,
    page,
    limit
  };
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/GetByUserId`,
    {
      params,
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const isCallValid = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/IsCallValid`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const deleteVideoRequest = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const stopVideoCallRequest = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/StopCall`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};