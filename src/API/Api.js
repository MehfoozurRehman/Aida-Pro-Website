import axios from "axios";

const Querystring = require("querystring");

export const login = (data) => {
  return axios.post(
    process.env.REACT_APP_BASEURL + "/api/user/Validate",
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getCountry = () => {
  return axios.get(process.env.REACT_APP_BASEURL + "/api/Country/GetAll", {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getCity = (countryId) => {
  return axios.get(
    process.env.REACT_APP_BASEURL +
    `/api/city/getByCountryId?countryId=${countryId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllIndustry = () => {
  return axios.get(process.env.REACT_APP_BASEURL + "/api/Industry/GetAll", {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const superToken = () => {
  let body = Querystring["stringify"]({
    username: "AIDApro",
    password: "AIDApro@123",
    grant_type: "password",
  });
  return axios.post(process.env.REACT_APP_BASEURL + "/tokensuper", body, {
    headers: {
      "Content-Type": "application/x-www-form-url; charset=urf-8",
    },
  });
};

export const userToken = ({ username, password }) => {
  let body = Querystring["stringify"]({
    username: username,
    password: password,
    grant_type: "password",
  });
  return axios.post(process.env.REACT_APP_BASEURL + "/token", body, {
    headers: {
      "Content-Type": "application/x-www-form-url; charset=urf-8",
    },
    ["axios-retry"]: {
      retries: 3,
    },
  });
};

export const getLookUpByPrefix = (prefix) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/LookupDetail/GetByLookupPrefix?prefix=${prefix}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllSkills = () => {
  return axios.get(`${process.env.REACT_APP_BASEURL}api/Skill/GetAll`, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const changePassword = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/User/ChangePassword`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const interestedJob = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/JobDetail/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const interestedProject = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/ProjectDetail/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const appliedJob = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/JobDetail/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const appliedProject = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/ProjectDetail/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllKeywords = () => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/keyword/GetByLimit?Limit=15`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};
export const getAnalyticsForJob = (
  Id,
  Type,
  isApplicant,
  isInterested,
  isVisitor,
  page
) => {
  const params = {
    limit: 5,
    page: page,
    Id,
    Type,
    isApplicant,
    isInterested,
    isVisitor,
  };
  return axios.get(`${process.env.REACT_APP_BASEURL}api/PostingDetail/Search`, {
    params,
    headers: {
      ["Content-Type"]: "application/json",
    },
    ["axios-retry"]: {
      retries: 2,
    },
  });
};

export const sendNotification = (data) => {
  return axios.post("https://fcm.googleapis.com/fcm/send", data, {
    headers: {
      ["Authorization"]:
        "key=AAAAI2uy2vo:APA91bFdh6MDeEnSP9Zih3oHDU_4l7HFvqXqNQP7-mO2joiPftL-HurpZXsyzfi0X-TL2YKiN0TeabTlyKlUvqtlBbhd3mGshXcxJX21erADiYb0SwKHXC6muyLB6jL2bRZxufNSJkRT",
      ["Content-Type"]: "application/json",
    },
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const sendVideoRequest = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/VideoRequest/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const contactUs = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/ContactUs/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const newsletter = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/NewsLetter/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllCountry = () => {
  return axios.get(`${process.env.REACT_APP_BASEURL}api/Country/GetAll`, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const updateJobIsRejected = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/JobDetail/RejectApplicant`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const updateProjectIsRejected = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/ProjectDetail/RejectApplicant`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getAllNotificationByUserId = (UserId) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Notification/GetByUserId?UserId=${UserId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const setAllNotificationsIsReadTrue = (UserId) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/Notification/UpdateByUserId?UserId=${UserId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};
