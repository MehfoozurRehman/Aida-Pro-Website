import axios from "axios";

export const projectPost = ({ data }) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Project/Post`;
  return axios.post(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const saveProjectDraftPost = (data) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Project/SaveDraft`;
  return axios.post(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const projectUpdate = ({ data }) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Project/Put`;
  return axios.put(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const projectStatusUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/Project/UpdateStatus`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const projectDelete = (data) => {
  return axios.put(`${process.env.REACT_APP_BASEURL}api/Project/Delete`, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllInterestedProjectByFreelancerId = (id, page, limit) => {
  const params = {};
  if (id) params.Id = id;
  if (page) params.page = page;
  if (limit) params.limit = limit;
  return axios.get(`${process.env.REACT_APP_BASEURL}/api/ProjectDetail/GetAllInterestedProjectByFreelancerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getProjectById = (Id) => {
  const params = {
    Id,
  };
  return axios.get(`${process.env.REACT_APP_BASEURL}api/Project/GetById`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};