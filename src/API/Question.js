import axios from "axios";

export const questionGetBySearch = (limit, pageNumber, keyword, tagId) => {
  let query = "?Limit=" + limit + "&page=" + pageNumber;
  if (keyword !== "" && keyword !== undefined && keyword !== null) {
    query += "&search=" + keyword;
  }

  if (tagId !== "" && tagId !== undefined && tagId !== null) {
    query += "&TagId=" + tagId;
  }

  return axios.get(`${process.env.REACT_APP_BASEURL}api/forum/Search` + query, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const trendingForums = () => {
  return axios.get(`${process.env.REACT_APP_BASEURL}api/Forum/GetTop`, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getForumbyId = (ForumId) => {
  let query = "?Id=" + ForumId;

  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Forum/GetById` + query,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const answersByForumId = (limit, pageNumber, forumId) => {
  let query = "?limit=" + limit + "&page=" + pageNumber;

  if (forumId !== "" && forumId !== undefined && forumId !== null) {
    query += "&Id=" + forumId;
  }

  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/forum/FormAnswerSearch` + query,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const questionPost = (data) => {
  return axios.post(`${process.env.REACT_APP_BASEURL}api/forum/Post`, data, {
    headers: {
      ["Content-Type"]: "application/json",
    },
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const answerToQuestionPost = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/ForumAnswer/Post`,
    data,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};
