import axios from "axios";

export const test = () => {
  return axios.get("https://reqres.in/api/users/2", {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};
