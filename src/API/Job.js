import axios from "axios";

export const jobPost = ({ data }) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Job/Post`;
  return axios.post(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const saveJobDraftPost = ({ data }) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Job/SaveDraft`;
  return axios.post(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const jobUpdate = ({ data }) => {
  var baseUrl = `${process.env.REACT_APP_BASEURL}api/Job/Put`;
  return axios.put(baseUrl, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const jobStatusUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/job/UpdateStatus`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const jobStatus = (type, userId, Id) => {
  const params = {
    type,
    userId,
    Id,
  };
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Detail/GetJobProjectDetail`,
    {
      params,
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 2,
      },
    }
  );
};

export const jobDelete = (data) => {
  return axios.put(`${process.env.REACT_APP_BASEURL}api/job/Delete`, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const GetAllInterestedJobByJobseekerId = (Id, page, limit) => {
  const params = {
    Id,
    page,
    limit,
  };
  return axios.put(`${process.env.REACT_APP_BASEURL}api/JobDetail/GetAllInterestedJobByJobseekerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getJobById = (Id) => {
  const params = {
    Id,
  };
  return axios.get(`${process.env.REACT_APP_BASEURL}api/Job/GetById`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};