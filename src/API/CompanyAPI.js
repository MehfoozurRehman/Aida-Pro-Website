import axios from "axios";

export const companySignUp = (data) => {
  return axios.post(`${process.env.REACT_APP_BASEURL}api/company/Post`, data, {
    headers: {
      ["Content-Type"]: "application/json",
    },
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const companyUpdate = (data) => {
  return axios.put(`${process.env.REACT_APP_BASEURL}/api/company/Put`, data, {
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const companyGetById = (id) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Company/GetById?Id=${id}`,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const companyGetAllJobProject = (
  companyProfileId,
  searchByjobSeeker,
  searchByFreelancer,
  pageLimit,
  pageNo
) => {
  const params = {
    companyProfileId: companyProfileId,
    limit: pageLimit,
    page: pageNo,
  };

  if (searchByjobSeeker !== undefined) params.searchBy = searchByjobSeeker;

  if (searchByFreelancer !== undefined) params.searchBy = searchByFreelancer;

  if (searchByjobSeeker !== undefined && searchByFreelancer !== undefined)
    params.searchBy = searchByFreelancer + "," + searchByjobSeeker;

  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Company/SearchPosting`,
    {
      params,
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 2,
      },
    }
  );
};

export const jobSekkerRecentJobsForCompany = ({
  page,
  limit,
  title,
  jobLocation,
  jobLocationLat,
  jobLocationLng,
  skillId,
  industryId,
  experienceLevel,
  jobTypeId,
  educationId,
  availabilityId,
  languageId,
  sortBy,
  searchBy,
  searchByFreelancer,
  salaryFrom,
  salaryTo,
  selectedRadius,
}) => {
  const params = {};
  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (title) params.title = title;
  if (jobLocation) params.jobLocation = jobLocation;
  if (skillId.length > 0) params.skillId = skillId.toString();
  if (industryId.length > 0) params.industryId = industryId.toString();
  if (experienceLevel)
    params.experienceLevel = experienceLevel
  if (jobTypeId.length > 0) params.jobTypeId = jobTypeId.toString();
  if (educationId.length > 0) params.educationId = educationId.toString();
  if (availabilityId.length > 0)
    params.availabilityId = availabilityId.toString();
  if (languageId.length > 0) params.languageId = languageId.toString();

  if (sortBy) params.sortBy = sortBy;
  if (searchBy === "Employment" && searchByFreelancer === "Freelancer") {
    params.searchBy = searchBy + "," + searchByFreelancer;
  } else {
    if (searchBy === "Employment") params.searchBy = searchBy;
    else params.searchBy = searchByFreelancer;
  }

  if (salaryFrom) params.salaryFrom = salaryFrom;
  if (salaryTo) params.salaryTo = salaryTo;

  if (selectedRadius) params.radius = selectedRadius;
  if (jobLocationLat) params.latitude = jobLocationLat;
  if (jobLocationLng) params.longitude = jobLocationLng;

  return axios.get(`${process.env.REACT_APP_BASEURL}api/JobSeeker/TestSearch`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const jobSekkerJobsForHomePage = (
  page,
  limit,
  title,
  jobLocation,
  searchBy,
  searchByFreelancer,
  range
) => {
  const params = {};

  if ((searchBy === "Employment") & (searchByFreelancer === "Freelancer")) {
    params.searchBy = searchBy + "," + searchByFreelancer;
  } else {
    if (searchBy === "Employment") params.searchBy = searchBy;
    else params.searchBy = searchByFreelancer;
  }

  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (title) params.title = title;
  if (jobLocation) params.jobLocation = jobLocation;
  if (range) params.range = range;
  // if (searchBy) params.searchBy = searchBy;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/JobSeeker/TestSearch`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllJobsProjectsCoffeCorner = (
  companyProfileId,
  jobs,
  projects,
  pageLimit,
  pageNo
) => {
  const params = {
    companyProfileId: companyProfileId,
    limit: pageLimit,
    page: pageNo,
  };
  if (jobs !== null) params.searchBy = jobs;

  if (projects !== null) params.searchBy = projects;

  if (jobs !== null && projects !== null)
    params.searchBy = jobs + "," + projects;

  console.log("Job Projects trending: ===> ", `${process.env.REACT_APP_BASEURL}api/Company/SearchPosting`, params)

  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/Company/SearchPosting`,
    {
      params,
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 2,
      },
    }
  );
};
