import axios from "axios";

export const getBlog = (limit, pageNumber) => {
  let query = "?Limit=" + limit + "&page=" + pageNumber;

  return axios.get(
    process.env.REACT_APP_BASEURL + "/api/Blogs/GetAll" + query,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const getBlogById = (blogId) => {
  return axios.get(
    process.env.REACT_APP_BASEURL + `/api/Blogs/GetById?Id=${blogId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};
