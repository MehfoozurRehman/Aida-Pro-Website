import axios from "axios";

export const freelancerSignUp = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/freelancer/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerProjectPortfolio = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/FreelancerPortfolio/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerProjectPortfolioUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerPortfolio/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerProjectPortfolioDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerPortfolio/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerProjectPortfoliosList = (freelanceId) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/FreelancerPortfolio/Search?limit=5&page=1&FreelancerId=${freelanceId}`,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerPersonalDetailsUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/freelancer/PersonalDetail`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerGetById = (id) => {
  return axios.get(
    `${process.env.REACT_APP_BASEURL}api/freelancer/GetById?Id=${id}`,
    {
      headers: {
        ["Content-Type"]: "application/json",
      },
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelacerProfessionalDetailsUpdate = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/freelancer/UpdateMainProfessionalDetail`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

// export const freelancerSkillUpdate = (data) => {
//   return axios.post(
//     `${process.env.REACT_APP_BASEURL}api/FreelancerSkill/Post`,
//     data,
//     {
//       ["axios-retry"]: {
//         retries: 5,
//       },
//     }
//   );
// };

export const freelancerSkillUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/FreelancerSkill/MultiPost`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerSkillDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerSkill/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerQualificationUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/FreelancerQualification/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerQualificationEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerQualification/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerQualificationDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerQualification/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerCertificateUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/FreelancerCertificate/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerCertificateEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerCertificate/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerCertificateDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerCertificate/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerExperienceUpdate = (data) => {
  return axios.post(
    `${process.env.REACT_APP_BASEURL}api/FreelancerExperience/Post`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerExperienceEdit = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerExperience/Put`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerExperienceDelete = (data) => {
  return axios.put(
    `${process.env.REACT_APP_BASEURL}api/FreelancerExperience/Delete`,
    data,
    {
      ["axios-retry"]: {
        retries: 5,
      },
    }
  );
};

export const freelancerProjects = (
  page,
  limit,
  title,
  skill,
  jobLocation,
  jobLocationLat,
  jobLocationLng,
  dateOfPosting,
  currentDate,
  selectedRadius
) => {
  const params = {};
  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (title) params.title = title;
  if (dateOfPosting) params.dateOfPosting = dateOfPosting;
  if (skill.length > 0) params.skillId = skill.toString();
  if (jobLocation) params.location = jobLocation;
  if (currentDate) params.startDate = currentDate;
  if (selectedRadius) params.radius = selectedRadius;
  if (jobLocationLat) params.latitude = jobLocationLat;
  if (jobLocationLng) params.longitude = jobLocationLng;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/project/Search`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const freelancerProjectsForHomePage = (page, limit, title) => {
  const params = {};
  if (page) params.page = page;
  if (limit) params.limit = limit;
  if (title) params.title = title;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/project/Search`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllInterestedProjectByFreelancerId = (id, page, limit) => {
  const params = {};
  if (id) params.Id = id;
  if (page) params.page = page;
  if (limit) params.limit = limit;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/ProjectDetail/GetAllInterestedProjectByFreelancerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};

export const getAllAppliedProjectByFreelancerId = (id, page, limit) => {
  const params = {};
  if (id) params.Id = id;
  if (page) params.page = page;
  if (limit) params.limit = limit;
  return axios.get(`${process.env.REACT_APP_BASEURL}api/ProjectDetail/GetAllIsAppliedProjectByFreelancerId`, {
    params,
    ["axios-retry"]: {
      retries: 5,
    },
  });
};