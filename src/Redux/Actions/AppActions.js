import { companyGetById } from "../../API/CompanyAPI";
import { getUserDetailById } from "../../API/Users";
import { jobSekkerGetById } from "../../API/EmploymentAPI";
import { freelancerGetById } from "../../API/FreelancerApi";

import {
  GET_USER_BY_ID,
  CURRENT_USER,
  SET_APP_LOADING,
  GET_COMPANY_BY_ID,
  GET_JOBSEKKER_BY_ID,
  GET_FREELANCER_BY_ID,
  FIREBASE_LOGGEDIN_USER,
  SET_MAIN_STREAM,
  ADD_PARTICIPANT,
  SET_USER,
  REMOVE_PARTICIPANT,
  UPDATE_USER,
  UPDATE_PARTICIPANT,
  GET_NOTIFICATION,
} from "../constants";

export const getUser = (userId) => (dispatch) => {
  getUserDetailById(userId).then(({ data }) => {
    dispatch({
      type: GET_USER_BY_ID,
      payload: {
        user: data.result,
      },
    });
    dispatch({
      type: SET_APP_LOADING,
      payload: { isLoading: false },
    });
  });
};
export const setLoadingFalse = () => (dispatch) => {
  dispatch({
    type: SET_APP_LOADING,
    payload: { isLoading: false },
  });
};

export const setLoadingTrue = () => (dispatch) => {
  dispatch({
    type: SET_APP_LOADING,
    payload: { isLoading: true },
  });
};

export const setCurrentUser = (user) => (dispatch) => {
  dispatch({
    type: CURRENT_USER,
    payload: { user },
  });
};

export const setCurrentFirebaseUser = (user) => (dispatch) => {
  dispatch({
    type: FIREBASE_LOGGEDIN_USER,
    payload: { user },
  });
};

export const getCompanyById = (compId) => (dispatch) => {
  companyGetById(compId).then(({ data }) => {
    let country,
      billingCountry,
      city,
      billingCity,
      zipCode,
      billingZipCode,
      addressDetail,
      billingAddressDetail,
      phone,
      email,
      lat,
      lng,
      billingLat,
      billingLng,
      zoom,
      CompanyProfileId,
      primaryId,
      Description,
      billingId,
      companyBranch;
    data &&
      data.result &&
      data.result.CompanyAddresses &&
      data.result.CompanyAddresses.map((e) => {
        if (e.AddressTypeLookupDetailId === 10) {
          if (e.Id) {
            primaryId = e.Id;
          }
          if (e.Country !== null && e.Country !== undefined) {
            country = e.Country;
          }
          if (e.City) {
            city = {
              label: e.City.Title,
              value: e.City.Id,
            };
          }
          if (e.ZipCode) {
            zipCode = e.ZipCode;
          }
          if (e.AddressDetail) {
            addressDetail = e.AddressDetail;
          }
          if (e.PhoneNo) {
            phone = e.PhoneNo;
          }
          if (e.Email) {
            email = e.Email;
          }
          if (e.CompanyProfileId) {
            CompanyProfileId = e.CompanyProfileId;
          }
          billingLat = e.Latitude;
          billingLng = e.Longitude;
          if (e.Zoom) {
            zoom = e.Zoom;
          }
        }
        if (e.AddressTypeLookupDetailId === 11) {
          if (e.Id) {
            billingId = e.Id;
          }
          if (e.Country) {
            billingCountry = {
              label: e.Country.Title,
              value: e.Country.Id,
            };
          }
          if (e.City) {
            billingCity = {
              label: e.City.Title,
              value: e.City.Id,
            };
          }
          if (e.ZipCode) {
            billingZipCode = e.ZipCode;
          }
          if (e.AddressDetail) {
            billingAddressDetail = e.AddressDetail;
          }
        }

        if (e.AddressTypeLookupDetailId === null) {
          if (e.Country !== null && e.Country !== undefined) {
            country = {
              label: e.Country.Title,
              value: e.Country.Id,
            };
          }
          if (e.Email) {
            email = e.Email;
          }
          if (e.CompanyProfileId) {
            CompanyProfileId = e.CompanyProfileId;
          }
          if (e.Id) {
            primaryId = e.Id;
          }
        }
      });
    if (data.result.Industry) {
      companyBranch = {
        label: data.result.Industry.Title,
        value: data.result.Industry.Id,
      };
    }
    if (data.result.Description) {
      Description = data.result.Description;
    }
    lat = data.result.Latitude;
    lng = data.result.Longitude;
    let customData = {
      Id: data.result.Id,
      companyName: data.result.CompanyName,
      companyContantPerson: data.result.ContactPerson,
      companyNoOfEmp: data.result.NoOfEmployees,
      companyAddressCountry: country,
      companyCellNo: phone,
      companyEmailAddress: email,
      CompanyProfileId: CompanyProfileId,
      companyAddressZipCode: zipCode,
      companyAddress: addressDetail,
      companyAddressCity: city,
      lat: lat,
      lng: lng,
      billingLat: billingLat,
      billingLng: billingLng,
      zoom: zoom,
      companyBillingAddressCountry: billingCountry,
      companyBillingAddressCity: billingCity,
      companyBillingAddressZipCode: billingZipCode,
      companyBillingAddress: billingAddressDetail,
      Description,
      primaryId,
      billingId,
      companyBranch,
      LogoUrl: data.result.LogoUrl,
    };
    dispatch({
      type: GET_COMPANY_BY_ID,
      payload: {
        company: customData,
      },
    });
  });
};

export const getJobSekkerById = (jobsekkerId) => (dispatch) => {
  jobSekkerGetById(jobsekkerId).then(({ data }) => {
    let lang = [];
    if (data.result.JobSeekerLanguages) {
      data.result.JobSeekerLanguages.map(
        (e) =>
          e.LanguageLookupDetail &&
          lang.push({
            Id: e.Id,
            value: e.LanguageLookupDetail.Id,
            label: e.LanguageLookupDetail.Title,
          })
      );
    }

    let gender;
    if (data.result.GenderLookupDetail) {
      gender = {
        label: data.result.GenderLookupDetail.Title,
        value: data.result.GenderLookupDetail.Id,
      };
    }

    let country;
    let city;
    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      country = data.result.JobSeekerAddresses[0].Country;

      if (data.result.JobSeekerAddresses[0].City) {
        city = {
          label: data.result.JobSeekerAddresses[0].City.Title,
          value: data.result.JobSeekerAddresses[0].City.Id,
        };
      }
    }

    let qualification;
    if (data.result.JobSeekerQualifications) {
      qualification = data.result.JobSeekerQualifications;
    }

    let certificates;
    if (data.result.JobSeekerCertificates) {
      certificates = data.result.JobSeekerCertificates;
    }

    let skills;
    if (data.result.JobSeekerSkills) {
      skills = data.result.JobSeekerSkills;
    }

    let phoneNo,
      emailAddress,
      primaryZipCode,
      Latitude,
      Longitude,
      Zoom,
      JobSeekerAddressesId,
      JobSeekerCV,
      JobSeekerPrimaryAddress;

    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      phoneNo = data.result.JobSeekerAddresses[0].PhoneNo;
    }
    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      emailAddress = data.result.JobSeekerAddresses[0].Email;
    }

    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      primaryZipCode = data.result.JobSeekerAddresses[0].ZipCode;
    }
    if (data.result.Latitude) {
      Latitude = data.result.Latitude;
    }

    if (data.result.Longitude) {
      Longitude = data.result.Longitude;
    }

    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      Zoom = data.result.JobSeekerAddresses[0].Zoom;
    }

    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      JobSeekerAddressesId = data.result.JobSeekerAddresses[0].Id;
    }

    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      JobSeekerPrimaryAddress =
        data.result.JobSeekerAddresses[0].JobSeekerAddress;
    }
    let AddressDetail;
    if (
      data.result.JobSeekerAddresses != null &&
      data.result.JobSeekerAddresses.length > 0
    ) {
      AddressDetail = data.result.JobSeekerAddresses[0].AddressDetail;
    }

    if (data.result.JobSeekerCV) {
      JobSeekerCV = data.result.JobSeekerCV;
    }

    let LinkedInProfile;
    if (data.result.LinkedInProfile) {
      LinkedInProfile = data.result.LinkedInProfile;
    }
    let GoogleProfile;
    if (data.result.GoogleProfile) {
      GoogleProfile = data.result.GoogleProfile;
    }
    let FacebookProfile;
    if (data.result.FacebookProfile) {
      FacebookProfile = data.result.FacebookProfile;
    }
    let Availbility;
    if (data.result.AvailabilityLookupDetail) {
      Availbility = {
        label:
          data.result.AvailabilityLookupDetail &&
          data.result.AvailabilityLookupDetail.Title,
        value:
          data.result.AvailabilityLookupDetail &&
          data.result.AvailabilityLookupDetail.Id,
      };
    }

    let personalDetailsJobSeekerImage;
    if (data.result.ProfilePicture) {
      personalDetailsJobSeekerImage = data.result.ProfilePicture;
    }

    let JobTypeLookupDetail;
    if (data.result.JobTypeLookupDetail != null) {
      data.result.JobTypeLookupDetail["value"] =
        data.result.JobTypeLookupDetail.Id;
      data.result.JobTypeLookupDetail["label"] =
        data.result.JobTypeLookupDetail.Title;
      JobTypeLookupDetail = data.result.JobTypeLookupDetail;
    }

    let jobSeekerPortfolios = [];
    jobSeekerPortfolios = data.result.JobSeekerPortfolios;

    let customData = {
      Id: data.result.Id,
      firstName: data.result.FirstName,
      lastName: data.result.LastName,
      emailAddress: emailAddress,
      phoneNo: phoneNo,
      gender: gender,
      age: data.result.Age,
      primaryZipCode: primaryZipCode,
      language: lang,
      primaryCountry: country,
      primaryCity: city,
      lat: Latitude,
      lng: Longitude,
      zoom: Zoom,
      JobSeekerAddressesId: JobSeekerAddressesId,
      JobSeekerCV: JobSeekerCV,
      primaryAddress: AddressDetail,
      JobTypeLookupDetail: JobTypeLookupDetail,
      qualification: qualification,
      skills: skills,
      linkedInProfile: LinkedInProfile,
      facebookProfile: FacebookProfile,
      googleProfile: GoogleProfile,
      workRemote: data.result.WorkRemote,
      workOffice: data.result.WorkOffice,
      canRelocate: data.result.CanRelocate,
      experience: data.result.JobSeekerExperiences,
      expectedSalary: data.result.ExpectedSalary,
      certificates,
      Availbility,
      personalDetailsJobSeekerImage,
      portfolios: jobSeekerPortfolios,
    };
    dispatch({
      type: GET_JOBSEKKER_BY_ID,
      payload: {
        company: customData,
      },
    });
  });
};

export const getFreelancerById = (jobsekkerId) => (dispatch) => {
  freelancerGetById(jobsekkerId).then(({ data }) => {
    let lang = [];
    if (data.result.FreelancerLanguages.length) {
      data.result.FreelancerLanguages.map(
        (e) =>
          e.LanguageLookupDetail &&
          lang.push({
            Id: e.Id,
            value: e.LanguageLookupDetail.Id,
            label: e.LanguageLookupDetail.Title,
          })
      );
    }
    let gender;
    if (data.result.GenderLookupDetail) {
      gender = {
        label: data.result.GenderLookupDetail.Title,
        value: data.result.GenderLookupDetail.Id,
      };
    }

    let country;
    if (
      data.result.FreelancerAddresses.length &&
      data.result.FreelancerAddresses[0].Country
    ) {
      country = data.result.FreelancerAddresses[0].Country;
    }

    let city;
    if (
      data.result.FreelancerAddresses.length &&
      data.result.FreelancerAddresses[0].City
    ) {
      city = {
        label: data.result.FreelancerAddresses[0].City.Title,
        value: data.result.FreelancerAddresses[0].City.Id,
      };
    }

    let qualification;
    if (data.result.FreelancerQualifications) {
      qualification = data.result.FreelancerQualifications;
    }

    let certificates;
    if (data.result.FreelancerCertificates) {
      certificates = data.result.FreelancerCertificates;
    }

    let skills;
    if (data.result.FreelancerSkills) {
      skills = data.result.FreelancerSkills;
    }

    let phoneNo,
      emailAddress,
      primaryZipCode,
      Latitude,
      Longitude,
      Zoom,
      FreelancerCv,
      FreelancerAddressesAddress,
      FreelancerAddressesId;
    if (data.result.FreelancerAddresses.length) {
      phoneNo = data.result.FreelancerAddresses[0].PhoneNo;
    }

    if (data.result.FreelancerCv) {
      FreelancerCv = data.result.FreelancerCv;
    }

    if (data.result.Users.length) {
      emailAddress = data.result.Users[0].UserName;
    }

    if (data.result.FreelancerAddresses.length) {
      primaryZipCode = data.result.FreelancerAddresses[0].ZipCode;
    }

    if (data.result.Latitude) {
      Latitude = data.result.Latitude;
    }

    if (data.result.Longitude) {
      Longitude = data.result.Longitude;
    }

    if (data.result.FreelancerAddresses.length) {
      Zoom = data.result.FreelancerAddresses[0].Zoom;
    }

    if (data.result.FreelancerAddresses.length) {
      FreelancerAddressesId = data.result.FreelancerAddresses[0].Id;
    }
    if (data.result.FreelancerAddresses.length) {
      FreelancerAddressesId = data.result.FreelancerAddresses[0].Id;
    }

    if (data.result.FreelancerAddresses.length) {
      FreelancerAddressesAddress =
        data.result.FreelancerAddresses[0].FreelancerAddres;
    }

    let LinkedInProfile;
    if (data.result.LinkedInProfile) {
      LinkedInProfile = data.result.LinkedInProfile;
    }
    let GoogleProfile;
    if (data.result.GoogleProfile) {
      GoogleProfile = data.result.GoogleProfile;
    }
    let FacebookProfile;
    if (data.result.FacebookProfile) {
      FacebookProfile = data.result.FacebookProfile;
    }

    let AddressDetail;
    if (data.result.FreelancerAddresses.length) {
      AddressDetail = data.result.FreelancerAddresses[0].AddressDetail;
    }
    let Availbility;
    if (data.result.AvailabilityLookupDetail) {
      Availbility = {
        label:
          data.result.AvailabilityLookupDetail &&
          data.result.AvailabilityLookupDetail.Title,
        value:
          data.result.AvailabilityLookupDetail &&
          data.result.AvailabilityLookupDetail.Id,
      };
    }

    let personalDetailsFreelancerImage;
    if (data.result.ProfilePicture) {
      personalDetailsFreelancerImage = data.result.ProfilePicture;
    }

    let JobTypeLookupDetail;
    if (data.result.JobTypeLookupDetail != null) {
      data.result.JobTypeLookupDetail["value"] =
        data.result.JobTypeLookupDetail.Id;
      data.result.JobTypeLookupDetail["label"] =
        data.result.JobTypeLookupDetail.Title;
      JobTypeLookupDetail = data.result.JobTypeLookupDetail;
    }

    let freelancerPortfolios = [];
    freelancerPortfolios = data.result.FreelancerPortfolios;

    let customData = {
      Id: data.result.Id,
      firstName: data.result.FirstName,
      lastName: data.result.LastName,
      emailAddress: emailAddress,
      phoneNo: phoneNo,
      gender: gender,
      age: data.result.Age,
      primaryZipCode: primaryZipCode,
      language: lang,
      primaryCountry: country,
      primaryCity: city,
      lat: Latitude,
      lng: Longitude,
      zoom: Zoom,
      FreelancerAddressesId: FreelancerAddressesId,
      JobSeekerCV: FreelancerCv,
      JobTypeLookupDetail: JobTypeLookupDetail,
      primaryAddress: AddressDetail,
      qualification: qualification,
      skills: skills,
      linkedInProfile: LinkedInProfile,
      facebookProfile: FacebookProfile,
      googleProfile: GoogleProfile,
      workRemote: data.result.WorkRemote,
      workOffice: data.result.WorkOffice,
      canRelocate: data.result.CanRelocate,
      experience: data.result.FreelancerExperiences,
      expectedSalary: data.result.HourlyRate,
      certificates: data.result.FreelancerCertificates,
      Availbility,
      personalDetailsFreelancerImage,
      portfolios: freelancerPortfolios,
    };
    dispatch({
      type: GET_FREELANCER_BY_ID,
      payload: {
        company: customData,
      },
    });
  });
};

export const setMainStream = (stream) => {
  return {
    type: SET_MAIN_STREAM,
    payload: {
      mainStream: stream,
    },
  };
};

export const setUser = (user) => {
  return {
    type: SET_USER,
    payload: {
      currentUser: user,
    },
  };
};

export const addParticipant = (user) => {
  return {
    type: ADD_PARTICIPANT,
    payload: {
      newUser: user,
    },
  };
};

export const updateUser = (user) => {
  return {
    type: UPDATE_USER,
    payload: {
      currentUser: user,
    },
  };
};

export const updateParticipant = (user) => {
  return {
    type: UPDATE_PARTICIPANT,
    payload: {
      newUser: user,
    },
  };
};

export const removeParticipant = (userId) => {
  return {
    type: REMOVE_PARTICIPANT,
    payload: {
      id: userId,
    },
  };
};

export const updateNotification = (notification) => {
  return {
    type: GET_NOTIFICATION,
    payload: {
      notification: notification,
    },
  };
};
