import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { USER_LOGOUT } from "./constants";
import UserReducer from "./Reducers/UserReducer";
import AppLoadingReducer from "./Reducers/AppLoadingReducer";
import CompanyReducer from "./Reducers/CompanyReducer";
import JobSekeerReducer from "./Reducers/JobSekeerReducer";
import FreelancerReducer from "./Reducers/FreelancerReducer";
import VideoCallReducer from "./Reducers/VideoCallReducer";

const appReducer = combineReducers({
  user: UserReducer,
  appLoading: AppLoadingReducer,
  company: CompanyReducer,
  jobsekker: JobSekeerReducer,
  freelancer: FreelancerReducer,
  video: VideoCallReducer,
});

export const rootReducer = (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = undefined;
  }
  return appReducer(state, action);
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);
