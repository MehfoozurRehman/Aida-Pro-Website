import { GET_FREELANCER_BY_ID } from "../constants";

const INITIAL_STATE = {
  freelancer: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_FREELANCER_BY_ID:
      return { ...state, freelancer: action.payload.company };

    default:
      return state;
  }
};
