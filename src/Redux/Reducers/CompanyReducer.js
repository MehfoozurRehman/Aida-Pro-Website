import { GET_COMPANY_BY_ID } from "../constants";

const INITIAL_STATE = {
  company: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_COMPANY_BY_ID:
      return { ...state, company: action.payload.company };

    default:
      return state;
  }
};
