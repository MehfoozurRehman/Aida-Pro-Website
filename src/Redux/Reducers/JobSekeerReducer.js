import { GET_JOBSEKKER_BY_ID } from "../constants";

const INITIAL_STATE = {
  jobsekker: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_JOBSEKKER_BY_ID:
      return { ...state, jobsekker: action.payload.company };

    default:
      return state;
  }
};
