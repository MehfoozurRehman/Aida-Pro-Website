import {
  TEST_USER,
  GET_TEST_USER,
  CURRENT_USER,
  GET_USER_BY_ID,
  FIREBASE_LOGGEDIN_USER,
} from "../constants";

const INITIAL_STATE = {
  test: true,
  user: {},
  firebaseUser: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TEST_USER:
      return { ...state, test: action.payload.test };
    case GET_TEST_USER:
      return { ...state, user: action.payload.user };
    case CURRENT_USER:
      return { ...state, user: action.payload.user };
    case GET_USER_BY_ID:
      return { ...state, user: action.payload.user };
    case FIREBASE_LOGGEDIN_USER:
      return { ...state, firebaseUser: action.payload.user };

    default:
      return state;
  }
};
