import { GET_NOTIFICATION } from "../constants";

const INITIAL_STATE = {
    notification: {},
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFICATION:
            return { ...state, notification: action.payload.notification };
        default:
            return state;
    }
};