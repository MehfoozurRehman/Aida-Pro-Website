import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDYso5KV4oK0diHno1Q5oSKDA8dWknOKaM",
  authDomain: "aida-dev-chat.firebaseapp.com",
  databaseURL: "https://aida-dev-chat-default-rtdb.firebaseio.com",
  projectId: "aida-dev-chat",
  storageBucket: "aida-dev-chat.appspot.com",
  messagingSenderId: "882477990543",
  appId: "1:882477990543:web:bdc3912d7afee1bbf366fa",
  measurementId: "G-0T59ECX61E",
};

if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);
else firebase.app();

// export const db = firebase;

export var userName = "";
export const userNameValue = (value) => {
  userName = value;
};

var firepadRef = firebase.database();
// const urlparams = new URLSearchParams(window.location.search);
//
// const roomId = urlparams.get("id");
// if (roomId) {
//
//     firepadRef = firepadRef.child(roomId);
// } else {
//
//     firepadRef = firepadRef.push();
//     window.history.replaceState(null, "Meet", "?id=" + firepadRef.key);
// }

export default firepadRef;
