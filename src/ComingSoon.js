import React, { useEffect } from "react";
import { aidaLogo, coming__soon__img } from "Assets";
import Img from "react-cool-img";

export default function ComingSoon() {
  useEffect(() => {
    document.body.style.overflow = "hidden";
  }, []);
  return (
    <div className="coming__soon">
      <div className="coming__soon__header">
        <Img
          loading="lazy"
          src={aidaLogo}
          alt="logo"
          className="coming__soon__header__img"
        />
      </div>
      <div className="coming__soon__content">
        <Img
          loading="lazy"
          src={coming__soon__img}
          alt="coming__soon"
          className="coming__soon__img"
        />
        <div className="coming__soon__text">
          Something big is coming your way. Drop by again from 1 February
          onwards.
        </div>
      </div>
    </div>
  );
}
