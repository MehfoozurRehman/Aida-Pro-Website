import { navigate } from "@reach/router";
import React, { useEffect, useState, useContext } from "react";
import { aidaLogo, germany, unitedstates } from "Assets";
import { PopupContext } from "Routes/AppRouter";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import HeaderNav from "./HeaderNav";
import firebase from "firebase/app";
import Img from "react-cool-img";
import {
  getAllNotificationByUserId,
  setAllNotificationsIsReadTrue,
} from "API/Api";

export default function Header({
  isWelcomeScreen,
  isOnDashboard,
  isOnSignUp,
  isOnHomeCompany,
  dashboard,
}) {
  const { setIsLoginOpen } = useContext(PopupContext);
  const { notification } = useSelector((state) => state.notification);
  const isOn = window.localStorage.getItem("isOn");
  const [isScrolling, setIsScrolling] = useState(false);
  const [isNotificationPanelOpen, setIsNotificationPanelOpen] = useState(false);
  const [isMessagePanelOpen, setIsMessagePanelOpen] = useState(false);
  const [isLanguagePanelOpen, setIsLanguagePanelOpen] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(true);
  const [isUserLogedIn, setIsUserLogedIn] = useState(false);
  const [languageSelected, setLanguageSelected] = useState("en");
  const [userImage, setUserImage] = useState(null);
  const user = useContext(UserContext);
  let [messageNotificationData, setMessageNotificationData] = useState(null);
  const [unreadMessagesLength, setUnreadMessagesLength] = useState(null);
  let [notificationData, setNotificationData] = useState(null);
  const [unreadNotificationLength, setUnreadNotificationLength] =
    useState(null);

  useEffect(() => {
    if (Object.keys(user).length > 0) {
      if (user.Freelancer != null) {
        setUserImage(user.Freelancer.ProfilePicture);
      } else if (user.JobSeeker != null) {
        setUserImage(user.JobSeeker.ProfilePicture);
      } else if (user.CompanyProfile != null) {
        setUserImage(user.CompanyProfile.LogoUrl);
      }
    }
  });

  useEffect(() => {
    if (window.innerWidth > 800) {
      setIsMenuOpen(true);
    } else {
      setIsMenuOpen(false);
    }
    window.addEventListener("resize", () => {
      if (window.innerWidth > 800) {
        setIsMenuOpen(true);
      } else {
        setIsMenuOpen(false);
      }
    });
    if (Object.keys(user).length != 0) {
      setIsUserLogedIn(true);
      fetchNotifications();
      // getUserNotification(user.Id);
    } else {
      setIsUserLogedIn(false);
    }
  }, []);

  useEffect(() => {
    if (Object.keys(user).length != 0) {
      getUserNotification(user.Id);
    }
  }, [notification]);

  const getUserNotification = (Id) => {
    getAllNotificationByUserId(Id)
      .then((data) => {
        if (data.data.length > 0) {
          setNotificationData((notificationData = data.data));
          let newArray = [];
          for (let index = 0; index < notificationData.length; index++) {
            const element = notificationData[index];
            if (element.Isread == false) {
              newArray.push(element);
              setUnreadNotificationLength(newArray);
            }
          }
        }
      })
      .catch((err) => {
        // console.log("notification err", err);
      });
  };

  const setNotificationIsReadTrue = () => {
    setAllNotificationsIsReadTrue(user.Id)
      .then((response) => {
        if (response.success) {
          let newArray = notificationData;
          for (let index = 0; index < newArray.length; index++) {
            const element = newArray[index];
            element.Isread = true;
          }
          setNotificationData(newArray);
        }
      })
      .catch((err) => {
        // console.log("err", err);
      });
  };

  const fetchNotifications = () => {
    const messageListener = firebase
      .firestore()
      .collection("Notification")
      .doc(
        user.Freelancer != null
          ? `Freelancer_${user.FreelancerId}`
          : user.JobSeeker != null
            ? `Jobseeker_${user.JobSeekerId}`
            : user.CompanyProfile != null
              ? `Company_${user.CompanyId}`
              : null
      )
      .collection("Message")
      .orderBy("createdAt", "asc")
      .onSnapshot((querySnapShot) => {
        const firebaseMessages = querySnapShot.docs.map((doc) => {
          const firebaseData = doc.data();
          const data = {
            _id: doc.id,
            ...firebaseData,
          };
          return data;
        });
        setMessageNotificationData(
          (messageNotificationData = firebaseMessages)
        );
        let newArray = [];
        for (let index = 0; index < messageNotificationData.length; index++) {
          const element = messageNotificationData[index];
          if (element.isRead == false) {
            newArray.push(element);
            setUnreadMessagesLength(newArray);
          }
        }
      });
    return () => messageListener();
  };

  const changeBackgrond = () => {
    if (window.scrollY > 0) {
      setIsScrolling(true);
    } else {
      setIsScrolling(false);
    }
  };
  useEffect(() => {
    changeBackgrond();
  }, []);

  const updateNotificationDataAcceptReject = (value, isReject) => {
    let newArray = [];
    for (let index = 0; index < notificationData.length; index++) {
      const element = notificationData[index];
      if (element.Id == value.Id) {
        element.IsAccepted = !isReject ? true : false;
      }
      newArray.push(element);
    }
    setNotificationData(newArray);
  };
  const onClickOutsideLanguage = () => {
    setIsLanguagePanelOpen(false);
    console.log("onClickOutsideLanguage");
  };
  const onClickOutsideMenu = () => {
    if (window.innerWidth < 800) {
      setIsMenuOpen(false);
    }
  };
  const onClickOutsideMessages = () => {
    setIsMessagePanelOpen(false);
    console.log("onClickOutsideMessages");
  };
  const onClickOutsideNotifications = () => {
    setIsNotificationPanelOpen(false);
    console.log("onClickOutsideNotifications");
  };
  window.addEventListener("scroll", changeBackgrond);
  return (
    <div className={isScrolling ? "header header__active" : "header"}>
      <div className="header__wrapper">
        <div
          onClick={() => {
            window.location.href =
              dashboard && isUserLogedIn ? `/home-${dashboard}` : "/";
          }}
          className="header__logo animate__animated animate__fadeInUp"
          style={{ cursor: "pointer" }}
        >
          <Img loading="lazy" src={aidaLogo} alt="aidaLogo" />
        </div>
        {isWelcomeScreen ? (
          <button
            className="animate__animated animate__fadeInRight header__nav__btn btn__primary"
            onClick={() => {
              setIsLoginOpen(true);
            }}
            title="sign in"
          >
            Sign In
          </button>
        ) : null}
        {isWelcomeScreen ? null : (
          <HeaderNav
            isMenuOpen={isMenuOpen}
            setIsMenuOpen={setIsMenuOpen}
            setIsLanguagePanelOpen={setIsLanguagePanelOpen}
            setIsMessagePanelOpen={setIsMessagePanelOpen}
            setIsNotificationPanelOpen={setIsNotificationPanelOpen}
            onClickOutsideLanguage={onClickOutsideLanguage}
            onClickOutsideMenu={onClickOutsideMenu}
            onClickOutsideNotifications={onClickOutsideNotifications}
            onClickOutsideMessages={onClickOutsideMessages}
            isOnDashboard={isOnDashboard}
            isUserLogedIn={isUserLogedIn}
            isMessagePanelOpen={isMessagePanelOpen}
            setUnreadMessagesLength={setUnreadMessagesLength}
            isOn={isOn}
            unreadMessagesLength={unreadMessagesLength}
            isNotificationPanelOpen={isNotificationPanelOpen}
            setUnreadNotificationLength={setUnreadNotificationLength}
            setNotificationIsReadTrue={setNotificationIsReadTrue}
            unreadNotificationLength={unreadNotificationLength}
            notificationData={notificationData}
            updateNotificationDataAcceptReject={
              updateNotificationDataAcceptReject
            }
            userImage={userImage}
            Date={Date}
            navigate={navigate}
            setIsLoginOpen={setIsLoginOpen}
            isOnSignUp={isOnSignUp}
            isOnHomeCompany={isOnHomeCompany}
            languageSelected={languageSelected}
            unitedstates={unitedstates}
            germany={germany}
            isLanguagePanelOpen={isLanguagePanelOpen}
            setLanguageSelected={setLanguageSelected}
            messageNotificationData={messageNotificationData}
          />
        )}
      </div>
    </div>
  );
}
