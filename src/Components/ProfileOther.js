import React from "react";
import { InputBox } from "Components";

export default function ProfileOther({
  oldPasswordError,
  oldPasswordErrorMessage,
  onOldPasswordTextChangeListener,
  newPasswordError,
  newPasswordErrorMessage,
  onPasswordTextChangeListener,
  confrimPasswordError,
  confirmPasswordErrorMessage,
  onConfirmPasswordTextChangeListener,
  onChangePassword,
}) {
  return (
    <>
      <div className="homepage__container__password__wrapper">
        <div className="homepage__container__password">
          <div
            style={{
              width: "50%",
            }}
            className="homepage__container__password__left"
          >
            <InputBox // variant="simple"
              type="password"
              error={oldPasswordError}
              errorMessage={oldPasswordErrorMessage}
              secureKey={true}
              placeholder="Previous Password"
              onChange={onOldPasswordTextChangeListener}
              name="oldPassword"
              svg={
                <svg
                  id="padlock"
                  xmlns="http://www.w3.org/2000/svg"
                  width="11.192"
                  height="14.922"
                  viewBox="0 0 11.192 14.922"
                >
                  <path
                    id="Path_293"
                    data-name="Path 293"
                    d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                    transform="translate(-3 -3.404)"
                    fill="#374957"
                  />
                  <path
                    id="Path_294"
                    data-name="Path 294"
                    d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                    transform="translate(-4.135)"
                    fill="#374957"
                  />
                  <path
                    id="Path_295"
                    data-name="Path 295"
                    d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                    transform="translate(-5.648 -4.917)"
                    fill="#374957"
                  />
                  <path
                    id="Path_296"
                    data-name="Path 296"
                    d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                    transform="translate(-6.12 -5.957)"
                    fill="#374957"
                  />
                </svg>
              }
            />
            <InputBox // variant="simple"
              type="password"
              error={newPasswordError}
              errorMessage={newPasswordErrorMessage}
              placeholder="New Password"
              secureKey={true}
              onChange={onPasswordTextChangeListener}
              name="newPassword"
              svg={
                <svg
                  id="padlock"
                  xmlns="http://www.w3.org/2000/svg"
                  width="11.192"
                  height="14.922"
                  viewBox="0 0 11.192 14.922"
                >
                  <path
                    id="Path_293"
                    data-name="Path 293"
                    d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                    transform="translate(-3 -3.404)"
                    fill="#374957"
                  />
                  <path
                    id="Path_294"
                    data-name="Path 294"
                    d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                    transform="translate(-4.135)"
                    fill="#374957"
                  />
                  <path
                    id="Path_295"
                    data-name="Path 295"
                    d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                    transform="translate(-5.648 -4.917)"
                    fill="#374957"
                  />
                  <path
                    id="Path_296"
                    data-name="Path 296"
                    d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                    transform="translate(-6.12 -5.957)"
                    fill="#374957"
                  />
                </svg>
              }
            />
            <InputBox // variant="simple"
              type="password"
              error={confrimPasswordError}
              errorMessage={confirmPasswordErrorMessage}
              placeholder="Confirm Password"
              secureKey={true}
              onChange={(event) => {
                onConfirmPasswordTextChangeListener(event.currentTarget.value);
              }}
              name="confirmPassword"
              svg={
                <svg
                  id="padlock"
                  xmlns="http://www.w3.org/2000/svg"
                  width="11.192"
                  height="14.922"
                  viewBox="0 0 11.192 14.922"
                >
                  <path
                    id="Path_293"
                    data-name="Path 293"
                    d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                    transform="translate(-3 -3.404)"
                    fill="#374957"
                  />
                  <path
                    id="Path_294"
                    data-name="Path 294"
                    d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                    transform="translate(-4.135)"
                    fill="#374957"
                  />
                  <path
                    id="Path_295"
                    data-name="Path 295"
                    d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                    transform="translate(-5.648 -4.917)"
                    fill="#374957"
                  />
                  <path
                    id="Path_296"
                    data-name="Path 296"
                    d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                    transform="translate(-6.12 -5.957)"
                    fill="#374957"
                  />
                </svg>
              }
            />
          </div>
          <div className="homepage__container__password__right"></div>
        </div>
        <div className="homepage__container__btn">
          <div
            onClick={onChangePassword} // to="/home-professional/personal-details-preview"
            style={{
              width: 150,
              marginTop: "1em",
            }}
            className="header__nav__btn btn__secondary"
          >
            Save
          </div>
        </div>
      </div>
    </>
  );
}
