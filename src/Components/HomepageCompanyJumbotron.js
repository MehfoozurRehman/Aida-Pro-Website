import React from "react";
import HomeComapanySearchFilter from "Components/HomeComapanySearchFilter";
import Img from "react-cool-img";

export default function HomepageCompanyJumbotron({
  keywords,
  searchDataByTitle,
  jobSekkerRecentJobsForCompanyAPI,
  searchByLocation,
  searchByKeyword,
  searchByRange,
  title,
  searchBy,
  searchByFreelancer,
  searchByUser,
  setIsLoginOpen,
  homePageCompanySvg,
  resetFilter,
  range,
  jobLocation,
}) {
  return (
    <section id="homepagecompanysearchsection">
      <div className="homepage__container__jumbotron">
        <div className="homepage__container__jumbotron__wrapper">
          <div className="homepage__container__jumbotron__left">
            <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
              AIDApro helps you find the <br /> right
              <span> professional or freelancer</span>
            </div>
            <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft">
              Find the right professionals through our database or let them find
              you by posting vacancies or projects
            </div>

            <HomeComapanySearchFilter
              placeholder="Search job titles, keywords"
              keywords={keywords}
              searchByTitle={searchDataByTitle}
              searchByFilter={jobSekkerRecentJobsForCompanyAPI}
              searchByLocation={(e) => searchByLocation(e)}
              searchByKeyword={(e) => searchByKeyword(e)}
              searchByRange={searchByRange}
              title={title}
              searchBy={searchBy}
              searchByFreelancer={searchByFreelancer}
              searchByUser={searchByUser}
              resetFilter={resetFilter}
              range={range}
              location={jobLocation}
            />
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "2em",
              }}
              className="animate__animated animate__fadeInRight"
            >
              <div
                className="header__nav__btn btn__primary"
                style={{
                  width: 150,
                  fontSize: 15,
                  marginRight: "1em",
                }}
                onClick={() => {
                  window.location.href = "/sign-up";
                  // setIsLoginOpen(true)
                }} // to={user.Id ? "/home-company" : "/home-company/login"}
              >
                Post a job
              </div>
              <div
                className="header__nav__btn btn__secondary"
                style={{
                  width: 150,
                  fontSize: 15,
                  marginRight: "1em",
                }}
                onClick={() => {
                  window.location.href = "/sign-up";
                  // setIsLoginOpen(true)
                }} // to={user.Id ? "/home-company" : "/home-company/login"}
              >
                Post a project
              </div>
            </div>
          </div>
          <Img
            loading="lazy"
            src={homePageCompanySvg}
            alt="homePageCompanySvg"
            className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          />
        </div>
      </div>
    </section>
  );
}
