import React from "react";
import Select from "react-select";

const dateOfPostingDDL = [
  { value: "7", label: "Last 7 days" },
  { value: "14", label: "Last 14 days" },
  { value: "30", label: "Last 30 days" },
];

export default function HomeFreelancerFilters({
  dateOfPosting,
  dateOfPostingChange,
  skills,
  skillId,
  skillSetChange,
  currentDate,
  handleChangeDate,
  freelancerProjectApi,
  clearAllFilter,
}) {
  return (
    <div className="dashboard__container__main__filters">
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Date of posting
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={dateOfPostingDDL}
            value={dateOfPosting}
            onChange={(e) => dateOfPostingChange(e)}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Required skills
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={skills}
            value={skillId}
            isMulti={true}
            onChange={(e) => skillSetChange(e)}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Project start
        </div>
        <div className="dashboard__container__main__filters__entry__select__date">
          <input
            type="date"
            value={currentDate}
            onChange={(e) => handleChangeDate(e)}
          />
        </div>
      </div>

      <div className="dashboard__company__container__left__form__view__filter">
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          type="button"
          onClick={() => freelancerProjectApi()}
          title="search"
        >
          Search
        </button>
      </div>
      <div
        className="dashboard__company__container__left__form__view__filter"
        style={{
          marginLeft: ".8em",
        }}
      >
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          type="button"
          onClick={() => clearAllFilter()}
          style={{
            background: "#71797E",
            color: "#ffffff",
          }}
          title="clear all"
        >
          Clear all
        </button>
      </div>
    </div>
  );
}
