import React, { useEffect, useState } from "react";
import Img from "react-cool-img";
import moment from "moment";
import { DeleteSvg, EditSvg } from "Assets";
import { getText } from "Utils/functions";

export default function PlannedVideoCallsList({
  data,
  startVideoCallRequest,
  timeZone,
  setIsMessageOpen,
  setUserData,
  deleteScheduleRequest,
  editVideoCall,
}) {
  const isOn = window.localStorage.getItem("isOn");

  const [postingList, setPostingList] = useState(data);

  const onClickItem = (item) => {
    startVideoCallRequest(item);
  };

  const timeUtcConvert = (time) => {
    var today = new Date(time);
    if (timeZone.charAt(0) == "-") {
      let newValue = timeZone.substring(1);
      today.setHours(today.getHours() - newValue);
    } else today.setHours(today.getHours() + parseInt(timeZone));
    return moment(today).format("LT");
  };

  const getName = (value) => {
    let name = "";
    if (isOn == "company") {
      if (value.JobSeeker != null)
        name = value.JobSeeker.FirstName + " " + value.JobSeeker.LastName;
      else name = value.Freelancer.FirstName + " " + value.Freelancer.LastName;
    } else name = value.CompanyProfile.CompanyName;
    // return name;

    return getText(name).length > 10 ? (
      <>{`${getText(name).substring(0, 10)}...`}</>
    ) : (
      <>{getText(name)}</>
    );
  };

  const valueOfTimezone = () => {
    // let timezone = company.companyAddressCountry.timezone_offset;
    let timeZoneGMTValue = null;
    if (timeZone.charAt(0) == "-") timeZoneGMTValue = `GMT${timeZone}`;
    else timeZoneGMTValue = `GMT+${timeZone}`;
    return timeZoneGMTValue;
  };

  return (
    postingList &&
    postingList.map((e) => (
      <div className="posting__container__table__list__wrapper" key={e.Id}>
        <div className="posting__container__table__list__wrapper__header">
          <div className="posting__container__table__list__wrapper__header__entry">
            Date
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            Name
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            From
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            To
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            Timezone
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            Agenda
          </div>
          <div className="posting__container__table__list__wrapper__header__entry">
            {/* Start/Join Call */}
          </div>
          <div
            style={{ opacity: 0 }}
            className="posting__container__table__list__wrapper__header__entry"
          ></div>
          <div
            style={{ opacity: 0 }}
            className="posting__container__table__list__wrapper__header__entry"
          ></div>
        </div>
        <div className="posting__container__table__list">
          <div
            className="posting__container__table__list__entry"
            state={{ selectedUser: e }}
          >
            <div className="--time abs-sf-icon i-time">
              {moment(e.StartDate).format("DD MMM, YYYY")}
            </div>
          </div>
          <div className="posting__container__table__list__entry">
            {getName(e)}
          </div>
          <div className="posting__container__table__list__entry">
            {timeUtcConvert(e.StartTime)}
          </div>
          <div className="posting__container__table__list__entry">
            {timeUtcConvert(e.EndTime)}
          </div>
          <div className="posting__container__table__list__entry">
            {valueOfTimezone()}
          </div>
          <div className="posting__container__table__list__entry">
            <div className="posting__container__table__list__entry__link">
              View Agenda
            </div>
            <div className="posting__container__table__list__entry__overlay">
              {getText(e.Notes).length > 1000 ? (
                <>{`${getText(e.Notes).substring(0, 1000)}...`}</>
              ) : (
                <>{getText(e.Notes)}</>
              )}
            </div>
          </div>
          <div className="posting__container__table__list__entry">
            <button
              className="header__nav__btn btn__secondary__meetings"
              // style={{ background: "#DE3F3F" }}
              onClick={() => onClickItem(e)}
              style={{
                background: "#71797E",
                boxShadow: "none",
              }}
              title="Start"
            >
              {isOn != "company" ? "Join" : "Start"}
            </button>
          </div>
          {isOn != "company" ? (
            <div className="posting__container__table__list__entry">
              <button
                className="header__nav__btn btn__secondary"
                onClick={() => {
                  setUserData(e);
                  setIsMessageOpen(true);
                }}
                title="Message"
              >
                Message
              </button>
              <button
                className="posting__container__table__list__entry__btn__del"
                onClick={() => deleteScheduleRequest(e)}
                title="Delete"
              >
                <Img loading="lazy" src={DeleteSvg} alt="DeleteSvg" />
              </button>
            </div>
          ) : (
            <div className="posting__container__table__list__entry">
              <button
                className="posting__container__table__list__entry__btn__del"
                onClick={() => editVideoCall(e)}
                title="Edit"
              >
                <Img loading="lazy" src={EditSvg} alt="EditSvg" />
              </button>
              <button
                className="posting__container__table__list__entry__btn__del"
                onClick={() => deleteScheduleRequest(e)}
                title="Delete"
              >
                <Img loading="lazy" src={DeleteSvg} alt="DeleteSvg" />
              </button>
            </div>
          )}
        </div>
      </div>
    ))
  );
}
