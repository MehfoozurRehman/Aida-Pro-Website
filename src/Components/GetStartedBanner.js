import { Link } from "@reach/router";
import React from "react";

export default function GetStartedBanner() {
  return (
    <div className="homepage__container__blog__get__started">
      <div className="homepage__container__blog__get__started__heading">
        Get started with AIDApro
      </div>
      <div className="homepage__container__blog__get__started__content">
        <div className="homepage__container__blog__get__started__content__left">
          Join our community today. Improve your career or find the best professionals for employment or to assist in any of your needs.
        </div>
        <div className="homepage__container__blog__get__started__content__right">
          <Link
            to="/"
            className="header__nav__btn btn__secondary"
            style={{ width: 180 }}
          >
            Get started
          </Link>
        </div>
      </div>
    </div>
  );
}
