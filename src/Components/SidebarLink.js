import React, { useEffect, useState } from "react";
import { useNavigate } from "@reach/router";

export default function SidebarLink({
  name,
  path,
  selectedImg,
  notSelectedImg,
  defaultChecked,
}) {
  const navigate = useNavigate();
  const [pathCheck, setPathCheck] = useState(window.location.pathname === path);
  useEffect(() => {
    setPathCheck(window.location.pathname === path);
  }, [window.location.pathname]);

  return (
    <div className="dashboard__container__sidebar__link animate__animated animate__fadeInDown">
      <input
        type="radio"
        name="dashboard__container__sidebar__link__input"
        className="dashboard__container__sidebar__link__input"
        defaultChecked={defaultChecked}
        checked={pathCheck}
        onChange={() => {
          navigate(path);
          setTimeout(() => {
            window.scrollTo({
              top: 0,
              behavior: "smooth",
            });
          }, 50);
        }}
      />
      <div className="dashboard__container__sidebar__link__content">
        <div className="dashboard__container__sidebar__link__svg">
          {pathCheck ? selectedImg : notSelectedImg}
        </div>
        <span>{name}</span>
      </div>
    </div>
  );
}
