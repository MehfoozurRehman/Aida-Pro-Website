import React from "react";
import Avatar from "Components/Avatar";
import timeSvg from "../Assets/timeSvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import milesSvg from "../Assets/milesSvg.svg";
import { getTotalYearsExperience } from "Utils/common";
import { Link } from "@reach/router";
import Img from "react-cool-img";

export default function HomePageCompanyResultCard({
  data,
  allRecords,
  isFreelancer,
}) {
  let industry = "";
  if (data.JobSeekerExperiences.length > 0) {
    industry = data.JobSeekerExperiences[0].Industry
      ? data.JobSeekerExperiences[0].Industry_Title != undefined
        ? data.JobSeekerExperiences[0].Industry_Title
        : data.JobSeekerExperiences[0].Industry_title
      : "";
  }

  return (
    <Link
      // onClick={() => {
      //   let dataPasObject = {
      //     redirectFrom: "CompanyHome",
      //     listOfUsers: allRecords,
      //     selectedUser: data,
      //   };
      //   // setDataAfterPaymentConfirmation(dataPasObject);
      //   // setIsPaymentConfirmation(true);
      // }}
      to="/home-company/posting/details"
      state={{
        redirectFrom: "CompanyHome",
        listOfUsers: allRecords,
        selectedUser: data,
      }}
      className="homepage__container__result__card"
    >
      <div className="homepage__container__result__card__header">
        <Avatar
          userPic={
            data.ProfilePicture != "" && data.ProfilePicture != null
              ? process.env.REACT_APP_BASEURL.concat(data.ProfilePicture)
              : null
          }
        />
        <div className="homepage__container__result__card__header__content">
          <div className="homepage__container__result__card__header__name">
            {data.FirstName} {data.LastName}
          </div>
          <div className="homepage__container__result__card__header__designation">
            {data.JobSeekerExperiences &&
              data.JobSeekerExperiences[0] &&
              data.JobSeekerExperiences[0].Title}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__content">
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={timeSvg}
            alt="timeSvg"
            className="homepage__container__result__card__content__entry__svg"
          />
          <div className="homepage__container__result__card__content__entry__text">
            {data.Availability_Title
              ? data.Availability_Title
              : "Not specified"}
          </div>
        </div>
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={experienceSvg}
            alt="experienceSvg"
            className="homepage__container__result__card__content__entry__svg"
          />
          <div className="homepage__container__result__card__content__entry__text">
            {
              data && data.TotalExperience == 0
                ? " Fresh"
                : data.TotalExperience > 0 && data.TotalExperience <= 1
                ? "Junior"
                : getTotalYearsExperience(data.TotalExperience)
              // parseFloat(data.TotalExperience).toFixed(1) + " Years of Experience"
            }
          </div>
        </div>
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={IndustrySvg}
            alt="IndustrySvg"
            className="homepage__container__result__card__content__entry__svg"
          />

          <div className="homepage__container__result__card__content__entry__text">
            {data
              ? data.JobSeekerExperiences != null &&
                data.JobSeekerExperiences.length > 0
                ? data.JobSeekerExperiences[0].Industry_title != undefined
                  ? data.JobSeekerExperiences[0].Industry_title
                  : data.JobSeekerExperiences[0].Industry_Title
                : "Not specified"
              : "Not specified"}
            {/* {industry != "" && industry != null ? industry : "Not specified"} */}
          </div>
        </div>
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={milesSvg}
            alt="milesSvg"
            className="homepage__container__result__card__content__entry__svg"
          />
          <div className="homepage__container__result__card__content__entry__text">
            {data.distance == 0
              ? "Nearby"
              : data.distance.toFixed(0) + " Km Away"}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__badges">
        {data.JobSeekerSkills &&
          data.JobSeekerSkills.map((e) => (
            <div
              className="homepage__container__result__card__badge"
              key={e.Id}
            >
              {e.Title}
            </div>
          ))}
      </div>
      {isFreelancer ? (
        <div className="homepage__container__result__card__professional__badge">
          FL
        </div>
      ) : null}
    </Link>
  );
}
