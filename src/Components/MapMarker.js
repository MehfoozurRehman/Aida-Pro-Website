import React, { useState } from "react";
import { Marker } from "@react-google-maps/api";

const MapMarker = ({ data }) => {
  const { position } = data;
  const [overlayViewVisible, setOverlayViewVisible] = useState(false);

  const handleMouseOver = () => {
    setOverlayViewVisible(true);
  };

  const handleMouseOut = () => {
    setOverlayViewVisible(false);
  };
  return (
    <Marker
      position={position}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
    >
      {/* {overlayViewVisible && <MapMarkerInfoWindow data={data} />} */}
    </Marker>
  );
};

export default MapMarker;
