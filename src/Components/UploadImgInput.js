import { pictureplaceholder } from "Assets";
import React, { useState } from "react";
import Img from "react-cool-img";
import { getBase64 } from "Utils/common";

export default function UploadImgInput({
  uploadedImagesArray,
  error,
  errorMessage,
  fileString,
}) {
  const [isPictureUploaded, setIsPictureUploaded] = useState(
    fileString != null && fileString != undefined ? true : false
  );
  const [imageFileFromData, setimageFileFromData] = useState(
    fileString != null && fileString != undefined ? fileString : null
  );
  const [uploadedImages, setUploadedImages] = useState([]);

  const fetchImage = async (img) => {
    setimageFileFromData(null);
    var convertedFilesCount = 0;
    var convertedFilesArray = [];
    for (let index = 0; index < img.length; index++) {
      let imageFile = img[index];
      await getBase64(imageFile, (result) => {
        convertedFilesArray.push(result);
        convertedFilesCount += 1;
        if (convertedFilesCount === img.length) {
          setUploadedImages(convertedFilesArray);
          setIsPictureUploaded(true);
          uploadedImagesArray(convertedFilesArray);
        }
      });
    }
  };

  return (
    <>
      {isPictureUploaded ? (
        <div className="pupup__container__from__wrapper__form__col__upload__btn__active__wrapper">
          <div className="pupup__container__from__wrapper__form__col__upload__btn__active">
            <Img
              loading="lazy"
              src={
                imageFileFromData != null && imageFileFromData != undefined
                  ? process.env.REACT_APP_BASEURL.concat(imageFileFromData)
                  : uploadedImages.length > 0
                  ? uploadedImages[0]
                  : pictureplaceholder
              }
              alt="project picture"
              className="pupup__container__from__wrapper__form__col__upload__btn__active__img"
            />
          </div>
          <button
            title="upload"
            className="header__nav__btn btn__secondary"
            style={{
              width: 150,
              background: "linear-gradient(#DB1414,#FF0E0E)",
              position: "relative",
            }}
          >
            <input
              type="file"
              accept="image/*"
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                top: 0,
                opacity: 0,
              }}
              onChange={(e) => fetchImage(e.currentTarget.files)}
            />
            Change Photo
          </button>
        </div>
      ) : (
        <div
          className={
            error
              ? "pupup__container__from__wrapper__form__col__upload__btn pupup__container__from__wrapper__form__col__upload__btn__error"
              : "pupup__container__from__wrapper__form__col__upload__btn"
          }
        >
          {error ? (
            <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
              {errorMessage}
            </div>
          ) : null}
          <input
            type="file"
            accept="image/*"
            className="pupup__container__from__wrapper__form__col__upload__btn__input"
            onChange={(e) => fetchImage(e.currentTarget.files)}
          />
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="30"
            height="30"
            viewBox="0 0 30 30"
          >
            <g
              id="Icon_feather-upload"
              data-name="Icon feather-upload"
              transform="translate(-3 -3)"
            >
              <path
                id="Path_21626"
                data-name="Path 21626"
                d="M31.5,22.5v6a3,3,0,0,1-3,3H7.5a3,3,0,0,1-3-3v-6"
                fill="none"
                stroke="#747474"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="3"
              />
              <path
                id="Path_21627"
                data-name="Path 21627"
                d="M25.5,12,18,4.5,10.5,12"
                fill="none"
                stroke="#747474"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="3"
              />
              <path
                id="Path_21628"
                data-name="Path 21628"
                d="M18,4.5v18"
                fill="none"
                stroke="#747474"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="3"
              />
            </g>
          </svg>
          <div className="pupup__container__from__wrapper__form__col__upload__btn__text">
            Upload Photo
          </div>
        </div>
      )}
    </>
  );
}
