import React from "react";
import Img from "react-cool-img";

export default function HomepageResearcherJumbotron({ homePageEmploymentSvg }) {
  return (
    <section id="homeemploymentfindjob">
      <div className="homepage__container__jumbotron">
        <div className="homepage__container__jumbotron__wrapper">
          <div className="homepage__container__jumbotron__left">
            <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
              Find a job that you <span>love</span>
            </div>
            <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft">
              Looking for the next step in your career or like to stay in touch
              with data or AI professionals for updates, questions and more? Our
              community can help you. Without obligations and completely free.
            </div>
            <a
              href="#homeemploymentsearchpanel"
              className="animate__animated animate__fadeInRight header__nav__btn btn__primary"
              style={{
                width: 150,
                height: 50,
              }}
            >
              Find job
            </a>
          </div>
          <Img
            loading="lazy"
            src={homePageEmploymentSvg}
            alt="homePageCompanySvg"
            className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          />
        </div>
      </div>
    </section>
  );
}
