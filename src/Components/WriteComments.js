import UserContext from "Context/UserContext";
import React, { useState, useEffect, useContext } from "react";
import Avatar from "./Avatar";

export default function WriteComments({
  placeholder,
  getWriteComment,
  forumId,
  questions,
}) {
  const user = useContext(UserContext);
  const [writeComment, setWriteComment] = useState("");
  const [userImage, setUserImage] = useState(null);
  useEffect(() => {
    if (Object.keys(user).length > 0) {
      if (user.Freelancer != null) {
        setUserImage(user.Freelancer.ProfilePicture != "" ? user.Freelancer.ProfilePicture : null);
      }
      else if (user.JobSeeker != null) {
        setUserImage(user.JobSeeker.ProfilePicture != "" ? user.JobSeeker.ProfilePicture : null);
      }
      else if (user.CompanyProfile != null) {
        setUserImage(user.CompanyProfile.LogoUrl != "" ? user.CompanyProfile.LogoUrl : null);
      }
    }
  });

  const addComment = () => {
    getWriteComment(writeComment, forumId, questions);
    setWriteComment("");
  };

  return (
    <div className="homepage__blog__artical__content__left__comment__container">
      <div className="homepage__blog__artical__content__left__comment__write__comment">
        <Avatar
          userPic={
            userImage != null
              ? process.env.REACT_APP_BASEURL.concat(userImage)
              : null
          }
        />
        <div className="homepage__blog__artical__content__left__comment__form">
          <input
            type="text"
            placeholder={placeholder}
            value={writeComment}
            className="homepage__blog__artical__content__left__comment__form"
            onChange={(e) => setWriteComment(e.currentTarget.value)}
            onKeyDown={(event) => {
              if (event.key === "Enter") addComment();
            }}
          />
          <button
            type="button"
            className="homepage__blog__artical__content__left__comment__write__comment__button"
            onClick={() => addComment()}
            title="add comment"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-send"
            >
              <line x1="22" y1="2" x2="11" y2="13"></line>
              <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
}
