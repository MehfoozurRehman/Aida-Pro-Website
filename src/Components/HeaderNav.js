import { Link } from "@reach/router";
import React from "react";
import Avatar from "Components/Avatar";
import HeaderNavNotifications from "./HeaderNavNotifications";
import HeaderNavLanguages from "./HeaderNavLanguages";
import HeaderNavMenu from "./HeaderNavMenu";
import HeaderNavMessages from "./HeaderNavMessages";

export default function HeaderNav({
  isMenuOpen,
  setIsMenuOpen,
  setIsLanguagePanelOpen,
  setIsMessagePanelOpen,
  setIsNotificationPanelOpen,
  onClickOutsideMessages,
  onClickOutsideNotifications,
  onClickOutsideMenu,
  onClickOutsideLanguage,
  isOnDashboard,
  isUserLogedIn,
  isMessagePanelOpen,
  setUnreadMessagesLength,
  isOn,
  unreadMessagesLength,
  isNotificationPanelOpen,
  setUnreadNotificationLength,
  setNotificationIsReadTrue,
  unreadNotificationLength,
  notificationData,
  updateNotificationDataAcceptReject,
  userImage,
  Date,
  navigate,
  setIsLoginOpen,
  isOnSignUp,
  isOnHomeCompany,
  languageSelected,
  unitedstates,
  germany,
  isLanguagePanelOpen,
  setLanguageSelected,
  messageNotificationData,
}) {
  return (
    <div className="header__nav animate__animated animate__fadeInRight">
      <HeaderNavMenu
        isMenuOpen={isMenuOpen}
        setIsMenuOpen={setIsMenuOpen}
        setIsLanguagePanelOpen={setIsLanguagePanelOpen}
        setIsMessagePanelOpen={setIsMessagePanelOpen}
        setIsNotificationPanelOpen={setIsNotificationPanelOpen}
        onClickOutsideMenu={onClickOutsideMenu}
        isOn={isOn}
        isUserLogedIn={isUserLogedIn}
      />
      {isOnDashboard ? (
        isUserLogedIn ? (
          <>
            <HeaderNavMessages
              isMessagePanelOpen={isMessagePanelOpen}
              setIsMessagePanelOpen={setIsMessagePanelOpen}
              setIsNotificationPanelOpen={setIsNotificationPanelOpen}
              setIsLanguagePanelOpen={setIsLanguagePanelOpen}
              setIsMenuOpen={setIsMenuOpen}
              setUnreadMessagesLength={setUnreadMessagesLength}
              isOn={isOn}
              unreadMessagesLength={unreadMessagesLength}
              onClickOutsideMessages={onClickOutsideMessages}
              messageNotificationData={messageNotificationData}
            />
            <HeaderNavNotifications
              isNotificationPanelOpen={isNotificationPanelOpen}
              setIsNotificationPanelOpen={setIsNotificationPanelOpen}
              setUnreadNotificationLength={setUnreadNotificationLength}
              setIsMessagePanelOpen={setIsMessagePanelOpen}
              setIsLanguagePanelOpen={setIsLanguagePanelOpen}
              setIsMenuOpen={setIsMenuOpen}
              setNotificationIsReadTrue={setNotificationIsReadTrue}
              unreadNotificationLength={unreadNotificationLength}
              onClickOutsideNotifications={onClickOutsideNotifications}
              notificationData={notificationData}
              updateNotificationDataAcceptReject={
                updateNotificationDataAcceptReject
              }
            />
            <Avatar
              userPic={
                userImage != null && userImage != ""
                  ? process.env.REACT_APP_BASEURL.concat(userImage) +
                    "?" +
                    new Date()
                  : null
              }
              style={{
                boxShadow: "0px 0px 10px rgba(12, 166, 157,.9)",
              }}
              onClick={() => {
                setIsNotificationPanelOpen(false);
                setUnreadNotificationLength(null);
                setIsMessagePanelOpen(false);
                setIsLanguagePanelOpen(false);
                window.innerWidth < 800
                  ? setIsMenuOpen(false)
                  : setIsMenuOpen(true);
                isOn == "company"
                  ? navigate("/home-company/profile")
                  : isOn == "professional"
                  ? navigate("/home-professional/profile")
                  : navigate("/home-freelancer/profile");
                window.scrollTo({
                  top: 0,
                  behavior: "smooth",
                });
              }}
              dontShowHover={false}
            />
          </>
        ) : (
          <>
            <button
              title="sign in"
              className="header__nav__btn btn__primary"
              onClick={() => {
                setIsLoginOpen(true);
                setIsLanguagePanelOpen(false);
              }}
            >
              Sign In
            </button>
            {isOnSignUp ? null : (
              <Link
                to="/sign-up"
                className="header__nav__btn btn__secondary"
                style={
                  isOnHomeCompany
                    ? {
                        width: "160px",
                      }
                    : null
                }
              >
                {isOnHomeCompany ? "Sign up to hire" : "Sign Up"}
              </Link>
            )}
          </>
        )
      ) : isUserLogedIn ? (
        <>
          <Avatar
            userPic={
              userImage != null && userImage != ""
                ? process.env.REACT_APP_BASEURL.concat(userImage) +
                  "?" +
                  new Date()
                : null
            }
            onClick={() => {
              if (isOn == "company") navigate("/home-company/profile");
              else if (isOn == "professional")
                navigate("/home-professional/profile");
              else navigate("/home-freelancer/profile");
              window.scrollTo({
                top: 0,
                behavior: "smooth",
              });
            }}
            dontShowHover={false}
          />
        </>
      ) : (
        <>
          <button
            title="sign in"
            className="header__nav__btn btn__primary"
            onClick={() => {
              setIsLoginOpen(true);
              setIsLanguagePanelOpen(false);
            }}
          >
            Sign In
          </button>
          {isOnSignUp ? null : (
            <Link
              to="/sign-up"
              className="header__nav__btn btn__secondary"
              style={
                isOnHomeCompany
                  ? {
                      width: "160px",
                    }
                  : null
              }
            >
              {isOnHomeCompany ? "Sign up to hire" : "Sign Up"}
            </Link>
          )}
        </>
      )}
      <HeaderNavLanguages
        setIsLanguagePanelOpen={setIsLanguagePanelOpen}
        languageSelected={languageSelected}
        unitedstates={unitedstates}
        germany={germany}
        isLanguagePanelOpen={isLanguagePanelOpen}
        onClickOutsideLanguage={onClickOutsideLanguage}
        setLanguageSelected={setLanguageSelected}
      />
    </div>
  );
}
