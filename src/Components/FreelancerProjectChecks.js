import React, { useState } from "react";

export default function FreelancerProjectChecks({ checked }) {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <div className="homepage__container__result__card__wrapper">
      <input
        type="radio"
        name="homepage__container__result__card"
        id="homepage__container__result__card"
        className="homepage__container__result__card__input"
        defaultChecked={checked}
      />
      <div
        onClick={() => {}}
        className="homepage__container__result__card"
        style={isSelected ? { backgroundColor: "#303043" } : null}
      >
        <div className="homepage__container__result__card__header">
          <div
            className="homepage__container__result__card__header__content"
            style={{ marginBottom: -10 }}
          >
            <div
              className="homepage__container__result__card__header__designation"
              style={{ marginBottom: "0.3em" }}
            >
              15 hours ago
            </div>
            <div className="homepage__container__result__card__header__name">
              Machine Learning Engineering
            </div>
          </div>
        </div>
        <div
          className="homepage__container__result__card__content"
          style={{ marginBottom: -20 }}
        >
          <div className="homepage__container__result__card__content__entry">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24.547"
              height="26.044"
              viewBox="0 0 24.547 26.044"
              fill="currentColor"
            >
              <g
                id="building_1_"
                data-name="building (1)"
                transform="translate(0)"
                opacity="0.55"
              >
                <path
                  id="Path_2270"
                  data-name="Path 2270"
                  d="M38.5,24.518H36.487V.763A.763.763,0,0,0,35.724,0H18.255a.763.763,0,0,0-.763.763V24.518H15.479a.763.763,0,0,0,0,1.526H38.5a.763.763,0,1,0,0-1.526Zm-14.111,0v-5.2h5.2v5.2Zm6.728,0V18.553a.763.763,0,0,0-.763-.763H23.626a.763.763,0,0,0-.763.763v5.965H19.018V1.526H34.961V24.518Z"
                  transform="translate(-14.716)"
                />
                <path
                  id="Path_2271"
                  data-name="Path 2271"
                  d="M139.742,66.309H136a.763.763,0,0,0-.763.763v3.739a.763.763,0,0,0,.763.763h3.739a.763.763,0,0,0,.763-.763V67.072A.763.763,0,0,0,139.742,66.309Zm-.763,3.739h-2.213V67.835h2.213Z"
                  transform="translate(-129.109 -62.936)"
                />
                <path
                  id="Path_2272"
                  data-name="Path 2272"
                  d="M277.754,66.309h-3.739a.763.763,0,0,0-.763.763v3.739a.763.763,0,0,0,.763.763h3.739a.763.763,0,0,0,.763-.763V67.072A.763.763,0,0,0,277.754,66.309Zm-.763,3.739h-2.213V67.835h2.213Z"
                  transform="translate(-260.101 -62.936)"
                />
                <path
                  id="Path_2273"
                  data-name="Path 2273"
                  d="M139.742,205.954H136a.763.763,0,0,0-.763.763v3.739a.763.763,0,0,0,.763.763h3.739a.763.763,0,0,0,.763-.763v-3.739A.763.763,0,0,0,139.742,205.954Zm-.763,3.739h-2.213V207.48h2.213Z"
                  transform="translate(-129.109 -195.478)"
                />
                <path
                  id="Path_2274"
                  data-name="Path 2274"
                  d="M277.754,205.954h-3.739a.763.763,0,0,0-.763.763v3.739a.763.763,0,0,0,.763.763h3.739a.763.763,0,0,0,.763-.763v-3.739A.763.763,0,0,0,277.754,205.954Zm-.763,3.739h-2.213V207.48h2.213Z"
                  transform="translate(-260.101 -195.478)"
                />
              </g>
            </svg>

            <div className="homepage__container__result__card__content__entry__text">
              Technology
            </div>
          </div>
          <div className="homepage__container__result__card__content__entry">
            <svg
              id="group"
              xmlns="http://www.w3.org/2000/svg"
              width="25.336"
              height="21.008"
              viewBox="0 0 25.336 21.008"
              fill="currentColor"
            >
              <g
                id="Group_736"
                data-name="Group 736"
                transform="translate(0 9.202)"
              >
                <g id="Group_735" data-name="Group 735">
                  <path
                    id="Path_2275"
                    data-name="Path 2275"
                    d="M21.624,231.584h-2.48a3.7,3.7,0,0,0-.975.131,3.714,3.714,0,0,0-3.309-2.035H10.476a3.714,3.714,0,0,0-3.309,2.035,3.7,3.7,0,0,0-.975-.131H3.711A3.716,3.716,0,0,0,0,235.3v3.965a2.229,2.229,0,0,0,2.227,2.227H23.109a2.229,2.229,0,0,0,2.227-2.227V235.3A3.716,3.716,0,0,0,21.624,231.584Zm-14.86,1.808V240H2.227a.743.743,0,0,1-.742-.742V235.3a2.229,2.229,0,0,1,2.227-2.227h2.48a2.221,2.221,0,0,1,.582.078C6.768,233.227,6.765,233.309,6.765,233.391ZM17.087,240H8.249v-6.611a2.229,2.229,0,0,1,2.227-2.227H14.86a2.229,2.229,0,0,1,2.227,2.227Zm6.765-.742a.743.743,0,0,1-.742.742H18.571v-6.611c0-.082,0-.164-.009-.245a2.222,2.222,0,0,1,.582-.078h2.48a2.229,2.229,0,0,1,2.227,2.227Z"
                    transform="translate(0 -229.68)"
                    strokeWidth={0.5}
                  />
                </g>
              </g>
              <g
                id="Group_738"
                data-name="Group 738"
                transform="translate(1.653 4.096)"
              >
                <g id="Group_737" data-name="Group 737">
                  <path
                    id="Path_2276"
                    data-name="Path 2276"
                    d="M36.712,126.5a3.3,3.3,0,1,0,3.3,3.3A3.3,3.3,0,0,0,36.712,126.5Zm0,5.111a1.813,1.813,0,1,1,1.813-1.813A1.815,1.815,0,0,1,36.712,131.615Z"
                    transform="translate(-33.414 -126.504)"
                    strokeWidth={0.5}
                  />
                </g>
              </g>
              <g
                id="Group_740"
                data-name="Group 740"
                transform="translate(8.262 0)"
              >
                <g id="Group_739" data-name="Group 739">
                  <path
                    id="Path_2277"
                    data-name="Path 2277"
                    d="M171.368,43.729a4.406,4.406,0,1,0,4.406,4.406A4.411,4.411,0,0,0,171.368,43.729Zm0,7.327a2.921,2.921,0,1,1,2.921-2.921A2.925,2.925,0,0,1,171.368,51.056Z"
                    transform="translate(-166.962 -43.729)"
                    strokeWidth={0.5}
                  />
                </g>
              </g>
              <g
                id="Group_742"
                data-name="Group 742"
                transform="translate(17.086 4.096)"
              >
                <g id="Group_741" data-name="Group 741">
                  <path
                    id="Path_2278"
                    data-name="Path 2278"
                    d="M348.592,126.5a3.3,3.3,0,1,0,3.3,3.3A3.3,3.3,0,0,0,348.592,126.5Zm0,5.111a1.813,1.813,0,1,1,1.813-1.813A1.815,1.815,0,0,1,348.592,131.615Z"
                    transform="translate(-345.294 -126.504)"
                    strokeWidth={0.5}
                  />
                </g>
              </g>
            </svg>
            <div className="homepage__container__result__card__content__entry__text">
              340 employees
            </div>
          </div>
        </div>
        <div
          className="homepage__container__result__card__badges"
          style={{ paddingBottom: "0em", overflow: "hidden" }}
        >
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
        </div>
      </div>
    </div>
  );
}
