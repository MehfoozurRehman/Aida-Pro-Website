import React from "react";

export default function TrendingTopicBadge({ data, setSelectedTagId, tagId }) {
  return (
    <button
      onClick={() => {
        setSelectedTagId(data.Id);
      }}
      className={
        data.Id === tagId
          ? "homepage__container__jumbotron__form__filters__badge homepage__container__jumbotron__form__filters__badge__active"
          : "homepage__container__jumbotron__form__filters__badge"
      }
      title={`${data.Title} | ${data.SearchCount} Posts`}
    >
      {`${data.Title} | ${data.SearchCount} Posts`}
    </button>
  );
}
