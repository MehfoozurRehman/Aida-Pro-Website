import React from "react";
import Select from "react-select";
import _ from "lodash";

export default function HomeCompanyFilters({
  skillsDropDownData,
  skills,
  skillChange,
  industryTypeDropDownData,
  industryChange,
  industryType,
  experienceTypeDropDownData,
  experienceChange,
  experience,
  jobTypeDropDownData,
  jobTypeChange,
  jobType,
  handleChangeInput,
  salaryFrom,
  salaryTo,
  educationTypeDropDownData,
  educationChange,
  education,
  availbilityTypeDropDownData,
  availabilityChange,
  availbility,
  languageTypeDropDownData,
  languageChange,
  languages,
  jobSekkerRecentJobsForCompanyAPI,
  resetFilters,
}) {
  return (
    <div className="dashboard__container__main__filters">
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Skills
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={skillsDropDownData}
            value={skills}
            isMulti={true}
            onChange={(e) => skillChange(e)}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Industries worked in
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={industryTypeDropDownData}
            onChange={(e) => industryChange(e)}
            value={industryType}
            isMulti={true}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Experience level
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={experienceTypeDropDownData}
            onChange={(e) => experienceChange(e)}
            value={experience}
            isMulti={true}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Job type
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={jobTypeDropDownData}
            onChange={(e) => jobTypeChange(e)}
            value={jobType}
            isMulti={true}
          />
        </div>
      </div>
      <div
        className="dashboard__container__main__filters__entry"
        style={{
          minWidth: "300px",
        }}
      >
        <div className="dashboard__container__main__filters__entry__name">
          Salary or Hourly rate (€)
        </div>
        <div className="dashboard__container__main__filters__entry__select__wrapper">
          <div className="dashboard__container__main__filters__entry__select">
            <input
              name="minSalary"
              onChange={(event) => {
                const re = /^[0-9\b]+$/;

                if (
                  event.currentTarget.value === "" ||
                  re.test(event.currentTarget.value)
                ) {
                  handleChangeInput(event);
                }
              }}
              value={salaryFrom}
              type="text"
              placeholder="Min"
              className="dashboard__container__main__filters__entry__select__input"
            />
          </div>
          <div className="dashboard__container__main__filters__entry__spacer"></div>
          <div className="dashboard__container__main__filters__entry__select">
            <input
              name="maxSalary"
              onChange={(event) => {
                const re = /^[0-9\b]+$/;

                if (
                  event.currentTarget.value === "" ||
                  re.test(event.currentTarget.value)
                ) {
                  handleChangeInput(event);
                }
              }}
              value={salaryTo}
              type="text"
              placeholder="Max"
              className="dashboard__container__main__filters__entry__select__input"
            />
          </div>
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Education
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={educationTypeDropDownData}
            onChange={(e) => educationChange(e)}
            value={education}
            isMulti={true}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Availability
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={availbilityTypeDropDownData}
            onChange={(e) => availabilityChange(e)}
            value={availbility}
            isMulti={true}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Language
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={languageTypeDropDownData}
            onChange={(e) => languageChange(e)}
            value={languages}
            isMulti={true}
          />
        </div>
      </div>
      <div className="dashboard__company__container__left__form__view__filter">
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          type="button"
          onClick={() => jobSekkerRecentJobsForCompanyAPI()}
          title="search"
        >
          Search
        </button>
      </div>
      <div
        className="dashboard__company__container__left__form__view__filter"
        style={{
          marginLeft: ".8em",
        }}
      >
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          style={{
            background: "#71797E",
            color: "#ffffff",
          }}
          type="button"
          onClick={() => resetFilters()}
          title="clear all"
        >
          Clear all
        </button>
      </div>
    </div>
  );
}
