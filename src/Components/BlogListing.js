import React from "react";
import Pagination from "react-js-pagination";
import { BlogCard } from "Components";

export default function BlogListing({
  navigateToBlogArticle,
  pageNumber,
  limit,
  totalRecords,
  blogApiData,
  setPageNumber,
}) {
  return (
    <>
      <div className="homepage__container__blog">
        <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
          Latest <span>Blogs</span>
        </div>
      </div>
      <div className="homepage__container__blog__cards">
        {blogApiData.length > 0
          ? blogApiData.map((blogData, i) => (
              <BlogCard
                key={i}
                id="12131312"
                blogData={blogData}
                onClick={navigateToBlogArticle}
              />
            ))
          : null}
      </div>
      <div className="posting__container__pagination">
        <Pagination
          activePage={pageNumber}
          itemsCountPerPage={limit}
          totalItemsCount={totalRecords}
          pageRangeDisplayed={9}
          onChange={(e) => {
            setPageNumber(e);
          }}
        />
      </div>
    </>
  );
}
