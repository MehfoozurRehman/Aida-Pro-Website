import React, { useEffect, useState } from "react";
import Avatar from "./Avatar";
import experienceSvg from "../Assets/experienceSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import { getTotalYearsExperience } from "Utils/common";
import Img from "react-cool-img";

export default function JobProjectCard({
  checked,
  data,
  showJobDetail,
  isFreelancer,
  setProjectData,
  selectedTab,
}) {
  const [isSelected, setIsSelected] = useState(false);

  const {
    JobTitle,
    PostingDate,
    Location,
    SalaryFrom,
    SalaryTo,
    SalaryTypeLookup,
    JobSkills,
  } = data;
  let Title = JobTitle;
  let Skills = JobSkills;
  if (data != null) {
    if (!Title) {
      if (!data.Title) {
        Title = data.FirstName + " " + data.LastName;
      } else {
        Title = data.Title;
      }
    }
    if (!Skills) {
      if (!data.ProjectSkills) {
        Skills = data.JobSeekerSkills;
      } else {
        Skills = data.ProjectSkills;
      }
    }
  }

  useEffect(() => {
    if (checked) {
      showJobDetail ? showJobDetail(data, isFreelancer) : setProjectData(data);
    }
  }, []);

  return (
    <div
      className="homepage__container__result__card__wrapper"
      style={{
        borderBottom: "1px solid lightgray",
        paddingBottom: ".5em",
        marginBottom: ".5em",
      }}
    >
      <input
        type="radio"
        name="homepage__container__result__card"
        id="homepage__container__result__card"
        className="homepage__container__result__card__input"
        defaultChecked={checked}
        onChange={
          showJobDetail
            ? () => showJobDetail(data, isFreelancer)
            : () => setProjectData(data)
        }
      />
      <div
        className="homepage__container__result__card"
        style={isSelected ? { backgroundColor: "#303043" } : null}
      >
        <div className="homepage__container__result__card__header">
          <Avatar
            userPic={
              data.ProfilePicture != null && data.ProfilePicture != ""
                ? process.env.REACT_APP_BASEURL.concat(data.ProfilePicture)
                : null
            }
          />
          <div className="homepage__container__result__card__header__content">
            <div className="homepage__container__result__card__header__name">
              {Title}
            </div>
            <div className="homepage__container__result__card__header__designation">
              {data
                ? data.JobSeekerExperiences != null &&
                  data.JobSeekerExperiences.length > 0
                  ? data.JobSeekerExperiences[0].Title
                  : "Not specified"
                : "Not specified"}
            </div>
          </div>
        </div>
        <div
          className="homepage__container__result__card__content"
          style={{ marginBottom: -20 }}
        >
          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={IndustrySvg}
              alt="IndustrySvg"
              className="homepage__container__result__card__content__entry__svg"
            />
            <div className="homepage__container__result__card__content__entry__text">
              {data
                ? data.JobSeekerExperiences != null &&
                  data.JobSeekerExperiences.length > 0
                  ? data.JobSeekerExperiences[0].Industry_title != undefined
                    ? data.JobSeekerExperiences[0].Industry_title
                    : data.JobSeekerExperiences[0].Industry_Title
                  : "Not specified"
                : "Not specified"}
              {/* {data.Availability_Title
                ? data.Availability_Title
                : "Not specified"} */}
            </div>
          </div>

          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={experienceSvg}
              alt="experienceSvg"
              className="homepage__container__result__card__content__entry__svg"
            />
            <div className="homepage__container__result__card__content__entry__text">
              {data != null
                ? data.TotalExperience == 0
                  ? "Fresh"
                  : getTotalYearsExperience(data.TotalExperience)
                : "Not specified"}
            </div>
          </div>
        </div>
        <div
          className="homepage__container__result__card__badges"
          style={{ paddingBottom: "0em", overflow: "hidden", marginTop: 15 }}
        >
          {Skills.length > 0
            ? Skills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Skill != undefined ? skill.Skill.Title : skill.Title}
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}
