import { germany, unitedstates } from "Assets";
import React from "react";
import Img from "react-cool-img";

export default function LanguagePanel({
  setLanguageSelected,
  setIsLanguagePanelOpen,
}) {
  return (
    <div className="header__language__panel animate__animated animate__fadeIn">
      <button
        title="En language"
        className="language__btn"
        onClick={() => {
          setLanguageSelected("en");
          setIsLanguagePanelOpen(false);
        }}
      >
        <Img
          loading="lazy"
          src={unitedstates}
          alt="flag"
          className="language__btn__icon"
        />
        <div className="language__btn__name">EN</div>
      </button>
      <button
        title="De language"
        className="language__btn"
        onClick={() => {
          setLanguageSelected("de");
          setIsLanguagePanelOpen(false);
        }}
      >
        <Img
          loading="lazy"
          src={germany}
          alt="flag"
          className="language__btn__icon"
        />
        <div className="language__btn__name">DE</div>
      </button>
    </div>
  );
}
