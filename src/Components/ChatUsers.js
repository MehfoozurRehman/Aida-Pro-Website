import React, { useState } from "react";
import { MoreVertical } from "react-feather";
import { Avatar } from "Components";
import OutsideClickHandler from "react-outside-click-handler";
import { navigate } from "@reach/router";

export default function ChatUsers({
  data,
  onClickChatNode,
  selectedChat,
  showName,
  onClickDecline,
  setVideoCallUserData,
  setIsRequestVideoCallOpen
}) {
  const isOn = window.localStorage.getItem("isOn");
  const [userMenu, setUserMenu] = useState(false);

  return (
    <div
      onClick={() => {
        onClickChatNode(data);
      }}
    >
      <div className="messenger__container__sidebar__users__entry">
        <input
          type="radio"
          defaultChecked={data.ChatId === selectedChat.ChatId ? true : false}
          className="messenger__container__sidebar__users__entry__input"
          name="messenger__container__sidebar__users__entry__input"
        />
        <div className="messenger__container__sidebar__users__entry__wrapper">
          <Avatar
            userPic={
              data.ProfilePicture != null && data.ProfilePicture != ""
                ? process.env.REACT_APP_BASEURL.concat(data.ProfilePicture)
                : null
            }
          />
          <div className="messenger__container__sidebar__users__entry__content">
            <div className="messenger__container__sidebar__users__entry__content__name">
              {data.FirstName != null ? showName(data) : null}
            </div>
          </div>
          <button
            onClick={() => {
              userMenu ? setUserMenu(false) : setUserMenu(true);
            }}
            className="messenger__container__sidebar__users__entry__btn"
          >
            <MoreVertical size={20} color="currentColor" />
          </button>
          {userMenu ? (
            <OutsideClickHandler
              onOutsideClick={() => {
                setUserMenu(false);
              }}
            >
              <div className="messenger__container__sidebar__users__entry__menu">
                {isOn == "company" ?
                  <>
                    <button
                      className="messenger__container__sidebar__users__entry__menu__entry"
                      onClick={() => {
                        setUserMenu(false);
                        navigate("/home-company/messenger-user-profile", {
                          state: {
                            redirectFrom: "Messenger",
                            jobseekerId: selectedChat.JobSeekerId,
                            freelancerId: selectedChat.FreelancerId,
                            selectedUser: selectedChat,
                          },
                        });
                      }}
                    >
                      View profile
                </button>
                    <button
                      className="messenger__container__sidebar__users__entry__menu__entry"
                      onClick={() => {
                        setUserMenu(false);
                        let object = {
                          JobSeekerId: selectedChat.FreelancerId != null ? selectedChat.FreelancerId : 0,
                          FreelancerId: selectedChat.JobSeekerId != null ? selectedChat.JobSeekerId : 0
                        }
                        setVideoCallUserData(object)
                        setIsRequestVideoCallOpen(true);
                      }}
                    >
                      Video call
                </button>
                    <button
                      className="messenger__container__sidebar__users__entry__menu__entry"
                      onClick={() => {
                        setUserMenu(false);
                        window.open(process.env.REACT_APP_BASEURL.concat(selectedChat.Cv), "_blank");
                      }} >
                      View CV
                </button>
                  </>
                  :
                  null}
                <button
                  className="messenger__container__sidebar__users__entry__menu__entry"
                  onClick={() => {
                    setUserMenu(false);
                    onClickDecline();
                  }}
                >
                  Delete chat
                </button>
              </div>
            </OutsideClickHandler>
          ) : null}
        </div>
      </div>
    </div>
  );
}
