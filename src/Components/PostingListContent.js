import React from "react";
import { Link } from "@reach/router";
import PostingListDropDown from "Components/PostingListDropDown";
import Img from "react-cool-img";
import moment from "moment";

export default function PostingListContent({
  undefined,
  statusAPI,
  changeStatusApi,
  editRecord,
  data,
  EditSvg,
  deleteRecord,
  DeleteSvg,
  index,
  statusDataRecieved,
  setStatusDataRecieved,
}) {
  return (
    <div className="posting__container__table__list">
      <div className="posting__container__table__list__entry">
        {(data.JobTitle && data.JobTitle.trim()) ||
          (data.Title && data.Title.trim())}
      </div>
      <Link
        to={data.TotalApplied === 0 ? "" : "/home-company/posting/details"}
        state={{
          isApplicant: data.TotalApplied > 0 ? 1 : 0,
          isInterested: 0,
          isVisitor: 0,
          Id: data.Id,
          Type: data.Type,
          title: "Applicants",
        }}
        className="posting__container__table__list__entry"
        onClick={() => {
          localStorage.setItem("isApplicant", data.TotalApplied > 0 ? 1 : 0);
          localStorage.setItem("isInterested", 0);
          localStorage.setItem("isVisitor", 0);
          localStorage.setItem("JobId", data.Id);
          localStorage.setItem("JobType", data.Type);
        }}
      >
        {data.TotalApplied === null || data.TotalApplied === undefined
          ? "0"
          : data.TotalApplied}
      </Link>
      <Link
        to={data.TotalInterested === 0 ? "" : "/home-company/posting/details"}
        state={{
          isApplicant: 0,
          isInterested: data.TotalInterested > 0 ? 1 : 0,
          isVisitor: 0,
          Id: data.Id,
          Type: data.Type,
          title: "Interested candidates",
        }}
        onClick={() => {
          localStorage.setItem("isApplicant", 0);
          localStorage.setItem(
            "isInterested",
            data.TotalInterested > 1 ? 1 : 0
          );
          localStorage.setItem("isVisitor", 0);
          localStorage.setItem("JobId", data.Id);
          localStorage.setItem("JobType", data.Type);
        }}
        className="posting__container__table__list__entry"
      >
        {data.TotalInterested === null || data.TotalInterested === undefined
          ? "0"
          : data.TotalInterested}
      </Link>
      <div className="posting__container__table__list__entry">
        {data.TotalVisited === null || data.TotalVisited === undefined
          ? "0"
          : data.TotalVisited}
      </div>
      <div className="posting__container__table__list__entry">
        <div className="--time abs-sf-icon i-time">
          {moment(data.CreatedOn).format("DD MMM, YYYY")}
        </div>
      </div>
      <div className="posting__container__table__list__entry">
        <PostingListDropDown
          statusData={statusAPI}
          changeStatusApiCall={(label, status, type) =>
            changeStatusApi(label, data.Id, status, type)
          }
          recordType={data.Type}
          currentStatus={data.StatusTypeLookupDetail.Title}
          index={index}
          statusDataRecieved={statusDataRecieved}
          setStatusDataRecieved={setStatusDataRecieved}
        />
      </div>
      <div className="posting__container__table__list__entry">
        <button
          className="posting__container__table__list__entry__btn__del"
          onClick={() => editRecord(data)}
          title="Edit"
        >
          <Img loading="lazy" src={EditSvg} alt="EditSvg" />
        </button>
        <button
          className="posting__container__table__list__entry__btn__del"
          onClick={() => {
            if (window.confirm("Are you sure ?")) {
              deleteRecord(data);
            }
          }}
          title="Delete"
        >
          <Img loading="lazy" src={DeleteSvg} alt="DeleteSvg" />
        </button>
      </div>
    </div>
  );
}
