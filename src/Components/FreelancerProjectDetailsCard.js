import React, { useState, useContext, useEffect } from "react";
import FreelancerProjectChecks from "./FreelancerProjectChecks";
import { interestedJob, interestedProject } from "../API/Api";
import UserContext from "Context/UserContext";
import { CustomError } from "../Screens/Toasts";
import { LoadingMask } from "../Screens/LoadingMask";
import moment from "moment";
import timeSvg from "../Assets/timeSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import milesSvg from "../Assets/milesSvg.svg";
import employeeSvg from "../Assets/employeeSvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import { useSelector } from "react-redux";
import { getText } from "Utils/functions";
import Img from "react-cool-img";

export default function FreelancerProjectDetailsCard({
  isFreelancer,
  setIsApplyForJob,
  isRandom,
  data,
  jobStatusApiData,
  callBackToGetInterested,
  setIsApplyForJobProject,
  isApplyForJobProject,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) {
  const isOn = window.localStorage.getItem("isOn");
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);
  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    isInterestedAndAppliedCheck();
  });

  const [isInterested, setIsInterested] = useState(false);
  const [isJobApplied, setIsJobApplied] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const user = useContext(UserContext);

  const isInterestedAndAppliedCheck = () => {
    let isInterestedCheck = false;
    let isJobCheck = false;
    if (jobStatusApiData) {
      isInterestedCheck = jobStatusApiData.IsInterested;
      isJobCheck = jobStatusApiData.IsApplied;
    }
    setIsInterested(isInterestedCheck);
    setIsJobApplied(isJobCheck);
  };

  const profileCompleted = () => {
    if (jobsekker.skills.length > 0 && jobsekker.qualification.length > 0) {
      return true;
    } else {
      return false;
    }
  };

  const submitInterested = () => {
    // if (profileCompleted()) {
    if (isFreelancer) submitProjectInterested();
    else submitJobInterested();
    // } else {
    //   setHeadingAndTextForAlertDialog("Please complete your profile to interest, thanks")
    //   setAlertDialogVisibility(true);
    // }
  };

  const submitJobInterested = () => {
    // setIsLoading(true);
    let interested = false;
    if (jobStatusApiData) {
      interested = jobStatusApiData.IsInterested;
    }
    setIsInterested(!interested);
    let requestData = {
      JobId: data.Id,
      JobSeekerId: user.JobSeekerId,
      IsInterested: !interested,
    };

    interestedJob(requestData)
      .then(({ res }) => {
        if (requestData.IsInterested)
          // //CustomSuccess(`Interested Successfully...`);
          callBackToGetInterested();
        setIsLoading(false);
        // window.location.reload();
      })
      .catch((err) => {
        CustomError("Failed to Interested Job.");
        //setIsLoading(false);
      });
  };

  const submitProjectInterested = () => {
    // setIsLoading(true);
    let interested = false;
    if (jobStatusApiData) {
      interested = jobStatusApiData.IsInterested;
    }
    let requestData = {
      ProjectId: data.Id,
      FreelancerId: user.FreelancerId,
      IsInterested: !interested,
    };

    interestedProject(requestData)
      .then(({ responseData }) => {
        if (requestData.IsInterested)
          //CustomSuccess(`Interested Successfully...`);
          callBackToGetInterested();
        setIsLoading(false);
      })
      .catch((err) => {
        CustomError("Failed to Interested Project.");
        setIsLoading(false);
      });
  };

  useEffect(() => {
    if (isApplyForJobProject) {
      callBackToGetInterested();
      setIsApplyForJobProject(false);
    }
  });

  const navigateToApplyJob = () => {
    if (profileCompleted()) {
      localStorage.setItem("jobId", data.Id);
      setIsApplyForJob(true);
    } else {
      setHeadingAndTextForAlertDialog(
        "Please complete your professional detail to apply, thanks"
      );
      setAlertDialogVisibility(true);
    }
  };

  return isLoading ? (
    <LoadingMask />
  ) : (
    <div className="homepage__container__jobs__projects__penel__container">
      {isRandom ? null : (
        <div className="homepage__container__jobs__projects__penel__container__sidebar">
          <div className="homepage__container__results">
            <FreelancerProjectChecks checked={true} />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
            <FreelancerProjectChecks />
          </div>
        </div>
      )}

      <div
        className="homepage__container__jobs__projects__penel__container__details"
        style={isRandom ? { width: "100%" } : null}
      >
        <div className="homepage__container__jobs__projects__penel__container__details__heading">
          {!data.Type
            ? isFreelancer
              ? data.Title
              : data.JobTitle
            : data.FirstName + " " + data.LastName}
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__feature">
          <div className="homepage__container__result__card__content">
            <div className="homepage__container__result__card__content__entry">
              <Img
                loading="lazy"
                src={timeSvg}
                alt="timeSvg"
                className="homepage__container__result__card__content__entry__svg"
              />
              <div className="homepage__container__result__card__content__entry__text">
                {!data.Type
                  ? data.Deadline
                    ? moment(data.Deadline).format("DD MMM, YYYY")
                    : data.JobTypes
                    ? data.JobTypes.map((item) =>
                        item.JobTypeDetailTitle != undefined
                          ? item.JobTypeDetailTitle
                          : item.LookupDetail.Title
                      ).join(", ")
                    : "Not specified"
                  : data.Availability_Title}
              </div>
            </div>

            {!data.Type ? (
              <>
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={milesSvg}
                    alt="milesSvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />
                  <div className="homepage__container__result__card__content__entry__text">
                    {data.Location}
                  </div>
                </div>
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={IndustrySvg}
                    alt="IndustrySvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />
                  <div className="homepage__container__result__card__content__entry__text">
                    {isOn == "freelancer" ? (
                      data.Industry != null ? (
                        <div>{data.Industry.Title}</div>
                      ) : (
                        "Not specified"
                      )
                    ) : data.Company_Industry_Title != undefined ? (
                      data.Company_Industry_Title != "" ? (
                        <div>{data.Company_Industry_Title}</div>
                      ) : (
                        "Not specified"
                      )
                    ) : data.CompanyProfile.Industry != null ? (
                      <div>{data.CompanyProfile.Industry.Title}</div>
                    ) : (
                      "Not specified"
                    )}
                  </div>
                </div>
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={employeeSvg}
                    alt="employeeSvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />

                  <div className="homepage__container__result__card__content__entry__text">
                    {data.Company_NoOfEmployees ? (
                      getText(data.Company_NoOfEmployees).length > 10 ? (
                        <>{`${getText(data.Company_NoOfEmployees).substring(
                          0,
                          10
                        )}... employees`}</>
                      ) : (
                        <>
                          {getText(data.Company_NoOfEmployees) + " employees"}
                        </>
                      )
                    ) : data.NoOfEmployees != null ? (
                      getText(data.NoOfEmployees).length > 10 ? (
                        <>{`${getText(data.NoOfEmployees).substring(
                          0,
                          10
                        )}... employees`}</>
                      ) : (
                        <>{getText(data.NoOfEmployees) + " employees"}</>
                      )
                    ) : data.CompanyProfile ? (
                      data.CompanyProfile.NoOfEmployees != null ? (
                        data.CompanyProfile.NoOfEmployees
                      ) : (
                        "Not specified"
                      )
                    ) : (
                      "Not specified"
                    )}
                  </div>
                </div>
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={moneySvg}
                    alt="moneySvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />
                  <div className="homepage__container__result__card__content__entry__text">
                    {data.SalaryTypeLookup != undefined
                      ? data.SalaryTypeLookup === "Negotiable"
                        ? data.SalaryTypeLookup
                        : data.SalaryFrom
                        ? data.SalaryTo
                          ? "€ " + data.SalaryFrom + " - € " + data.SalaryTo
                          : "€ " + data.SalaryFrom
                        : 0
                      : data.BudgetTypeLookupDetail === "Negotiable"
                      ? data.BudgetTypeLookupDetail
                      : data.BudgetFrom
                      ? data.BudgetTo
                        ? "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                        : "€ " + data.BudgetFrom
                      : 0}
                  </div>
                </div>
              </>
            ) : null}
          </div>

          <div className="homepage__container__result__card__content__right__buttons">
            {jobStatusApiData ? (
              isJobApplied ? (
                <button
                  title="already applied"
                  className="header__nav__btn btn__secondary"
                  style={{
                    width: "150px",
                    height: "50px",
                    marginBottom: "1em",
                  }}
                >
                  Already Applied
                </button>
              ) : (
                <button
                  title="apply"
                  onClick={() => navigateToApplyJob()}
                  className="header__nav__btn btn__secondary"
                  style={{
                    width: "150px",
                    height: "50px",
                    marginBottom: "1em",
                  }}
                >
                  Apply
                </button>
              )
            ) : null}
            <button
              title="interested / already interested button"
              onClick={() => submitInterested()}
              className={
                isInterested
                  ? "header__nav__btn btn__primary"
                  : "homepage__container__result__card__content__right__buttons__intrested"
              }
              style={{ width: "150px", height: "50px" }}
            >
              {isInterested ? "Already interested" : "Interested"}
              {/* {jobStatusApiData
                ? jobStatusApiData.IsInterested
                  ? "Already Interested"
                  : "Interested"
                : "Interested"} */}
            </button>
          </div>
        </div>
        <div className="homepage__container__result__card__badges">
          {!data.Type
            ? isFreelancer
              ? data.ProjectSkills.map((skill, i) => (
                  <div
                    className="homepage__container__result__card__badge"
                    key={i}
                  >
                    {skill.Title}
                  </div>
                ))
              : data.JobSkills.map((skill, i) => (
                  <div
                    className="homepage__container__result__card__badge"
                    key={i}
                  >
                    {skill.Skill != undefined ? skill.Skill.Title : skill.Title}
                  </div>
                ))
            : data.JobSeekerSkills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Title}
                </div>
              ))}
        </div>
        {!data.Type ? (
          <>
            <div className="homepage__container__jobs__projects__penel__container__details__info">
              <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                {isFreelancer ? "Project" : "Job"} description
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {getText(data.Description)}
              </div>
            </div>
            {data.Requirements != null ? (
              <div className="homepage__container__jobs__projects__penel__container__details__info">
                <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                  {isFreelancer ? "Project" : "Job"} requirements
                </div>
                <div className="homepage__container__jobs__projects__penel__container__details__content">
                  {getText(data.Requirements)}
                </div>
              </div>
            ) : null}
          </>
        ) : (
          <>
            <div className="homepage__container__jobs__projects__penel__container__details__info">
              <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                Experience
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {data.JobSeekerExperiences.map(
                  (experience, experienceIndex) => (
                    <div
                      className="homepage__container__result__card__badge"
                      key={experienceIndex}
                    >
                      {experience.Title}
                    </div>
                  )
                )}
              </div>
            </div>

            <div className="homepage__container__jobs__projects__penel__container__details__info">
              <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                Qualification
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {data.JobSeekerQualifications.map(
                  (qualification, qualificationIndex) => (
                    <div
                      className="homepage__container__result__card__badge"
                      key={qualificationIndex}
                    >
                      {qualification.Title}
                    </div>
                  )
                )}
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
}
