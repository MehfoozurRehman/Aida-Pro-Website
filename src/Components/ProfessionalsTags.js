import React from "react";

export default function ProfessionalsTags({ heading, removeSkillMethod }) {
  return (
    <div className="homepage__container__jobs__projects__penel__container__details__content__profile__card__entry__skills">
      {heading}
      <button
        className="homepage__container__jobs__projects__penel__container__details__content__profile__card__entry__skills__del"
        onClick={() => removeSkillMethod()}
        title="remove skills"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="8.08"
          height="8.08"
          viewBox="0 0 13.08 13.08"
        >
          <g id="cancel-close-svgrepo-com" transform="translate(0 -0.016)">
            <g
              id="Group_1767"
              data-name="Group 1767"
              transform="translate(0 0.016)"
            >
              <path
                id="Path_22069"
                data-name="Path 22069"
                d="M7.981,6.556l4.89-4.891a.717.717,0,0,0,0-1.012L12.443.225a.717.717,0,0,0-1.012,0L6.54,5.115,1.65.225a.717.717,0,0,0-1.012,0L.209.653a.716.716,0,0,0,0,1.012L5.1,6.556.209,11.446a.717.717,0,0,0,0,1.012l.429.428a.717.717,0,0,0,1.012,0L6.54,8l4.89,4.89a.71.71,0,0,0,.506.209h0a.71.71,0,0,0,.506-.209l.429-.428a.717.717,0,0,0,0-1.012Z"
                transform="translate(0 -0.016)"
              />
            </g>
          </g>
        </svg>
      </button>
    </div>
  );
}
