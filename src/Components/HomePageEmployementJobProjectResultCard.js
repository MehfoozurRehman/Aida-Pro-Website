import React, { useEffect, useState } from "react";
import { getText } from "Utils/functions";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import Img from "react-cool-img";

export default function HomePageEmployementJobProjectResultCard({
  checked,
  data,
  showJobDetail,
  isFreelancer,
  setProjectData,
}) {
  const [isSelected, setIsSelected] = useState(false);
  const { JobTitle, JobSkills } = data;

  let Title = JobTitle;
  let Skills = JobSkills;

  if (!Title) {
    if (!data.Title) {
      Title = data.FirstName + " " + data.LastName;
    } else {
      Title = data.Title;
    }
  }

  if (!Skills) {
    if (!data.ProjectSkills) {
      Skills = data.JobSeekerSkills;
    } else {
      Skills = data.ProjectSkills;
    }
  }

  useEffect(() => {
    if (checked) {
      showJobDetail ? showJobDetail(data, isFreelancer) : setProjectData(data);
    }
  }, []);

  return (
    <div
      className="homepage__container__result__card__wrapper"
      style={{
        borderBottom: "1px solid lightgray",
        paddingBottom: ".5em",
        marginBottom: ".5em",
      }}
    >
      <input
        type="radio"
        name="homepage__container__result__card"
        id="homepage__container__result__card"
        className="homepage__container__result__card__input"
        defaultChecked={checked}
        onChange={
          showJobDetail
            ? () => showJobDetail(data, isFreelancer)
            : () => setProjectData(data)
        }
      />
      <div
        className="homepage__container__result__card"
        style={isSelected ? { backgroundColor: "#303043" } : null}
      >
        <div className="homepage__container__result__card__header">
          <div className="homepage__container__result__card__header__icon">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20.4"
              height="17"
              viewBox="0 0 20.4 17"
              fill="currentColor"
            >
              <g id="suitcase" transform="translate(0 -2)">
                <path
                  id="Path_2267"
                  data-name="Path 2267"
                  d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                  transform="translate(-1.2)"
                />
                <path
                  id="Path_2268"
                  data-name="Path 2268"
                  d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                  transform="translate(0 -1.443)"
                />
                <path
                  id="Path_2269"
                  data-name="Path 2269"
                  d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                  transform="translate(0 -0.45)"
                />
              </g>
            </svg>
          </div>
          <div className="homepage__container__result__card__header__content">
            <div
              className="homepage__container__result__card__header__designation"
              style={{ marginBottom: "0.3em" }}
            >
              {/* {data.JobTypeLookupDetail != null &&
                data.JobTypeLookupDetail != ""
                ? data.JobTypeLookupDetail
                : "Not specified"} */}
              {data.JobTypes != null && data.JobTypes.length > 0
                ? data.JobTypes.map((item) => item.JobTypeDetailTitle).join(
                    ", "
                  )
                : "Not specified"}
            </div>
            <div className="homepage__container__result__card__header__name">
              {Title}
            </div>
          </div>
        </div>

        <div className="homepage__container__result__card__content__info text-overflow">
          {getText(data.Description).length > 150 ? (
            <>{`${getText(data.Description).substring(0, 150)}...`}</>
          ) : (
            <>{getText(data.Description)}</>
          )}
        </div>

        <div
          className="homepage__container__result__card__content"
          style={{ marginBottom: -20 }}
        >
          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={IndustrySvg}
              alt="IndustrySvg"
              className="homepage__container__result__card__content__entry__svg"
            />

            <div className="homepage__container__result__card__content__entry__text">
              {`${
                data.Company_Industry_Title != null &&
                data.Company_Industry_Title != ""
                  ? data.Company_Industry_Title
                  : "Not specified"
              }`}
            </div>
          </div>
          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={moneySvg}
              alt="moneySvg"
              className="homepage__container__result__card__content__entry__svg"
            />
            <div className="homepage__container__result__card__content__entry__text">
              {/* {data.NoOfEmployees != null && data.NoOfEmployees != "" ? (
                getText(data.NoOfEmployees).length > 10 ? (
                  <>{`${getText(data.NoOfEmployees).substring(
                    0,
                    10
                  )}... employees`}</>
                ) : (
                  <>{getText(data.NoOfEmployees) + " employees"}</>
                )
              ) : (
                "Not specified"
              )} */}
              {data.SalaryTypeLookup == "Negotiable"
                ? data.SalaryTypeLookup
                : data.SalaryFrom
                ? data.SalaryTo
                  ? "€ " + data.SalaryFrom + " - € " + data.SalaryTo
                  : "€ " + data.SalaryFrom
                : 0}
            </div>
          </div>
        </div>
        <div
          className="homepage__container__result__card__badges"
          style={{ paddingBottom: "0em", overflow: "hidden", marginTop: 15 }}
        >
          {Skills.length > 0
            ? Skills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Skill != undefined ? skill.Skill.Title : skill.Title}
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}
