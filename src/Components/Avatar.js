import React from "react";
import Img from "react-cool-img";
import { tempAvatar } from "Assets";

export default function Avatar({
  userPic,
  onClick,
  style,
  dontShowHover = true,
}) {
  return (
    <>
      {userPic ? (
        <div
          className="avatar__btn"
          onClick={onClick}
          style={style}
          title="avatar"
        >
          <Img
            loading="lazy"
            src={userPic}
            alt="user image"
            className="avatar__img"
          />
        </div>
      ) : (
        <div
          className={
            dontShowHover
              ? "avatar__btn__without__cursor__change"
              : "avatar__btn"
          }
          onClick={() => onClick(dontShowHover)}
          style={style}
        >
          <Img
            loading="lazy"
            src={tempAvatar}
            alt="tempAvatar"
            className="avatar__img"
          />
        </div>
      )}
    </>
  );
}
