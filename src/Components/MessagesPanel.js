import UserContext from "Context/UserContext";
import React, { useContext, useEffect, useState } from "react";
import Message from "./Message";
import firebase from "firebase/app";

export default function MessagesPanel({ setIsMessagePanelOpen }) {
  const user = useContext(UserContext);
  let [notificationData, setNotificationData] = useState(null);

  useEffect(() => {
    fetchNotifications();
    updateAllNotification();
  }, []);

  const fetchNotifications = () => {
    const messageListener = firebase
      .firestore()
      .collection("Notification")
      .doc(
        user.Freelancer != null
          ? `Freelancer_${user.FreelancerId}`
          : user.JobSeeker != null
            ? `Jobseeker_${user.JobSeekerId}`
            : user.CompanyProfile != null
              ? `Company_${user.CompanyId}`
              : null
      )
      .collection("Message")
      .orderBy("createdAt", "desc")
      .onSnapshot((querySnapShot) => {
        const firebaseMessages = querySnapShot.docs.map((doc) => {
          const firebaseData = doc.data();
          const data = {
            _id: doc.id,
            ...firebaseData,
          };
          return data;
        });
        setNotificationData((notificationData = firebaseMessages));
      });
    return () => messageListener();
  };

  const updateAllNotification = () => {
    let DocKey =
      user.Freelancer != null
        ? `Freelancer_${user.FreelancerId}`
        : user.JobSeeker != null
          ? `Jobseeker_${user.JobSeekerId}`
          : user.CompanyProfile != null
            ? `Company_${user.CompanyId}`
            : null;
    firebase
      .firestore()
      .collection("Notification")
      .doc(DocKey)
      .collection("Message")
      .onSnapshot((querySnapShot) => {
        querySnapShot.docs.map((doc) => {
          firebase
            .firestore()
            .collection("Notification")
            .doc(DocKey)
            .collection("Message")
            .doc(doc.id)
            .update({ isRead: true }, { merge: true });
          return;
        });
        return;
      });
  };

  return (
    <div className="header__message__panel animate__animated animate__fadeIn">
      <div className="header__message__panel__header">
        <div className="header__message__panel__header__heading">Messages</div>
        {/* <button title="clear" className="header__message__panel__btn">Clear All</button> */}
      </div>
      <div className="header__message__panel__content">
        {notificationData != null
          ? notificationData.map((item, i) => (
            <Message
              key={i}
              item={item}
              setIsMessagePanelOpen={setIsMessagePanelOpen}
            />
          ))
          : null}
      </div>
    </div>
  );
}
