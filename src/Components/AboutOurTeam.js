import { ourTeam } from "Assets";
import React from "react";
import Img from "react-cool-img";

export default function AboutOurTeam({}) {
  return (
    <div className="homepage__container__qualification homepage__container__qualification__reverse">
      <div className="homepage__container__qualification__right">
        <div className="homepage__container__qualification__heading">
          <span>Our team</span>
        </div>
        <div className="homepage__container__qualification__info">
          We are an experienced and talented team having worked for
          international corporations in IT, engineering, marketing and
          recruitment. All founders share a common passion for AI & Data. This
          experience and expertise is bundled in AIDApro, creating a dedicated
          community where companies and professionals communicate directly,
          share experiences, knowledge, and ask questions.
        </div>
      </div>
      <Img
        loading="lazy"
        src={ourTeam}
        alt="ourTeam"
        className="homepage__container__qualification__left"
      />
    </div>
  );
}

function AboutOurMission({ bestIdea }) {
  return (
    <div className="homepage__container__vacancy">
      <div className="homepage__container__vacancy__content">
        <div className="homepage__container__vacancy__content__heading">
          <span>Our mission</span>
        </div>
        <div className="homepage__container__vacancy__content__info">
          Through AIDApro we can create a global community connecting companies
          and professionals in the fields of AI and Data. AIDApro’s sincere
          commitment to explore and undertake AI and Data initiatives in
          developing countries. Helping local professionals with better careers
          and local poor communities using AI and Data solutions that can
          improve their basic life necessities..
        </div>
      </div>
      <Img
        loading="lazy"
        src={bestIdea}
        alt="bestIdea"
        className="homepage__container__vacancy__img"
      />
    </div>
  );
}

function AboutOurGoals({ ourStorySvg }) {
  return (
    <div
      className="homepage__container__jumbotron__our__story"
      style={{
        margin: "7em 0em",
      }}
    >
      <div className="homepage__container__jumbotron__our__story__container">
        <div className="homepage__container__jumbotron__our__story__container__svg">
          <Img
            loading="lazy"
            src={ourStorySvg}
            alt="ourStorySvg"
            style={{
              width: "100%",
              height: "100%",
            }}
          />
        </div>
        <div className="homepage__container__jumbotron__our__story__container__content">
          <div className="homepage__container__jumbotron__our__story__container__content__heading">
            Our goals
          </div>
          <div className="homepage__container__jumbotron__our__story__container__content__para">
            • Assist our platform professionals in their careers <br /> •
            Develop AI and data initiatives in third world countries in Africa
            and Asia <br /> • Match professionals with the right companies, and
            vice versa <br /> • Knowledge platform for technical and other
            issues
          </div>
        </div>
      </div>
    </div>
  );
}
