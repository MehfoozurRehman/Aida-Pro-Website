import React from "react";

export default function PostingListDropDownChoice({
  label,
  btnBg,
  setSelectedBadgeColor,
  setSelectedLabel,
  statusDataObject,
  setStatusData,
  index,
}) {

  return (
    <button
      onClick={() => {
        setSelectedBadgeColor(btnBg);
        setSelectedLabel(label);
        setStatusData(statusDataObject);
      }}
      className="posting__container__table__list__entry__dropdown__list__entry"
      title="dropdown entry"
    >
      <div
        className="posting__container__table__list__entry__dropdown__list__entry__badge "
        style={{
          background: btnBg,
        }}
      ></div>
      <div className="posting__container__table__list__entry__dropdown__list__entry__text">
        {label}
      </div>
    </button>
  );
}
