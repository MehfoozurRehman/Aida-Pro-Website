import React, { useEffect } from "react";
import PostingDetailsTab from "Components/PostingDetailsTab";
import Img from "react-cool-img";

export default function UserProfileTabs({
  setIsOnContact,
  setIsOnEducation,
  setIsOnProfile,
  setIsOnProjectPortfolio,
  setIsOnWorkExperiance,
  UserActive,
  userInActive,
  isOnProfile,
  WorkActive,
  WorkInactive,
  isOnWorkExperiance,
  ProjectActive,
  ProjectInactive,
  isOnProjectPortfolio,
  EducationActive,
  EducationInactive,
  isOnEducation,
  ContactActive,
  ContactInactive,
  isOnContact,
}) {
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <div
      className="homepage__container__jobs__projects__penel__container__details__tabs"
      id="homepage__container__jobs__projects__penel__container__details__tabs"
    >
      <PostingDetailsTab
        label="Profile"
        onClick={() => {
          setIsOnContact(false);
          setIsOnEducation(false);
          setIsOnProfile(true);
          setIsOnProjectPortfolio(false);
          setIsOnWorkExperiance(false);
        }}
        activeSvg={<Img loading="lazy" src={UserActive} alt="UserActive" />}
        inActiveSvg={
          <Img loading="lazy" src={userInActive} alt="userInActive" />
        }
        defaultChecked={true}
        isChecked={isOnProfile}
      />
      <PostingDetailsTab
        label="Work Experience"
        onClick={() => {
          setIsOnContact(false);
          setIsOnEducation(false);
          setIsOnProfile(false);
          setIsOnProjectPortfolio(false);
          setIsOnWorkExperiance(true);
        }}
        activeSvg={<Img loading="lazy" src={WorkActive} />}
        inActiveSvg={<Img loading="lazy" src={WorkInactive} />}
        isChecked={isOnWorkExperiance}
      />
      <PostingDetailsTab
        label="Project Portfolio"
        onClick={() => {
          setIsOnContact(false);
          setIsOnEducation(false);
          setIsOnProfile(false);
          setIsOnProjectPortfolio(true);
          setIsOnWorkExperiance(false);
        }}
        activeSvg={<Img loading="lazy" src={ProjectActive} />}
        inActiveSvg={<Img loading="lazy" src={ProjectInactive} />}
        isChecked={isOnProjectPortfolio}
      />
      <PostingDetailsTab
        label="Education"
        onClick={() => {
          setIsOnContact(false);
          setIsOnEducation(true);
          setIsOnProfile(false);
          setIsOnProjectPortfolio(false);
          setIsOnWorkExperiance(false);
        }}
        activeSvg={<Img loading="lazy" src={EducationActive} />}
        inActiveSvg={<Img loading="lazy" src={EducationInactive} />}
        isChecked={isOnEducation}
      />
      <PostingDetailsTab
        label="Contact"
        onClick={() => {
          setIsOnContact(true);
          setIsOnEducation(false);
          setIsOnProfile(false);
          setIsOnProjectPortfolio(false);
          setIsOnWorkExperiance(false);
        }}
        activeSvg={<Img loading="lazy" src={ContactActive} />}
        inActiveSvg={<Img loading="lazy" src={ContactInactive} />}
        isChecked={isOnContact}
      />
    </div>
  );
}
