import React from "react";
import WriteComments from "Components/WriteComments";
import CommentWithReply from "Components/CommentWithReply";
import { getText } from "Utils/functions";

export default function DiscussionCard({
  item,
  getWriteCommentAndIdAndData,
  to,
}) {
  const getWriteComment = (value) => {
    getWriteCommentAndIdAndData(value, item.ForumId, item);
  };

  const navigateToDiscussion = () => {
    // navigate(to, { state: { forumId: item.ForumId } })
  };

  return (
    <div onClick={() => navigateToDiscussion()} className="discussion__card">
      <div className="homepage__container__jobs__projects__penel__container__details__heading">
        {item.Subject != "" ? item.Subject : "Subject Name"}
      </div>
      <div className="homepage__container__jobs__projects__penel__container__details__content">
        {getText(item.Description).length > 150 ? (
          <>{`${getText(item.Description).substring(0, 150)}...`}</>
        ) : (
          <>{getText(item.Description)}</>
        )}
      </div>
      <div
        className="homepage__container__result__card__badges"
        style={{ marginBottom: "1em" }}
      >
        {item.ForumTags != null && item.ForumTags.length > 0
          ? item.ForumTags.map((item, i) => {
              return (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {item.Tag.Title}
                </div>
              );
            })
          : null}
      </div>
      <WriteComments
        placeholder="Write Comment"
        getWriteComment={getWriteComment}
      />
      {item.ForumAnswers.map((item, index) => (
        <div
          className="homepage__blog__artical__content__left__messages"
          key={index}
        >
          <CommentWithReply item={item} />
        </div>
      ))}
      {/* <Link to="/" style={{ color: "#303043" }}>
        View All
      </Link> */}
    </div>
  );
}
