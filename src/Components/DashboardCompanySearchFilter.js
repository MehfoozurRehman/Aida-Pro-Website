import React, { useState } from "react";
import SearchFilterPanelBadge from "./SearchFilterPanelBadge";
import Autocomplete from "react-google-autocomplete";
import RangeSelector from "./RangeSelector";

export default function DashboardCompanySearchFilter({
  setIsFilterOpen,
  isFilterOpen,
  isMain,
  keywords,
  searchByTitle,
  searchByFilter,
  title,
  searchBy,
  searchByFreelancer,
  searchByUser,
  searchByRange,
  searchByLocation,
  jobLocation,
  radius,
}) {
  const [titleLocal, setTitleLocal] = useState(title);
  const [employeeSearch, setEmployeeSearch] = useState(searchBy ? true : false);
  const [selectedKeywordId, setSelectedKeywordId] = useState(null);
  const [freelancerSearch, setFreelancerSearch] = useState(
    searchByFreelancer ? true : false
  );
  const [range, setRange] = useState(radius);

  const setStateOfTitle = (e) => {
    setTitleLocal(e.currentTarget.value);
    searchByTitle(e.currentTarget.value);
  };

  const setStateOfTitleFromKeyword = (e, value) => {
    setSelectedKeywordId(e.Id);
    setTitleLocal(value);
    searchByTitle(value);
  };

  const toggleFreelancerSearch = (event) => {
    setFreelancerSearch(event.currentTarget.checked);

    searchByUser(event.currentTarget.checked, employeeSearch);
  };

  const toggleEmployeeSearch = (event) => {
    setEmployeeSearch(event.currentTarget.checked);

    searchByUser(freelancerSearch, event.currentTarget.checked);
  };

  return (
    <div
      className="homepage__container__jumbotron__form animate__animated animate__fadeInUp"
      style={{ borderRadius: "50px", boxShadow: "none" }}
    >
      <div className="homepage__container__jumbotron__form__input">
        <input
          type="text"
          className="homepage__container__jumbotron__form__input__field"
          placeholder="Search"
          value={titleLocal}
          onChange={(e) => {
            setStateOfTitle(e);
          }}
          onKeyDown={(event) => {
            if (event.key === "Enter") searchByFilter();
          }}
        />
        <button
          className="homepage__container__jumbotron__form__button"
          onClick={() => searchByFilter()}
          title="search by filter"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="17.064"
            height="17.064"
            viewBox="0 0 17.064 17.064"
          >
            <g
              id="Icon_feather-search"
              data-name="Icon feather-search"
              transform="translate(1 1)"
            >
              <path
                id="Path_2299"
                data-name="Path 2299"
                d="M17.522,11.011A6.511,6.511,0,1,1,11.011,4.5,6.511,6.511,0,0,1,17.522,11.011Z"
                transform="translate(-4.5 -4.5)"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
              <path
                id="Path_2300"
                data-name="Path 2300"
                d="M28.515,28.515l-3.54-3.54"
                transform="translate(-13.866 -13.866)"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
            </g>
          </svg>
        </button>
      </div>
      <div className="homepage__container__jumbotron__form__filters__dashboard">
        {isMain ? null : (
          <div className="homepage__container__jumbotron__form__filters__role">
            <input
              className="styled-checkbox"
              id="styled-checkbox-jobseeker-role"
              type="checkbox"
              value="Remember"
              name="Remember"
              checked={employeeSearch}
              onClick={(e) => toggleEmployeeSearch(e)}
            />
            <label htmlFor="styled-checkbox-jobseeker-role">Professional</label>
            <input
              className="styled-checkbox"
              id="styled-checkbox-freelancer-role"
              type="checkbox"
              value="Remember"
              name="Remember"
              checked={freelancerSearch}
              onClick={(e) => toggleFreelancerSearch(e)}
            />
            <label htmlFor="styled-checkbox-freelancer-role">Freelancer</label>
          </div>
        )}

        <div className="homepage__container__jumbotron__form__filters__location">
          <Autocomplete
            apiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
            // lang="en"
            value={jobLocation}
            onPlaceSelected={(place) => {
              searchByLocation(place, false);
            }}
            onChange={(place) => {
              searchByLocation(place, true);
            }}
            placeholder="Location"
            className="homepage__container__jumbotron__form__filters__location__select"
          />
          <RangeSelector radius={radius} searchByRange={searchByRange} />
        </div>
        <div className="homepage__container__jumbotron__form__filters__badges">
          {keywords.map((keyword, i) => (
            <SearchFilterPanelBadge
              key={i}
              data={keyword}
              searchThisTitle={setStateOfTitleFromKeyword}
              selectedKeywordId={selectedKeywordId}
            />
          ))}
        </div>
        <div className="dashboard__company__container__left__form__view__filter">
          <button
            className="dashboard__company__container__left__form__view__filter__btn"
            // style={{ background: "#71797E", color: "#ffffff" }}
            type="button"
            onClick={() => {
              !isFilterOpen ? setIsFilterOpen(true) : setIsFilterOpen(false);
            }}
            title="view / hide filter"
          >
            {!isFilterOpen ? "View Filter" : "Hide Filter"}
          </button>
        </div>
      </div>
    </div>
  );
}
