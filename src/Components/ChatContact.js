import React from "react";
import Avatar from "./Avatar";

export default function ChatContact({ chatUsers }) {
  return (
    <div className="messenger__container__sidebar__users__entry">
      <input
        type="radio"
        className="messenger__container__sidebar__users__entry__input"
        name="messenger__container__sidebar__users__entry__input"
      />
      <div className="messenger__container__sidebar__users__entry__wrapper">
        <Avatar />
        <div className="messenger__container__sidebar__users__entry__content">
          <div className="messenger__container__sidebar__users__entry__content__name">
            {chatUsers.FirstName + " " + chatUsers.LastName}
          </div>
          <div className="messenger__container__sidebar__users__entry__content__last__message">
            2h ago
          </div>
        </div>
      </div>
    </div>
  );
}
