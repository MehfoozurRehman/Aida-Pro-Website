import React from "react";
import Img from "react-cool-img";

export default function DashboardHeading({ svg, heading }) {
  return (
    <div className="dashboard__container__heading animate__animated animate__fadeInUp">
      <Img loading="lazy" src={svg} alt={heading} style={{ marginRight: 12 }} />
      {heading}
    </div>
  );
}
