import React from "react";
import { Link } from "@reach/router";
import Img from "react-cool-img";

export default function BlogCard({ id, blogData, onClick }) {
  return (
    <Link
      to="/blog-artical/"
      state={{ blogId: blogData.Id }}
      className="blog__card__container"
    >
      <Img
        loading="lazy"
        src={process.env.REACT_APP_BASEURL + blogData.thumbnail}
        // src={blogCardPic}
        alt="blogCardPic"
        style={{ width: "100%", borderRadius: 14 }}
      />

      <div className="blog__card__content">
        <div className="blog__card__content__top">
          <div className="blog__card__content__top__left">
            <div className="blog__card__content__top__left__heading">
              {blogData.Title}
              {/* blog */}
            </div>
            <div className="blog__card__content__top__left__subHeading">
              {/* Zunaira */}
              {blogData.Author}
            </div>
          </div>
          {/* <div className="blog__card__content__top__right">
            <Avatar />
          </div> */}
        </div>
        <div className="blog__card__content__centered">{/* description */}</div>
        <div className="blog__card__content__buttom">
          {/* <div className="homepage__container__result__card__badges">
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
            <div className="homepage__container__result__card__badge">
              Machine Learning
            </div>
          </div> */}
        </div>
      </div>
    </Link>
  );
}
