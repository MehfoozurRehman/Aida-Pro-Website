import React, { useState } from "react";
import countries__list from "../API/CountriesList.json";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import Select from "react-select";
import Autocomplete from "react-google-autocomplete";
import { isNullOrEmpty } from "Utils/TextUtils";
import moment from "moment";
import { VIEW_DATE_FORMAT } from "Utils/Constants";

export default function InputBox({
  svg,
  placeholder,
  variant,
  type,
  style,
  options,
  isMulti,
  onChange,
  onSelected,
  value,
  name,
  disabled,
  width,
  error,
  errorMessage,
  secureKey,
  minDate,
  maxDate,
  minValue,
  maxValue,
  selectedCountryCode,
  onChangeCountryCode,
  phoneMinLength,
  phoneMaxLength,
  onKeyDown,
  id,
  onBlur,
}) {
  const [isFocused, setIsFocused] = useState(true);
  const [passwordShown, setPasswordShown] = useState(false);
  if (type === "password" && passwordShown === true) {
    type = "text";
  }
  if (variant === "list") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={style && style}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <input
          list="browsers"
          name="browser"
          id="browser"
          placeholder={placeholder}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
          onChange={onChange}
        />
        <datalist id="browsers">
          {countries__list.map((country, i) => (
            <option key={i} value={country.name} />
          ))}
        </datalist>
      </div>
    );
  } else if (variant === "location") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={style && style}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <Autocomplete
          apiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
          // lang="en"
          // language="en"
          options={{
            types: ["(regions)"],
            // fields: [
            //   "address_components",
            //   "geometry.location",
            //   "place_id",
            //   "formatted_address",
            //   "formatted_phone_number",
            //   "international_phone_number",
            //   "name",
            //   "plus_code",
            //   "types",
            //   "url",
            // ],
            // componentRestrictions: { country: "ru" },
          }}
          onPlaceSelected={(place) => {
            onChange = onSelected(place);
          }}
          onChange={onChange}
          value={value}
          placeholder="Location"
          className="homepage__container__jumbotron__signup__wrapper__input__box"
        />
      </div>
    );
  } else if (variant === "datalist") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <input
          list="browsers"
          name="browser"
          id="browser"
          placeholder={placeholder}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
        />
        <datalist id="browsers">
          {options.map((item, i) => (
            <option key={i} value={item.value} />
          ))}
        </datalist>
      </div>
    );
  } else if (variant === "textarea") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form__textarea homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form__textarea"
        }
        style={style && style}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        <CKEditor
          editor={ClassicEditor}
          config={{
            placeholder: placeholder ? placeholder : "Job Description",
          }}
          data={value}
          onChange={(event, editor) => onChange(editor.getData())}
        />
      </div>
    );
  } else if (variant === "textarea-white") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form__textarea homepage__container__jumbotron__signup__wrapper__input__form__textarea__white homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form__textarea homepage__container__jumbotron__signup__wrapper__input__form__textarea__white"
        }
        style={style && style}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        <CKEditor
          editor={ClassicEditor}
          config={{
            placeholder: placeholder ? placeholder : "Job Description",
          }}
          data={value}
          onChange={(event, editor) => onChange(editor.getData())}
        />
      </div>
    );
  } else if (variant === "simple") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form__simple homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form__simple"
        }
        style={style && style}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}

        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <input
          type={type ? type : "text"}
          placeholder={placeholder}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
          onChange={onChange}
          name={name}
          maxLength={maxValue}
          value={value}
        />
      </div>
    );
  } else if (variant === "white") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form__white homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form__white"
        }
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <input
          type={type ? type : "text"}
          placeholder={placeholder}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
          name={name}
          onChange={onChange}
          value={value}
          min={minValue}
          max="5"
          maxLength={maxValue}
          disabled={disabled}
        />
      </div>
    );
  } else if (variant === "date") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={style && style}
        onClick={() => {
          setIsFocused(false);
        }}
        name={name}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        {isFocused ? (
          <div
            className="homepage__container__jumbotron__signup__wrapper__input__form__label"
            style={svg ? style : { left: 18 }}
          >
            {isNullOrEmpty(value)
              ? placeholder
              : moment(value).format(VIEW_DATE_FORMAT)}
          </div>
        ) : null}

        <input
          type={type ? type : "date"}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
          disabled={disabled}
          value={value}
          // defaultValue={value}
          min={minDate}
          max={maxDate}
          onChange={onChange}
          // onKeyDown={(e) => e.preventDefault()}
          name={name}
        />
      </div>
    );
  } else if (variant === "time") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={style && style}
        onClick={() => {
          setIsFocused(false);
        }}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        {isFocused ? (
          <div
            className="homepage__container__jumbotron__signup__wrapper__input__form__label"
            style={svg ? null : { left: 18 }}
          >
            {placeholder}
          </div>
        ) : null}

        <input
          type="time"
          id={id ? id : "time"}
          name={name}
          value={value}
          min={minDate}
          onChange={onChange}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
        />
      </div>
    );
  } else if (variant === "select") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={
          ({ height: "fit-content", minHeight: 45, maxHeight: "fit-content" },
            style)
        }
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <div className="homepage__container__jumbotron__signup__wrapper__input__form__input__wrapper">
          <Select
            options={options}
            placeholder={placeholder}
            isMulti={isMulti}
            onChange={onChange}
            value={value}
          />
        </div>
      </div>
    );
  } else if (variant === "phone") {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
        style={{
          height: "fit-content",
          minHeight: 45,
          ...style,
        }}
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <div
          className="homepage__container__jumbotron__signup__wrapper__input__form__input__wrapper"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Select
            options={options}
            placeholder={"Code"}
            isMulti={isMulti}
            isDisabled={disabled}
            onChange={onChangeCountryCode}
            value={selectedCountryCode}
          />
          <input
            type={type ? type : "text"}
            placeholder={placeholder}
            className="homepage__container__jumbotron__signup__wrapper__input__box"
            name={name}
            onChange={onChange}
            value={value}
            disabled={disabled}
            minLength={phoneMinLength}
            maxLength={phoneMaxLength}
            style={{ maxWidth: width ? width : "50%" }}
          />
        </div>
      </div>
    );
  } else {
    return (
      <div
        className={
          error
            ? "homepage__container__jumbotron__signup__wrapper__input__form homepage__container__jumbotron__signup__wrapper__input__form__error"
            : "homepage__container__jumbotron__signup__wrapper__input__form"
        }
      >
        {error ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
            {errorMessage}
          </div>
        ) : null}
        {svg ? (
          <div className="homepage__container__jumbotron__signup__wrapper__input__form__svg">
            {svg}
          </div>
        ) : null}
        <input
          type={type ? type : "text"}
          placeholder={placeholder}
          className="homepage__container__jumbotron__signup__wrapper__input__box"
          onChange={onChange}
          // onBlur={onBlur}
          onKeyDown={onKeyDown}
          value={value}
          maxLength={maxValue}
          name={name}
        />
        {secureKey ? (
          <button
            type="button"
            tabindex="-1"
            className="homepage__container__jumbotron__signup__wrapper__password__btn"
            onClick={() => {
              passwordShown ? setPasswordShown(false) : setPasswordShown(true);
            }}
            title="show / hide password"
          >
            {passwordShown ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={15}
                height={15}
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-eye"
              >
                <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                <circle cx="12" cy="12" r="3"></circle>
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width={15}
                height={15}
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-eye-off"
              >
                <path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path>
                <line x1="1" y1="1" x2="23" y2="23"></line>
              </svg>
            )}
          </button>
        ) : null}
      </div>
    );
  }
}
