import { useEffect } from "react";
import { X } from "react-feather";
import ImageGallery from "react-image-gallery";

const images = [
  {
    original: "https://picsum.photos/id/1018/1000/600/",
    thumbnail: "https://picsum.photos/id/1018/250/150/",
  },
  {
    original: "https://picsum.photos/id/1015/1000/600/",
    thumbnail: "https://picsum.photos/id/1015/250/150/",
  },
  {
    original: "https://picsum.photos/id/1019/1000/600/",
    thumbnail: "https://picsum.photos/id/1019/250/150/",
  },
];

export default function Gallery({ onClose }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <div className="gallery__wrapper">
      <button
        className="gallery__wrapper__close"
        onClick={() => {
          onClose(false);
        }}
      >
        <X size={34} color="currentColor" strokeWidth={1.5} />
      </button>
      <ImageGallery items={images} />
    </div>
  );
}
