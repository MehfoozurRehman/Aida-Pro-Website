import React, { useContext, useState } from "react";
import { useDispatch } from "react-redux";
import { superToken } from "../API/Api";
import { SET_APP_LOADING } from "../Redux/constants";
import { CustomInfo } from "Screens/Toasts";
import firebase from "firebase/app";
import UserContext from "Context/UserContext";
import { changeStateOfUserOffline } from "Utils/common";

export default function Logout({}) {
  const [isUserLogedIn, setIsUserLogedIn] = useState(false);
  const user = useContext(UserContext);

  let dispatch = useDispatch();

  const logout = async () => {
    try {
      dispatch({
        type: "USER_LOGOUT",
      });
      window.localStorage.removeItem("userId");
      window.localStorage.removeItem("token");
      changeStateOfUserOffline(user.FirebaseId);
      superToken()
        .then(({ data }) => {
          if (data.access_token) {
            // CustomInfo("Logged out successfully");
            localStorage.setItem("token", data.access_token);
            setIsUserLogedIn(false);
            dispatch({
              type: SET_APP_LOADING,
              payload: { isLoading: false },
            });
            window.location.href = "/";
          }
        })
        .catch((err) => {
          // console.log("Error", err);
          dispatch({
            type: SET_APP_LOADING,
            payload: { isLoading: false },
          });
        });
    } catch (e) {
      // console.log(e);
    }
  };

  return (
    <button
      className="header__nav__btn btn__secondary"
      onClick={() => {
        logout();
      }}
      style={{
        background: "#A7B8C3",
        boxShadow: "none",
        marginRight: 0,
      }}
      title="logout"
    >
      Logout
    </button>
  );
}
