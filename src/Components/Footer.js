import React, { useEffect, useState } from "react";
import { Link } from "@reach/router";
import { newsletter } from "API/Api";
import { CustomError, CustomSuccess } from "Screens/Toasts";
import Img from "react-cool-img";
import { isNullOrEmpty } from "Utils/TextUtils";
import { isInvalidEmail } from "Utils/Validations";
import { useSelector } from "react-redux";
import { aidaLogo } from "Assets";

export default function Footer() {
  const { user } = useSelector((state) => state.user);
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState(
    Object.keys(user).length > 0 ? user.UserName : ""
  );

  const [loggedInUser, setLoggedInUser] = useState("");

  useEffect(() => {
    if (Object.keys(user).length > 0) {
      if (user.Freelancer != null) {
        setLoggedInUser("Freelancer");
      } else if (user.JobSeeker != null) {
        setLoggedInUser("Professional");
      } else if (user.CompanyProfile != null) {
        setLoggedInUser("Company");
      }
    }
  });

  //validation
  var [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");

  const handleChangeValues = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else setEmailErrorMessageAndVisibility("");
    setEmail(event.currentTarget.value);
  };
  const setEmailErrorMessageAndVisibility = (text) => {
    setEmailError((emailError = !isNullOrEmpty(text)));
    setEmailErrorMessage(text);
  };
  const sendcontactForm = () => {
    if (isNullOrEmpty(email))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(email))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else {
      let data = {
        Email: email,
      };
      newsletter(data)
        .then(({ data }) => {
          if (data.success) {
            setEmail("");
            //CustomSuccess("Subscribed");
          } else {
            CustomError(data.errors);
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
        });
    }
  };

  return (
    <div className="footer">
      <div className="footer__wrapper">
        <div className="footer__wrapper__col">
          <Link to="/" refresh="true" className="header__logo">
            <Img loading="lazy" src={aidaLogo} alt="aidaLogo" />
          </Link>
        </div>
        <div className="footer__wrapper__col footer__wrapper__col__reverse">
          <div className="footer__wrapper__col__heading">Pages</div>
          <div className="footer__wrapper__col__links">
            <Link
              to="/about-us"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
            >
              About us
            </Link>
            <Link
              to="/blog"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
            >
              Blog
            </Link>
            <Link
              to="/contact"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
            >
              Contact
            </Link>
            <Link
              to="/faq"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
              state={{ loggedInUser: loggedInUser }}
            >
              FAQ
            </Link>
            <Link
              to="/terms-conditions"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
            >
              Terms & Conditions
            </Link>
            <Link
              to="/privacy-policy"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="footer__wrapper__col__links__entry"
            >
              Privacy Policy
            </Link>
          </div>
        </div>
        <div className="footer__wrapper__col footer__wrapper__col__reverse">
          <div className="footer__wrapper__col__heading">Socials</div>
          <div className="footer__wrapper__col__links">
            <a
              href="https://www.linkedin.com/company/aidapro"
              target="_Blank"
              className="footer__wrapper__col__links__entry"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="17.316"
                height="17.316"
                viewBox="0 0 17.316 17.316"
              >
                <path
                  id="iconmonstr-linkedin-3"
                  d="M13.709,0H3.608A3.608,3.608,0,0,0,0,3.608v10.1a3.608,3.608,0,0,0,3.608,3.608h10.1a3.608,3.608,0,0,0,3.608-3.608V3.608A3.608,3.608,0,0,0,13.709,0ZM5.772,13.709H3.608V5.772H5.772ZM4.69,4.857A1.273,1.273,0,1,1,5.953,3.584,1.268,1.268,0,0,1,4.69,4.857Zm9.74,8.852H12.266V9.665c0-2.43-2.886-2.246-2.886,0v4.043H7.215V5.772H9.38V7.046A2.744,2.744,0,0,1,14.43,8.832Z"
                  fill="currentColor"
                />
              </svg>
              LinkedIn
            </a>
            <a
              href="https://www.facebook.com/aidaprofessional/"
              target="_Blank"
              className="footer__wrapper__col__links__entry"
            >
              <svg
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 50 50"
                width="50px"
                height="50px"
              >
                <path d="M41,4H9C6.24,4,4,6.24,4,9v32c0,2.76,2.24,5,5,5h32c2.76,0,5-2.24,5-5V9C46,6.24,43.76,4,41,4z M37,19h-2c-2.14,0-3,0.5-3,2 v3h5l-1,5h-4v15h-5V29h-4v-5h4v-3c0-4,2-7,6-7c2.9,0,4,1,4,1V19z" />
              </svg>
              Facebook
            </a>
            <a
              href="https://www.instagram.com/aidaprofessionals/"
              target="_Blank"
              className="footer__wrapper__col__links__entry"
            >
              <svg
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 50 50"
                width="50px"
                height="50px"
              >
                <path d="M 16 3 C 8.83 3 3 8.83 3 16 L 3 34 C 3 41.17 8.83 47 16 47 L 34 47 C 41.17 47 47 41.17 47 34 L 47 16 C 47 8.83 41.17 3 34 3 L 16 3 z M 37 11 C 38.1 11 39 11.9 39 13 C 39 14.1 38.1 15 37 15 C 35.9 15 35 14.1 35 13 C 35 11.9 35.9 11 37 11 z M 25 14 C 31.07 14 36 18.93 36 25 C 36 31.07 31.07 36 25 36 C 18.93 36 14 31.07 14 25 C 14 18.93 18.93 14 25 14 z M 25 16 C 20.04 16 16 20.04 16 25 C 16 29.96 20.04 34 25 34 C 29.96 34 34 29.96 34 25 C 34 20.04 29.96 16 25 16 z" />
              </svg>
              Instagram
            </a>
            <a
              href="https://twitter.com/aidaprofession"
              target="_Blank"
              className="footer__wrapper__col__links__entry"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="17.063"
                height="13.84"
                viewBox="0 0 17.063 13.84"
              >
                <path
                  id="Path_2302"
                  data-name="Path 2302"
                  d="M43.4,15.84a9.865,9.865,0,0,0,9.953-9.953V5.413a7.705,7.705,0,0,0,1.706-1.8,7.869,7.869,0,0,1-1.991.569,3.688,3.688,0,0,0,1.517-1.9,8.7,8.7,0,0,1-2.18.853A3.385,3.385,0,0,0,49.849,2a3.561,3.561,0,0,0-3.507,3.507,1.848,1.848,0,0,0,.095.758,9.8,9.8,0,0,1-7.2-3.7,3.63,3.63,0,0,0-.474,1.8,3.766,3.766,0,0,0,1.517,2.939,3.2,3.2,0,0,1-1.611-.474h0a3.464,3.464,0,0,0,2.844,3.412,2.922,2.922,0,0,1-.948.095,1.613,1.613,0,0,1-.664-.095,3.591,3.591,0,0,0,3.318,2.465,7.157,7.157,0,0,1-4.36,1.517A2.624,2.624,0,0,1,38,14.133a8.947,8.947,0,0,0,5.4,1.706"
                  transform="translate(-38 -2)"
                  fill="currentColor"
                  fillRule="evenodd"
                />
              </svg>
              Twitter
            </a>
          </div>
        </div>
        <div className="footer__wrapper__col ">
          <div className="footer__wrapper__col__heading">
            Subscribe to Newsletter
          </div>
          <div
            className={
              emailError
                ? "footer__wrapper__col__form homepage__container__jumbotron__signup__wrapper__input__form__error"
                : "footer__wrapper__col__form"
            }
          >
            <div className="homepage__container__jumbotron__signup__wrapper__input__form__error__message">
              {emailErrorMessage}
            </div>
            <input
              type="text"
              className="footer__wrapper__col__form__input "
              placeholder="Enter email"
              name="email"
              value={email}
              onChange={(e) => handleChangeValues(e)}
            />
            <button
              title="subscribe"
              className="footer__wrapper__col__form__button"
              onClick={() => sendcontactForm()}
            >
              Subscribe
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
