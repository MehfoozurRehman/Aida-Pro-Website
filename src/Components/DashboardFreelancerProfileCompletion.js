import React from "react";
import { Link } from "@reach/router";
import { X } from "react-feather";

export default function DashboardFreelancerProfileCompletion({
  profilePercentage,
  isPersonelDetailCompleted,
  isProfessionalDetailCompleted,
  isPortfolioCompleted,
}) {
  return (
    <div className="dashboard__company__container__top">
      <div className="dashboard__company__container__top__close">
        <X size={20} color="currentColor" />
      </div>
      <div className="dashboard__company__container__top__percentage">
        {profilePercentage}%
      </div>
      <div className="dashboard__company__container__top__content">
        <div className="dashboard__company__container__top__content__heading">
          {profilePercentage == 100
            ? "Thanks for completing your profile."
            : "Your profile is not complete click below to complete."}
        </div>
        <div className="edit__buttons__dashboard">
          {!isPersonelDetailCompleted() ? (
            <Link
              to="/home-freelancer/personal-details-preview"
              className="header__nav__btn btn__primary"
              style={{
                height: 35,
                fontSize: 13,
                background: "#71797E",
                boxShadow: "none",
              }}
            >
              Personal
            </Link>
          ) : null}
          {!isProfessionalDetailCompleted() ? (
            <Link
              to="/home-freelancer/professional-details"
              className="header__nav__btn btn__primary"
              style={{
                height: 35,
                fontSize: 13,
                background: "#71797E",
                boxShadow: "none",
              }}
            >
              Professional
            </Link>
          ) : null}
          {!isPortfolioCompleted() ? (
            <Link
              to="/home-freelancer/project"
              className="header__nav__btn btn__primary"
              style={{
                height: 35,
                fontSize: 13,
                background: "#71797E",
                boxShadow: "none",
              }}
            >
              Projects
            </Link>
          ) : null}
        </div>
      </div>
    </div>
  );
}
