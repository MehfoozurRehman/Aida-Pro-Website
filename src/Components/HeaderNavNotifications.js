import React from "react";
import NotificationPanel from "./NotificationPanel";
import OutsideClickHandler from "react-outside-click-handler";

export default function HeaderNavNotifications({
  isNotificationPanelOpen,
  setIsNotificationPanelOpen,
  setUnreadNotificationLength,
  setIsMessagePanelOpen,
  setIsLanguagePanelOpen,
  setIsMenuOpen,
  setNotificationIsReadTrue,
  unreadNotificationLength,
  onClickOutsideNotifications,
  notificationData,
  updateNotificationDataAcceptReject,
}) {
  return (
    <>
      <button
        title="open / close notification panel"
        className="header__nav__icon__btn"
        onClick={() => {
          if (!isNotificationPanelOpen) {
            setIsNotificationPanelOpen(true);
            setUnreadNotificationLength(null);
            setIsMessagePanelOpen(false);
            setIsLanguagePanelOpen(false);

            if (window.innerWidth < 800) {
              setIsMenuOpen(false);
            }

            setNotificationIsReadTrue();
          } else {
            setIsNotificationPanelOpen(false);
          }
        }}
      >
        {unreadNotificationLength == null ? null : (
          <div className="header__nav__icon__btn__badge">
            {unreadNotificationLength.length}
          </div>
        )}
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="26.156"
          height="31.388"
          viewBox="0 0 26.156 31.388"
        >
          <defs>
            <linearGradient
              id="linear-gradient"
              x1="0.5"
              x2="0.5"
              y2="1"
              gradientUnits="objectBoundingBox"
            >
              <stop offset="0" stopColor="#0ee1a3" />
              <stop offset="1" stopColor="#0ca69d" />
            </linearGradient>
          </defs>
          <g id="bell" transform="translate(-2)">
            <path
              id="Path_3920"
              data-name="Path 3920"
              d="M27.344,22.119a8.76,8.76,0,0,1-3.111-6.7V11.77a9.162,9.162,0,0,0-7.847-9.05V1.308a1.308,1.308,0,1,0-2.616,0V2.72a9.161,9.161,0,0,0-7.847,9.05v3.646A8.769,8.769,0,0,1,2.8,22.13a2.288,2.288,0,0,0,1.488,4.027H25.868a2.289,2.289,0,0,0,1.477-4.037Z"
              transform="translate(0 0)"
              fill="url(#linear-gradient)"
            />
            <path
              id="Path_3921"
              data-name="Path 3921"
              d="M13.131,24.923A4.912,4.912,0,0,0,17.936,21H8.326A4.912,4.912,0,0,0,13.131,24.923Z"
              transform="translate(1.947 6.464)"
              fill="url(#linear-gradient)"
            />
          </g>
        </svg>
      </button>
      {isNotificationPanelOpen ? (
        <OutsideClickHandler
          display="contents"
          onOutsideClick={onClickOutsideNotifications}
        >
          <NotificationPanel
            notificationData={notificationData}
            updateNotificationDataAcceptReject={
              updateNotificationDataAcceptReject
            }
            setIsNotificationPanelOpen={setIsNotificationPanelOpen}
          />
        </OutsideClickHandler>
      ) : null}
    </>
  );
}
