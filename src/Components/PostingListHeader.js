import React from "react";

export default function PostingListHeader({}) {
  return (
    <div className="posting__container__table__list__wrapper__header">
      <div className="posting__container__table__list__wrapper__header__entry">
        Title
      </div>
      <div className="posting__container__table__list__wrapper__header__entry">
        APPLICANTS
      </div>
      <div className="posting__container__table__list__wrapper__header__entry">
        INTERESTED
      </div>
      <div className="posting__container__table__list__wrapper__header__entry">
        VISITORS
      </div>
      <div className="posting__container__table__list__wrapper__header__entry">
        CREATED
      </div>
      <div className="posting__container__table__list__wrapper__header__entry">
        STATUS
      </div>
      <div
        style={{
          opacity: 0,
        }}
        className="posting__container__table__list__wrapper__header__entry"
      >
        Delete
      </div>
    </div>
  );
}
