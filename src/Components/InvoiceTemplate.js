import React from "react";
import {
  Document,
  Page,
  Text,
  View,
  PDFViewer,
  Image,
} from "@react-pdf/renderer";
import { InvoiceLogo } from "Assets";
import EntryLine from "Components/EntryLine";
import Summary from "Components/Summary";
import Checkout from "Components/Checkout";

// Create Document Component
const InvoiceTemplate = () => (
  <PDFViewer
    style={{
      width: "100%",
      height: "95%",
      border: "none",
      borderRadius: 10,
      marginTop: 35,
    }}
  >
    <Document>
      <Page size="A4" style={{ backgroundColor: "#F8F8F8" }}>
        <View style={{ paddingHorizontal: 30 }}>
          <View
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 10,
            }}
          >
            <View
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                width: 100,
                height: 30,
              }}
            >
              <Image source={InvoiceLogo} />
            </View>
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              width: "100%",
              marginTop: 40,
            }}
          >
            <View
              style={{
                display: "flex",
                flexDirection: "column",
                width: "40%",
              }}
            >
              <EntryLine Heading="Company Name" subHeading="DSME Global" />
              <EntryLine Heading="Person Name" subHeading="Dayyan Shahid" />
              <EntryLine Heading="Phone" subHeading="92 545 6565565" />
              <EntryLine Heading="CNIC" subHeading="334305 4565 75677" />
              <EntryLine Heading="Sales Person" subHeading="Ammar Haider" />
            </View>
            <View
              style={{
                display: "flex",
                flexDirection: "column",
                width: "40%",
              }}
            >
              <EntryLine Heading="Date:" subHeading="15th July 2021" />
              <EntryLine Heading="Order Id:" subHeading="23204972318" />
            </View>
          </View>
          <View
            style={{
              width: "100%",
              height: 150,
              backgroundColor: "#fff",
              borderRadius: 10,
              marginTop: 20,
              padding: 20,
            }}
          >
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                width: "100%",
              }}
            >
              <Text
                style={{
                  fontSize: 11,
                  color: "#0DCBA0",
                  fontWeight: "bold",
                  width: "40%",
                }}
              >
                Product Name
              </Text>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 11,
                    color: "#0DCBA0",
                    fontWeight: "bold",
                    width: "20%",
                  }}
                >
                  Qty
                </Text>
                <Text
                  style={{
                    fontSize: 11,
                    color: "#0DCBA0",
                    fontWeight: "bold",
                    width: "20%",
                  }}
                >
                  Unit Price
                </Text>
                <Text
                  style={{
                    fontSize: 11,
                    color: "#0DCBA0",
                    fontWeight: "bold",
                    width: "20%",
                  }}
                >
                  Total
                </Text>
              </View>
            </View>
            <View
              style={{
                width: "100%",
                height: 1,
                backgroundColor: "#2C4C4C",
                borderRadius: 2,
                marginVertical: 6,
              }}
            ></View>
            <Summary />
            <Summary />
            <Summary />
            <View style={{ marginTop: 10 }}>
              <Checkout />
              <Checkout />
              <Checkout />
            </View>
          </View>
          <Text style={{ color: "#2C4C4C", fontSize: 10, marginTop: 8 }}>
            This is a system generated purchase order and does not require any
            signature / stamps.
          </Text>
        </View>
        <View
          style={{
            width: "100%",
            borderTopLeftRadius: 40,
            borderTopRightRadius: 40,
            height: 230,
            marginTop: 30,
            backgroundColor: "#ECECEC",
            display: "flex",
            flexDirection: "column",
            paddingHorizontal: 30,
            paddingVertical: 20,
          }}
        >
          <Text
            style={{
              textDecoration: "underline",
              fontSize: 11,
              color: "#2C4C4C",
              marginBottom: 8,
            }}
          >
            Payment Policy
          </Text>
          <Text style={{ color: "#2C4C4C", fontSize: 10 }}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum
          </Text>
          <Text
            style={{
              textDecoration: "underline",
              fontSize: 11,
              color: "#2C4C4C",
              marginBottom: 6,
              marginTop: 18,
            }}
          >
            Payment Office
          </Text>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 4,
            }}
          >
            <Text
              style={{
                marginRight: 20,
                width: "15%",
                fontSize: 11,
                color: "#2C4C4C",
                fontWeight: "bold",
              }}
            >
              Arnhem Office
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontSize: 10,
                width: "80%",
                color: "#2C4C4C",
              }}
            >
              Meander 251, 6825MC Arnhem, Netherlands
            </Text>
          </View>
          <View
            style={{
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 5,
              width: "100%",
            }}
          >
            <Text
              style={{
                marginRight: 20,
                width: "15%",
                fontSize: 11,
                color: "#2C4C4C",
                fontWeight: "bold",
              }}
            >
              Phone #
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontSize: 10,
                width: "20%",
                color: "#2C4C4C",
              }}
            >
              +92 545 6565565
            </Text>
            <Text
              style={{
                marginRight: 20,
                width: "10%",
                fontSize: 11,
                color: "#2C4C4C",
                fontWeight: "bold",
              }}
            >
              Tel #
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontSize: 10,
                width: "22%",
                color: "#2C4C4C",
              }}
            >
              +92 545 6565565
            </Text>
            <Text
              style={{
                marginRight: 20,
                width: "10%",
                fontSize: 11,
                color: "#2C4C4C",
                fontWeight: "bold",
              }}
            >
              Email
            </Text>
            <Text
              style={{
                marginRight: 10,
                fontSize: 10,
                width: "25%",
                color: "#2C4C4C",
              }}
            >
              accounts@dsme.com
            </Text>
          </View>
        </View>
      </Page>
    </Document>
  </PDFViewer>
);

export default InvoiceTemplate;
