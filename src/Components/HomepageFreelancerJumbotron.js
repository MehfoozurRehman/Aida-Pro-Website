import React from "react";
import Img from "react-cool-img";
import _ from "lodash";

export default function HomepageFreelancerJumbotron({
  user,
  setIsLoginOpen,
  mainsvgcolored3,
}) {
  return (
    <section id="homeemploymentfindjob">
      <div className="homepage__container__jumbotron">
        <div className="homepage__container__jumbotron__wrapper">
          <div className="homepage__container__jumbotron__left">
            <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
              <span>Join our freelance community</span>
            </div>
            <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft">
              Find your next AI or Data project! Grow your network and portfolio
              with AIDApro. Free without any obligations.
            </div>
            <div
              onClick={() => {
                if (user.Id !== undefined)
                  window.location.href = "/home-freelancer";
                else setIsLoginOpen(true);
              }}
              className="header__nav__btn btn__primary animate__animated animate__fadeInRight"
              style={{
                width: 150,
                height: 50,
              }}
            >
              Find projects
            </div>
          </div>
          <Img
            loading="lazy"
            src={mainsvgcolored3}
            alt="mainsvgcolored3"
            className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          />
        </div>
      </div>
    </section>
  );
}
