import React from "react";
import { Helmet } from "react-helmet";

export default function Head({ title, description }) {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{title}</title>
      <link rel="description" href={description} />
    </Helmet>
  );
}
