import React from "react";

export default function PostingFilterTab({
  defaultChecked,
  label,
  tabGradiant,
  onClick,
}) {
  return (
    <div className="posting__container__header__filter animate__animated animate__fadeInLeft">
      <input
        type="radio"
        defaultChecked={defaultChecked}
        onChange={onClick}
        name="posting__container__header__filter__input"
        className="posting__container__header__filter__input"
      />
      <div className="posting__container__header__filter__content">
        {tabGradiant === null ? null : (
          <div
            className="posting__container__header__filter__content__badge "
            style={{ background: tabGradiant }}
          ></div>
        )}
        <div className="posting__container__header__filter__content__text">
          {label}
        </div>
      </div>
    </div>
  );
}
