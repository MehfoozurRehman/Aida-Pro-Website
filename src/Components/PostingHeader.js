import React from "react";
import { PostingFilterTab } from "Components";

export default function PostingHeader({
  handleTabChange,
  isJobsShow,
  changeCheckBox,
  isJProjectsShow,
}) {
  return (
    <div className="posting__container__header">
      <div className="posting__container__header__left">
        <PostingFilterTab
          tabGradiant={null}
          label="All"
          defaultChecked={true}
          onClick={(e) => handleTabChange("All")}
        />
        <PostingFilterTab
          tabGradiant="linear-gradient(45deg,#0EE1A3,#0CA69D)"
          label="Live"
          defaultChecked={false}
          onClick={(e) => handleTabChange("Live")}
        />
        <PostingFilterTab
          tabGradiant="linear-gradient(45deg, #f6a938, #f5833c)"
          label="Drafts"
          defaultChecked={false}
          onClick={(e) => handleTabChange("Draft")}
        />
        <PostingFilterTab
          tabGradiant="linear-gradient(45deg, #ffdd00, #e3eb00)"
          label="Hold"
          defaultChecked={false}
          onClick={(e) => handleTabChange("Hold")}
        />
        <PostingFilterTab
          tabGradiant="linear-gradient(45deg, #DE3F3F, #FF6161)"
          label="Closed"
          defaultChecked={false}
          onClick={(e) => handleTabChange("Closed")}
        />
      </div>
      <div className="homepage__container__jumbotron__form__filters__role animate__animated animate__fadeInRight">
        <input
          className="styled-checkbox"
          id="styled-checkbox-freelancer-role"
          type="checkbox"
          value="Remember"
          name="job"
          checked={isJobsShow}
          onChange={(e) => changeCheckBox(e)}
        />
        <label
          style={{
            marginRight: "1em",
          }}
          htmlFor="styled-checkbox-freelancer-role"
        >
          Jobs
        </label>
        <input
          className="styled-checkbox"
          id="styled-checkbox-jobseeker-role"
          type="checkbox"
          value="Remember"
          name="project"
          checked={isJProjectsShow}
          onChange={(e) => changeCheckBox(e)}
        />
        <label htmlFor="styled-checkbox-jobseeker-role">Projects</label>
      </div>
    </div>
  );
}
