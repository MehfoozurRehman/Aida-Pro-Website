import React from "react";

export default function Plan1({ data, handlePlanData }) {
  return (
    <div className="plans__container__row__main__card animate__animated animate__fadeIn">
      <div className="plans__container__row__main__card__name">Plan 1</div>
      <div className="plans__container__row__main__card__heading">
        {data && data[0] && data[0].Title && data[0].Title}
      </div>
      <div className="plans__container__row__main__card__content">
        {data &&
          data[0] &&
          data[0].PlanDetails &&
          data[0].PlanDetails.map((e) => (
            <button
              className="plans__container__row__main__card__content__entry animate__animated animate__fadeInLeft"
              onClick={() => handlePlanData(e)}
              title="plan"
            >
              <div className="plans__container__row__main__card__content__entry__heading">
                {e.Title}
              </div>
              <div className="plans__container__row__main__card__content__entry__sub__heading">
                {e.Description}
              </div>
            </button>
          ))}
      </div>
    </div>
  );
}
