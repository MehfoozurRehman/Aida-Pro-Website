import React, { useState } from "react";

export default function TrendingTopicsCard({ checked }) {
  const [isSelected, setIsSelected] = useState(false);
  return (
    <div className="homepage__container__result__card__wrapper">
      <input
        type="radio"
        name="homepage__container__result__card"
        id="homepage__container__result__card"
        className="homepage__container__result__card__input"
        defaultChecked={checked}
      />
      <div
        onClick={() => {}}
        className="homepage__container__result__card"
        style={isSelected ? { backgroundColor: "#303043" } : null}
      >
        <div className="homepage__container__result__card__header">
          <div
            className="homepage__container__result__card__header__content"
            style={{
              marginBottom: -10,
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              width: "100%",
            }}
          >
            <div className="homepage__container__result__card__header__name">
              Topic name
            </div>
            <div
              className="homepage__container__result__card__header__designation"
              style={{ marginBottom: "0.3em", fontSize: 14 }}
            >
              200 posts
            </div>
          </div>
        </div>
        <div className="homepage__container__result__card__content__info">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book.
        </div>
        <div
          className="homepage__container__result__card__badges"
          style={{ paddingBottom: "0em", overflow: "hidden" }}
        >
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
          <div className="homepage__container__result__card__badge">
            Machine Learning
          </div>
        </div>
      </div>
    </div>
  );
}
