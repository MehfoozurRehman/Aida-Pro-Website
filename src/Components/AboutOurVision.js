import { ourVision } from "Assets";
import React from "react";
import Img from "react-cool-img";

export default function AboutOurVision({}) {
  return (
    <div className="homepage__container__qualification">
      <Img
        loading="lazy"
        src={ourVision}
        alt="ourVision"
        className="homepage__container__qualification__left"
      />
      <div className="homepage__container__qualification__right">
        <div className="homepage__container__qualification__heading">
          <span>Our vision</span>
        </div>
        <div className="homepage__container__qualification__info">
          We strongly believe AI & Data developments can contribute to a better
          and more sustainable world.
        </div>
      </div>
    </div>
  );
}
