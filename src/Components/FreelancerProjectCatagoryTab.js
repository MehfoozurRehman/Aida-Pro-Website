import React from "react";

export default function FreelancerProjectCatagoryTab({ defaultChecked }) {
  return (
    <div className="freelancer__project__container__catagory__filter">
      <input
        type="radio"
        name="freelancer__project__container__catagory__filter__input"
        id=""
        defaultChecked={defaultChecked}
        className="freelancer__project__container__catagory__filter__input"
      />
      <div className="freelancer__project__container__catagory__filter__content">
        Catagory Here
      </div>
    </div>
  );
}
