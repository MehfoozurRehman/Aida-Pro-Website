import React from "react";
import { Avatar } from "Components";
import moment from "moment";

export default function CoffeeCornerDiscussionCard({
  index,
  getText,
  item,
  navigate,
  shareQuestion,
}) {
  return (
    <div className="coffee__corner__new__discussion__card" key={index}>
      {/* <div className="coffee__corner__new__discussion__card__left">
        <button
          className="coffee__corner__new__discussion__card__left__vote__up"
          title="vote up"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-chevron-up"
          >
            <polyline points="18 15 12 9 6 15"></polyline>
          </svg>
        </button>
        <div className="coffee__corner__new__discussion__card__left__no__of__vote">
          2323
        </div>
        <button
          className="coffee__corner__new__discussion__card__left__vote__down"
          title="vote down"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-chevron-down"
          >
            <polyline points="6 9 12 15 18 9"></polyline>
          </svg>
        </button>
      </div> */}

      <div className="coffee__corner__new__discussion__card__right">
        <div className="coffee__corner__new__discussion__card__right__header">
          <Avatar
            userPic={
              item.User != null
                ? item.User.Freelancer != null
                  ? item.User.Freelancer.ProfilePicture != ""
                    ? process.env.REACT_APP_BASEURL.concat(
                        item.User.Freelancer.ProfilePicture
                      )
                    : null
                  : item.User.JobSeeker != null &&
                    item.User.JobSeeker.ProfilePicture != ""
                  ? item.User.JobSeeker.ProfilePicture != ""
                    ? process.env.REACT_APP_BASEURL.concat(
                        item.User.JobSeeker.ProfilePicture
                      )
                    : null
                  : item.User.CompanyProfile != null
                  ? item.User.CompanyProfile.LogoUrl != ""
                    ? process.env.REACT_APP_BASEURL.concat(
                        item.User.CompanyProfile.LogoUrl
                      )
                    : null
                  : null
                : null
            }
          />
          <div className="coffee__corner__new__discussion__card__right__header__name">
            {item.User != null ? item.User.LoginName : "Not specified"}
          </div>
          {/* <div className="coffee__corner__new__discussion__card__right__header__posted">
            Posted by{" "}
            <span>
              {item.User != null ? item.User.LoginName : "Not specified"}
            </span>
          </div> */}
          <div className="coffee__corner__new__discussion__card__right__header__time">
            Asked <span>{moment(item.PostedOn).startOf("day").fromNow()}</span>
          </div>
        </div>
        <div className="coffee__corner__new__discussion__card__right__heading">
          {item.Subject}
        </div>
        <div className="coffee__corner__new__discussion__card__right__header__info">
          {getText().length > 150 ? (
            <>{`${getText(item.Description).substring(0, 150)}...`}</>
          ) : (
            <>{getText(item.Description)}</>
          )}
        </div>
        <div className="homepage__container__result__card__badges">
          {item.ForumTags.map((forum, i) => (
            <div
              className="homepage__container__result__card__badge"
              key={i}
              style={{
                backgroundColor: "#0ee1a3",
                color: "#ffffff",
              }}
            >
              {forum.Tag.Title}
            </div>
          ))}
        </div>
        <div className="coffee__corner__new__discussion__card__right__header__buttons">
          <button
            className="coffee__corner__new__discussion__card__right__header__buttons__btn"
            onClick={() => {
              navigate("/coffee-corner-discussion/" + item.ForumId, {
                state: {
                  forumId: item.ForumId,
                },
              });
            }}
            title="comments"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-message-square"
            >
              <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
            </svg>
            <span>{item.ForumAnswers.length}</span> Comments
          </button>
          <button
            className="coffee__corner__new__discussion__card__right__header__buttons__btn"
            onClick={() => shareQuestion(item.ForumId)}
            title="share"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-share-2"
            >
              <circle cx="18" cy="5" r="3"></circle>
              <circle cx="6" cy="12" r="3"></circle>
              <circle cx="18" cy="19" r="3"></circle>
              <line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line>
              <line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line>
            </svg>
            Share
          </button>
        </div>
      </div>
    </div>
  );
}
