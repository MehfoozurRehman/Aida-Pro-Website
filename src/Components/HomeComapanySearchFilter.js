import React, { useEffect, useState } from "react";
import SearchFilterPanelBadge from "./SearchFilterPanelBadge";
import Autocomplete from "react-google-autocomplete";
import RangeSelector from "./RangeSelector";

function SearchFilterPanel({
  keywords,
  isMainSearch,
  placeholder,
  isOnCoffeeCorner,
  searchByTitle,
  searchByFilter,
  searchByKeyword,
  title,
  searchBy,
  searchByFreelancer,
  searchByUser,
  searchByRange,
  searchByLocation,
  location,
  resetFilter,
  isFiltersOpenn,
  tagId,
  range,
}) {
  const [isFiltersOpen, setIsFiltersOpen] = useState(isFiltersOpenn);
  const [localTitle, setTitleLocal] = useState(title);
  const [employeeSearch, setEmployeeSearch] = useState(searchBy ? true : false);
  const [selectedKeywordId, setSelectedKeywordId] = useState(null);
  const [freelancerSearch, setFreelancerSearch] = useState(
    searchByFreelancer ? true : false
  );

  useEffect(() => {
    setSelectedKeywordId(tagId ? tagId : null);
  }, [tagId]);

  const setStateOfTitle = (e) => {
    setTitleLocal(e.currentTarget.value);
    searchByTitle(e.currentTarget.value);
  };

  const setStateOfKeyword = (e, value) => {
    setSelectedKeywordId(e.Id);
    setTitleLocal(value);
    searchByKeyword(value);
  };

  const toggleFreelancerSearch = (event) => {
    if (!event.currentTarget.checked) {
      setEmployeeSearch(true);
      setFreelancerSearch(false);
      searchByUser(false, true);
    } else {
      searchByUser(true, employeeSearch);
    }
  };

  const toggleEmployeeSearch = (event) => {
    if (!event.currentTarget.checked) {
      setEmployeeSearch(false);
      setFreelancerSearch(true);
      searchByUser(true, false);
    } else {
      searchByUser(freelancerSearch, true);
    }
  };

  const onClickReset = () => {
    setSelectedKeywordId(null);
    setTitleLocal("");
    resetFilter();
  };

  return (
    <div
      className="homepage__container__jumbotron__form animate__animated animate__fadeInUp"
      style={isFiltersOpen ? null : { borderRadius: "50px", boxShadow: "none" }}
      onClick={() => {
        if (!isFiltersOpen) {
          setIsFiltersOpen(true);
        }
      }}
    >
      <div
        className="homepage__container__jumbotron__form__input"
        style={
          isMainSearch
            ? isFiltersOpen
              ? null
              : { backgroundColor: "#eeeeee", boxShadow: "none" }
            : null
        }
      >
        <input
          type="text"
          className="homepage__container__jumbotron__form__input__field"
          placeholder={placeholder ? placeholder : "Search"}
          value={localTitle}
          onChange={(e) => setStateOfTitle(e)}
          onKeyDown={(event) => {
            if (event.key === "Enter") searchByFilter(10, 1, localTitle, selectedKeywordId);
          }}
        />
        <button
          title="search"
          className="homepage__container__jumbotron__form__button"
          onClick={() => searchByFilter(10, 1, localTitle, selectedKeywordId)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="17.064"
            height="17.064"
            viewBox="0 0 17.064 17.064"
          >
            <g
              id="Icon_feather-search"
              data-name="Icon feather-search"
              transform="translate(1 1)"
            >
              <path
                id="Path_2299"
                data-name="Path 2299"
                d="M17.522,11.011A6.511,6.511,0,1,1,11.011,4.5,6.511,6.511,0,0,1,17.522,11.011Z"
                transform="translate(-4.5 -4.5)"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
              <path
                id="Path_2300"
                data-name="Path 2300"
                d="M28.515,28.515l-3.54-3.54"
                transform="translate(-13.866 -13.866)"
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
            </g>
          </svg>
        </button>
      </div>

      {isFiltersOpen ? (
        <div className="homepage__container__jumbotron__form__filters">
          {isOnCoffeeCorner ? null : (
            <>
              {isMainSearch ? null : (
                <div className="homepage__container__jumbotron__form__filters__role">
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-jobseeker-role"
                    type="checkbox"
                    value="Remember"
                    name="Remember"
                    checked={searchBy ? true : false}
                    onClick={(e) => toggleEmployeeSearch(e)}
                  />
                  <label htmlFor="styled-checkbox-jobseeker-role">
                    Professional
                  </label>
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-freelancer-role"
                    type="checkbox"
                    value="Remember"
                    name="Remember"
                    checked={searchByFreelancer ? true : false}
                    onClick={(e) => toggleFreelancerSearch(e)}
                  />
                  <label htmlFor="styled-checkbox-freelancer-role">
                    Freelancer
                  </label>
                </div>
              )}
              <div className="homepage__container__jumbotron__form__filters__location">
                <Autocomplete
                  // ref={ref => locationRef = ref}
                  apiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
                  // lang="en"
                  onPlaceSelected={(place) => {
                    searchByLocation(place);
                  }}
                  defaultValue={location}
                  placeholder="Location"
                  className="homepage__container__jumbotron__form__filters__location__select"
                />
                <RangeSelector radius={range} searchByRange={searchByRange} />
              </div>
            </>
          )}
          <div className="homepage__container__jumbotron__form__filters__badges">
            {keywords.length > 0
              ? keywords.map((keyword, i) => (
                <SearchFilterPanelBadge
                  key={i}
                  data={keyword}
                  selectedKeywordId={selectedKeywordId}
                  searchThisTitle={setStateOfKeyword}
                />
              ))
              : null}
          </div>
          <div className="dashboard__company__container__left__form__view__filter">
            <button
              className="dashboard__company__container__left__form__view__filter__btn"
              style={{ background: "#71797E", color: "#ffffff" }}
              type="button"
              title="reset"
              onClick={() => onClickReset()}
            >
              Reset
            </button>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default SearchFilterPanel;
