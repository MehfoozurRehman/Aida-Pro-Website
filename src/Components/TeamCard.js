import React from "react";
import Avatar from "./Avatar";

export default function TeamCard({
  avatar,
  firstName,
  lastName,
  designation,
  content,
}) {
  return (
    <section className="testimonials__card__reverse">
      <Avatar />
      <div className="testimonials__card__name">
        {firstName}, <span>{lastName}</span>
      </div>
      <div className="testimonials__card__designation">{designation}</div>
      <div className="testimonials__card__content">{content}</div>
    </section>
  );
}
