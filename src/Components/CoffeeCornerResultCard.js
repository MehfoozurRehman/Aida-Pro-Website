import React from "react";
import { Link } from "@reach/router";
import { getText } from "Utils/functions";

export default function CoffeeCornerResultCard({ item, to }) {
  return (
    <Link
      to={to + "/" + item.ForumId}
      state={{ forumId: item.ForumId }}
      className="homepage__container__result__card"
    >
      <div className="homepage__container__result__card__header">
        {/* <Avatar /> */}
        <div className="homepage__container__result__card__header__content">
          <div className="homepage__container__result__card__header__name">
            {item.Subject}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__content__info">
        {getText(item.Description).length > 150 ? (
          <>{`${getText(item.Description).substring(0, 150)}...`}</>
        ) : (
          <>{getText(item.Description)}</>
        )}
      </div>
      <div className="homepage__container__result__card__badges">
        {item.ForumTags.map((element, i) => (
          <div className="homepage__container__result__card__badge" key={i}>
            {element.Tag.Title}
          </div>
        ))}
      </div>
    </Link>
  );
}
