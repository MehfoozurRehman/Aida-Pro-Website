import { navigate } from "@reach/router";
import { crossSvg } from "Assets";
import React, { useEffect } from "react";

export default function PaymentConfirmation({
  onClose,
  heading,
  text,
  onConfirm,
  onDecline,
}) {
  const isOn = window.localStorage.getItem("isOn");

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <>
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              onClose(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "25px",
                  textAlign: "center",
                  marginTop: 0,
                }}
              >
                {heading}
              </div>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "14px", textAlign: "center", marginTop: 0 }}
              >
                {text}
              </div>
              <div style={{ display: "flex" }}>
                <button
                  type="submit"
                  className="header__nav__btn btn__primary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "40px",
                    width: "150px",
                    // marginRight: "1em",
                  }}
                  onClick={() => {
                    if (text == "Please complete your professional detail to apply, thanks") {
                      if (isOn == "freelancer") navigate("/home-freelancer/professional-details")
                      else if (isOn == "professional") navigate("/home-professional/professional-details")
                      onClose(false);
                    } else onClose(false)
                  }}
                  title="action button"
                >
                  Ok
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
