import { noData } from "Assets";
import React from "react";
import Img from "react-cool-img";

export default function NoData({ text }) {
  return (
    <div className="no__data__container">
      <Img loading="lazy" src={noData} className="no__data__container__img" />
      {text ? <div className="no__data__container__text">{text}</div> : null}
    </div>
  );
}
