import { navigate } from "@reach/router";
import React from "react";
import Avatar from "./Avatar";
import firebase from "firebase";

export default function Message({ item, setIsMessagePanelOpen }) {
  const isOn = window.localStorage.getItem("isOn");

  const navigateToMessenger = () => {
    if (isOn == "company") {
      if (window.location.pathname != "/home-company/messenger") {
        navigate("/home-company/messenger", {
          state: { ChatNodeName: item.ChatNodeName },
        });
        setIsMessagePanelOpen(false);
      } else setIsMessagePanelOpen(false);
    } else if (isOn == "professional") {
      if (window.location.pathname != "/home-professional/messenger") {
        navigate("/home-professional/messenger", {
          state: { ChatNodeName: item.ChatNodeName },
        });
        setIsMessagePanelOpen(false);
      } else setIsMessagePanelOpen(false);
    } else {
      if (window.location.pathname != "/home-freelancer/messenger") {
        navigate("/home-freelancer/messenger", {
          state: { ChatNodeName: item.ChatNodeName },
        });
        setIsMessagePanelOpen(false);
      } else setIsMessagePanelOpen(false);
    }
  };

  return (
    <div
      // to={isOn == "company" ? "/home-company/messenger" : isOn == "professional" ? "/home-professional/messenger" : "home-freelancer/messenger"}
      onClick={() => navigateToMessenger()}
      className="header__message__panel__message"
    >
      <div className="header__message__panel__message__header">
        <Avatar />
        <div className="header__message__panel__message__heading">
          {item.senderName}
        </div>
      </div>
      <div className="header__message__panel__message__info">
        {"You have new messages"}
      </div>
      <div
        className="header__message__panel__message__info"
        style={{ width: "100%", textAlign: "right" }}
      >
        {new Date(item.createdAt).toLocaleTimeString("en-US", {
          hour12: true,
          hour: "numeric",
          minute: "numeric",
        })}
      </div>
    </div>
  );
}
