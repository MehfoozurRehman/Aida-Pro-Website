import React, { useState } from "react";
import Img from "react-cool-img";
import moment from "moment";
import { DeleteSvg, EditSvg } from "Assets";
import { navigate } from "@reach/router";

export default function JobsProjectsAppliedList({ item }) {
  let currentStatus =
    item.IsRejected == null || !item.IsRejected
      ? item.Job != undefined
        ? item.Job.StatusTypeLookupDetail.Title
        : item.Project.StatusTypeLookupDetail.Title
      : item.IsRejected
        ? "Rejected"
        : null;
  let backgroundColor = "";

  backgroundColor =
    currentStatus === "Live"
      ? "linear-gradient(45deg,#0EE1A3,#0CA69D)"
      : currentStatus === "Draft"
        ? "linear-gradient(45deg, #f6a938, #f5833c)"
        : currentStatus === "Hold"
          ? "linear-gradient(45deg, #ffdd00, #e3eb00)"
          : currentStatus === "Closed"
            ? "linear-gradient(45deg, #DE3F3F, #FF6161)"
            : currentStatus == "Rejected"
              ? "linear-gradient(45deg, #DE3F3F, #FF6161)"
              : "#ffae0c";

  const onClickItem = () => {
    const requestData = {
      redirectFrom: item.Job != undefined ? "EmployeeApplied" : "FreelancerApplied",
      objectData: item.Job != undefined ? item.Job : item.Project,
      jobType: item.Job != undefined ? "Jobs" : "Projects",
    };
    navigate("/job-details", { state: requestData });
  };

  return (
    <div className="posting__container__table__list__wrapper">
      <div className="posting__container__table__list__wrapper__header">
        <div className="posting__container__table__list__wrapper__header__entry">
          Applied Date
        </div>
        <div className="posting__container__table__list__wrapper__header__entry">
          Job/Project Title
        </div>
        <div className="posting__container__table__list__wrapper__header__entry">
          Status
        </div>
      </div>
      <div onClick={onClickItem} className="posting__container__table__list">
        <div className="posting__container__table__list__entry">
          {moment(item.CreatedOn).format("LL")}
        </div>
        <div className="posting__container__table__list__entry">
          {item.Job != undefined ? item.Job.JobTitle : item.Project.Title}
        </div>
        <div className="posting__container__table__list__entry">
          <button className="posting__container__table__list__entry__dropdown">
            <div
              className="posting__container__table__list__entry__dropdown__text "
              style={{
                background: backgroundColor,
                justifyContent: "center",
              }}
            >
              <div className="posting__container__table__list__entry__dropdown__text__label">
                {item.IsRejected == null || !item.IsRejected
                  ? item.Job != undefined
                    ? item.Job.StatusTypeLookupDetail.Title == "Live"
                      ? "Open"
                      : item.Job.StatusTypeLookupDetail.Title
                    : item.Project.StatusTypeLookupDetail.Title == "Live"
                      ? "Open"
                      : item.Project.StatusTypeLookupDetail.Title
                  : item.IsRejected
                    ? "Rejected"
                    : null}
              </div>
            </div>
          </button>
        </div>
      </div>
    </div>
  );
}
