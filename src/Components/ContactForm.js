import React from "react";
import { InputBox } from "Components";

export default function ContactForm({
  isOnOther,
  firstNameError,
  firstNameErrorMessage,
  firstName,
  onNameTextChangeListener,
  lastName,
  lastNameError,
  lastNameErrorMessage,
  onLastNameTextChangeListener,
  email,
  emailError,
  emailErrorMessage,
  onEmailTextChangeListener,
  countryCodes,
  selectedCountryCode,
  onChangeCountryCode,
  phoneError,
  phoneErrorMessage,
  phoneMinLength,
  phoneMaxLength,
  phone,
  onPhoneNumberChangeListener,
  subject,
  subjectError,
  subjectErrorMessage,
  onSubjectTextChangeListener,
  message,
  messageError,
  messageErrorMessage,
  onDescriptionTextChangeListener,
}) {
  return (
    <>
      <div
        style={{
          marginTop: "1em",
        }}
        className="homepage__container__jumbotron__signup__wrapper"
      >
        <InputBox
          placeholder={isOnOther ? "First Name" : "Company Name"}
          name="name"
          error={firstNameError}
          errorMessage={firstNameErrorMessage}
          value={firstName}
          svg={
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="13.419"
              height="17.918"
              viewBox="0 0 13.419 17.918"
            >
              <g id="name" transform="translate(0)">
                <path
                  id="Path_285"
                  data-name="Path 285"
                  d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                  transform="translate(-138.624 -166.569)"
                  fill="#374957"
                />
                <path
                  id="Path_286"
                  data-name="Path 286"
                  d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                  transform="translate(-105.092 -314.857)"
                  fill="#374957"
                />
                <path
                  id="Path_287"
                  data-name="Path 287"
                  d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                  transform="translate(4.582)"
                  fill="#374957"
                />
                <path
                  id="Path_288"
                  data-name="Path 288"
                  d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                  transform="translate(-138.624 -57.016)"
                  fill="#374957"
                />
              </g>
            </svg>
          }
          onChange={(e) => onNameTextChangeListener(e)}
        />
        <InputBox
          placeholder={isOnOther ? "Last Name" : "Contact Person"}
          name="lastName"
          value={lastName}
          error={lastNameError}
          errorMessage={lastNameErrorMessage}
          svg={
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="13.419"
              height="17.918"
              viewBox="0 0 13.419 17.918"
            >
              <g id="name" transform="translate(0)">
                <path
                  id="Path_285"
                  data-name="Path 285"
                  d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                  transform="translate(-138.624 -166.569)"
                  fill="#374957"
                />
                <path
                  id="Path_286"
                  data-name="Path 286"
                  d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                  transform="translate(-105.092 -314.857)"
                  fill="#374957"
                />
                <path
                  id="Path_287"
                  data-name="Path 287"
                  d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                  transform="translate(4.582)"
                  fill="#374957"
                />
                <path
                  id="Path_288"
                  data-name="Path 288"
                  d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                  transform="translate(-138.624 -57.016)"
                  fill="#374957"
                />
              </g>
            </svg>
          }
          onChange={(e) => onLastNameTextChangeListener(e)}
        />
        <InputBox
          placeholder="Email"
          type="email"
          value={email}
          error={emailError}
          errorMessage={emailErrorMessage}
          name="email"
          svg={
            <svg
              id="mail"
              xmlns="http://www.w3.org/2000/svg"
              width="17.919"
              height="11.946"
              viewBox="0 0 17.919 11.946"
            >
              <g id="Group_202" data-name="Group 202">
                <path
                  id="Path_292"
                  data-name="Path 292"
                  d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                  transform="translate(0 -85.333)"
                  fill="#374957"
                />
              </g>
            </svg>
          }
          onChange={(e) => onEmailTextChangeListener(e)}
        />
        <InputBox
          variant="phone"
          placeholder="Phone"
          type="tel"
          options={countryCodes}
          selectedCountryCode={selectedCountryCode}
          onChangeCountryCode={onChangeCountryCode}
          style={{
            backgroundColor: "#f6f6f6",
          }}
          width="50%"
          error={phoneError}
          errorMessage={phoneErrorMessage}
          phoneMinLength={phoneMinLength}
          phoneMaxLength={phoneMaxLength}
          svg={
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="15.428"
              height="15.45"
              viewBox="0 0 15.428 15.45"
            >
              <g id="phone-call" transform="translate(-0.344 0)">
                <g
                  id="Group_201"
                  data-name="Group 201"
                  transform="translate(0.344 0)"
                >
                  <path
                    id="Path_289"
                    data-name="Path 289"
                    d="M12.544,36.083a1.52,1.52,0,0,0-1.1-.505,1.571,1.571,0,0,0-1.115.5L9.3,37.107c-.085-.046-.17-.088-.251-.13-.117-.059-.228-.114-.323-.173a11.2,11.2,0,0,1-2.684-2.446,6.607,6.607,0,0,1-.88-1.389c.267-.245.515-.5.757-.743.091-.091.183-.186.274-.277a1.5,1.5,0,0,0,0-2.257L5.3,28.8c-.1-.1-.205-.205-.3-.31-.2-.2-.4-.411-.613-.607a1.544,1.544,0,0,0-1.092-.479,1.6,1.6,0,0,0-1.109.479l-.007.007L1.066,29.01a2.386,2.386,0,0,0-.708,1.516,5.716,5.716,0,0,0,.417,2.42A14.041,14.041,0,0,0,3.27,37.107a15.346,15.346,0,0,0,5.11,4,7.961,7.961,0,0,0,2.87.848c.068,0,.14.007.205.007a2.457,2.457,0,0,0,1.882-.809c0-.007.01-.01.013-.016a7.413,7.413,0,0,1,.571-.59c.14-.134.284-.274.424-.421A1.627,1.627,0,0,0,14.836,39a1.567,1.567,0,0,0-.5-1.118Zm1.167,3.434s0,0,0,0c-.127.137-.258.261-.4.4a8.574,8.574,0,0,0-.629.652,1.572,1.572,0,0,1-1.226.518c-.049,0-.1,0-.15,0a7.073,7.073,0,0,1-2.544-.763,14.485,14.485,0,0,1-4.813-3.77A13.239,13.239,0,0,1,1.6,32.64,4.656,4.656,0,0,1,1.238,30.6a1.5,1.5,0,0,1,.45-.968L2.8,28.518a.741.741,0,0,1,.5-.232.7.7,0,0,1,.476.228l.01.01c.2.186.388.378.587.584.1.1.205.209.31.316l.89.89a.622.622,0,0,1,0,1.011c-.095.095-.186.189-.28.28-.274.28-.535.541-.818.8-.007.007-.013.01-.016.016a.665.665,0,0,0-.17.74l.01.029a7.147,7.147,0,0,0,1.053,1.719l0,0a11.969,11.969,0,0,0,2.9,2.635,4.451,4.451,0,0,0,.4.218c.117.059.228.114.323.173.013.007.026.016.039.023a.707.707,0,0,0,.323.082.7.7,0,0,0,.5-.225L10.943,36.7a.738.738,0,0,1,.492-.245.664.664,0,0,1,.47.238l.007.007,1.8,1.8A.645.645,0,0,1,13.711,39.517Z"
                    transform="translate(-0.344 -26.512)"
                    fill="#374957"
                  />
                  <path
                    id="Path_290"
                    data-name="Path 290"
                    d="M245.306,86.8a4.2,4.2,0,0,1,3.417,3.417.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007.441.441,0,0,0,.362-.509,5.075,5.075,0,0,0-4.135-4.135.443.443,0,0,0-.509.359A.435.435,0,0,0,245.306,86.8Z"
                    transform="translate(-236.968 -83.123)"
                    fill="#374957"
                  />
                  <path
                    id="Path_291"
                    data-name="Path 291"
                    d="M256.1,6.816A8.356,8.356,0,0,0,249.287.007a.44.44,0,1,0-.143.867,7.464,7.464,0,0,1,6.085,6.085.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007A.432.432,0,0,0,256.1,6.816Z"
                    transform="translate(-240.674 0)"
                    fill="#374957"
                  />
                </g>
              </g>
            </svg>
          }
          name="phone"
          value={phone}
          onChange={(event) => {
            const re = /^[0-9\b]+$/;

            if (
              event.currentTarget.value === "" ||
              re.test(event.currentTarget.value)
            ) {
              onPhoneNumberChangeListener(event);
            }
          }}
        />
        <InputBox
          placeholder="Subject"
          type="text"
          name="subject"
          value={subject}
          error={subjectError}
          errorMessage={subjectErrorMessage}
          svg={
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 24 24"
              fill="none"
              stroke="#374957"
              strokeWidth="1"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-book"
            >
              <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
              <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
            </svg>
          }
          onChange={(e) => onSubjectTextChangeListener(e)}
        />
        <InputBox
          placeholder="Message"
          variant="textarea"
          value={message}
          style={{
            width: "400px",
          }}
          error={messageError}
          errorMessage={messageErrorMessage}
          name="message"
          onChange={(e) => onDescriptionTextChangeListener(e)}
        />

        <div
          className="homepage__container__jumbotron__signup__button"
          style={{
            justifyContent: "center",
          }}
        >
          <button
            title="send message"
            className="header__nav__btn btn__primary"
            style={{
              width: 200,
              height: 45,
            }}
            type="submit"
          >
            Send
          </button>
        </div>
      </div>
    </>
  );
}
