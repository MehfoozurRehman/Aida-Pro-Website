import React from "react";

export default function Plan3({}) {
  return (
    <div className="plans__container__row__main__card animate__animated animate__fadeIn">
      <div className="plans__container__row__main__card__name">Plan 3</div>
      <div className="plans__container__row__main__card__heading">
        Screening of professionals
      </div>
      <div className="plans__container__row__main__card__content">
        <button
          title="plan"
          className="plans__container__row__main__card__content__entry animate__animated animate__fadeInRight"
        >
          <div className="plans__container__row__main__card__content__entry__info">
            Free quotation for customized recruitment of job applicants or
            freelancers. For all your data and AI employment needs.
          </div>
        </button>
      </div>
    </div>
  );
}
