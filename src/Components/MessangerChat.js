import React from "react";
import { ChatMessage } from "Components";
import Img from "react-cool-img";

export default function MessangerChat({
  messages,
  user,
  selectedChat,
  file,
  AlwaysScrollToBottom,
}) {
  return (
    <div className="messenger__container__chat__messages">
      {messages.map((e, i) => {
        return (
          <ChatMessage
            key={i}
            selectedChat={selectedChat}
            messageData={e}
            userId={
              user.CompanyId != null
                ? user.CompanyId
                : user.FreelancerId != null
                ? user.FreelancerId
                : user.JobSeekerId != null
                ? user.JobSeekerId
                : null
            }
          />
        );
      })}
      {file != null ? (
        <div className="messenger__container__chat__images__entry">
          <Img
            loading="lazy"
            src={file}
            alt="project_img"
            id="project_img"
            style={{
              width: "170px",
              height: "230px",
              borderRadius: 10,
            }}
          />
        </div>
      ) : null}
      <AlwaysScrollToBottom />
    </div>
  );
}
