import React from "react";

export default function PostingDetailsTab({
  label,
  defaultChecked,
  activeSvg,
  inActiveSvg,
  onClick,
  isChecked,
}) {
  return (
    <div className="homepage__container__jobs__projects__penel__container__details__tabs__entry">
      <input
        type="radio"
        defaultChecked={defaultChecked}
        onChange={(e) => {
          onClick();
        }}
        name="homepage__container__jobs__projects__penel__container__details__tabs__entry__input"
        className="homepage__container__jobs__projects__penel__container__details__tabs__entry__input"
      />
      <div className="homepage__container__jobs__projects__penel__container__details__tabs__entry__content">
        {isChecked ? activeSvg : inActiveSvg}
        {label}
      </div>
    </div>
  );
}
