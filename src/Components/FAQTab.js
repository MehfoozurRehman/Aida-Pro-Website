import React from "react";

export default function FAQTab({ defaultChecked, label, onClick }) {
  return (
    <div className="freelancer__project__container__catagory__filter">
      <input
        type="radio"
        name="freelancer__project__container__catagory__filter__input"
        onClick={onClick}
        defaultChecked={defaultChecked}
        className="freelancer__project__container__catagory__filter__input"
      />
      <div className="freelancer__project__container__catagory__filter__content">
        {label}
      </div>
    </div>
  );
}
