import { Link } from "@reach/router";
import React from "react";
import { Menu, X } from "react-feather";
import OutsideClickHandler from "react-outside-click-handler";

export default function HeaderNavMenu({
  isMenuOpen,
  setIsMenuOpen,
  setIsLanguagePanelOpen,
  setIsMessagePanelOpen,
  setIsNotificationPanelOpen,
  onClickOutsideMenu,
  isOn,
  isUserLogedIn,
}) {
  function onLinkClick() {
    setTimeout(() => {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
      if (isMenuOpen) {
        setIsMenuOpen(false);
      } else {
        setIsMenuOpen(true);
        setIsLanguagePanelOpen(false);
        setIsMessagePanelOpen(false);
        setIsNotificationPanelOpen(false);
      }
    }, 300);
  }
  // console.log("isOn", isOn);
  // console.log("isUserLogedIn", isUserLogedIn);
  return (
    <>
      <button
        title="open / close nav menu"
        className="header__nav__button"
        onClick={() => {
          if (isMenuOpen) {
            setIsMenuOpen(false);
          } else {
            setIsMenuOpen(true);
            setIsLanguagePanelOpen(false);
            setIsMessagePanelOpen(false);
            setIsNotificationPanelOpen(false);
          }
        }}
      >
        {isMenuOpen ? (
          <X size={18} color="currentColor" strokeWidth={3} />
        ) : (
          <Menu size={18} color="currentColor" strokeWidth={3} />
        )}
      </button>
      {isMenuOpen ? (
        <OutsideClickHandler
          display="contents"
          onOutsideClick={onClickOutsideMenu}
        >
          <div className="header__nav__links">
            <Link
              to="/coffee-corner"
              onClick={() => {
                setTimeout(() => {
                  window.scrollTo({
                    top: 0,
                    behavior: "smooth",
                  });
                }, 300);
              }}
              className="header__nav__link"
            >
              Coffee Corner
            </Link>
            {isUserLogedIn ? (
              <div className="header__nav__link__wrapper">
                <div className="header__nav__link__bar" />
                {isOn === "company" ? (
                  <>
                    <Link
                      to="/home-company"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Home
                    </Link>
                    <Link
                      to="/home-company/messenger"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Messanger
                    </Link>
                    <Link
                      to="/home-company/posting"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Posting
                    </Link>
                    <Link
                      to="/home-company/post-job"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Post a job
                    </Link>
                    <Link
                      to="/home-company/post-project"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Post a project
                    </Link>
                    <Link
                      to="/home-company/plan"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Plan & pricing
                    </Link>
                    <Link
                      to="/home-company/billing"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Balance
                    </Link>
                    <Link
                      to="/home-company/planned-video-calls"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Video meetings
                    </Link>
                  </>
                ) : isOn === "professional" ? (
                  <>
                    <Link
                      to="/home-professional"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Home
                    </Link>
                    <Link
                      to="/home-professional/messenger"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Messanger
                    </Link>
                    <Link
                      to="/home-professional/personal-details-preview"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Personal details
                    </Link>
                    <Link
                      to="/home-professional/professional-details"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Professional details
                    </Link>
                    <Link
                      to="/home-professional/project"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      My projects
                    </Link>
                    <Link
                      to="/home-professional/applied"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Applied
                    </Link>
                    <Link
                      to="/home-professional/interested"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Interested
                    </Link>
                    <Link
                      to="/home-professional/planned-video-calls"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Video meetings
                    </Link>
                  </>
                ) : isOn === "freelancer" ? (
                  <>
                    <Link
                      to="/home-freelancer"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Home
                    </Link>
                    <Link
                      to="/home-freelancer/messenger"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Messanger
                    </Link>
                    <Link
                      to="/home-freelancer/personal-details-preview"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Personal details
                    </Link>
                    <Link
                      to="/home-freelancer/professional-details"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Professional details
                    </Link>
                    <Link
                      to="/home-freelancer/project"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      My projects
                    </Link>
                    <Link
                      to="/home-freelancer/applied"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Applied
                    </Link>
                    <Link
                      to="/home-freelancer/interested"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Interested
                    </Link>
                    <Link
                      to="/home-freelancer/planned-video-calls"
                      onClick={onLinkClick}
                      className="header__nav__link"
                    >
                      Video meetings
                    </Link>
                  </>
                ) : null}
              </div>
            ) : null}
          </div>
        </OutsideClickHandler>
      ) : null}
    </>
  );
}
