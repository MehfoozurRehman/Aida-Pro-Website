import React from "react";
import { Camera, Paperclip } from "react-feather";

export default function MessengerChatForm({
  newMessage,
  setNewMessage,
  CustomError,
  onClickSend,
  selectedChat,
  fileSelectedHandler,
}) {
  return (
    <div className="messenger__container__chat__form">
      <div className="messenger__container__chat__form__input">
        <input
          type="text"
          className="messenger__container__chat__form__input__field"
          placeholder="Type a message"
          value={newMessage}
          disabled={!selectedChat.IsChatRequestAccepted}
          onChange={(event) => {
            setNewMessage(event.currentTarget.value);
          }}
          onKeyDown={(event) => {
            if (event.key === "Enter") {
              if (!selectedChat.IsChatRequestAccepted)
                CustomError("Kindly accept chat first.");
              else onClickSend(null);
            }
          }}
        />
        <button
          className="messenger__container__chat__header__controls__btn"
          title="attachment"
        >
          <Paperclip size={18} color="currentColor" />
          <input
            type="file"
            accept=".doc,.docm,.docx,.dot,.dotx,.pdf,.rtf,.txt,.xml,.csv,.xls,.xlsx,.xlw,.ppt,.pptx"
            disabled={!selectedChat.IsChatRequestAccepted}
            onChange={(e) => fileSelectedHandler(e.currentTarget.files, "file")}
            className="messenger__container__chat__form__input__attachment"
          />
        </button>
        <button
          className="messenger__container__chat__header__controls__btn"
          title="image"
        >
          <Camera size={18} color="currentColor" />
          <input
            type="file"
            accept="image/png, image/jpg, image/jpeg"
            disabled={!selectedChat.IsChatRequestAccepted} // onClick={event => event.target.value = null}
            onChange={(e) =>
              fileSelectedHandler(e.currentTarget.files, "image")
            }
            className="messenger__container__chat__form__input__attachment"
          />
        </button>
      </div>
      <button
        className="header__nav__btn btn__secondary"
        onClick={() => {
          if (!selectedChat.IsChatRequestAccepted)
            CustomError("Kindly accept chat first.");
          else onClickSend(null);
        }}
        title="send message"
      >
        Send
      </button>
    </div>
  );
}
