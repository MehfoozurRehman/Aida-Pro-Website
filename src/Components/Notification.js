import React from "react";
import { navigate } from "@reach/router";
import { NOTIFICATION_TYPE } from "Utils/Constants";
import { useSelector } from "react-redux";
import { videoRequestUpdateStatus } from 'API/Video';
import { CustomError, CustomSuccess } from "Screens/Toasts";
import { getJobById } from "API/Job";
import { getProjectById } from "API/Projects";
import { store } from "index";
import { updateNotification } from "Redux/Actions/AppActions";

export default function Notification({ item, updateNotificationDataAcceptReject, setIsNotificationPanelOpen }) {
  const { user } = useSelector((state) => state.user);

  const onClickNotification = () => {
    var notificationData = null;

    if (item.notificationdata != null)
      notificationData = item.notificationdata.notification;
    if (item.notificationtype == NOTIFICATION_TYPE.CHAT_REQUEST) {
    } else if (item.notificationtype == NOTIFICATION_TYPE.VIDEO_REQUEST) {
      if (item.IsAccepted) {
        if (user.Freelancer != null) {
          if (window.location.pathname != "/home-freelancer/planned-video-calls");
          navigate("/home-freelancer/planned-video-calls");
        } else if (user.JobSeeker != null) {
          if (window.location.pathname != "/home-professional/planned-video-calls");
          navigate("/home-professional/planned-video-calls");
        } else if (user.CompanyProfile != null) {
          if (window.location.pathname != "/home-company/planned-video-calls")
            navigate("/home-company/planned-video-calls");
        }
        let newNotificationObject = {
          newNotify: true
        }
        store.dispatch(updateNotification(newNotificationObject));
      }
    } else if (item.notificationtype == NOTIFICATION_TYPE.VIEWED_PROFILE) {
    } else if (item.notificationtype == NOTIFICATION_TYPE.COMMENT_ON_QUESTION) {
      navigate("/coffee-corner-discussion/" + notificationData.Id, {
        state: { forumId: notificationData.Id },
      });
    } else if (
      item.notificationtype == NOTIFICATION_TYPE.INTERESTED_JOBDETAIL
    ) {
      window.localStorage.setItem("isInterested", 1);
      window.localStorage.removeItem("isApplicant");
      window.localStorage.setItem("JobId", notificationData.Id);
      window.localStorage.setItem("JobType", "Jobs");
      navigate("/home-company/posting/details");
    } else if (
      item.notificationtype == NOTIFICATION_TYPE.INTERESTED_PROJECTDETAIL
    ) {
      window.localStorage.setItem("isInterested", 1);
      window.localStorage.removeItem("isApplicant");
      window.localStorage.setItem("JobId", notificationData.Id);
      window.localStorage.setItem("JobType", "Projects");
      navigate("/home-company/posting/details");
    } else if (item.notificationtype == NOTIFICATION_TYPE.APPLIED_JOBDETAIL) {
      window.localStorage.setItem("isApplicant", 1);
      window.localStorage.removeItem("isInterested");
      window.localStorage.setItem("JobId", notificationData.Id);
      window.localStorage.setItem("JobType", "Jobs");
      navigate("/home-company/posting/details");
    } else if (item.notificationtype == NOTIFICATION_TYPE.APPLIES_PROJECTDETAIL) {
      window.localStorage.setItem("isApplicant", 1);
      window.localStorage.removeItem("isInterested");
      window.localStorage.setItem("JobId", notificationData.Id);
      window.localStorage.setItem("JobType", "Projects");
      navigate("/home-company/posting/details");
    } else if (item.notificationtype == NOTIFICATION_TYPE.REJECT_JOB_APPLICATION) {
      getJobById(item.notificationdata.notification.Id).then((data) => {
        if (data.data.success) {
          const requestData = {
            redirectFrom: "Employee",
            objectData: data.data.result,
            jobType: "Jobs",
          };
          navigate("/job-details", { state: requestData });
        }
      }).catch((err) => {
        // console.log("err", err)
      })
    } else if (item.notificationtype == NOTIFICATION_TYPE.REJECT_PROJECT_APPLICATION) {
      getProjectById(item.notificationdata.notification.Id).then((data) => {
        if (data.data.success) {
          const requestData = {
            redirectFrom: "Freelancer",
            objectData: data.data.result,
            jobType: "Projects",
          };
          navigate("/job-details", { state: requestData });
        }
      }).catch((err) => {
        // console.log("err", err)
      })
    } else if (item.notificationtype == NOTIFICATION_TYPE.VIDEO_CALL_STARTED) {
      if (user.Freelancer != null) {
        if (window.location.pathname != "/home-freelancer/planned-video-calls");
        navigate("/home-freelancer/planned-video-calls");
      } else if (user.JobSeeker != null) {
        if (window.location.pathname != "/home-professional/planned-video-calls");
        navigate("/home-professional/planned-video-calls");
      } else if (user.CompanyProfile != null) {
        if (window.location.pathname != "/home-company/planned-video-calls")
          navigate("/home-company/planned-video-calls");
      }
    } else
      // console.log("nothing")
      setIsNotificationPanelOpen(false)
  };

  const onAcceptRejectRequest = (isReject) => {
    let object = {
      "videorequest": {
        "Id": item.notificationdata.notification.Id,
        "Isrejected": isReject,
      },
      "chatmodel": {
        "chat": {
          "CompanyProfileId": item.notificationdata.notification.from_id,
          "JobSeekerId": "string",
          "FreelancerId": "string",
          "IsVideo": true,
          "IsVideoRequestAccepted": isReject ? false : true
        },
        "message": {
          "Title": "Video Call",
          "Body": `${user.LoginName} has ${!isReject ? "accepted" : "rejected"} your video call request.`,
          "Id": item.Id
        }
      }
    }

    videoRequestUpdateStatus(object).then((data) => {
      updateNotificationDataAcceptReject(item, isReject);
      // //CustomSuccess("Video status updated successfully.")
      if (user.Freelancer != null) {
        if (window.location.pathname != "/home-freelancer/planned-video-calls");
        navigate("/home-freelancer/planned-video-calls");
      } else if (user.JobSeeker != null) {
        if (window.location.pathname != "/home-professional/planned-video-calls");
        navigate("/home-professional/planned-video-calls");
      } else if (user.CompanyProfile != null) {
        if (window.location.pathname != "/home-company/planned-video-calls")
          navigate("/home-company/planned-video-calls");
      }
      let newNotificationObject = {
        newNotify: true
      }
      store.dispatch(updateNotification(newNotificationObject));
    }).catch((err) => {
      CustomError("Some error occured.")
    })
  }

  return (
    <div
      className="header__notification__panel__notification"
      title="notification"
      onClick={onClickNotification}
    >
      <div className="header__notification__panel__notification__heading">
        {item.Title}
      </div>
      <div className="header__notification__panel__notification__info">
        {item.Body}
      </div>
      {item.notificationtype == NOTIFICATION_TYPE.VIDEO_REQUEST && item.notificationdata != null ?
        <>
          {item.IsAccepted == null ?
            <div className="header__notification__panel__notification__buttons">
              <button
                className="header__notification__panel__notification__buttons__primary"
                onClick={() => {
                  onAcceptRejectRequest(false);
                }}
              >
                Accept
            </button>
              <button
                className="header__notification__panel__notification__buttons__secondary"
                onClick={() => {
                  onAcceptRejectRequest(true);
                }}
              >
                Decline
          </button>
            </div>
            :
            (item.IsAccepted ? (
              <div className="header__notification__panel__notification__info" >Request has been accepted.</div>
            )
              :
              <div className="header__notification__panel__notification__info" >Request has been declined.</div>
            )
          }
          {/* <div className="header__notification__panel__notification__buttons">
            <button
              className="header__notification__panel__notification__buttons__primary"
              onClick={() => {
                onAcceptRejectRequest(false);
              }}
            >
              Accept
            </button>
            <button
              className="header__notification__panel__notification__buttons__secondary"
              onClick={() => {
                onAcceptRejectRequest(true);
              }}
            >
              Decline
          </button>
          </div> */}
        </>
        :
        null}
    </div>
  );
}
