import React from "react";
import { Link } from "@reach/router";

export default function UserPanel({
  logout,
  dashboard,
  dashboardName,
  setIsUserPanelOpen,
}) {
  return (
    <div className="header__user__panel animate__animated animate__fadeIn">
      <Link
        to={
          dashboard
            ? "/login"
            : `/home-${dashboardName ? dashboardName : "company"}/profile`
        }
        onClick={() => {
          setIsUserPanelOpen(false);
        }}
        className="header__nav__btn btn__primary"
      >
        {dashboard ? "Dashboard" : "Profile"}
      </Link>
      <button
        className="header__nav__btn btn__secondary"
        onClick={logout}
        title="logout"
      >
        Logout
      </button>
    </div>
  );
}
