import React from "react";

export default function SocialLinkCard({ svg, text, onClick }) {
  return (
    <a
      onClick={onClick}
      className="homepage__container__jumbotron__left__social__links"
    >
      <div className="homepage__container__jumbotron__left__social__links__svg">
        {svg}
      </div>
    </a>
  );
}
