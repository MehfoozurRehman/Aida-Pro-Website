import React from "react";
import { Text, View } from "@react-pdf/renderer";

function Checkout() {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center",
        marginTop: 5,
      }}
    >
      <View
        style={{
          width: "34%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Text
          style={{
            fontSize: 10,
            color: "#0DCBA0",
            fontWeight: "bold",
          }}
        >
          Total Amount
        </Text>
      </View>
      <View
        style={{
          width: "14%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            fontSize: 10,
            color: "#2C4C4C",
            fontWeight: "bold",
          }}
        >
          32,400.00
        </Text>
      </View>
    </View>
  );
}

export default Checkout;
