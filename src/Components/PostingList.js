import React, { useEffect, useState } from "react";
import { navigate } from "@reach/router";
import { getLookUpByPrefix } from "../API/Api";
import { jobStatusUpdate, jobDelete } from "../API/Job";
import { projectStatusUpdate, projectDelete } from "../API/Projects";
import { CustomError } from "../Screens/Toasts";
import { DeleteSvg, EditSvg } from "Assets";
import PostingListContent from "./PostingListContent";
import PostingListHeader from "./PostingListHeader";

export default function PostingList({
  data,
  handleReloadData,
  postingFilterStatus,
}) {
  const [selected, setSelected] = useState("Live");
  const [postingList, setPostingList] = useState(data);
  const [statusAPI, setStatusAPI] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [statusDataRecieved, setStatusDataRecieved] = useState(null);

  const changeStatusApi = (selected, Id, status, Type) => {
    if (selected === "Live") {
      setSelected("Live");
    } else if (selected === "Draft") {
      setSelected("Draft");
    } else if (selected === "POSTED") {
      setSelected("POSTED");
    } else if (selected === "Hold") {
      setSelected("Hold");
    } else if (selected === "Closed") {
      setSelected("Closed");
    }
    setIsLoading(true);
    let data = {
      Id: Id,
      StatusTypeLookupDetailId: status.value,
    };

    if (Type === "Jobs") {
      jobStatusUpdate(data)
        .then(({ data }) => {
          setIsLoading(false);
          setStatusDataRecieved(null);
          if (postingFilterStatus != "All") {
            let newArray = [...postingList];

            let itemIndex = newArray.findIndex(
              (element) => element.Id == data.result.Id
            );
            if (itemIndex > -1) {
              newArray.splice(itemIndex, 1); // 2nd parameter means remove one item only
            }
            setPostingList([]);
            setPostingList(newArray);
          }
          // handleReloadData();
        })
        .catch((err) => {
          CustomError("Status not Updated");
          setIsLoading(false);
        });
    } else {
      projectStatusUpdate(data)
        .then(({ data }) => {
          setIsLoading(false);
          setStatusDataRecieved(null);
          if (postingFilterStatus != "All") {
            let newArray = [...postingList];

            let itemIndex = newArray.findIndex(
              (element) => element.Id == data.result.Id
            );
            if (itemIndex > -1) {
              newArray.splice(itemIndex, 1); // 2nd parameter means remove one item only
            }
            setPostingList([]);
            setPostingList(newArray);
          }
          // handleReloadData();
        })
        .catch((err) => {
          CustomError("Status not Updated");
          setIsLoading(false);
        });
    }
  };

  const getByPrefixAPI = () => {
    setIsLoading(true);
    getLookUpByPrefix("JBSTTYP")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setStatusAPI(formattedData);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    setPostingList(data);
  }, [data]);

  useEffect(() => {
    getByPrefixAPI();
  }, []);

  const deleteRecord = (objectData) => {
    let requestedData = {
      Id: objectData.Id,
    };

    if (objectData.Type === "Projects") {
      projectDelete(requestedData)
        .then(({ data }) => {
          // CustomInfo("Record Deleted");
          setIsLoading(false);
          handleReloadData();
        })
        .catch((err) => {
          CustomError("Record Not Deleted");
          setIsLoading(false);
        });
    } else {
      jobDelete(requestedData)
        .then(({ data }) => {
          // CustomInfo("Record Deleted");
          setIsLoading(false);
          handleReloadData();
        })
        .catch((err) => {
          CustomError("Record Not Deleted");
          setIsLoading(false);
        });
    }
  };

  const editRecord = (objectData) => {
    if (objectData.Type === "Projects") {
      navigate("/home-company/post-project", {
        state: { projectData: objectData },
      });
    } else {
      navigate("/home-company/post-job", { state: { jobData: objectData } });
    }
  };

  return postingList.map((item, i) => (
    <div className="posting__container__table__list__wrapper" key={i}>
      <PostingListHeader />
      <PostingListContent
        statusAPI={statusAPI}
        changeStatusApi={changeStatusApi}
        editRecord={editRecord}
        data={item}
        EditSvg={EditSvg}
        deleteRecord={deleteRecord}
        DeleteSvg={DeleteSvg}
        index={i}
        statusDataRecieved={statusDataRecieved}
        setStatusDataRecieved={setStatusDataRecieved}
      />
    </div>
  ));
}
