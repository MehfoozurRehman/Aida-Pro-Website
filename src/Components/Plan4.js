import React from "react";

export default function Plan4({ setIsCustomizedRequirmentsOpen }) {
  return (
    <div className="plans__container__row__main__card animate__animated animate__fadeIn">
      <div className="plans__container__row__main__card__name">Plan 4</div>
      <div className="plans__container__row__main__card__heading">
        Customized recruitment
      </div>
      <div className="plans__container__row__main__card__content">
        <button
          className="plans__container__row__main__card__content__entry animate__animated animate__fadeInRight"
          onClick={() => {
            setIsCustomizedRequirmentsOpen(true);
            window.scrollTo({
              top: 0,
              behavior: "smooth",
            });
          }}
          title="plan"
        >
          <div className="plans__container__row__main__card__content__entry__info">
            Free quotation for customized recruitment of job applicants or
            freelancers. For all your data and AI employment needs.
          </div>
        </button>
      </div>
    </div>
  );
}
