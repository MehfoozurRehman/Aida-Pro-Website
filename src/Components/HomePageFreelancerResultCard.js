import React from "react";
import { getText } from "Utils/functions";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import Img from "react-cool-img";

export default function HomePageFreelancerResultCard({ data, moveTo }) {
  const navigate = () => {
    const requestData = {
      redirectFrom: "Freelancer",
      objectData: data,
      jobType: "Projects",
    };
    moveTo(requestData);
  };

  return (
    <div
      className="homepage__container__result__card"
      onClick={() => navigate()}
    >
      <div className="homepage__container__result__card__header">
        <div className="homepage__container__result__card__header__icon">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20.4"
            height="17"
            viewBox="0 0 20.4 17"
            fill="currentColor"
          >
            <g id="suitcase" transform="translate(0 -2)">
              <path
                id="Path_2267"
                data-name="Path 2267"
                d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                transform="translate(-1.2)"
              />
              <path
                id="Path_2268"
                data-name="Path 2268"
                d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                transform="translate(0 -1.443)"
              />
              <path
                id="Path_2269"
                data-name="Path 2269"
                d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                transform="translate(0 -0.45)"
              />
            </g>
          </svg>
        </div>
        <div className="homepage__container__result__card__header__content">
          {/* <div
            className="homepage__container__result__card__header__designation"
            style={{ marginBottom: "0.3em" }}
          >
            Full Time Remote Job
          </div> */}
          <div className="homepage__container__result__card__header__name">
            {data ? data.Title : null}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__content__info">
        {getText(data.Description).length > 100 ? (
          <>{`${getText(data.Description).substring(0, 100)}...`}</>
        ) : (
          <>{getText(data.Description)}</>
        )}
      </div>
      <div className="homepage__container__result__card__content">
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={IndustrySvg}
            alt="IndustrySvg"
            className="homepage__container__result__card__content__entry__svg"
          />
          <div className="homepage__container__result__card__content__entry__text">
            {data.Industry != null ? data.Industry.Title : "Not specified"}
          </div>
        </div>
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={moneySvg}
            alt="moneySvg"
            className="homepage__container__result__card__content__entry__svg"
          />
          <div className="homepage__container__result__card__content__entry__text">
            {data.BudgetTypeLookupDetail === "Negotiable"
              ? data.BudgetTypeLookupDetail
              : data.BudgetFrom
              ? data.BudgetTo
                ? "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                : "€ " + data.BudgetFrom
              : 0}
            {/* {data ? (
              data.BudgetFrom ? (
                data.BudgetTo ? (
                  "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                ) : (
                  "€ " + data.BudgetFrom
                )
              ) : (
                0
              )
            ) : (
              null
            )} */}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__badges">
        {data
          ? data.ProjectSkills.map((skill, i) => (
              <div className="homepage__container__result__card__badge" key={i}>
                {skill.Title}
              </div>
            ))
          : null}
      </div>
    </div>
  );
}
