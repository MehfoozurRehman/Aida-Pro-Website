import React, { useEffect, useState } from "react";

const RangeSelector = ({ searchByRange, radius }) => {
  const [rangeValues, setRangeValues] = useState([25, 50, 100, 150, 250, 500]);
  const [currentRangeValue, setCurrentRangeValue] = useState(5);

  useEffect(() => {
    if (radius == 0) setCurrentRangeValue(0)
    else if (radius == 25) setCurrentRangeValue(0)
    else if (radius == 50) setCurrentRangeValue(1)
    else if (radius == 100) setCurrentRangeValue(2)
    else if (radius == 150) setCurrentRangeValue(3)
    else if (radius == 250) setCurrentRangeValue(4)
    else if (radius == 500) setCurrentRangeValue(5)
    else setCurrentRangeValue(5);
  }, [radius])

  const handleInputChange = (e) => {
    setCurrentRangeValue(e.target.value);
    searchByRange(rangeValues[e.target.value]);
  };

  return (
    <div className="homepage__container__jumbotron__form__filters__location__range__wrapper">
      <div
        className="homepage__container__jumbotron__form__filters__location__range__value"
        id="output"
      >
        {rangeValues[currentRangeValue] == 500
          ? "500+ km"
          : rangeValues[currentRangeValue] + " km"}
      </div>
      <input
        className="homepage__container__jumbotron__form__filters__location__range"
        onChange={handleInputChange}
        type={"range"}
        min={0}
        value={currentRangeValue}
        // defaultValue={currentRangeValue}
        max={5}
        step={1}
        list={"tick-list"}
      />
      <datalist id="tick-list">
        <option>0</option>
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
      </datalist>
    </div>
  );
};

export default RangeSelector;
