import React from "react";
import LanguagePanel from "./LanguagePanel";
import Img from "react-cool-img";
import OutsideClickHandler from "react-outside-click-handler";

export default function HeaderNavLanguages({
  setIsLanguagePanelOpen,
  languageSelected,
  unitedstates,
  germany,
  isLanguagePanelOpen,
  onClickOutsideLanguage,
  setLanguageSelected,
}) {
  return (
    <>
      <button
        title="open / close language panel"
        className="language__btn"
        onClick={() => {
          setIsLanguagePanelOpen(true);
        }}
      >
        <Img
          loading="lazy"
          src={
            languageSelected === "en"
              ? unitedstates
              : languageSelected === "de"
              ? germany
              : unitedstates
          }
          alt="flag"
          className="language__btn__icon"
        />
        <div className="language__btn__name">
          {languageSelected === "en"
            ? "EN"
            : languageSelected === "de"
            ? "DE"
            : "EN"}
        </div>
      </button>
      {isLanguagePanelOpen ? (
        <OutsideClickHandler
          display="contents"
          onOutsideClick={onClickOutsideLanguage}
        >
          <LanguagePanel
            setLanguageSelected={setLanguageSelected}
            setIsLanguagePanelOpen={setIsLanguagePanelOpen}
          />
        </OutsideClickHandler>
      ) : null}
    </>
  );
}
