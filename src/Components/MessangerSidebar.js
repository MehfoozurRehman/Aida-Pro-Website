import React from "react";
import { Search } from "react-feather";
import Img from "react-cool-img";
import ChatUsers from "./ChatUsers";

export default function MessangerSidebar({
  chatSelected,
  onSearchUser,
  chatUsers,
  searchedUsers,
  onClickChatNode,
  selectedChat,
  showName,
  chat,
  onClickDecline,
  setVideoCallUserData,
  setIsRequestVideoCallOpen
}) {
  return (
    <div className="messenger__container__sidebar">
      <div className="messenger__container__sidebar__header">
        <div className="messenger__container__sidebar__heading">
          <Img loading="lazy" src={chatSelected} alt="chatSelected" />
          Contacts
        </div>
      </div>
      <div className="messenger__container__sidebar__search">
        <Search size={18} color="currentColor" />
        <input
          type="search"
          className="messenger__container__sidebar__search__input"
          placeholder="Search User"
          onChange={(event) => onSearchUser(event.currentTarget.value)}
        />
      </div>
      {chatUsers != null ? (
        <div className="messenger__container__sidebar__users">
          {searchedUsers != null
            ? searchedUsers.map((e) => (
              <ChatUsers
                data={e}
                onClickChatNode={onClickChatNode}
                selectedChat={selectedChat}
                showName={showName}
                onClickDecline={onClickDecline}
                setVideoCallUserData={setVideoCallUserData}
                setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
              />
            ))
            : chatUsers.map((e) => (
              <ChatUsers
                data={e}
                onClickChatNode={onClickChatNode}
                selectedChat={selectedChat}
                showName={showName}
                onClickDecline={onClickDecline}
                setVideoCallUserData={setVideoCallUserData}
                setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
              />
            ))}
        </div>
      ) : (
        <div className="messenger__container__sidebar__users messenger__container__sidebar__users__noChat">
          <Img
            loading="lazy"
            src={chat}
            alt="chat"
            className="messenger__container__sidebar__users__img"
          />

          {/* <div className="messenger__container__sidebar__users__text">
         No contacts here yet
        </div> */}
        </div>
      )}
    </div>
  );
}
