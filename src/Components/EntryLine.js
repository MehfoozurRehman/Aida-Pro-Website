import React from "react";
import { Text, View } from "@react-pdf/renderer";

function EntryLine({ Heading, subHeading }) {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 5,
      }}
    >
      <Text
        style={{
          marginRight: 20,
          width: "45%",
          fontSize: 10,
          color: "#0DCBA0",
          fontWeight: "bold",
        }}
      >
        {Heading}
      </Text>
      <Text
        style={{
          marginRight: 10,
          fontSize: 10,
          width: "55%",
          color: "#2C4C4C",
          fontWeight: "bold",
        }}
      >
        {subHeading}
      </Text>
    </View>
  );
}

export default EntryLine;
