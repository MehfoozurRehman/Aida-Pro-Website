import React, { useState } from "react";

export default function SearchFilterPanelBadge({
  data,
  searchThisTitle,
  selectedKeywordId,
}) {
  const [isSelected, setIsSelected] = useState(
    data.Id == selectedKeywordId ? true : false
  );
  const searchKeyword = (data, title) => {
    searchThisTitle(data, title);
  };

  return (
    <button
      type="button"
      onClick={() => {
        if (!isSelected) {
          if (data) {
            searchKeyword(data, data.Title);
          }
        } else {
          searchKeyword(null, "");
        }
      }}
      title={data ? data.Title : "No data"}
      className={
        data.Id == selectedKeywordId
          ? "homepage__container__jumbotron__form__filters__badge homepage__container__jumbotron__form__filters__badge__active"
          : "homepage__container__jumbotron__form__filters__badge"
      }
    >
      {data ? data.Title : "No data"}
    </button>
  );
}
