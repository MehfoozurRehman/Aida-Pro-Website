import React from "react";
import Avatar from "./Avatar";
import { userPic } from "Assets";

export default function CommentWithReply({ item }) {
  return (
    <form className="homepage__blog__artical__content__left__comment__container">
      <div className="homepage__blog__artical__content__left__comment__write__comment">
        <Avatar
          userPic={
            item.User.CompanyProfile !== null
              ? item.User.CompanyProfile.LogoUrl != null
                ? process.env.REACT_APP_BASEURL +
                  item.User.CompanyProfile.LogoUrl
                : userPic
              : item.User.Freelancer != null
              ? item.User.Freelancer.LogoUrl != null
                ? process.env.REACT_APP_BASEURL + item.User.Freelancer.LogoUrl
                : userPic
              : item.User.JobSeeker != null
              ? item.User.JobSeeker.LogoUrl != null
                ? process.env.REACT_APP_BASEURL + item.User.JobSeeker.LogoUrl
                : userPic
              : userPic
          }
        />
        <div className="homepage__blog__artical__content__left__comment__write__comment__showcase">
          <div className="homepage__blog__artical__content__left__comment__write__comment__name">
            {item.User.UserName}
          </div>
          <div className="homepage__blog__artical__content__left__comment__write__comment__para">
            {item.Description}
          </div>
        </div>
      </div>
    </form>
  );
}
