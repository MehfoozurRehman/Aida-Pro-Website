import React, { useEffect, useState } from "react";
import PostingListDropDownChoice from "./PostingListDropDownChoice";

export default function PostingListDropDown({
  statusData,
  changeStatusApiCall,
  recordType,
  currentStatus,
  index,
}) {
  const [isDropDownOpen, setIsDropDownOpen] = useState(false);
  const [selectedLabel, setSelectedLabel] = useState(currentStatus);
  const [statusDataRecieved, setStatusData] = useState();

  const [selectedBadgeColor, setSelectedBadgeColor] = useState(
    currentStatus === "Live"
      ? "linear-gradient(45deg,#0EE1A3,#0CA69D)"
      : currentStatus === "Draft"
        ? "linear-gradient(45deg, #f6a938, #f5833c)"
        : currentStatus === "Hold"
          ? "linear-gradient(45deg, #ffdd00, #e3eb00)"
          : currentStatus === "Closed"
            ? "linear-gradient(45deg, #DE3F3F, #FF6161)"
            : "#ffae0c"
  );

  useEffect(() => {
    setSelectedBadgeColor(currentStatus === "Live"
      ? "linear-gradient(45deg,#0EE1A3,#0CA69D)"
      : currentStatus === "Draft"
        ? "linear-gradient(45deg, #f6a938, #f5833c)"
        : currentStatus === "Hold"
          ? "linear-gradient(45deg, #ffdd00, #e3eb00)"
          : currentStatus === "Closed"
            ? "linear-gradient(45deg, #DE3F3F, #FF6161)"
            : "#ffae0c")
    setSelectedLabel(currentStatus);
  }, [currentStatus])

  useEffect(() => {

    if (statusDataRecieved) {
      changeStatusApiCall(selectedLabel, statusDataRecieved, recordType);
    }
  }, [selectedLabel, statusDataRecieved, selectedBadgeColor]);

  return (
    <button
      className="posting__container__table__list__entry__dropdown"
      onClick={() => {
        isDropDownOpen ? setIsDropDownOpen(false) : setIsDropDownOpen(true);
      }}
      title="show / hide dropdown"
    >
      <div
        className="posting__container__table__list__entry__dropdown__text "
        style={{
          background: selectedBadgeColor,
        }}
      >
        <div className="posting__container__table__list__entry__dropdown__text__label">
          {selectedLabel}
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 6 5">
          <path
            id="Polygon_25"
            data-name="Polygon 25"
            d="M3,0,6,5H0Z"
            transform="translate(6 5) rotate(180)"
            fill="#fff"
          />
        </svg>
      </div>
      {isDropDownOpen ? (
        <div className="posting__container__table__list__entry__dropdown__list">
          {statusData.map((status, i) => (
            <PostingListDropDownChoice
              index={index}
              key={i}
              label={status.label}
              btnBg={
                status.label === "Live"
                  ? "linear-gradient(45deg,#0EE1A3,#0CA69D)"
                  : status.label === "Draft"
                    ? "linear-gradient(45deg, #f6a938, #f5833c)"
                    : status.label === "Hold"
                      ? "linear-gradient(45deg, #ffdd00, #e3eb00)"
                      : status.label === "Closed"
                        ? "linear-gradient(45deg, #DE3F3F, #FF6161)"
                        : "#ffae0c"
              }
              setSelectedBadgeColor={setSelectedBadgeColor}
              setSelectedLabel={setSelectedLabel}
              statusDataObject={status}
              setStatusData={setStatusData}
            />
          ))}
        </div>
      ) : null}
    </button>
  );
}
