import React from "react";
import { Link } from "@reach/router";
import Img from "react-cool-img";
import { AidaLogo } from "Assets";
import { getText } from "Utils/functions";

export default function ProjectsCard({ data, isOnFreelancer }) {
  return (
    <>
      <Link
        to={
          isOnFreelancer
            ? "/home-freelancer/project-details"
            : "/home-professional/project-details"
        }
        state={{ dataObject: data }}
        className="homepage__container__result__card"
      >
        <Img
          loading="lazy"
          src={
            isOnFreelancer
              ? data
                ? data.FreelancerUploads.length > 0
                  ? process.env.REACT_APP_BASEURL +
                    data.FreelancerUploads[0].UploadFilePath
                  : AidaLogo
                : AidaLogo
              : data
              ? data.JobSeekerUploads.length > 0
                ? process.env.REACT_APP_BASEURL +
                  data.JobSeekerUploads[0].UploadFilePath
                : AidaLogo
              : AidaLogo
          }
          alt="project_img"
          className="homepage__container__result__card__img"
          style={data && data.ProjectName ? null : { marginBottom: 0 }}
        />
        {data && data.ProjectName ? (
          <div
            className="homepage__container__result__card__header"
            style={data && data.ProjectDescription ? null : { marginBottom: 0 }}
          >
            <div className="homepage__container__result__card__header__icon">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20.4"
                height="17"
                viewBox="0 0 20.4 17"
                fill="currentColor"
              >
                <g id="suitcase" transform="translate(0 -2)">
                  <path
                    id="Path_2267"
                    data-name="Path 2267"
                    d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                    transform="translate(-1.2)"
                  />
                  <path
                    id="Path_2268"
                    data-name="Path 2268"
                    d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                    transform="translate(0 -1.443)"
                  />
                  <path
                    id="Path_2269"
                    data-name="Path 2269"
                    d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                    transform="translate(0 -0.45)"
                  />
                </g>
              </svg>
            </div>
            <div className="homepage__container__result__card__header__content">
              <div className="homepage__container__result__card__header__name">
                {data && data.ProjectName}
              </div>
            </div>
          </div>
        ) : null}
        {data && data.ProjectDescription ? (
          <div className="homepage__container__result__card__content__info">
            {getText(data.ProjectDescription).length > 150 ? (
              <>{`${getText(data.ProjectDescription).substring(0, 150)}...`}</>
            ) : (
              <>{getText(data.ProjectDescription)}</>
            )}
          </div>
        ) : null}
        {data && data.BudgetFrom ? (
          <div className="homepage__container__result__card__content">
            <div className="homepage__container__result__card__content__entry">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="26.044"
                height="26.044"
                viewBox="0 0 26.044 26.044"
                fill="currentColor"
              >
                <g id="coins" transform="translate(0 0)">
                  <g
                    id="Group_998"
                    data-name="Group 998"
                    transform="translate(0 0)"
                  >
                    <path
                      id="Path_3189"
                      data-name="Path 3189"
                      d="M15.39,4.947V2.919C15.39-.973,0-.973,0,2.919V14.778c0,1.53,2.4,2.484,5.277,2.823-1.054.473-1.725,1.111-1.725,1.92v3.558c0,1.946,3.871,2.965,7.695,2.965s7.695-1.019,7.695-2.965V21.291c3.621-.1,7.1-1.109,7.1-2.955V7.663C26.044,5.205,19.913,4.3,15.39,4.947ZM7.695,1.184c4.213,0,6.511,1.173,6.511,1.776s-2.3,1.776-6.511,1.776S1.184,3.562,1.184,2.959,3.482,1.184,7.695,1.184ZM1.184,4.606A13.886,13.886,0,0,0,7.695,5.919a13.886,13.886,0,0,0,6.511-1.312v.588c-2.057.456-3.551,1.289-3.551,2.5v.35a18.775,18.775,0,0,1-2.96.242c-4.213,0-6.511-1.173-6.511-1.776v-1.9ZM11.838,16.445c.113.068.23.134.356.2-.118,0-.237-.01-.356-.013ZM1.184,8.158A13.886,13.886,0,0,0,7.695,9.47a20.066,20.066,0,0,0,2.96-.232v2.95a18.775,18.775,0,0,1-2.96.242c-4.213,0-6.511-1.173-6.511-1.776v-2.5Zm0,6.64V12.3a13.886,13.886,0,0,0,6.511,1.312,20.066,20.066,0,0,0,2.96-.232v2.95a18.775,18.775,0,0,1-2.96.242C3.482,16.573,1.184,15.4,1.184,14.8Zm16.574,8.287c0,.6-2.3,1.776-6.511,1.776s-6.511-1.173-6.511-1.776v-1.9a13.886,13.886,0,0,0,6.511,1.312,14.479,14.479,0,0,0,6.324-1.2l.187,0v1.79Zm-6.511-1.776c-4.213,0-6.511-1.173-6.511-1.776s2.3-1.776,6.511-1.776,6.511,1.173,6.511,1.776S15.459,21.309,11.246,21.309Zm13.614-2.96c0,.573-2.1,1.649-5.919,1.755v-.571c0-.74-.56-1.337-1.462-1.8.289.012.58.02.87.02a13.886,13.886,0,0,0,6.511-1.312v1.9Zm0-3.551c0,.6-2.3,1.776-6.511,1.776S11.838,15.4,11.838,14.8v-1.9a13.886,13.886,0,0,0,6.511,1.312,13.886,13.886,0,0,0,6.511-1.312Zm0-3.551c0,.6-2.3,1.776-6.511,1.776s-6.511-1.173-6.511-1.776v-1.9a13.885,13.885,0,0,0,6.511,1.312A13.885,13.885,0,0,0,24.86,9.342ZM18.349,9.47c-4.213,0-6.511-1.173-6.511-1.776s2.3-1.776,6.511-1.776S24.86,7.092,24.86,7.695,22.562,9.47,18.349,9.47Z"
                      transform="translate(0 0)"
                    />
                  </g>
                </g>
              </svg>
              <div className="homepage__container__result__card__content__entry__text">
                {data && data.BudgetFrom
                  ? data.BudgetTo
                    ? "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                    : "€ " + data.BudgetFrom
                  : data && data.LookupDetail.Title}
              </div>
            </div>
          </div>
        ) : null}
        <div className="homepage__container__result__card__badges">
          {data && data.ProjectSkills
            ? data &&
              data.ProjectSkills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Skill.Title}
                </div>
              ))
            : null}
        </div>
      </Link>
    </>
  );
}
