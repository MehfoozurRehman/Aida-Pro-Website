import React from "react";
import { Avatar, WriteComments } from "Components";

export default function CoffeeCornerDiscussionComments({
  getWriteCommentAndIdAndData,
  questions,
  getText,
}) {
  return (
    <div className="coffee__corner__new__discussion__card__right__header__comments">
      <WriteComments
        placeholder="Write comment"
        getWriteComment={getWriteCommentAndIdAndData}
        forumId={questions.ForumId}
        questions={questions}
      />
      <div className="homepage__blog__artical__content__left__messages">
        {questions.ForumAnswers.map((answer, i) => (
          <form
            className="homepage__blog__artical__content__left__comment__container"
            key={i}
          >
            <div className="homepage__blog__artical__content__left__comment__write__comment">
              <Avatar
                onClick={(event) => {
                  if (event.type != undefined) {
                    window.open(
                      process.env.REACT_APP_BASEURL.concat(
                        answer.User.Freelancer != null
                          ? answer.User.Freelancer.ProfilePicture
                          : answer.User.JobSeeker != null
                          ? answer.User.JobSeeker.ProfilePicture
                          : answer.User.CompanyProfile.LogoUrl
                      ),
                      "_Blank"
                    );
                  }
                }}
                userPic={
                  answer.User != null
                    ? answer.User.Freelancer != null
                      ? answer.User.Freelancer.ProfilePicture != "" &&
                        answer.User.Freelancer.ProfilePicture != null
                        ? process.env.REACT_APP_BASEURL.concat(
                            answer.User.Freelancer.ProfilePicture
                          )
                        : null
                      : answer.User.JobSeeker != null
                      ? answer.User.JobSeeker.ProfilePicture != "" &&
                        answer.User.JobSeeker.ProfilePicture != null
                        ? process.env.REACT_APP_BASEURL.concat(
                            answer.User.JobSeeker.ProfilePicture
                          )
                        : null
                      : answer.User.CompanyProfile != null
                      ? answer.User.CompanyProfile.LogoUrl != "" &&
                        answer.User.CompanyProfile.LogoUrl != null
                        ? process.env.REACT_APP_BASEURL.concat(
                            answer.User.CompanyProfile.LogoUrl
                          )
                        : null
                      : null
                    : null
                }
              />
              <div className="homepage__blog__artical__content__left__comment__write__comment__showcase">
                <div className="homepage__blog__artical__content__left__comment__write__comment__name">
                  {answer.User.LoginName}
                </div>
                <div className="homepage__blog__artical__content__left__comment__write__comment__para">
                  {getText(answer.Description).length > 350 ? (
                    <>{`${getText(answer.Description).substring(0, 350)}...`}</>
                  ) : (
                    <>{getText(answer.Description)}</>
                  )}
                </div>
              </div>
            </div>
          </form>
        ))}
      </div>
    </div>
  );
}
