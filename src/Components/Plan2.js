import React from "react";

export default function Plan2({ data, handlePlanData }) {
  return (
    <div className="plans__container__row__main__card animate__animated animate__fadeIn">
      <div className="plans__container__row__main__card__name">Plan 2</div>
      <div className="plans__container__row__main__card__heading">
        {data && data[1] && data[1].Title && data[1].Title}
      </div>
      <div className="plans__container__row__main__card__content">
        {data &&
          data[1] &&
          data[1].PlanDetails &&
          data[1].PlanDetails.map((e) => (
            <button
              className="plans__container__row__main__card__content__entry animate__animated animate__fadeInDown"
              onClick={() => handlePlanData(e)}
              title="plan"
            >
              <div className="plans__container__row__main__card__content__entry__heading">
                {e.Title}
              </div>
              <div className="plans__container__row__main__card__content__entry__sub__heading">
                {e.Description}
              </div>
            </button>
          ))}
      </div>
    </div>
  );
}
