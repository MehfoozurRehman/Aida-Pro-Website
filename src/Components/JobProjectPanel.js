import React, { useState, useEffect, useContext } from "react";
import HomeComapanySearchFilter from "./HomeComapanySearchFilter";
import { getAllKeywords } from "../API/Api";
import UserContext from "Context/UserContext";
import HomePageEmployementJobProjectResultCard from "Components/HomePageEmployementJobProjectResultCard";
import timeSvg from "../Assets/timeSvg.svg";
import Img from "react-cool-img";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import milesSvg from "../Assets/milesSvg.svg";
import employeeSvg from "../Assets/employeeSvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import { getText } from "Utils/functions";
import NoData from "./NoData";

export default function JobProjectPanel({
  descriptionHeading,
  responsibilitesHeading,
  isFreelancer,
  jobsList,
  setIsApplyForJob,
  searchByFilter,
  setIsLoginOpen,
  resetFilter,
  keyword,
  selectedLocation,
  selectedRange,
}) {
  const [title, setTitle] = useState("");
  let [keyWordTitle, setKeyWordTitle] = useState(keyword);
  const [id, setId] = useState(0);
  const [skills, setSkills] = useState([]);
  const [jobType, setJobType] = useState("");
  const [description, setDescription] = useState("");
  const [requirements, setRequirements] = useState("");
  const [jobLocation, setJobLocation] = useState("");
  let [location, setLocation] = useState(selectedLocation);
  const [projectDeadline, setProjectDeadline] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [searchBy, setSearchBy] = useState("Employment");
  const [searchByFreelancer, setSearchByFreelancer] = useState("Freelancer");
  const [keywords, setKeywords] = useState([]);
  const [jobStatusApiData, setJobStatusApiData] = useState();
  const [isInterestedCheck, setIsInterestedCheck] = useState(false);
  const [isAppliedCheck, setIsAppliedCheck] = useState(false);
  let [range, setRange] = useState(selectedRange);
  const [data, setData] = useState(null);
  const [noOfEmployees, setNoOfEmployees] = useState(null);
  const [industry, setIndustry] = useState(null);
  const [budget, setBudget] = useState(null);
  const user = useContext(UserContext);

  useEffect(() => {
    handleShowJobDetail(jobsList[0], isFreelancer);
  }, [jobsList]);

  const handleShowJobDetail = (selectedJobData, isFreelancer) => {
    setData(selectedJobData);
    setId(selectedJobData.Id);

    if (!isFreelancer) {
      setTitle(
        selectedJobData.JobTitle ? selectedJobData.JobTitle : "Not specified"
      );
      setSkills(selectedJobData.JobSkills);
      setJobType(
        selectedJobData.JobTypes
          ? selectedJobData.JobTypes.map(
              (item) => item.JobTypeDetailTitle
            ).join(", ")
          : "Not specified"
      );
      setJobLocation(selectedJobData.Location);
      setNoOfEmployees(selectedJobData.NoOfEmployees);
      setIndustry(selectedJobData.Company_Industry_Title);
      setDescription(selectedJobData.Description);
      setRequirements(selectedJobData.Requirements);
      if (selectedJobData.SalaryTypeLookup == null) setBudget("Salary");
      else {
        setBudget(
          selectedJobData.SalaryTypeLookup == "Negotiable"
            ? selectedJobData.SalaryTypeLookup
            : selectedJobData.SalaryFrom
            ? selectedJobData.SalaryTo
              ? "€ " +
                selectedJobData.SalaryFrom +
                " - € " +
                selectedJobData.SalaryTo
              : "€ " + selectedJobData.SalaryFrom
            : 0
        );
      }
    } else {
      setTitle(selectedJobData.Title);
      setSkills(selectedJobData.ProjectSkills);
      setJobLocation(selectedJobData.Location);
      setDescription(selectedJobData.Description);
      setProjectDeadline(selectedJobData.Deadline);
      if (selectedJobData.SalaryTypeLookup == null) setBudget("Salary");
      else {
        setBudget(
          selectedJobData.SalaryTypeLookup == "Negotiable"
            ? selectedJobData.SalaryTypeLookup
            : selectedJobData.SalaryFrom
            ? selectedJobData.SalaryTo
              ? "€ " +
                selectedJobData.SalaryFrom +
                " - € " +
                selectedJobData.SalaryTo
              : "€ " + selectedJobData.SalaryFrom
            : 0
        );
      }
    }
    // jobStatusAPI(!isFreelancer ? "Jobs" : "Projects", selectedJobData.Id);
  };

  // const jobStatusAPI = (type, Id) => {
  //   setIsLoading(true);
  //   jobStatus(type, user.Id, Id)
  //     .then(({ data }) => {
  //       setJobStatusApiData(data.result);
  //       if (data.result) {
  //         setIsInterestedCheck(data.result.IsInterested);
  //         setIsAppliedCheck(data.result.IsApplied);
  //       }
  //       setIsLoading(false);
  //     })
  //     .catch((err) => {
  //       setIsLoading(false);
  //     });
  // };

  const searchDataByTitle = (data) => {
    setKeyWordTitle((keyWordTitle = data));
  };

  const searchByLocation = (data) => {
    setLocation(data.formatted_address);
  };

  const searchByKeyword = (data) => {
    setKeyWordTitle((keyWordTitle = data));
  };

  const searchByUser = (freelancer, employee) => {
    setSearchBy(employee ? "Employment" : "");
    setSearchByFreelancer(freelancer ? "Freelancer" : "");
  };

  useEffect(() => {
    getAllKeywordsApi();
  }, []);

  const getAllKeywordsApi = () => {
    setIsLoading(true);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  const showApplyScreen = () => {
    localStorage.setItem("jobId", id);
    if (Object.keys(user).length != 0) {
      setIsApplyForJob(true);
    } else {
      setIsLoginOpen(true);
    }
  };

  const searchData = () => {
    searchByFilter(keyWordTitle, range, location);
  };

  const searchByRange = (value) => {
    setRange((range = value));
  };

  const resetFilters = () => {
    setKeywords([]);
    setKeyWordTitle((keyWordTitle = ""));
    setRange((range = 500));
    setLocation((location = ""));
    getAllKeywordsApi();
    resetFilter();
  };

  return (
    <div className="homepage__container__jobs__projects__penel">
      {keywords.length > 0 ? (
        <HomeComapanySearchFilter
          isMainSearch={true}
          keywords={keywords}
          searchByTitle={searchDataByTitle}
          searchByFilter={searchData}
          searchByLocation={searchByLocation}
          searchByKeyword={(e) => searchByKeyword(e)}
          title={keyword}
          searchByRange={searchByRange}
          searchBy={searchBy}
          searchByFreelancer={searchByFreelancer}
          searchByUser={searchByUser}
          location={location}
          resetFilter={() => resetFilters()}
        />
      ) : null}
      <div className="homepage__container__jobs__projects__penel__container">
        <div className="homepage__container__jobs__projects__penel__container__sidebar">
          {jobsList.length > 0 ? (
            <div className="homepage__container__results">
              {jobsList.map((item, i) => (
                <HomePageEmployementJobProjectResultCard
                  key={i}
                  data={item}
                  showJobDetail={handleShowJobDetail}
                  isFreelancer={isFreelancer}
                  checked={i === 0 ? true : false}
                />
              ))}
            </div>
          ) : (
            <NoData />
          )}
        </div>
        {id !== 0 ? (
          <div className="homepage__container__jobs__projects__penel__container__details">
            <div className="homepage__container__jobs__projects__penel__container__details__heading">
              {title}
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__feature">
              <div className="homepage__container__result__card__content">
                <div className="homepage__container__result__card__content">
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={timeSvg}
                      alt="timeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {jobType ? jobType : "Not specified"}
                      {/* Available */}
                    </div>
                  </div>

                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={milesSvg}
                      alt="milesSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {jobLocation != "" ? jobLocation : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={IndustrySvg}
                      alt="IndustrySvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {industry != null ? industry : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={employeeSvg}
                      alt="employeeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {noOfEmployees != null && noOfEmployees != "" ? (
                        getText(noOfEmployees).length > 10 ? (
                          <>{`${getText(noOfEmployees).substring(
                            0,
                            10
                          )}... employees`}</>
                        ) : (
                          <>{getText(noOfEmployees) + " employees"}</>
                        )
                      ) : (
                        "employees"
                      )}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={moneySvg}
                      alt="moneySvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {budget}
                    </div>
                  </div>
                </div>
              </div>

              {/* {jobType ? (
                <div className="homepage__container__result__card__content__entry">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="22.623"
                    height="23.964"
                    viewBox="0 0 22.623 23.964"
                    fill="currentColor"
                  >
                    <g
                      id="Group_990"
                      data-name="Group 990"
                      transform="translate(0.25 0.255)"
                      stroke="currentColor"
                      opacity="0.55"
                    >
                      <path
                        id="Path_2281"
                        data-name="Path 2281"
                        d="M1811.3,2038.608a9.383,9.383,0,1,1-9.361,9.355A9.394,9.394,0,0,1,1811.3,2038.608Zm.423,17.935a8.138,8.138,0,0,0,5.351-2.224l-.456-.5.484-.583.553.505a8.124,8.124,0,0,0,2.215-5.344l-.705-.038v-.747l.705-.034c.153-1.51-1.179-4.666-2.242-5.328l-.472.445-.582-.486.528-.582a8.606,8.606,0,0,0-5.4-2.228v.744h-.755l-.033-.748a8.66,8.66,0,0,0-5.37,2.232l.508.55-.52.553c-.12-.112-.2-.191-.289-.269s-.169-.153-.281-.254a8.657,8.657,0,0,0-2.229,5.366l.75.042v.746l-.7.034c-.16,1.5,1.17,4.652,2.241,5.329l.5-.473.548.525c-.117.125-.2.21-.274.3s-.151.17-.244.275a8.609,8.609,0,0,0,5.4,2.228v-.745h.777Z"
                        transform="translate(-1800.96 -2033.916)"
                        stroke="currentColor"
                        strokeWidth="0.5"
                      />
                      <path
                        id="Path_2282"
                        data-name="Path 2282"
                        d="M2015.993,1984.875a7.729,7.729,0,0,1-.819-.174c-.472-.148-.526-.479-.134-.793a2.692,2.692,0,0,0,1.069-2.006,2.719,2.719,0,0,0-1.01-2.309c-.141-.123-.321-.366-.281-.488a.733.733,0,0,1,.471-.381,3.121,3.121,0,0,1,3.666,4.312A3.116,3.116,0,0,1,2015.993,1984.875Zm.052-.8a2.321,2.321,0,0,0,2.092-1.167,2.212,2.212,0,0,0-.069-2.469,2.274,2.274,0,0,0-2.016-1.051A3.645,3.645,0,0,1,2016.045,1984.077Z"
                        transform="translate(-1997.16 -1978.602)"
                        stroke="currentColor"
                        strokeWidth="0.5"
                      />
                      <path
                        id="Path_2283"
                        data-name="Path 2283"
                        d="M1874.574,1987.85l-.5-.641.374-.435h-7.187v-.794h7.191l-.437-.541.536-.54c.417.426.824.807,1.181,1.232a.537.537,0,0,1-.035.511C1875.366,1987.052,1874.981,1987.423,1874.574,1987.85Z"
                        transform="translate(-1861.181 -1984.407)"
                        stroke="currentColor"
                        strokeWidth="0.5"
                      />
                      <path
                        id="Path_2284"
                        data-name="Path 2284"
                        d="M1792.957,1997.863a1.758,1.758,0,1,1-1.777-1.747A1.759,1.759,0,0,1,1792.957,1997.863Zm-.787.012a.97.97,0,1,0-.969.971A.975.975,0,0,0,1792.171,1997.875Z"
                        transform="translate(-1789.442 -1994.747)"
                        stroke="currentColor"
                        strokeWidth="0.5"
                      />
                      <path
                        id="Path_2296"
                        data-name="Path 2296"
                        d="M1870.046,2076.979c1.536.274,1.823.627,1.821,2.24,0,.6.015,1.2-.01,1.8a.506.506,0,0,0,.332.534c.855.441,1.7.9,2.587,1.376l-.357.689c-.632-.332-1.23-.643-1.824-.959-1.161-.618-2.325-1.231-3.477-1.866a.584.584,0,0,0-.7.02c-1.23.805-2.477,1.585-3.757,2.4l-.436-.651c.364-.232.688-.468,1.039-.653a.619.619,0,0,0,.35-.655c-.019-.807-.005-1.615,0-2.423a1.591,1.591,0,0,1,1.721-1.807,1.111,1.111,0,0,0,.149-.038,2.076,2.076,0,0,1-.681-1.8,1.913,1.913,0,0,1,.663-1.207,1.955,1.955,0,0,1,2.576.019C1870.885,2074.742,1870.888,2075.7,1870.046,2076.979Zm1.035,3.971c0-.8.005-1.539,0-2.275a.788.788,0,0,0-.878-.867q-1.466-.008-2.931,0a.784.784,0,0,0-.876.867c-.007.742,0,1.485,0,2.228a1.682,1.682,0,0,0,.043.246c.654-.415,1.276-.793,1.878-1.2a.688.688,0,0,1,.824-.034C1869.756,2080.267,1870.393,2080.587,1871.081,2080.951Zm-2.335-6.66a1.169,1.169,0,1,0,1.16,1.179A1.184,1.184,0,0,0,1868.746,2074.291Z"
                        transform="translate(-1858.376 -2066.077)"
                        stroke="currentColor"
                        strokeWidth="0.5"
                      />
                    </g>
                  </svg>
                  <div className="homepage__container__result__card__content__entry__text">
                    {jobType ? jobType : "Not specified"}
                  </div>
                </div>
                ) : (
                  null
                )}
                {jobLocation ? (
                <div className="homepage__container__result__card__content__entry">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="13.824"
                    height="18.568"
                    viewBox="0 0 13.824 18.568"
                    fill="currentColor"
                  >
                    <g id="place" opacity="0.55">
                      <g
                        id="Group_749"
                        data-name="Group 749"
                        transform="translate(0)"
                      >
                        <g id="Group_748" data-name="Group 748">
                          <path
                            id="Path_2297"
                            data-name="Path 2297"
                            d="M61.47,3.228a6.6,6.6,0,0,0-11.344,0,8.049,8.049,0,0,0-.8,6.908,6.023,6.023,0,0,0,1,1.8l5.019,6.406a.567.567,0,0,0,.91,0l5.017-6.4a6.029,6.029,0,0,0,1-1.794A8.051,8.051,0,0,0,61.47,3.228Zm-.317,6.458a4.722,4.722,0,0,1-.788,1.4l0,0L55.8,16.917l-4.567-5.828a4.724,4.724,0,0,1-.789-1.407,6.658,6.658,0,0,1,.667-5.715,5.454,5.454,0,0,1,9.378,0A6.659,6.659,0,0,1,61.153,9.686Z"
                            transform="translate(-48.886)"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_751"
                        data-name="Group 751"
                        transform="translate(3.567 4.05)"
                      >
                        <g id="Group_750" data-name="Group 750">
                          <path
                            id="Path_2298"
                            data-name="Path 2298"
                            d="M159.1,106.219a3.346,3.346,0,1,0,3.346,3.346A3.35,3.35,0,0,0,159.1,106.219Zm0,5.5a2.151,2.151,0,1,1,2.151-2.151A2.153,2.153,0,0,1,159.1,111.716Z"
                            transform="translate(-155.754 -106.219)"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>

                  <div className="homepage__container__result__card__content__entry__text">
                    {jobLocation ? jobLocation : "Not specified"}
                  </div>
                </div>
                ) : (
                  null
                )}
              </div> */}

              {isAppliedCheck ? (
                <a
                  className="header__nav__btn btn__secondary"
                  style={{
                    width: "150px",
                    height: "45px",
                    marginTop: "1em",
                    marginBottom: "2em",
                  }}
                >
                  Already Applied
                </a>
              ) : (
                <a
                  className="header__nav__btn btn__secondary"
                  style={{
                    width: "150px",
                    height: "45px",
                    marginTop: "1em",
                    marginBottom: "2em",
                  }}
                  onClick={() => showApplyScreen()}
                >
                  Apply
                </a>
              )}
            </div>
            <div className="homepage__container__result__card__badges">
              {skills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Title}
                </div>
              ))}
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__info">
              <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                {descriptionHeading}
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {getText(description).length > 1000 ? (
                  <>{`${getText(description).substring(0, 1000)}...`}</>
                ) : (
                  <>{getText(description)}</>
                )}
              </div>
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__info">
              <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
                {responsibilitesHeading}
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {getText(requirements).length > 1000 ? (
                  <>{`${getText(requirements).substring(0, 1000)}...`}</>
                ) : (
                  <>{getText(requirements)}</>
                )}
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}
