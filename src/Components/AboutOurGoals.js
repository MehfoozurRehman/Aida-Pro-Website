import React from "react";
import Img from "react-cool-img";
import { ourStorySvg } from "Assets";

export default function AboutOurGoals({}) {
  return (
    <div
      className="homepage__container__jumbotron__our__story"
      style={{
        margin: "7em 0em",
      }}
    >
      <div className="homepage__container__jumbotron__our__story__container">
        <div className="homepage__container__jumbotron__our__story__container__svg">
          <Img
            loading="lazy"
            src={ourStorySvg}
            alt="ourStorySvg"
            style={{
              width: "100%",
              height: "100%",
            }}
          />
        </div>
        <div className="homepage__container__jumbotron__our__story__container__content">
          <div className="homepage__container__jumbotron__our__story__container__content__heading">
            Our goals
          </div>
          <div className="homepage__container__jumbotron__our__story__container__content__para">
            • Assist our platform professionals in their careers <br /> •
            Develop AI and data initiatives in third world countries in Africa
            and Asia <br /> • Match professionals with the right companies, and
            vice versa <br /> • Knowledge platform for technical and other
            issues
          </div>
        </div>
      </div>
    </div>
  );
}
