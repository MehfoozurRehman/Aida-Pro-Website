import React from "react";

export default function PostingTableHeader({}) {
  return (
    <div className="posting__container__table__header">
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Title
      </div>
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Applicants
      </div>
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Interested
      </div>
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Visitors
      </div>
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Created
      </div>
      <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
        Status
      </div>
      <div
        className="posting__container__table__header__entry animate__animated animate__fadeInDown"
        style={{
          opacity: 0,
        }}
      >
        Delete
      </div>
    </div>
  );
}
