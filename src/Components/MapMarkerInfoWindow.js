import React from "react";
import { InfoWindow } from "@react-google-maps/api";

const MapMarkerInfoWindow = ({ data }) => {
  const { position, jobTitle, Location } = data;

  return (
    <InfoWindow position={position}>
      <div>
        <h3>{jobTitle}</h3> <h4>{Location}</h4>
      </div>
    </InfoWindow>
  );
};

export default MapMarkerInfoWindow;
