import React from "react";
import Img from "react-cool-img";
import { getText } from "Utils/functions";

export default function ChatMessage({ messageData, selectedChat, userId }) {
  return (
    <>
      {userId === messageData.from && selectedChat.Type === messageData.type ? (
        <>
          {messageData.attachment != null &&
            messageData.attachment != undefined ? (
            messageData.attachment_type == "image" ? (
              <div className="messenger__container__chat__images__entry">
                <button className="messenger__container__chat__images__entry__btn">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="feather feather-x"
                  >
                    <line x1="18" y1="6" x2="6" y2="18"></line>
                    <line x1="6" y1="6" x2="18" y2="18"></line>
                  </svg>
                </button>
                <Img
                  loading="lazy"
                  onClick={() => {
                    window.open(messageData.attachment, "_Blank");
                  }}
                  src={messageData.attachment}
                  alt="project_img"
                  width="200px"
                  style={{
                    aspectRatio: 0.56,
                    borderRadius: 10,
                  }}
                />
              </div>
            ) : (
              <div className="messenger__container__chat__images__entry">
                <div className="messenger__container__chat__files">
                  <div className="messenger__container__chat__files__top">
                    <button className="messenger__container__chat__images__entry__btn">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-x"
                      >
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                      </svg>
                    </button>
                    <div className="messenger__container__chat__files__file">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-file"
                      >
                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                        <polyline points="13 2 13 9 20 9"></polyline>
                      </svg>
                      Document
                    </div>
                    <a
                      href={messageData.attachment}
                      target="_BLANK"
                      className="messenger__container__chat__files__download"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-arrow-down"
                      >
                        <line x1="12" y1="5" x2="12" y2="19"></line>
                        <polyline points="19 12 12 19 5 12"></polyline>
                      </svg>
                    </a>
                  </div>
                  <div className="messenger__container__chat__messages__entry__content__timestamp">
                    {new Date(messageData.createdAt).toLocaleTimeString(
                      "en-US",
                      {
                        hour12: true,
                        hour: "numeric",
                        minute: "numeric",
                      }
                    )}
                  </div>
                </div>
              </div>
            )
          ) : null}

          {messageData.message != "" ? (
            <div className="messenger__container__chat__messages__entry ">
              <div className="messenger__container__chat__messages__entry__content">
                <div className="messenger__container__chat__messages__entry__content__text">
                  {getText(messageData.message)}
                </div>
                <div className="messenger__container__chat__messages__entry__content__timestamp">
                  {new Date(messageData.createdAt).toLocaleTimeString("en-US", {
                    hour12: true,
                    hour: "numeric",
                    minute: "numeric",
                  })}
                </div>
              </div>
            </div>
          ) : null}
        </>
      ) : (
        <>
          {messageData.attachment != null &&
            messageData.attachment != undefined ? (
            messageData.attachment_type == "image" ? (
              <div
                onClick={() => {
                  window.open(messageData.attachment, "_Blank");
                }}
                className="messenger__container__chat__images__entry messenger__container__chat__images__entry__others"
              >
                <Img
                  loading="lazy"
                  src={messageData.attachment}
                  alt="project_img"
                  width="200px"
                  style={{
                    aspectRatio: 0.56,
                    borderRadius: 10,
                  }}
                />
              </div>
            ) : (
              <div className="messenger__container__chat__images__entry messenger__container__chat__images__entry__others">
                <div className="messenger__container__chat__files">
                  <div className="messenger__container__chat__files__top">
                    <button className="messenger__container__chat__images__entry__btn">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-x"
                      >
                        <line x1="18" y1="6" x2="6" y2="18"></line>
                        <line x1="6" y1="6" x2="18" y2="18"></line>
                      </svg>
                    </button>
                    <div className="messenger__container__chat__files__file">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-file"
                      >
                        <path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path>
                        <polyline points="13 2 13 9 20 9"></polyline>
                      </svg>
                      Document
                    </div>
                    <a
                      href={messageData.attachment}
                      target="_BLANK"
                      className="messenger__container__chat__files__download"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18"
                        height="18"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        class="feather feather-arrow-down"
                      >
                        <line x1="12" y1="5" x2="12" y2="19"></line>
                        <polyline points="19 12 12 19 5 12"></polyline>
                      </svg>
                    </a>
                  </div>
                  <div className="messenger__container__chat__messages__entry__content__timestamp">
                    {new Date(messageData.createdAt).toLocaleTimeString(
                      "en-US",
                      {
                        hour12: true,
                        hour: "numeric",
                        minute: "numeric",
                      }
                    )}
                  </div>
                </div>
              </div>
            )
          ) : null}
          {messageData.message != "" ? (
            <div className="messenger__container__chat__messages__entry messenger__container__chat__messages__entry__others">
              <div className="messenger__container__chat__messages__entry__content">
                <div className="messenger__container__chat__messages__entry__content__text">
                  {getText(messageData.message)}
                </div>
                <div className="messenger__container__chat__messages__entry__content__timestamp">
                  {new Date(messageData.createdAt).toLocaleTimeString("en-US", {
                    hour12: true,
                    hour: "numeric",
                    minute: "numeric",
                  })}
                </div>
              </div>
            </div>
          ) : null}
        </>
      )}
    </>
  );
}
