import React from "react";
import Select from "react-select";
import _ from "lodash";

const dateOfPostingDDL = [
  { value: "7", label: "Last 7 days" },
  { value: "14", label: "Last 14 days" },
  { value: "30", label: "Last 30 days" },
];

export default function HomeEmploymentFilters({
  dateOfPosting,
  dateOfPostingChange,
  jobType,
  contractType,
  jobTypeChange,
  fieldChange,
  fieldWork,
  industryType,
  handleChangeInput,
  salaryFrom,
  salaryTo,
  jobSellerJobsApi,
  clearAllFilter,
}) {
  return (
    <div className="dashboard__container__main__filters">
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Date of posting
        </div>
        <div className="dashboard__container__main__filters__entry__select__date">
          <Select
            options={dateOfPostingDDL}
            value={dateOfPosting}
            onChange={(e) => dateOfPostingChange(e)}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Contract type
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            options={jobType}
            value={contractType}
            isMulti={true}
            onChange={(e) => jobTypeChange(e)}
          />
        </div>
      </div>
      <div className="dashboard__container__main__filters__entry">
        <div className="dashboard__container__main__filters__entry__name">
          Field of work
        </div>
        <div className="dashboard__container__main__filters__entry__select">
          <Select
            onChange={(e) => fieldChange(e)}
            value={fieldWork}
            options={industryType}
            isMulti={true}
          />
        </div>
      </div>
      <div
        className="dashboard__container__main__filters__entry"
        style={{
          minWidth: "300px",
        }}
      >
        <div className="dashboard__container__main__filters__entry__name">
          Salary estimation (monthly){" "}
          <b
            style={{
              marginLeft: 4,
            }}
          >
            €
          </b>
        </div>
        <div className="dashboard__container__main__filters__entry__select__wrapper">
          <div className="dashboard__container__main__filters__entry__select">
            <input
              name="minSalary"
              onChange={(event) => {
                const re = /^[0-9\b]+$/;

                if (
                  event.currentTarget.value === "" ||
                  re.test(event.currentTarget.value)
                ) {
                  handleChangeInput(event);
                }
              }}
              type="text"
              value={salaryFrom}
              placeholder="Min"
              className="dashboard__container__main__filters__entry__select__input"
            />
          </div>
          <div className="dashboard__container__main__filters__entry__spacer"></div>
          <div className="dashboard__container__main__filters__entry__select">
            <input
              name="maxSalary"
              onChange={(event) => {
                const re = /^[0-9\b]+$/;

                if (
                  event.currentTarget.value === "" ||
                  re.test(event.currentTarget.value)
                ) {
                  handleChangeInput(event);
                }
              }}
              type="text"
              value={salaryTo}
              placeholder="Max"
              className="dashboard__container__main__filters__entry__select__input"
            />
          </div>
        </div>
      </div>
      <div className="dashboard__company__container__left__form__view__filter">
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          type="button"
          onClick={() => jobSellerJobsApi()}
          title="search"
        >
          Search
        </button>
      </div>
      <div
        className="dashboard__company__container__left__form__view__filter"
        style={{
          marginLeft: ".8em",
        }}
      >
        <button
          className="dashboard__company__container__left__form__view__filter__btn"
          type="button"
          onClick={() => clearAllFilter()}
          style={{
            background: "#71797E",
            color: "#ffffff",
          }}
          title="clear all"
        >
          Clear all
        </button>
      </div>
    </div>
  );
}
