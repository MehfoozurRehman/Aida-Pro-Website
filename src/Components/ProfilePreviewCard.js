import React from "react";
import { getText } from "Utils/functions";

export default function ProfilePreviewCard({
  svg,
  heading,
  subHeading,
  is,
  maxLength,
  isDescription,
}) {
  if (is === "whiteText") {
    return (
      <div
        style={{ marginTop: "1em" }}
        className="dashboard__company__container__profile__preview__content"
      >
        <div className="dashboard__company__container__profile__preview__content">
          <div className="dashboard__company__container__profile__preview__content__svg">
            {svg}
          </div>
          <div className="dashboard__company__container__profile__preview__content__text">
            <div className="dashboard__company__container__profile__preview__content__text__upper__white__text">
              {heading}
            </div>
            <div className="dashboard__company__container__profile__preview__content__text__bottom__white__text">
              {subHeading}
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div
        style={{ marginTop: "1em" }}
        className="dashboard__company__container__profile__preview__content"
      >
        <div className="dashboard__company__container__profile__preview__content">
          <div className="dashboard__company__container__profile__preview__content__svg">
            {svg}
          </div>
          <div className="dashboard__company__container__profile__preview__content__text">
            <div className="dashboard__company__container__profile__preview__content__text__upper">
              {heading}
            </div>
            <div className="dashboard__company__container__profile__preview__content__text__bottom">
              {getText(subHeading).length > maxLength ? (
                <>{`${getText(subHeading).substring(0, maxLength)}..`}</>
              ) : (
                <>{getText(subHeading)}</>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
