import React from "react";
import { Trash2 } from "react-feather";
import { Avatar } from "Components";
import MessengerChatForm from "Components/MessengerChatForm";
import Img from "react-cool-img";
import MessangerChat from "Components/MessangerChat";

export default function MessangerContent({
  messages,
  chatNodeName,
  isOn,
  navigate,
  selectedChat,
  showName,
  isSelectedUserOnline,
  onClickDecline,
  onClickApprove,
  user,
  file,
  AlwaysScrollToBottom,
  newMessage,
  setNewMessage,
  CustomError,
  onClickSend,
  fileSelectedHandler,
  noChat,
  chatUsers,
}) {
  return (
    <>
      {messages != null && chatNodeName != "" ? (
        <div className="messenger__container__chat">
          <div className="messenger__container__chat__header">
            <Avatar
              onClick={() => {
                if (isOn === "company") {
                  navigate("/home-company/messenger-user-profile", {
                    state: {
                      redirectFrom: "Messenger",
                      jobseekerId: selectedChat.JobSeekerId,
                      freelancerId: selectedChat.FreelancerId,
                      selectedUser: selectedChat,
                    },
                  });
                }
              }}
              userPic={
                selectedChat.ProfilePicture != null &&
                selectedChat.ProfilePicture != ""
                  ? process.env.REACT_APP_BASEURL.concat(
                      selectedChat.ProfilePicture
                    )
                  : null
              }
            />
            <div className="messenger__container__chat__header__content">
              <div className="messenger__container__chat__header__name">
                {selectedChat != null ? showName(selectedChat) : null}
              </div>
              <div className="messenger__container__chat__header__status">
                <div
                  className={
                    isSelectedUserOnline
                      ? "messenger__container__chat__header__status__color__online"
                      : "messenger__container__chat__header__status__color__offline"
                  }
                ></div>
                <div className="messenger__container__chat__header__status__text">
                  {isSelectedUserOnline ? "Online" : "Offline"}
                </div>
              </div>
            </div>
            <div className="messenger__container__chat__header__controls">
              <button
                className="messenger__container__chat__header__controls__btn messenger__container__chat__header__controls__btn"
                title="Delete"
                onClick={() => onClickDecline()}
              >
                <Trash2 size={18} color="currentColor" strokeWidth={1.5} />
              </button>
            </div>
          </div>
          {selectedChat.IsChatRequestAccepted ? null : (
            <div className="messenger__container__chat__messages__top">
              {`Approve request to get in touch with the project owner ${selectedChat.CompanyEmail} to explore
                career opportunity. They won't know you've seen it until you
                accept their request.`}
              <div className="messenger__container__chat__messages__top__btns">
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    onClickApprove();
                  }}
                  style={{
                    height: 35,
                  }}
                  title="approve"
                >
                  Approve
                </button>
                <button
                  className="header__nav__btn btn__primary"
                  onClick={() => {
                    onClickDecline();
                  }}
                  style={{
                    height: 35,
                    background: "#303043",
                    boxShadow: "none",
                  }}
                  title="decline"
                >
                  Decline
                </button>
              </div>
            </div>
          )}
          {selectedChat.IsChatRequestAccepted ? (
            <MessangerChat
              user={user}
              messages={messages}
              selectedChat={selectedChat}
              file={file}
              AlwaysScrollToBottom={AlwaysScrollToBottom}
            />
          ) : null}
          <MessengerChatForm
            newMessage={newMessage}
            setNewMessage={setNewMessage}
            CustomError={CustomError}
            onClickSend={onClickSend}
            fileSelectedHandler={fileSelectedHandler}
            selectedChat={selectedChat}
          />
        </div>
      ) : (
        <div className="messenger__container__chat__form messenger__container__chat__form__no__chat__selected">
          <Img
            loading="lazy"
            src={noChat}
            alt="noChat"
            className="messenger__container__chat__form__no__chat__selected__img"
          />
          <div className="messenger__container__chat__form__no__chat__selected__text">
            {chatUsers != null
              ? "Please select a contact to start chating"
              : "No conversations yet"}
          </div>
        </div>
      )}
    </>
  );
}
