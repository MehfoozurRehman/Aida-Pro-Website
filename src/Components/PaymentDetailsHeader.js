import React from "react";

export default function PaymentDetailsHeader() {
  return (
    <div className="payment__details__container__entry__content__header">
      <div className="payment__details__container__entry__content__header__entry">
        Receipts
      </div>
      <div className="payment__details__container__entry__content__header__entry">
        Status
      </div>
      <div className="payment__details__container__entry__content__header__entry">
        Total
      </div>
      <div className="payment__details__container__entry__content__header__entry">
        Date of Invoice
      </div>
      <div className="payment__details__container__entry__content__header__entry">
        Date of Payment
      </div>
      <div className="payment__details__container__entry__content__header__entry">
        Invoice
      </div>
    </div>
  );
}
