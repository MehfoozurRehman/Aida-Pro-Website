import React from "react";

export default function DraftEntry({ onClick }) {
  return (
    <button title="open entry" className="job__draft__entry" onClick={onClick}>
      <div className="job__draft__entry__left">Machine Learning Engineer</div>
      <div className="job__draft__entry__right">
        Saved 3 Days Ago
        <button title="delete">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="13.57"
            height="13.57"
            viewBox="0 0 13.57 13.57"
          >
            <defs>
              <linearGradient
                id="linear-gradient"
                x1="0.5"
                x2="0.5"
                y2="1"
                gradientUnits="objectBoundingBox"
              >
                <stop offset="0" stop-color="#de3f3f" />
                <stop offset="1" stop-color="#ff6161" />
              </linearGradient>
            </defs>
            <path
              id="Icon_metro-cross"
              data-name="Icon metro-cross"
              d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
              transform="translate(-2.571 -1.928)"
              fill="url(#linear-gradient)"
            />
          </svg>
        </button>
      </div>
    </button>
  );
}
