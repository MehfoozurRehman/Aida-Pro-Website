import React, { useEffect, useState } from "react";
import Img from "react-cool-img";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import { getText } from "Utils/functions";

export default function HomePageFreelancerResultCard({ item, moveTo }) {
  const [interestedData, setInterestedData] = useState(null);

  useEffect(() => {
    bindData(item.Project != undefined ? item.Project : item.Job);
  }, []);

  const bindData = (data) => {
    let newObject = {
      ActionTypeId: data.ActionTypeId,
      AlteredById: data.AlteredById,
      AlteredOn: data.AlteredOn,
      BudgetFrom:
        data.BudgetFrom != undefined ? data.BudgetFrom : data.SalaryFrom,
      BudgetTo: data.BudgetTo != undefined ? data.BudgetTo : data.SalaryTo,
      BudgetTypeLookupDetailId: data.BudgetTypeLookupDetailId,
      CompanyProfile: data.CompanyProfile,
      CompanyProfileId: data.CompanyProfileId,
      CreatedById: data.CreatedById,
      CreatedOn: data.CreatedOn,
      Deadline: data.Deadline,
      Description: data.Description,
      Id: data.Id,
      Industry: data.Industry,
      IndustryId: data.IndustryId,
      IsShowCompanyDetail: data.IsShowCompanyDetail,
      Latitude: data.Latitude,
      Location: data.Location,
      Longitude: data.Longitude,
      LookupDetail: data.LookupDetail,
      details:
        data.ProjectDetails != undefined
          ? data.ProjectDetails
          : data.JobDetails,
      skills:
        data.ProjectSkills != undefined ? data.ProjectSkills : data.JobSkills,
      Requirment: data.Requirments,
      StartDate: data.StartDate,
      StatusTypeLookupDetail: data.StatusTypeLookupDetail,
      StatusTypeLookupDetailId: data.StatusTypeLookupDetailId,
      Title: data.Title != undefined ? data.Title : data.JobTitle,
      TotalApplied: data.TotalApplied,
      TotalInterested: data.TotalInterested,
      TotalRejected: data.TotalRejected,
      TotalVisited: data.TotalVisited,
    };
    setInterestedData(newObject);
  };

  const navigateToProject = () => {
    const requestData = {
      redirectFrom:
        item.Project != undefined
          ? "FreelancerInterested"
          : "EmployeeInterested",
      objectData: item.Project != undefined ? item.Project : item.Job,
      jobType: item.Project != undefined ? "Projects" : "Jobs",
    };
    // ;
    moveTo(requestData);
  };

  return (
    <div
      onClick={() => navigateToProject()}
      to="/project-details"
      className="homepage__container__result__card"
    >
      <div className="homepage__container__result__card__header">
        <div className="homepage__container__result__card__header__icon">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20.4"
            height="17"
            viewBox="0 0 20.4 17"
            fill="currentColor"
          >
            <g id="suitcase" transform="translate(0 -2)">
              <path
                id="Path_2267"
                data-name="Path 2267"
                d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                transform="translate(-1.2)"
              />
              <path
                id="Path_2268"
                data-name="Path 2268"
                d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                transform="translate(0 -1.443)"
              />
              <path
                id="Path_2269"
                data-name="Path 2269"
                d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                transform="translate(0 -0.45)"
              />
            </g>
          </svg>
        </div>
        <div className="homepage__container__result__card__header__content">
          {/* <div
            className="homepage__container__result__card__header__designation"
            style={{ marginBottom: "0.3em" }}
          >
            Full Time Remote Job
          </div> */}
          <div className="homepage__container__result__card__header__name">
            {interestedData ? interestedData.Title : "Not specified"}
          </div>
        </div>
      </div>
      <div className="homepage__container__result__card__content__info">
        {interestedData ? (
          getText(interestedData.Description).length > 80 ? (
            <>{`${getText(interestedData.Description).substring(0, 80)}...`}</>
          ) : (
            <>{getText(interestedData.Description)}</>
          )
        ) : (
          "Not specified"
        )}
      </div>
      <div className="homepage__container__result__card__content">
        <div className="homepage__container__result__card__content__entry">
          <Img
            loading="lazy"
            src={IndustrySvg}
            alt="IndustrySvg"
            className="homepage__container__result__card__content__entry__svg"
          />

          <div className="homepage__container__result__card__content__entry__text">
            {interestedData
              ? interestedData.Industry
                ? interestedData.Industry.Title
                : "Not specified"
              : "Not specified"}
          </div>
        </div>

        {item.Project != undefined ? (
          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={moneySvg}
              alt="moneySvg"
              className="homepage__container__result__card__content__entry__svg"
            />
            <div className="homepage__container__result__card__content__entry__text">
              {interestedData
                ? interestedData.BudgetTypeLookupDetail === "Negotiable"
                  ? interestedData.BudgetTypeLookupDetail
                  : interestedData.BudgetFrom
                  ? interestedData.BudgetTo
                    ? "€ " +
                      interestedData.BudgetFrom +
                      " - € " +
                      interestedData.BudgetTo
                    : "€ " + interestedData.BudgetFrom
                  : 0
                : "Not specified"}
            </div>
          </div>
        ) : (
          <div className="homepage__container__result__card__content__entry">
            <Img
              loading="lazy"
              src={experienceSvg}
              alt="experienceSvg"
              className="homepage__container__result__card__content__entry__svg"
            />
            <div className="homepage__container__result__card__content__entry__text">
              {interestedData != undefined ? (
                getText(interestedData.CompanyProfile.NoOfEmployees).length >
                10 ? (
                  <>{`${getText(
                    interestedData.CompanyProfile.NoOfEmployees
                  ).substring(0, 10)}... employees`}</>
                ) : (
                  <>
                    {getText(interestedData.CompanyProfile.NoOfEmployees) +
                      " employees"}
                  </>
                )
              ) : (
                "Not specified"
              )}
            </div>
          </div>
        )}
      </div>
      <div className="homepage__container__result__card__badges">
        {interestedData
          ? interestedData.skills.map((item, i) => (
              <div className="homepage__container__result__card__badge" key={i}>
                {item.Skill.Title}
              </div>
            ))
          : null}
      </div>
    </div>
  );
}
