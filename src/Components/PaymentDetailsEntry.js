import moment from "moment";
import React from "react";

export default function PaymentDetailsEntry({
  ReceiptsList,
  Status,
  Total,
  CreatedAt,
  PaidAt,
  setIsInvoiceOpen,
}) {
  return (
    <div className="payment__details__container__entry__content__header">
      <div className="payment__details__container__entry__content__row__entry">
        {ReceiptsList}
      </div>
      <div className="payment__details__container__entry__content__row__entry">
        {Status}
      </div>
      <div className="payment__details__container__entry__content__row__entry">
        {Total}
      </div>
      <div className="payment__details__container__entry__content__row__entry">
        {moment(CreatedAt).format("DD MMM,YYYY")}
      </div>
      <div className="payment__details__container__entry__content__row__entry">
        {moment(PaidAt).format("DD MMM,YYYY")}
      </div>
      <button
        className="header__nav__btn btn__primary"
        onClick={() => {
          setIsInvoiceOpen(true);
        }}
        style={{ width: 200, marginTop: -12 }}
        title="invoice"
      >
        Invoice
      </button>
    </div>
  );
}
