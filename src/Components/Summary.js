import React from "react";
import { Text, View } from "@react-pdf/renderer";

function Summary() {
  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        width: "100%",
        marginTop: 6,
      }}
    >
      <Text
        style={{
          fontSize: 10,
          color: "#0DCBA0",
          fontWeight: "bold",
          width: "40%",
        }}
      >
        Product Name
      </Text>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            fontSize: 10,
            color: "#2C4C4C",
            fontWeight: "bold",
            width: "20%",
          }}
        >
          1.00
        </Text>
        <Text
          style={{
            fontSize: 10,
            color: "#2C4C4C",
            fontWeight: "bold",
            width: "20%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          32,400.00
        </Text>
        <Text
          style={{
            fontSize: 10,
            color: "#2C4C4C",
            fontWeight: "bold",
            width: "20%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          32,400.00
        </Text>
      </View>
    </View>
  );
}

export default Summary;
