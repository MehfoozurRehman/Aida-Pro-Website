import React from "react";
import { aboutUsSvg } from "Assets";
import Img from "react-cool-img";

export default function AboutUsJumbotron() {
  return (
    <div className="homepage__container__jumbotron">
      <div className="homepage__container__jumbotron__wrapper homepage__container__jumbotron__wrapper__reverse__left">
        <div className="homepage__container__jumbotron__left">
          <div
            className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
            style={{
              fontSize: 30,
            }}
          >
            <span>About Us</span>
            <br />
            Who we are?
          </div>
          <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft">
            AIDApro is founded by a group of managers and data freaks who are
            all truly passionate about AI and data. With different professional
            backgrounds and from all over the world, Our platform helps the AI
            and data community to further flourish.
          </div>
        </div>
        <Img
          loading="lazy"
          src={aboutUsSvg}
          alt="aboutUsSvg"
          className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          style={{
            height: "unset",
            maxWidth: 500,
          }}
        />
      </div>
    </div>
  );
}
