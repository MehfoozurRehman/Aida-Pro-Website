import React, { useContext } from "react";
import Img from "react-cool-img";
import { PopupContext } from "Routes/AppRouter";
import { getText } from "Utils/functions";

export default function PostingDetailsProtfolioCard({ item }) {
  const { isGalleryOpen, setIsGalleryOpen } = useContext(PopupContext);
  return (
    <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry">
      <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__heading">
        {item.ProjectName}
      </div>
      <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__content">
        {getText(item.ProjectDescription).length > 1000 ? (
          <>{`${getText(item.ProjectDescription).substring(0, 1000)}...`}</>
        ) : (
          <>{getText(item.ProjectDescription)}</>
        )}
      </div>
      <div
        className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__images"
        // onClick={() => {
        //   setIsGalleryOpen(true);
        //   window.scrollTo({ top: 0, behavior: "smooth" });
        // }}
      >
        {item.Uploads.map((item, i) => (
          <div
            onClick={() => {
              setIsGalleryOpen(true);
              window.scrollTo({ top: 0, behavior: "smooth" });
              window.open(
                process.env.REACT_APP_BASEURL.concat(item.UploadFilePath),
                "_Blank"
              );
            }}
          >
            <Img
              loading="lazy"
              key={i}
              onclick
              src={process.env.REACT_APP_BASEURL.concat(item.UploadFilePath)}
              alt="portfolioImg"
              className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__images__entry"
            />
          </div>
        ))}
      </div>
    </div>
  );
}
