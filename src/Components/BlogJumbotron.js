import React from "react";
import Img from "react-cool-img";
import { blogPageBackgroundSvg } from "Assets";

export default function BlogJumbotron({}) {
  return (
    <div className="homepage__container__jumbotron">
      <div className="homepage__container__jumbotron__wrapper">
        <div className="homepage__container__jumbotron__left">
          <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
            Blogs are dedicated to our <br />
            <span>AI and data community</span>.
          </div>
          <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft">
            Would you like to contribute or help us?
            <br />
            We highly appreciate comments or corrections, please just contact
            us.
            <br />
            Enjoy the readings :)
          </div>
          <div
            className="homepage__container__jumbotron__form__input"
            style={{
              position: "relative",
            }}
          >
            <input
              type="text"
              className="homepage__container__jumbotron__form__input__field"
              placeholder="Search"
            />
            <button
              className="homepage__container__jumbotron__form__button"
              title="search button"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="currentColor"
                viewBox="0 0 21.681 21.681"
              >
                <g
                  id="Icon_feather-search"
                  data-name="Icon feather-search"
                  transform="translate(1.5 1.5)"
                >
                  <path
                    id="Path_2299"
                    data-name="Path 2299"
                    d="M20.553,12.526A8.026,8.026,0,1,1,12.526,4.5,8.026,8.026,0,0,1,20.553,12.526Z"
                    transform="translate(-4.5 -4.5)"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="3"
                  />
                  <path
                    id="Path_2300"
                    data-name="Path 2300"
                    d="M29.339,29.339l-4.364-4.364"
                    transform="translate(-11.28 -11.28)"
                    fill="none"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="3"
                  />
                </g>
              </svg>
            </button>
          </div>
        </div>
        <Img
          loading="lazy"
          src={blogPageBackgroundSvg}
          alt="blogPageBackgroundSvg"
          className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
        />
      </div>
    </div>
  );
}
