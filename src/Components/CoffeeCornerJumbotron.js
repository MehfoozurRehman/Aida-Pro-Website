import React from "react";
import Img from "react-cool-img";
import HomeComapanySearchFilter from "Components/HomeComapanySearchFilter";

export default function CoffeeCornerJumbotron({
  tags,
  searchByTitle,
  getQuestionsBySearch,
  limit,
  pageNumber,
  keyword,
  tagId,
  searchByKeyword,
  setIsAskQuestionOpen,
  setIsLoginOpen,
  coffeeCornerSvg,
  user,
  resetFilter,
}) {
  return (
    <div className="homepage__container__jumbotron">
      <div className="homepage__container__jumbotron__wrapper">
        <div className="homepage__container__jumbotron__left">
          <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
            <span>Ask questions, participate in discussions</span>
          </div>
          <div className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft"></div>
          <HomeComapanySearchFilter
            placeholder="Search Topics or discussions"
            isOnCoffeeCorner={true}
            keywords={tags}
            searchByTitle={(e) => {
              searchByTitle(e);
            }}
            searchByFilter={() =>
              getQuestionsBySearch(limit, pageNumber, keyword, tagId)
            }
            searchByKeyword={(e) => {
              searchByKeyword(e);
            }}
            title={keyword}
            resetFilter={resetFilter}
          />
          <button
            onClick={() => {
              if (Object.keys(user).length != 0) {
                setIsAskQuestionOpen(true);
              } else {
                setIsLoginOpen(true);
              }
            }}
            type="button"
            className="header__nav__btn btn__secondary"
            style={{
              width: 150,
              marginTop: "2em",
              height: 50,
            }}
            title="ask question"
          >
            Ask Question
          </button>
        </div>
        <Img
          loading="lazy"
          src={coffeeCornerSvg}
          alt="coffeeCornerSvg"
          className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
        />
      </div>
    </div>
  );
}
