import React from "react";
import Img from "react-cool-img";
import { appleappbadge, googleplaybadge, mobileApp } from "Assets";

export default function MobileAppAdvert() {
  return (
    <div className="homepage__container">
      <div className="homepage__container__app">
        <Img
          loading="lazy"
          src={mobileApp}
          alt="app__picture"
          className="homepage__container__app__left"
        />
        <div className="homepage__container__app__right">
          <div className="homepage__container__qualification__heading">
            Find us on
          </div>
          <div className="homepage__container__app__right__buttons">
            <a href="#" className="homepage__container__app__right__button">
              <Img
                loading="lazy"
                src={appleappbadge}
                alt="play store link"
                className="homepage__container__app__right__button__img"
              />
            </a>
            <a href="#" className="homepage__container__app__right__button">
              <Img
                loading="lazy"
                src={googleplaybadge}
                alt="play store link"
                className="homepage__container__app__right__button__img__play"
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
