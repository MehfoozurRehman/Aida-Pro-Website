import React from "react";
import Img from "react-cool-img";
import moment from "moment";
import { workexperianceimg, AidaLogo } from "Assets";
import { getText } from "Utils/functions";

export default function PostingDetailsExperianceCard({
  onClick,
  data,
  removeExperience,
}) {
  return (
    <button
      title="work experiance"
      className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry"
    >
      <div
        style={{ textAlign: "left", flex: 1, width: "100%" }}
        onClick={() => onClick(data)}
      >
        <Img
          loading="lazy"
          src={AidaLogo}
          alt="workexperianceimg"
          className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__img"
        />
        <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info">
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__heading">
            {data.jobTitle ? data.jobTitle : "Title"}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__duration"></div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__designation">
            {data.jobCompany ? data.jobCompany : null}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__date">
            {data ? moment(data.jobFrom).format("MMMM DD, YYYY") : null} -{" "}
            {data
              ? data.jobTo != null
                ? moment(data.jobTo).format("MMMM DD, YYYY")
                : "Present"
              : null}
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__content">
          {getText(data.jobDescription).length > 1000 ? (
            <>{`${getText(data.jobDescription).substring(0, 1000)}...`}</>
          ) : (
            <>{getText(data.jobDescription)}</>
          )}
        </div>
      </div>
      {removeExperience != undefined ? (
        <button
          title="remove experience"
          onClick={() => removeExperience()}
          className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__entry__del"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="8.08"
            height="8.08"
            viewBox="0 0 13.08 13.08"
            fill="currentColor"
          >
            <g id="cancel-close-svgrepo-com" transform="translate(0 -0.016)">
              <g
                id="Group_1767"
                data-name="Group 1767"
                transform="translate(0 0.016)"
              >
                <path
                  id="Path_22069"
                  data-name="Path 22069"
                  d="M7.981,6.556l4.89-4.891a.717.717,0,0,0,0-1.012L12.443.225a.717.717,0,0,0-1.012,0L6.54,5.115,1.65.225a.717.717,0,0,0-1.012,0L.209.653a.716.716,0,0,0,0,1.012L5.1,6.556.209,11.446a.717.717,0,0,0,0,1.012l.429.428a.717.717,0,0,0,1.012,0L6.54,8l4.89,4.89a.71.71,0,0,0,.506.209h0a.71.71,0,0,0,.506-.209l.429-.428a.717.717,0,0,0,0-1.012Z"
                  transform="translate(0 -0.016)"
                />
              </g>
            </g>
          </svg>
        </button>
      ) : null}
    </button>
  );
}
