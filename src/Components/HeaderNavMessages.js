import React from "react";
import MessagesPanel from "./MessagesPanel";
import OutsideClickHandler from "react-outside-click-handler";

export default function HeaderNavMessages({
  isMessagePanelOpen,
  setIsMessagePanelOpen,
  setIsNotificationPanelOpen,
  setIsLanguagePanelOpen,
  setIsMenuOpen,
  setUnreadMessagesLength,
  isOn,
  unreadMessagesLength,
  onClickOutsideMessages,
  messageNotificationData,
}) {
  return (
    <>
      <button
        title="open / close message panel"
        className="header__nav__icon__btn"
        onClick={() => {
          if (messageNotificationData.length > 0) {
            if (!isMessagePanelOpen) {
              setIsMessagePanelOpen(true);
              setIsNotificationPanelOpen(false);
              setIsLanguagePanelOpen(false);

              if (window.innerWidth < 800) {
                setIsMenuOpen(false);
              }

              setUnreadMessagesLength(null);
            } else {
              setIsMessagePanelOpen(false);
            }
          } else {
            if (isOn == "company")
              window.location.href = "/home-company/messenger";
            else if (isOn == "professional")
              window.location.href = "/home-professional/messenger";
            else window.location.href = "/home-freelancer/messenger";
          }
        }}
      >
        {unreadMessagesLength === null ? null : (
          <div className="header__nav__icon__btn__badge">
            {unreadMessagesLength.length}
          </div>
        )}
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="29.225"
          height="29.225"
          viewBox="0 0 29.225 29.225"
        >
          <defs>
            <linearGradient
              id="linear-gradient"
              x1="0.316"
              y1="0.784"
              x2="0.949"
              y2="0.15"
              gradientUnits="objectBoundingBox"
            >
              <stop offset="0" stopColor="#0ee1a3" />
              <stop offset="1" stopColor="#0ca69d" />
            </linearGradient>
            <linearGradient
              id="linear-gradient-2"
              x1="0.756"
              y1="0.176"
              x2="-0.27"
              y2="1.201"
              gradientUnits="objectBoundingBox"
            >
              <stop offset="0" stopColor="#00a2f3" stopOpacity="0" />
              <stop offset="1" stopColor="#0075cd" />
            </linearGradient>
          </defs>
          <g id="chat_1_" data-name="chat (1)" transform="translate(0 -9.884)">
            <path
              id="Path_3916"
              data-name="Path 3916"
              d="M14.84,9.886A14.623,14.623,0,0,0,.343,27.657,26.5,26.5,0,0,1,1,33.363v3.713a1.03,1.03,0,0,0,1.03,1.03H5.746a26.5,26.5,0,0,1,5.706.66A14.614,14.614,0,1,0,14.84,9.886Z"
              transform="translate(0 0)"
              fill="url(#linear-gradient)"
            />
            <circle
              id="Ellipse_368"
              data-name="Ellipse 368"
              cx="2.263"
              cy="2.263"
              r="2.263"
              transform="translate(5.471 22.999)"
              fill="#fff"
            />
            <circle
              id="Ellipse_369"
              data-name="Ellipse 369"
              cx="2.263"
              cy="2.263"
              r="2.263"
              transform="translate(11.872 22.999)"
              fill="#fff"
            />
            <path
              id="Path_3919"
              data-name="Path 3919"
              d="M261.385,195.392a2.261,2.261,0,1,0-3.2,3.2l4.882,4.882a2.261,2.261,0,1,0,3.2-3.2Z"
              transform="translate(-239.202 -171.679)"
              fill="url(#linear-gradient-2)"
            />
            <circle
              id="Ellipse_370"
              data-name="Ellipse 370"
              cx="2.263"
              cy="2.263"
              r="2.263"
              transform="translate(18.273 22.999)"
              fill="#fff"
            />
          </g>
        </svg>
      </button>
      {isMessagePanelOpen ? (
        <OutsideClickHandler
          display="contents"
          onOutsideClick={onClickOutsideMessages}
        >
          <MessagesPanel setIsMessagePanelOpen={setIsMessagePanelOpen} />
        </OutsideClickHandler>
      ) : null}
    </>
  );
}
