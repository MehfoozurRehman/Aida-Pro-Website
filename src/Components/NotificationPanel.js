import React from "react";
import Notification from "./Notification";

export default function NotificationPanel({
  notificationData,
  updateNotificationDataAcceptReject,
  setIsNotificationPanelOpen,
}) {
  return (
    <div className="header__notification__panel animate__animated animate__fadeIn">
      <div className="header__notification__panel__header">
        <div className="header__notification__panel__header__heading">
          Notifications
        </div>
        <button title="clear" className="header__notification__panel__btn">
          Clear All
        </button>
      </div>
      <div className="header__notification__panel__content">
        {notificationData != null
          ? notificationData.map((item, i) => (
            <Notification
              key={i}
              item={item}
              updateNotificationDataAcceptReject={
                updateNotificationDataAcceptReject
              }
              setIsNotificationPanelOpen={setIsNotificationPanelOpen}
            />
          ))
          : null}
      </div>
    </div>
  );
}
