import React, { useState, useContext, useEffect } from "react";
import { interestedJob, interestedProject } from "../API/Api";
import UserContext from "Context/UserContext";
import { CustomError } from "../Screens/Toasts";
import { LoadingMask } from "../Screens/LoadingMask";
import moment from "moment";
import timeSvg from "../Assets/timeSvg.svg";
import locationSvg from "../Assets/locationSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import { getText } from "Utils/functions";
import { useSelector } from "react-redux";
import Img from "react-cool-img";

export default function JobProjectDetailsCard({
  isFreelancer,
  setIsApplyForJob,
  isRandom,
  data,
  callBackToGetInterested,
  setIsApplyForJobProject,
  isApplyForJobProject,
  jobStatusApiData,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) {
  const isOn = window.localStorage.getItem("isOn");
  const [isInterested, setIsInterested] = useState(false);
  const [isJobApplied, setIsJobApplied] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const user = useContext(UserContext);

  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);
  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    if (isOn != "company") {
      if (isApplyForJobProject) {
        callBackToGetInterested();
        setIsApplyForJobProject(false);
      }
    }
  }, [isApplyForJobProject]);

  useEffect(() => {
    if (isOn != "company") isInterestedAndAppliedCheck();
  }, [jobStatusApiData]);

  const isInterestedAndAppliedCheck = () => {
    let isInterestedCheck = false;
    let isJobCheck = false;
    if (jobStatusApiData) {
      isInterestedCheck = jobStatusApiData.IsInterested;
      isJobCheck = jobStatusApiData.IsApplied;
    }
    setIsInterested(isInterestedCheck);
    setIsJobApplied(isJobCheck);
  };

  const profileCompleted = () => {
    if (jobsekker.skills.length > 0 && jobsekker.qualification.length > 0)
      return true;
    else return false;
  };

  const navigateToApplyJob = () => {
    if (profileCompleted()) {
      localStorage.setItem("jobId", data.Id);
      setIsApplyForJob(true);
    } else {
      setHeadingAndTextForAlertDialog(
        "Please complete your professional detail to apply, thanks"
      );
      setAlertDialogVisibility(true);
    }
  };

  const submitInterested = () => {
    if (isFreelancer) submitProjectInterested();
    else submitJobInterested();
  };

  const submitProjectInterested = () => {
    // setIsLoading(true);
    let interested = false;
    if (jobStatusApiData) {
      interested = jobStatusApiData.IsInterested;
    }
    let requestData = {
      ProjectId: data.Id,
      FreelancerId: user.FreelancerId,
      IsInterested: !interested,
    };

    interestedProject(requestData)
      .then(({ responseData }) => {
        if (requestData.IsInterested) callBackToGetInterested();
        setIsLoading(false);
      })
      .catch((err) => {
        CustomError("Failed to Interested Project.");
        setIsLoading(false);
      });
  };

  const submitJobInterested = () => {
    let interested = false;
    if (jobStatusApiData) {
      interested = jobStatusApiData.IsInterested;
    }
    setIsInterested(!interested);
    let requestData = {
      JobId: data.Id,
      JobSeekerId: user.JobSeekerId,
      IsInterested: !interested,
    };

    interestedJob(requestData)
      .then(({ res }) => {
        if (requestData.IsInterested) callBackToGetInterested();
        setIsLoading(false);
      })
      .catch((err) => {
        CustomError("Failed to Interested Job.");
      });
  };

  console.log("data", data);

  return isLoading ? (
    <LoadingMask />
  ) : (
    <div className="homepage__container__jobs__projects__penel__container">
      <div
        className="homepage__container__jobs__projects__penel__container__details"
        style={isRandom ? { width: "100%" } : null}
      >
        <div className="homepage__container__jobs__projects__penel__container__details__heading">
          {data.Title}
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__feature">
          <div className="homepage__container__result__card__content">
            <div className="homepage__container__result__card__content__entry">
              <Img
                loading="lazy"
                src={timeSvg}
                alt="timeSvg"
                className="homepage__container__result__card__content__entry__svg"
              />
              <div className="homepage__container__result__card__content__entry__text">
                {data.Deadline
                  ? moment(data.Deadline).format("DD MMM, YYYY")
                  : data.JobTypeLookupDetail
                  ? data.JobTypeLookupDetail
                  : "Not specified"}
              </div>
            </div>

            <div className="homepage__container__result__card__content__entry">
              <Img
                loading="lazy"
                src={locationSvg}
                alt="locationSvg"
                className="homepage__container__result__card__content__entry__svg"
              />
              <div className="homepage__container__result__card__content__entry__text">
                {data.Location}
              </div>
            </div>
            <div className="homepage__container__result__card__content__entry">
              <Img
                loading="lazy"
                src={IndustrySvg}
                alt="IndustrySvg"
                className="homepage__container__result__card__content__entry__svg"
              />
              <div className="homepage__container__result__card__content__entry__text">
                {data.Industry != null
                  ? data.Industry.Title
                  : data.CompanyDetail != null
                  ? data.CompanyDetail.Industry.Title
                  : "Not specified"}
              </div>
            </div>
            {isFreelancer ? (
              <div className="homepage__container__result__card__content__entry">
                <Img
                  loading="lazy"
                  src={moneySvg}
                  alt="moneySvg"
                  className="homepage__container__result__card__content__entry__svg"
                />
                <div className="homepage__container__result__card__content__entry__text">
                  {data.BudgetTypeLookupDetail === "Negotiable"
                    ? data.BudgetTypeLookupDetail
                    : data.BudgetFrom
                    ? data.BudgetTo
                      ? "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                      : "€ " + data.BudgetFrom
                    : 0}
                </div>
              </div>
            ) : (
              <div className="homepage__container__result__card__content__entry">
                <Img
                  loading="lazy"
                  src={experienceSvg}
                  alt="experienceSvg"
                  className="homepage__container__result__card__content__entry__svg"
                />
                <div className="homepage__container__result__card__content__entry__text">
                  {data.CompanyDetail != null &&
                  data.CompanyDetail != undefined ? (
                    getText(data.CompanyDetail.NoOfEmployees).length > 10 ? (
                      <>{`${getText(data.CompanyDetail.NoOfEmployees).substring(
                        0,
                        10
                      )}... employees`}</>
                    ) : (
                      <>
                        {getText(data.CompanyDetail.NoOfEmployees) +
                          " employees"}
                      </>
                    )
                  ) : (
                    "Not specified"
                  )}
                </div>
              </div>
            )}

            <div className="homepage__container__result__card__content__entry">
              <Img
                loading="lazy"
                src={moneySvg}
                alt="moneySvg"
                className="homepage__container__result__card__content__entry__svg"
              />
              <div className="homepage__container__result__card__content__entry__text">
                {data.SalaryTypeLookup != null
                  ? data.SalaryTypeLookup.Title == "Negotiable"
                    ? data.SalaryTypeLookup.Title
                    : data.SalaryFrom
                    ? data.SalaryTo
                      ? "€ " + data.SalaryFrom + " - € " + data.SalaryTo
                      : "€ " + data.SalaryFrom
                    : 0
                  : data.BudgetTypeLookupDetail.Title == "Negotiable"
                  ? data.BudgetTypeLookupDetail.Title
                  : data.BudgetFrom
                  ? data.BudgetTo
                    ? "€ " + data.BudgetFrom + " - € " + data.BudgetTo
                    : "€ " + data.BudgetFrom
                  : 0}
              </div>
            </div>
          </div>

          <div className="homepage__container__result__card__content__right__buttons">
            {jobStatusApiData ? (
              <>
                {isJobApplied ? (
                  <button
                    title="already applied"
                    className="header__nav__btn btn__secondary"
                    style={{
                      width: "150px",
                      height: "50px",
                      marginBottom: "1em",
                    }}
                  >
                    Already Applied
                  </button>
                ) : (
                  <button
                    title="apply"
                    onClick={() => navigateToApplyJob()}
                    className="header__nav__btn btn__secondary"
                    style={{
                      width: "150px",
                      height: "50px",
                      marginBottom: "1em",
                    }}
                  >
                    Apply
                  </button>
                )}
                <button
                  title="interested / already interested button"
                  onClick={() => submitInterested()}
                  className={
                    isInterested
                      ? "header__nav__btn btn__primary"
                      : "homepage__container__result__card__content__right__buttons__intrested"
                  }
                  style={{ width: "150px", height: "50px" }}
                >
                  {isInterested ? "Already interested" : "Interested"}
                </button>
              </>
            ) : null}
          </div>
        </div>

        <div className="homepage__container__result__card__badges">
          {isFreelancer
            ? data.ProjectSkills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Title}
                </div>
              ))
            : data.JobSkills.map((skill, i) => (
                <div
                  className="homepage__container__result__card__badge"
                  key={i}
                >
                  {skill.Skill != undefined ? skill.Skill.Title : skill.Title}
                </div>
              ))}
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__info">
          <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
            {isFreelancer ? "Project" : "Job"} description
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content">
            {getText(data.Description)}
          </div>
        </div>
        {data.Requirements != null ? (
          <div className="homepage__container__jobs__projects__penel__container__details__info">
            <div className="homepage__container__jobs__projects__penel__container__details__info__heading">
              {isFreelancer ? "Project" : "Job"} requirements
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__content">
              {getText(data.Requirements)}
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}
