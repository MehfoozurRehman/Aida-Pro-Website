import React from "react";
import firebase from "firebase/app";

export function getBase64(file, cb) {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    cb(reader.result);
  };
  reader.onerror = function (error) {
    // console.log(error);
  };
}

const fileSelectedHandler = async (event) => {
  const element = event.target.files[0];
  await getBase64(element, (result) => {
    this.setState({
      fileString: result,
      file: result,
      readyToSave: true,
    });
  });
};

export const alarmAlert = (alert) => {
  try {
    const audio = new Audio(alert);
    audio.currentTime = 0;
    audio.play();
  } catch (error) {
    // console.log(error);
  }
};

export const isUserLoggedIn = (userData) => {
  if (Object.keys(userData).length !== 0) return true;
  else return false;
};

export const createMarkup = (description) => {
  return { __html: description };
};

export const checkOnlineOffline = (firebaseUid) => {
  var userStatusDatabaseRef = firebase.database().ref("/status/" + firebaseUid);

  var isOfflineForDatabase = {
    state: "offline",
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  var isOnlineForDatabase = {
    state: "online",
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  firebase
    .database()
    .ref(".info/connected")
    .on("value", function (snapshot) {
      userStatusDatabaseRef
        .onDisconnect()
        .set(isOfflineForDatabase)
        .then(function () {
          userStatusDatabaseRef.set(isOnlineForDatabase);
        });
    });
};

export const changeStateOfUserOffline = (firebaseUid) => {
  var userStatusDatabaseRef = firebase.database().ref("/status/" + firebaseUid);

  var isOfflineForDatabase = {
    state: "offline",
    last_changed: firebase.database.ServerValue.TIMESTAMP,
  };

  firebase
    .database()
    .ref(".info/connected")
    .on("value", function (snapshot) {
      userStatusDatabaseRef
        .onDisconnect()
        .set(isOfflineForDatabase)
        .then(function () { });
    });
};

export const calculateAge = (dob) => {
  if (dob == "" && dob == null) return ""
  else {
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
}

export const getDate10YearsAgo = () => {
  var myCurrentDate = new Date();
  var maxDate = new Date(myCurrentDate);
  return maxDate.setDate(maxDate.getDate() - 3652);
}

export const getTotalYearsExperience = (experience) => {
  let value = (Math.round(experience / 0.05) * 0.05).toFixed(0);
  if (value == 0) return "Fresh"
  else return value + " Years of Experience";
}