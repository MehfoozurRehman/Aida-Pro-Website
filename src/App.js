import React from "react";
import { AppRouter } from "Routes/AppRouter";
import { connect } from "react-redux";
import UserContext from "Context/UserContext";
import { superToken } from "API/Api";
import { getUser } from "Redux/Actions/AppActions";
import { LoadingMask } from "Screens/LoadingMask";
import { setLoadingFalse } from "Redux/Actions/AppActions";
import { ToastContainer } from "react-toastify";

class App extends React.Component {
  componentDidMount() {
    if (window.location.href == "http://aidapro.com" + window.location.pathname)
      window.location.href = "https://aidapro.com" + window.location.pathname
    const userId = window.localStorage.getItem("userId");
    if (userId === undefined || userId === null) {
      window.localStorage.removeItem("token");
      superToken()
        .then(({ data }) => {
          if (data.access_token) {
            localStorage.setItem("token", data.access_token);
            this.props.store.dispatch(setLoadingFalse());
          }
        })
        .catch((err) => {
          console.log("Error", err);
        });
    } else {
      this.props.getUser(userId);
    }
  }

  render() {
    if (this.props.isLoading) {
      return <LoadingMask />;
    } else {
      return (
        <UserContext.Provider value={this.props.user.user}>
          {/* <ComingSoon /> */}
          <AppRouter />
          <ToastContainer
            position="top-right"
            autoClose={3000}
            hideProgressBar={true}
            newestOnTop
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </UserContext.Provider>
      );
    }
  }
}

const mapStateToProps = ({ user, appLoading }) => ({
  user,
  isLoading: appLoading.isLoading,
});

export default connect(mapStateToProps, { getUser })(App);
