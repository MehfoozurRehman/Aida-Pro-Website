import React from "react";
import { Provider } from "react-redux";
import ReactDOM from "react-dom";
//styles imports start
import "./Styles/index.scss";
import "animate.css";
import "react-toastify/dist/ReactToastify.css";
//styles imports end
import App from "App";
import reportWebVitals from "reportWebVitals";
import { registerServiceWorker } from "./serviceWorker";
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { USER_LOGOUT } from "./Redux/constants";
import UserReducer from "./Redux/Reducers/UserReducer";
import AppLoadingReducer from "./Redux/Reducers/AppLoadingReducer";
import CompanyReducer from "./Redux/Reducers/CompanyReducer";
import JobSekeerReducer from "./Redux/Reducers/JobSekeerReducer";
import FreelancerReducer from "./Redux/Reducers/FreelancerReducer";
import VideoCallReducer from "./Redux/Reducers/VideoCallReducer";
import NotificationReducer from "Redux/Reducers/NotificationReducer";
import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDYso5KV4oK0diHno1Q5oSKDA8dWknOKaM",
  authDomain: "aida-dev-chat.firebaseapp.com",
  databaseURL: "https://aida-dev-chat-default-rtdb.firebaseio.com",
  projectId: "aida-dev-chat",
  storageBucket: "aida-dev-chat.appspot.com",
  messagingSenderId: "882477990543",
  appId: "1:882477990543:web:bdc3912d7afee1bbf366fa",
  measurementId: "G-0T59ECX61E",
};

if (!firebase.apps.length) firebase.initializeApp(firebaseConfig);
else firebase.app();

<script
  async
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk&language=en&libraries=places&callback=initMap"
  type="text/javascript"
></script>;

const appReducer = combineReducers({
  user: UserReducer,
  appLoading: AppLoadingReducer,
  company: CompanyReducer,
  jobsekker: JobSekeerReducer,
  freelancer: FreelancerReducer,
  video: VideoCallReducer,
  notification: NotificationReducer,
});

const rootReducer = (state, action) => {
  if (action.type === USER_LOGOUT) {
    state = undefined;
  }
  return appReducer(state, action);
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <App store={store} />
  </Provider>,
  document.getElementById("root")
);

reportWebVitals();
registerServiceWorker();
