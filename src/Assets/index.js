export { default as NotFoundImg } from "./404-not-found.png";
export { default as aboutUsSvg } from "./aboutUsSvg.svg";
export { default as accurateJobMatches } from "./accurateJobMatches.svg";
export { default as AdressSvg } from "./AdressSvg.svg";
export { default as AIDALOADINGGIF } from "./AIDALOADINGGIF.gif";
export { default as AidaLogo } from "./AidaLogo.jpeg";
export { default as aidaLogo } from "./aidaLogo.svg";
export { default as appleappbadge } from "./apple-app-badge.png";
export { default as googleplaybadge } from "./google-play-badge.png";
export { default as mobileApp } from "./mobileApp.png";
export { default as workexperianceimg } from "./workexperianceimg.png";
export { default as EmailSvg } from "./EmailSvg.svg";
export { default as PhoneSvg } from "./PhoneSvg.svg";
export { default as SocialsSvg } from "./SocialsSvg.svg";
export { default as UserActive } from "./UserActive.svg";
export { default as userInActive } from "./userInActive.svg";
export { default as ProjectActive } from "./ProjectActive.svg";
export { default as ProjectInactive } from "./ProjectInactive.svg";
export { default as WorkActive } from "./WorkActive.svg";
export { default as WorkInactive } from "./WorkInactive.svg";
export { default as ContactActive } from "./ContactActive.svg";
export { default as ContactInactive } from "./ContactInactive.svg";
export { default as EducationActive } from "./EducationActive.svg";
export { default as EducationInactive } from "./EducationInactive.svg";
export { default as homePageCompanySvg } from "./homePageCompanySvg.svg";
export { default as hassleFreeJobRecruitment } from "./hassleFreeJobRecruitment.svg";
export { default as screeningByExperts } from "./screeningByExperts.svg";
export { default as vacancy } from "./vacancy.svg";
export { default as qualificationCheck } from "./qualificationCheck.svg";
export { default as ourStorySvg } from "./ourStorySvg.svg";
export { default as ourVision } from "./ourVision.svg";
export { default as ourTeam } from "./ourTeam.svg";
export { default as filteredSkills } from "./filteredSkills.png";
export { default as bestIdea } from "./bestIdea.png";
export { default as coming__soon__img } from "./coming__soon__img.png";
export { default as tempAvatar } from "./tempAvatar.png";
export { default as userPic } from "./userPic.png";
export { default as germany } from "./germany.png";
export { default as unitedstates } from "./unitedstates.png";
export { default as homePageBackground } from "./homePageBackground.png";
export { default as homeSelected } from "./homeSelected.svg";
export { default as homeNotSelected } from "./homeNotSelected.svg";
export { default as noData } from "./noData.svg";
export { default as DeleteSvg } from "./DeleteSvg.svg";
export { default as EditSvg } from "./EditSvg.svg";
export { default as pictureplaceholder } from "./pictureplaceholder.png";
export { default as askQuestion } from "./askQuestion.png";
export { default as HomeSignupSvg } from "./HomeSignupSvg.svg";
export { default as HourlyRateSvg } from "./HourlyRateSvg.svg";
export { default as ProfileSvg } from "./ProfileSvg.svg";
export { default as PlaneSvg } from "./PlaneSvg.svg";
export { default as ProfessionalSvg } from "./ProfessionalSvg.svg";
export { default as postProjectSvg } from "./postProjectSvg.svg";
export { default as postJobSvg } from "./postJobSvg.svg";
export { default as lock } from "./lock.png";
export { default as postingSvg } from "./postingSvg.svg";
export { default as balanceSvg } from "./balanceSvg.svg";
export { default as coffeeCornerSvg } from "./coffeeCornerSvg.svg";
export { default as blogPageBackgroundSvg } from "./blogPageBackgroundSvg.svg";
export { default as contactSvg } from "./contactSvg.svg";
export { default as mainsvgcolored1 } from "./mainsvgcolored1.svg";
export { default as mainsvgcolored2 } from "./mainsvgcolored2.svg";
export { default as mainsvgcolored3 } from "./mainsvgcolored3.svg";
export { default as homePageEmploymentSvg } from "./homePageEmploymentSvg.svg";
export { default as applyForJob } from "./applyForJob.svg";
export { default as planSvg } from "./planSvg.svg";
export { default as profilePreviewAvatarSvg } from "./profilePreviewAvatarSvg.svg";
export { default as FreelancerProfileDetailsSvg } from "./FreelancerProfileDetailsSvg.svg";
export { default as profilePreviewEmailSvg } from "./profilePreviewEmailSvg.svg";
export { default as offlineSvg } from "./offlineSvg.svg";
export { default as chatSelected } from "./chatSelected.svg";
export { default as chatNotSelected } from "./chatNotSelected.svg";
export { default as chat } from "./chat.svg";
export { default as noChat } from "./noChat.svg";
export { default as InvoiceLogo } from "./InvoiceLogo.png";
export { default as profileNotSelected } from "./profileNotSelected.svg";
export { default as profileSelected } from "./profileSelected.svg";
export { default as postingNotSelected } from "./postingNotSelected.svg";
export { default as postingSelected } from "./postingSelected.svg";
export { default as interestedSelected } from "./interestedSelected.svg";
export { default as interestedNotSelected } from "./interestedNotSelected.svg";
export { default as myProjectSelected } from "./myProjectSelected.svg";
export { default as myProjectNotSelected } from "./myProjectNotSelected.svg";
export { default as postJobSelected } from "./postJobSelected.svg";
export { default as postJobNotSelected } from "./postJobNotSelected.svg";
export { default as postProjectSelected } from "./postProjectSelected.svg";
export { default as postProjectNotSelected } from "./postProjectNotSelected.svg";
export { default as PlanNotSelected } from "./PlanNotSelected.svg";
export { default as PlanSelected } from "./PlanSelected.svg";
export { default as BalanceSelected } from "./BalanceSelected.svg";
export { default as BalanceNotSelected } from "./BalanceNotSelected.svg";
export { default as facetimeButtonActive } from "./facetimeButtonActive.svg";
export { default as facetimeButtonInActive } from "./facetimeButtonInActive.svg";
export { default as appliedActive } from "./appliedActive.svg";
export { default as appliedInActive } from "./appliedInActive.svg";

export const crossSvg = (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="14.372"
    height="14.372"
    viewBox="0 0 14.372 14.372"
  >
    <defs>
      <linearGradient
        id="linear-gradient"
        x1="0.5"
        x2="0.5"
        y2="1"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#0ee1a3" />
        <stop offset="1" stopColor="#0ca69d" />
      </linearGradient>
    </defs>
    <path
      id="Icon_metro-cross"
      data-name="Icon metro-cross"
      d="M16.812,13.474h0l-4.36-4.36,4.36-4.36h0a.45.45,0,0,0,0-.635l-2.06-2.06a.45.45,0,0,0-.635,0h0l-4.36,4.36L5.4,2.059h0a.45.45,0,0,0-.635,0L2.7,4.119a.45.45,0,0,0,0,.635h0l4.36,4.36L2.7,13.474h0a.45.45,0,0,0,0,.635l2.06,2.06a.45.45,0,0,0,.635,0h0l4.36-4.36,4.36,4.36h0a.45.45,0,0,0,.635,0l2.06-2.06a.45.45,0,0,0,0-.635Z"
      transform="translate(-2.571 -1.928)"
      fill="url(#linear-gradient)"
    />
  </svg>
);
