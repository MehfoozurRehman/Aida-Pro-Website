export const LOGIN_FORM_INITIAL_VALUES = {
  email: "",
  password: "",
};

export const JOB_POST_INITIAL_VALUES = {
  jobTitle: "zzzz",
  jobLocation: "",
  jobSkills: [],
  jobType: "",
  jobEducation: "",
  jobSalaryType: "",
  jobDescription: "",
  jobRequirements: "",
  companyDetailSave: false,
  jobPostingDate: "",
};
export const PROJECT_POST_INITIAL_VALUES = {
  projectTitle: "",
  projectLocation: "",
  projectSkill: [],
  projectDeadline: "",
  projectBudgetType: "",
  projectDescription: "",
  projectBudgetFrom: "",
  projectBudgetTo: "",
  projectStartDate: "",
  projectFixedAmount: "",
};

export const COMPANY_SIGNUP_FORM_INITIAL_VALUES = {
  companyName: "",
  contactPerson: "",
  email: "",
  country: "",
  password: "",
  confirmPassword: "",
  acceptTerms: false,
};

export const OTHERS_SIGNUP_FORM_INITIAL_VALUES = {
  fname: "",
  lname: "",
  email: "",
  country: "",
  password: "",
  confirmPassword: "",
  acceptTerms: false,
};

export const PERSONAL_DETAILS_COMPANY_VALUES = {
  companyName: "",
  companyContantPerson: "",
  companyEmailAddress: "",
  companyCellNo: "",
  companyBranch: "",
  companyNoOfEmp: "",
  companyAddress: "",
  companyAddressZipCode: "",
  companyAddressCity: "",
  companyAddressCountry: "",
  lat: "",
  lng: "",
  zoom: "",
};

export const PORTFOLIO_PROJECT_DETAILS = {
  portfolioProjectName: "",
  portfolioProjectDisciption: "",
  portfolioProjectImage: null,
};

export const CHANGE_PASSWORD = {
  oldPassword: "",
  newPassword: "",
  confirmPassword: "",
};

export const DETAIL_FORM = {
  firstName: "",
  lastName: "",
  emailAddress: "",
  phoneNo: "",
  gender: "",
  age: "",
  primaryAddress: "",
  primaryZipCode: "",
  primaryCity: "",
  primaryCountry: "",
  language: "",
  lat: "",
  lng: "",
  zoom: "",
};

export const PROFESSIONAL_DETAILS_SKILLS = {
  skills: [],
};

export const PROFESSIONAL_DETAILS_CERTIFICATIONS = {
  certificateName: "",
  institueName: "",
  credentialId: "",
  credentialUrl: "",
  dateFrom: "",
  dateTo: "",
};

export const PROFESSIONAL_DETAILS_EDUCATION = {
  educationType: "",
  startMonth: "",
  startYear: "",
  institueName: "",
  fieldOfStudy: "",
  grades: "",
  endMonth: "",
  endYear: "",
};

export const PROFESSIONAL_DETAILS_EXPERIENCE = {
  jobTitle: "",
  jobDescription: "",
  jobEmployementType: "",
  jobIndustryType: "",
  jobCompany: "",
  jobLocation: "",
  jobFrom: "",
  jobTo: "",
};

export const APPLIED_JOB = {
  Email: "",
  CountryId: "",
  Phone: "",
  AdditionalNotes: "",
  UploadCv: "",
};

export const REQ_INVITAION_VIDEO_CALL = {
  reqVideoCallNote: "",
  reqVideoCallTime: "",
  reqVideoCallDate: "",
  reqVideoEndCallTime: "",
};
