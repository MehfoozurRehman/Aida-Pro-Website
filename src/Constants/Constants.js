export const tableTabs = [
  {
    id: "tab1",
    label: "Open",
    status: "Live",
  },
  {
    id: "tab2",
    label: "Draft",
    status: "Draft",
  },
  {
    id: "tab3",
    label: "Closed",
    status: "Closed",
  },
  {
    id: "tab4",
    label: "Hold",
    status: "Hold",
  },
  {
    id: "tab5",
    label: "All",
    status: "All",
  },
];
export const months = [
  {
    label: "January",
    value: "January",
  },
  { label: "February", value: "February" },
  { label: "March", value: "March" },
  { label: "April", value: "April" },
  { label: "May", value: "May" },
  { label: "June", value: "June" },
  { label: "July", value: "July" },
  { label: "August", value: "August" },
  { label: "September", value: "September" },
  { label: "October", value: "October" },
  { label: "November", value: "November" },
  { label: "December", value: "December" },
];

function generateArrayOfYears() {
  var max = new Date().getFullYear();
  var min = max - 100;
  var years = [];

  // for (var i = max; i >= min; i--) {
  for (let index = 1960; index <= max; index++) {
    years.push({ label: index, value: index });
  }
  return years.reverse();
}

export var years = generateArrayOfYears();
