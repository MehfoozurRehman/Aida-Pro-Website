import * as Yup from "yup";

export const LoginFormValidationSchema = Yup.object().shape({
  email: Yup.string().email("Invalid Email").required("Email is required."),
  password: Yup.string().required("Password is required.").min(8),
});

export const CompanySignUpFormValidationSchema = Yup.object().shape({
  companyName: Yup.string()
    .required("*Required")
    .min(3, "*Must be greater than 3character"),
  contactPerson: Yup.string()
    .required("Required.")
    .min(3, "*Must be greater than 3 character"),
  country: Yup.string()
    .required("*Required.")
    .min(3, "*Must be greater than 3 character"),
  email: Yup.string().email("Invalid Email").required("*Required."),
  password: Yup.string()
    .required("*Required.")
    .min(8, "*Must be greater than 8 character"),
  confirmPassword: Yup.string()
    .required("*Required.")
    .when("password", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Both password need to be the same"
      ),
    }),
  acceptTerms: Yup.bool().oneOf([true], ""),
});

export const OthersSignUpFormValidationSchema = Yup.object().shape({
  fname: Yup.string().required("*Required.").min(3),
  lname: Yup.string().required("*Required.").min(3),
  country: Yup.string().required("*Required.").min(3),
  email: Yup.string().email("Invalid Email").required("*Required."),
  password: Yup.string().required("*Required.").min(8),
  confirmPassword: Yup.string()
    .required("*Required.")
    .when("password", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("password")],
        "Both password need to be the same"
      ),
    }),
  acceptTerms: Yup.bool().oneOf([true], ""),
});

export const FormikPostJobValidationSchemaRange = Yup.object().shape({
  jobTitle: Yup.string().required("*Required.").min(3),
  jobLocation: Yup.string().required("*Required."),
  skills: Yup.mixed().required("*Required."),
  jobType: Yup.mixed().required("*Required."),
  jobEducation: Yup.mixed().required("*Required."),
  jobSalaryType: Yup.mixed().required("*Required."),
  jobDescription: Yup.mixed().required("*Required."),
  jobRequirements: Yup.mixed().required("*Required."),
  jobPostingDate: Yup.mixed().required("*Required."),
  salaryFrom: Yup.number().integer().required("*Required."),
  salaryTo: Yup.number()
    .integer()
    .required("*Required.")
    .moreThan(Yup.ref("salaryFrom"), "Must be greater than salary to"),
});
export const FormikPostJobValidationSchemaFixed = Yup.object().shape({
  jobTitle: Yup.string().required("*Required.").min(3),
  jobLocation: Yup.string().required("*Required."),
  skills: Yup.mixed().required("*Required."),
  jobType: Yup.mixed().required("*Required."),
  jobEducation: Yup.mixed().required("*Required."),
  jobSalaryType: Yup.mixed().required("*Required."),
  jobDescription: Yup.mixed().required("*Required."),
  jobRequirements: Yup.mixed().required("*Required."),
  jobPostingDate: Yup.mixed().required("*Required."),
  jobFixedAmount: Yup.string().required("*Required."),
});

export const FormikPostJobValidationSchema = Yup.object().shape({
  jobTitle: Yup.string().required("*Required.").min(3),
  jobLocation: Yup.string().required("*Required."),
  skills: Yup.mixed().required("*Required."),
  jobType: Yup.mixed().required("*Required."),
  jobEducation: Yup.mixed().required("*Required."),
  jobSalaryType: Yup.mixed().required("*Required."),
  jobDescription: Yup.mixed().required("*Required."),
  jobRequirements: Yup.mixed().required("*Required."),
  jobPostingDate: Yup.mixed().required("*Required."),
});

export const FormikPostProjectValidationSchema = Yup.object().shape({
  projectTitle: Yup.string().required("*Required.").min(3),
  projectLocation: Yup.string().required("*Required."),
  skill: Yup.mixed().required("*Required."),
  projectDeadline: Yup.mixed().required("*Required."),
  //projectStartDate: Yup.mixed().required("*Required."),
  projectBudgetType: Yup.mixed().required("*Required."),
  projectDescription: Yup.mixed().required("*Required."),
});

export const FormikPostProjectValidationSchemaFixed = Yup.object().shape({
  projectTitle: Yup.string().required("*Required.").min(3),
  projectLocation: Yup.string().required("*Required."),
  skill: Yup.mixed().required("*Required."),
  projectDeadline: Yup.mixed().required("*Required."),
  // projectStartDate: Yup.mixed().required("*Required."),
  projectBudgetType: Yup.mixed().required("*Required."),
  projectFixedAmount: Yup.mixed().required("*Required."),
  projectDescription: Yup.mixed().required("*Required."),
});

export const FormikPostProjectValidationSchemaRange = Yup.object().shape({
  projectTitle: Yup.string().required("*Required.").min(3),
  projectLocation: Yup.string().required("*Required."),
  skill: Yup.mixed().required("*Required."),
  projectDeadline: Yup.mixed().required("*Required."),
  // projectStartDate: Yup.mixed().required("*Required."),
  projectBudgetType: Yup.mixed().required("*Required."),
  projectDescription: Yup.mixed().required("*Required."),
  BudgetFrom: Yup.number().integer().required("*Required."),
  BudgetTo: Yup.number()
    .integer()
    .required("*Required.")
    .moreThan(Yup.ref("BudgetFrom"), "Must be greater than Budget to"),
});

export const FormikPersonalDetailsCompanyValidationSchema = Yup.object().shape({
  companyName: Yup.string().required("*Required.").min(3),
  companyContantPerson: Yup.string().required("*Required."),
  companyEmailAddress: Yup.string().email().required("*Required."),
  companyCellNo: Yup.mixed().required("*Required."),
  companyBranch: Yup.mixed().required("*Required."),
  companyNoOfEmp: Yup.mixed().required("*Required."),
  companyAddress: Yup.string().required("*Required."),
  companyAddressZipCode: Yup.mixed().required("*Required."),
  companyAddressCity: Yup.mixed().required("*Required."),
  companyAddressCountry: Yup.mixed().required("*Required."),
  lat: Yup.mixed().required("*Required."),
  Description: Yup.mixed().required("*Required."),
  companyProfileImage: Yup.mixed().required("*Required."),
});
export const FormikPersonalDetailsCompanyBillingValidationSchema =
  Yup.object().shape({
    companyName: Yup.string().required("*Required.").min(3),
    companyContantPerson: Yup.string().required("*Required."),
    companyEmailAddress: Yup.string().email().required("*Required."),
    companyCellNo: Yup.mixed().required("*Required."),
    companyBranch: Yup.mixed().required("*Required."),
    companyNoOfEmp: Yup.mixed().required("*Required."),
    companyAddress: Yup.mixed().required("*Required."),
    companyAddressZipCode: Yup.mixed().required("*Required."),
    companyAddressCity: Yup.mixed().required("*Required."),
    companyAddressCountry: Yup.mixed().required("*Required."),
    lat: Yup.mixed().required("*Required."),
    companyBillingAddress: Yup.mixed().required("*Required."),
    companyBillingAddressZipCode: Yup.mixed().required("*Required."),
    companyBillingAddressCity: Yup.mixed().required("*Required."),
    companyBillingAddressCountry: Yup.mixed().required("*Required."),
    companyProfileImage: Yup.mixed().required("*Required."),
    Description: Yup.mixed().required("*Required."),
  });

export const FormikPortfolioProjectDetailValidationSchema = Yup.object().shape({
  portfolioProjectName: Yup.string().required("*Required.").min(3),
  portfolioProjectDisciption: Yup.mixed().required("*Required."),
  portfolioProjectImage: Yup.mixed().required("Image is required"),
});

export const FormikChangePasswordValidationSchema = Yup.object().shape({
  oldPassword: Yup.string().required("*Required."),
  newPassword: Yup.string().required("*Required."),
  confirmPassword: Yup.string()
    .required("*Required.")
    .when("newPassword", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref("newPassword")],
        "Both password need to be the same"
      ),
    }),
});

export const FormikDetailFormValidtionSchema = Yup.object().shape({
  firstName: Yup.string().required("*Required."),
  lastName: Yup.string().required("*Required."),
  emailAddress: Yup.mixed().required("*Required."),
  phoneNo: Yup.mixed().required("*Required."),
  gender: Yup.mixed().required("*Required."),
  age: Yup.mixed().required("*Required."),
  primaryAddress: Yup.mixed().required("*Required."),
  primaryZipCode: Yup.mixed().required("*Required."),
  primaryCity: Yup.mixed().required("*Required."),
  primaryCountry: Yup.mixed().required("*Required."),
  language: Yup.mixed().required("*Required."),
  personalDetailsJobSeekerImage: Yup.mixed().required("*Required."),
  lat: Yup.mixed().required("*Required."),
  lng: Yup.string().required("*Required."),
});

export const FormikProfessionalSkillsValidtionSchema = Yup.object().shape({
  skills: Yup.mixed().required("*Required."),
});

export const FormikProfessionalCertificationValidtionSchema =
  Yup.object().shape({
    certificateName: Yup.string().required("*Required."),
    institueName: Yup.string().required("*Required."),
    credentialId: Yup.string().notRequired(),
    credentialUrl: Yup.string().notRequired(),
    dateFrom: Yup.mixed().required("*Required."),
    dateTo: Yup.mixed().notRequired(),
  });

export const FormikProfessionalEducationValidtionSchema = Yup.object().shape({
  educationType: Yup.mixed().required("*Required."),
  institueName: Yup.string().required("*Required."),
  fieldOfStudy: Yup.string().required("*Required."),
  grades: Yup.string().required("*Required."),
  startMonth: Yup.mixed().required("*Required."),
  startYear: Yup.mixed().required("*Required."),
  endMonth: Yup.mixed().required("*Required."),
  endYear: Yup.mixed().required("*Required."),
});

export const FormikProfessionalExperienceValidtionSchema = Yup.object().shape({
  jobTitle: Yup.string().required("*Required."),
  jobDescription: Yup.string().required("*Required."),
  jobEmployementType: Yup.mixed().required("*Required."),
  jobIndustryType: Yup.mixed().required("*Required."),
  jobCompany: Yup.string().required("*Required."),
  jobLocation: Yup.string().required("*Required."),
  jobFrom: Yup.mixed().required("*Required."),
  jobTo: Yup.mixed().required("*Required."),
});

export const FormikAppliedJobValidationSchema = Yup.object().shape({
  Email: Yup.string().email("*Invalid email").required("*Required."),
  CountryId: Yup.mixed().required("*Required."),
  Phone: Yup.mixed().required("*Required."),
  //AdditionalNotes: Yup.mixed().required("*Required."),
  //UploadCv: Yup.string().required("*Required."),
});

export const FormikReqInvitationVideoCallValidationSchema = Yup.object().shape({
  reqVideoCallTime: Yup.string().required("*Required."),
  reqVideoCallDate: Yup.string().required("*Required."),
  reqVideoEndCallTime: Yup.string().required("*Required."),
});
