import firebase from "firebase/app";
import "firebase/auth"; // for authentication
import "firebase/storage"; // for storage
import "firebase/database"; // for realtime database
import "firebase/firestore"; // for cloud firestore
import "firebase/messaging"; // for cloud messaging
import "firebase/functions"; // for cloud functions

const Firebase = firebase;

// export const messaging = firebase.messaging();
export const auth = firebase.auth;
export const db = firebase.database();
export const globalFireStore = firebase.firestore();

export default Firebase;
