import React, { useState, useEffect } from "react";
import { createPayment, getPlans } from "../API/Plans";
import { useSelector } from "react-redux";
import { balanceSvg } from "Assets";
import { Head, DashboardHeading } from "Components";

export default function Billing({ }) {
  const { company } = useSelector((state) => state.company);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedData, setSelectedData] = useState();
  const [apiData, setApiData] = useState();
  const [noCredit, setNoCredit] = useState(false);
  const [noSubscription, setNoSubscription] = useState(false);

  useEffect(() => {
    getPlans()
      .then(({ data }) => {
        setData(data.result);
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);

  const handlePlanData = (data) => {
    setIsLoading(true);
    setSelectedData(data);
    callCreatePaymentAPI(data);
  };
  const callCreatePaymentAPI = (data) => {
    let data1 = {
      currency: "EUR",
      value: data.Price,
      paymentMode: "test",
      description: `${data.Title}-${data.Description}`,
      redirectUrl: process.env.REACT_APP_NAME == "production" ? "https://aidapro.com/home-company/plan" : "http://localhost:3000/home-company/plan",
      Status: 1,
      companyProfileId: company.Id,
      PlanDetailId: data.Id,
    };

    createPayment(data1)
      .then(({ data }) => {
        setApiData(data);
        setIsLoading(false);
        if (data.status === 200) {
          localStorage.setItem("TransactionId", data.result.Id);
          let url =
            data &&
            data.result &&
            data.result._links &&
            data.result._links.Checkout &&
            data.result._links.Checkout.Href &&
            data.result._links.Checkout.Href;
          window.open(url, "_blank");
          //window.location.href = url;
        }
        if (data.status === 521) {
          setNoSubscription(true);
        }
        if (data.status === 522) {
          setNoCredit(true);
        }
      })
      .catch((e) => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <Head title="AIDApro | Balance" description="Balance" />
      <section className="plans__container">
        <DashboardHeading heading="Balance" svg={balanceSvg} />
        <div className="plans__container__row">
          <div className="plans__container__entry">
            <div className="plans__container__entry__header">
              <div className="plans__container__entry__header__name animate__animated animate__fadeInLeft">
                Plan Details
              </div>
            </div>
            <div className="plans__container__entry__content">
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInLeft">
                <div className="plans__container__entry__content__entry__heading">
                  Plan 1
                </div>
                <div className="plans__container__entry__content__entry__content">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__details">
                      EUR 39/Credit
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      EUR 40
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="plans__container__entry">
            <div className="plans__container__entry__header">
              <div className="plans__container__entry__header__name animate__animated animate__fadeInLeft">
                Credit Usage
              </div>
            </div>
            <div className="plans__container__entry__content">
              <div className="plans__container__entry__content__entry animate__animated animate__fadeInDown">
                <div className="plans__container__entry__content__entry__header">
                  <div className="plans__container__entry__content__entry__heading">
                    Plans
                  </div>
                  <div className="plans__container__entry__content__entry__heading">
                    Total Credits
                  </div>
                  <div className="plans__container__entry__content__entry__heading">
                    Remaining Credits
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
                <div className="plans__container__entry__content__entry__content animate__animated animate__fadeInRight">
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__contact">
                      Credit Contacts
                    </div>
                    <div className="plans__container__entry__content__entry__content__col__date">
                      Sept 19 - Oct 19
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__credit">
                      10 Credits
                    </div>
                  </div>
                  <div className="plans__container__entry__content__entry__content__col">
                    <div className="plans__container__entry__content__entry__content__col__price">
                      8 Credits
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
