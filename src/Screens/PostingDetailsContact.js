import React from "react";
import { Head } from "Components";
import Img from "react-cool-img";
import { AdressSvg, EmailSvg, PhoneSvg, SocialsSvg } from "Assets";

export default function PostingDetailsContact({ data }) {
  return (
    <>
      <Head
        title="AIDApro | Posting Details - Contact"
        description="Posting Details - Contact"
      />
      <div className="homepage__container__jobs__projects__penel__container__details__content__contact">
        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
          <Img loading="lazy" src={EmailSvg} alt="EmailSvg" />
          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
              Email
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
              {data != null
                ? data.Addresses != null && data.Addresses.length > 0
                  ? data.Addresses[0].Email
                  : "Not specified"
                : "Not specified"}
            </div>
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
          <Img loading="lazy" src={PhoneSvg} alt="PhoneSvg" />
          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
              Phone
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
              {data != null
                ? data.Addresses != null && data.Addresses.length > 0
                  ? "+" + data.Addresses[0].PhoneNo
                  : "Not specified"
                : "Not specified"}
            </div>
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
          <Img loading="lazy" src={AdressSvg} alt="AdressSvg" />
          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
              Address
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
              {data != null
                ? data.Addresses != null && data.Addresses.length > 0
                  ? data.Addresses[0].AddressDetail
                  : "Not specified"
                : "Not specified"}
            </div>
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
          <Img loading="lazy" src={SocialsSvg} alt="SocialsSvg" />
          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
              Socials
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__links">
              {data != null &&
              data.FacebookProfile != null &&
              data.FacebookProfile != "" ? (
                <a
                  href={
                    data != null
                      ? data.FacebookProfile != null
                        ? data.FacebookProfile
                        : ""
                      : ""
                  }
                  className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                  target="_Blank"
                >
                  <svg
                    id="facebook_2_"
                    data-name="facebook (2)"
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.426"
                    height="25.426"
                    viewBox="0 0 25.426 25.426"
                  >
                    <path
                      id="Path_659"
                      data-name="Path 659"
                      d="M22.248,0H3.178A3.181,3.181,0,0,0,0,3.178V22.247a3.181,3.181,0,0,0,3.178,3.178H22.248a3.181,3.181,0,0,0,3.178-3.178V3.178A3.181,3.181,0,0,0,22.248,0Z"
                      fill="#1976d2"
                    />
                    <path
                      id="Path_660"
                      data-name="Path 660"
                      d="M203.918,103.946h-3.973v-3.178c0-.877.712-.795,1.589-.795h1.589V96h-3.178a4.767,4.767,0,0,0-4.767,4.767v3.178H192v3.973h3.178v8.74h4.767v-8.74h2.384Z"
                      transform="translate(-182.465 -91.233)"
                      fill="#fafafa"
                    />
                  </svg>
                </a>
              ) : null}

              {data != null &&
              data.GoogleProfile != null &&
              data.GoogleProfile != "" ? (
                <a
                  href={
                    data != null
                      ? data.GoogleProfile != null
                        ? data.GoogleProfile
                        : ""
                      : ""
                  }
                  className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                  target="_Blank"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.99"
                    height="25.99"
                    viewBox="0 0 13.189 13.19"
                  >
                    <g
                      id="Group_1746"
                      data-name="Group 1746"
                      transform="translate(408.739 -103.684)"
                    >
                      <path
                        id="Path_21771"
                        data-name="Path 21771"
                        d="M-408.739,115.986V104.559a1.007,1.007,0,0,1,.16-.489.869.869,0,0,1,.641-.373c.025,0,.052.007.073-.013h11.425a.945.945,0,0,1,.574.222.97.97,0,0,1,.314.758c0,.352,0,.7,0,1.055q0,5.07,0,10.14a.985.985,0,0,1-.43.869.971.971,0,0,1-.588.146q-4.741,0-9.482,0c-.57,0-1.141,0-1.711,0a.972.972,0,0,1-.758-.314A.945.945,0,0,1-408.739,115.986Z"
                        fill="#1795d6"
                      />
                      <path
                        id="Path_21772"
                        data-name="Path 21772"
                        d="M-330.722,194.67a1.778,1.778,0,0,1-.731.924,2.073,2.073,0,0,0,.5-.092c.15-.043.3-.1.467-.151a3.282,3.282,0,0,1-.763.817.2.2,0,0,0-.088.181,5.041,5.041,0,0,1-1.589,3.787,4.574,4.574,0,0,1-2.572,1.243,4.983,4.983,0,0,1-3.307-.607c-.038-.022-.087-.035-.115-.1a3.564,3.564,0,0,0,2.5-.726,1.818,1.818,0,0,1-1.624-1.215,1.94,1.94,0,0,0,.771-.042,1.8,1.8,0,0,1-.945-.562,1.692,1.692,0,0,1-.427-1.011c-.01-.149-.007-.149.126-.092a1.77,1.77,0,0,0,.639.146,2.494,2.494,0,0,1-.359-.34,1.735,1.735,0,0,1-.227-1.883c.045-.1.07-.108.148-.019a4.9,4.9,0,0,0,2.883,1.657c.168.031.34.038.509.067.1.017.129-.009.114-.111a1.726,1.726,0,0,1,.315-1.314,1.744,1.744,0,0,1,2.591-.25.223.223,0,0,0,.238.059,3.756,3.756,0,0,0,.826-.315A.224.224,0,0,1-330.722,194.67Z"
                        transform="translate(-67.417 -87.703)"
                        fill="#fff"
                      />
                    </g>
                  </svg>
                </a>
              ) : null}
              {data != null &&
              data.LinkedInProfile != null &&
              data.LinkedInProfile != "" ? (
                <a
                  href={
                    data != null
                      ? data.LinkedInProfile != null
                        ? data.LinkedInProfile
                        : ""
                      : ""
                  }
                  className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                  target="_Blank"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="25.426"
                    height="25.426"
                    viewBox="0 0 25.426 25.426"
                  >
                    <path
                      id="linkedin"
                      d="M23.126,0H2.3A2.3,2.3,0,0,0,0,2.3V23.126a2.3,2.3,0,0,0,2.3,2.3H23.126a2.3,2.3,0,0,0,2.3-2.3V2.3A2.3,2.3,0,0,0,23.126,0ZM7.868,21.954a.669.669,0,0,1-.669.669H4.349a.669.669,0,0,1-.669-.669V10.011a.669.669,0,0,1,.669-.669H7.2a.669.669,0,0,1,.669.669ZM5.774,8.216A2.707,2.707,0,1,1,8.481,5.509,2.707,2.707,0,0,1,5.774,8.216ZM22.758,22.008a.615.615,0,0,1-.615.615H19.085a.615.615,0,0,1-.615-.615v-5.6c0-.836.245-3.662-2.184-3.662-1.884,0-2.266,1.935-2.343,2.8v6.462a.615.615,0,0,1-.615.615H10.37a.615.615,0,0,1-.615-.615V9.957a.615.615,0,0,1,.615-.615h2.957a.615.615,0,0,1,.615.615V11A4.2,4.2,0,0,1,17.89,9.141c4.9,0,4.868,4.574,4.868,7.087v5.78Z"
                      fill="#0077b7"
                    />
                  </svg>
                </a>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
