import React, { useState, useEffect } from "react";
import Header from "Components/Header";
import { Head, ContactTab } from "Components";
import Img from "react-cool-img";
import { CustomError } from "./Toasts";
import { contactUs, getCountry } from "API/Api";
import { isNullOrEmpty } from "Utils/TextUtils";
import { isInvalidEmail, isInvalidPhoneNumber } from "Utils/Validations";
import { contactSvg } from "Assets";
import ContactForm from "Components/ContactForm";

export default function Contact({ setIsLoginOpen }) {
  const isOn = window.localStorage.getItem("isOn");

  const [isOnOther, setIsOnOther] = useState(false);
  var [firstName, setFirstName] = useState("");
  var [lastName, setLastName] = useState("");
  var [email, setEmail] = useState("");
  var [phone, setPhone] = useState("");
  var [subject, setSubject] = useState("");
  var [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  // validation

  var [firstNameError, setFirstNameError] = useState(false);
  const [firstNameErrorMessage, setFirstNameErrorMessage] = useState("");
  var [lastNameError, setLastNameError] = useState(false);
  const [lastNameErrorMessage, setLastNameErrorMessage] = useState("");

  var [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  var [subjectError, setSubjectError] = useState(false);
  const [subjectErrorMessage, setSubjectErrorMessage] = useState("");
  var [messageError, setMessageError] = useState(false);
  const [messageErrorMessage, setMessageErrorMessage] = useState("");
  var [phoneError, setPhoneError] = useState(false);
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");

  const [countryCodes, setCountryCode] = useState([]);
  const [selectedCountryCode, setSelectedCountryCode] = useState(null);
  const [phoneMinLength, setPhoneMinLength] = useState(null);
  const [phoneMaxLength, setPhoneMaxLength] = useState(null);

  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });

    getCountry()
      .then(({ data }) => {
        let options = data.result.map((e) => ({
          value: e.Id,
          label: e.digit_name + " - " + e.phone_country_code,
          capital_city: e.capital_city,
          long_name: e.long_name,
          digit_name: e.digit_name,
          phone_country_code: e.phone_country_code,
          phone_number_length_min: e.phone_number_length_min,
          phone_number_length_max: e.phone_number_length_max,
          postal_validation_regex: e.postal_validation_regex,
          postal_code_char_set: e.postal_code_char_set,
          postal_code_length_min: e.postal_code_length_min,
          postal_code_length_max: e.postal_code_length_max,
        }));
        setCountryCode(options);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
  }, []);

  // const handleChangeValues = (event) => {

  // };

  const onNameTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setFirstNameErrorMessageAndVisibility(
        isOnOther ? "Please enter first name" : "Please enter company name"
      );
    else setFirstNameErrorMessageAndVisibility("");
    setFirstName(event.currentTarget.value);
  };
  const onLastNameTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setLastNameErrorMessageAndVisibility(
        isOnOther ? "Please enter last name" : "Please enter contact person"
      );
    else setLastNameErrorMessageAndVisibility("");
    setLastName(event.currentTarget.value);
  };
  const onEmailTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else setEmailErrorMessageAndVisibility("");
    setEmail(event.currentTarget.value);
  };
  const onPhoneTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setPhoneErrorMessageAndVisibility("Please enter phone number");
    else if (isInvalidPhoneNumber(event.currentTarget.value))
      setPhoneErrorMessageAndVisibility("Please enter valid phone number");
    else setPhoneErrorMessageAndVisibility("");
    setPhone(event.currentTarget.value);
  };
  const onSubjectTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setSubjectErrorMessageAndVisibility("Please enter Subject");
    else setSubjectErrorMessageAndVisibility("");
    setSubject(event.currentTarget.value);
  };

  const onDescriptionTextChangeListener = (data) => {
    if (isNullOrEmpty(data))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else setDescriptionErrorMessageAndVisibility("");
    setMessage(data);
  };

  const sendcontactForm = (e) => {
    e.preventDefault();
    if (isNullOrEmpty(firstName))
      setFirstNameErrorMessageAndVisibility(
        isOnOther ? "Please enter first name" : "Please enter company name"
      );
    else if (isNullOrEmpty(lastName))
      setLastNameErrorMessageAndVisibility(
        isOnOther ? "Please enter last name" : "Please enter contact person"
      );
    else if (isNullOrEmpty(email))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(email))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else if (isNullOrEmpty(phone))
      setPhoneErrorMessageAndVisibility("Please enter phone number");
    else if (isInvalidPhoneNumber(phone))
      setPhoneErrorMessageAndVisibility("Please enter valid phone number");
    else if (subject === "")
      setSubjectErrorMessageAndVisibility("Please enter Subject");
    else if (isNullOrEmpty(message))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else {
      let data = {
        Id: 0,
        CompanyName: firstName,
        ContactPerson: lastName,
        Email: email,
        Phoneno: phone,
        Subject: subject,
        Message: message,
        // Date: new Date(),
        Type: isOnOther ? "other" : "company",
      };

      contactUs(data)
        .then(({ data }) => {
          if (data.success) {
            //CustomSuccess("Submitted");
            resetViewFields();
          } else {
            CustomError(data.errors);
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Can't able to post.");
        });
    }
  };

  const onTabValueSelect = (isFromCompany) => {
    setIsOnOther(!isFromCompany);
    resetViewFields();
  };
  const resetViewFields = () => {
    setFirstName((firstName = ""));
    setLastName((lastName = ""));
    setEmail((email = ""));
    setPhone((phone = ""));
    setSubject((subject = ""));
    setMessage((message = ""));

    setFirstNameErrorMessageAndVisibility("");
    setLastNameErrorMessageAndVisibility("");
    setEmailErrorMessageAndVisibility("");
    setSubjectErrorMessageAndVisibility("");
    setDescriptionErrorMessageAndVisibility("");
    setPhoneErrorMessageAndVisibility("");
  };

  const setFirstNameErrorMessageAndVisibility = (text) => {
    setFirstNameError((firstNameError = !isNullOrEmpty(text)));
    setFirstNameErrorMessage(text);
  };
  const setLastNameErrorMessageAndVisibility = (text) => {
    setLastNameError((lastNameError = !isNullOrEmpty(text)));
    setLastNameErrorMessage(text);
  };
  const setEmailErrorMessageAndVisibility = (text) => {
    setEmailError((emailError = !isNullOrEmpty(text)));
    setEmailErrorMessage(text);
  };
  const setSubjectErrorMessageAndVisibility = (text) => {
    setSubjectError((subjectError = !isNullOrEmpty(text)));
    setSubjectErrorMessage(text);
  };
  const setDescriptionErrorMessageAndVisibility = (text) => {
    setMessageError((messageError = !isNullOrEmpty(text)));
    setMessageErrorMessage(text);
  };
  const setPhoneErrorMessageAndVisibility = (text) => {
    setPhoneError((phoneError = !isNullOrEmpty(text)));
    setPhoneErrorMessage(text);
  };

  const onPhoneNumberChangeListener = (event) => {
    setPhoneErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };

  const setPhoneErrorMessageAndVisibilityAndSetText = (enteredText) => {
    setPhone(enteredText);
    if (isNullOrEmpty(enteredText)) {
      setPhoneNumberErrorMessageAndVisibility("Please enter phone number");
      return true;
    } else if (selectedCountryCode == null) {
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter country code first"
      );
      return true;
    } else if (isInvalidPhoneNumber(enteredText)) {
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter valid phone number"
      );
      return true;
    } else if (enteredText.length < phoneMaxLength) {
      setPhoneNumberErrorMessageAndVisibility(
        `Please enter ${phoneMaxLength} digit phone number`
      );
      return true;
    } else if (enteredText.length > phoneMaxLength) {
      setPhoneNumberErrorMessageAndVisibility(
        `Phone number can't be greater than ${phoneMaxLength} digits`
      );
      return true;
    } else {
      setPhoneNumberErrorMessageAndVisibility("");
      return false;
    }
  };

  const setPhoneNumberErrorMessageAndVisibility = (text) => {
    setPhoneError((phoneError = !isNullOrEmpty(text)));
    setPhoneErrorMessage(text);
  };

  const onChangeCountryCode = (event) => {
    setSelectedCountryCode(event);
    setPhoneMinLength(parseInt(event.phone_number_length_min));
    setPhoneMaxLength(parseInt(event.phone_number_length_max));
  };
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <>
      <Head title="AIDApro | Contact Us" description="Contact Us" />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <div
        className="homepage__container__jumbotron"
        style={{ paddingTop: "8em" }}
      >
        <form
          onSubmit={sendcontactForm}
          className="homepage__container__jumbotron__wrapper homepage__container__jumbotron__wrapper__reverse"
        >
          <div className="homepage__container__jumbotron__left">
            <div
              className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
              style={{ fontSize: 30 }}
            >
              Get in <span>Touch</span>
            </div>
            <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                margin: "1em auto",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              <ContactTab
                defaultChecked={true}
                label="Company"
                onClick={() => {
                  onTabValueSelect(true);
                }}
              />
              <ContactTab
                label="Others"
                onClick={() => {
                  onTabValueSelect(false);
                }}
              />
            </div>
            <ContactForm
              isOnOther={isOnOther}
              firstNameError={firstNameError}
              firstNameErrorMessage={firstNameErrorMessage}
              firstName={firstName}
              onNameTextChangeListener={onNameTextChangeListener}
              lastName={lastName}
              lastNameError={lastNameError}
              lastNameErrorMessage={lastNameErrorMessage}
              onLastNameTextChangeListener={onLastNameTextChangeListener}
              email={email}
              emailError={emailError}
              emailErrorMessage={emailErrorMessage}
              onEmailTextChangeListener={onEmailTextChangeListener}
              countryCodes={countryCodes}
              selectedCountryCode={selectedCountryCode}
              onChangeCountryCode={onChangeCountryCode}
              phoneError={phoneError}
              phoneErrorMessage={phoneErrorMessage}
              phoneMinLength={phoneMinLength}
              phoneMaxLength={phoneMaxLength}
              phone={phone}
              onPhoneNumberChangeListener={onPhoneNumberChangeListener}
              subject={subject}
              subjectError={subjectError}
              subjectErrorMessage={subjectErrorMessage}
              onSubjectTextChangeListener={onSubjectTextChangeListener}
              message={message}
              messageError={messageError}
              messageErrorMessage={messageErrorMessage}
              onDescriptionTextChangeListener={onDescriptionTextChangeListener}
            />

            <div className="homepage__container__jumbotron__left__sign__up__footer">
              <div className="homepage__container__jumbotron__left__sign__up__footer__left">
                Phone
                <div className="homepage__container__jumbotron__left__sign__up__footer__left__phone1">
                  +31 26 376 4525
                </div>
              </div>
              <div className="homepage__container__jumbotron__left__sign__up__footer__left">
                Email
                <div className="homepage__container__jumbotron__left__sign__up__footer__left__phone1">
                  info@aidapro.com
                </div>
              </div>
              <div className="homepage__container__jumbotron__left__sign__up__footer__right">
                Socials
                <div className="homepage__container__jumbotron__left__sign__up__footer__right__wrapper">
                  <a
                    href="https://wa.me/+923219652222"
                    className="homepage__container__jumbotron__left__sign__up__footer__social__links"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="20"
                      height="20"
                      viewBox="0 0 24 24"
                      fill="#ffffff"
                    >
                      <path d="M 12.011719 2 C 6.5057187 2 2.0234844 6.478375 2.0214844 11.984375 C 2.0204844 13.744375 2.4814687 15.462563 3.3554688 16.976562 L 2 22 L 7.2324219 20.763672 C 8.6914219 21.559672 10.333859 21.977516 12.005859 21.978516 L 12.009766 21.978516 C 17.514766 21.978516 21.995047 17.499141 21.998047 11.994141 C 22.000047 9.3251406 20.962172 6.8157344 19.076172 4.9277344 C 17.190172 3.0407344 14.683719 2.001 12.011719 2 z M 12.009766 4 C 14.145766 4.001 16.153109 4.8337969 17.662109 6.3417969 C 19.171109 7.8517969 20.000047 9.8581875 19.998047 11.992188 C 19.996047 16.396187 16.413812 19.978516 12.007812 19.978516 C 10.674812 19.977516 9.3544062 19.642812 8.1914062 19.007812 L 7.5175781 18.640625 L 6.7734375 18.816406 L 4.8046875 19.28125 L 5.2851562 17.496094 L 5.5019531 16.695312 L 5.0878906 15.976562 C 4.3898906 14.768562 4.0204844 13.387375 4.0214844 11.984375 C 4.0234844 7.582375 7.6067656 4 12.009766 4 z M 8.4765625 7.375 C 8.3095625 7.375 8.0395469 7.4375 7.8105469 7.6875 C 7.5815469 7.9365 6.9355469 8.5395781 6.9355469 9.7675781 C 6.9355469 10.995578 7.8300781 12.182609 7.9550781 12.349609 C 8.0790781 12.515609 9.68175 15.115234 12.21875 16.115234 C 14.32675 16.946234 14.754891 16.782234 15.212891 16.740234 C 15.670891 16.699234 16.690438 16.137687 16.898438 15.554688 C 17.106437 14.971687 17.106922 14.470187 17.044922 14.367188 C 16.982922 14.263188 16.816406 14.201172 16.566406 14.076172 C 16.317406 13.951172 15.090328 13.348625 14.861328 13.265625 C 14.632328 13.182625 14.464828 13.140625 14.298828 13.390625 C 14.132828 13.640625 13.655766 14.201187 13.509766 14.367188 C 13.363766 14.534188 13.21875 14.556641 12.96875 14.431641 C 12.71875 14.305641 11.914938 14.041406 10.960938 13.191406 C 10.218937 12.530406 9.7182656 11.714844 9.5722656 11.464844 C 9.4272656 11.215844 9.5585938 11.079078 9.6835938 10.955078 C 9.7955938 10.843078 9.9316406 10.663578 10.056641 10.517578 C 10.180641 10.371578 10.223641 10.267562 10.306641 10.101562 C 10.389641 9.9355625 10.347156 9.7890625 10.285156 9.6640625 C 10.223156 9.5390625 9.737625 8.3065 9.515625 7.8125 C 9.328625 7.3975 9.131125 7.3878594 8.953125 7.3808594 C 8.808125 7.3748594 8.6425625 7.375 8.4765625 7.375 z"></path>
                    </svg>
                  </a>
                  <a
                    href="https://www.facebook.com/aidaprofessional/"
                    className="homepage__container__jumbotron__left__sign__up__footer__social__links"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="9.294"
                      height="18.587"
                      viewBox="0 0 9.294 18.587"
                    >
                      <path
                        id="facebook"
                        d="M13.785,3.086h1.7V.131A21.912,21.912,0,0,0,13.009,0C10.563,0,8.887,1.539,8.887,4.367v2.6h-2.7v3.3h2.7v8.313H12.2V10.275h2.591l.411-3.3h-3V4.695c0-.955.258-1.609,1.588-1.609Z"
                        transform="translate(-6.187)"
                        fill="#fff"
                      />
                    </svg>
                  </a>
                  <a
                    href="https://www.instagram.com/aidaprofessionals/"
                    className="homepage__container__jumbotron__left__sign__up__footer__social__links"
                  >
                    <svg
                      id="instagram_1_"
                      data-name="instagram (1)"
                      xmlns="http://www.w3.org/2000/svg"
                      width="18.587"
                      height="18.587"
                      viewBox="0 0 18.587 18.587"
                    >
                      <path
                        id="Path_148"
                        data-name="Path 148"
                        d="M214.267,212.634A1.634,1.634,0,1,1,212.634,211,1.634,1.634,0,0,1,214.267,212.634Zm0,0"
                        transform="translate(-203.34 -203.34)"
                        fill="#fff"
                      />
                      <path
                        id="Path_149"
                        data-name="Path 149"
                        d="M127.7,120h-5.518A2.181,2.181,0,0,0,120,122.178V127.7a2.181,2.181,0,0,0,2.178,2.178H127.7a2.181,2.181,0,0,0,2.178-2.178v-5.518A2.181,2.181,0,0,0,127.7,120Zm-2.759,7.66a2.723,2.723,0,1,1,2.723-2.723A2.726,2.726,0,0,1,124.937,127.66Zm3.122-5.3a.545.545,0,1,1,.545-.545A.545.545,0,0,1,128.059,122.36Zm0,0"
                        transform="translate(-115.644 -115.644)"
                        fill="#fff"
                      />
                      <path
                        id="Path_150"
                        data-name="Path 150"
                        d="M13.686,0H4.9A4.907,4.907,0,0,0,0,4.9v8.785a4.907,4.907,0,0,0,4.9,4.9h8.785a4.907,4.907,0,0,0,4.9-4.9V4.9A4.907,4.907,0,0,0,13.686,0ZM15.32,12.053a3.271,3.271,0,0,1-3.267,3.267H6.535a3.271,3.271,0,0,1-3.267-3.267V6.535A3.271,3.271,0,0,1,6.535,3.267h5.518A3.271,3.271,0,0,1,15.32,6.535Zm0,0"
                        fill="#fff"
                      />
                    </svg>
                  </a>
                  <a
                    href="https://twitter.com/aidaprofession"
                    className="homepage__container__jumbotron__left__sign__up__footer__social__links"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20.017"
                      height="16.264"
                      viewBox="0 0 20.017 16.264"
                    >
                      <path
                        id="twitter"
                        d="M17.96,6.306a8.192,8.192,0,0,0,2.057-2.13h0a8.57,8.57,0,0,1-2.365.648,4.082,4.082,0,0,0,1.806-2.268,8.211,8.211,0,0,1-2.6.993,4.1,4.1,0,0,0-7.1,2.806,4.225,4.225,0,0,0,.1.936A11.618,11.618,0,0,1,1.394,3,4.1,4.1,0,0,0,2.655,8.482,4.056,4.056,0,0,1,.8,7.977v.045a4.123,4.123,0,0,0,3.288,4.033,4.081,4.081,0,0,1-1.076.135,3.622,3.622,0,0,1-.777-.07A4.144,4.144,0,0,0,6.07,14.978a8.249,8.249,0,0,1-5.089,1.75A7.761,7.761,0,0,1,0,16.672a11.557,11.557,0,0,0,6.3,1.842A11.6,11.6,0,0,0,17.96,6.306Z"
                        transform="translate(0 -2.25)"
                        fill="#fff"
                      />
                    </svg>
                  </a>
                  <a
                    href="https://www.linkedin.com/company/aidapro"
                    className="homepage__container__jumbotron__left__sign__up__footer__social__links"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="#ffffff"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-linkedin"
                    >
                      <path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path>
                      <rect x="2" y="9" width="4" height="12"></rect>
                      <circle cx="4" cy="4" r="2"></circle>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
            <div className="homepage__container__jumbotron__left__sign__up__footer__bottom">
              <div className="homepage__container__jumbotron__left__sign__up__footer__left">
                Head Office
                <div
                  style={{ marginBottom: 20 }}
                  className="homepage__container__jumbotron__left__sign__up__footer__left__phone1"
                >
                  Meander 251, 6825 MC Arnhem, the Netherlands
                </div>
              </div>
            </div>
          </div>
          <Img
            loading="lazy"
            src={contactSvg}
            alt="contactSvg"
            style={{ height: 550, marginTop: "0em" }}
            className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          />
        </form>
      </div>
    </>
  );
}
