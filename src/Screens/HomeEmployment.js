import React, { useEffect, useState, useContext } from "react";
import Header from "Components/Header";
import { useDispatch } from "react-redux";
import { HomeEmployeeRouter } from "../Routes/HomeEmployeeRouter";
import EmploymentResultCard from "Components/HomePageEmploymentResultCard";
import { jobSekkerJobs } from "../API/EmploymentAPI";
import { getAllKeywords } from "../API/Api";
import UserContext from "Context/UserContext";
import { Head } from "Components";
import Pagination from "react-js-pagination";
import { getLookUpByPrefix, getAllIndustry } from "API/Api";
import { getJobSekkerById } from "../Redux/Actions/AppActions";
import Img from "react-cool-img";
import { navigate } from "@reach/router";
import { homePageBackground } from "Assets";
import { checkOnlineOffline } from "Utils/common";
import HomeEmploymentSidebar from "Routes/HomeEmploymentSidebar";
import HomeEmploymentFilters from "Components/HomeEmploymentFilters";

export default function HomeEmployment({
  setIsAddDegreeOpen,
  setEditDegree,
  setIsAddCertificationsOpen,
  setIsAddSkillsOpen,
  setIsAddWorkExperianceOpen,
  setAddWorkExperianceEditData,
  setIsEditDegreeOpen,
  setIsEditCertificationsOpen,
  setEditCertificationData,
  setIsEditSkillsOpen,
  setIsEditWorkExperianceOpen,
  setIsAddSocialsOpen,
  setIsEditProjectOpen,
  setIsEditSocialsOpen,
  setIsUploadProjectOpen,
  getSelectedProjectData,
  setSelectedSkills,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
  setIsSetHourlyRateOpen,
  setIsSelectWorkTimeOpen,
  setSelectedHourlyRate,
  setIsMessageOpen,
  setUserData,
  setVideoCallUpdate,
}) {
  localStorage.setItem("isOn", "professional");
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [jobs, setJobs] = useState([]);
  const [title, setTitle] = useState("");
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(9);
  const [keywords, setKeywords] = useState([]);
  let [location, setJobLocation] = useState("");
  const [totalCards, setTotalCards] = useState(0);
  let [jobType, setJobType] = useState([]);
  let [industryType, setIndustryType] = useState([]);
  let [dateOfPosting, setDateOfPosting] = useState();
  let [salaryFrom, setSalaryFrom] = useState("");
  let [salaryTo, setSalaryTo] = useState("");
  let [contractType, setContractType] = useState([]);
  let [fieldWork, setFieldWork] = useState([]);
  const [salaryEstimate, setSalaryEstimate] = useState();
  let [latitude, setLatitude] = useState(null);
  let [longitude, setLongitude] = useState(null);
  let [selectedRangeValue, setSelectedRangeValue] = useState(0);
  let dispatch = useDispatch();
  const user = useContext(UserContext);

  useEffect(() => {
    checkOnlineOffline(user.FirebaseId);
  }, []);

  const jobSellerJobsApi = (
    jobTitle,
    jobLocation,
    selectedRadius,
    lat,
    lng
  ) => {
    let jobTitleCustom = "";
    let jobLocationCustom = "";
    let jobLocationLat = null;
    let jobLocationLng = null;
    let radius = null;

    if (jobTitle) {
      jobTitleCustom = jobTitle;
    } else {
      jobTitleCustom = title;
    }

    if (location) {
      jobLocationCustom = location;
    } else {
      jobLocationCustom = jobLocation;
    }
    if (lat) {
      jobLocationLat = lat;
    }
    if (lng) {
      jobLocationLng = lng;
    }
    if (selectedRadius != 0) {
      radius = selectedRadius;
    }

    setIsLoading(true);
    // let jobType = contractType.map((e) => {
    //   e.value.join(", ");
    // });

    let jobType = contractType.map((element) => element.value).join(",");
    // let jobType = contractType.map((element) => element.value).join(", ")
    let fieldWorkTemp = fieldWork.map((e) => {
      return e.value;
    });
    jobSekkerJobs({
      page,
      limit,
      Title: jobTitleCustom,
      jobType,
      jobLocation: jobLocationCustom,
      jobLocationLat,
      jobLocationLng,
      dateOfPosting: dateOfPosting ? dateOfPosting.value : "",
      salaryEstimate,
      fieldWork: fieldWorkTemp,
      salaryFrom,
      salaryTo,
      radius,
    })
      .then(({ data }) => {
        setJobs(data.result);
        setTotalCards(data.totalRecords);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const getAllKeywordsApi = () => {
    setIsLoading(true);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    jobSellerJobsApi();
  }, [page]);

  useEffect(() => {
    if (Object.keys(user).length !== 0) {
      dispatch(getJobSekkerById(user.JobSeekerId));
      getAllKeywordsApi();
      getLookUpByPrefix("JOBT")
        .then(({ data }) => {
          let formattedData = [];
          data.result.map((e) => {
            formattedData.push({ label: e.Title, value: e.Id });
          });
          setJobType(formattedData);
          setIsLoading(false);
        })
        .catch(() => {
          setIsLoading(false);
        });
      getAllIndustry()
        .then(({ data }) => {
          let formattedData = [];
          data.result.map((e) => {
            formattedData.push({ label: e.Title, value: e.Id });
          });
          setIndustryType(formattedData);
          setIsLoading(false);
        })
        .catch(() => {
          setIsLoading(false);
        });
    } else {
      window.location.href = "/professional";
    }
  }, []);

  useEffect(() => {
    if (title === "") {
      jobSellerJobsApi();
    }
  }, [title]);

  const handlePageChange = (pageNumber) => {
    setPage(pageNumber);
  };

  const filteredDataCall = (title, location, selectedRadius, lat, lng) => {
    setTitle(title);
    setLatitude(lat);
    setLongitude(lng);
    setJobLocation(location);
    setSelectedRangeValue(selectedRadius);
    jobSellerJobsApi(title, location, selectedRadius, lat, lng);
  };

  const handleChangeInput = (event) => {
    if (event.currentTarget.name === "minSalary") {
      setSalaryFrom(event.currentTarget.value);
    } else {
      setSalaryTo(event.currentTarget.value);
    }
  };

  const clearAllFilter = () => {
    setTitle("");
    setDateOfPosting((dateOfPosting = null));
    setContractType((contractType = []));
    setFieldWork((fieldWork = []));
    setSalaryFrom((salaryFrom = ""));
    setSalaryTo((salaryTo = ""));
    setLatitude((latitude = null));
    setLongitude((longitude = null));
    setJobLocation((location = ""));
    jobSellerJobsApi();
  };

  const dateOfPostingChange = (data) => {
    setDateOfPosting(data);
  };

  const fieldChange = (data) => {
    setFieldWork(data);
  };

  const jobTypeChange = (data) => {
    setContractType(data);
  };

  const navigateToDetail = (dataObject) => {
    navigate("/job-details", { state: dataObject });
  };

  return (
    <>
      <Head title="AIDApro | Professional" description="Professional" />
      <Header isOnDashboard={true} dashboard="professional" />
      <div className="dashboard__container__img">
        <Img loading="lazy" src={homePageBackground} alt="homePageBackground" />
        <div className="dashboard__container__img__gradiant"></div>
      </div>
      <div className="dashboard__container">
        <HomeEmploymentSidebar />
        <div className="dashboard__container__content">
          {keywords.length > 0 ? (
            <HomeEmployeeRouter
              setIsFilterOpen={setIsFilterOpen}
              isFilterOpen={isFilterOpen}
              setIsAddDegreeOpen={setIsAddDegreeOpen}
              setEditDegree={setEditDegree}
              setIsAddCertificationsOpen={setIsAddCertificationsOpen}
              setIsAddSkillsOpen={setIsAddSkillsOpen}
              setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
              setAddWorkExperianceEditData={setAddWorkExperianceEditData}
              setIsEditDegreeOpen={setIsEditDegreeOpen}
              setIsEditCertificationsOpen={setIsEditCertificationsOpen}
              setEditCertificationData={setEditCertificationData}
              setIsEditSkillsOpen={setIsEditSkillsOpen}
              setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
              setIsAddSocialsOpen={setIsAddSocialsOpen}
              setIsEditSocialsOpen={setIsEditSocialsOpen}
              filteredDataCall={filteredDataCall}
              latitude={latitude}
              longitude={longitude}
              mapLocation={location}
              title={title}
              setIsEditProjectOpen={setIsEditProjectOpen}
              getSelectedProjectData={getSelectedProjectData}
              setIsUploadProjectOpen={setIsUploadProjectOpen}
              keywords={keywords}
              userName={user ? user.LoginName : ""}
              setSelectedSkills={setSelectedSkills}
              setIsDeleteConfirmation={setIsDeleteConfirmation}
              isDeleteConfirmationResponse={isDeleteConfirmationResponse}
              setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
              setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
              setSelectedHourlyRate={setSelectedHourlyRate}
              selectedRangeValue={selectedRangeValue}
              setIsMessageOpen={setIsMessageOpen}
              setUserData={setUserData}
              setVideoCallUpdate={setVideoCallUpdate}
            />
          ) : null}
        </div>
      </div>
      {window.location.pathname === "/home-professional" ? (
        <div className="dashboard__container__main">
          {isFilterOpen ? (
            <HomeEmploymentFilters
              dateOfPosting={dateOfPosting}
              dateOfPostingChange={dateOfPostingChange}
              jobType={jobType}
              contractType={contractType}
              jobTypeChange={jobTypeChange}
              fieldChange={fieldChange}
              fieldWork={fieldWork}
              industryType={industryType}
              handleChangeInput={handleChangeInput}
              salaryFrom={salaryFrom}
              salaryTo={salaryTo}
              jobSellerJobsApi={jobSellerJobsApi}
              clearAllFilter={clearAllFilter}
            />
          ) : null}
          <div
            className="homepage__container__results"
            style={{ marginTop: "0em" }}
          >
            {isLoading
              ? null
              : jobs &&
                jobs.map((e, i) => (
                  <EmploymentResultCard
                    key={i}
                    data={e}
                    moveTo={navigateToDetail}
                  />
                ))}
          </div>
          <div className="posting__container__pagination">
            <Pagination
              activePage={page}
              itemsCountPerPage={limit}
              totalItemsCount={totalCards}
              pageRangeDisplayed={5}
              onChange={(e) => handlePageChange(e)}
            />
          </div>
        </div>
      ) : null}
    </>
  );
}
