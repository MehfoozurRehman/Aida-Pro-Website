import React, { useState, useEffect, useContext } from "react";
import Pagination from "react-js-pagination";
import { useDispatch, useSelector } from "react-redux";
import { facetimeButtonActive, noData } from "Assets";
import { Head, DashboardHeading } from "Components";
import PlannedVideoCallsList from "Components/PlannedVideoCallsList";
import { getAllVideoRequest, isCallValid, deleteVideoRequest } from "API/Video";
import { CustomError } from "./Toasts";
import UserContext from "Context/UserContext";
import moment from "moment";
import { navigate } from "@reach/router";
import NoData from "Components/NoData";
import {
  getCompanyById,
  getFreelancerById,
  getJobSekkerById,
} from "Redux/Actions/AppActions";
import Img from "react-cool-img";

const PlannedVideoCalls = ({
  isOn,
  setIsMessageOpen,
  setUserData,
  setVideoCallUpdate,
  setIsRequestVideoCallOpen,
  videoCallUpdateSuccess,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
}) => {
  const { notification } = useSelector((state) => state.notification);
  const user = useContext(UserContext);
  const { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);
  const { company } = useSelector((state) => state.company);
  let dispatch = useDispatch();

  useEffect(() => {
    setVideoCallUpdate(null);
    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "company") {
      dispatch(getCompanyById(user.CompanyId));
    } else {
      dispatch(getFreelancerById(user.FreelancerId));
    }
  }, []);

  const [pageLimit, setPageLimit] = useState(10);
  const [pageNo, setPageNo] = useState(1);
  const [totalRecords, setTotalRecords] = useState(0);
  const [allVideoRequest, setAllVideoRequest] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  let [timeZone, setTimeZone] = useState(0);
  const [selectedItem, setSelectedItem] = useState(null);
  const [havePermissions, setHavePermissions] = useState(false);

  useEffect(() => {
    if (Object.keys(company).length > 0) {
      setTimeZone(
        (timeZone =
          company.companyAddressCountry != null
            ? company.companyAddressCountry.timezone_offset
            : 0)
      );
      getAllPlannedList();
    } else if (Object.keys(freelancer).length > 0) {
      setTimeZone(
        (timeZone =
          freelancer.primaryCountry != null
            ? freelancer.primaryCountry.timezone_offset
            : 0)
      );
      getAllPlannedList();
    } else {
      setTimeZone(
        (timeZone =
          jobsekker.primaryCountry != null
            ? jobsekker.primaryCountry.timezone_offset
            : 0)
      );
      getAllPlannedList();
    }
  }, [pageNo, notification]);

  const getAllPlannedList = () => {
    if (timeZone != 0) {
      setIsLoading(true);
      getAllVideoRequest(
        user.CompanyId,
        user.JobSeekerId,
        user.FreelancerId,
        pageNo,
        pageLimit
      )
        .then((data) => {
          if (data.data.success) {
            setAllVideoRequest([]);
            setAllVideoRequest(data.data.result);
            setTotalRecords(data.data.totalRecords);
            setPageNo(data.data.page);
          } else CustomError(data.data.message);
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Some error occured.");
        });
    } else {
      CustomError("Please select country first.");
    }
  };

  useEffect(() => {
    if (videoCallUpdateSuccess != null) {
      getAllPlannedList();
    }
  }, [videoCallUpdateSuccess]);

  const startVideoCallRequest = (itemData) => {
    setSelectedItem(itemData);
    const permissions = navigator.mediaDevices.getUserMedia({ audio: true, video: true })
    permissions.then((stream) => {
      setHavePermissions(prevState => !prevState.havePermissions);
      startVideoCall();
    }).catch((err) => {
      setHavePermissions(false);
      console.log(`${err.name} : ${err.message}`)
      alert("Please give microphone and camera permission first.")
    });
  };

  useEffect(() => {
    if (havePermissions)
      startVideoCall();
  }, [havePermissions])

  const startVideoCall = () => {
    var startD = new Date();
    var startDateUtc = new Date(
      startD.getTime() + startD.getTimezoneOffset() * 60000
    );
    let object = {
      Id: selectedItem.Id,
      Iscompany: user.CompanyId != null ? true : false,
      StartDate: moment(new Date()).format("YYYY-MM-DD"),
      StartTime: moment(startDateUtc).format("HH:mm"),
    };

    setIsLoading(true);
    isCallValid(object)
      .then((data) => {
        if (data.data.success) {
          if (data.data.result != null) {
            window.localStorage.setItem(
              "videoNodeName",
              selectedItem.VideoNodeName
            );
            navigate("/videoChat/" + selectedItem.VideoNodeName, {
              state: { selectedItem: selectedItem, user: user.LoginName },
            });
          } else {
            CustomError(data.data.message);
            navigator.mediaDevices.getUserMedia({ audio: false, video: false })
          }
        } else {
          CustomError(data.data.message);
          navigator.mediaDevices.getUserMedia({ audio: false, video: false })
        }
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Some error occured.");
        navigator.mediaDevices.getUserMedia({ audio: false, video: false })
      });
  };

  useEffect(() => {
    if (isDeleteConfirmationResponse) {
      deleteScheduleRequest();
    }
  }, [isDeleteConfirmationResponse]);

  const deleteScheduleConfirmation = (item) => {
    setIsDeleteConfirmation(true);
    setSelectedItem(item);
  };

  const deleteScheduleRequest = () => {
    let object = {
      Id: selectedItem.Id,
    };
    deleteVideoRequest(object)
      .then((data) => {
        if (data.data.success) {
          let newArray = [...allVideoRequest];
          const indexOfItem = newArray.findIndex(
            (element) => element.Id == selectedItem.Id
          );

          if (indexOfItem > -1) {
            newArray.splice(indexOfItem, 1); // 2nd parameter means remove one item only
          }
          setAllVideoRequest([]);
          setAllVideoRequest(newArray);
        }
      })
      .catch((err) => {
        CustomError("Some error occured.");
      });
    setIsDeleteConfirmation(false);
  };

  const editVideoCall = (item) => {
    setVideoCallUpdate(item);
    setIsRequestVideoCallOpen(true);
  };

  return (
    <>
      <Head
        title="AIDApro | Video Calls Schedules"
        description="Video Calls Schedules"
      />
      <div className="posting__container">
        <DashboardHeading
          heading="Video call schedule"
          svg={facetimeButtonActive}
        />
        {/* <div
          className="homepage__container__jobs__projects__penel__container__details__tabs"
          style={{
            width: "fit-content",
            margin: "1em auto",
            marginTop: "0em",
          }}
          id="homepage__container__jobs__projects__penel__container__details__tabs"
        >
          <ContactTab label="Scheduled" />
          <ContactTab label="History" />
        </div> */}
        <div className="posting__container__table">
          <div className="posting__container__table__header">
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Date
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Name
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              From
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              To
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Timezone
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Agenda
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              {/* Start/Join Call */}
            </div>
            <div
              className="posting__container__table__header__entry animate__animated animate__fadeInDown"
              style={{ opacity: 0 }}
            ></div>
          </div>
          {allVideoRequest.length > 0 ? (
            <>
              <PlannedVideoCallsList
                timeZone={timeZone}
                data={allVideoRequest}
                startVideoCallRequest={startVideoCallRequest}
                isOn={isOn}
                setIsMessageOpen={setIsMessageOpen}
                setUserData={setUserData}
                deleteScheduleRequest={deleteScheduleConfirmation}
                editVideoCall={editVideoCall}
              />
              <div className="posting__container__pagination">
                <Pagination
                  activePage={pageNo}
                  itemsCountPerPage={pageLimit}
                  totalItemsCount={totalRecords}
                  pageRangeDisplayed={5}
                  onChange={setPageNo}
                />
              </div>
            </>
          ) : (
            <div className="no__data__container" style={{ height: 750 }}>
              <Img
                loading="lazy"
                src={noData}
                className="no__data__container__img"
              />
              <div className="no__data__container__text">
                No meetings scheduled yet.
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default PlannedVideoCalls;
