import React, { useEffect } from "react";
import { Head, InputBox } from "Components";
import { crossSvg } from "Assets";

export default function EditDegree({ setIsEditDegreeOpen }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Edit Degree" description="Edit Degree" />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsEditDegreeOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="110.348"
                height="86.268"
                viewBox="0 0 110.348 86.268"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-6"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#fff" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                </defs>
                <g id="degrees" transform="translate(-19.758 -25.831)">
                  <g
                    id="Group_1490"
                    data-name="Group 1490"
                    transform="translate(19.758 25.832)"
                  >
                    <g
                      id="Group_1489"
                      data-name="Group 1489"
                      transform="translate(0 0)"
                    >
                      <g id="Group_1488" data-name="Group 1488">
                        <g
                          id="Group_1472"
                          data-name="Group 1472"
                          transform="translate(18.067 39.391)"
                        >
                          <g id="Group_1471" data-name="Group 1471">
                            <g id="Group_1470" data-name="Group 1470">
                              <path
                                id="Path_21312"
                                data-name="Path 21312"
                                d="M125.536,136.123,94.883,149.451a6.453,6.453,0,0,1-5.146,0L59.083,136.123A6.453,6.453,0,0,1,55.2,130.2V103.11h74.214V130.2a6.455,6.455,0,0,1-3.881,5.918Z"
                                transform="translate(-55.203 -103.11)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1473"
                          data-name="Group 1473"
                          transform="translate(18.067 61.389)"
                        >
                          <path
                            id="Path_21313"
                            data-name="Path 21313"
                            d="M125.536,152.184,94.883,165.512a6.453,6.453,0,0,1-5.146,0L59.083,152.184a6.453,6.453,0,0,1-3.88-5.918v5.1a6.454,6.454,0,0,0,3.88,5.918l30.653,13.328a6.453,6.453,0,0,0,5.146,0l30.654-13.328a6.453,6.453,0,0,0,3.881-5.918v-5.1A6.455,6.455,0,0,1,125.536,152.184Z"
                            transform="translate(-55.203 -146.266)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g id="Group_1476" data-name="Group 1476">
                          <g id="Group_1475" data-name="Group 1475">
                            <g id="Group_1474" data-name="Group 1474">
                              <path
                                id="Path_21314"
                                data-name="Path 21314"
                                d="M72.3,81.733,23.578,59.955a6.453,6.453,0,0,1,0-11.783L72.3,26.393a6.451,6.451,0,0,1,5.267,0l48.721,21.779a6.453,6.453,0,0,1,0,11.783L77.565,81.733a6.453,6.453,0,0,1-5.268,0Z"
                                transform="translate(-19.758 -25.832)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1478"
                          data-name="Group 1478"
                          transform="translate(17.046 2.038)"
                        >
                          <g id="Group_1477" data-name="Group 1477">
                            <path
                              id="Path_21315"
                              data-name="Path 21315"
                              d="M54.22,48.134a1.019,1.019,0,0,1-.413-1.952L89.528,30.217a4.353,4.353,0,0,1,1.794-.387h.01a4.353,4.353,0,0,1,1.794.387l8.212,3.665a1.027,1.027,0,0,1,.515,1.35,1.052,1.052,0,0,1-1.351.515L92.3,32.078a2.348,2.348,0,0,0-1.937,0L54.643,48.042h-.005A.98.98,0,0,1,54.22,48.134ZM104.642,37.5a1.061,1.061,0,0,1-.413-.087,1.007,1.007,0,0,1-.535-.566.987.987,0,0,1,.02-.78,1.036,1.036,0,0,1,1.346-.515,1.02,1.02,0,0,1,.515,1.345,1.019,1.019,0,0,1-.933.6Z"
                              transform="translate(-53.2 -29.83)"
                              fill="url(#linear-gradient)"
                            />
                          </g>
                        </g>
                        <g
                          id="Group_1479"
                          data-name="Group 1479"
                          transform="translate(0.002 23.005)"
                        >
                          <path
                            id="Path_21316"
                            data-name="Path 21316"
                            d="M127.445,70.963a6.665,6.665,0,0,1-1.157.664L77.568,93.405a6.451,6.451,0,0,1-5.267,0L23.58,71.628a6.7,6.7,0,0,1-1.157-.664A6.458,6.458,0,0,0,23.58,82.082L72.3,103.86a6.452,6.452,0,0,0,5.267,0l48.721-21.778A6.458,6.458,0,0,0,127.445,70.963Z"
                            transform="translate(-19.762 -70.963)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g
                          id="Group_1482"
                          data-name="Group 1482"
                          transform="translate(5.85 21.912)"
                        >
                          <g id="Group_1481" data-name="Group 1481">
                            <g id="Group_1480" data-name="Group 1480">
                              <path
                                id="Path_21317"
                                data-name="Path 21317"
                                d="M32.849,112.65a1.614,1.614,0,0,1-1.614-1.614V82.66A1.612,1.612,0,0,1,32.448,81.1l47.71-12.226a1.613,1.613,0,0,1,.8,3.125l-46.5,11.916v27.125a1.613,1.613,0,0,1-1.613,1.614Z"
                                transform="translate(-31.235 -68.819)"
                                fill="url(#linear-gradient-6)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1485"
                          data-name="Group 1485"
                          transform="translate(2.406 53.777)"
                        >
                          <g
                            id="Group_1484"
                            data-name="Group 1484"
                            transform="translate(0 0)"
                          >
                            <g id="Group_1483" data-name="Group 1483">
                              <path
                                id="Path_21318"
                                data-name="Path 21318"
                                d="M32.5,145.853H26.57a2.091,2.091,0,0,1-2.091-2.091V136.39a5.057,5.057,0,1,1,10.114,0v7.372A2.091,2.091,0,0,1,32.5,145.853Z"
                                transform="translate(-24.479 -131.333)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1486"
                          data-name="Group 1486"
                          transform="translate(2.406 53.777)"
                        >
                          <path
                            id="Path_21319"
                            data-name="Path 21319"
                            d="M29.535,131.333l-.04,0v7.33a2.091,2.091,0,0,1-2.091,2.091H24.478v3.006a2.091,2.091,0,0,0,2.091,2.091H32.5a2.091,2.091,0,0,0,2.091-2.091V136.39A5.057,5.057,0,0,0,29.535,131.333Z"
                            transform="translate(-24.478 -131.333)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g
                          id="Group_1487"
                          data-name="Group 1487"
                          transform="translate(2.406 54.221)"
                        >
                          <path
                            id="Path_21320"
                            data-name="Path 21320"
                            d="M31.6,132.2a5.032,5.032,0,0,1,.444,2.065v7.372a2.091,2.091,0,0,1-2.091,2.091H24.479v.458a2.091,2.091,0,0,0,2.091,2.091H32.5a2.091,2.091,0,0,0,2.091-2.091v-7.372A5.055,5.055,0,0,0,31.6,132.2Z"
                            transform="translate(-24.479 -132.204)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Edit Degree
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  Edit Your Educational History here
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox variant="simple" placeholder="Degree Name" />
                <InputBox
                  variant="simple"
                  placeholder="University / Institute"
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Field Of Study"
                  options={[
                    {
                      value: "BBA",
                      label: "BBA",
                    },
                    {
                      value: "MBA",
                      label: "MBA",
                    },
                    {
                      value: "BSCS",
                      label: "BSCS",
                    },
                  ]}
                />
                <InputBox variant="simple" placeholder="GPA" />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox variant="date" placeholder="Starting Year" />
                <InputBox variant="date" placeholder="Ending Year" />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  setIsEditDegreeOpen(false);
                }}
                title="edit degree"
              >
                Edit Degree
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
