import React, { useState, useContext, useEffect } from "react";
import { Head, InputBox } from "Components";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import {
  jobSekkerCertificateUpdate,
  jobSekkerCertificateEdit,
} from "../API/EmploymentAPI";
import {
  freelancerCertificateUpdate,
  freelancerCertificateEdit,
} from "../API/FreelancerApi";
import { isNullOrEmpty } from "Utils/TextUtils";
import { months, years } from "Constants/Constants";
import { crossSvg } from "Assets";

export default function AddCertifications({
  setIsAddCertificationsOpen,
  editCertificationData,
}) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  let [certificationName, setCertificationName] = useState(
    editCertificationData != null ? editCertificationData.certificateName : ""
  );
  let [university, setUniversity] = useState(
    editCertificationData != null ? editCertificationData.institueName : ""
  );
  let [certificationId, setCertificationId] = useState("");
  let [certificationURL, setCertificationURL] = useState("");
  let [issueDate, setIssueDate] = useState("");
  let [expirationDate, setExpirationDate] = useState("");
  const [isExpirationDate, setIsExpirationDate] = useState(
    editCertificationData != null
      ? editCertificationData.dateTo == null
        ? true
        : false
      : false
  );
  const [isLoading, setIsLoading] = useState(false);

  const [startMonth, setStartMonth] = useState("");
  const [startYear, setStartYear] = useState("");
  let [endMonth, setEndMonth] = useState("");
  let [endYear, setEndYear] = useState("");

  //validation
  let [certificateNameError, setCertificateNameError] = useState(false);
  let [certificateNameErrorMessage, setCertificateNameErrorMessage] =
    useState("");
  let [universityError, setUniversityError] = useState(false);
  let [universityErrorMessage, setUniversityErrorMessage] = useState("");
  let [certificateIdError, setCertificateIdError] = useState(false);
  let [certificateIdErrorMessage, setCertificateIdErrorMessage] = useState("");
  let [certificateUrlError, setCertificateUrlError] = useState(false);
  let [certificateUrlErrorMessage, setCertificateUrlErrorMessage] =
    useState("");
  let [issueDateError, setIssueDateError] = useState(false);
  let [issueDateErrorMessage, setIssueDateErrorMessage] = useState("");
  let [expirationDateError, setExpirationDateError] = useState(false);
  let [expirationDateErrorMessage, setExpirationDateErrorMessage] =
    useState("");

  const [startMonthError, setStartMonthError] = useState(false);
  const [startMonthErrorMessage, setStartMonthErrorMessage] = useState("");
  const [startYearError, setStartYearError] = useState(false);
  const [startYearErrorMessage, setStartYearErrorMessage] = useState("");

  const [endMonthError, setEndMonthError] = useState(false);
  const [endMonthErrorMessage, setEndMonthErrorMessage] = useState("");
  const [endYearError, setEndYearError] = useState(false);
  const [endYearErrorMessage, setEndYearErrorMessage] = useState("");

  const user = useContext(UserContext);
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);
  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    if (editCertificationData != null) {
      const start_month = months.find(
        (element) => element.label == editCertificationData.StartMonth
      );
      setStartMonth(start_month);
      const start_year = years.find(
        (element) => element.label == editCertificationData.StartYear
      );
      setStartYear(start_year);
      if (isExpirationDate == false) {
        const end_month = months.find(
          (element) => element.label == editCertificationData.EndMonth
        );
        setEndMonth(end_month);
        const end_year = years.find(
          (element) => element.label == editCertificationData.EndYear
        );
        setEndYear(end_year);
      }
    }
  }, []);

  const handleChange = (event) => {
    if (event.currentTarget.name === "name") {
      if (isNullOrEmpty(event.currentTarget.value))
        setCertificationNameErrorMessageAndVisibility(
          "Please enter certificate name"
        );
      else {
        setCertificationNameErrorMessageAndVisibility("");
      }
      setCertificationName(event.currentTarget.value);
    } else if (event.currentTarget.name === "university") {
      if (isNullOrEmpty(event.currentTarget.value))
        setUniversityNameErrorMessageAndVisibility(
          "Please enter university / institute"
        );
      else {
        setUniversityNameErrorMessageAndVisibility("");
      }
      setUniversity(event.currentTarget.value);
    }
  };

  const issueDateChange = (event) => {
    if (event.target.value === "") {
      setIssueDateError(true);
      setIssueDateErrorMessage("Please enter issue date");
    } else {
      setIssueDateError(false);
      setIssueDateErrorMessage("");
    }
    setIssueDate(event.target.value);
  };

  const expirationDateChange = (event) => {
    if (event.target.value === "") {
      setExpirationDateError(true);
      setExpirationDateErrorMessage("Please enter expiration date");
    } else if (event.target.value < issueDate) {
      setExpirationDateError(true);
      setExpirationDateErrorMessage("End date can't be less than start date.");
    } else {
      setExpirationDateError(false);
      setExpirationDateErrorMessage("");
    }
    setExpirationDate(event.target.value);
  };

  const viewIsValid = () => {
    if (isNullOrEmpty(certificationName))
      setCertificationNameErrorMessageAndVisibility(
        "Please enter certificate name"
      );
    else if (isNullOrEmpty(university))
      setUniversityNameErrorMessageAndVisibility(
        "Please enter university / institute"
      );
    else if (isNullOrEmpty(startMonth)) {
      setStartMonthError(true);
      setStartMonthErrorMessage("Please select starting month.");
    } else if (isNullOrEmpty(startYear)) {
      setStartYearError(true);
      setStartYearErrorMessage("Please select starting year");
    } else if (isExpirationDate == false) {
      if (isNullOrEmpty(endMonth)) {
        setEndMonthError(true);
        setEndMonthErrorMessage("Please select ending month");
      } else if (isNullOrEmpty(endYear)) {
        setEndYearError(true);
        setEndYearErrorMessage("Please select ending year");
      } else if (endYear.label < startYear.label) {
        setEndYearError(true);
        setEndYearErrorMessage("Ending year can't be less than starting year.");
      } else return true;
    } else return true;
    return false;
  };

  const setCertificationNameErrorMessageAndVisibility = (text) => {
    setCertificateNameError((certificateNameError = !isNullOrEmpty(text)));
    setCertificateNameErrorMessage(text);
  };
  const setUniversityNameErrorMessageAndVisibility = (text) => {
    setUniversityError((universityError = !isNullOrEmpty(text)));
    setUniversityErrorMessage(text);
  };

  const saveCertification = () => {
    if (viewIsValid()) {
      if (isExpirationDate) {
        setEndMonth((endMonth = ""));
        setEndYear((endYear = ""));
      }
      setIsLoading(true);
      if (user.Role.Title !== "JobSekker") {
        let certificateObject = {
          Id: editCertificationData != null ? editCertificationData.Id : 0,
          CertificateName: certificationName,
          FreelancerId: user.FreelancerId,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
          Institute: university,
        };
        if (editCertificationData != null) {
          freelancerCertificateEdit(certificateObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Certificate Edited Successfully...");
              setIsAddCertificationsOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Certificate ");
              setIsAddCertificationsOpen(false);
            });
        } else {
          freelancerCertificateUpdate(certificateObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Certificate Update Successfully...");
              setIsAddCertificationsOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Certificate ");
              setIsAddCertificationsOpen(false);
            });
        }
      } else {
        let certificateObject = {
          Id: editCertificationData != null ? editCertificationData.Id : 0,
          CertificateName: certificationName,
          jobseekerId: user.JobSeekerId,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
          Institute: university,
        };
        if (editCertificationData != null) {
          jobSekkerCertificateEdit(certificateObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Certificate Edited Successfully...");
              setIsAddCertificationsOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Certificate ");
              setIsAddCertificationsOpen(false);
            });
        } else {
          jobSekkerCertificateUpdate(certificateObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Certificate Update Successfully...");
              setIsAddCertificationsOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Certificate ");
              setIsAddCertificationsOpen(false);
            });
        }
      }
    }
  };

  const setExpirationDisabled = (e) => {
    setIsExpirationDate(e.currentTarget.checked);
  };

  return (
    <>
      <Head
        title="AIDApro | Add certifications"
        description="Add certifications"
      />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsAddCertificationsOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="86.122"
                height="98.621"
                viewBox="0 0 86.122 98.621"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                </defs>
                <g
                  id="Page-1"
                  transform="matrix(0.574, -0.819, 0.819, 0.574, -25.423, 57.352)"
                >
                  <g
                    id="_001---Degree"
                    data-name="001---Degree"
                    transform="translate(-0.018 31.049)"
                  >
                    <path
                      id="Shape"
                      d="M52.125,34.2a34.5,34.5,0,0,1,1.181,7.662,11.368,11.368,0,0,0-11.4,1.06,44.259,44.259,0,0,0-1.125-8.735q2.977.062,6.106.062C48.673,34.245,50.412,34.226,52.125,34.2Zm-3.6,26.261a8.191,8.191,0,1,1,8.191-8.191A8.191,8.191,0,0,1,48.521,60.457ZM1.012,45.713A45.948,45.948,0,0,1,2.65,31.788,45.948,45.948,0,0,1,4.288,45.713,45.948,45.948,0,0,1,2.65,59.638,45.948,45.948,0,0,1,1.012,45.713ZM5.964,59.918a54.329,54.329,0,0,0,1.6-14.205,54.341,54.341,0,0,0-1.6-14.207A287.439,287.439,0,0,0,37.355,34.1a42.172,42.172,0,0,1,1.337,11.614c0,.238,0,.464-.01.691a11.35,11.35,0,0,0-.528,10.734l-.072.174A285.5,285.5,0,0,0,5.964,59.918ZM42.635,71.925l-1.2-2.752a1.638,1.638,0,0,0-2.148-.852L36.534,69.54l3.842-9.21a11.456,11.456,0,0,0,5.786,3.162Zm15.116-3.6a1.638,1.638,0,0,0-2.148.852L54.391,71.9l-3.512-8.409a11.468,11.468,0,0,0,5.791-3.163L60.5,69.5Zm33.037-7.946a265.314,265.314,0,0,0-31.782-2.949l-.116-.282a11.4,11.4,0,0,0-2.208-12.919,45.6,45.6,0,0,0-1.17-10.093,273.463,273.463,0,0,0,35.277-3.083c.76,1.291,1.966,6.4,1.966,14.664S91.54,59.084,90.788,60.375Z"
                      transform="translate(-0.982 -31.049)"
                      fill="url(#linear-gradient)"
                    />
                  </g>
                </g>
              </svg>

              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  {editCertificationData != null ? "Edit " : "Add "}
                  certifications
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  {editCertificationData != null ? "Edit " : "Add "}your
                  certifications history here.
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Certification name"
                  name="name"
                  value={certificationName}
                  error={certificateNameError}
                  errorMessage={certificateNameErrorMessage}
                  onChange={(e) => handleChange(e)}
                />
                <InputBox
                  placeholder="University / Institute"
                  name="university"
                  value={university}
                  error={universityError}
                  errorMessage={universityErrorMessage}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <div
                  className="homepage__container__jumbotron__form__filters__role"
                  style={{ marginLeft: 10 }}
                >
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-credential-does-not-expire"
                    type="checkbox"
                    value="Remember"
                    defaultChecked={isExpirationDate}
                    defaultValue={isExpirationDate}
                    name="Remember"
                    onClick={(e) => setExpirationDisabled(e)}
                  />
                  <label htmlFor="styled-checkbox-credential-does-not-expire">
                    This certificate does not expire
                  </label>
                </div>
              </div>
              <div
                className="pupup__container__from__wrapper__header__content__info"
                style={{ marginLeft: 5, marginBottom: 10, marginTop: -10 }}
              >
                Start Date
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Month"
                  options={months}
                  name="issueDate"
                  error={startMonthError}
                  errorMessage={startMonthErrorMessage}
                  type="month"
                  value={startMonth}
                  onChange={(e) => {
                    setStartMonthError(false);
                    setStartMonth(e);
                  }}
                />
                <InputBox
                  variant="select"
                  placeholder="Year"
                  name="expirationDate"
                  options={years}
                  error={startYearError}
                  errorMessage={startYearErrorMessage}
                  type="month"
                  minDate={issueDate}
                  value={startYear}
                  onChange={(e) => {
                    setStartYearError(false);
                    setStartYear(e);
                  }}
                />
              </div>
              {isExpirationDate ? null : (
                <>
                  <div
                    className="pupup__container__from__wrapper__header__content__info"
                    style={{ marginLeft: 5, marginBottom: 10, marginTop: -10 }}
                  >
                    End Date
                  </div>
                  <div className="pupup__container__from__wrapper__form__row">
                    <InputBox
                      variant="select"
                      placeholder="Month"
                      name="issueDate"
                      options={months}
                      error={endMonthError}
                      errorMessage={endMonthErrorMessage}
                      type="month"
                      value={endMonth}
                      onChange={(e) => {
                        setEndMonthError(false);
                        setEndMonth(e);
                      }}
                    />
                    <InputBox
                      variant="select"
                      options={years}
                      placeholder="Year"
                      name="expirationDate"
                      error={endYearError}
                      errorMessage={endYearErrorMessage}
                      type="month"
                      value={endYear}
                      onChange={(e) => {
                        setEndYearError(false);
                        setEndYear(e);
                      }}
                    />
                  </div>
                </>
              )}
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="button"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => saveCertification()}
                disabled={isLoading ? true : false}
                title="add certification"
              >
                {isLoading ?
                  ("Loading...")
                  :
                  (editCertificationData != null ? "Edit certification" : "Add certification")
                }
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
