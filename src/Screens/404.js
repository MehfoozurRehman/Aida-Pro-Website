import React from "react";
import { Link } from "@reach/router";
import { Head } from "Components";
import Img from "react-cool-img";
import { NotFoundImg } from "Assets";

export function PageNotFound() {
  return (
    <>
      <Head title="AIDApro | 404" description="Page not found" />
      <div
        style={{
          minHeight: "100vh",
          minWidth: "100vw",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          padding: "5em",
        }}
      >
        <Img
          loading="lazy"
          style={{ width: "300px" }}
          src={NotFoundImg}
          alt="404-not-found"
        />
        <Link
          to="/"
          className="header__nav__btn btn__secondary"
          style={{ width: 180, marginTop: "3em" }}
        >
          Back to home
        </Link>
      </div>
    </>
  );
}
