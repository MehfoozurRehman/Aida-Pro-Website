import React, { useState, useContext, useEffect } from "react";
import { Head, InputBox } from "Components";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import { jobSekkerProfessionalDetailsUpdate } from "../API/EmploymentAPI";
import { freelacerProfessionalDetailsUpdate } from "../API/FreelancerApi";
import Img from "react-cool-img";
import { CustomError, CustomSuccess } from "./Toasts";
import { getLookUpByPrefix } from "API/Api";
import { crossSvg, HourlyRateSvg } from "Assets";
import { PART_TIME } from "Utils/Constants";

export default function SetHourlyRate({
  setIsSetHourlyRateOpen,
  selectedHourlyRate,
}) {
  const [hourlyRate, setHourlyRate] = useState(selectedHourlyRate);
  const [joiningAvailbiltyAPI, setJoiningAvailbiltyAPI] = useState([]);
  const [joiningAvailbilty, setJoiningAvailbilty] = useState([]);
  const [canRelocate, setCanRelocate] = useState(0);
  const [workOffice, setWorkOffice] = useState(0);
  const [workRemote, setWorkRemote] = useState(true);
  const [jobTypeDropDownSelected, setJobTypeDropDownSelected] = useState(null);

  const user = useContext(UserContext);
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    setJoiningAvailbilty(jobsekker.Availbility);
    setCanRelocate(jobsekker.canRelocate);
    setWorkRemote(jobsekker.workRemote);
    setWorkOffice(jobsekker.workOffice);
    setJobTypeDropDownSelected(jobsekker.JobTypeLookupDetail);
    getLookUpByPrefix("JOINAVAIL")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setJoiningAvailbiltyAPI(formattedData);
      })
      .catch((error) => {
        // console.log(error);
      });
  }, []);

  const saveProfessionalDetailData = () => {
    let relocate = canRelocate;
    let officeWork = workOffice;
    if (jobTypeDropDownSelected != null) {
      if (
        jobTypeDropDownSelected.value == PART_TIME ||
        jobTypeDropDownSelected.value == PART_TIME
      ) {
        relocate = false;
        officeWork = false;
      }
    }

    if (user.JobSeekerId) {
      let data = {
        Id: user.JobSeekerId,
        ExpectedSalary: hourlyRate,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        WorkOffice: officeWork ? 1 : 0,
        CanRelocate: relocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      jobSekkerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          window.location.reload();
          //CustomSuccess("Professional Detail Update Successfully...");
        })
        .catch((error) => {
          CustomError("Failed to Update Professional Detail ");
        });
    } else {
      let data = {
        Id: user.FreelancerId,
        HourlyRate: hourlyRate,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        WorkOffice: officeWork ? 1 : 0,
        CanRelocate: relocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      freelacerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          window.location.reload();
          //CustomSuccess("Professional Detail Update Successfully...");
        })
        .catch((error) => {
          CustomError("Failed to Update Professional Detail ");
        });
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Add Skills" description="Add skills" />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
          onSubmit={() => {
            setIsSetHourlyRateOpen(false);
          }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={(e) => {
              e.preventDefault();
              setIsSetHourlyRateOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <Img
                loading="lazy"
                src={HourlyRateSvg}
                alt="HourlyRateSvg"
                style={{ width: 40, height: 40, marginRight: ".5em" }}
              />
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Work Details
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  Add your hourly rate hare (€) and select when you can start
                  working
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Amount"
                  value={hourlyRate}
                  style={{
                    height: "fit-content",
                  }}
                  onChange={(event) => {
                    setHourlyRate(event.currentTarget.value);
                  }}
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Select"
                  options={joiningAvailbiltyAPI}
                  value={joiningAvailbilty}
                  onChange={(event) => setJoiningAvailbilty(event)}
                  style={{
                    height: "fit-content",
                    minHeight: "45px",
                  }}
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  if (joiningAvailbilty.length > 0 || hourlyRate != null)
                    saveProfessionalDetailData();
                  else setIsSetHourlyRateOpen(false);
                }}
                title="set professional data"
              >
                Set
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
