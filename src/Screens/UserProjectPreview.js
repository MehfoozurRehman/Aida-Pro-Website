import React, { useEffect, useState } from "react";
import { jobSeekerProjectPortfolioDelete } from "../API/EmploymentAPI";
import { freelancerProjectPortfolioDelete } from "../API/FreelancerApi";
import { CustomError, CustomSuccess } from "./Toasts";
import { pictureplaceholder } from "Assets";
import { getText } from "Utils/functions";
import { Head } from "Components";
import Img from "react-cool-img";

export default function UserProjectPreview({
  location,
  setIsEditProjectOpen,
  getSelectedProjectData,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
}) {
  const data = location.state.dataObject;

  const [uploadedImages, setUploadedImages] = useState([]);

  useEffect(() => {
    if (data.JobSeekerId != null && data.JobSeekerId != undefined)
      setUploadedImages(data.JobSeekerUploads);
    else setUploadedImages(data.FreelancerUploads);
  }, []);

  useEffect(() => {
    deleteProjectJobPortfolio();
  }, [isDeleteConfirmationResponse]);

  const deletePortFolio = () => {
    setIsDeleteConfirmation(true);
    deleteProjectJobPortfolio();
  };

  const deleteProjectJobPortfolio = () => {
    if (isDeleteConfirmationResponse) {
      if (data.JobSeekerId != null && data.JobSeekerId != undefined)
        deleteJobSeekerProject();
      else deleteFreeLancerProject();
    }
  };

  const deleteJobSeekerProject = () => {
    let deleteObject = {
      Id: data.Id,
    };

    jobSeekerProjectPortfolioDelete(deleteObject)
      .then((data) => {
        //CustomSuccess("Project deleted successfully.");
        window.location.href = "/home-professional/project";
      })
      .catch((error) => {
        CustomError("Failed to delete project.");
      });
  };

  const deleteFreeLancerProject = () => {
    let deleteObject = {
      Id: data.Id,
    };

    freelancerProjectPortfolioDelete(deleteObject)
      .then((data) => {
        //CustomSuccess("Project deleted successfully.");
        window.location.href = "/home-freelancer/project";
      })
      .catch((error) => {
        CustomError("Failed to delete project.");
      });
  };

  const ShowUploadImages = ({ item }) => {
    return (
      <div className="homepage__container__result__card">
        <Img
          loading="lazy"
          src={
            item.UploadFilePath != null && item.UploadFilePath != ""
              ? process.env.REACT_APP_BASEURL.concat(item.UploadFilePath) +
                "?" +
                new Date()
              : pictureplaceholder
          }
          alt="project_img"
          className="homepage__container__result__card__img"
          style={{ marginBottom: 0 }}
        />
      </div>
    );
  };

  return (
    <>
      <Head
        title="AIDApro | Freelancer Project Preview"
        description="Freelancer Project Preview"
      />
      <div className="freelancer__project__preview__container">
        <div className="freelancer__project__preview__header">
          <div className="freelancer__project__preview__header__left">
            <div className="freelancer__project__preview__header__left__heading">
              {data.ProjectName}
            </div>
          </div>
          <div className="freelancer__project__preview__header__right">
            <button
              className="header__nav__btn btn__secondary"
              onClick={() => {
                getSelectedProjectData(data);
                setIsEditProjectOpen(true);
              }}
              title="edit project"
              style={{ width: 150 }}
            >
              Edit Project
            </button>
            <button
              className="header__nav__btn btn__secondary"
              style={{
                width: 150,
                background: "linear-gradient(#DB1414,#FF0E0E)",
              }}
              onClick={() => {
                deletePortFolio();
              }}
              title="delete project"
            >
              Delete Project
            </button>
          </div>
        </div>
        <div className="freelancer__project__preview__container__info">
          {getText(data.ProjectDescription)}
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            marginTop: "2em",
          }}
          className="freelancer__project__preview__header__left__heading"
        >
          Related Images
        </div>
        <div className="homepage__container__results">
          {uploadedImages.length > 0
            ? uploadedImages.map((item, i) => (
                <ShowUploadImages item={item} key={i} />
              ))
            : null}
        </div>
      </div>
    </>
  );
}
