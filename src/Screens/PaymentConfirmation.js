import React, { useEffect } from "react";
import { Head } from "Components";
import { Link } from "@reach/router";
import { crossSvg } from "Assets";

export default function PaymentConfirmation({
  setIsPaymentConfirmation,
  setDataAfterPaymentConfirmation,
}) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Email Verification"
        description="Email Verification"
      />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsPaymentConfirmation(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              {/* <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "25px",
                  textAlign: "center",
                  marginTop: 0,
                }}
              >
                Payment Confirmation
              </div> */}
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "18px", textAlign: "center", marginTop: 0 }}
              >
                1 credit will get deducted.
              </div>
              <div
                style={{
                  display: "flex",
                  width: "fit-content",
                  margin: "0em auto",
                }}
              >
                <div
                  type="submit"
                  className="header__nav__btn btn__primary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "35px",
                    width: "100px",
                    marginRight: ".5em",
                  }}
                  // to="/home-company/posting/details"
                  // state={{
                  //   redirectFrom: dataAfterPaymentConfirmation.redirectFrom,
                  //   listOfUsers: dataAfterPaymentConfirmation.listOfUsers,
                  //   selectedUser: dataAfterPaymentConfirmation.selectedUser,
                  // }}
                  onClick={() => {
                    setIsPaymentConfirmation(false);
                    setDataAfterPaymentConfirmation(true);
                  }}
                >
                  Continue
                </div>
                <button
                  type="submit"
                  className="header__nav__btn btn__secondary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2.4em",
                    height: "35px",
                    width: "100px",
                    fontSize: 13,
                  }}
                  onClick={() => {
                    setIsPaymentConfirmation(false);
                  }}
                  title="not now"
                >
                  Not Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
