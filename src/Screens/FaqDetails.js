import Header from "Components/Header";
import React, { useState } from "react";
import { Head } from "Components";
import { Link } from "@reach/router";
import { useEffect } from "react";

export default function FaqDetails({ setIsLoginOpen }) {
  const [data, setData] = useState([]);

  useEffect(async () => {
    await setData(JSON.parse(window.localStorage.getItem("faqData")));
  }, []);

  return (
    <>
      <Head
        title="AIDApro | Frequently Asked Questions"
        description="Frequently Asked Questions"
      />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <div className="faq__container">
        <div className="faq__container__content">
          <div className="faq__container__content__left">
            <Link to="/faq" className="faq__container__content__left__button">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
                className="feather feather-chevron-left"
              >
                <polyline points="15 18 9 12 15 6"></polyline>
              </svg>
              Back
            </Link>
            <div className="faq__container__content__heading">{data.title}</div>
            {data.info &&
              data.info.map((item, i) => (
                <div className="faq__container__content__info" key={i}>
                  {item.paragraph}
                </div>
              ))}
          </div>
          <div className="faq__container__content__right">
            <div className="faq__container__content__right__content__heading">
              Other questions?
            </div>
            <div className="faq__container__content__right__content__text">
              Simply fill in the contact form. We’ll get back to you asap.
            </div>
            <Link
              to="/contact"
              className="header__nav__btn btn__secondary"
              style={{
                width: "100%",
                height: 45,
                fontSize: 15,
                margin: "0px auto",
              }}
            >
              Contact Us
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
