import React, { useState, useEffect, useContext } from "react";
import { Link, navigate } from "@reach/router";
import Header from "Components/Header";
import { jobSekkerJobsForHomePage } from "../API/CompanyAPI";
import { getAllKeywords } from "../API/Api";
import UserContext from "Context/UserContext";
import CompanyResultCard from "Components/HomePageCompanyResultCard";
import HomeComapanySearchFilter from "Components/HomeComapanySearchFilter";
import { Head, MobileAppAdvert, Testmonials } from "Components";
import Img from "react-cool-img";
import { isUserLoggedIn } from "Utils/common";
import _ from "lodash";
import {
  accurateJobMatches,
  filteredSkills,
  hassleFreeJobRecruitment,
  homePageCompanySvg,
  qualificationCheck,
  screeningByExperts,
  vacancy,
} from "Assets";
import { companyDefaultListing } from "Constants/defaultData";
import HomepageCompanyJumbotron from "Components/HomepageCompanyJumbotron";

const HomepageCompany = ({
  setIsLoginOpen,
  setRedirectURLFromLogin,
  setDataAfterPaymentConfirmation,
}) => {
  localStorage.setItem("isOn", "company");
  const [searchBy, setSearchBy] = useState("Employment");
  const [searchByFreelancer, setSearchByFreelancer] = useState("Freelancer");
  let [title, setTitle] = useState("");
  let [jobLocation, setJobLocation] = useState("");
  const [jobSeekers, setJobSeekers] = useState(companyDefaultListing);
  let [range, setRange] = useState(500);
  const [keywords, setKeywords] = useState(false);
  const user = useContext(UserContext);

  useEffect(() => {
    let response = isUserLoggedIn(user);
    if (response) {
      if (user.Freelancer != null) navigate("/home-freelancer");
      else if (user.JobSeeker != null) navigate("/home-professional");
      else if (user.CompanyProfile != null) navigate("/home-company");
    }
  }, [user]);

  const jobSekkerRecentJobsForCompanyAPI = () => {
    jobSekkerJobsForHomePage(
      1,
      6,
      title,
      jobLocation,
      searchBy,
      searchByFreelancer,
      range
    )
      .then(({ data }) => {
        let finalList = [];
        if (data && data.result) {
          finalList = data.result;
        }
        setJobSeekers(finalList);
        getAllKeywordsApi();
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  const getAllKeywordsApi = () => {
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
      })
      .catch((err) => {
        // console.log(err);
      });
  };

  useEffect(() => {
    jobSekkerRecentJobsForCompanyAPI();
  }, [searchBy, searchByFreelancer]);

  useEffect(() => {
    setRedirectURLFromLogin("/home-company");
  }, []);

  const searchDataByTitle = (data) => {
    setTitle((title = data));
  };

  const searchByLocation = (data) => {
    setJobLocation((jobLocation = data.formatted_address));
  };

  const searchByKeyword = (data) => {
    setTitle((title = data));
  };

  const searchByUser = (freelancer, employee) => {
    setSearchBy(employee ? "Employment" : "");
    setSearchByFreelancer(freelancer ? "Freelancer" : "");
  };

  const searchByRange = (value) => {
    setRange((range = value));
  };

  const resetFilter = () => {
    setRange((range = 500));
    setJobLocation((jobLocation = ""));
    setTitle((title = ""));
    setTimeout(() => {
      jobSekkerRecentJobsForCompanyAPI();
    }, 800);
  };

  return (
    <>
      <Head title="AIDApro | Company" description="Company" />
      <Header
        isOnHomeCompany={true}
        setIsLoginOpen={setIsLoginOpen}
        redirectURLSignUp="/sign-up"
      />
      <HomepageCompanyJumbotron
        keywords={keywords}
        searchDataByTitle={searchDataByTitle}
        jobSekkerRecentJobsForCompanyAPI={jobSekkerRecentJobsForCompanyAPI}
        searchByLocation={searchByLocation}
        searchByKeyword={searchByKeyword}
        searchByRange={searchByRange}
        title={title}
        searchBy={searchBy}
        searchByFreelancer={searchByFreelancer}
        searchByUser={searchByUser}
        setIsLoginOpen={setIsLoginOpen}
        homePageCompanySvg={homePageCompanySvg}
        resetFilter={resetFilter}
        range={range}
        jobLocation={jobLocation}
      />
      <div className="homepage__container">
        <div className="homepage__container__results">
          {jobSeekers.map((e, i) => (
            <CompanyResultCard
              key={i}
              data={e}
              redirectedFrom="Company"
              allRecords={jobSeekers}
              isFreelancer={e.Type == "Freelancer" ? true : false}
              setIsPaymentConfirmation={setIsLoginOpen}
              setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
            />
          ))}
        </div>
        {user.Id !== undefined ? (
          <Link
            to="/home-company"
            className="header__nav__btn btn__secondary"
            style={{
              width: 200,
              height: 50,
              fontSize: 15,
              margin: "0em auto",
              marginTop: "-4em",
              marginBottom: "4em",
            }}
          >
            Dashboard
          </Link>
        ) : (
          <Link
            to="/sign-up"
            className="header__nav__btn btn__secondary"
            style={{
              width: 200,
              height: 50,
              fontSize: 15,
              margin: "0em auto",
              marginTop: "-4em",
              marginBottom: "4em",
            }}
          >
            Sign up to see more
          </Link>
        )}

        <div className="homepage__container__features">
          <div className="homepage__container__feature">
            <Img
              loading="lazy"
              src={accurateJobMatches}
              alt="homepage__container__feature__img"
              className="homepage__container__feature__img"
            />
            <div className="homepage__container__feature__text">
              Find the right professionals
            </div>
            <div className="homepage__container__feature__tooltip">
              Search for the right professional or place vacancies or projects
              to let them apply. AIDApro’s video, chat, map, and other features
              can support your selection.
            </div>
          </div>
          <div className="homepage__container__feature">
            <Img
              loading="lazy"
              src={hassleFreeJobRecruitment}
              alt="homepage__container__feature__img"
              className="homepage__container__feature__img"
            />
            <div className="homepage__container__feature__text">
              Professional screening
            </div>
            <div className="homepage__container__feature__tooltip">
              Our experts can screen your selected professionals to verify their
              background and profile.
            </div>
          </div>
          <div className="homepage__container__feature">
            <Img
              loading="lazy"
              src={screeningByExperts}
              alt="homepage__container__feature__img"
              className="homepage__container__feature__img"
            />
            <div className="homepage__container__feature__text">
              Recruitment services
            </div>
            <div className="homepage__container__feature__tooltip">
              We can provide customized services for professional recruitment.
              For all your AI and data vacancies and projects.
            </div>
          </div>
        </div>
        <div className="homepage__container__vacancy">
          <div className="homepage__container__vacancy__content">
            <div className="homepage__container__vacancy__content__heading">
              Accurate <span>job matches</span>
            </div>
            <div className="homepage__container__vacancy__content__info">
              When you post your job on AIDApro, our candidates applying use
              selective filters to find you. For instance based on your required
              programming skills, experience level and so on. Saving you
              valuable time finding the right match. You can also search our
              database for candidates fitting your vacancy or job needs based on
              criteria that are important to you. Found the right matches? View
              and download their resumes or contact them via the secure AIDApro
              messaging and video options. Other communication options, like
              email and social media, are also provided.
            </div>
          </div>
          <Img
            loading="lazy"
            src={vacancy}
            alt="vacancy"
            className="homepage__container__vacancy__img"
          />
        </div>
        <div className="homepage__container__qualification">
          <Img
            loading="lazy"
            src={qualificationCheck}
            alt="qualificationCheck"
            className="homepage__container__qualification__left"
          />
          <div className="homepage__container__qualification__right">
            <div className="homepage__container__qualification__heading">
              You decide who's <span>qualified</span>
            </div>
            <div className="homepage__container__qualification__info">
              AIDApro has a large pool of qualified professionals. Find and
              contact candidates from our database. Use our platform features
              such as the secured chat or video calls.
            </div>
            <div style={{ display: "flex", alignItems: "center" }}>
              <Link
                to="/sign-up"
                className="header__nav__btn btn__primary"
                style={{
                  width: 180,
                  height: 45,
                  fontSize: 15,
                  marginRight: "1em",
                }}
                // onClick={() => setIsLoginOpen(true)}
                // to="/home-company/posting"
              >
                Post job or project
              </Link>

              <a
                href="#homepagecompanysearchsection"
                to="/"
                className="header__nav__btn btn__secondary"
                style={{ width: 150, height: 45, fontSize: 15 }}
              >
                Start searching
              </a>
            </div>
          </div>
        </div>
        <div className="homepage__container__tagline">
          <Img
            loading="lazy"
            src={filteredSkills}
            alt="filteredSkills"
            className="homepage__container__tagline__background"
          />
          <div className="homepage__container__tagline__content">
            <div className="homepage__container__tagline__heading">
              100+ <span>Skills</span> available
            </div>
            <div className="homepage__container__tagline__info">
              Connect proactively professionals with skills you are looking for.
              Or let professionals with the background and experience you are
              looking for find you instead.
            </div>
            <Link
              // href="#homepagecompanysearchsection"
              to="/sign-up"
              className="header__nav__btn btn__secondary"
              style={{ width: 180 }}
            >
              Find professionals
            </Link>
          </div>
        </div>
      </div>
      <Testmonials />
      <MobileAppAdvert />
    </>
  );
};

export default HomepageCompany;
