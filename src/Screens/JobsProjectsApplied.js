import React, { useState, useEffect, useContext } from "react";
import Pagination from "react-js-pagination";
import { useDispatch, useSelector } from "react-redux";
import { appliedActive, noData } from "Assets";
import { Head, DashboardHeading } from "Components";
import UserContext from "Context/UserContext";
import JobsProjectsAppliedList from "Components/JobsProjectsAppliedList";
import { getAllAppliedJobByJobseekerId } from "API/EmploymentAPI";
import { getAllAppliedProjectByFreelancerId } from "API/FreelancerApi";
import { getFreelancerById, getJobSekkerById } from "Redux/Actions/AppActions";
import Img from "react-cool-img";
import NoData from "Components/NoData";

const JobsProjectsApplied = () => {
  const isOn = window.localStorage.getItem("isOn");
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);
  const [totalRecords, setTotalRecords] = useState();
  const [interestedData, setInterestedData] = useState([]);

  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    if (isOn === "professional") dispatch(getJobSekkerById(user.JobSeekerId));
    else if (isOn === "freelancer")
      dispatch(getFreelancerById(user.FreelancerId));
  }, []);

  useEffect(() => {
    if (isOn == "professional") {
      getInterestedJobs();
    } else {
      getInterestedProjects();
    }
  }, [page, jobsekker, freelancer]);

  const getInterestedJobs = () => {
    if (Object.keys(jobsekker).length > 0) {
      getAllAppliedJobByJobseekerId(jobsekker.Id, page, limit)
        .then((data) => {
          if (data.data.success) {
            setInterestedData(data.data.result);
            setTotalRecords(data.data.totalRecords);
            setPage(data.data.page);
          }
        })
        .catch((err) => {
          // console.log("err", err);
        });
    }
  };

  const getInterestedProjects = () => {
    if (Object.keys(freelancer).length > 0) {
      getAllAppliedProjectByFreelancerId(freelancer.Id, page, limit)
        .then((data) => {
          if (data.data.success) {
            setInterestedData(data.data.result);
            setTotalRecords(data.data.totalRecords);
            setPage(data.data.page);
          }
        })
        .catch((err) => {
          // console.log("err", err);
        });
    }
  };

  return (
    <>
      <Head
        title="AIDApro | Jobs Project Applied"
        description="Jobs Project Applied"
      />
      <div className="posting__container">
        <DashboardHeading
          heading={isOn == "professional" ? "Jobs Applied" : "Projects Applied"}
          svg={appliedActive}
        />
        <div className="posting__container__table">
          <div className="posting__container__table__header">
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Date
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              {isOn == "freelancer" ? "Project " : "Job "}title
            </div>
            <div className="posting__container__table__header__entry animate__animated animate__fadeInDown">
              Status
            </div>
          </div>
          {interestedData.length > 0 ? (
            <>
              {interestedData.map((item, i) => (
                <JobsProjectsAppliedList key={i} item={item} />
              ))}
              <div className="posting__container__pagination">
                <Pagination
                  activePage={page}
                  itemsCountPerPage={limit}
                  totalItemsCount={totalRecords}
                  pageRangeDisplayed={10}
                  onChange={(e) => {
                    setPage(e);
                  }}
                />
              </div>
            </>
          ) : (
            <div className="no__data__container" style={{ height: 750 }}>
              <Img
                loading="lazy"
                src={noData}
                className="no__data__container__img"
              />
              {/* <div className="no__data__container__text">No applied yet</div> */}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default JobsProjectsApplied;
