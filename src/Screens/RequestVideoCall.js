import React, { useContext, useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { crossSvg } from "Assets";
import { isNullOrEmpty } from "Utils/TextUtils";
import { videoRequest, videoRequestUpdate } from "API/Video";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import moment from "moment";
import { CustomError, CustomSuccess } from "./Toasts";

export default function RequestVideoCall({
  setIsRequestVideoCallOpen,
  videoCallUserData,
  videoCallUpdate,
  setVideoCallUpdateSuccess,
}) {
  const user = useContext(UserContext);
  const { company } = useSelector((state) => state.company);

  const [isOnRequestNew, setIsOnRequestNew] = useState(true);
  const [isOnPreviousRequest, setIsOnPreviousRequest] = useState(false);
  const [description, setDescription] = useState("");
  let [startdate, setStartDate] = useState("");
  let [startTime, setStartTime] = useState("");
  let [endTime, setEndTime] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  let [timeZone, setTimeZone] = useState("");

  //validation
  const [startDateError, setStartDateError] = useState(false);
  const [startDateErrorMessage, setStartDateErrorMessage] = useState("");
  const [startTimeError, setStartTimeError] = useState(false);
  const [startTimeErrorMessage, setStartTimeErrorMessage] = useState("");
  const [endTimeError, setEndTimeError] = useState(false);
  const [endTimeErrorMessage, setEndTimeErrorMessage] = useState("");

  useEffect(() => {
    if (Object.keys(company).length > 0)
      setTimeZone((timeZone = company.companyAddressCountry.timezone_offset));

    setDescription(videoCallUpdate != null ? videoCallUpdate.Notes : "");
    setStartDate(
      videoCallUpdate != null
        ? moment(videoCallUpdate.StartDate).format("YYYY-MM-DD")
        : ""
    );
    setStartTime(
      videoCallUpdate != null ? timeUtcConvert(videoCallUpdate.StartTime) : ""
    );
    setEndTime(
      videoCallUpdate != null ? timeUtcConvert(videoCallUpdate.EndTime) : ""
    );
  }, []);

  const timeUtcConvert = (time) => {
    var today = new Date(time);
    if (timeZone.charAt(0) == "-") {
      let newValue = timeZone.substring(1);
      today.setHours(today.getHours() - newValue);
    } else {
      today.setHours(today.getHours() + parseInt(timeZone));
    }
    return moment(today).format("HH:mm:ss");
  };

  const handleStartDate = (event) => {
    if (isNullOrEmpty(event.target.value)) {
      setStartDate("");
      setStartDateError(true);
      setStartDateErrorMessage("Please select start date");
    } else {
      setStartDate(event.target.value);
      setStartDateError(false);
    }
  };

  const handleStartTime = (event) => {
    debugger;
    setStartTime((startTime = event.target.value));
    if (startTime >= moment(startdate).format("HH:mm")) {
      debugger;
      setStartTime((startTime = event.target.value));
      if (isNullOrEmpty(startTime)) {
        setStartTimeError(true);
        setStartTimeErrorMessage("Please select start time");
      } else if (startTime > endTime) {
        if (endTime != "") {
          setStartTimeError(true);
          setStartTimeErrorMessage("Start time can't be less than end time.");
        }
      }
      else {
        setStartTimeError(false);
        setStartTimeErrorMessage("");
      }
    } else {
      debugger;
      setStartTimeError(true);
      setStartTimeErrorMessage(
        "Meeting can't be scheduled prior to current time."
      );
    }
  };

  const handleEndTime = (event) => {
    setEndTime((endTime = event.target.value));
    if (isNullOrEmpty(endTime)) {
      setEndTimeError(true);
      setEndTimeErrorMessage("Please select start date");
    } else if (startTime > endTime) {
      setStartTimeError(true);
      setStartTimeErrorMessage("Start time can't be greater than end time.");
    } else {
      setStartTimeError(false);
      setEndTimeError(false);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isNullOrEmpty(startdate)) {
      setStartDateError(true);
      setStartDateErrorMessage("Please select start date");
    } else if (isNullOrEmpty(startTime)) {
      setStartTimeError(true);
      setStartTimeErrorMessage("Please select start time");
    } else if (isNullOrEmpty(endTime)) {
      setEndTimeError(true);
      setEndTimeErrorMessage("Please select end time");
    } else if (startTime > endTime) {
      setStartTimeError(true);
      setStartTimeErrorMessage("Start time can't be greater than end time.");
    }
    else {
      if (videoCallUpdate != null) updateVideoCallRequest();
      else requestVideoCall();
    }
  };

  const requestVideoCall = () => {
    var startD = new Date(startdate);
    var startDateUtc = new Date(
      startD.getTime() + startD.getTimezoneOffset() * 60000
    );
    var startT = new Date(new Date(startdate + " " + startTime));
    var startTimeUtc = new Date(
      startT.getTime() + startT.getTimezoneOffset() * 60000
    );
    var endT = new Date(new Date(startdate + " " + endTime));
    var endTimeUtc = new Date(
      endT.getTime() + endT.getTimezoneOffset() * 60000
    );

    let videoRequestObject = {
      Id: 0,
      CompanyProfileId: company.Id,
      JobSeekerId:
        videoCallUserData.JobSeekerId != 0
          ? videoCallUserData.JobSeekerId
          : null,
      FreelancerId:
        videoCallUserData.FreelancerId != 0
          ? videoCallUserData.FreelancerId
          : null,
      StartDate: moment(startDateUtc).format("YYYY-MM-DD"),
      StartTime: moment(startTimeUtc).format("HH:mm"),
      EndTime: moment(endTimeUtc).format("HH:mm"),
      Notes: description,
      CreatedById: user.Id,
    };
    videoCallRequestApi(videoRequestObject);
  };

  const updateVideoCallRequest = () => {
    var startD = new Date(startdate);
    var startDateUtc = new Date(
      startD.getTime() + startD.getTimezoneOffset() * 60000
    );
    var startT = new Date(new Date(startdate + " " + startTime));
    var startTimeUtc = new Date(
      startT.getTime() + startT.getTimezoneOffset() * 60000
    );
    var endT = new Date(new Date(startdate + " " + endTime));
    var endTimeUtc = new Date(
      endT.getTime() + endT.getTimezoneOffset() * 60000
    );

    let videoRequestObject = {
      Id: videoCallUpdate.Id,
      CompanyProfileId: videoCallUpdate.CompanyProfileId,
      JobSeekerId:
        videoCallUpdate.JobSeekerId != null
          ? videoCallUpdate.JobSeekerId
          : null,
      FreelancerId:
        videoCallUpdate.FreelancerId != null
          ? videoCallUpdate.FreelancerId
          : null,
      StartDate: moment(startDateUtc).format("YYYY-MM-DD"),
      StartTime: moment(startTimeUtc).format("HH:mm"),
      EndTime: moment(endTimeUtc).format("HH:mm"),
      Notes: description,
      CreatedById: user.Id,
    };
    videoCallRequestUpdateApi(videoRequestObject);
  };

  const videoCallRequestApi = (postObject) => {
    setIsLoading(true);
    videoRequest(postObject)
      .then((data) => {
        if (data.data.success) {
          //CustomSuccess("Video call requested successfully done.");
          setIsRequestVideoCallOpen(false);
        } else CustomError(data.data.message);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Can't able to make a video call schedule.");
      });
  };

  const videoCallRequestUpdateApi = (updateObject) => {
    setIsLoading(true);
    videoRequestUpdate(updateObject)
      .then((data) => {
        if (data.data.success) {
          //CustomSuccess("Video call requested successfully done.");
          setVideoCallUpdateSuccess(data.data.result);
          setIsRequestVideoCallOpen(false);
        } else CustomError(data.data.message);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Can't able to make a video call schedule.");
      });
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  const valueOfTimezone = () => {
    if (Object.keys(company).length > 0) {
      let timezone = company.companyAddressCountry.timezone_offset;
      let timeZoneGMTValue = null;
      if (timezone.charAt(0) == "-") {
        timeZoneGMTValue = `GMT${timezone}`;
      } else timeZoneGMTValue = `GMT+${timezone}`;
      return timeZoneGMTValue;
    }
  };

  return (
    <>
      <Head
        title="AIDApro | Request Video Call"
        description="Request Video Call"
      />
      <div className="pupup__container">
        <div
          // onSubmit={handleSubmit}
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsRequestVideoCallOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                id="facetime-button"
                xmlns="http://www.w3.org/2000/svg"
                width="65.849"
                height="42.249"
                viewBox="0 0 65.849 42.249"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                </defs>
                <path
                  id="Path_2762"
                  data-name="Path 2762"
                  d="M64.416,74.311a2.814,2.814,0,0,0-.919-.165,2.311,2.311,0,0,0-1.653.627L47.035,88.042V82.6a8.716,8.716,0,0,0-3.105-6.717,10.8,10.8,0,0,0-7.478-2.789H10.583A10.8,10.8,0,0,0,3.1,75.879,8.717,8.717,0,0,0,0,82.6v23.237a8.716,8.716,0,0,0,3.1,6.717,10.8,10.8,0,0,0,7.478,2.789H36.452a10.8,10.8,0,0,0,7.478-2.789,8.715,8.715,0,0,0,3.105-6.717v-5.48l14.809,13.3a2.311,2.311,0,0,0,1.653.627,2.819,2.819,0,0,0,.919-.165,1.982,1.982,0,0,0,1.433-1.947V76.259A1.983,1.983,0,0,0,64.416,74.311Z"
                  transform="translate(0 -73.09)"
                  fill="url(#linear-gradient)"
                />
              </svg>
              <div
                className="pupup__container__from__wrapper__header__content"
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  flex: 1,
                }}
              >
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Request Video Call
                </div>
                <div
                  className="pupup__container__from__wrapper__header__content__heading"
                  style={{
                    fontSize: 16,
                    fontWeight: "normal",
                    background: "#f6a938",
                    color: "white",
                    padding: ".5em 1em",
                    borderRadius: 10,
                  }}
                >
                  {valueOfTimezone()}
                </div>
              </div>
            </div>
            {/* <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                margin: "1em auto",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              <ContactTab
                defaultChecked={true}
                label="Request New"
                onClick={() => {
                  setIsOnRequestNew(true);
                  setIsOnPreviousRequest(false);
                }}
              />
              <ContactTab
                label="Previous Request"
                onClick={() => {
                  setIsOnRequestNew(false);
                  setIsOnPreviousRequest(true);
                }}
              />
            </div> */}
            {isOnRequestNew ? (
              <>
                <div className="pupup__container__from__wrapper__form">
                  <InputBox
                    variant="textarea"
                    placeholder="Agenda"
                    style={{
                      height: "fit-content",
                    }}
                    value={description}
                    onChange={(event) => {
                      setDescription(event);
                    }}
                  />

                  <div className="pupup__container__from__wrapper__form__row pupup__container__from__wrapper__form__row__special">
                    <InputBox
                      variant="date"
                      placeholder="Date"
                      error={startDateError}
                      errorMessage={startDateErrorMessage}
                      style={{
                        height: "fit-content",
                      }}
                      value={startdate}
                      minDate={moment(new Date()).format("YYYY-MM-DD")}
                      name="date"
                      onChange={(event) => handleStartDate(event)}
                    />
                    <InputBox
                      variant="time"
                      placeholder={startTime != "" ? startTime : "Start time"}
                      error={startTimeError}
                      errorMessage={startTimeErrorMessage}
                      style={{
                        height: "fit-content",
                      }}
                      value={startTime}
                      id="time"
                      name="time"
                      onChange={handleStartTime}
                    />
                    <InputBox
                      variant="time"
                      placeholder={endTime != "" ? endTime : "End time"}
                      error={endTimeError}
                      errorMessage={endTimeErrorMessage}
                      style={{
                        height: "fit-content",
                      }}
                      value={endTime}
                      name="time_1"
                      id="time_1"
                      onChange={handleEndTime}
                    />
                  </div>
                </div>
                <div className="pupup__container__from__wrapper__cta">
                  <button
                    type="submit"
                    className="header__nav__btn btn__secondary"
                    style={{
                      height: "50px",
                      width: "180px",
                    }}
                    onClick={handleSubmit}
                    disabled={isLoading ? true : false}
                    title="request video call"
                  >
                    {isLoading ? "Processing..." : "Request Video Call"}
                  </button>
                </div>
              </>
            ) : isOnPreviousRequest ? (
              <>
                <div className="pupup__container__from__wrapper__table">
                  <div className="pupup__container__from__wrapper__table__header">
                    <div className="pupup__container__from__wrapper__table__header__heading">
                      Date
                    </div>
                    <div className="pupup__container__from__wrapper__table__header__heading">
                      Time
                    </div>
                  </div>
                  <div className="pupup__container__from__wrapper__table__entry">
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      5 May 2021
                    </div>
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      09:00pm
                    </div>
                    <button
                      className="pupup__container__from__wrapper__table__entry__button"
                      type="button"
                      title="delete"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.57"
                        height="13.57"
                        viewBox="0 0 13.57 13.57"
                      >
                        <defs>
                          <linearGradient
                            id="linear-gradient"
                            x1="0.5"
                            x2="0.5"
                            y2="1"
                            gradientUnits="objectBoundingBox"
                          >
                            <stop offset="0" stopColor="#de3f3f" />
                            <stop offset="1" stopColor="#ff6161" />
                          </linearGradient>
                        </defs>
                        <path
                          id="Icon_metro-cross"
                          data-name="Icon metro-cross"
                          d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
                          transform="translate(-2.571 -1.928)"
                          fill="url(#linear-gradient)"
                        />
                      </svg>
                    </button>
                  </div>
                  <div className="pupup__container__from__wrapper__table__entry">
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      5 May 2021
                    </div>
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      09:00pm
                    </div>
                    <button
                      className="pupup__container__from__wrapper__table__entry__button"
                      type="button"
                      title="delete"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.57"
                        height="13.57"
                        viewBox="0 0 13.57 13.57"
                      >
                        <defs>
                          <linearGradient
                            id="linear-gradient"
                            x1="0.5"
                            x2="0.5"
                            y2="1"
                            gradientUnits="objectBoundingBox"
                          >
                            <stop offset="0" stopColor="#de3f3f" />
                            <stop offset="1" stopColor="#ff6161" />
                          </linearGradient>
                        </defs>
                        <path
                          id="Icon_metro-cross"
                          data-name="Icon metro-cross"
                          d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
                          transform="translate(-2.571 -1.928)"
                          fill="url(#linear-gradient)"
                        />
                      </svg>
                    </button>
                  </div>
                  <div className="pupup__container__from__wrapper__table__entry">
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      5 May 2021
                    </div>
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      09:00pm
                    </div>
                    <button
                      className="pupup__container__from__wrapper__table__entry__button"
                      type="button"
                      title="delete"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.57"
                        height="13.57"
                        viewBox="0 0 13.57 13.57"
                      >
                        <defs>
                          <linearGradient
                            id="linear-gradient"
                            x1="0.5"
                            x2="0.5"
                            y2="1"
                            gradientUnits="objectBoundingBox"
                          >
                            <stop offset="0" stopColor="#de3f3f" />
                            <stop offset="1" stopColor="#ff6161" />
                          </linearGradient>
                        </defs>
                        <path
                          id="Icon_metro-cross"
                          data-name="Icon metro-cross"
                          d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
                          transform="translate(-2.571 -1.928)"
                          fill="url(#linear-gradient)"
                        />
                      </svg>
                    </button>
                  </div>
                  <div className="pupup__container__from__wrapper__table__entry">
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      5 May 2021
                    </div>
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      09:00pm
                    </div>
                    <button
                      className="pupup__container__from__wrapper__table__entry__button"
                      type="button"
                      title="delete"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.57"
                        height="13.57"
                        viewBox="0 0 13.57 13.57"
                      >
                        <defs>
                          <linearGradient
                            id="linear-gradient"
                            x1="0.5"
                            x2="0.5"
                            y2="1"
                            gradientUnits="objectBoundingBox"
                          >
                            <stop offset="0" stopColor="#de3f3f" />
                            <stop offset="1" stopColor="#ff6161" />
                          </linearGradient>
                        </defs>
                        <path
                          id="Icon_metro-cross"
                          data-name="Icon metro-cross"
                          d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
                          transform="translate(-2.571 -1.928)"
                          fill="url(#linear-gradient)"
                        />
                      </svg>
                    </button>
                  </div>
                  <div className="pupup__container__from__wrapper__table__entry">
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      5 May 2021
                    </div>
                    <div className="pupup__container__from__wrapper__table__entry__text">
                      09:00pm
                    </div>
                    <button
                      className="pupup__container__from__wrapper__table__entry__button"
                      type="button"
                      title="delete"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.57"
                        height="13.57"
                        viewBox="0 0 13.57 13.57"
                      >
                        <defs>
                          <linearGradient
                            id="linear-gradient"
                            x1="0.5"
                            x2="0.5"
                            y2="1"
                            gradientUnits="objectBoundingBox"
                          >
                            <stop offset="0" stopColor="#de3f3f" />
                            <stop offset="1" stopColor="#ff6161" />
                          </linearGradient>
                        </defs>
                        <path
                          id="Icon_metro-cross"
                          data-name="Icon metro-cross"
                          d="M16.017,12.83h0L11.9,8.713,16.017,4.6h0a.425.425,0,0,0,0-.6L14.072,2.052a.425.425,0,0,0-.6,0h0L9.356,6.169,5.239,2.052h0a.425.425,0,0,0-.6,0L2.695,4a.425.425,0,0,0,0,.6h0L6.811,8.713,2.695,12.83h0a.425.425,0,0,0,0,.6l1.945,1.945a.425.425,0,0,0,.6,0h0l4.117-4.117,4.117,4.117h0a.425.425,0,0,0,.6,0l1.945-1.945a.425.425,0,0,0,0-.6Z"
                          transform="translate(-2.571 -1.928)"
                          fill="url(#linear-gradient)"
                        />
                      </svg>
                    </button>
                  </div>
                </div>
              </>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}
