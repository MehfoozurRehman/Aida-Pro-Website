import React, { useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { useSelector } from "react-redux";
import { freelancerPersonalDetailsUpdate } from "API/FreelancerApi";
import { jobSekkerPersonalDetailsUpdate } from "API/EmploymentAPI";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { isNullOrEmpty } from "Utils/TextUtils";
import { crossSvg } from "Assets";

export default function AddSocials({ setIsAddSocialsOpen }) {
  let userData = null;
  const { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    userData = freelancer;
  } else {
    userData = jobsekker;
  }

  const [facebookLink, setFacebookLink] = useState("");
  const [twitterLink, setTwitterLink] = useState("");
  const [linkedInLink, setLinkedInLink] = useState("");

  //#region  validation
  let [facebookLinkError, setFacebookLinkError] = useState(false);
  let [facebookLinkErrorMessage, setFacebookLinkErrorMessage] = useState("");
  let [twitterLinkError, setTwitterLinkError] = useState(false);
  let [twitterLinkErrorMessage, setTwitterLinkErrorMessage] = useState("");
  let [linkedinLinkError, setLinkedinLinkError] = useState(false);
  let [linkedinLinkErrorMessage, setLinkedinLinkErrorMessage] = useState("");
  //#endregion

  useEffect(() => {
    setFacebookLink(userData.facebookProfile);
    setTwitterLink(userData.googleProfile);
    setLinkedInLink(userData.linkedInProfile);
  }, []);

  const onChangeFacebookLink = (enteredValue) => {
    let isEmpty = isNullOrEmpty(enteredValue);
    setFacebookLinkError((facebookLinkError = isEmpty));
    setFacebookLinkErrorMessage(isEmpty ? "Please enter facebook link" : "");
    setFacebookLink(enteredValue);
    return isEmpty;
  };
  const onChangeTwitterLink = (enteredValue) => {
    let isEmpty = isNullOrEmpty(enteredValue);
    setTwitterLinkError((twitterLinkError = isEmpty));
    setTwitterLinkErrorMessage(isEmpty ? "Please enter twitter link" : "");
    setTwitterLink(enteredValue);
    return isEmpty;
  };
  const onChangeLinkedinLink = (enteredValue) => {
    let isEmpty = isNullOrEmpty(enteredValue);
    setLinkedinLinkError((linkedinLinkError = isEmpty));
    setLinkedinLinkErrorMessage(isEmpty ? "Please enter linkedin link" : "");
    setLinkedInLink(enteredValue);
    return isEmpty;
  };

  const isViewValid = () => {
    if (
      onChangeFacebookLink(facebookLink) ||
      onChangeTwitterLink(twitterLink) ||
      onChangeLinkedinLink(linkedInLink)
    )
      return false;
    else return true;
  };

  function handleSubmit() {
    if (
      facebookLink != undefined ||
      twitterLink != undefined ||
      linkedInLink != undefined
    ) {
      if (jobsekker.Id != undefined) {
        addSocialJobSeeker();
      } else {
        addSocialFreelancer();
      }
    } else setIsAddSocialsOpen(false);
  }

  const addSocialFreelancer = () => {
    let langData = [];
    if (userData.language != null && userData.language.length > 0) {
      userData.language.map((e) => {
        langData.push({ LanguageLookupDetailId: e.value, Id: e.Id });
      });
    }

    let data = {
      Age: userData.age,
      FirstName: userData.firstName,
      ProfilePicture: userData.personalDetailsFreelancerImage,
      Id: userData.Id,
      LastName: userData.lastName,
      FreelancerLanguages: langData,
      Latitude: userData.lat,
      Longitude: userData.lng,
      FacebookProfile: facebookLink,
      LinkedInProfile: linkedInLink,
      GoogleProfile: twitterLink,
      FreelancerAddresses: [
        {
          ZipCode: userData.primaryZipCode,
          PhoneNo: userData.phoneNo,
          Id: freelancer.FreelancerAddressesId,
          FreelancerId: userData.Id,
          Email: userData.emailAddress,
          CreatedById: userData.Id,
          Latitude: userData.lat,
          Longitude: userData.lng,
          Zoom: 1,
          AddressDetail: userData.primaryAddress,
        },
      ],
    };

    freelancerPersonalDetailsUpdate(data)
      .then(({ data }) => {
        //CustomSuccess("Social links update successfully");
        setIsAddSocialsOpen(false);
        window.location.reload();
      })
      .catch((err) => {
        CustomError("Failed to update social links");
      });
  };

  const addSocialJobSeeker = () => {
    let langData = [];
    if (userData.language != null && userData.language.length > 0) {
      userData.language.map((e) => {
        langData.push({ LanguageLookupDetailId: e.value, Id: e.Id });
      });
    }

    let data = {
      Age: userData.age,
      FirstName: userData.firstName,
      ProfilePicture: userData.personalDetailsJobSeekerImage,
      Id: userData.Id,
      LastName: userData.lastName,
      JobSeekerLanguages: langData,
      Latitude: userData.lat,
      Longitude: userData.lng,
      FacebookProfile: facebookLink,
      LinkedInProfile: linkedInLink,
      GoogleProfile: twitterLink,
      JobSeekerAddresses: [
        {
          ZipCode: userData.primaryZipCode,
          PhoneNo: userData.phoneNo,
          Id: jobsekker.JobSeekerAddressesId,
          JobSeekerId: userData.Id,
          Email: userData.emailAddress,
          CreatedById: userData.Id,
          Latitude: userData.lat,
          Longitude: userData.lng,
          Zoom: 1,
          AddressDetail: userData.primaryAddress,
        },
      ],
    };

    jobSekkerPersonalDetailsUpdate(data)
      .then(({ data }) => {
        //CustomSuccess("Social links update successfully");
        setIsAddSocialsOpen(false);
        window.location.reload();
      })
      .catch((err) => {
        CustomError("Failed to update social links");
      });
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Add Socails" description="Add Socails" />
      <div className="pupup__container">
        <div
          // onSubmit={handleSubmit}
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsAddSocialsOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="41.749"
                height="41.749"
                viewBox="0 0 41.749 41.749"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-2"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#007e77" />
                  </linearGradient>
                </defs>
                <g id="tools" transform="translate(-3.323 -3.323)">
                  <path
                    id="Path_21160"
                    data-name="Path 21160"
                    d="M44.416,10.182l-5.328,5.328L33.964,14.48,32.935,9.356l5.328-5.328a8.7,8.7,0,0,0-10.894,9.4,2.932,2.932,0,0,1-.877,2.377L17.7,24.6l-1.892,1.892a2.932,2.932,0,0,1-2.377.877,8.7,8.7,0,0,0-9.4,10.894l5.328-5.328,5.125,1.029,1.029,5.125-5.328,5.328a8.7,8.7,0,0,0,10.894-9.4,2.932,2.932,0,0,1,.877-2.377L32.637,21.953a2.932,2.932,0,0,1,2.377-.877,8.7,8.7,0,0,0,9.4-10.894Z"
                    transform="translate(-0.113 -0.113)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_21161"
                    data-name="Path 21161"
                    d="M7.732,30.586a8.689,8.689,0,0,1,7.147-2.493,2.932,2.932,0,0,0,2.377-.877l1.892-1.892,8.792-8.792a2.932,2.932,0,0,0,.877-2.377,8.685,8.685,0,0,1,8.994-9.676l.45-.45a8.7,8.7,0,0,0-10.894,9.4,2.932,2.932,0,0,1-.877,2.377L17.7,24.6l-1.892,1.892a2.932,2.932,0,0,1-2.377.877,8.7,8.7,0,0,0-9.4,10.894L5.2,37.087a8.647,8.647,0,0,1,2.529-6.5Z"
                    transform="translate(-0.113 -0.113)"
                    fill="url(#linear-gradient-2)"
                  />
                  <path
                    id="Path_21162"
                    data-name="Path 21162"
                    d="M55.1,58.263l4.219,1.055L58.263,55.1A4.335,4.335,0,0,0,55.1,58.263Z"
                    transform="translate(-14.247 -14.247)"
                    fill="#58595b"
                  />
                  <path
                    id="Path_21163"
                    data-name="Path 21163"
                    d="M52.169,52.17l-2.562-.512v4.1l3.982,1a4.335,4.335,0,0,1,3.163-3.163l-1-3.979h-4.1Z"
                    transform="translate(-12.736 -12.737)"
                    fill="#d8d8d8"
                  />
                  <path
                    id="Path_21164"
                    data-name="Path 21164"
                    d="M0,0H0Z"
                    transform="translate(11.769 13.822) rotate(-45)"
                    fill="#d1e7f8"
                  />
                  <path
                    id="Path_21165"
                    data-name="Path 21165"
                    d="M17.134,10.983,14.058,7.908l-6.15,6.15,3.075,3.075,2.05-2.05,2.051-2.051Z"
                    transform="translate(-1.262 -1.262)"
                    fill="#303043"
                  />
                  <path
                    id="Path_21166"
                    data-name="Path 21166"
                    d="M10.747,4.6a4.349,4.349,0,1,0-6.15,6.15l2.05,2.05,6.15-6.15Z"
                    fill="#d1d3d4"
                  />
                  <path
                    id="Path_21167"
                    data-name="Path 21167"
                    d="M36.318,27.056l-1.025-1.025L38.4,22.919a.725.725,0,0,1,1.025,1.025Z"
                    transform="translate(-8.797 -5.334)"
                    fill="#fff"
                  />
                  <path
                    id="Path_21168"
                    data-name="Path 21168"
                    d="M22.919,39.43a.725.725,0,0,1,0-1.025l3.112-3.112,1.025,1.025L23.944,39.43A.725.725,0,0,1,22.919,39.43Z"
                    transform="translate(-5.334 -8.797)"
                    fill="#fff"
                  />
                  <path
                    id="Path_21169"
                    data-name="Path 21169"
                    d="M14.2,17.809,12.15,19.861,20.391,28.1l6.147,6.147L39.3,47v-4.1Z"
                    transform="translate(-2.429 -3.986)"
                    fill="#ff7f23"
                  />
                  <path
                    id="Path_21170"
                    data-name="Path 21170"
                    d="M47,39.3,34.242,26.538,28.1,20.391,19.861,12.15,17.809,14.2l25.1,25.1Z"
                    transform="translate(-3.986 -2.429)"
                    fill="#f6a039"
                  />
                  <path
                    id="Path_21171"
                    data-name="Path 21171"
                    d="M40.079,42.129l2.562.512-.512-2.562-25.1-25.1-2.05,2.05Z"
                    transform="translate(-3.208 -3.208)"
                    fill="#f58e3b"
                  />
                </g>
              </svg>
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Add socials
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  Add your socials here
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Facebook"
                  style={{
                    height: "fit-content",
                  }}
                  onChange={(event) =>
                    onChangeFacebookLink(event.currentTarget.value)
                  }
                  name="facebooklink"
                  value={facebookLink}
                  error={facebookLinkError}
                  errorMessage={facebookLinkErrorMessage}
                />
                <InputBox
                  placeholder="Twitter"
                  style={{
                    height: "fit-content",
                  }}
                  onChange={(event) =>
                    onChangeTwitterLink(event.currentTarget.value)
                  }
                  name="twitterlink"
                  value={twitterLink}
                  error={twitterLinkError}
                  errorMessage={twitterLinkErrorMessage}
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="LinkedIn"
                  style={{
                    height: "fit-content",
                  }}
                  onChange={(event) =>
                    onChangeLinkedinLink(event.currentTarget.value)
                  }
                  name="linkedinlink"
                  value={linkedInLink}
                  error={linkedinLinkError}
                  errorMessage={linkedinLinkErrorMessage}
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                onClick={handleSubmit}
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                title="add socials"
              >
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
