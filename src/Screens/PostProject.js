import React, { useContext, useState, useEffect } from "react";
import UserContext from "Context/UserContext";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { getAllSkills, getLookUpByPrefix, getAllIndustry } from "../API/Api";
import { Head, DashboardHeading, InputBox } from "Components";
import moment from "moment";
import { isNullOrEmpty, isNullOrEmptyArray } from "Utils/TextUtils";
import { postProjectSvg } from "Assets";
import {
  projectPost,
  projectUpdate,
  saveProjectDraftPost,
} from "../API/Projects";

const PostProject = ({ setIsJobPreviewOpen, previewData, location }) => {
  const isUpdateFromObject = location.state.projectData;
  let projectSkillslocal = [];
  let projectBudgetTypeLocal = [];
  let projectStatusLookupDetailIdLocal = null;
  let industryTypeLocal = [];

  if (isUpdateFromObject) {
    let projectSkillList = [];
    isUpdateFromObject.ProjectSkills.map((projSk, projSkIndex) =>
      projectSkillList.push({ label: projSk.Title, value: projSk.SkillId })
    );
    projectSkillslocal = projectSkillList;

    if (isUpdateFromObject.Industry.Title != null) {
      let object = {
        label: isUpdateFromObject.Industry.Title,
        value: isUpdateFromObject.Industry.Id,
      };
      industryTypeLocal.push(object);
    }

    if (isUpdateFromObject.BudgetTypeLookupDetail) {
      projectBudgetTypeLocal = {
        label: isUpdateFromObject.BudgetTypeLookupDetail.Title,
        value: isUpdateFromObject.BudgetTypeLookupDetail.Id,
      };
    }

    if (isUpdateFromObject.StatusTypeLookupDetail) {
      projectStatusLookupDetailIdLocal = {
        label: isUpdateFromObject.StatusTypeLookupDetail.Title,
        value: isUpdateFromObject.StatusTypeLookupDetail.Id,
      };
    }
  }

  const [isUpdate, setIsUpdate] = useState(isUpdateFromObject ? true : false);
  const [projectId, setProjectId] = useState(
    isUpdateFromObject ? isUpdateFromObject.Id : 0
  );

  const [projectTitle, setProjectTitle] = useState(
    isUpdateFromObject ? isUpdateFromObject.Title : ""
  );
  const [projectLocation, setProjectLocation] = useState(
    isUpdateFromObject ? isUpdateFromObject.Location : ""
  );
  const [projectLat, setProjectLat] = useState(
    isUpdateFromObject ? isUpdateFromObject.Latitude : ""
  );
  const [projectLng, setProjectLng] = useState(
    isUpdateFromObject ? isUpdateFromObject.Longitude : ""
  );
  const [projectStartDate, setProjectStartDate] = useState(
    isUpdateFromObject
      ? moment(isUpdateFromObject.StartDate).format("YYYY-MM-DD")
      : ""
  );
  const [projectEndDate, setProjectEndDate] = useState(
    isUpdateFromObject
      ? moment(isUpdateFromObject.Deadline).format("YYYY-MM-DD")
      : ""
  );
  const [budgetType, setBudgetType] = useState(projectBudgetTypeLocal);

  const [budgetMin, setBudgetMin] = useState(
    isUpdateFromObject ? isUpdateFromObject.BudgetFrom : ""
  );
  const [budgetMax, setBudgetMax] = useState(
    isUpdateFromObject ? isUpdateFromObject.BudgetTo : ""
  );
  const user = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [dateValue, dateOnValueChange] = useState(new Date());
  const [showComponent, setShowComponent] = useState(false);
  const [budgetTypeDropDownData, setBudgetTypeDropDownData] = useState([]);
  const [skillsDropDownData, setSkillsDropDownData] = useState([]);
  const [showFixedComponent, setShowFixedComponent] = useState(false);
  const [projectSkills, setProjectSkills] = useState(projectSkillslocal);
  const [industryType, setIndustryType] = useState([]);
  const [selectedIndustryType, setSelectedIndustryType] =
    useState(industryTypeLocal);
  const [projectDescription, setProjectDescription] = useState(
    isUpdateFromObject ? isUpdateFromObject.Description : ""
  );

  //validations
  var [projectTitleError, setProjectTitleError] = useState(false);
  const [projectTitleErrorMessage, setProjectTitleErrorMessage] = useState("");
  var [startDateError, setStartDateError] = useState(false);
  const [startDateErrorMessage, setStartDateErrorMessage] = useState("");
  var [endDateError, setEndDateError] = useState(false);
  const [endDateErrorMessage, setEndDateErrorMessage] = useState("");
  var [locationError, setLocationError] = useState(false);
  const [locationErrorMessage, setLocationErrorMessage] = useState("");
  var [skillsError, setSkillsError] = useState(false);
  const [skillsErrorMessage, setSkillsErrorMessage] = useState("");
  var [industryError, setIndustryError] = useState(false);
  const [industryErrorMessage, setIndustryErrorMessage] = useState("");
  var [budgetTypeError, setBudgetTypeError] = useState(false);
  const [budgetTypeErrorMessage, setBudgetTypeErrorMessage] = useState("");
  var [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");

  var [minBudgetError, setMinBudgetError] = useState(false);
  const [minBudgetErrorMessage, setMinBudgetErrorMessage] = useState("");
  var [maxBudgetError, setMaxBudgetError] = useState(false);
  const [maxBudgetErrorMessage, setMaxBudgetErrorMessage] = useState("");

  const handleChangelocation = (data, isInput) => {
    if (isNullOrEmpty(data.geometry)) {
      setLocationErrorMessageAndVisibility("Please enter your location");
      setProjectLat(null);
      setProjectLng(null);
    } else {
      setLocationErrorMessageAndVisibility("");
      if (isInput) {
        setProjectLocation(data.currentTarget.value);
      } else {
        setProjectLocation(data.formatted_address);
        setProjectLat(data.geometry.location.lat());
        setProjectLng(data.geometry.location.lng());
      }
    }
    setProjectLocation(data.formatted_address);
  };

  const setLocationErrorMessageAndVisibility = (text) => {
    setLocationError((locationError = !isNullOrEmpty(text)));
    setLocationErrorMessage(text);
  };

  const setProjectTitleErrorMessageAndVisibility = (text) => {
    setProjectTitleError((projectTitleError = !isNullOrEmpty(text)));
    setProjectTitleErrorMessage(text);
  };

  const setIndustryErrorMessageAndVisibility = (text) => {
    setIndustryError((industryError = !isNullOrEmpty(text)));
    setIndustryErrorMessage(text);
  };
  const setSkillsErrorMessageAndVisibility = (text) => {
    setSkillsError((skillsError = !isNullOrEmpty(text)));
    setSkillsErrorMessage(text);
  };
  const setStartDateErrorMessageAndVisibility = (text) => {
    setStartDateError((startDateError = !isNullOrEmpty(text)));
    setStartDateErrorMessage(text);
  };
  const setEndDateErrorMessageAndVisibility = (text) => {
    setEndDateError((endDateError = !isNullOrEmpty(text)));
    setEndDateErrorMessage(text);
  };
  const setDescriptionErrorMessageAndVisibility = (text) => {
    setDescriptionError((descriptionError = !isNullOrEmpty(text)));
    setDescriptionErrorMessage(text);
  };

  const setMinBudgetErrorMessageAndVisibility = (text) => {
    setMinBudgetError((minBudgetError = !isNullOrEmpty(text)));
    setMinBudgetErrorMessage(text);
  };
  const setMaxBudgetErrorMessageAndVisibility = (text) => {
    setMaxBudgetError((maxBudgetError = !isNullOrEmpty(text)));
    setMaxBudgetErrorMessage(text);
  };

  const setBudgetTypeErrorMessageAndVisibility = (text) => {
    setBudgetTypeError((budgetTypeError = !isNullOrEmpty(text)));
    setBudgetTypeErrorMessage(text);
  };

  const save = () => {
    if (isViewValid()) {
      setIsLoading(true);
      if (projectId === 0) {
        saveProject(getRequestData());
      } else {
        updateProject(getRequestData());
      }
    }
  };
  const isViewValid = () => {
    if (isNullOrEmpty(projectTitle))
      setProjectTitleErrorMessageAndVisibility("Please enter project title");
    else if (isNullOrEmpty(projectStartDate))
      setStartDateErrorMessageAndVisibility("Please select start date");
    else if (isNullOrEmptyArray(projectEndDate))
      setEndDateErrorMessageAndVisibility("Please select end date");
    else if (isNullOrEmpty(projectLat) && isNullOrEmpty(projectLng))
      setLocationErrorMessageAndVisibility("Please enter your location");
    else if (isNullOrEmptyArray(selectedIndustryType))
      setIndustryErrorMessageAndVisibility("Please select your industry");
    else if (isNullOrEmptyArray(projectSkills))
      setSkillsErrorMessageAndVisibility("Please select at least on skill");
    else if (isNullOrEmptyArray(budgetType))
      setBudgetTypeErrorMessageAndVisibility("Please select budget type");
    else if (budgetType.label === "Fixed" && isNullOrEmpty(budgetMin))
      setMinBudgetErrorMessageAndVisibility("Please enter budget");
    else if (
      budgetType.label === "Range" &&
      (isNullOrEmpty(budgetMin) || isNullOrEmpty(budgetMax))
    ) {
      if (isNullOrEmpty(budgetMin))
        setMinBudgetErrorMessageAndVisibility("Enter range");
      else if (isNullOrEmpty(budgetMax))
        setMaxBudgetErrorMessageAndVisibility("Enter range");
      else if (parseInt(budgetMin) > parseInt(budgetMax))
        setMaxBudgetErrorMessageAndVisibility("Min can't > max");
    } else if (isNullOrEmptyArray(projectDescription))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else return true;
    return false;
  };
  const getRequestData = () => {
    let projectSkillTempArr = [];

    projectSkills.map((e) => {
      projectSkillTempArr.push({ SkillId: e.value });
    });

    let data = {
      Id: projectId,
      Title: projectTitle,
      Location: projectLocation,
      Latitude: projectLat,
      Longitude: projectLng,
      ProjectSkills: projectSkillTempArr,
      Description: projectDescription,
      StartDate: projectStartDate,
      Deadline: projectEndDate,
      BudgetTypeLookupDetailId: budgetType.value,
      BudgetFrom: budgetMin,
      BudgetTo: budgetMax,
      StatusTypeLookupDetailId: projectStatusLookupDetailIdLocal
        ? projectStatusLookupDetailIdLocal.value
        : projectStatusLookupDetailIdLocal,
      IndustryId: selectedIndustryType.value,
      CompanyProfileId: user.CompanyId,
    };
    return data;
  };

  const saveProject = (data) => {
    projectPost({ data })
      .then(({ data }) => {
        setIsLoading(false);
        //CustomSuccess("Project Posted Successfully");

        setTimeout(() => {
          window.location.href = "/home-company/posting";
        }, 1000);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Failed to Post a Project");
      });
  };
  const saveDraftProject = () => {
    if (isViewValid()) {
      setIsLoading(true);
      saveProjectDraftPost(getRequestData())
        .then(({ data }) => {
          setIsLoading(false);
          //CustomSuccess("Project saved as draft successfully");

          setTimeout(() => {
            window.location.href = "/home-company/posting";
          }, 1000);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Project failed to save as draft");
        });
    }
  };

  const updateProject = (data) => {
    projectUpdate({ data })
      .then(({ data }) => {
        setIsLoading(false);
        //CustomSuccess("Project Updated Successfully");
        setTimeout(() => {
          window.location.href = "/home-company/posting";
        }, 1000);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Failed to Update a Project");
      });
  };

  useEffect(() => {
    getLookUpByPrefix("BUDTY")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setBudgetTypeDropDownData(formattedData);
      })
      .catch((err) => {
      });

    getAllSkills()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setSkillsDropDownData(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getAllIndustry()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });

        setIndustryType(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    setIsLoading(false);
  }, []);

  const handleBudgetType = (data) => {
    if (isNullOrEmptyArray(data))
      setBudgetTypeErrorMessageAndVisibility("Please select budget type");
    else setBudgetTypeErrorMessageAndVisibility("");
    setBudgetType({ label: data.label, value: data.value });

    setMinBudgetErrorMessageAndVisibility("");
    setMaxBudgetErrorMessageAndVisibility("");
    setBudgetMin("");
  };

  const handleSkills = (data) => {
    if (isNullOrEmptyArray(data))
      setSkillsErrorMessageAndVisibility("Please select at least one skills");
    else setSkillsErrorMessageAndVisibility("");
    setProjectSkills(data);
  };

  const handleChangeValues = (event) => {
    if (event.currentTarget.name === "title") {
      if (isNullOrEmpty(event.currentTarget.value))
        setProjectTitleErrorMessageAndVisibility("Please enter project title");
      else setProjectTitleErrorMessageAndVisibility("");
      setProjectTitle(event.currentTarget.value);
    } else if (event.currentTarget.name === "minbudget") {
      if (budgetType.label === "Range") {
        if (isNullOrEmpty(event.currentTarget.value))
          setMinBudgetErrorMessageAndVisibility("Enter range");
        else setMinBudgetErrorMessageAndVisibility("");
      } else {
        if (isNullOrEmpty(event.currentTarget.value))
          setMinBudgetErrorMessageAndVisibility("Please enter budget");
        else setMinBudgetErrorMessageAndVisibility("");
      }
      setBudgetMin(event.currentTarget.value);
    } else if (event.currentTarget.name === "maxbudget") {
      if (isNullOrEmpty(event.currentTarget.value))
        setMaxBudgetErrorMessageAndVisibility("Enter range");
      else if (parseInt(budgetMin) > parseInt(event.currentTarget.value))
        setMaxBudgetErrorMessageAndVisibility("Min can't > max");
      else setMaxBudgetErrorMessageAndVisibility("");
      setBudgetMax(event.currentTarget.value);
    }
  };

  const handleChangeStartDateValues = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setStartDateErrorMessageAndVisibility("Please enter start date");
    else setStartDateErrorMessageAndVisibility("");
    setProjectStartDate(event.currentTarget.value);
  };

  const handleChangeEndDateValues = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setEndDateErrorMessageAndVisibility("Please enter end date");
    else setEndDateErrorMessageAndVisibility("");
    setProjectEndDate(event.currentTarget.value);
  };

  const handleChangeIndustryType = (e) => {
    if (isNullOrEmptyArray(e))
      setIndustryErrorMessageAndVisibility("Please select your industry");
    else setIndustryErrorMessageAndVisibility("");
    setSelectedIndustryType({ label: e.label, value: e.value });
  };

  const showPreviewData = () => {
    let previewDTO = {
      type: "project",
      title: projectTitle,
      startDate: projectStartDate,
      endDate: projectEndDate,
      location: projectLocation,
      skill: projectSkills,
      jobType: null,
      salaryType: budgetType.label,
      salaryMin: budgetMin,
      salaryMax: budgetMax,
      description: projectDescription,
      requirements: null,
      industry: selectedIndustryType
    };
    previewData(previewDTO);
    setIsJobPreviewOpen(true);
  };

  const handleChangeDescriptionValues = (data) => {
    if (isNullOrEmptyArray(data))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else setDescriptionErrorMessageAndVisibility("");
    setProjectDescription(data);
  };

  return (
    <>
      <Head title="AIDApro | Post a project" description="Post a project" />
      <div className="post__job__container">
        <DashboardHeading heading="Post a project" svg={postProjectSvg} />
        <div className="post__job__container__input__container">
          <div className="post__job__container__input__container__wrapper__top">
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="simple"
                placeholder="Project title"
                name="title"
                error={projectTitleError}
                errorMessage={projectTitleErrorMessage}
                value={projectTitle}
                svg={
                  <svg
                    id="keyboard"
                    xmlns="http://www.w3.org/2000/svg"
                    width="15.431"
                    height="9.428"
                    viewBox="0 0 15.431 9.428"
                  >
                    <g id="Group_161" data-name="Group 161">
                      <path
                        id="Path_276"
                        data-name="Path 276"
                        d="M26.431,114.888v-7.747a.869.869,0,0,0-.891-.841H11.891a.869.869,0,0,0-.891.841v7.747a.869.869,0,0,0,.891.841H25.539A.867.867,0,0,0,26.431,114.888Zm-14.659,0v-7.747c0-.028.047-.069.12-.069H25.539c.076,0,.12.044.12.069v7.747c0,.028-.047.069-.12.069H11.891C11.819,114.957,11.772,114.916,11.772,114.888Z"
                        transform="translate(-11 -106.3)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_275"
                        data-name="Ellipse 275"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(2.299 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_276"
                        data-name="Ellipse 276"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(4.273 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_277"
                        data-name="Ellipse 277"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(6.245 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_278"
                        data-name="Ellipse 278"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(8.219 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_279"
                        data-name="Ellipse 279"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(10.194 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_280"
                        data-name="Ellipse 280"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(12.168 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_281"
                        data-name="Ellipse 281"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(3.287 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_282"
                        data-name="Ellipse 282"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(5.259 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_283"
                        data-name="Ellipse 283"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(7.233 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_284"
                        data-name="Ellipse 284"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(9.205 4.232)"
                        fill="#374957"
                      />
                      <path
                        id="Path_277"
                        data-name="Path 277"
                        d="M366.482,240.7h0a.482.482,0,0,0,0,.964h0a.482.482,0,1,0,0-.964Z"
                        transform="translate(-354.82 -236.468)"
                        fill="#374957"
                      />
                      <path
                        id="Path_278"
                        data-name="Path 278"
                        d="M141.671,308.7h-6.884a.387.387,0,1,0,0,.775h6.884a.387.387,0,1,0,0-.775Z"
                        transform="translate(-130.514 -302.326)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
                onChange={(e) => handleChangeValues(e)}
              />
              <InputBox
                variant="date"
                placeholder="Start Date"
                name="startDate"
                error={startDateError}
                errorMessage={startDateErrorMessage}
                style={{ minWidth: "32% " }}
                value={projectStartDate}
                minDate={moment(new Date()).format("YYYY-MM-DD")}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12.019"
                    height="12.011"
                    viewBox="0 0 12.019 12.011"
                  >
                    <g id="deadline" transform="translate(0.1 -0.076)">
                      <g
                        id="Group_1396"
                        data-name="Group 1396"
                        transform="translate(7.626 6.295)"
                      >
                        <g id="Group_1395" data-name="Group 1395">
                          <path
                            id="Path_21141"
                            data-name="Path 21141"
                            d="M331.053,266.95l-.006-1.7-.691,0,.007,1.987,1.753,1.724.485-.493Z"
                            transform="translate(-330.356 -265.251)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_1398"
                        data-name="Group 1398"
                        transform="translate(0 0.176)"
                      >
                        <g
                          id="Group_1397"
                          data-name="Group 1397"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_21142"
                            data-name="Path 21142"
                            d="M11.8,7.718v-6.5H10.368V.176H9.677V1.213H8.986V.176H8.295V1.213H7.6V.176H6.912V1.213H6.221V.176H5.53V1.213H4.839V.176H4.147V1.213H3.456V.176H2.765V1.213H2.074V.176H1.382V1.213H0v9.78H5.39A3.848,3.848,0,0,0,11.819,8.14,3.92,3.92,0,0,0,11.8,7.718ZM.691,10.3h0V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9H5.53v.806h.691V1.9h.691v.806H7.6V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.737v4a3.894,3.894,0,0,0-.413-.487A3.85,3.85,0,0,0,5.753,5H2.079v.691H5.007a3.834,3.834,0,0,0-.458.691H2.079v.691h2.2a3.839,3.839,0,0,0-.132.689H2.079v.691H4.137A3.81,3.81,0,0,0,4.788,10.3H.691Zm9.512.07a3.132,3.132,0,0,1-1.886.906v-.629H7.626v.629a3.157,3.157,0,0,1-2.8-2.828h.561V7.758H4.838A3.158,3.158,0,0,1,7.626,5v.557h.691V5a3.158,3.158,0,0,1,2.788,2.756h-.542v.691h.55A3.133,3.133,0,0,1,10.2,10.372Z"
                            transform="translate(0 -0.176)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                }
                onChange={handleChangeStartDateValues}
              />
              <InputBox
                variant="date"
                name="deadLine"
                placeholder="Deadline"
                error={endDateError}
                errorMessage={endDateErrorMessage}
                style={{ minWidth: "32% " }}
                minDate={
                  projectEndDate != ""
                    ? projectEndDate
                    : moment(new Date()).format("YYYY-MM-DD")
                }
                value={projectEndDate}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12.019"
                    height="12.011"
                    viewBox="0 0 12.019 12.011"
                  >
                    <g id="deadline" transform="translate(0.1 -0.076)">
                      <g
                        id="Group_1396"
                        data-name="Group 1396"
                        transform="translate(7.626 6.295)"
                      >
                        <g id="Group_1395" data-name="Group 1395">
                          <path
                            id="Path_21141"
                            data-name="Path 21141"
                            d="M331.053,266.95l-.006-1.7-.691,0,.007,1.987,1.753,1.724.485-.493Z"
                            transform="translate(-330.356 -265.251)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_1398"
                        data-name="Group 1398"
                        transform="translate(0 0.176)"
                      >
                        <g
                          id="Group_1397"
                          data-name="Group 1397"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_21142"
                            data-name="Path 21142"
                            d="M11.8,7.718v-6.5H10.368V.176H9.677V1.213H8.986V.176H8.295V1.213H7.6V.176H6.912V1.213H6.221V.176H5.53V1.213H4.839V.176H4.147V1.213H3.456V.176H2.765V1.213H2.074V.176H1.382V1.213H0v9.78H5.39A3.848,3.848,0,0,0,11.819,8.14,3.92,3.92,0,0,0,11.8,7.718ZM.691,10.3h0V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9H5.53v.806h.691V1.9h.691v.806H7.6V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.737v4a3.894,3.894,0,0,0-.413-.487A3.85,3.85,0,0,0,5.753,5H2.079v.691H5.007a3.834,3.834,0,0,0-.458.691H2.079v.691h2.2a3.839,3.839,0,0,0-.132.689H2.079v.691H4.137A3.81,3.81,0,0,0,4.788,10.3H.691Zm9.512.07a3.132,3.132,0,0,1-1.886.906v-.629H7.626v.629a3.157,3.157,0,0,1-2.8-2.828h.561V7.758H4.838A3.158,3.158,0,0,1,7.626,5v.557h.691V5a3.158,3.158,0,0,1,2.788,2.756h-.542v.691h.55A3.133,3.133,0,0,1,10.2,10.372Z"
                            transform="translate(0 -0.176)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                }
                onChange={(e) => handleChangeEndDateValues(e)}
              />
            </div>
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="location"
                style={{ minWidth: "32% " }}
                placeholder="Location"
                error={locationError}
                errorMessage={locationErrorMessage}
                value={projectLocation}
                svg={
                  <svg
                    id="pin"
                    xmlns="http://www.w3.org/2000/svg"
                    width="7.983"
                    height="11.61"
                    viewBox="0 0 7.983 11.61"
                  >
                    <path
                      id="Path_280"
                      data-name="Path 280"
                      d="M87.4,1.988A3.94,3.94,0,0,0,84.049,0c-.059,0-.119,0-.179,0a3.94,3.94,0,0,0-3.348,1.987,4.042,4.042,0,0,0-.053,3.994l2.879,5.269,0,.007a.7.7,0,0,0,1.214,0l0-.007,2.879-5.269A4.042,4.042,0,0,0,87.4,1.988ZM83.959,5.26a1.633,1.633,0,1,1,1.633-1.633A1.634,1.634,0,0,1,83.959,5.26Z"
                      transform="translate(-79.968 0)"
                      fill="#374957"
                    />
                  </svg>
                }
                onChange={(e) => handleChangelocation(e, true)}
                onSelected={(e) => handleChangelocation(e, false)}
              />
              <InputBox
                variant="select"
                placeholder="Industry"
                options={industryType}
                isMulti={false}
                error={industryError}
                errorMessage={industryErrorMessage}
                value={selectedIndustryType}
                style={{ minWidth: "32%", height: "fit-content" }}
                onChange={(e) => handleChangeIndustryType(e)}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18.263"
                    height="18.56"
                    viewBox="0 0 21.263 22.56"
                  >
                    <g id="building_1_" data-name="building (1)" opacity="0.44">
                      <path
                        id="Path_2270"
                        data-name="Path 2270"
                        d="M35.317,21.237H33.574V.661A.661.661,0,0,0,32.913,0H17.781a.661.661,0,0,0-.661.661V21.237H15.377a.661.661,0,0,0,0,1.322H35.317a.661.661,0,1,0,0-1.322Zm-12.223,0V16.732H27.6v4.506Zm5.828,0V16.071a.661.661,0,0,0-.661-.661H22.433a.661.661,0,0,0-.661.661v5.167h-3.33V1.322h13.81V21.237Z"
                        transform="translate(-14.716)"
                      />
                      <path
                        id="Path_2271"
                        data-name="Path 2271"
                        d="M139.14,66.309H135.9a.661.661,0,0,0-.661.661v3.239a.661.661,0,0,0,.661.661h3.239a.661.661,0,0,0,.661-.661V66.97A.661.661,0,0,0,139.14,66.309Zm-.661,3.239h-1.917V67.631h1.917Z"
                        transform="translate(-129.93 -63.387)"
                      />
                      <path
                        id="Path_2272"
                        data-name="Path 2272"
                        d="M277.152,66.309h-3.239a.661.661,0,0,0-.661.661v3.239a.661.661,0,0,0,.661.661h3.239a.661.661,0,0,0,.661-.661V66.97A.661.661,0,0,0,277.152,66.309Zm-.661,3.239h-1.917V67.631h1.917Z"
                        transform="translate(-261.861 -63.387)"
                      />
                      <path
                        id="Path_2273"
                        data-name="Path 2273"
                        d="M139.14,205.954H135.9a.661.661,0,0,0-.661.661v3.239a.661.661,0,0,0,.661.661h3.239a.661.661,0,0,0,.661-.661v-3.239A.661.661,0,0,0,139.14,205.954Zm-.661,3.239h-1.917v-1.917h1.917Z"
                        transform="translate(-129.93 -196.879)"
                      />
                      <path
                        id="Path_2274"
                        data-name="Path 2274"
                        d="M277.152,205.954h-3.239a.661.661,0,0,0-.661.661v3.239a.661.661,0,0,0,.661.661h3.239a.661.661,0,0,0,.661-.661v-3.239A.661.661,0,0,0,277.152,205.954Zm-.661,3.239h-1.917v-1.917h1.917Z"
                        transform="translate(-261.861 -196.879)"
                      />
                    </g>
                  </svg>
                }
              />
            </div>
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="select"
                placeholder="Skills required"
                options={skillsDropDownData}
                isMulti={true}
                error={skillsError}
                errorMessage={skillsErrorMessage}
                style={{ minWidth: "32%", height: "fit-content" }}
                value={projectSkills}
                onChange={(e) => handleSkills(e)}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.818"
                    height="11.818"
                    viewBox="0 0 11.818 11.818"
                  >
                    <g id="pencil" transform="translate(-0.001)">
                      <g
                        id="Group_334"
                        data-name="Group 334"
                        transform="translate(0.001 0)"
                      >
                        <path
                          id="Path_381"
                          data-name="Path 381"
                          d="M11.113,7.737a2.4,2.4,0,0,0-2.395-.594L7.874,6.3,11.56,2.613a.883.883,0,0,0,0-1.248L10.454.259a.883.883,0,0,0-1.248,0L5.52,3.945,4.676,3.1A2.391,2.391,0,0,0,1.427.209a.231.231,0,0,0-.07.374l1.1,1.1-.779.778-1.1-1.1a.231.231,0,0,0-.374.07A2.391,2.391,0,0,0,3.1,4.681l.844.844L1.117,8.348a.233.233,0,0,0-.056.091L.055,11.472a.231.231,0,0,0,.292.292l3.03-1a.278.278,0,0,0,.094-.057L6.295,7.879l.844.844a2.391,2.391,0,0,0,3.249,2.892.231.231,0,0,0,.07-.374l-1.1-1.1.779-.779,1.1,1.1a.231.231,0,0,0,.374-.07,2.391,2.391,0,0,0-.5-2.654ZM9.532.585a.421.421,0,0,1,.6,0l1.106,1.106a.421.421,0,0,1,0,.6l-.749.749-1.7-1.7ZM3.321,4.253A.231.231,0,0,0,3.076,4.2,1.93,1.93,0,0,1,.514,1.95l1,1a.231.231,0,0,0,.326,0l1.1-1.1a.231.231,0,0,0,0-.326l-1-1A1.93,1.93,0,0,1,4.2,3.081a.231.231,0,0,0,.053.245l.945.945L4.266,5.2ZM.638,11.181l.334-1.007.674.674Zm1.5-.5-1-1,.247-.744,1.5,1.5Zm1.172-.472L2.62,9.525,4.88,7.266a.231.231,0,0,0-.326-.326L2.294,9.2l-.688-.688L8.457,1.66l.019.019.669.669L6.029,5.464a.231.231,0,1,0,.326.326L9.471,2.674l.688.688ZM11.3,9.873l-1-1a.231.231,0,0,0-.326,0l-1.1,1.1a.231.231,0,0,0,0,.326l1,1a1.93,1.93,0,0,1-2.25-2.562A.231.231,0,0,0,7.566,8.5l-.945-.945.927-.927.945.945a.231.231,0,0,0,.245.053A1.93,1.93,0,0,1,11.3,9.873Z"
                          transform="translate(-0.001 0)"
                          fill="#374957"
                        />
                        <g
                          id="Group_333"
                          data-name="Group 333"
                          transform="translate(5.226 6.132)"
                        >
                          <path
                            id="Path_382"
                            data-name="Path 382"
                            d="M226.63,266.112a.231.231,0,1,1,.226-.186A.233.233,0,0,1,226.63,266.112Z"
                            transform="translate(-226.399 -265.651)"
                            fill="#374957"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                }
              />
              <InputBox
                variant="select"
                placeholder="Budget type"
                options={budgetTypeDropDownData}
                isMulti={false}
                error={budgetTypeError}
                errorMessage={budgetTypeErrorMessage}
                onChange={(e) => handleBudgetType(e)}
                value={budgetType}
                style={{ minWidth: "32%", height: "fit-content" }}
                svg={
                  <svg
                    id="checklist"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.169"
                    height="13.488"
                    viewBox="0 0 11.169 13.488"
                  >
                    <g
                      id="Group_304"
                      data-name="Group 304"
                      transform="translate(5.321 4.004)"
                    >
                      <g
                        id="Group_303"
                        data-name="Group 303"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_363"
                          data-name="Path 363"
                          d="M250.058,152h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -152)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_306"
                      data-name="Group 306"
                      transform="translate(8.007 5.269)"
                    >
                      <g
                        id="Group_305"
                        data-name="Group 305"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_364"
                          data-name="Path 364"
                          d="M348.391,200.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,200.078Z"
                          transform="translate(-347.941 -200.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_308"
                      data-name="Group 308"
                      transform="translate(5.321 1.133)"
                    >
                      <g id="Group_307" data-name="Group 307">
                        <path
                          id="Path_365"
                          data-name="Path 365"
                          d="M246.441,43.078a.263.263,0,1,0,.077.186A.265.265,0,0,0,246.441,43.078Z"
                          transform="translate(-245.991 -43.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_310"
                      data-name="Group 310"
                      transform="translate(5.321 5.269)"
                    >
                      <g
                        id="Group_309"
                        data-name="Group 309"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_366"
                          data-name="Path 366"
                          d="M247.842,200h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -200)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_312"
                      data-name="Group 312"
                      transform="translate(5.321 6.849)"
                    >
                      <g
                        id="Group_311"
                        data-name="Group 311"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_367"
                          data-name="Path 367"
                          d="M250.058,260h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_314"
                      data-name="Group 314"
                      transform="translate(8.007 8.114)"
                    >
                      <g
                        id="Group_313"
                        data-name="Group 313"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_368"
                          data-name="Path 368"
                          d="M348.391,308.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,308.078Z"
                          transform="translate(-347.941 -308.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_316"
                      data-name="Group 316"
                      transform="translate(5.321 8.114)"
                    >
                      <g
                        id="Group_315"
                        data-name="Group 315"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_369"
                          data-name="Path 369"
                          d="M247.842,308h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -308)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_318"
                      data-name="Group 318"
                      transform="translate(5.321 9.694)"
                    >
                      <g
                        id="Group_317"
                        data-name="Group 317"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_370"
                          data-name="Path 370"
                          d="M250.058,368h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_320"
                      data-name="Group 320"
                      transform="translate(8.007 10.959)"
                    >
                      <g
                        id="Group_319"
                        data-name="Group 319"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_371"
                          data-name="Path 371"
                          d="M348.391,416.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,416.078Z"
                          transform="translate(-347.941 -416.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_322"
                      data-name="Group 322"
                      transform="translate(5.321 10.959)"
                    >
                      <g
                        id="Group_321"
                        data-name="Group 321"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_372"
                          data-name="Path 372"
                          d="M247.842,416h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -416)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_324"
                      data-name="Group 324"
                      transform="translate(0)"
                    >
                      <g id="Group_323" data-name="Group 323">
                        <path
                          id="Path_373"
                          data-name="Path 373"
                          d="M53.886,1.027H51.874A1.614,1.614,0,0,0,50.757.58h-.231a1.054,1.054,0,0,0-1.882,0h-.231a1.614,1.614,0,0,0-1.117.448H45.285A1.286,1.286,0,0,0,44,2.312V12.2a1.286,1.286,0,0,0,1.284,1.284h8.6A1.286,1.286,0,0,0,55.17,12.2V2.312A1.286,1.286,0,0,0,53.886,1.027Zm-5.473.079h.411a.263.263,0,0,0,.254-.193.527.527,0,0,1,1.015,0,.263.263,0,0,0,.254.193h.41a1.1,1.1,0,0,1,1.089,1H47.324A1.1,1.1,0,0,1,48.413,1.106Zm6.23,11.1a.758.758,0,0,1-.758.758h-8.6a.758.758,0,0,1-.758-.758V2.312a.758.758,0,0,1,.758-.758h1.642a1.61,1.61,0,0,0-.134.645v.171a.263.263,0,0,0,.263.263h5.057a.263.263,0,0,0,.263-.263V2.2a1.61,1.61,0,0,0-.134-.645h1.643a.758.758,0,0,1,.758.758Z"
                          transform="translate(-44.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_326"
                      data-name="Group 326"
                      transform="translate(1.614 3.864)"
                    >
                      <g
                        id="Group_325"
                        data-name="Group 325"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_374"
                          data-name="Path 374"
                          d="M107.557,146.744a.263.263,0,0,0-.373,0l-1.126,1.127-.347-.347a.263.263,0,0,0-.373.373l.533.533a.263.263,0,0,0,.373,0l1.313-1.313A.263.263,0,0,0,107.557,146.744Z"
                          transform="translate(-105.261 -146.667)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_328"
                      data-name="Group 328"
                      transform="translate(1.739 9.694)"
                    >
                      <g
                        id="Group_327"
                        data-name="Group 327"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_375"
                          data-name="Path 375"
                          d="M111.529,368h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,368Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_330"
                      data-name="Group 330"
                      transform="translate(1.739 6.849)"
                    >
                      <g
                        id="Group_329"
                        data-name="Group 329"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_376"
                          data-name="Path 376"
                          d="M111.529,260h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,260Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                  </svg>
                }
              />
            </div>

            {budgetType.label === "Range" ? (
              <div className="post__job__container__input__container__wrapper__top__sub">
                <div className="post__job__container__input__container__wrapper__top__label">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    style={{ marginRight: ".5em" }}
                    width="17.158"
                    height="14.745"
                    viewBox="0 0 17.158 14.745"
                  >
                    <g id="budget" transform="translate(0 -36)">
                      <path
                        id="Path_383"
                        data-name="Path 383"
                        d="M13.437,43.943a3.976,3.976,0,0,0-.2-7.943h-.046a3.975,3.975,0,0,0-2.529.907.251.251,0,1,0,.32.388,3.476,3.476,0,1,1,.417,5.657A3.51,3.51,0,0,1,10.1,41.58a3.461,3.461,0,0,1,.117-3.413.251.251,0,0,0-.429-.262A3.962,3.962,0,0,0,9.274,40.7a3.971,3.971,0,0,0-4.906,4.326L2.8,45.508a.661.661,0,0,0-.6-.38H.66a.661.661,0,0,0-.66.661v3.784a.661.661,0,0,0,.66.66H2.2a.661.661,0,0,0,.624-.445l2.021.351a.269.269,0,0,0,.043,0,.251.251,0,0,0,.043-.5l-2.07-.359v-3.27l1.6-.493.39-.121.09-.028a8.523,8.523,0,0,1,2.879-.481h.815a.988.988,0,0,1,.27.088.779.779,0,0,1,.429.549.7.7,0,0,1,.01.188c-.024.3-.255.616-.83.616H7.879A.817.817,0,0,0,7,47.028a.833.833,0,0,0,.7.934l.016,0L8,48.013l1.67.279.062.01.022,0c.119.009.235.023.346.037a2.874,2.874,0,0,0,1.062-.012l3.609-.843h0c.422-.106.636.089.7.306a.521.521,0,0,1-.406.64L9.4,50.126a2.711,2.711,0,0,1-1.345.06l-.013,0-1.968-.341a.251.251,0,1,0-.086.5l1.961.34a3,3,0,0,0,.649.068,3.3,3.3,0,0,0,.946-.138l5.661-1.692a1.026,1.026,0,0,0,.747-1.255,1,1,0,0,0-1.3-.662l-3.607.843a1.8,1.8,0,0,1-.549.037,3.992,3.992,0,0,0,.8-.7,3.948,3.948,0,0,0,.984-2.879,4.015,4.015,0,0,0-.057-.468,3.971,3.971,0,0,0,.958.117q.128,0,.257-.008ZM2.36,49.573a.16.16,0,0,1-.158.157H.66A.159.159,0,0,1,.5,49.573V45.789a.158.158,0,0,1,.157-.158H2.2a.16.16,0,0,1,.158.158Zm8.558-2.723a3.456,3.456,0,0,1-1.3.925l-1.816-.3c-.219-.054-.334-.191-.307-.366a.317.317,0,0,1,.358-.269h.662a1.213,1.213,0,0,0,1.331-1.078,1.256,1.256,0,0,0-.6-1.162.811.811,0,0,0-.624-.293H8a.318.318,0,0,1,0-.636H9.191a.251.251,0,1,0,0-.5H8.564V42.74a.251.251,0,0,0-.5,0v.425H8A.82.82,0,0,0,7.3,44.4a9.271,9.271,0,0,0-2.439.473,3.461,3.461,0,0,1,.85-2.613h0a3.477,3.477,0,0,1,3.716-.995,4,4,0,0,0,2.243,2.386,3.515,3.515,0,0,1,.112.684A3.448,3.448,0,0,1,10.918,46.85Z"
                        fill="#0b2239"
                      />
                      <path
                        id="Path_384"
                        data-name="Path 384"
                        d="M361.2,95.945h-1.187a.251.251,0,1,0,0,.5h.626v.424a.251.251,0,0,0,.5,0v-.424h.058a.821.821,0,0,0,0-1.642h-.621a.318.318,0,0,1,0-.636h1.187a.251.251,0,1,0,0-.5h-.627v-.424a.251.251,0,0,0-.5,0v.424h-.057a.821.821,0,0,0,0,1.641h.621a.318.318,0,0,1,0,.637Z"
                        transform="translate(-347.708 -55.082)"
                        fill="#0b2239"
                      />
                    </g>
                  </svg>
                  Budget Range (Monthly)<b style={{ marginLeft: 4 }}>€</b>
                </div>
                <InputBox
                  variant="simple"
                  placeholder="Min"
                  name="minbudget"
                  error={minBudgetError}
                  errorMessage={minBudgetErrorMessage}
                  value={budgetMin}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      handleChangeValues(event);
                    }
                  }}
                  maxValue={15}
                />
                <InputBox
                  variant="simple"
                  placeholder="Max"
                  name="maxbudget"
                  error={maxBudgetError}
                  errorMessage={maxBudgetErrorMessage}
                  value={budgetMax}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      handleChangeValues(event);
                    }
                  }}
                  maxValue={15}
                />
              </div>
            ) : budgetType.label === "Fixed" ? (
              <div className="post__job__container__input__container__wrapper__top__sub">
                <div className="post__job__container__input__container__wrapper__top__label">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    style={{ marginRight: ".5em" }}
                    width="17.158"
                    height="14.745"
                    viewBox="0 0 17.158 14.745"
                  >
                    <g id="budget" transform="translate(0 -36)">
                      <path
                        id="Path_383"
                        data-name="Path 383"
                        d="M13.437,43.943a3.976,3.976,0,0,0-.2-7.943h-.046a3.975,3.975,0,0,0-2.529.907.251.251,0,1,0,.32.388,3.476,3.476,0,1,1,.417,5.657A3.51,3.51,0,0,1,10.1,41.58a3.461,3.461,0,0,1,.117-3.413.251.251,0,0,0-.429-.262A3.962,3.962,0,0,0,9.274,40.7a3.971,3.971,0,0,0-4.906,4.326L2.8,45.508a.661.661,0,0,0-.6-.38H.66a.661.661,0,0,0-.66.661v3.784a.661.661,0,0,0,.66.66H2.2a.661.661,0,0,0,.624-.445l2.021.351a.269.269,0,0,0,.043,0,.251.251,0,0,0,.043-.5l-2.07-.359v-3.27l1.6-.493.39-.121.09-.028a8.523,8.523,0,0,1,2.879-.481h.815a.988.988,0,0,1,.27.088.779.779,0,0,1,.429.549.7.7,0,0,1,.01.188c-.024.3-.255.616-.83.616H7.879A.817.817,0,0,0,7,47.028a.833.833,0,0,0,.7.934l.016,0L8,48.013l1.67.279.062.01.022,0c.119.009.235.023.346.037a2.874,2.874,0,0,0,1.062-.012l3.609-.843h0c.422-.106.636.089.7.306a.521.521,0,0,1-.406.64L9.4,50.126a2.711,2.711,0,0,1-1.345.06l-.013,0-1.968-.341a.251.251,0,1,0-.086.5l1.961.34a3,3,0,0,0,.649.068,3.3,3.3,0,0,0,.946-.138l5.661-1.692a1.026,1.026,0,0,0,.747-1.255,1,1,0,0,0-1.3-.662l-3.607.843a1.8,1.8,0,0,1-.549.037,3.992,3.992,0,0,0,.8-.7,3.948,3.948,0,0,0,.984-2.879,4.015,4.015,0,0,0-.057-.468,3.971,3.971,0,0,0,.958.117q.128,0,.257-.008ZM2.36,49.573a.16.16,0,0,1-.158.157H.66A.159.159,0,0,1,.5,49.573V45.789a.158.158,0,0,1,.157-.158H2.2a.16.16,0,0,1,.158.158Zm8.558-2.723a3.456,3.456,0,0,1-1.3.925l-1.816-.3c-.219-.054-.334-.191-.307-.366a.317.317,0,0,1,.358-.269h.662a1.213,1.213,0,0,0,1.331-1.078,1.256,1.256,0,0,0-.6-1.162.811.811,0,0,0-.624-.293H8a.318.318,0,0,1,0-.636H9.191a.251.251,0,1,0,0-.5H8.564V42.74a.251.251,0,0,0-.5,0v.425H8A.82.82,0,0,0,7.3,44.4a9.271,9.271,0,0,0-2.439.473,3.461,3.461,0,0,1,.85-2.613h0a3.477,3.477,0,0,1,3.716-.995,4,4,0,0,0,2.243,2.386,3.515,3.515,0,0,1,.112.684A3.448,3.448,0,0,1,10.918,46.85Z"
                        fill="#0b2239"
                      />
                      <path
                        id="Path_384"
                        data-name="Path 384"
                        d="M361.2,95.945h-1.187a.251.251,0,1,0,0,.5h.626v.424a.251.251,0,0,0,.5,0v-.424h.058a.821.821,0,0,0,0-1.642h-.621a.318.318,0,0,1,0-.636h1.187a.251.251,0,1,0,0-.5h-.627v-.424a.251.251,0,0,0-.5,0v.424h-.057a.821.821,0,0,0,0,1.641h.621a.318.318,0,0,1,0,.637Z"
                        transform="translate(-347.708 -55.082)"
                        fill="#0b2239"
                      />
                    </g>
                  </svg>
                  Budget Fixed (Monthly)<b style={{ marginLeft: 4 }}>€</b>
                </div>
                <InputBox
                  placeholder="Amount"
                  name="minbudget"
                  error={minBudgetError}
                  errorMessage={minBudgetErrorMessage}
                  value={budgetMin}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      handleChangeValues(event);
                    }
                  }}
                  maxValue={15}
                />
              </div>
            ) : null}
          </div>
          <div className="post__job__container__input__container__wrapper__bottom">
            <InputBox
              variant="textarea"
              placeholder="Project description "
              name="description"
              error={descriptionError}
              errorMessage={descriptionErrorMessage}
              value={projectDescription}
              onChange={(e) => handleChangeDescriptionValues(e)}
            />
          </div>

          <div className="post__job__container__input__container__wrapper__bottom__btns">
            <button
              className="post__job__container__input__container__wrapper__bottom__btns__preview"
              onClick={() => {
                showPreviewData();
              }}
              title="preview data"
            >
              Preview
            </button>
            <button
              className="homepage__container__result__card__content__right__buttons__drafts "
              onClick={() => saveDraftProject()}
              title="save draft"
              disabled={isLoading ? true : false}
            >
              {isLoading ? "Processing..." : "Save Draft"}

            </button>
            <button
              className="header__nav__btn btn__secondary"
              style={{
                height: "40px",
              }}
              onClick={() => save()}
              title={isUpdate ? "Update" : "Post"}
              disabled={isLoading ? true : false}
            >
              {isUpdate ? isLoading ? "Processing..." : "Update" : isLoading ? "Processing..." : "Post"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default PostProject;
