import React, { useContext, useState, useEffect } from "react";
import Header from "Components/Header";
import { Link, useNavigate } from "@reach/router";
import { getTopTags } from "API/Tags";
import UserContext from "Context/UserContext";
import CoffeeCornerResultCard from "Components/CoffeeCornerResultCard";
import MobileAppAdvert from "Components/MobileAppAdvert";
import { Head, Avatar, ContactTab } from "Components";
import { useDispatch } from "react-redux";
import moment from "moment";
import { getAllJobsProjectsCoffeCorner } from "API/CompanyAPI";
import Pagination from "react-js-pagination";
import { coffeeCornerSvg } from "Assets";
import Img from "react-cool-img";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import CoffeeCornerJumbotron from "Components/CoffeeCornerJumbotron";
import { questionGetBySearch, trendingForums } from "API/Question";
import {
  getCompanyById,
  getFreelancerById,
  getJobSekkerById,
} from "Redux/Actions/AppActions";
import GetStartedBanner from "Components/GetStartedBanner";
import { getText } from "Utils/functions";
import NoData from "Components/NoData";
import TrendingTopicBadge from "Components/TrendingTopicBadge";
import CoffeeCornerDiscussionCard from "Components/CoffeeCornerDiscussionCard";

export default function CoffeeCorner({
  setIsAskQuestionOpen,
  setIsLoginOpen,
  askQuestionSuccess,
  setIsShareDiscusssion,
}) {
  const isOn = window.localStorage.getItem("isOn");

  const [questions, setQuestions] = useState(null);
  const [trendingQuestions, setTrendingQuestions] = useState([]);
  const [tags, setTags] = useState([]);
  const [tagId, setTagId] = useState();
  const [limit, setLimit] = useState(10);
  const [totalRecords, setTotalRecords] = useState();
  const [pageNumber, setPageNumber] = useState(1);
  let [keyword, setKeyword] = useState("");
  const [jobs, setJobs] = useState("Jobs");
  const [projects, setProjects] = useState(null);
  const [showJobs, setShowJobs] = useState(true);
  const [showProjects, setShowProjects] = useState(false);
  const [postingTableData, setPostingTableData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const user = useContext(UserContext);
  let dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (askQuestionSuccess) getQuestionsBySearch(limit, pageNumber, null, null);
  }, [askQuestionSuccess]);

  useEffect(() => {
    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "company") {
      dispatch(getCompanyById(user.CompanyId));
    } else {
      dispatch(getFreelancerById(user.FreelancerId));
    }
    getTrendingQuestions();
    getTagsList();
  }, []);

  useEffect(() => {
    setPostingTableData([]);
    getJobsOrProject();
  }, [showJobs, showProjects]);

  useEffect(() => {
    getQuestionsBySearch(limit, pageNumber, keyword, tagId);
  }, [pageNumber]);

  const getTagsList = () => {
    getTopTags(10)
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          let formattedDataObject = {
            Id: e.TagId,
            Title: e.Title,
            CompanyProfileId: null,
            JobSeekerId: null,
            FreelancerId: null,
            CreatedOn: e.CreatedOn,
            SearchCount: e.ForumCount,
          };
          formattedData.push(formattedDataObject);
        });
        setTags(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
  };

  const getTrendingQuestions = () => {
    setIsLoading(true);
    trendingForums()
      .then(({ data }) => {
        if (data.result.length > 0) setTrendingQuestions(data.result);
      })
      .catch((err) => {
        // console.log("Err", err);
        setIsLoading(false);
      });
  };

  const getQuestionsBySearch = (limit, pageNumber, keyword, tagId) => {
    setIsLoading(true);
    questionGetBySearch(limit, pageNumber, keyword, tagId)
      .then(({ data }) => {
        setIsLoading(false);
        setQuestions(data.result);
        setTotalRecords(data.totalRecords);
        setPageNumber(data.page);
      })
      .catch((err) => {
        // console.log("Err", err);
        setIsLoading(false);
      });
  };

  const getJobsOrProject = () => {
    getAllJobsProjectsCoffeCorner(null, jobs, projects, 10, pageNumber)
      .then(({ data }) => {
        if (data.success) setPostingTableData(data.result);
        else setPostingTableData([]);
      })
      .catch((err) => {
        // console.log("err", err);
      });
  };

  const setSelectedTagId = (value) => {
    setTagId(value);
    setKeyword((keyword = ""));
    getQuestionsBySearch(limit, pageNumber, keyword, value);
  };

  const searchByKeyword = (event) => {
    setTagId(event.Id);
    setKeyword("");
  };

  const searchByTitle = (value) => {
    setKeyword(value);
  };

  const shareQuestion = (forumId) => {
    localStorage.setItem("forumId", forumId);
    setIsShareDiscusssion(true);
  };

  const resetFilter = () => {
    setKeyword((keyword = ""));
    setTagId();
    setTimeout(() => {
      getQuestionsBySearch(limit, pageNumber, keyword, tagId);
    }, 800);
  };

  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);

  return (
    <>
      <Head title="AIDApro | Coffee Corner" description="Coffee Corner" />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <CoffeeCornerJumbotron
        tags={tags}
        searchByTitle={searchByTitle}
        getQuestionsBySearch={getQuestionsBySearch}
        limit={limit}
        pageNumber={pageNumber}
        keyword={keyword}
        tagId={tagId}
        searchByKeyword={searchByKeyword}
        setIsAskQuestionOpen={setIsAskQuestionOpen}
        setIsLoginOpen={setIsLoginOpen}
        coffeeCornerSvg={coffeeCornerSvg}
        user={user}
        resetFilter={resetFilter}
      />

      <div className="homepage__container">
        {trendingQuestions.length > 0 ? (
          <>
            <div
              className="homepage__container__heading"
              style={{ textAlign: "center", marginBottom: "-2em" }}
            >
              Trending Discussions
            </div>
            <div className="homepage__container__results">
              {trendingQuestions.map((item, index) => (
                <CoffeeCornerResultCard
                  item={item}
                  key={index}
                  to="/coffee-corner-discussion"
                />
              ))}
            </div>
          </>
        ) : null}
        <div className="homepage__container__descussions__panel">
          <div className="homepage__container__descussions__panel__side">
            <div
              className="homepage__container__heading"
              style={{ marginBottom: "1em" }}
            >
              Trending Topics
            </div>
            <div className="homepage__container__descussions__panel__side__content">
              {tags.length > 0
                ? tags.map((data, index) => (
                    <TrendingTopicBadge
                      data={data}
                      key={index}
                      setSelectedTagId={setSelectedTagId}
                      tagId={tagId}
                    />
                  ))
                : null}
            </div>
            <div
              className="homepage__container__heading"
              style={{ marginBottom: "1em" }}
            >
              Trending Jobs & Projects
            </div>

            <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                margin: "1em auto",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              {/* {postingTableData.length > 0 ? ( */}

              <>
                <ContactTab
                  defaultChecked={true}
                  label="Trending Jobs"
                  onClick={(event) => {
                    setShowProjects(false);
                    setShowJobs(true);
                    setProjects(null);
                    setJobs("Jobs");
                  }}
                />
                <ContactTab
                  label="Trending Projects"
                  onClick={(event) => {
                    setShowJobs(false);
                    setShowProjects(true);
                    setJobs(null);
                    setProjects("Projects");
                  }}
                />
              </>
              {/* ) : null} */}
            </div>
            {postingTableData.length > 0 ? (
              <div className="homepage__container__descussions__panel__side__links">
                {postingTableData.map((item, index) => (
                  <div
                    key={index}
                    className="homepage__container__result__card__wrapper"
                    style={{
                      marginBottom: ".5em",
                    }}
                    onClick={() => {
                      if (Object.keys(user).length != 0) {
                        const requestData = {
                          redirectFrom: item.Type,
                          objectData: item,
                          jobType: item.Type,
                        };
                        navigate("/job-project-details", {
                          state: requestData,
                        });
                        setTimeout(() => {
                          window.scrollTo({ top: 0, behavior: "smooth" });
                        }, 300);
                      } else setIsLoginOpen(true);
                    }}
                  >
                    <div className="homepage__container__result__card">
                      <div className="homepage__container__result__card__header">
                        <div className="homepage__container__result__card__header__icon">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20.4"
                            height="17"
                            viewBox="0 0 20.4 17"
                            fill="currentColor"
                          >
                            <g id="suitcase" transform="translate(0 -2)">
                              <path
                                id="Path_2267"
                                data-name="Path 2267"
                                d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                                transform="translate(-1.2)"
                              />
                              <path
                                id="Path_2268"
                                data-name="Path 2268"
                                d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                                transform="translate(0 -1.443)"
                              />
                              <path
                                id="Path_2269"
                                data-name="Path 2269"
                                d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                                transform="translate(0 -0.45)"
                              />
                            </g>
                          </svg>
                        </div>
                        <div className="homepage__container__result__card__header__content">
                          <div
                            className="homepage__container__result__card__header__designation"
                            style={{ marginBottom: "0.3em" }}
                          ></div>
                          <div className="homepage__container__result__card__header__name">
                            {item.Title ? item.Title : null}
                          </div>
                        </div>
                      </div>
                      <div className="homepage__container__result__card__content__info">
                        {getText(item.Description).length > 150 ? (
                          <>{`${getText(item.Description).substring(
                            0,
                            150
                          )}...`}</>
                        ) : (
                          <>{getText(item.Description)}</>
                        )}
                      </div>
                      <div
                        className="homepage__container__result__card__content"
                        style={{ marginBottom: -20 }}
                      >
                        <div className="homepage__container__result__card__content__entry">
                          <Img
                            loading="lazy"
                            src={IndustrySvg}
                            alt="IndustrySvg"
                            className="homepage__container__result__card__content__entry__svg"
                          />
                          <div className="homepage__container__result__card__content__entry__text">
                            {item.Industry != null
                              ? item.Industry.Title
                              : "Not specified"}
                          </div>
                        </div>
                        <div className="homepage__container__result__card__content__entry">
                          <Img
                            loading="lazy"
                            src={experienceSvg}
                            alt="experienceSvg"
                            className="homepage__container__result__card__content__entry__svg"
                          />
                          <div className="homepage__container__result__card__content__entry__text">
                            {item.CompanyDetail != null &&
                            item.CompanyDetail != undefined ? (
                              getText(item.CompanyDetail.NoOfEmployees).length >
                              8 ? (
                                <>{`${getText(
                                  item.CompanyDetail.NoOfEmployees
                                ).substring(0, 8)}... employees`}</>
                              ) : (
                                <>
                                  {getText(item.CompanyDetail.NoOfEmployees)}{" "}
                                  employees
                                </>
                              )
                            ) : (
                              "Not specified"
                            )}
                          </div>
                        </div>
                      </div>

                      <div
                        className="homepage__container__result__card__badges"
                        style={{
                          paddingBottom: "0em",
                          overflow: "hidden",
                          marginTop: 15,
                        }}
                      >
                        {item.Type == "Jobs"
                          ? item.JobSkills.map((skill, index) => (
                              <div
                                className="homepage__container__result__card__badge"
                                key={index}
                              >
                                {skill.Title}
                              </div>
                            ))
                          : item.ProjectSkills.map((skill, index) => (
                              <div
                                className="homepage__container__result__card__badge"
                                key={index}
                              >
                                {skill.Title}
                              </div>
                            ))}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            ) : (
              <NoData />
            )}
          </div>
          <div className="homepage__container__discussions__panel__content">
            <div
              className="homepage__container__heading"
              style={{ marginBottom: "1em" }}
            >
              Recent Discussions
            </div>
            {questions != null ? (
              <>
                {questions.map((item, index) => (
                  <CoffeeCornerDiscussionCard
                    item={item}
                    getText={getText}
                    navigate={navigate}
                    shareQuestion={shareQuestion}
                  />
                ))}
                <div className="posting__container__pagination">
                  <Pagination
                    activePage={pageNumber}
                    itemsCountPerPage={limit}
                    totalItemsCount={totalRecords}
                    pageRangeDisplayed={5}
                    onChange={(e) => setPageNumber(e)}
                  />
                </div>
              </>
            ) : (
              <NoData />
            )}
          </div>
        </div>
        {!user.Id ? <GetStartedBanner /> : null}
        <MobileAppAdvert />
      </div>
    </>
  );
}
