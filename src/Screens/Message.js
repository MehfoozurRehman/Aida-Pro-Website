import React, { useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { useSelector } from "react-redux";
import { chatRequestPost } from "API/Chat";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { navigate } from "@reach/router";
import { crossSvg } from "Assets";

export default function Message({ userData, setIsMessageOpen }) {
  const isOn = window.localStorage.getItem("isOn");

  const { user } = useSelector((state) => state.user);

  const [message, setMessage] = useState("");
  var [isLoading, setIsLoading] = useState(false);

  // validation
  const [messageError, setMessageError] = useState(false);
  const [messageErrorMessage, setMessageErrorMessage] = useState("");

  const onClickSendMessage = async () => {
    if (message === "") {
      setErrorMessageVisibility(true, "Please enter something.");
    } else {
      setIsLoading(isLoading = true);
      let response = await chatRequestPost(
        user.CompanyId != undefined
          ? user.CompanyId
          : userData.CompanyProfileId,
        userData.FreelancerId != 0 ? userData.FreelancerId : null,
        userData.JobSeekerId != 0 ? userData.JobSeekerId : null,
        message,
        user.LoginName
      );

      if (response.data.success) {
        //CustomSuccess(response.data.message);
        setIsLoading(false);
        setIsMessageOpen(false);
      } else if (response.data.status == 535) {
        CustomError(response.data.message);
        setIsLoading(false);
        setIsMessageOpen(false);
        navigate(
          isOn == "company"
            ? "/home-company/messenger"
            : isOn == "professional"
              ? "/home-professional/messenger"
              : "/home-freelancer/messenger"
        );
      } else {
        CustomError(response.data.message);
        setIsLoading(false);
        setIsMessageOpen(false);
      }
    }
  };

  const setErrorMessageVisibility = (value, errorMessage) => {
    setMessageError(value);
    setMessageErrorMessage(errorMessage);
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDAPro | New Message" description="New message" />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsMessageOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                id="paper-plane"
                xmlns="http://www.w3.org/2000/svg"
                width="60.414"
                height="57.896"
                viewBox="0 0 60.414 57.896"
              >
                <path
                  id="Path_2760"
                  data-name="Path 2760"
                  d="M8.75,17.612V29.287A1.887,1.887,0,0,0,12.158,30.4l6.829-9.293Z"
                  transform="translate(13.276 26.721)"
                  fill="#ff7f23"
                />
                <path
                  id="Path_2761"
                  data-name="Path 2761"
                  d="M59.621.35A1.889,1.889,0,0,0,57.652.214L1.015,29.791a1.889,1.889,0,0,0,.262,3.461l15.745,5.382L50.554,9.963,24.606,41.224l26.388,9.019a1.932,1.932,0,0,0,.609.1,1.886,1.886,0,0,0,1.868-1.608L60.394,2.167A1.891,1.891,0,0,0,59.621.35Z"
                  transform="translate(0 0)"
                  fill="#ff7f23"
                />
              </svg>

              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Send Message
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Write message here"
                  error={messageError}
                  errorMessage={messageErrorMessage}
                  style={{
                    height: "fit-content",
                  }}
                  onChange={(event) => {
                    if (event.currentTarget.value == "")
                      setErrorMessageVisibility(
                        true,
                        "Please enter something."
                      );
                    else setErrorMessageVisibility(false, "");
                    setMessage(event.currentTarget.value);
                  }}
                  onKeyDown={(event) => {
                    if (event.key === "Enter") {
                      if (!isLoading)
                        onClickSendMessage();
                    }
                  }}
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => onClickSendMessage()}
                title="send message"
                disabled={isLoading}
              >
                {isLoading ? "Processsing" : "Send"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
