import React from "react";
import { GoogleMap, Autocomplete, LoadScript } from "@react-google-maps/api";

export const CustomGoogleMap = (props) => {
  const libraries = ["drawing", "places"];

  return (
    <LoadScript
      id="google-map-script-loader"
      googleMapsApiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
      // language="English"
      libraries={libraries}
    >
      <div
        style={{
          width: "100%",
          padding: "25px 18px 25px 18px",
        }}
      >
        <GoogleMap
          mapContainerStyle={{
            height: "225px",
            width: "100%",
            borderRadius: "12px",
          }}
          // mapContainerClassName="gm-overides"
          center={props.center}
          onLoad={props.onLoad}
          onCenterChanged={props.onCenterChanged}
          zoom={props.zoom}
          options={{
            disableDefaultUI: true,
          }}
        >
          <Autocomplete
            onLoad={props.onAutocompleteLoad}
            onPlaceChanged={props.onPlaceChanged}
          >
            <input
              type="text"
              placeholder="Search Nearby..."
              style={{
                boxSizing: `border-box`,
                border: `1px solid transparent`,
                width: `350px`,
                height: `35px`,
                padding: `5px 12px`,
                borderRadius: `3px`,
                boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                fontSize: `14px`,
                outline: `none`,
                textOverflow: `ellipses`,
                position: "absolute",
                left: "35%",
                backgroundColor: "#f3f3f3 ",
                top: "2px",
              }}
            />
          </Autocomplete>
        </GoogleMap>
      </div>
    </LoadScript>
  );
};
