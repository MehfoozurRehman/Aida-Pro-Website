import React from "react";
import DraftEntry from "Components/DraftEntry";
import { Head, DashboardHeading } from "Components";
import { postJobSvg } from "Assets";

const Draft = ({ name, setIsJobPreviewOpen }) => {
  return (
    <>
      <Head title={"AIDApro | " + name} description={name} />
      <div className="post__job__container">
        <DashboardHeading heading={name} svg={postJobSvg} />
        <div className="post__job__container__drafts">
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
          <DraftEntry
            title="Machine Learning Engineer"
            date="Saved 3 Days Ago"
            onClick={() => {
              setIsJobPreviewOpen(true);
            }}
          />
        </div>
      </div>
    </>
  );
};
export default Draft;
