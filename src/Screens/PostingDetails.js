import React, { useEffect, useState, useContext } from "react";
import {
  Avatar,
  ContactTab,
  Head,
  JobProjectCard,
  PostingDetailsTab,
} from "Components";
import { useNavigate } from "@reach/router";
import PostingDetailsContact from "./PostingDetailsContact";
import PostingDetailsEducation from "./PostingDetailsEducation";
import PostingDetailsProtfolio from "./PostingDetailsProtfolio";
import PostingDetailsExperiance from "./PostingDetailsExperiance";
import PostingDetailsProfile from "./PostingDetailsProfile";
import {
  getAnalyticsForJob,
  updateJobIsRejected,
  updateProjectIsRejected,
} from "../API/Api";
import { getUserDetailByUserId } from "../API/Users";
import Img from "react-cool-img";
import UserContext from "Context/UserContext";
import { CustomError } from "./Toasts";
import noData from "../Assets/noDataList.svg";
import {
  UserActive,
  userInActive,
  ProjectActive,
  ProjectInactive,
  WorkActive,
  WorkInactive,
  ContactActive,
  ContactInactive,
  EducationActive,
  EducationInactive,
  lock,
} from "Assets";
import timeSvg from "../Assets/timeSvg.svg";
import locationSvg from "../Assets/locationSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import { getTotalYearsExperience } from "Utils/common";

const UserProfile = ({
  setDontShowSidebar,
  setIsRequestVideoCallOpen,
  setVideoCallUserData,
  setIsRejectRequest,
  setIsMessageOpen,
  isRandom,
  location,
  setUserData,
  from,
  isOn,
  setIsPaymentConfirmation,
  dataAfterPaymentConfirmation,
}) => {
  const dataComingFrom = location.state.redirectFrom;
  let listOfUsers;

  if (location.state) {
    listOfUsers = location.state.listOfUsers;
  }

  const [selectedUser, setSelectedUser] = useState(location.state.selectedUser);

  const Id = localStorage.getItem("JobId");
  const Type = localStorage.getItem("JobType");
  const isApplicant = localStorage.getItem("isApplicant");
  const isInterested = localStorage.getItem("isInterested");
  const isVisitor = localStorage.getItem("isVisitor");

  const [isOnProfile, setIsOnProfile] = useState(true);
  const [isOnWorkExperiance, setIsOnWorkExperiance] = useState(false);
  const [isOnProjectPortfolio, setIsOnProjectPortfolio] = useState(false);
  const [isOnEducation, setIsOnEducation] = useState(false);
  const [isOnContact, setIsOnContact] = useState(false);
  const [selectedTab, setSelectedTab] = useState("Active");
  const [activeRejectedData, setActiveRejectedData] = useState([]);

  useEffect(() => {
    setDontShowSidebar && setDontShowSidebar(true);
    return () => setDontShowSidebar && setDontShowSidebar(false);
  }, []);

  const [dataa, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isShowUserProfile, setIsShowUserProfile] = useState(false);
  const [currentJobSeekerData, setCurrentJobSeekerData] = useState();
  const [page, setPage] = useState(1);
  const [pageLimit, setPageLimit] = useState(5);
  const [totalRecords, setTotalRecords] = useState(0);
  const [selectedData, setSelectedData] = useState(null);
  const [experiences, setExperiences] = useState([]);
  const [educations, setEducations] = useState([]);
  const [skills, setSkills] = useState([]);
  const [certificates, setCertificates] = useState([]);
  const [languages, setLanguages] = useState([]);
  const [selectedUserDetailData, setSelectedUserDetailData] = useState(null);
  const user = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (listOfUsers != undefined) {
      getUserDetailByUserId(
        selectedUser.UserId,
        selectedUser.Type,
        user.CompanyId
      )
        .then(({ data }) => {
          if (data.status === 521) {
            CustomError(data.errors);
          } else {
            setSelectedUserDetailData(data.result);
            setIsLoading(false);
          }
        })
        .catch(() => {
          setIsLoading(false);
        });
    }
  }, []);

  useEffect(() => {
    setIsLoading(true);
    if (listOfUsers !== undefined) {
      const selectedItemIndex = listOfUsers.findIndex(
        (element) => element.Id == selectedUser.Id
      );
      listOfUsers.splice(selectedItemIndex, 1);
      listOfUsers.splice(0, 0, selectedUser);
      setData(listOfUsers);
      setActiveRejectedData(listOfUsers);
      setTotalRecords(listOfUsers.length);
      setIsLoading(false);
    } else getAnalyticsForJobApiCall();
  }, [page]);

  const getAnalyticsForJobApiCall = () => {
    setIsLoading(true);
    getAnalyticsForJob(Id, Type, isApplicant, isInterested, isVisitor, page)
      .then(({ data }) => {
        if (data.result) {
          setSelectedUser(data.result[0]);
          setData(data.result);

          setTotalRecords(data.totalRecords);
          let newArray = [];
          {
            data.result.map((dataObject, i) => {
              if (
                dataObject.IsRejected == false ||
                dataObject.IsRejected == null
              )
                newArray.push(dataObject);
            });
          }
          setActiveRejectedData(newArray);
        }
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  const handleShowUserProfile = (selectedJobData) => {
    setCurrentJobSeekerData(selectedJobData);
    setIsShowUserProfile(!isShowUserProfile);
  };

  const handlePageChange = (pageNumber) => {
    setPage(pageNumber);
  };

  const handleSetJobProjectData = (data) => {
    setSelectedUser(data);
    setSelectedUserDetailData(null);
    getUserDetailByUserId(data.UserId, data.Type, user.CompanyId)
      .then(({ data }) => {
        if (data.status === 521) {
          CustomError(data.errors);
        } else {
          setSelectedUserDetailData(data.result);
          bindData(data.result);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const bindData = (data) => {
    let JobSeekerQualifications = [];
    if (data && data.Qualifications) {
      data.Qualifications.map((e) => {
        JobSeekerQualifications.push({
          Id: e.Id,
          JobSeekerId: user.JobSeekerId,
          dateFrom: e.StartDate,
          dateTo: e.EndDate,
          institueName: e.Institute,
          educationType: {
            label: e.DegreeLookupDetail.Title,
            value: e.Id,
          },
        });
      });
    }

    let JobSeekerExperiences = [];
    if (data && data.Experiences) {
      data.Experiences &&
        data.Experiences.map((e) => {
          JobSeekerExperiences.push({
            Id: e.Id,
            jobCompany: e.CompanyName,
            jobDescription: e.Description,
            jobEmployementType: { value: e.EmploymentTypeLookupDetailId },
            jobIndustryType: { value: e.IndustryId },
            jobLocation: e.Location,
            jobTitle: e.Title,
            jobFrom: e.StartDate,
            jobTo: e.EndDate,
          });
        });
    }

    let skillsDTO = [];
    if (data && data.Skills) {
      data.Skills &&
        data.Skills.map((e) => {
          skillsDTO.push({
            label: e.Skill && e.Skill.Title,
            value: e.Skill && e.Id,
            Id: e.Id,
          });
        });
      setSkills(skillsDTO);
    }

    let certificateDTO = [];
    if (data && data.Certificates) {
      data.Certificates &&
        data.Certificates.map((e) => {
          certificateDTO.push({
            Id: e.Id,
            certificateName: e.CertificateName,
            JobSeekerId: user.JobSeekerId,
            dateFrom: e.StartDate,
            dateTo: e.EndDate,
            institueName: e.Institute,
          });
        });
    }

    let languageDTO = [];
    if (data && data.Languages) {
      languageDTO = data.Languages;
      // data.Languages &&
      //   data.Languages.map((e) => {
      //     languageDTO.push({
      //       Id: e.Id,
      //       certificateName: e.CertificateName,
      //       JobSeekerId: user.JobSeekerId,
      //       dateFrom: e.StartDate,
      //       dateTo: e.EndDate,
      //       institueName: e.Institute,
      //     });
      //   });
    }

    setEducations(JobSeekerQualifications);
    setExperiences(JobSeekerExperiences);
    setCertificates(certificateDTO);
    setLanguages(languageDTO);
    setSkills(skillsDTO);
    setSelectedData(data);
    setIsLoading(false);
  };

  const goBack = () => {
    if (dataComingFrom === "Freelancer")
      window.location.href = "/home-freelancer";
    else if (dataComingFrom === "Employee")
      window.location.href = "/home-professional";
    else if (dataComingFrom == "CompanyHome")
      window.location.href = "/home-company";
    else if (dataComingFrom == "Messenger")
      window.location.href = "/home-company/messenger";
    else window.location.href = "/home-company/posting";
  };
  const [overlayOpen, setOverlayOpen] = useState(true);

  const onClickAccept = () => {
    let acceptedObject = {
      Id: selectedUser.Id,
      IsRejected: false,
    };
    setIsLoading(true);
    if (selectedUser.Type == "Employment") {
      updateJobIsRejected(acceptedObject)
        .then(({ data }) => {
          if (data.success) {
            selectedUser.IsRejected = false;
            setSelectedUser(selectedUser);
          } else {
            CustomError("Failed to accept.");
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to accept.");
        });
    } else {
      updateProjectIsRejected(acceptedObject)
        .then(({ data }) => {
          if (data.success) {
            selectedUser.IsRejected = false;
            setSelectedUser(selectedUser);
          } else {
            CustomError("Failed to accept.");
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to accept.");
        });
    }
  };

  const onClickReject = () => {
    let acceptedObject = {
      Id: selectedUser.Id,
      IsRejected: true,
    };
    setIsLoading(true);
    if (selectedUser.Type == "Employment") {
      updateJobIsRejected(acceptedObject)
        .then(({ data }) => {
          if (data.success) {
            // selectedUser.IsRejected = false;
            // setSelectedUser(selectedUser);

            let found = dataa.find((element) => element.Id == selectedUser.Id);
            if (found != null && found != undefined) {
              let newArray = [];
              {
                dataa.map((dataObject, i) => {
                  if (dataObject.Id == found.Id) {
                    dataObject.IsRejected = true;
                  }
                  if (selectedTab == "Active") {
                    if (
                      dataObject.IsRejected == false ||
                      dataObject.IsRejected == null
                    ) {
                      newArray.push(dataObject);
                    }
                  } else {
                    if (dataObject.IsRejected == true) {
                      newArray.push(dataObject);
                    }
                  }
                });
              }
              setActiveRejectedData([]);
              setActiveRejectedData(newArray);
              setSelectedUser(selectedUser);
            }
          } else {
            CustomError("Failed to accept.");
          }
          // window.location.reload();
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to reject.");
        });
    } else {
      updateProjectIsRejected(acceptedObject)
        .then(({ data }) => {
          if (data.success) {
            // selectedUser.IsRejected = false;
            let found = dataa.find((element) => element.Id == selectedUser.Id);
            if (found != null && found != undefined) {
              let newArray = [];
              {
                dataa.map((dataObject, i) => {
                  if (dataObject.Id == found.Id) {
                    dataObject.IsRejected = true;
                  }
                  if (selectedTab == "Active") {
                    if (
                      dataObject.IsRejected == false ||
                      dataObject.IsRejected == null
                    ) {
                      newArray.push(dataObject);
                    }
                  } else {
                    if (dataObject.IsRejected == true) {
                      newArray.push(dataObject);
                    }
                  }
                });
              }
              setActiveRejectedData([]);
              setActiveRejectedData(newArray);
              setSelectedUser(selectedUser);
            } else {
              CustomError("Failed to accept.");
            }
          }
          // window.location.reload();
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to reject.");
        });
    }
  };
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);

  useEffect(() => {
    if (dataAfterPaymentConfirmation) {
      setOverlayOpen(false);
    }
  }, [dataAfterPaymentConfirmation]);

  return (
    <>
      <Head title="AIDApro | Posting Details" description="Posting Details" />
      <div className="posting__details__container">
        {from === "profile" ? null : (
          <div
            className="posting__details__container__back__link"
            onClick={() => goBack()}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="7.639"
              height="13.363"
              style={{ marginRight: "1em" }}
              viewBox="0 0 7.639 13.363"
            >
              <path
                id="Icon_ionic-ios-arrow-back"
                data-name="Icon ionic-ios-arrow-back"
                d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                transform="translate(-11.251 -6.194)"
              />
            </svg>
            Back
          </div>
        )}
        <div
          className="homepage__container__jobs__projects__penel"
          style={{ margin: "0em" }}
        >
          <div
            className="homepage__container__jobs__projects__penel__container"
            style={{ margin: "0em" }}
          >
            {from === "profile" ? null : (
              <div className="homepage__container__jobs__projects__penel__container__sidebar">
                {dataa != null ? (
                  <div
                    className="homepage__container__results"
                    style={
                      activeRejectedData.length > 0
                        ? null
                        : {
                            backgroundColor: "transparent",
                            boxShadow: "none",
                          }
                    }
                  >
                    {dataComingFrom === undefined && isApplicant == 1 ? (
                      <div
                        className="homepage__container__jobs__projects__penel__container__details__tabs"
                        style={{
                          width: "fit-content",
                          margin: "1em auto",
                          marginTop: "1em",
                        }}
                        id="homepage__container__jobs__projects__penel__container__details__tabs"
                      >
                        <ContactTab
                          defaultChecked={
                            selectedTab == "Active" ? true : false
                          }
                          label="Active"
                          onClick={() => {
                            setSelectedTab("Active");
                            setSelectedUserDetailData(null);
                            getAnalyticsForJobApiCall();
                          }}
                        />
                        {/* <ContactTab
                          label="Accepted"
                          defaultChecked={selectedTab == "Accepted" ? true : false}
                          onClick={() => {
                            setSelectedTab("Accepted");
                            setSelectedUserDetailData(null);
                          }}
                        /> */}
                        <ContactTab
                          label="Rejected"
                          defaultChecked={
                            selectedTab == "Rejected" ? true : false
                          }
                          onClick={() => {
                            setSelectedTab("Rejected");
                            setSelectedUserDetailData(null);
                            let newArray = [];
                            {
                              dataa.map((dataObject, i) => {
                                if (dataObject.IsRejected == true)
                                  newArray.push(dataObject);
                              });
                            }
                            setActiveRejectedData(newArray);
                            // handleSetJobProjectData(selectedUser);
                          }}
                        />
                      </div>
                    ) : null}
                    {activeRejectedData.length > 0 ? (
                      activeRejectedData.map((item, index) => (
                        <JobProjectCard
                          key={index}
                          data={item}
                          setProjectData={handleSetJobProjectData}
                          selectedTab={selectedTab}
                          checked={selectedUser.Id == item.Id ? true : false}
                        />
                      ))
                    ) : (
                      <div
                        className="no__data__container"
                        style={{ height: 750 }}
                      >
                        <Img
                          loading="lazy"
                          src={noData}
                          className="no__data__container__img"
                        />
                        <div className="no__data__container__text">
                          No Rejected
                        </div>
                      </div>
                    )}
                    {/* {data.map((dataObject, i) => {
                          if (selectedTab == "Active") {
                            if (
                              dataObject.IsRejected == false ||
                              dataObject.IsRejected == null
                            ) {
                              newData = dataObject;
                            }
                          } else {
                            if (dataObject.IsRejected == true) {
                              newData = dataObject;
                            }
                          }
                          if (newData != null) {
                            return (
                              <JobProjectCard
                                key={i}
                                data={newData}
                                setProjectData={handleSetJobProjectData}
                                selectedTab={selectedTab}
                                checked={
                                  selectedUser.Id == newData.Id ? true : false
                                }
                              />
                            );
                          } else {
                            return (
                              <div
                                className="no__data__container"
                                style={{ height: 750 }}
                              >
                                <Img loading="lazy"
                                  src={noData}
                                  className="no__data__container__img"
                                />
                                <div className="no__data__container__text">
                                  No Rejected
                                </div>
                              </div>
                            );
                          }
                        })} */}
                  </div>
                ) : null}
              </div>
            )}
            <div
              className="homepage__container__jobs__projects__penel__container__details"
              style={from === "profile" ? { width: "100%" } : null}
            >
              {overlayOpen ? (
                <div className="homepage__container__jobs__projects__penel__container__details__overlay">
                  <button
                    className="homepage__container__jobs__projects__penel__container__details__overlay__btn"
                    onClick={() => {
                      setIsPaymentConfirmation(true);
                      // setOverlayOpen(false);
                    }}
                    title="unlock"
                  >
                    <Img
                      loading="lazy"
                      src={lock}
                      alt="lock"
                      className="homepage__container__jobs__projects__penel__container__details__overlay__btn__img"
                    />
                  </button>
                  <div className="homepage__container__jobs__projects__penel__container__details__overlay__text">
                    Subscribe to Unlock
                  </div>
                </div>
              ) : null}
              <div className="homepage__container__jobs__projects__penel__container__details__heading">
                <Avatar
                  userPic={
                    selectedUserDetailData != null
                      ? selectedUserDetailData.ProfilePicture != null &&
                        selectedUserDetailData.ProfilePicture != ""
                        ? process.env.REACT_APP_BASEURL.concat(
                            selectedUserDetailData.ProfilePicture
                          )
                        : null
                      : null
                  }
                />{" "}
                {selectedUserDetailData != null
                  ? selectedUserDetailData.FullName
                  : "hey"}
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__feature">
                <div className="homepage__container__result__card__content">
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={timeSvg}
                      alt="timeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {/* {selectedUserDetailData
                        ? selectedUserDetailData.JobSeekerExperiences != null &&
                          selectedUserDetailData.JobSeekerExperiences.length > 0
                          ? selectedUserDetailData.JobSeekerExperiences[0].Title
                          : "Not specified"
                        : "Not specified"} */}
                      {selectedUserDetailData != null
                        ? selectedUserDetailData.AvailabilityLookupDetail !=
                          null
                          ? selectedUserDetailData.AvailabilityLookupDetail
                              .Title
                          : "Not specified"
                        : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={locationSvg}
                      alt="locationSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {selectedUserDetailData != null
                        ? selectedUserDetailData.Location != ""
                          ? selectedUserDetailData.Location
                          : "Not specified"
                        : "Not specified"}
                    </div>
                  </div>

                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={IndustrySvg}
                      alt="IndustrySvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {selectedUserDetailData != null
                        ? selectedUserDetailData.Experiences != null &&
                          selectedUserDetailData.Experiences.length > 0
                          ? selectedUserDetailData.Experiences[0].Industry.Title
                          : "Not specified"
                        : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={experienceSvg}
                      alt="experienceSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {selectedUserDetailData != null
                        ? selectedUserDetailData.TotalExperience == 0
                          ? "Junior"
                          : getTotalYearsExperience(
                              selectedUserDetailData.TotalExperience
                            )
                        : "Not specified"}
                    </div>
                  </div>
                </div>
                <div className="homepage__container__btns">
                  {from === "profile" ? (
                    <>
                      <button
                        onClick={() => {
                          navigate(`/home-${isOn}/change-password`);
                        }}
                        className="header__nav__btn btn__secondary"
                        style={{
                          width: "150px",
                          height: "40px",
                          display: "flex",
                          alignItems: "center",
                          marginBottom: 10,
                        }}
                        title="change password"
                      >
                        Change Password
                      </button>
                    </>
                  ) : (
                    <>
                      {" "}
                      {selectedUserDetailData != null ? (
                        <button
                          onClick={() => {
                            if (selectedUserDetailData.ChatNodeName == "") {
                              setUserData(selectedData);
                              setIsMessageOpen(true);
                            } else {
                              navigate("/home-company/messenger", {
                                state: {
                                  ChatNodeName:
                                    selectedUserDetailData.ChatNodeName,
                                },
                              });
                            }
                          }}
                          className="header__nav__btn btn__secondary"
                          style={{
                            width: "150px",
                            height: "50px",
                            display: "flex",
                            alignItems: "center",
                          }}
                          title="message"
                        >
                          <svg
                            id="paper-plane"
                            xmlns="http://www.w3.org/2000/svg"
                            width="21.295"
                            height="20.408"
                            viewBox="0 0 21.295 20.408"
                            style={{ marginRight: ".5em" }}
                          >
                            <path
                              id="Path_2760"
                              data-name="Path 2760"
                              d="M8.75,17.612v4.115a.665.665,0,0,0,1.2.394l2.407-3.276Z"
                              transform="translate(-0.986 -1.985)"
                              fill="#fff"
                            />
                            <path
                              id="Path_2761"
                              data-name="Path 2761"
                              d="M21.015.123a.666.666,0,0,0-.694-.048L.358,10.5a.666.666,0,0,0,.092,1.22L6,13.618,17.819,3.512,8.673,14.531l9.3,3.179a.681.681,0,0,0,.215.035.665.665,0,0,0,.658-.567L21.288.764a.667.667,0,0,0-.272-.641Z"
                              transform="translate(0 0)"
                              fill="#fff"
                            />
                          </svg>
                          Message
                        </button>
                      ) : null}
                      {selectedUserDetailData != null ? (
                        <>
                          <button
                            onClick={() => {
                              setIsRequestVideoCallOpen(true);
                              setVideoCallUserData(selectedUserDetailData);
                            }}
                            className="homepage__container__btns__entry"
                            title="request video call"
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="22.593"
                              height="16.138"
                              style={{ marginRight: ".5em" }}
                              viewBox="0 0 22.593 16.138"
                            >
                              <defs>
                                <linearGradient
                                  id="linear-gradient"
                                  x1="0.5"
                                  x2="0.5"
                                  y2="1"
                                  gradientUnits="objectBoundingBox"
                                >
                                  <stop offset="0" stopColor="#0ee1a3" />
                                  <stop offset="1" stopColor="#0ca69d" />
                                </linearGradient>
                              </defs>
                              <g id="facetime-button" transform="translate(0)">
                                <path
                                  id="Path_2762"
                                  data-name="Path 2762"
                                  d="M22.1,73.556a.878.878,0,0,0-.315-.063.75.75,0,0,0-.567.24L16.138,78.8v-2.08a3.637,3.637,0,0,0-3.631-3.631H3.631a3.5,3.5,0,0,0-2.566,1.065A3.5,3.5,0,0,0,0,76.721V85.6a3.5,3.5,0,0,0,1.065,2.566,3.5,3.5,0,0,0,2.566,1.065h8.876A3.637,3.637,0,0,0,16.138,85.6V83.5l5.081,5.081a.75.75,0,0,0,.567.24.88.88,0,0,0,.315-.063.754.754,0,0,0,.492-.744V74.3A.754.754,0,0,0,22.1,73.556Z"
                                  transform="translate(0 -73.09)"
                                  fill="url(#linear-gradient)"
                                />
                              </g>
                            </svg>
                            Video Call
                          </button>
                          {selectedUserDetailData.CV != null &&
                          selectedUserDetailData.CV != "" ? (
                            <button
                              className="homepage__container__btns__entry"
                              onClick={() => {
                                window.open(
                                  process.env.REACT_APP_BASEURL.concat(
                                    selectedUserDetailData.CV
                                  ),
                                  "_blank",
                                  "noopener,noreferrer"
                                );
                              }}
                              title="download cv"
                            >
                              <svg
                                id="cv"
                                xmlns="http://www.w3.org/2000/svg"
                                width="17.371"
                                height="21.4"
                                style={{ marginRight: ".5em" }}
                                viewBox="0 0 17.371 21.4"
                              >
                                <defs>
                                  <linearGradient
                                    id="linear-gradient"
                                    x1="0.5"
                                    x2="0.5"
                                    y2="1"
                                    gradientUnits="objectBoundingBox"
                                  >
                                    <stop offset="0" stopColor="#0ee1a3" />
                                    <stop offset="1" stopColor="#0ca69d" />
                                  </linearGradient>
                                </defs>
                                <path
                                  id="Path_2763"
                                  data-name="Path 2763"
                                  d="M142.468,94.267h2.775v2.775h-2.775Z"
                                  transform="translate(-138.528 -90.327)"
                                  fill="url(#linear-gradient)"
                                />
                                <path
                                  id="Path_2764"
                                  data-name="Path 2764"
                                  d="M336.319,13.792h3.953l-5.005-5.005V12.74A1.053,1.053,0,0,0,336.319,13.792Z"
                                  transform="translate(-323.268 -8.42)"
                                  fill="url(#linear-gradient)"
                                />
                                <path
                                  id="Path_2765"
                                  data-name="Path 2765"
                                  d="M61.251,6.626A2.308,2.308,0,0,1,58.945,4.32V0H50.506A2.308,2.308,0,0,0,48.2,2.306V19.095A2.308,2.308,0,0,0,50.506,21.4H63.265a2.308,2.308,0,0,0,2.306-2.306V6.626ZM50.886,3.313a.627.627,0,0,1,.627-.627h4.029a.627.627,0,0,1,.627.627V7.342a.627.627,0,0,1-.627.627H51.513a.627.627,0,0,1-.627-.627ZM51.513,9.4h8.059a.627.627,0,1,1,0,1.254H51.513a.627.627,0,0,1,0-1.254Zm10.745,9.312H51.513a.627.627,0,1,1,0-1.254H62.258a.627.627,0,0,1,0,1.254Zm0-2.686H51.513a.627.627,0,1,1,0-1.254H62.258a.627.627,0,0,1,0,1.254Zm0-2.686H51.513a.627.627,0,1,1,0-1.254H62.258a.627.627,0,0,1,0,1.254Z"
                                  transform="translate(-48.2 0)"
                                  fill="url(#linear-gradient)"
                                />
                              </svg>
                              View CV
                            </button>
                          ) : null}
                        </>
                      ) : null}
                    </>
                  )}
                  {selectedUserDetailData != null ? (
                    dataComingFrom == undefined && isApplicant == 1 ? (
                      selectedUser.IsRejected == null ? (
                        <button
                          onClick={() => {
                            onClickReject();
                            // setIsRejectRequest(true);
                          }}
                          className="homepage__container__btns__entry"
                          title="Reject"
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="26.014"
                            height="26.02"
                            viewBox="0 0 22.014 22.02"
                            style={{ marginRight: ".7em" }}
                          >
                            <defs>
                              <linearGradient
                                id="linear-gradient"
                                x1="0.5"
                                x2="0.5"
                                y2="1"
                                gradientUnits="objectBoundingBox"
                              >
                                <stop offset="0" stop-color="#0ee1a3" />
                                <stop offset="1" stop-color="#0ca69d" />
                              </linearGradient>
                            </defs>
                            <g
                              id="Group_1768"
                              data-name="Group 1768"
                              transform="translate(-179.677 -142.963)"
                            >
                              <path
                                id="Path_22079"
                                data-name="Path 22079"
                                d="M367.319,324.977c-.072.349-.114.707-.22,1.045a5.8,5.8,0,0,1-4.9,4.307,5.963,5.963,0,1,1,5.035-6.885c.023.139.055.277.083.415Zm-5.947.707a2.273,2.273,0,0,0,.166.219c.272.275.54.555.825.816a.9.9,0,0,0,1.466-.943,1.307,1.307,0,0,0-.287-.443c-.287-.3-.594-.591-.905-.9.34-.337.652-.642.959-.953a.912.912,0,1,0-1.289-1.29c-.315.31-.624.627-.964.968-.335-.345-.64-.689-.975-1a.911.911,0,1,0-1.229,1.345c.308.305.615.613.928.926-.329.328-.64.635-.947.946a.882.882,0,0,0-.249.887.9.9,0,0,0,1.517.422C360.713,326.376,361.018,326.045,361.372,325.683Z"
                                transform="translate(-165.628 -165.418)"
                                fill="url(#linear-gradient)"
                              />
                              <path
                                id="Path_22080"
                                data-name="Path 22080"
                                d="M193.134,142.962c.127.04.255.078.382.119a2.52,2.52,0,0,1,1.759,2.374c.01,1.691,0,3.382,0,5.073a.7.7,0,0,1-.693.743.682.682,0,0,1-.691-.74c0-1.605,0-3.21,0-4.815,0-.107,0-.215-.01-.322a1.14,1.14,0,0,0-1.1-1.055c-.064,0-.129,0-.193,0h-10.21a1.181,1.181,0,0,0-1.319,1.328q0,6.932,0,13.865a1.181,1.181,0,0,0,1.328,1.32q2.429,0,4.858,0a.687.687,0,0,1,.2,1.346.837.837,0,0,1-.233.026c-1.691,0-3.382.005-5.073,0a2.511,2.511,0,0,1-2.455-2.462q0-7.169,0-14.337a2.522,2.522,0,0,1,2.01-2.414,1.255,1.255,0,0,0,.137-.047Z"
                                fill="url(#linear-gradient)"
                              />
                              <path
                                id="Path_22081"
                                data-name="Path 22081"
                                d="M255.122,274.135h-2.9a.692.692,0,0,1-.755-.76,2.935,2.935,0,0,1,.27-1.522,2.4,2.4,0,0,1,2.064-1.368c.885-.043,1.776-.044,2.662,0a2.5,2.5,0,0,1,2.349,2.43c.006.172.006.344,0,.515a.691.691,0,0,1-.728.706Q256.6,274.137,255.122,274.135Z"
                                transform="translate(-67.665 -120.166)"
                                fill="url(#linear-gradient)"
                              />
                              <path
                                id="Path_22082"
                                data-name="Path 22082"
                                d="M285.285,194.559a1.835,1.835,0,1,1,1.862-1.729A1.829,1.829,0,0,1,285.285,194.559Z"
                                transform="translate(-97.842 -45.177)"
                                fill="url(#linear-gradient)"
                              />
                              <path
                                id="Path_22083"
                                data-name="Path 22083"
                                d="M245.985,359.92c-.608,0-1.216,0-1.824,0a.688.688,0,1,1-.01-1.373q1.846,0,3.691,0a.688.688,0,1,1-.011,1.373C247.215,359.921,246.6,359.92,245.985,359.92Z"
                                transform="translate(-60.092 -203.2)"
                                fill="url(#linear-gradient)"
                              />
                              <path
                                id="Path_22084"
                                data-name="Path 22084"
                                d="M245.726,406.449c.53,0,1.06,0,1.59,0a.687.687,0,1,1,0,1.373q-1.59,0-3.18,0a.686.686,0,1,1,0-1.372C244.666,406.447,245.2,406.449,245.726,406.449Z"
                                transform="translate(-60.092 -248.351)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </svg>
                          Reject
                        </button>
                      ) : null
                    ) : null
                  ) : null}
                </div>
              </div>
              <div
                className="homepage__container__jobs__projects__penel__container__details__tabs"
                id="homepage__container__jobs__projects__penel__container__details__tabs"
              >
                <PostingDetailsTab
                  label="Profile"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(true);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={
                    <Img loading="lazy" src={UserActive} alt="UserActive" />
                  }
                  inActiveSvg={
                    <Img loading="lazy" src={userInActive} alt="userInActive" />
                  }
                  defaultChecked={true}
                  isChecked={isOnProfile}
                />
                <PostingDetailsTab
                  label="Work Experience"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(true);
                  }}
                  activeSvg={<Img loading="lazy" src={WorkActive} />}
                  inActiveSvg={<Img loading="lazy" src={WorkInactive} />}
                  isChecked={isOnWorkExperiance}
                />
                <PostingDetailsTab
                  label="Project Portfolio"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(true);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={ProjectActive} />}
                  inActiveSvg={<Img loading="lazy" src={ProjectInactive} />}
                  isChecked={isOnProjectPortfolio}
                />
                <PostingDetailsTab
                  label="Education"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(true);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={EducationActive} />}
                  inActiveSvg={<Img loading="lazy" src={EducationInactive} />}
                  isChecked={isOnEducation}
                />
                <PostingDetailsTab
                  label="Contact"
                  onClick={() => {
                    setIsOnContact(true);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={ContactActive} />}
                  inActiveSvg={<Img loading="lazy" src={ContactInactive} />}
                  isChecked={isOnContact}
                />
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {isOnProfile ? (
                  <PostingDetailsProfile
                    data={selectedUserDetailData}
                    skills={skills}
                    languages={languages}
                  />
                ) : isOnWorkExperiance ? (
                  <PostingDetailsExperiance data={experiences} />
                ) : isOnProjectPortfolio ? (
                  <PostingDetailsProtfolio
                    Portfolios={
                      selectedUserDetailData != null
                        ? selectedUserDetailData.Portfolios
                        : []
                    }
                  />
                ) : isOnEducation ? (
                  <PostingDetailsEducation
                    data={educations}
                    certificatesData={certificates}
                  />
                ) : isOnContact ? (
                  <PostingDetailsContact data={selectedUserDetailData} />
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UserProfile;
