import React, { useContext, useEffect, useState } from "react";
import { DashboardGoogleMap } from "./DashboardGoogleMap";
import DashboardCompanySearchFilter from "Components/DashboardCompanySearchFilter";
import { Head } from "Components";
import { useDispatch, useSelector } from "react-redux";
import { getCompanyById } from "Redux/Actions/AppActions";
import UserContext from "Context/UserContext";
import DashboardCompanyProfileCompletion from "Components/DashboardCompanyProfileCompletion";

const DashboardCompany = ({
  setIsFilterOpen,
  isFilterOpen,
  filterCall,
  title,
  searchBy,
  searchByFreelancer,
  searchByUser,
  keywords,
  latitude,
  longitude,
  mapLocation,
  selectedRangeValue,
  jobSeekers,
}) => {
  localStorage.setItem("isOn", "company");

  let { company } = useSelector((state) => state.company);
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCompanyById(user.CompanyId));
  }, []);

  const [isLoading, setIsLoading] = useState(false);
  const [localTitle, setTitleLocal] = useState(title);
  let [jobLocation, setJobLocation] = useState(mapLocation);
  const [lat, setLat] = useState(latitude);
  const [lng, setLng] = useState(longitude);
  const [range, setRange] = useState(
    selectedRangeValue == 0 ? 500 : selectedRangeValue
  );
  let [profilePercentage, setProfilePercentage] = useState(0);
  const [showProfilePercentageScreen, setShowProfilePercentageScreen] =
    useState(false);

  const apiCall = () => {
    filterCall(localTitle, jobLocation, range, lat, lng);
    setTitleLocal(localTitle);
    setJobLocation(jobLocation);
  };

  const searchDataByTitle = (data) => {
    setTitleLocal(data);
  };

  const searchByLocation = (data, isInput) => {
    if (isInput == false) {
      if (data.name) {
        setJobLocation((jobLocation = ""));
        // setLocationError(true);
        // setLocationErrorMessage("Please enter your correct location");
      } else {
        setJobLocation((jobLocation = data.formatted_address));
        setLat(data.geometry.location.lat());
        setLng(data.geometry.location.lng());
      }
    } else {
      if (data.currentTarget.value === "" && data.name) {
        setLat(null);
        setLng(null);
        setJobLocation((jobLocation = ""));
      } else {
        setLat(null);
        setLng(null);
        setJobLocation(data.currentTarget.value);
      }
    }
  };

  const searchByRange = (data) => {
    setRange(data);
  };

  const searchByKeyword = (data) => {
    setTitleLocal(data);
  };

  const isSignUpProfileCompleted = () => {
    if (
      company.companyName != "" &&
      company.companyContantPerson != "" &&
      company.companyEmailAddress != "" &&
      company.lat != "" &&
      company.lng != "" &&
      company.companyAddress != "" &&
      company.companyBranch != undefined &&
      company.companyBranch != null
    )
      return true;
    else return false;
  };

  const isPersonelDetailCompleted = () => {
    if (
      isSignUpProfileCompleted() &&
      company.companyNoOfEmp != "" &&
      company.companyCellNo != "" &&
      company.companyAddressZipCode != "" &&
      company.Description != "" &&
      company.companyBillingAddress != undefined &&
      company.companyBillingAddressZipCode != undefined
    )
      return true;
    else return false;
  };

  useEffect(() => {
    if (Object.keys(company).length > 0) {
      let signUpProfileCompletedPercentage = 0;
      let personelDetailCompletedPercentage = 0;
      if (isSignUpProfileCompleted()) {
        signUpProfileCompletedPercentage = 50;
      }
      if (isPersonelDetailCompleted()) {
        personelDetailCompletedPercentage = 50;
      }
      let percentage =
        signUpProfileCompletedPercentage + personelDetailCompletedPercentage;
      setProfilePercentage(percentage);
      if (percentage != 100) {
        setShowProfilePercentageScreen(true);
      }
    }
  }, [company]);

  return (
    <>
      <Head
        title="AIDApro | Company Dashboard"
        description="Company Dashbaord"
      />
      {showProfilePercentageScreen ? (
        <DashboardCompanyProfileCompletion
          profilePercentage={profilePercentage}
        />
      ) : null}
      <div className="dashboard__company__container">
        <div className="dashboard__company__container__left">
          <div
            className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
            style={{ textAlign: "left", fontSize: 25 }}
          >
            Search for the
            <br />
            <span>right professional & freelancer</span>
          </div>
          <DashboardCompanySearchFilter
            setIsFilterOpen={setIsFilterOpen}
            isFilterOpen={isFilterOpen}
            keywords={keywords}
            searchByTitle={(e) => searchDataByTitle(e)}
            searchByFilter={() => apiCall()}
            searchByLocation={searchByLocation}
            searchByKeyword={(e) => searchByKeyword(e)}
            searchByRange={searchByRange}
            jobLocation={jobLocation}
            title={localTitle}
            searchBy={searchBy}
            searchByFreelancer={searchByFreelancer}
            searchByUser={searchByUser}
            radius={range}
          />
        </div>
        <div className="dashboard__company__container__right animate__animated animate__fadeInRight">
          <DashboardGoogleMap
            lat={lat}
            lng={lng}
            jobLocation={jobLocation}
            style={{ height: "423px" }}
            jobSeekers={jobSeekers}
          />
        </div>
      </div>
    </>
  );
};

export default DashboardCompany;
