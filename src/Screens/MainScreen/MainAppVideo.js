import MainScreen from "./MainScreen.component";
import firepadRef, { db, userName } from "../../server/firebase";
import firebase from "firebase";
import "./App.css";
import { useContext, useEffect } from "react";
import { connect } from "react-redux";
import {
  addParticipant,
  removeParticipant,
  setMainStream,
  setUser,
  updateParticipant,
} from "Redux/Actions/AppActions";
import { isNullOrEmpty } from "Utils/TextUtils";
import UserContext from "Context/UserContext";
import { stopVideoCallRequest } from "../../API/Video";

function MainAppVideo(props) {
  const isOn = window.localStorage.getItem("isOn");

  const user = useContext(UserContext);

  const userName =
    !isNullOrEmpty(props.location.state) &&
      !isNullOrEmpty(props.location.state.user)
      ? props.location.state.user
      : "default";
  const videoNodeName =
    !isNullOrEmpty(props.location.state) &&
      !isNullOrEmpty(props.location.state.itemData)
      ? props.location.state.itemData.VideoNodeName
      : "CompanyId:1-FreelanceId:1";
  props.setShowFooter(false);
  const timeInterval =
    !isNullOrEmpty(props.location.state) &&
      !isNullOrEmpty(props.location.state.itemData)
      ? props.location.state.itemData.Interval
      : null;
  props.setShowFooter(false);

  const videoNodeData = props.location.state.itemData;

  let notCurrentUserImage = null;

  // if (isOn == "professional") {
  //   notCurrentUserImage = user.JobSeeker.ProfilePicture != null && user.JobSeeker.ProfilePicture != "" ? user.JobSeeker.ProfilePicture : null
  // } else if (isOn == "freelancer")
  //   notCurrentUserImage = user.Freelancer.ProfilePicture != null && user.Freelancer.ProfilePicture != "" ? user.Freelancer.ProfilePicture : null

  // const notCurrentUserImage =
  // videoNodeData.JobSeekerId != null ?
  //   videoNodeData.JobSeeker.ProfilePicture != null && videoNodeData.JobSeeker.ProfilePicture != "" ?
  //     videoNodeData.JobSeeker.ProfilePicture : null
  //   :
  //   videoNodeData.JobSeekerId != null ?
  //     videoNodeData.Freelancer.ProfilePicture != null && videoNodeData.Freelancer.ProfilePicture != "" ?
  //       videoNodeData.Freelancer.ProfilePicture : null
  //     :
  //     null

  const getUserStream = async () => {
    const localStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true,
    });
    return localStream;
  };
  useEffect(async () => {
    const stream = await getUserStream();
    firepadRef
      .ref(videoNodeName)
      .child("participants")
      .once("value")
      .then((snap) => {
        if (snap.numChildren() < 2) {
          stream.getVideoTracks()[0].enabled = false;
          props.setMainStream(stream);
          connectedRef.on("value", (snap) => {
            if (snap.val()) {
              const defaultPreference = {
                audio: true,
                video: false,
                screen: false,
              };
              const userStatusRef = participantRef.push({
                userName,
                preferences: defaultPreference,
              });
              props.setUser({
                [userStatusRef.key]: { name: userName, ...defaultPreference },
              });
              userStatusRef.onDisconnect().remove();
            }
          });
        }
      });
  }, []);

  const connectedRef = firebase.database().ref(".info/connected");
  const participantRef = firepadRef.ref(videoNodeName).child("participants");

  const isUserSet = !!props.user;
  const isStreamSet = !!props.stream;

  useEffect(() => {
    if (isStreamSet && isUserSet) {
      firepadRef
        .ref(videoNodeName)
        .child("participants")
        .once("value")
        .then((snap) => {
          if (snap.numChildren() <= 2) {
            participantRef.on("child_added", (snap) => {
              const preferenceUpdateEvent = participantRef
                .child(snap.key)
                .child("preferences");
              preferenceUpdateEvent.on("child_changed", (preferenceSnap) => {
                props.updateParticipant({
                  [snap.key]: {
                    [preferenceSnap.key]: preferenceSnap.val(),
                  },
                });
              });
              const { userName: name, preferences = {} } = snap.val();
              props.addParticipant({
                [snap.key]: {
                  name,
                  ...preferences,
                },
              });
            });
            participantRef.on("child_removed", (snap) => {
              props.removeParticipant(snap.key);
            });
          }
        });
    }
  }, [isStreamSet, isUserSet]);

  useEffect(() => {
    if (Object.keys(props.participants).length == 2) {
      setTimeout(() => {
        onCallDisconnect();
      }, timeInterval * 60000);
    }
  }, [props.participants]);

  const onCallDisconnect = () => {
    participantRef.on("child_removed", (snap) => {
      props.removeParticipant(snap.key);
    });
    stopVideo();
  };

  const stopVideo = () => {
    let data = {
      Id: props.location.state.itemData.Id
    }
    stopVideoCallRequest(data).then((data) => {

      if (isOn === "professional")
        window.location.href = "/home-professional/planned-video-calls";
      else if (isOn === "company")
        window.location.href = "/home-company/planned-video-calls";
      else
        window.location.href = "/home-freelancer/planned-video-calls";
    }).catch((err) => {
      // console.log("err", err)
    })
  }

  useEffect(() => {
    window.addEventListener("beforeunload", handleUnload);
  });

  const handleUnload = (e) => {
    stopVideo();
    // changeStateOfUserOffline(user.FirebaseId);
  };

  return (
    <div className="App">
      <MainScreen
        onCallDisconnect={onCallDisconnect}
        timeInterval={timeInterval}
        notCurrentUserImage={notCurrentUserImage}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    stream: state.video.mainStream,
    user: state.video.currentUser,
    participants: state.video.participants,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setMainStream: (stream) => dispatch(setMainStream(stream)),
    addParticipant: (user) => dispatch(addParticipant(user)),
    setUser: (user) => dispatch(setUser(user)),
    removeParticipant: (userId) => dispatch(removeParticipant(userId)),
    updateParticipant: (user) => dispatch(updateParticipant(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainAppVideo);
