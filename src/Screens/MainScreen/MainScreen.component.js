import React, { useRef, useEffect, useState } from "react";
import MeetingFooter from "../MeetingFooter/MeetingFooter.component";
import Participants from "../Participants/Participants.component";
import "./MainScreen.css";
import { connect } from "react-redux";
import { setMainStream, updateUser } from "Redux/Actions/AppActions";

const MainScreen = (props) => {
  const participantRef = useRef(props.participants);

  const [counter, setCounter] = React.useState(props.timeInterval * 60);

  const onMicClick = (micEnabled) => {
    if (props.stream) {
      props.stream.getAudioTracks()[0].enabled = micEnabled;
      props.updateUser({ audio: micEnabled });
    }
  };
  const onVideoClick = (videoEnabled) => {
    if (props.stream) {
      props.stream.getVideoTracks()[0].enabled = videoEnabled;
      props.updateUser({ video: videoEnabled });
    }
  };

  const updateStream = (stream) => {

    for (let key in props.participants) {

      const sender = props.participants[key];
      if (sender.currentUser) continue;
      const peerConnection = sender.peerConnection
        .getSenders()
        .find((s) => (s.track ? s.track.kind === "video" : false));

      peerConnection.replaceTrack(stream.getVideoTracks()[0]);
    }
    props.setMainStream(stream);
  };

  const onScreenShareEnd = async () => {
    const localStream = await navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true,
    });
    localStream.getVideoTracks()[0].enabled = Object.values(
      props.currentUser
    )[0].video;

    updateStream(localStream);

    props.updateUser({ screen: false });
  };

  const onScreenClick = async () => {
    let mediaStream;

    if (navigator.getDisplayMedia) {

      mediaStream = await navigator.getDisplayMedia({ video: true });
    } else if (navigator.mediaDevices.getDisplayMedia) {

      mediaStream = await navigator.mediaDevices.getDisplayMedia({
        video: true,
      });
    } else {

      mediaStream = await navigator.mediaDevices.getUserMedia({
        video: { mediaSource: "screen" },
      });
    }

    mediaStream.getVideoTracks()[0].onended = onScreenShareEnd;
    updateStream(mediaStream);
    props.updateUser({ screen: true });
  };

  useEffect(() => {
    let timer;
    if (Object.keys(props.participants).length == 2) {
      if (counter > 0) {
        timer = setTimeout(() => setCounter((c) => c - 1), 1000);
      } else {
        props.onCallDisconnect();
      }
      return () => {
        if (timer) {
          clearTimeout(timer);
        }
      };
    } else {
      clearTimeout(timer);
    }
  }, [counter, props.participants]);

  const format = (time) => {
    // Convert seconds into hour
    const hours = Math.floor(time / 3600);
    time = time - hours * 3600;
    // Convert seconds into minutes and take the whole part
    const minutes = Math.floor(time / 60);
    time = time - minutes * 60;
    // Get the seconds left after converting minutes
    const seconds = time % 60;

    //Return combined values as string in format mm:ss
    return `${hours}:${minutes}:${padTime(seconds)}`;
  };

  const padTime = (time) => {
    return String(time).length === 1 ? `0${time}` : `${time}`;
  };

  return (
    <div className="wrapper">
      <div className="main-screen">
        <Participants notCurrentUserImage={props.notCurrentUserImage} />
        {counter === 0 ? (
          "Time over"
        ) : (
          <div
            style={{
              color: "white",
              display: "flex",
              justifyContent: "flex-end",
              marginRight: 20,
              fontSize: 25,
            }}
          >{`Timer:  ${format(counter)}`}</div>
        )}
      </div>

      {/* <div>
				{counter === 0 ? "Time over" : <div>Countdown: {format(counter)}</div>}
			</div> */}

      <div className="footerr">
        <MeetingFooter
          onScreenClick={onScreenClick}
          onMicClick={onMicClick}
          onVideoClick={onVideoClick}
          onCallDisconnect={props.onCallDisconnect}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    stream: state.video.mainStream,
    participants: state.video.participants,
    currentUser: state.video.currentUser,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setMainStream: (stream) => dispatch(setMainStream(stream)),
    updateUser: (user) => dispatch(updateUser(user)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
