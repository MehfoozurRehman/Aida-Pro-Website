import React, { useEffect, useState } from "react";
import { Avatar, Head } from "Components";
import PostingDetailsProfile from "./PostingDetailsProfile";
import PostingDetailsTab from "Components/PostingDetailsTab";
import Img from "react-cool-img";
import moment from "moment";
import NoData from "Components/NoData";
import { jobSekkerGetById } from "API/EmploymentAPI";
import { freelancerGetById } from "API/FreelancerApi";
import timeSvg from "../Assets/timeSvg.svg";
import locationSvg from "../Assets/locationSvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import {
  AdressSvg,
  ContactActive,
  ContactInactive,
  EducationActive,
  EducationInactive,
  EmailSvg,
  PhoneSvg,
  ProjectActive,
  ProjectInactive,
  SocialsSvg,
  UserActive,
  userInActive,
  WorkActive,
  workexperianceimg,
  WorkInactive,
} from "Assets";
import { getText } from "Utils/functions";

export default function PersonelDetailPreviewMessengerUser({
  from,
  isOn,
  location,
  setDontShowSidebar,
}) {
  // setDontShowSidebar(true);
  const [isOnProfile, setIsOnProfile] = useState(true);
  const [isOnWorkExperiance, setIsOnWorkExperiance] = useState(false);
  const [isOnProjectPortfolio, setIsOnProjectPortfolio] = useState(false);
  const [isOnEducation, setIsOnEducation] = useState(false);
  const [isOnContact, setIsOnContact] = useState(false);
  let [data, setData] = useState([]);
  const [experiences, setExperiences] = useState([]);
  const [educations, setEducations] = useState([]);
  const [skills, setSkills] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [certificates, setCertificates] = useState([]);
  const [selectedData, setSelectedData] = useState(null);
  const [languages, setLanguages] = useState([]);
  const [portfolios, setPortfolios] = useState([]);
  const [addresses, setAddresses] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    if (location.state.jobseekerId != null) {
      jobSekkerGetById(location.state.jobseekerId, "Employment")
        .then((data) => {
          if (data.data.success) {
            setData(data.data.result);
            bindDataJobseeker(data.data.result);
          }
        })
        .catch((error) => {
          setIsLoading(false);
        });
    } else {
      freelancerGetById(location.state.freelancerId, "Freelancer")
        .then((data) => {
          if (data.data.success) {
            setData(data.data.result);
            bindData(data.data.result);
          }
        })
        .catch((error) => {
          setIsLoading(false);
        });
    }
  }, []);

  const bindDataJobseeker = (data) => {
    let JobSeekerQualifications = [];
    if (data && data.JobSeekerQualifications) {
      data.JobSeekerQualifications.map((e) => {
        JobSeekerQualifications.push({
          Id: e.Id,
          JobSeekerId: location.state.jobseekerId,
          dateFrom: e.StartDate,
          dateTo: e.EndDate,
          institueName: e.Institute,
          educationType: {
            label: e.LookupDetail.Title,
            value: e.Id,
          },
        });
      });
    }

    let JobSeekerExperiences = [];
    if (data && data.JobSeekerExperiences) {
      data.JobSeekerExperiences &&
        data.JobSeekerExperiences.map((e) => {
          JobSeekerExperiences.push({
            Id: e.Id,
            jobCompany: e.CompanyName,
            jobDescription: e.Description,
            jobEmployementType: { value: e.EmploymentTypeLookupDetailId },
            jobIndustryType: { value: e.IndustryId },
            jobLocation: e.Location,
            jobTitle: e.Title,
            jobFrom: e.StartDate,
            jobTo: e.EndDate,
          });
        });
    }

    let skillsDTO = [];
    if (data && data.JobSeekerSkills) {
      data.JobSeekerSkills &&
        data.JobSeekerSkills.map((e) => {
          skillsDTO.push({
            label: e.Skill && e.Skill.Title,
            value: e.Skill && e.Id,
            Id: e.Id,
          });
        });
      setSkills(skillsDTO);
    }

    let certificateDTO = [];
    if (data && data.JobSeekerCertificates) {
      data.JobSeekerCertificates &&
        data.JobSeekerCertificates.map((e) => {
          certificateDTO.push({
            Id: e.Id,
            certificateName: e.CertificateName,
            JobSeekerId: location.state.jobseekerId,
            dateFrom: e.StartDate,
            dateTo: e.EndDate,
            institueName: e.Institute,
          });
        });
    }

    let languageDTO = [];
    if (data && data.JobSeekerLanguages) {
      languageDTO = data.JobSeekerLanguages;
    }

    let portfoliosDTO = [];
    if (data && data.JobSeekerPortfolios) {
      data.JobSeekerPortfolios &&
        data.JobSeekerPortfolios.map((e) => {
          portfoliosDTO.push({
            Id: e.Id,
            ProjectName: e.ProjectName,
            ProjectDescription: e.ProjectDescription,
            UserId: e.JobSeekerId,
            CreatedById: e.CreatedById,
            ActionTypeId: e.ActionTypeId,
            CreatedOn: e.CreatedOn,
            Uploads: e.JobSeekerUploads,
          });
        });
    }

    let addressDTO = [];
    if (data && data.JobSeekerAddresses) {
      addressDTO = data.JobSeekerAddresses;
    }

    setEducations(JobSeekerQualifications);
    setExperiences(JobSeekerExperiences);
    setCertificates(certificateDTO);
    setLanguages(languageDTO);
    setSkills(skillsDTO);
    setSelectedData(data);
    setPortfolios(portfoliosDTO);
    setAddresses(addressDTO);
    setIsLoading(false);
  };

  const bindData = (data) => {
    let JobSeekerQualifications = [];
    if (data && data.FreelancerQualifications) {
      data.FreelancerQualifications.map((e) => {
        JobSeekerQualifications.push({
          Id: e.Id,
          JobSeekerId: location.state.jobseekerId,
          dateFrom: e.StartDate,
          dateTo: e.EndDate,
          institueName: e.Institute,
          educationType: {
            label: e.DegreeLookupDetail.Title,
            value: e.Id,
          },
        });
      });
    }

    let JobSeekerExperiences = [];
    if (data && data.FreelancerExperiences) {
      data.FreelancerExperiences &&
        data.FreelancerExperiences.map((e) => {
          JobSeekerExperiences.push({
            Id: e.Id,
            jobCompany: e.CompanyName,
            jobDescription: e.Description,
            jobEmployementType: { value: e.EmploymentTypeLookupDetailId },
            jobIndustryType: { value: e.IndustryId },
            jobLocation: e.Location,
            jobTitle: e.Title,
            jobFrom: e.StartDate,
            jobTo: e.EndDate,
          });
        });
    }

    let skillsDTO = [];
    if (data && data.FreelancerSkills) {
      data.FreelancerSkills &&
        data.FreelancerSkills.map((e) => {
          skillsDTO.push({
            label: e.Skill && e.Skill.Title,
            value: e.Skill && e.Id,
            Id: e.Id,
          });
        });
      setSkills(skillsDTO);
    }

    let certificateDTO = [];
    if (data && data.FreelancerCertificates) {
      data.FreelancerCertificates &&
        data.FreelancerCertificates.map((e) => {
          certificateDTO.push({
            Id: e.Id,
            certificateName: e.CertificateName,
            JobSeekerId: location.state.jobseekerId,
            dateFrom: e.StartDate,
            dateTo: e.EndDate,
            institueName: e.Institute,
          });
        });
    }

    let languageDTO = [];
    if (data && data.FreelancerLanguages) {
      languageDTO = data.FreelancerLanguages;
    }

    let portfoliosDTO = [];
    if (data && data.FreelancerPortfolios) {
      data.FreelancerPortfolios &&
        data.FreelancerPortfolios.map((e) => {
          portfoliosDTO.push({
            Id: e.Id,
            ProjectName: e.ProjectName,
            ProjectDescription: e.ProjectDescription,
            UserId: e.FreelancerId,
            CreatedById: e.CreatedById,
            ActionTypeId: e.ActionTypeId,
            CreatedOn: e.CreatedOn,
            Uploads: e.FreelancerUploads,
          });
        });
    }

    let addressDTO = [];
    if (data && data.FreelancerAddresses) {
      addressDTO = data.FreelancerAddresses;
    }

    setEducations(JobSeekerQualifications);
    setExperiences(JobSeekerExperiences);
    setCertificates(certificateDTO);
    setLanguages(languageDTO);
    setSkills(skillsDTO);
    setSelectedData(data);
    setPortfolios(portfoliosDTO);
    setAddresses(addressDTO);
    setIsLoading(false);
  };

  const WorkExperience = ({ data }) => {
    return (
      <button
        className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry"
        title="edit work experiance"
      >
        <Img
          loading="lazy"
          src={workexperianceimg}
          alt="workexperianceimg"
          className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__img"
        />
        <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info">
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__heading">
            {data ? data.jobTitle : "Title"}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__duration"></div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__designation">
            {data ? data.CompanyName : null}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__date">
            {data
              ? moment(data.jobFrom).format("MMMM DD, YYYY")
              : "Not specified"}
            {data
              ? data.jobTo != null
                ? ` - ${moment(data.jobTo).format("MMMM DD, YYYY")}`
                : "Continue"
              : "Not specified"}
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__content">
          {getText(data.jobDescription).length > 1000 ? (
            <>{`${getText(data.jobDescription).substring(0, 1000)}...`}</>
          ) : (
            <>{getText(data.jobDescription)}</>
          )}
        </div>
      </button>
    );
  };
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <>
      <Head title="AIDApro | Posting Details" description="Posting Details" />
      <div className="posting__details__container">
        {/* {from === "profile" ? null : (
              <div
                className="posting__details__container__back__link"
                onClick={() => goBack()}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="7.639"
                  height="13.363"
                  style={{ marginRight: "1em" }}
                  viewBox="0 0 7.639 13.363"
                >
                  <path
                    id="Icon_ionic-ios-arrow-back"
                    data-name="Icon ionic-ios-arrow-back"
                    d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                    transform="translate(-11.251 -6.194)"
                  />
                </svg>
                
                Back
              </div>
            )} */}
        <div
          className="homepage__container__jobs__projects__penel"
          style={{ margin: "0em" }}
        >
          <div
            className="homepage__container__jobs__projects__penel__container"
            style={{ margin: "0em" }}
          >
            <div
              className="homepage__container__jobs__projects__penel__container__details"
              style={from === "profile" ? { width: "100%" } : null}
            >
              <div className="homepage__container__jobs__projects__penel__container__details__heading">
                <Avatar
                  onClick={(event) => {
                    window.open(
                      process.env.REACT_APP_BASEURL.concat(data.ProfilePicture),
                      "_Blank"
                    );
                  }}
                  userPic={
                    data.ProfilePicture != null && data.ProfilePicture != ""
                      ? process.env.REACT_APP_BASEURL.concat(
                          data.ProfilePicture
                        )
                      : null
                  }
                />
                {data.FirstName + " " + data.LastName}
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__feature">
                <div className="homepage__container__result__card__content">
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={timeSvg}
                      alt="timeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {data.AvailabilityLookupDetail != null
                        ? data.AvailabilityLookupDetail.Title
                        : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={locationSvg}
                      alt="locationSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {addresses != null && addresses.length > 0
                        ? addresses[0].AddressDetail
                        : "Not specified"}
                    </div>
                  </div>

                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={experienceSvg}
                      alt="experienceSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {data.JobSeekerExperiences != null &&
                      data.JobSeekerExperiences.length > 0
                        ? data.JobSeekerExperiences[0].Industry != null
                          ? data.JobSeekerExperiences[0].Industry.Title
                          : data.FreelancerExperiences != null &&
                            data.FreelancerExperiences.length > 0
                          ? data.FreelancerExperiences[0].Industry != null
                            ? data.FreelancerExperiences[0].Industry.Title
                            : "Not specified"
                          : "Not specified"
                        : "Not specfied"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={experienceSvg}
                      alt="experienceSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {data.TotalExperience} Years of Experience
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="homepage__container__jobs__projects__penel__container__details__tabs"
                id="homepage__container__jobs__projects__penel__container__details__tabs"
              >
                <PostingDetailsTab
                  label="Profile"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(true);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={
                    <Img loading="lazy" src={UserActive} alt="UserActive" />
                  }
                  inActiveSvg={
                    <Img loading="lazy" src={userInActive} alt="userInActive" />
                  }
                  defaultChecked={true}
                  isChecked={isOnProfile}
                />
                <PostingDetailsTab
                  label="Work Experience"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(true);
                  }}
                  activeSvg={<Img loading="lazy" src={WorkActive} />}
                  inActiveSvg={<Img loading="lazy" src={WorkInactive} />}
                  isChecked={isOnWorkExperiance}
                />
                <PostingDetailsTab
                  label="Project Portfolio"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(true);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={ProjectActive} />}
                  inActiveSvg={<Img loading="lazy" src={ProjectInactive} />}
                  isChecked={isOnProjectPortfolio}
                />
                <PostingDetailsTab
                  label="Education"
                  onClick={() => {
                    setIsOnContact(false);
                    setIsOnEducation(true);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={EducationActive} />}
                  inActiveSvg={<Img loading="lazy" src={EducationInactive} />}
                  isChecked={isOnEducation}
                />
                <PostingDetailsTab
                  label="Contact"
                  onClick={() => {
                    setIsOnContact(true);
                    setIsOnEducation(false);
                    setIsOnProfile(false);
                    setIsOnProjectPortfolio(false);
                    setIsOnWorkExperiance(false);
                  }}
                  activeSvg={<Img loading="lazy" src={ContactActive} />}
                  inActiveSvg={<Img loading="lazy" src={ContactInactive} />}
                  isChecked={isOnContact}
                />
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {isOnProfile ? (
                  <PostingDetailsProfile
                    data={data}
                    skills={skills}
                    languages={languages}
                  />
                ) : isOnWorkExperiance ? (
                  <>
                    <Head
                      title="AIDApro | Posting Details - Experience"
                      description="Posting Details - Experience"
                    />
                    <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance">
                      {experiences.length === 0 ? (
                        <NoData text="No Work Experience" />
                      ) : (
                        experiences.map((experience, i) => (
                          <WorkExperience key={i} data={experience} />
                        ))
                      )}
                    </div>
                  </>
                ) : isOnProjectPortfolio ? (
                  <>
                    <Head
                      title="AIDApro | Posting Details - Portfolio"
                      description="Posting Details - Portfolio"
                    />
                    <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio">
                      {portfolios.length === 0 ? (
                        <NoData text="No Project" />
                      ) : (
                        portfolios.map((item, i) => (
                          <div
                            className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry"
                            key={i}
                          >
                            <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__heading">
                              {item.ProjectName}
                            </div>
                            <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__content">
                              {getText(item.ProjectDescription).length >
                              1000 ? (
                                <>{`${getText(
                                  item.ProjectDescription
                                ).substring(0, 1000)}...`}</>
                              ) : (
                                <>{getText(item.ProjectDescription)}</>
                              )}
                            </div>
                            <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__images">
                              {item.Uploads.map((item, i) => (
                                <Img
                                  loading="lazy"
                                  src={process.env.REACT_APP_BASEURL.concat(
                                    item.UploadFilePath
                                  )}
                                  key={i}
                                  alt="portfolioImg"
                                  className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio__entry__images__entry"
                                />
                              ))}
                            </div>
                          </div>
                        ))
                      )}
                    </div>
                  </>
                ) : isOnEducation ? (
                  <>
                    <Head
                      title="AIDApro | Posting Details - Education"
                      description="Posting Details - Education"
                    />
                    <div className="homepage__container__jobs__projects__penel__container__details__content__education">
                      <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry">
                        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__header">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="43.49"
                            height="34"
                            style={{ marginRight: "1em" }}
                            viewBox="0 0 43.49 34"
                          >
                            <defs>
                              <linearGradient
                                id="linear-gradient"
                                x1="0.5"
                                x2="0.5"
                                y2="1"
                                gradientUnits="objectBoundingBox"
                              >
                                <stop offset="0" stopColor="#f6a938" />
                                <stop offset="1" stopColor="#f5833c" />
                              </linearGradient>
                              <linearGradient
                                id="linear-gradient-6"
                                x1="0.5"
                                x2="0.5"
                                y2="1"
                                gradientUnits="objectBoundingBox"
                              >
                                <stop offset="0" stopColor="#fff" />
                                <stop offset="1" stopColor="#f5833c" />
                              </linearGradient>
                            </defs>
                            <g
                              id="degrees"
                              transform="translate(-19.758 -25.832)"
                            >
                              <g
                                id="Group_1490"
                                data-name="Group 1490"
                                transform="translate(19.758 25.832)"
                              >
                                <g
                                  id="Group_1489"
                                  data-name="Group 1489"
                                  transform="translate(0 0)"
                                >
                                  <g id="Group_1488" data-name="Group 1488">
                                    <g
                                      id="Group_1472"
                                      data-name="Group 1472"
                                      transform="translate(7.121 15.525)"
                                    >
                                      <g id="Group_1471" data-name="Group 1471">
                                        <g
                                          id="Group_1470"
                                          data-name="Group 1470"
                                        >
                                          <path
                                            id="Path_21312"
                                            data-name="Path 21312"
                                            d="M82.923,116.121l-12.081,5.253a2.543,2.543,0,0,1-2.028,0l-12.081-5.253a2.543,2.543,0,0,1-1.529-2.333V103.11H84.452v10.679a2.544,2.544,0,0,1-1.529,2.333Z"
                                            transform="translate(-55.203 -103.11)"
                                            fill="url(#linear-gradient)"
                                          />
                                        </g>
                                      </g>
                                    </g>
                                    <g
                                      id="Group_1473"
                                      data-name="Group 1473"
                                      transform="translate(7.121 24.194)"
                                    >
                                      <path
                                        id="Path_21313"
                                        data-name="Path 21313"
                                        d="M82.923,148.6l-12.081,5.253a2.543,2.543,0,0,1-2.028,0L56.732,148.6a2.543,2.543,0,0,1-1.529-2.333v2.009a2.544,2.544,0,0,0,1.529,2.333l12.081,5.253a2.543,2.543,0,0,0,2.028,0l12.081-5.253a2.543,2.543,0,0,0,1.529-2.333v-2.009A2.544,2.544,0,0,1,82.923,148.6Z"
                                        transform="translate(-55.203 -146.266)"
                                        fill="url(#linear-gradient)"
                                      />
                                    </g>
                                    <g id="Group_1476" data-name="Group 1476">
                                      <g id="Group_1475" data-name="Group 1475">
                                        <g
                                          id="Group_1474"
                                          data-name="Group 1474"
                                        >
                                          <path
                                            id="Path_21314"
                                            data-name="Path 21314"
                                            d="M40.465,47.863l-19.2-8.583a2.543,2.543,0,0,1,0-4.644l19.2-8.583a2.543,2.543,0,0,1,2.076,0l19.2,8.583a2.543,2.543,0,0,1,0,4.644l-19.2,8.583a2.543,2.543,0,0,1-2.076,0Z"
                                            transform="translate(-19.758 -25.832)"
                                            fill="url(#linear-gradient)"
                                          />
                                        </g>
                                      </g>
                                    </g>
                                    <g
                                      id="Group_1478"
                                      data-name="Group 1478"
                                      transform="translate(6.718 0.803)"
                                    >
                                      <g id="Group_1477" data-name="Group 1477">
                                        <path
                                          id="Path_21315"
                                          data-name="Path 21315"
                                          d="M53.6,37.044a.4.4,0,0,1-.163-.769l14.079-6.292a1.716,1.716,0,0,1,.707-.153h0a1.716,1.716,0,0,1,.707.153l3.236,1.444a.4.4,0,0,1,.2.532.415.415,0,0,1-.532.2l-3.234-1.446a.926.926,0,0,0-.763,0L53.769,37.008h0a.386.386,0,0,1-.165.036Zm19.872-4.193a.418.418,0,0,1-.163-.034.4.4,0,0,1-.211-.223.389.389,0,0,1,.008-.307.408.408,0,0,1,.53-.2.4.4,0,0,1,.2.53.4.4,0,0,1-.368.237Z"
                                          transform="translate(-53.2 -29.83)"
                                          fill="url(#linear-gradient)"
                                        />
                                      </g>
                                    </g>
                                    <g
                                      id="Group_1479"
                                      data-name="Group 1479"
                                      transform="translate(0.001 9.067)"
                                    >
                                      <path
                                        id="Path_21316"
                                        data-name="Path 21316"
                                        d="M62.2,70.963a2.627,2.627,0,0,1-.456.262l-19.2,8.583a2.543,2.543,0,0,1-2.076,0l-19.2-8.583a2.64,2.64,0,0,1-.456-.262,2.545,2.545,0,0,0,.456,4.382l19.2,8.583a2.543,2.543,0,0,0,2.076,0l19.2-8.583A2.545,2.545,0,0,0,62.2,70.963Z"
                                        transform="translate(-19.762 -70.963)"
                                        fill="url(#linear-gradient)"
                                      />
                                    </g>
                                    <g
                                      id="Group_1482"
                                      data-name="Group 1482"
                                      transform="translate(2.306 8.636)"
                                    >
                                      <g id="Group_1481" data-name="Group 1481">
                                        <g
                                          id="Group_1480"
                                          data-name="Group 1480"
                                        >
                                          <path
                                            id="Path_21317"
                                            data-name="Path 21317"
                                            d="M31.871,86.094a.636.636,0,0,1-.636-.636V74.274a.635.635,0,0,1,.478-.616l18.8-4.819a.636.636,0,0,1,.316,1.232l-18.325,4.7v10.69a.636.636,0,0,1-.636.636Z"
                                            transform="translate(-31.235 -68.819)"
                                            fill="url(#linear-gradient-6)"
                                          />
                                        </g>
                                      </g>
                                    </g>
                                    <g
                                      id="Group_1485"
                                      data-name="Group 1485"
                                      transform="translate(0.948 21.194)"
                                    >
                                      <g
                                        id="Group_1484"
                                        data-name="Group 1484"
                                        transform="translate(0 0)"
                                      >
                                        <g
                                          id="Group_1483"
                                          data-name="Group 1483"
                                        >
                                          <path
                                            id="Path_21318"
                                            data-name="Path 21318"
                                            d="M27.641,137.056H25.3a.824.824,0,0,1-.824-.824v-2.906a1.993,1.993,0,1,1,3.986,0v2.906A.824.824,0,0,1,27.641,137.056Z"
                                            transform="translate(-24.479 -131.333)"
                                            fill="url(#linear-gradient)"
                                          />
                                        </g>
                                      </g>
                                    </g>
                                    <g
                                      id="Group_1486"
                                      data-name="Group 1486"
                                      transform="translate(0.948 21.194)"
                                    >
                                      <path
                                        id="Path_21319"
                                        data-name="Path 21319"
                                        d="M26.471,131.333h-.016v2.889a.824.824,0,0,1-.824.824H24.478v1.185a.824.824,0,0,0,.824.824H27.64a.824.824,0,0,0,.824-.824v-2.906A1.993,1.993,0,0,0,26.471,131.333Z"
                                        transform="translate(-24.478 -131.333)"
                                        fill="url(#linear-gradient)"
                                      />
                                    </g>
                                    <g
                                      id="Group_1487"
                                      data-name="Group 1487"
                                      transform="translate(0.948 21.369)"
                                    >
                                      <path
                                        id="Path_21320"
                                        data-name="Path 21320"
                                        d="M27.286,132.2a1.983,1.983,0,0,1,.175.814v2.906a.824.824,0,0,1-.824.824H24.479v.18a.824.824,0,0,0,.824.824h2.338a.824.824,0,0,0,.824-.824v-2.906A1.992,1.992,0,0,0,27.286,132.2Z"
                                        transform="translate(-24.479 -132.204)"
                                        fill="url(#linear-gradient)"
                                      />
                                    </g>
                                  </g>
                                </g>
                              </g>
                            </g>
                          </svg>
                          Degrees
                        </div>
                        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content">
                          {educations.length === 0 ? (
                            <NoData text="No Degrees" />
                          ) : (
                            educations.map((education, i) => (
                              <button
                                title="education"
                                className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__entry"
                                key={i}
                              >
                                <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__heading">
                                  {education ? education.FieldOfStudy : "Title"}
                                </div>
                                <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__sub__heading">
                                  {education
                                    ? education.institueName
                                    : "Institue"}
                                </div>
                                <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__date">
                                  {education
                                    ? moment(education.dateFrom).format(
                                        "MMMM DD, YYYY"
                                      )
                                    : "Start Date"}{" "}
                                  -{" "}
                                  {education
                                    ? moment(education.dateTo).format(
                                        "MMMM DD, YYYY"
                                      )
                                    : "End Date"}
                                </div>
                              </button>
                            ))
                          )}
                        </div>
                      </div>
                      <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry">
                        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__header">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            style={{ marginRight: "1em" }}
                            width="42.586"
                            height="48.766"
                            viewBox="0 0 42.586 48.766"
                          >
                            <defs>
                              <linearGradient
                                id="linear-gradient"
                                x1="0.5"
                                x2="0.5"
                                y2="1"
                                gradientUnits="objectBoundingBox"
                              >
                                <stop offset="0" stopColor="#f6a938" />
                                <stop offset="1" stopColor="#f5833c" />
                              </linearGradient>
                            </defs>
                            <g
                              id="Page-1"
                              transform="matrix(0.574, -0.819, 0.819, 0.574, -25.423, 19.349)"
                            >
                              <g
                                id="_001---Degree"
                                data-name="001---Degree"
                                transform="translate(-0.018 31.049)"
                              >
                                <path
                                  id="Shape"
                                  d="M26.271,32.605a17.061,17.061,0,0,1,.584,3.789,5.621,5.621,0,0,0-5.639.524A21.885,21.885,0,0,0,20.66,32.6q1.472.031,3.019.031C24.565,32.629,25.424,32.62,26.271,32.605ZM24.489,45.591a4.05,4.05,0,1,1,4.05-4.05A4.05,4.05,0,0,1,24.489,45.591ZM1,38.3a22.721,22.721,0,0,1,.81-6.886,22.721,22.721,0,0,1,.81,6.886,22.721,22.721,0,0,1-.81,6.886A22.72,22.72,0,0,1,1,38.3Zm2.449,7.024A26.865,26.865,0,0,0,4.237,38.3a26.871,26.871,0,0,0-.791-7.025,142.134,142.134,0,0,0,15.522,1.282,20.853,20.853,0,0,1,.661,5.743c0,.117,0,.229,0,.342a5.612,5.612,0,0,0-.261,5.308l-.036.086A141.177,141.177,0,0,0,3.445,45.324Zm18.133,5.937L20.987,49.9a.81.81,0,0,0-1.062-.421l-1.363.6,1.9-4.554a5.665,5.665,0,0,0,2.861,1.563Zm7.475-1.782a.81.81,0,0,0-1.062.421l-.6,1.347L25.655,47.09a5.671,5.671,0,0,0,2.864-1.564l1.9,4.537ZM45.39,45.55a131.2,131.2,0,0,0-15.716-1.458l-.058-.139a5.637,5.637,0,0,0-1.092-6.388,22.548,22.548,0,0,0-.578-4.991A135.222,135.222,0,0,0,45.39,31.049c.376.638.972,3.163.972,7.251S45.761,44.912,45.39,45.55Z"
                                  transform="translate(-0.982 -31.049)"
                                  fill="url(#linear-gradient)"
                                />
                              </g>
                            </g>
                          </svg>
                          Certifications
                        </div>
                        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content">
                          {certificates.length === 0 ? (
                            <NoData text="No Certificates" />
                          ) : (
                            certificates.map(
                              (certificate, certificateIndex) => (
                                <button
                                  className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__entry"
                                  title="certificate"
                                >
                                  <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__heading">
                                    {certificate
                                      ? certificate.certificateName
                                      : "Not specified"}
                                  </div>
                                  <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content__date">
                                    {certificate
                                      ? moment(certificate.dateFrom).format(
                                          "MMMM DD, YYYY"
                                        )
                                      : "Not specified"}{" "}
                                    {certificate
                                      ? certificate.EndDate != null
                                        ? ` - ${moment(
                                            certificate.dateTo
                                          ).format("MMMM DD, YYYY")}`
                                        : "Continue"
                                      : "Not specified"}
                                  </div>
                                </button>
                              )
                            )
                          )}
                        </div>
                      </div>
                    </div>
                  </>
                ) : isOnContact ? (
                  <>
                    <Head
                      title="AIDApro | Posting Details - Contact"
                      description="Posting Details - Contact"
                    />
                    <div className="homepage__container__jobs__projects__penel__container__details__content__contact">
                      <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
                        <Img loading="lazy" src={EmailSvg} alt="EmailSvg" />
                        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
                            Email
                          </div>
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
                            {data != null
                              ? addresses != null && addresses.length > 0
                                ? addresses[0].Email
                                : "Not specified"
                              : "Not specified"}
                          </div>
                        </div>
                      </div>
                      <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
                        <Img loading="lazy" src={PhoneSvg} alt="PhoneSvg" />

                        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
                            Phone
                          </div>
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
                            {data != null
                              ? addresses != null && addresses.length > 0
                                ? "+" + addresses[0].PhoneNo
                                : "Not specified"
                              : "Not specified"}
                          </div>
                        </div>
                      </div>
                      <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
                        <Img loading="lazy" src={AdressSvg} alt="AdressSvg" />

                        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
                            Address
                          </div>
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__info">
                            {data != null
                              ? addresses != null && addresses.length > 0
                                ? addresses[0].AddressDetail
                                : "Not specified"
                              : "Not specified"}
                          </div>
                        </div>
                      </div>
                      <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry">
                        <Img loading="lazy" src={SocialsSvg} alt="SocialsSvg" />
                        <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content">
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__heading">
                            Socials
                          </div>
                          <div className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__links">
                            <a
                              href={
                                data != null
                                  ? data.FacebookProfile != null
                                    ? data.FacebookProfile
                                    : ""
                                  : ""
                              }
                              className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                              target="_Blank"
                            >
                              <svg
                                id="facebook_2_"
                                data-name="facebook (2)"
                                xmlns="http://www.w3.org/2000/svg"
                                width="25.426"
                                height="25.426"
                                viewBox="0 0 25.426 25.426"
                              >
                                <path
                                  id="Path_659"
                                  data-name="Path 659"
                                  d="M22.248,0H3.178A3.181,3.181,0,0,0,0,3.178V22.247a3.181,3.181,0,0,0,3.178,3.178H22.248a3.181,3.181,0,0,0,3.178-3.178V3.178A3.181,3.181,0,0,0,22.248,0Z"
                                  fill="#1976d2"
                                />
                                <path
                                  id="Path_660"
                                  data-name="Path 660"
                                  d="M203.918,103.946h-3.973v-3.178c0-.877.712-.795,1.589-.795h1.589V96h-3.178a4.767,4.767,0,0,0-4.767,4.767v3.178H192v3.973h3.178v8.74h4.767v-8.74h2.384Z"
                                  transform="translate(-182.465 -91.233)"
                                  fill="#fafafa"
                                />
                              </svg>
                            </a>
                            <a
                              href={
                                data != null
                                  ? data.GoogleProfile != null
                                    ? data.GoogleProfile
                                    : ""
                                  : ""
                              }
                              className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                              target="_Blank"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="25.426"
                                height="25.426"
                                viewBox="0 0 25.426 25.426"
                              >
                                <g id="google" transform="translate(-0.001)">
                                  <path
                                    id="Path_661"
                                    data-name="Path 661"
                                    d="M278.331,211.48H267.96a.829.829,0,0,0-.829.829v3.313a.829.829,0,0,0,.829.829h5.84a7.8,7.8,0,0,1-3.356,3.933l2.49,4.311a12.465,12.465,0,0,0,6.356-10.9,8.406,8.406,0,0,0-.143-1.628A.833.833,0,0,0,278.331,211.48Z"
                                    transform="translate(-253.865 -200.977)"
                                    fill="#167ee6"
                                  />
                                  <path
                                    id="Path_662"
                                    data-name="Path 662"
                                    d="M45.415,337.719a7.737,7.737,0,0,1-6.693-3.872l-4.311,2.485a12.7,12.7,0,0,0,17.361,4.659v-.006l-2.49-4.311A7.681,7.681,0,0,1,45.415,337.719Z"
                                    transform="translate(-32.701 -317.268)"
                                    fill="#12b347"
                                  />
                                  <path
                                    id="Path_663"
                                    data-name="Path 663"
                                    d="M262.356,395.1v-.006l-2.49-4.311A7.68,7.68,0,0,1,256,391.824V396.8A12.691,12.691,0,0,0,262.356,395.1Z"
                                    transform="translate(-243.286 -371.373)"
                                    fill="#0f993e"
                                  />
                                  <path
                                    id="Path_664"
                                    data-name="Path 664"
                                    d="M4.975,134.47A7.681,7.681,0,0,1,6.02,130.6l-4.311-2.485a12.655,12.655,0,0,0,0,12.7l4.311-2.485A7.681,7.681,0,0,1,4.975,134.47Z"
                                    transform="translate(0.001 -121.757)"
                                    fill="#ffd500"
                                  />
                                  <path
                                    id="Path_665"
                                    data-name="Path 665"
                                    d="M45.415,4.975a7.7,7.7,0,0,1,4.913,1.764.826.826,0,0,0,1.112-.05l2.347-2.347a.835.835,0,0,0-.048-1.222A12.685,12.685,0,0,0,34.411,6.362l4.311,2.485A7.737,7.737,0,0,1,45.415,4.975Z"
                                    transform="translate(-32.701 0)"
                                    fill="#ff4b26"
                                  />
                                  <path
                                    id="Path_666"
                                    data-name="Path 666"
                                    d="M260.913,6.738a.826.826,0,0,0,1.112-.05l2.347-2.347a.835.835,0,0,0-.048-1.222A12.675,12.675,0,0,0,256,0V4.975A7.7,7.7,0,0,1,260.913,6.738Z"
                                    transform="translate(-243.286 0)"
                                    fill="#d93f21"
                                  />
                                </g>
                              </svg>
                            </a>
                            <a
                              href={
                                data != null
                                  ? data.LinkedInProfile != null
                                    ? data.LinkedInProfile
                                    : ""
                                  : ""
                              }
                              className="homepage__container__jobs__projects__penel__container__details__content__contact__entry__content__link"
                              target="_Blank"
                            >
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="25.426"
                                height="25.426"
                                viewBox="0 0 25.426 25.426"
                              >
                                <path
                                  id="linkedin"
                                  d="M23.126,0H2.3A2.3,2.3,0,0,0,0,2.3V23.126a2.3,2.3,0,0,0,2.3,2.3H23.126a2.3,2.3,0,0,0,2.3-2.3V2.3A2.3,2.3,0,0,0,23.126,0ZM7.868,21.954a.669.669,0,0,1-.669.669H4.349a.669.669,0,0,1-.669-.669V10.011a.669.669,0,0,1,.669-.669H7.2a.669.669,0,0,1,.669.669ZM5.774,8.216A2.707,2.707,0,1,1,8.481,5.509,2.707,2.707,0,0,1,5.774,8.216ZM22.758,22.008a.615.615,0,0,1-.615.615H19.085a.615.615,0,0,1-.615-.615v-5.6c0-.836.245-3.662-2.184-3.662-1.884,0-2.266,1.935-2.343,2.8v6.462a.615.615,0,0,1-.615.615H10.37a.615.615,0,0,1-.615-.615V9.957a.615.615,0,0,1,.615-.615h2.957a.615.615,0,0,1,.615.615V11A4.2,4.2,0,0,1,17.89,9.141c4.9,0,4.868,4.574,4.868,7.087v5.78Z"
                                  fill="#0077b7"
                                />
                              </svg>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
