import React, { useEffect, useState } from "react";
import Header from "Components/Header";
import { Head, Avatar, BlogCard } from "Components";
import Img from "react-cool-img";
import { getBlog, getBlogById } from "API/Blogs";
import moment from "moment";
import { getText } from "Utils/functions";

function BlogArtical(props) {
  let blogId = props.location.state.blogId;
  const [blogDataById, setBlogDataById] = useState(null);
  const [blogApiData, setBlogApiData] = useState([]);

  useEffect(() => {
    fetchBlogById();
  }, [blogId]);

  useEffect(() => {
    getBlogsDataApiCall();
  }, []);

  const fetchBlogById = () => {
    getBlogById(blogId)
      .then((data) => {
        setBlogDataById(data.data.result);
      })
      .catch((err) => {
        // console.log("err", err);
      });
  };

  const getBlogsDataApiCall = () => {
    getBlog(3, 1).then((res) => {
      if (res.data.errors === null) {
        setBlogApiData(res.data.result);
      }
    });
  };

  const showTrendingBlogs = (data) => {};

  return (
    <>
      <Head title="AIDApro | Blog Artical" description="Blog Artical" />
      <Header isOnHomeCompany={false} />
      <div className="homepage__container">
        <div className="homepage__blog__artical__wrapper">
          <div className="homepage__blog__artical__main__background">
            <Img
              loading="lazy"
              src={
                blogDataById != null
                  ? process.env.REACT_APP_BASEURL + blogDataById.Media
                  : null
              }
              alt="blogArtical"
              style={{ width: "100%", height: "100%", borderRadius: 10 }}
            />
          </div>
          <div className="homepage__blog__artical__author__container">
            <div className="homepage__blog__artical__author__container__userpic">
              <Avatar
                userPic={
                  blogDataById != null
                    ? process.env.REACT_APP_BASEURL + blogDataById.AuthorImage
                    : null
                }
              />
            </div>
            <div className="homepage__blog__artical__author__container__content">
              <div className="homepage__blog__artical__author__container__content__name">
                {blogDataById != null ? blogDataById.Author : "Not specified"}
              </div>
              <div className="homepage__blog__artical__author__container__content__date">
                {blogDataById != null
                  ? moment(blogDataById.CreatedOn).fromNow()
                  : "Not specified"}
              </div>
            </div>
          </div>
        </div>
        <div className="homepage__blog__artical__content__container">
          <div className="homepage__blog__artical__content">
            <div className="homepage__blog__artical__content__heading">
              {blogDataById != null ? blogDataById.Title : "Not specified"}
            </div>
            {blogDataById != null ? (
              <div className="homepage__blog__artical__content__para">
                {getText(blogDataById.Description).length > 1000 ? (
                  <>{`${getText(blogDataById.Description).substring(
                    0,
                    1000
                  )}...`}</>
                ) : (
                  <>{getText(blogDataById.Description)}</>
                )}
              </div>
            ) : null}
          </div>
        </div>
        <div className="homepage__container__blog">
          <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
            Trending <span>Blogs</span>
          </div>
        </div>
        <div className="homepage__container__blog__cards">
          {blogApiData.length > 0
            ? blogApiData.map((item, i) => (
                <BlogCard
                  key={i}
                  id={item.Id}
                  blogData={item}
                  onClick={showTrendingBlogs}
                />
              ))
            : null}
        </div>
      </div>
    </>
  );
}
export default BlogArtical;
