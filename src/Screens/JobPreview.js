import React, { useEffect } from "react";
import { Head } from "Components";
import moment from "moment";
import { VIEW_DATE_FORMAT } from "Utils/Constants";
import { crossSvg } from "Assets";
import timeSvg from "../Assets/timeSvg.svg";
import milesSvg from "../Assets/milesSvg.svg";
import moneySvg from "../Assets/moneySvg.svg";
import { getText } from "Utils/functions";
import parse from "html-react-parser";
import Img from "react-cool-img";

export default function JobPreview({ setIsJobPreviewOpen, data }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <>
      <Head title="AIDApro | Job Preview" description="Job Preview" />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsJobPreviewOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__preview">
            <div className="pupup__container__from__preview__header">
              Preview
            </div>
            <div className="pupup__container__from__preview__heading">
              <span>{data ? data.title : "No title"}</span>
              {/* <button title="edit" className="header__nav__btn btn__secondary">Edit</button> */}
            </div>
            <div className="homepage__container__jobs__projects__penel__container__details__feature">
              <div className="homepage__container__result__card__content">
                {data.type === "job" ? (
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={timeSvg}
                      alt="timeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {data
                        ? data.jobType.length > 0
                          ? data.jobType
                              .map((item, index) => item.label)
                              .join(", ")
                          : "Available"
                        : "Available"}
                      {/* {data ? data.jobType.label : "Not"} Available */}
                    </div>
                  </div>
                ) : (
                  <>
                    <div className="homepage__container__result__card__content__entry">
                      <Img
                        loading="lazy"
                        src={timeSvg}
                        alt="timeSvg"
                        className="homepage__container__result__card__content__entry__svg"
                      />
                      <div className="homepage__container__result__card__content__entry__text">
                        {data
                          ? moment(data.startDate).format(VIEW_DATE_FORMAT)
                          : "No Start Date"}
                      </div>
                    </div>
                    <div className="homepage__container__result__card__content__entry">
                      <Img
                        loading="lazy"
                        src={timeSvg}
                        alt="timeSvg"
                        className="homepage__container__result__card__content__entry__svg"
                      />
                      <div className="homepage__container__result__card__content__entry__text">
                        {data
                          ? moment(data.endDate).format(VIEW_DATE_FORMAT)
                          : "No End Date"}
                      </div>
                    </div>
                  </>
                )}
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={milesSvg}
                    alt="milesSvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />
                  <div className="homepage__container__result__card__content__entry__text">
                    {data ? data.location : "No location"}
                  </div>
                </div>
                <div className="homepage__container__result__card__content__entry">
                  <Img
                    loading="lazy"
                    src={moneySvg}
                    alt="moneySvg"
                    className="homepage__container__result__card__content__entry__svg"
                  />
                  <div className="homepage__container__result__card__content__entry__text">
                    {data.salaryType === "Negotiable"
                      ? "Negotiable"
                      : data.salaryType === "Fixed"
                      ? "€ " + data.salaryMin
                      : "€ " + data.salaryMin + " - " + "€ " + data.salaryMax}
                  </div>
                </div>
              </div>
            </div>
            {data.type === "job" ? (
              <>
                <div className="pupup__container__from__preview__sub__heading">
                  Education Required
                </div>
                <div className="homepage__container__result__card__badges">
                  {data
                    ? data.education.length > 0
                      ? data.education
                          .map((item, index) => item.label)
                          .join(", ")
                      : "No education"
                    : "No education"}
                </div>
              </>
            ) : (
              <>
                <div className="pupup__container__from__preview__sub__heading">
                  Industry
                </div>
                <div className="homepage__container__result__card__badges">
                  {data ? data.industry?.label : "No industry"}
                </div>
              </>
            )}
            <div className="pupup__container__from__preview__sub__heading">
              Skills Required
            </div>
            <div className="homepage__container__result__card__badges">
              {data
                ? data.skill.map((skil, i) => (
                    <div
                      className="homepage__container__result__card__badge"
                      key={i}
                    >
                      {skil.label}
                    </div>
                  ))
                : "No Skill"}
            </div>
            <div className="pupup__container__from__preview__sub__heading">
              Description
            </div>
            <div className="pupup__container__from__preview__info">
              {data.description.length > 1000 ? (
                <div>{`${parse(data.description).substring(0, 1000)}...`}</div>
              ) : (
                <div>{parse(data.description)}</div>
              )}
            </div>
            {data.type == "project" ? null : (
              <>
                <div className="pupup__container__from__preview__sub__heading">
                  Responsibility
                </div>
                <div className="pupup__container__from__preview__info">
                  {getText(data.requirements).length > 1000 ? (
                    <div>{`${parse(data.requirements).substring(
                      0,
                      1000
                    )}...`}</div>
                  ) : (
                    <div>{parse(data.requirements)}</div>
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
