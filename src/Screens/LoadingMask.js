import React from "react";
import { Head } from "Components";
import { AIDALOADINGGIF } from "Assets";
import Img from "react-cool-img";

export const LoadingMask = ({ onDashboard }) => {
  return (
    <>
      <Head title="AIDApro | Loading" description="Loading" />
      <div
        className="pre-loader"
        style={onDashboard ? { maxWidth: "67%" } : null}
      >
        <Img
          loading="lazy"
          src={AIDALOADINGGIF}
          alt="AIDALOADINGGIF"
          className="pre-loader-gif"
        />
        <div className="pre-loader-text">Loading please wait</div>
      </div>
    </>
  );
};
