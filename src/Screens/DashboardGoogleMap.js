import React, { useEffect, useState } from "react";
import { GoogleMap } from "@react-google-maps/api";
import { MapMarker } from "Components";

export const DashboardGoogleMap = ({
  style,
  lat,
  lng,
  jobLocation,
  jobSeekers,
}) => {
  const [autocomplete, setAutocomplete] = useState();
  const [map, setMap] = useState();
  const [zoom, setZoom] = useState(8);
  const [center, setCenter] = useState({ lat: 33.596041, lng: 73.005094 });
  const [formattedAddress, setFormattedAddress] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    updateLatLng();
  }, [lat, lng]);

  useEffect(() => {
    setIsLoading(true);
    if (jobLocation != "") {
      setFormattedAddress(jobLocation);
      setIsLoading(false);
    } else {
      setIsLoading(false);
    }
  }, [jobLocation]);

  const updateLatLng = () => {
    setIsLoading(true);
    if (lat != null || lng != null) {
      let newLatLng = {
        lat: parseFloat(lat),
        lng: parseFloat(lng),
      };

      setCenter(newLatLng);
      setZoom(12);
      setIsLoading(false);
    } else {
      setIsLoading(false);
    }
  };

  const handleMapOnLoad = (map) => {
    setMap(map);
  };

  let jobSeeker = [
    {
      Id: 1,
      position: { lat: 33.6844202, lng: 73.04788479999999 },
      jobTitle: "Nodejs",
      Location: "Islamabad",
    },
    {
      Id: 2,
      position: { lat: 33.5651107, lng: 73.0169135 },
      jobTitle: "React js",
      Location: "Rawalpindi",
    },
    {
      Id: 3,
      position: { lat: 33.7715411, lng: 72.7510921 },
      jobTitle: "Angular",
      Location: "Wah Cant",
    },
    {
      Id: 4,
      position: { lat: 33.7462826, lng: 72.8397317 },
      jobTitle: "Asp.net",
      Location: "Taxila",
    },
  ];

  // const libraries = ["drawing", "places"];

  return (
    <>
      {/* <LoadScript
        id="google-map-script-loader"
        googleMapsApiKey="AIzaSyCyJ_-N0usmRLCZ7vsfupr-JlEqHjsdpGk"
        libraries={LIBRARIES}
      > */}
      <div
        style={{
          width: "100%",
          padding: "25px 0px 25px 18px",
        }}
      >
        <GoogleMap
          mapContainerStyle={{
            height: "350px",
            width: "100%",
            borderRadius: "20px",
            ...style,
          }}
          center={center}
          zoom={zoom}
          onLoad={handleMapOnLoad}
          options={{
            disableDefaultUI: true,
          }}
          value={formattedAddress}
        >
          {jobSeeker && jobSeeker.map((e) => <MapMarker key={e.Id} data={e} />)}
          {/* <Autocomplete
              onLoad={onAutocompleteLoad}
              onPlaceChanged={onPlaceChanged}
            >
              <input
                type="text"
                placeholder="Search Nearby..."
                value={formattedAddress}
                style={{
                  boxSizing: `border-box`,
                  border: `1px solid transparent`,
                  width: `350px`,
                  height: `35px`,
                  padding: `5px 12px`,
                  borderRadius: `3px`,
                  boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                  fontSize: `14px`,
                  outline: `none`,
                  textOverflow: `ellipses`,
                  position: "absolute",
                  left: "35%",
                  backgroundColor: "#f3f3f3 ",
                  top: "2px",
                }}
              />
            </Autocomplete> */}
        </GoogleMap>
      </div>
      {/* </LoadScript> */}
    </>
  );
};
