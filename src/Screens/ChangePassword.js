import React, { useEffect, useState } from "react";
import { Head, InputBox } from "Components";

export default function ChangePassword({ setIsChangePasswordOpen }) {
  const [isPasswordChanged, setIsPasswordChanged] = useState(false);
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Change Password" description="Change Password" />
      <div className="pupup__container">
        {isPasswordChanged ? (
          <div
            className="pupup__container__from animate__animated animate__slideInDown"
            style={{ maxWidth: "600px" }}
          >
            <button
              className="pupup__container__from__button"
              type="button"
              onClick={() => {
                setIsChangePasswordOpen(false);
              }}
              title="close popup"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="14.372"
                height="14.372"
                viewBox="0 0 14.372 14.372"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                </defs>
                <path
                  id="Icon_metro-cross"
                  data-name="Icon metro-cross"
                  d="M16.812,13.474h0l-4.36-4.36,4.36-4.36h0a.45.45,0,0,0,0-.635l-2.06-2.06a.45.45,0,0,0-.635,0h0l-4.36,4.36L5.4,2.059h0a.45.45,0,0,0-.635,0L2.7,4.119a.45.45,0,0,0,0,.635h0l4.36,4.36L2.7,13.474h0a.45.45,0,0,0,0,.635l2.06,2.06a.45.45,0,0,0,.635,0h0l4.36-4.36,4.36,4.36h0a.45.45,0,0,0,.635,0l2.06-2.06a.45.45,0,0,0,0-.635Z"
                  transform="translate(-2.571 -1.928)"
                  fill="url(#linear-gradient)"
                />
              </svg>
            </button>
            <div className="pupup__container__from__top">
              <div
                className="pupup__container__from__top__left"
                style={{
                  width: "100%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <div
                  className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                  style={{ fontSize: "30px", textAlign: "center" }}
                >
                  Password Changed Successfully
                </div>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="170.043"
                    height="180.672"
                    viewBox="0 0 187.043 180.672"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        y1="0.5"
                        x2="1"
                        y2="0.5"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-3"
                        y1="0.5"
                        x2="1"
                        y2="0.5"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#0ca69d" />
                      </linearGradient>
                    </defs>
                    <g
                      id="Group_1706"
                      data-name="Group 1706"
                      transform="translate(-2162.025 -546.459)"
                    >
                      <g
                        id="Group_1704"
                        data-name="Group 1704"
                        transform="translate(2162.025 546.459)"
                      >
                        <path
                          id="Path_21705"
                          data-name="Path 21705"
                          d="M2341.552,631.072c0,49.636-40,92.548-89.487,95.856-49.659,3.32-90.04-34.468-90.04-84.411s40.381-92.879,90.04-95.891C2301.555,543.624,2341.552,581.436,2341.552,631.072Z"
                          transform="translate(-2162.025 -546.459)"
                          fill="url(#linear-gradient)"
                        />
                        <path
                          id="Path_21706"
                          data-name="Path 21706"
                          d="M2381.969,797.261a10.314,10.314,0,0,1-9.529-4.791L2338.6,738.133c-3.171-5.092-1.463-12.067,3.814-15.577s12.116-2.227,15.283,2.863l23.467,37.718,30.965-63.064c2.779-5.659,9.456-8.256,14.908-5.8s7.622,9.027,4.847,14.683l-40.015,81.562a12.067,12.067,0,0,1-9.512,6.71C2382.229,797.241,2382.1,797.252,2381.969,797.261Z"
                          transform="translate(-2295.09 -658.159)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                      <g
                        id="Group_1705"
                        data-name="Group 1705"
                        transform="translate(2169.542 546.459)"
                      >
                        <path
                          id="Path_21707"
                          data-name="Path 21707"
                          d="M2372.929,631.072c0,49.636-40,92.548-89.487,95.856-49.659,3.32-90.04-34.468-90.04-84.411s40.381-92.879,90.04-95.891C2332.933,543.624,2372.929,581.436,2372.929,631.072Z"
                          transform="translate(-2193.402 -546.459)"
                          fill="url(#linear-gradient-3)"
                        />
                        <path
                          id="Path_21708"
                          data-name="Path 21708"
                          d="M2414.506,797.261a10.314,10.314,0,0,1-9.529-4.791l-30.633-38.49c-3.171-5.092-1.463-12.067,3.814-15.577s12.116-2.227,15.283,2.863l20.261,21.872,30.965-63.064c2.779-5.659,9.456-8.256,14.908-5.8s7.622,9.027,4.847,14.683l-40.015,81.562a12.066,12.066,0,0,1-9.512,6.71C2414.766,797.241,2414.636,797.252,2414.506,797.261Z"
                          transform="translate(-2333.501 -651.551)"
                          fill="#fff"
                        />
                      </g>
                    </g>
                  </svg>
                </div>

                <button
                  className="header__nav__btn btn__secondary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "50px",
                    width: "180px",
                  }}
                  onClick={() => {
                    setIsChangePasswordOpen(false);
                  }}
                  title="close popup"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        ) : (
          <form
            className="pupup__container__from animate__animated animate__slideInDown"
            style={{ maxWidth: "600px" }}
          >
            <button
              className="pupup__container__from__button"
              type="button"
              onClick={() => {
                setIsChangePasswordOpen(false);
              }}
              title="close popup"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="14.372"
                height="14.372"
                viewBox="0 0 14.372 14.372"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                </defs>
                <path
                  id="Icon_metro-cross"
                  data-name="Icon metro-cross"
                  d="M16.812,13.474h0l-4.36-4.36,4.36-4.36h0a.45.45,0,0,0,0-.635l-2.06-2.06a.45.45,0,0,0-.635,0h0l-4.36,4.36L5.4,2.059h0a.45.45,0,0,0-.635,0L2.7,4.119a.45.45,0,0,0,0,.635h0l4.36,4.36L2.7,13.474h0a.45.45,0,0,0,0,.635l2.06,2.06a.45.45,0,0,0,.635,0h0l4.36-4.36,4.36,4.36h0a.45.45,0,0,0,.635,0l2.06-2.06a.45.45,0,0,0,0-.635Z"
                  transform="translate(-2.571 -1.928)"
                  fill="url(#linear-gradient)"
                />
              </svg>
            </button>
            <div className="pupup__container__from__top">
              <div
                className="pupup__container__from__top__left"
                style={{ width: "100%" }}
              >
                <div
                  className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                  style={{ fontSize: "30px", textAlign: "center" }}
                >
                  Change Password
                </div>
                <div
                  className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                  style={{
                    fontSize: "16px",
                    textAlign: "center",
                    marginTop: "0em",
                    marginBottom: "3em",
                  }}
                >
                  Your Email has been verified, Please Enter your new Password
                </div>
                <InputBox
                  placeholder="Enter New Password"
                  type="password"
                  error={true}
                  errorMessage="Lorem, ipsum dolor."
                  svg={
                    <svg
                      id="padlock"
                      xmlns="http://www.w3.org/2000/svg"
                      width="11.192"
                      height="14.922"
                      viewBox="0 0 11.192 14.922"
                    >
                      <path
                        id="Path_293"
                        data-name="Path 293"
                        d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                        transform="translate(-3 -3.404)"
                        fill="#374957"
                      />
                      <path
                        id="Path_294"
                        data-name="Path 294"
                        d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                        transform="translate(-4.135)"
                        fill="#374957"
                      />
                      <path
                        id="Path_295"
                        data-name="Path 295"
                        d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                        transform="translate(-5.648 -4.917)"
                        fill="#374957"
                      />
                      <path
                        id="Path_296"
                        data-name="Path 296"
                        d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                        transform="translate(-6.12 -5.957)"
                        fill="#374957"
                      />
                    </svg>
                  }
                />
                <InputBox
                  placeholder="Re-Enter New Password"
                  type="password"
                  error={true}
                  errorMessage="Lorem, ipsum dolor."
                  svg={
                    <svg
                      id="padlock"
                      xmlns="http://www.w3.org/2000/svg"
                      width="11.192"
                      height="14.922"
                      viewBox="0 0 11.192 14.922"
                    >
                      <path
                        id="Path_293"
                        data-name="Path 293"
                        d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                        transform="translate(-3 -3.404)"
                        fill="#374957"
                      />
                      <path
                        id="Path_294"
                        data-name="Path 294"
                        d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                        transform="translate(-4.135)"
                        fill="#374957"
                      />
                      <path
                        id="Path_295"
                        data-name="Path 295"
                        d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                        transform="translate(-5.648 -4.917)"
                        fill="#374957"
                      />
                      <path
                        id="Path_296"
                        data-name="Path 296"
                        d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                        transform="translate(-6.12 -5.957)"
                        fill="#374957"
                      />
                    </svg>
                  }
                />
                <button
                  type="submit"
                  className="header__nav__btn btn__primary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "50px",
                    width: "180px",
                  }}
                  onClick={() => {
                    setIsPasswordChanged(true);
                  }}
                  title="change password"
                >
                  Change Password
                </button>
              </div>
            </div>
          </form>
        )}
      </div>
    </>
  );
}
