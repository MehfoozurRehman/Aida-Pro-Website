import { navigate } from "@reach/router";
import { NOTIFICATION_TYPE } from "../Utils/Constants";
import { getProjectById } from "API/Projects";
import { toast } from "react-toastify";
import { getJobById } from "API/Job";

export function CustomInfo(msg) {
  toast.info(msg, {
    position: "bottom-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function CustomSuccess(msg) {
  toast.success(msg, {
    position: "bottom-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function CustomError(msg) {
  toast.error(msg, {
    position: "bottom-right",
    autoClose: 3000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export function NotificationMessage(notification, setIsLoginOpen) {
  toast.info(<Display />);
  const isUserLogin = window.localStorage.getItem("userId");

  const onClickNotification = () => {
    let notificationData = JSON.parse(notification.data.notification);
    if (isUserLogin != undefined) {
      if (
        notification.data.notification_type == NOTIFICATION_TYPE.CHAT_REQUEST
      ) {
        // console.log("chat request");
      } else if (
        notification.data.notification_type == NOTIFICATION_TYPE.VIDEO_REQUEST
      ) {
        // console.log("VIDEO_REQUEST");
      } else if (
        notification.data.notification_type == NOTIFICATION_TYPE.VIEWED_PROFILE
      ) {
        // console.log("VIEWED_PROFILE");
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.COMMENT_ON_QUESTION
      ) {
        navigate("/coffee-corner-discussion/" + notificationData.Id, {
          state: { forumId: notificationData.Id },
        });
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.INTERESTED_JOBDETAIL
      ) {
        window.localStorage.setItem("isInterested", 1);
        window.localStorage.removeItem("isApplicant");
        window.localStorage.setItem("JobId", notificationData.Id);
        window.localStorage.setItem("JobType", "Job");
        navigate("/home-company/posting/details");
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.INTERESTED_PROJECTDETAIL
      ) {
        window.localStorage.setItem("isInterested", 1);
        window.localStorage.removeItem("isApplicant");
        window.localStorage.setItem("JobId", notificationData.Id);
        window.localStorage.setItem("JobType", "Project");
        navigate("/home-company/posting/details");
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.APPLIED_JOBDETAIL
      ) {
        window.localStorage.setItem("isApplicant", 1);
        window.localStorage.removeItem("isInterested");
        window.localStorage.setItem("JobId", notificationData.Id);
        window.localStorage.setItem("JobType", "Project");
        navigate("/home-company/posting/details");
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.APPLIES_PROJECTDETAIL
      ) {
        window.localStorage.setItem("isApplicant", 1);
        window.localStorage.removeItem("isInterested");
        window.localStorage.setItem("JobId", notificationData.Id);
        window.localStorage.setItem("JobType", "Projects");
        navigate("/home-company/posting/details");
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.REJECT_JOB_APPLICATION
      ) {
        getJobById(notificationData.Id)
          .then((data) => {
            if (data.data.success) {
              const requestData = {
                redirectFrom: "Employee",
                objectData: data.data.result,
                jobType: "Jobs",
              };
              navigate("/job-details", { state: requestData });
            }
          })
          .catch((err) => {
            // console.log("err", err)
          });
      } else if (
        notification.data.notification_type ==
        NOTIFICATION_TYPE.REJECT_PROJECT_APPLICATION
      ) {
        getProjectById(notificationData.Id)
          .then((data) => {
            if (data.data.success) {
              const requestData = {
                redirectFrom: "Freelancer",
                objectData: data.data.result,
                jobType: "Projects",
              };
              navigate("/job-details", { state: requestData });
            }
          })
          .catch((err) => {
            // console.log("err", err)
          });
      } else {
        // console.log("nothing");
      }
    } else setIsLoginOpen(true);
  };

  function Display() {
    return (
      <div
        onClick={() => {
          onClickNotification();
        }}
      >
        <h4>{notification.notification.title}</h4>
        <p>{notification.notification.body}</p>
      </div>
    );
  }
}
