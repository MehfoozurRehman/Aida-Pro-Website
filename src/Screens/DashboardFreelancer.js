import React, { useContext, useEffect, useState } from "react";
import { DashboardGoogleMap } from "./DashboardGoogleMap";
import DashboardCompanySearchFilter from "Components/DashboardCompanySearchFilter";
import { Head } from "Components";
import UserContext from "Context/UserContext";
import { useDispatch, useSelector } from "react-redux";
import { getFreelancerById } from "Redux/Actions/AppActions";
import DashboardFreelancerProfileCompletion from "Components/DashboardFreelancerProfileCompletion";

const DashboardFreelancer = ({
  setIsFilterOpen,
  isFilterOpen,
  filterCall,
  title,
  keywords,
  userName,
  latitude,
  longitude,
  mapLocation,
  selectedRangeValue,
}) => {
  localStorage.setItem("isOn", "freelancer");

  let { freelancer } = useSelector((state) => state.freelancer);
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFreelancerById(user.FreelancerId));
  }, []);

  const [isLoading, setIsLoading] = useState(false);
  const [titleLocal, setTitleLocal] = useState(title);
  const [jobLocation, setJobLocation] = useState(mapLocation);
  const [lat, setLat] = useState(latitude);
  const [lng, setLng] = useState(longitude);
  let [profilePercentage, setProfilePercentage] = useState(0);
  const [showProfilePercentageScreen, setShowProfilePercentageScreen] =
    useState(false);

  let [radius, setRadius] = useState(selectedRangeValue);

  const apiCall = () => {
    filterCall(titleLocal, jobLocation, radius, lat, lng);
  };

  const searchDataByTitle = (data) => {
    setTitleLocal(data);
  };

  const searchByLocation = (data, isInput) => {
    if (isInput == false) {
      if (data.name) {
        // setLocationError(true);
        // setLocationErrorMessage("Please enter your correct location");
      } else {
        setJobLocation(data.formatted_address);
        setLat(data.geometry.location.lat());
        setLng(data.geometry.location.lng());
      }
    } else {
      if (data.currentTarget.value === "" && data.name) {
        // setLocationError(true);
        // setLocationErrorMessage("Please enter your location");
      } else {
        // setLocationError(false);
        // setLocationErrorMessage("");
        setJobLocation(data.currentTarget.value);
      }
    }
  };

  const searchByKeyword = (data) => {
    setTitleLocal(data);
  };

  const searchByRange = (value) => {
    setRadius((radius = value));
  };

  const isSignUpProfileCompleted = () => {
    if (
      freelancer.emailAddress != "" ||
      freelancer.firstName != "" ||
      freelancer.lastName != "" ||
      freelancer.lat != "" ||
      freelancer.lng != "" ||
      freelancer.primaryAddress ||
      freelancer.phoneNo != ""
    )
      return true;
    else return false;
  };

  const isPersonelDetailCompleted = () => {
    if (
      freelancer.age != null &&
      freelancer.gender != undefined &&
      freelancer.language.length > 0 &&
      freelancer.primaryZipCode != null &&
      isSignUpProfileCompleted()
    )
      return true;
    else return false;
  };

  const isProfessionalDetailCompleted = () => {
    if (freelancer.qualification.length > 0 && freelancer.skills.length > 0)
      return true;
    else return false;
  };

  const isPortfolioCompleted = () => {
    if (isPersonelDetailCompleted() && freelancer.portfolios.length > 0)
      return true;
    else return false;
  };

  useEffect(() => {
    if (Object.keys(freelancer).length > 0) {
      let signUpProfileCompletedPercentage = 0;
      let personelDetailCompletedPercentage = 0;
      let professionalDetailCompletedPercentage = 0;
      let portfolioCompletedPercentage = 0;
      if (isSignUpProfileCompleted()) {
        signUpProfileCompletedPercentage = 25;
      }
      if (isPersonelDetailCompleted()) {
        personelDetailCompletedPercentage = 25;
      }
      if (isProfessionalDetailCompleted()) {
        professionalDetailCompletedPercentage = 25;
      }
      if (isPortfolioCompleted()) {
        portfolioCompletedPercentage = 25;
      }
      let percentage =
        signUpProfileCompletedPercentage +
        personelDetailCompletedPercentage +
        professionalDetailCompletedPercentage +
        portfolioCompletedPercentage;
      setProfilePercentage(percentage);
      if (percentage != 100) {
        setShowProfilePercentageScreen(true);
      }
    }
  }, [freelancer]);

  return (
    <>
      <Head
        title="AIDApro | Freelancer Dashboard"
        description="Freelancer Dashboard"
      />
      {showProfilePercentageScreen ? (
        <DashboardFreelancerProfileCompletion
          profilePercentage={profilePercentage}
          isPersonelDetailCompleted={isPersonelDetailCompleted}
          isProfessionalDetailCompleted={isProfessionalDetailCompleted}
          isPortfolioCompleted={isPortfolioCompleted}
        />
      ) : null}
      <div className="dashboard__company__container">
        <div className="dashboard__company__container__left">
          <div
            className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
            style={{ textAlign: "left", fontSize: 25 }}
          >
            Welcome <b>{userName}</b>
            <br />
            <span>find projects</span>
          </div>
          <DashboardCompanySearchFilter
            setIsFilterOpen={setIsFilterOpen}
            isFilterOpen={isFilterOpen}
            isMain={true}
            keywords={keywords}
            searchByTitle={(e) => searchDataByTitle(e)}
            searchByFilter={() => apiCall()}
            searchByLocation={searchByLocation}
            searchByKeyword={(e) => searchByKeyword(e)}
            searchByRange={searchByRange}
            title={titleLocal}
            jobLocation={jobLocation}
            radius={radius}
          />
        </div>
        <div className="dashboard__company__container__right">
          <DashboardGoogleMap lat={lat} lng={lng} jobLocation={jobLocation} />
        </div>
      </div>
    </>
  );
};
export default DashboardFreelancer;
