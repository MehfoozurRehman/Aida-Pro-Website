import React, { useState, useEffect, useContext } from "react";
import { getPaymentRequestByCompanyId } from "../API/Plans";
import { useSelector } from "react-redux";
import { balanceSvg } from "Assets";
import UserContext from "Context/UserContext";
import { CustomError } from "./Toasts";
import NoData from "Components/NoData";
import {
  Head,
  PaymentDetailsEntry,
  PaymentDetailsHeader,
  DashboardHeading,
} from "Components";

export default function Invoices({ setIsInvoiceOpen }) {
  const user = useContext(UserContext);
  const { company } = useSelector((state) => state.company);

  const [isLoading, setIsLoading] = useState(true);
  const [isPaymentHistoryShown, setIsPaymentHistoryShown] = useState(true);
  const [paymentHistoryData, setPaymentHistoryData] = useState([]);

  useEffect(() => {
    getPaymentRequest();
  }, []);

  const getPaymentRequest = () => {
    setIsLoading(true);
    getPaymentRequestByCompanyId(user.CompanyId)
      .then(({ data }) => {
        console.log("data", data);
        if (data.success) setPaymentHistoryData(data.result);
        else CustomError("Some error occured.");
        setIsLoading(false);
      })
      .catch((err) => {
        console.log("err", err);
        CustomError("Some error occured.");
        setIsLoading(false);
      });
  };

  return (
    <>
      <Head title="AIDApro | Balance" description="Balance" />
      <section className="plans__container">
        <DashboardHeading heading="Invoices" svg={balanceSvg} />
        {isPaymentHistoryShown ? (
          <div className="payment__details__container__entry__wrapper animate__animated animate__fadeIn">
            <div className="payment__details__container__entry__content">
              <PaymentDetailsHeader />
              {paymentHistoryData.length === 0 ? (
                <NoData />
              ) : (
                paymentHistoryData.map((paymentHistoryDataEntry, i) => (
                  <PaymentDetailsEntry
                    key={i}
                    ReceiptsList={paymentHistoryDataEntry.TransactionId}
                    Status={paymentHistoryDataEntry.Status}
                    Total={paymentHistoryDataEntry.Description}
                    CreatedAt={paymentHistoryDataEntry.CreatedOn}
                    PaidAt={paymentHistoryDataEntry.CreatedOn}
                    setIsInvoiceOpen={setIsInvoiceOpen}
                  />
                ))
              )}
            </div>
          </div>
        ) : null}
      </section>
    </>
  );
}
