import React from "react";
import Header from "Components/Header";
import { Head, MobileAppAdvert } from "Components";
import { useSelector } from "react-redux";
import AboutUsJumbotron from "Components/AboutUsJumbotron";
import GetStartedBanner from "Components/GetStartedBanner";
import AboutOurGoals from "Components/AboutOurGoals";
import AboutOurMission from "Components/AboutOurMission";
import AboutOurVision from "Components/AboutOurVision";
import AboutOurTeam from "Components/AboutOurTeam";

export default function AboutUs({ setIsLoginOpen }) {
  const { user } = useSelector((state) => state.user);
  return (
    <>
      <Head title="AIDApro | About Us" description="About Us" />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <AboutUsJumbotron />
      <div className="homepage__container">
        <AboutOurGoals />
        <AboutOurMission />
        <AboutOurVision />
        <AboutOurTeam />
        {!user.Id ? <GetStartedBanner /> : null}
        <MobileAppAdvert />
      </div>
    </>
  );
}
