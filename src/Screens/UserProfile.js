import React, { useEffect, useState, useContext } from "react";
import { useNavigate } from "@reach/router";
import PostingDetailsContact from "./PostingDetailsContact";
import PostingDetailsEducation from "./PostingDetailsEducation";
import PostingDetailsProtfolio from "./PostingDetailsProtfolio";
import PostingDetailsProfile from "./PostingDetailsProfile";
import PostingDetailsTab from "Components/PostingDetailsTab";
import experienceSvg from "../Assets/experienceSvg.svg";
import locationSvg from "../Assets/locationSvg.svg";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import { getUserInfoByUserId } from "../API/Users";
import { Avatar, Head, Logout } from "Components";
import UserContext from "Context/UserContext";
import timeSvg from "../Assets/timeSvg.svg";
import { getText } from "Utils/functions";
import NoData from "Components/NoData";
import Img from "react-cool-img";
import moment from "moment";
import {
  ProjectActive,
  ProjectInactive,
  UserActive,
  userInActive,
  WorkActive,
  WorkInactive,
  ContactActive,
  ContactInactive,
  EducationActive,
  EducationInactive,
  AidaLogo,
} from "Assets";
import UserProfileTabs from "Components/UserProfileTabs";

const UserProfile = ({ from, isOn }) => {
  const [isOnProfile, setIsOnProfile] = useState(true);
  const [isOnWorkExperiance, setIsOnWorkExperiance] = useState(false);
  const [isOnProjectPortfolio, setIsOnProjectPortfolio] = useState(false);
  const [isOnEducation, setIsOnEducation] = useState(false);
  const [isOnContact, setIsOnContact] = useState(false);
  let [data, setData] = useState([]);
  const [experiences, setExperiences] = useState([]);
  const [educations, setEducations] = useState([]);
  const [skills, setSkills] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [certificates, setCertificates] = useState([]);
  const [selectedData, setSelectedData] = useState(null);
  const [languages, setLanguages] = useState([]);
  const user = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    setIsLoading(true);
    window.scrollTo({ top: 0, behavior: "smooth" });

    if (isOn == "professional") {
      getUserInfoByUserId(user.Id, "Employment")
        .then((data) => {
          if (data.data.success) {
            setData(data.data.result);
            bindData(data.data.result);
          }
        })
        .catch((error) => {
          setIsLoading(false);
        });
    } else {
      getUserInfoByUserId(user.Id, "Freelancer")
        .then((data) => {
          if (data.data.success) {
            setData(data.data.result);
            bindData(data.data.result);
          }
        })
        .catch((error) => {
          setIsLoading(false);
        });
    }
  }, []);

  const bindData = (data) => {
    let JobSeekerQualifications = [];
    if (data && data.Qualifications) {
      data.Qualifications.map((e) => {
        JobSeekerQualifications.push({
          Id: e.Id,
          JobSeekerId: user.JobSeekerId,
          dateFrom: e.StartDate,
          dateTo: e.EndDate,
          institueName: e.Institute,
          educationType: {
            label: e.DegreeLookupDetail.Title,
            value: e.Id,
          },
        });
      });
    }

    let JobSeekerExperiences = [];
    if (data && data.Experiences) {
      data.Experiences &&
        data.Experiences.map((e) => {
          JobSeekerExperiences.push({
            Id: e.Id,
            jobCompany: e.CompanyName,
            jobDescription: e.Description,
            jobEmployementType: { value: e.EmploymentTypeLookupDetailId },
            jobIndustryType: { value: e.IndustryId },
            jobLocation: e.Location,
            jobTitle: e.Title,
            jobFrom: e.StartDate,
            jobTo: e.EndDate,
          });
        });
    }

    let skillsDTO = [];
    if (data && data.Skills) {
      data.Skills &&
        data.Skills.map((e) => {
          skillsDTO.push({
            label: e.Skill && e.Skill.Title,
            value: e.Skill && e.Id,
            Id: e.Id,
          });
        });
      setSkills(skillsDTO);
    }

    let certificateDTO = [];
    if (data && data.Certificates) {
      data.Certificates &&
        data.Certificates.map((e) => {
          certificateDTO.push({
            Id: e.Id,
            certificateName: e.CertificateName,
            JobSeekerId: user.JobSeekerId,
            dateFrom: e.StartDate,
            dateTo: e.EndDate,
            institueName: e.Institute,
          });
        });
    }

    let languageDTO = [];
    if (data && data.Languages) {
      languageDTO = data.Languages;
    }

    setEducations(JobSeekerQualifications);
    setExperiences(JobSeekerExperiences);
    setCertificates(certificateDTO);
    setLanguages(languageDTO);
    setSkills(skillsDTO);
    setSelectedData(data);
    setIsLoading(false);
  };

  const WorkExperience = ({ data }) => {
    return (
      <div
        className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry"
        title="edit work experiance"
      >
        <Img
          loading="lazy"
          src={AidaLogo}
          alt="workexperianceimg"
          className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__img"
        />
        <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info">
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__heading">
            {data ? data.Title : "Title"}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__duration"></div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__designation">
            {data ? data.CompanyName : null}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__info__date">
            {data
              ? moment(data.StartDate).format("MMMM DD, YYYY")
              : "Not specified"}
            {data
              ? data.EndDate != null
                ? ` - ${moment(data.EndDate).format("MMMM DD, YYYY")}`
                : " - Continue"
              : "Not specified"}
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance__entry__content">
            {getText(data.Description).length > 1000 ? (
              <>{`${getText(data.Description).substring(0, 1000)}...`}</>
            ) : (
              <>{getText(data.Description)}</>
            )}
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      <Head title="AIDApro | Posting Details" description="Posting Details" />
      <div className="posting__details__container">
        {/* {from === "profile" ? null : (
              <div
                className="posting__details__container__back__link"
                onClick={() => goBack()}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="7.639"
                  height="13.363"
                  style={{ marginRight: "1em" }}
                  viewBox="0 0 7.639 13.363"
                >
                  <path
                    id="Icon_ionic-ios-arrow-back"
                    data-name="Icon ionic-ios-arrow-back"
                    d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                    transform="translate(-11.251 -6.194)"
                  />
                </svg>
                
                Back
              </div>
            )} */}
        <div
          className="homepage__container__jobs__projects__penel"
          style={{ margin: "0em" }}
        >
          <div
            className="homepage__container__jobs__projects__penel__container"
            style={{ margin: "0em" }}
          >
            <div
              className="homepage__container__jobs__projects__penel__container__details"
              style={from === "profile" ? { width: "100%" } : null}
            >
              <div className="homepage__container__jobs__projects__penel__container__details__heading">
                <Avatar
                  onClick={(event) => {
                    if (
                      data.ProfilePicture != "" &&
                      data.ProfilePicture != null
                    ) {
                      window.open(
                        process.env.REACT_APP_BASEURL.concat(
                          data.ProfilePicture
                        ),
                        "_Blank"
                      );
                    }
                  }}
                  userPic={
                    data.ProfilePicture != null && data.ProfilePicture != ""
                      ? process.env.REACT_APP_BASEURL.concat(
                          data.ProfilePicture
                        )
                      : null
                  }
                />
                {data.FullName}
              </div>
              <div className="homepage__container__jobs__projects__penel__container__details__feature">
                <div className="homepage__container__result__card__content">
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={timeSvg}
                      alt="timeSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />
                    <div className="homepage__container__result__card__content__entry__text">
                      {data.AvailabilityLookupDetail != null
                        ? data.AvailabilityLookupDetail.Title
                        : "Not specified"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={locationSvg}
                      alt="locationSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      Remote
                    </div>
                  </div>

                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={IndustrySvg}
                      alt="IndustrySvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {data.Experiences != null && data.Experiences.length > 0
                        ? data.Experiences[0].Industry != null
                          ? data.Experiences[0].Industry.Title
                          : "Not specified"
                        : "Not specfied"}
                    </div>
                  </div>
                  <div className="homepage__container__result__card__content__entry">
                    <Img
                      loading="lazy"
                      src={experienceSvg}
                      alt="experienceSvg"
                      className="homepage__container__result__card__content__entry__svg"
                    />

                    <div className="homepage__container__result__card__content__entry__text">
                      {data.TotalExperience != null
                        ? parseFloat(data.TotalExperience).toFixed(1)
                        : 0}{" "}
                      Years of Experience
                    </div>
                  </div>
                </div>
                <div className="homepage__container__btns">
                  <button
                    onClick={() => {
                      navigate(`/home-${isOn}/change-password`);
                    }}
                    title="change password"
                    className="header__nav__btn btn__secondary"
                    style={{
                      width: "150px",
                      height: "40px",
                      display: "flex",
                      alignItems: "center",
                      marginBottom: 10,
                    }}
                  >
                    Change Password
                  </button>
                </div>
              </div>
              <UserProfileTabs
                setIsOnContact={setIsOnContact}
                setIsOnEducation={setIsOnEducation}
                setIsOnProfile={setIsOnProfile}
                setIsOnProjectPortfolio={setIsOnProjectPortfolio}
                setIsOnWorkExperiance={setIsOnWorkExperiance}
                UserActive={UserActive}
                userInActive={userInActive}
                isOnProfile={isOnProfile}
                WorkActive={WorkActive}
                WorkInactive={WorkInactive}
                isOnWorkExperiance={isOnWorkExperiance}
                ProjectActive={ProjectActive}
                ProjectInactive={ProjectInactive}
                isOnProjectPortfolio={isOnProjectPortfolio}
                EducationActive={EducationActive}
                EducationInactive={EducationInactive}
                isOnEducation={isOnEducation}
                ContactActive={ContactActive}
                ContactInactive={ContactInactive}
                isOnContact={isOnContact}
              />
              <div className="homepage__container__jobs__projects__penel__container__details__content">
                {isOnProfile ? (
                  <PostingDetailsProfile
                    data={data}
                    skills={skills}
                    languages={languages}
                  />
                ) : isOnWorkExperiance ? (
                  <>
                    <Head
                      title="AIDApro | Posting Details - Experience"
                      description="Posting Details - Experience"
                    />
                    <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance">
                      {data.Experiences.length === 0 ? (
                        <NoData text="No Work Experience" />
                      ) : (
                        data.Experiences.map((experience, i) => (
                          <WorkExperience key={i} data={experience} />
                        ))
                      )}
                    </div>
                  </>
                ) : isOnProjectPortfolio ? (
                  <PostingDetailsProtfolio Portfolios={data.Portfolios} />
                ) : isOnEducation ? (
                  <PostingDetailsEducation
                    data={educations}
                    certificatesData={certificates}
                  />
                ) : isOnContact ? (
                  <PostingDetailsContact data={data} />
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="dashboard__company__container__profile__preview__bottom__bar">
        <Logout />
      </div>
    </>
  );
};

export default UserProfile;
