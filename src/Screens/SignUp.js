import React, { useState, useEffect, useContext } from "react";
import { companySignUp } from "../API/CompanyAPI";
import { freelancerSignUp } from "../API/FreelancerApi";
import { employeeSignUp } from "../API/EmploymentAPI";
import { getCountry, getAllIndustry } from "../API/Api";
import { CustomError, CustomInfo, CustomSuccess } from "./Toasts";
import { Head, InputBox, SocialLinkCard, ContactTab } from "Components";
import { isNullOrEmpty, isNullOrEmptyArray } from "Utils/TextUtils";
import { setCurrentUser } from "Redux/Actions/AppActions";
import UserContext from "Context/UserContext";
import { isUserLoggedIn } from "Utils/common";
import FacebookLogin from "react-facebook-login";
import { GoogleLogin } from "react-google-login";
import { LoadingMask } from "./LoadingMask";
import { useDispatch } from "react-redux";
import { HomeSignupSvg } from "Assets";
import Header from "Components/Header";
import { Link } from "@reach/router";
import Img from "react-cool-img";
import { PopupContext } from "Routes/AppRouter";
import {
  isInvalidEmail,
  isInvalidPassword,
  isInvalidPhoneNumber,
} from "Utils/Validations";

const SignUp = ({
  setIsLoginOpen,
  setIsEmailVerificationOpen,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) => {
  let isOn = window.localStorage.getItem("isOn");
  const user = useContext(UserContext);
  let dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  var [countryCodes, setCountryCode] = useState([]);
  var [selectedCountryCode, setSelectedCountryCode] = useState(null);
  const [city, setCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState([]);
  var [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [industryTypeDropDownData, setIndustryTypeDropDownData] = useState([]);
  const [industryType, setIndustryType] = useState([]);
  const [redirectedFromRole, setRedirectedFromRole] = useState(
    isOn == "company"
      ? "company"
      : isOn == "professional"
      ? "professional"
      : "freelancer"
  );
  const [address, setAddress] = useState("");
  const [lat, setLat] = useState("");
  const [lng, setLng] = useState("");

  //validations
  var [firstNameError, setFirstNameError] = useState(false);
  const [firstNameErrorMessage, setFirstNameErrorMessage] = useState("");
  var [lastNameError, setLastNameError] = useState(false);
  const [lastNameErrorMessage, setLastNameErrorMessage] = useState("");
  var [industryError, setIndustryError] = useState(false);
  const [industryErrorMessage, setIndustryErrorMessage] = useState("");
  var [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  var [passwordError, setPasswordError] = useState(false);
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  var [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
    useState("");
  var [phoneError, setPhoneError] = useState(false);
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  var [locationError, setLocationError] = useState(false);
  const [locationErrorMessage, setLocationErrorMessage] = useState("");
  const [rememberMe, setRememberMe] = useState(false);
  var [phoneMinLength, setPhoneMinLength] = useState(null);
  var [phoneMaxLength, setPhoneMaxLength] = useState(null);

  useEffect(() => {
    let response = isUserLoggedIn(user);
    if (response) {
      if (user.Freelancer != null) window.location.href = "home-freelancer";
      else if (user.JobSeeker != null)
        window.location.href = "home-professional";
      else if (user.CompanyProfile != null)
        window.location.href = "home-company";
    } else {
      // setIsLoading(true);
      getCountry()
        .then(({ data }) => {
          let options = data.result.map((e) => ({
            value: e.Id,
            label: e.digit_name + " - " + e.phone_country_code,
            capital_city: e.capital_city,
            Title: e.Title,
            long_name: e.long_name,
            digit_name: e.digit_name,
            phone_country_code: e.phone_country_code,
            phone_number_length_min: e.phone_number_length_min,
            phone_number_length_max: e.phone_number_length_max,
            postal_validation_regex: e.postal_validation_regex,
            postal_code_char_set: e.postal_code_char_set,
            postal_code_length_min: e.postal_code_length_min,
            postal_code_length_max: e.postal_code_length_max,
          }));
          setCountryCode((countryCodes = options));
          setIsLoading(false);
        })
        .catch((error) => {
          // console.log(error);
        });

      getAllIndustry()
        .then(({ data }) => {
          let formattedData = [];
          data.result.map((e) => {
            formattedData.push({ label: e.Title, value: e.Id });
          });
          setIndustryTypeDropDownData(formattedData);
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }, []);

  const resetViewFields = () => {
    setFirstName("");
    setLastName("");
    setEmail("");
    setPhone("");
    // setCountryCode([]);
    setSelectedCountryCode(null);
    setCity([]);
    setSelectedCity([]);
    setPassword("");
    setConfirmPassword("");

    setIndustryType([]);

    setAddress("");
    setLat("");
    setLng("");

    setFirstNameErrorMessageAndVisibility("");
    setLastNameErrorMessageAndVisibility("");
    setEmailErrorMessageAndVisibility("");
    setPhoneNumberErrorMessageAndVisibility("");
    setPasswordErrorMessageAndVisibility("");
    setConfirmPasswordErrorMessageAndVisibility("");
    setLocationErrorMessageAndVisibility("");
    setIndustryErrorMessageAndVisibility("");
  };

  const isFromCompany = () => {
    return redirectedFromRole === "company";
  };
  const onNameTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setFirstNameErrorMessageAndVisibility(
        isFromCompany()
          ? "Please enter company name"
          : "Please enter first name"
      );
    else setFirstNameErrorMessageAndVisibility("");
    setFirstName(event.currentTarget.value);
  };
  const onLastNameTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value)) {
      setLastNameErrorMessageAndVisibility(
        isFromCompany()
          ? "Please enter contact person"
          : "Please enter last name"
      );
    } else {
      setLastNameErrorMessageAndVisibility("");
    }
    setLastName(event.currentTarget.value);
  };
  const onEmailTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(event.currentTarget.value))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else setEmailErrorMessageAndVisibility("");
    setEmail(event.currentTarget.value);
  };
  const onPhoneTextChangeListener = (enteredValue) => {
    setPhone(enteredValue);
    if (isNullOrEmpty(enteredValue))
      setPhoneNumberErrorMessageAndVisibility("Please enter phone number");
    else if (selectedCountryCode == null)
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter country code first"
      );
    else if (isInvalidPhoneNumber(enteredValue))
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter valid phone number"
      );
    else if (enteredValue.length < phoneMaxLength)
      setPhoneNumberErrorMessageAndVisibility(
        `Please enter ${phoneMaxLength} digit phone number`
      );
    else if (enteredValue.length > phoneMaxLength) {
      setPhoneNumberErrorMessageAndVisibility(
        `Phone number can't be greater than ${phoneMaxLength} digits`
      );
    } else {
      setPhoneNumberErrorMessageAndVisibility("");
      return true;
    }
  };
  const onPasswordTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setPasswordErrorMessageAndVisibility("Please enter password");
    else if (isInvalidPassword(event.currentTarget.value))
      setPasswordErrorMessageAndVisibility("Min 8 characters");
    else setPasswordErrorMessageAndVisibility("");
    setPassword((password = event.currentTarget.value));
    if (!isNullOrEmpty(confirmPassword))
      onConfirmPasswordTextChangeListener(confirmPassword);
  };
  const onConfirmPasswordTextChangeListener = (enteredValue) => {
    if (isNullOrEmpty(enteredValue))
      setConfirmPasswordErrorMessageAndVisibility("Please enter password");
    else if (isInvalidPassword(enteredValue))
      setConfirmPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (enteredValue !== password)
      setConfirmPasswordErrorMessageAndVisibility("Password do not match!");
    else setConfirmPasswordErrorMessageAndVisibility("");
    setConfirmPassword(enteredValue);
  };

  const industryChange = (event) => {
    if (event.length === 0) {
      setIndustryError(true);
      setIndustryErrorMessage("Please select industry");
    } else {
      setIndustryError(false);
      setIndustryErrorMessage("");
      setIndustryType([event]);
    }
  };

  const setFirstNameErrorMessageAndVisibility = (text) => {
    setFirstNameError((firstNameError = !isNullOrEmpty(text)));
    setFirstNameErrorMessage(text);
  };
  const setLastNameErrorMessageAndVisibility = (text) => {
    setLastNameError((lastNameError = !isNullOrEmpty(text)));
    setLastNameErrorMessage(text);
  };
  const setEmailErrorMessageAndVisibility = (text) => {
    setEmailError((emailError = !isNullOrEmpty(text)));
    setEmailErrorMessage(text);
  };
  const setPhoneNumberErrorMessageAndVisibility = (text) => {
    setPhoneError((phoneError = !isNullOrEmpty(text)));
    setPhoneErrorMessage(text);
  };
  const setPasswordErrorMessageAndVisibility = (text) => {
    setPasswordError((passwordError = !isNullOrEmpty(text)));
    setPasswordErrorMessage(text);
  };
  const setConfirmPasswordErrorMessageAndVisibility = (text) => {
    setConfirmPasswordError((confirmPasswordError = !isNullOrEmpty(text)));
    setConfirmPasswordErrorMessage(text);
  };
  const setLocationErrorMessageAndVisibility = (text) => {
    setLocationError((locationError = !isNullOrEmpty(text)));
    setLocationErrorMessage(text);
  };
  const setIndustryErrorMessageAndVisibility = (text) => {
    setIndustryError((industryError = !isNullOrEmpty(text)));
    setIndustryErrorMessage(text);
  };

  const signUpUser = () => {
    if (isNullOrEmpty(firstName))
      setFirstNameErrorMessageAndVisibility(
        isFromCompany()
          ? "Please enter company name"
          : "Please enter first name"
      );
    else if (isNullOrEmpty(lastName))
      setLastNameErrorMessageAndVisibility(
        isFromCompany()
          ? "Please enter contact person"
          : "Please enter last name"
      );
    else if (isNullOrEmpty(email))
      setEmailErrorMessageAndVisibility("Please enter email address");
    else if (isInvalidEmail(email))
      setEmailErrorMessageAndVisibility("Please enter valid email address");
    else if (isFromCompany() && isNullOrEmptyArray(industryType))
      setIndustryErrorMessageAndVisibility("Please select industry");
    else if (!isFromCompany() && isNullOrEmpty(phone))
      setPhoneNumberErrorMessageAndVisibility("Please enter phone number");
    else if (!isFromCompany() && !onPhoneTextChangeListener(phone))
      console.log("Please select phone");
    else if (isNullOrEmpty(lat) || isNullOrEmpty(lng))
      setLocationErrorMessageAndVisibility("Please select location");
    else if (isNullOrEmpty(password))
      setPasswordErrorMessageAndVisibility("Please enter password");
    else if (isInvalidPassword(password))
      setPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (isNullOrEmpty(confirmPassword))
      setConfirmPasswordErrorMessageAndVisibility("Please enter password");
    else if (isInvalidPassword(confirmPassword))
      setConfirmPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (password != confirmPassword) {
      setHeadingAndTextForAlertDialog("Password donot match");
      setAlertDialogVisibility(true);
    } else if (!rememberMe)
      CustomError(
        "Please accept AIDApro’s Terms & Conditions & Privacy Policy"
      );
    else {
      setIsLoading(true);
      if (redirectedFromRole === "company") signUpCompany();
      else if (redirectedFromRole === "freelancer") signUpFreelancer();
      else signUpProfession();
    }
  };

  const signUpCompany = () => {
    const firebaseToken = localStorage.getItem("ft");

    let industryId = industryType.map((e) => {
      return e.value;
    });

    let data = {
      CompanyName: firstName,
      ContactPerson: lastName,
      IndustryId: industryId[0],
      Latitude: lat,
      Longitude: lng,
      CompanyAddresses: [
        {
          Latitude: lat,
          Longitude: lng,
          AddressDetail: address,
          // CountryId: selectedCountryCode != null ? selectedCountryCode.value : null,
          CountryId: selectedCountryCode.value,
          Email: email,
        },
      ],
      Users: [
        {
          RoleId: 1,
          UserName: email,
          LoginName: firstName,
          LoginPassword: password,
          // CountryId: selectedCountryCode ? selectedCountryCode.value : null,
          CountryId: selectedCountryCode.value,
          WebToken: firebaseToken,
          Login_Type: "app",
        },
      ],
    };

    companySignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) navigateToHomePage(data);
          else {
            localStorage.setItem("emailTobeVerify", email);
            setIsEmailVerificationOpen(true);
          }
        } else {
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register company");
        setAlertDialogVisibility(true);
      });
  };

  const signUpProfession = () => {
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: firstName,
      LastName: lastName,
      JobSeekerAddresses: [
        {
          CountryId: selectedCountryCode.value,
          Email: email,
          Latitude: lat,
          Longitude: lng,
          AddressDetail: address,
          PhoneNo: selectedCountryCode.phone_country_code + phone,
        },
      ],
      Users: [
        {
          RoleId: 2,
          UserName: email,
          LoginName: firstName + " " + lastName,
          LoginPassword: password,
          WebToken: firebaseToken,
          CountryId: selectedCountryCode.value,
          Phoneno: selectedCountryCode.phone_country_code + phone,
          Login_Type: "app",
        },
      ],
    };
    employeeSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) navigateToHomePage(data);
          else {
            //CustomSuccess("Employee Register Successfully");
            localStorage.setItem("emailTobeVerify", email);
            setIsEmailVerificationOpen(true);
          }
        } else {
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register professional");
        setAlertDialogVisibility(true);
      });
  };

  const signUpFreelancer = () => {
    setIsLoading(true);
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: firstName,
      LastName: lastName,
      FreelancerAddresses: [
        {
          CountryId: selectedCountryCode.value,
          Email: email,
          Latitude: lat,
          Longitude: lng,
          AddressDetail: address,
          PhoneNo: selectedCountryCode.phone_country_code + phone,
        },
      ],
      Users: [
        {
          RoleId: 3,
          UserName: email,
          LoginName: firstName + " " + lastName,
          LoginPassword: password,
          WebToken: firebaseToken,
          CountryId: selectedCountryCode.value,
          Phoneno: selectedCountryCode.phone_country_code + phone,
          Login_Type: "app",
        },
      ],
    };

    freelancerSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) navigateToHomePage(data);
          else {
            //CustomSuccess("Freelancer Register Successfully");
            localStorage.setItem("emailTobeVerify", email);
            setIsEmailVerificationOpen(true);
          }
        } else {
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register freelancer");
        setAlertDialogVisibility(true);
      });
  };

  const navigateToHomePage = (data) => {
    localStorage.setItem("userId", data.result.Id);
    window.localStorage.removeItem("token");
    localStorage.setItem("token", data.token);
    dispatch(setCurrentUser(data.result));
    // CustomInfo("Login Successfully...");
    window.localStorage.removeItem("emailTobeVerify");
    window.location.reload();
    // window.location.href = "/login";
  };

  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);

  const handleChangelocation = (data, isInput) => {
    console.log("data.geometry", data.geometry);
    if (isNullOrEmpty(data.geometry))
      setLocationErrorMessage("Please enter your location");
    else {
      setLocationErrorMessage("");
      setAddress(data.formatted_address);
      setLat(data.geometry.location.lat());
      setLng(data.geometry.location.lng());
      if (redirectedFromRole == "company") {
        for (let index = 0; index < data.address_components.length; index++) {
          const element = data.address_components[index];
          if (element.types[0] == "country") {
            let languageNames = new Intl.DisplayNames(["en"], {
              type: "region",
            });
            let countryName = languageNames.of(element.short_name);
            const findCountry = countryCodes.find(
              (item) => item.long_name == countryName
            );
            if (findCountry != undefined) setSelectedCountryCode(findCountry);
          }
        }
      }
    }
    setAddress(data.formatted_address);
  };

  const onChangeCountryCode = (event) => {
    setSelectedCountryCode((selectedCountryCode = event));
    setPhoneMinLength(
      (phoneMinLength = parseInt(event.phone_number_length_min))
    );
    setPhoneMaxLength(
      (phoneMaxLength = parseInt(event.phone_number_length_max))
    );
    // onPhoneTextChangeListener(phone);
  };

  const signupWithFacebook = (response) => {
    if (redirectedFromRole === "freelancer") signUpFreelancerFacebook(response);
    else if (redirectedFromRole === "professional")
      signUpProfessionFacebook(response);
  };

  const signUpWithGoogle = (response) => {
    if (redirectedFromRole === "freelancer") signUpFreelancerGoogle(response);
    else if (redirectedFromRole === "professional")
      signUpProfessionGoogle(response);
  };

  const signUpFreelancerFacebook = (response) => {
    setIsLoading(true);
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: response.name,
      LastName: " ",
      FreelancerAddresses: [
        {
          CountryId: null,
          Email: response.email,
          Latitude: null,
          Longitude: null,
          AddressDetail: null,
          PhoneNo: null,
        },
      ],
      Users: [
        {
          RoleId: 3,
          UserName: response.email,
          LoginName: response.name,
          WebToken: firebaseToken,
          CountryId: null,
          Phoneno: null,
          facebookId: response.id,
          Login_Type: "facebook",
        },
      ],
    };
    freelancerSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) {
            localStorage.setItem("userId", data.result.Users[0].Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result.Users[0]));
            // CustomInfo("Login Successfully...");
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            setIsLoading(false);
            setHeadingAndTextForAlertDialog(data.message);
            setAlertDialogVisibility(true);
          }
        } else {
          setIsLoading(false);
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register freelancer");
        setAlertDialogVisibility(true);
      });
  };

  const signUpProfessionFacebook = (response) => {
    setIsLoading(true);
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: response.name,
      LastName: " ",
      JobSeekerAddresses: [
        {
          CountryId: null,
          Email: response.email,
          Latitude: null,
          Longitude: null,
          AddressDetail: null,
          PhoneNo: null,
        },
      ],
      Users: [
        {
          RoleId: 2,
          UserName: response.email,
          LoginName: response.name,
          WebToken: firebaseToken,
          CountryId: null,
          Phoneno: null,
          facebookId: response.id,
          Login_Type: "facebook",
        },
      ],
    };

    employeeSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) {
            localStorage.setItem("userId", data.result.Users[0].Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result.Users[0]));
            // CustomInfo("Login Successfully...");
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            setIsLoading(false);
            setHeadingAndTextForAlertDialog(data.message);
            setAlertDialogVisibility(true);
          }
        } else {
          setIsLoading(false);
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register professional");
        setAlertDialogVisibility(true);
      });
  };

  const signUpFreelancerGoogle = (response) => {
    setIsLoading(true);
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: response.profileObj.givenName,
      LastName: response.profileObj.familyName,
      FreelancerAddresses: [
        {
          CountryId: null,
          Email: response.profileObj.email,
          Latitude: null,
          Longitude: null,
          AddressDetail: null,
          PhoneNo: null,
        },
      ],
      Users: [
        {
          RoleId: 3,
          UserName: response.profileObj.email,
          LoginName: response.profileObj.name,
          WebToken: firebaseToken,
          CountryId: null,
          Phoneno: null,
          Login_Type: "google",
        },
      ],
    };

    freelancerSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) {
            localStorage.setItem("userId", data.result.Users[0].Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result.Users[0]));
            // CustomInfo("Login Successfully...");
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            setIsLoading(false);
            setHeadingAndTextForAlertDialog(data.message);
            setAlertDialogVisibility(true);
          }
        } else {
          setIsLoading(false);
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register freelancer");
        setAlertDialogVisibility(true);
      });
  };

  const signUpProfessionGoogle = (response) => {
    setIsLoading(true);
    const firebaseToken = localStorage.getItem("ft");

    let data = {
      FirstName: response.profileObj.givenName,
      LastName: response.profileObj.familyName,
      FreelancerAddresses: [
        {
          CountryId: null,
          Email: response.profileObj.email,
          Latitude: null,
          Longitude: null,
          AddressDetail: null,
          PhoneNo: null,
        },
      ],
      Users: [
        {
          RoleId: 3,
          UserName: response.profileObj.email,
          LoginName: response.profileObj.name,
          WebToken: firebaseToken,
          CountryId: null,
          Phoneno: null,
          Login_Type: "google",
        },
      ],
    };

    employeeSignUp(data)
      .then(({ data }) => {
        if (data.success) {
          if (data.result != null) {
            localStorage.setItem("userId", data.result.Users[0].Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result.Users[0]));
            // CustomInfo("Login Successfully...");
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            setIsLoading(false);
            setHeadingAndTextForAlertDialog(data.message);
            setAlertDialogVisibility(true);
          }
        } else {
          setIsLoading(false);
          setHeadingAndTextForAlertDialog(data.errors);
          setAlertDialogVisibility(true);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        setHeadingAndTextForAlertDialog("Failed to register professional");
        setAlertDialogVisibility(true);
      });
  };
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <>
      {isLoading ? <LoadingMask /> : null}
      <Head title="AIDApro | Sign Up " description="Sign Up" />
      <Header isOnSignUp={true} setIsLoginOpen={setIsLoginOpen} />
      <div
        className="homepage__container__jumbotron"
        style={{ paddingTop: "8em" }}
      >
        <div className="homepage__container__jumbotron__wrapper">
          <div className="homepage__container__jumbotron__left">
            <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
              Let's get you started <br />
            </div>
            <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                margin: "1em auto",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              <ContactTab
                defaultChecked={redirectedFromRole == "company" ? true : false}
                label="Company"
                onClick={() => {
                  resetViewFields();
                  setRedirectedFromRole("company");
                }}
              />
              <ContactTab
                defaultChecked={
                  redirectedFromRole == "professional" ? true : false
                }
                label="Professional"
                onClick={() => {
                  resetViewFields();
                  setRedirectedFromRole("professional");
                }}
              />
              <ContactTab
                defaultChecked={
                  redirectedFromRole == "freelancer" ? true : false
                }
                label="Freelancer"
                onClick={() => {
                  resetViewFields();
                  setRedirectedFromRole("freelancer");
                }}
              />
            </div>
            <div className="homepage__container__jumbotron__signup__wrapper homepage__container__jumbotron__signup__wrapper__sign">
              <InputBox
                placeholder={
                  redirectedFromRole === "company"
                    ? "Company Name"
                    : "First Name"
                }
                error={firstNameError}
                errorMessage={firstNameErrorMessage}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="13.419"
                    height="17.918"
                    viewBox="0 0 13.419 17.918"
                  >
                    <g id="name" transform="translate(0)">
                      <path
                        id="Path_285"
                        data-name="Path 285"
                        d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                        transform="translate(-138.624 -166.569)"
                        fill="#374957"
                      />
                      <path
                        id="Path_286"
                        data-name="Path 286"
                        d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                        transform="translate(-105.092 -314.857)"
                        fill="#374957"
                      />
                      <path
                        id="Path_287"
                        data-name="Path 287"
                        d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                        transform="translate(4.582)"
                        fill="#374957"
                      />
                      <path
                        id="Path_288"
                        data-name="Path 288"
                        d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                        transform="translate(-138.624 -57.016)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
                name="name"
                value={firstName}
                onChange={onNameTextChangeListener}
                // onBlur={signUpUser}
              />
              <InputBox
                placeholder={
                  redirectedFromRole === "company"
                    ? "Contact Person"
                    : "Last Name"
                }
                error={lastNameError}
                errorMessage={lastNameErrorMessage}
                value={lastName}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="13.419"
                    height="17.918"
                    viewBox="0 0 13.419 17.918"
                  >
                    <g id="name" transform="translate(0)">
                      <path
                        id="Path_285"
                        data-name="Path 285"
                        d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                        transform="translate(-138.624 -166.569)"
                        fill="#374957"
                      />
                      <path
                        id="Path_286"
                        data-name="Path 286"
                        d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                        transform="translate(-105.092 -314.857)"
                        fill="#374957"
                      />
                      <path
                        id="Path_287"
                        data-name="Path 287"
                        d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                        transform="translate(4.582)"
                        fill="#374957"
                      />
                      <path
                        id="Path_288"
                        data-name="Path 288"
                        d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                        transform="translate(-138.624 -57.016)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
                name="contactPerson"
                onChange={onLastNameTextChangeListener}
              />

              <InputBox
                placeholder="Email"
                type="email"
                error={emailError}
                errorMessage={emailErrorMessage}
                svg={
                  <svg
                    id="mail"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17.919"
                    height="11.946"
                    viewBox="0 0 17.919 11.946"
                  >
                    <g id="Group_202" data-name="Group 202">
                      <path
                        id="Path_292"
                        data-name="Path 292"
                        d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                        transform="translate(0 -85.333)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
                value={email}
                name="email"
                onChange={onEmailTextChangeListener}
              />

              {redirectedFromRole !== "company" ? (
                <InputBox
                  placeholder="Phone"
                  variant="phone"
                  type="text"
                  name="phone"
                  options={countryCodes}
                  selectedCountryCode={selectedCountryCode}
                  onChangeCountryCode={onChangeCountryCode}
                  value={phone}
                  error={phoneError}
                  errorMessage={phoneErrorMessage}
                  phoneMinLength={phoneMinLength}
                  phoneMaxLength={phoneMaxLength}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      onPhoneTextChangeListener(event.currentTarget.value);
                    }
                  }}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="13.21"
                      height="13.23"
                      viewBox="0 0 13.21 13.23"
                    >
                      <g id="phone-call" transform="translate(-0.344 0)">
                        <g
                          id="Group_201"
                          data-name="Group 201"
                          transform="translate(0.344 0)"
                        >
                          <path
                            id="Path_289"
                            data-name="Path 289"
                            d="M10.79,34.836a1.3,1.3,0,0,0-.944-.433,1.346,1.346,0,0,0-.955.43l-.882.88c-.073-.039-.145-.075-.215-.112-.1-.05-.2-.1-.276-.148a9.59,9.59,0,0,1-2.3-2.094,5.657,5.657,0,0,1-.754-1.189c.229-.209.441-.427.648-.637l.235-.237a1.281,1.281,0,0,0,0-1.932L4.586,28.6c-.087-.087-.176-.176-.26-.265-.168-.173-.343-.352-.525-.519a1.322,1.322,0,0,0-.935-.41,1.368,1.368,0,0,0-.949.41l-.006.006-.949.958a2.043,2.043,0,0,0-.606,1.3A4.895,4.895,0,0,0,.713,32.15a12.022,12.022,0,0,0,2.136,3.563,13.14,13.14,0,0,0,4.375,3.426,6.817,6.817,0,0,0,2.457.726c.059,0,.12.006.176.006a2.1,2.1,0,0,0,1.611-.692c0-.006.008-.008.011-.014a6.348,6.348,0,0,1,.489-.505c.12-.114.243-.235.363-.36a1.393,1.393,0,0,0,.422-.966,1.342,1.342,0,0,0-.43-.958Zm1,2.94s0,0,0,0c-.109.117-.221.223-.341.341a7.341,7.341,0,0,0-.539.558,1.346,1.346,0,0,1-1.05.444c-.042,0-.087,0-.128,0a6.056,6.056,0,0,1-2.178-.653,12.4,12.4,0,0,1-4.121-3.228,11.336,11.336,0,0,1-2.01-3.348A3.987,3.987,0,0,1,1.11,30.14,1.282,1.282,0,0,1,1.5,29.31l.952-.952a.635.635,0,0,1,.424-.2.6.6,0,0,1,.408.2l.008.008c.17.159.332.324.5.5l.265.271.762.762a.533.533,0,0,1,0,.866c-.081.081-.159.162-.24.24-.235.24-.458.464-.7.681-.006.006-.011.008-.014.014a.57.57,0,0,0-.145.634l.008.025a6.12,6.12,0,0,0,.9,1.471l0,0A10.248,10.248,0,0,0,7.11,36.087a3.811,3.811,0,0,0,.343.187c.1.05.2.1.276.148l.034.02a.605.605,0,0,0,.276.07.6.6,0,0,0,.424-.193l.955-.955a.632.632,0,0,1,.422-.209.569.569,0,0,1,.4.2l.006.006L11.787,36.9A.552.552,0,0,1,11.79,37.776Z"
                            transform="translate(-0.344 -26.641)"
                            fill="#374957"
                          />
                          <path
                            id="Path_290"
                            data-name="Path 290"
                            d="M245.254,86.673A3.594,3.594,0,0,1,248.18,89.6a.375.375,0,0,0,.371.313.5.5,0,0,0,.064-.006.377.377,0,0,0,.31-.436,4.345,4.345,0,0,0-3.541-3.541.379.379,0,0,0-.436.307A.373.373,0,0,0,245.254,86.673Z"
                            transform="translate(-238.114 -83.526)"
                            fill="#374957"
                          />
                          <path
                            id="Path_291"
                            data-name="Path 291"
                            d="M255.043,5.836a7.155,7.155,0,0,0-5.83-5.83.376.376,0,1,0-.123.743,6.391,6.391,0,0,1,5.21,5.21.375.375,0,0,0,.371.313.5.5,0,0,0,.064-.006A.37.37,0,0,0,255.043,5.836Z"
                            transform="translate(-241.839 0)"
                            fill="#374957"
                          />
                        </g>
                      </g>
                    </svg>
                  }
                />
              ) : null}

              {redirectedFromRole === "company" ? (
                <InputBox
                  variant="select"
                  placeholder="Industry"
                  style={{ minWidth: "32% " }}
                  options={industryTypeDropDownData}
                  error={industryError}
                  errorMessage={industryErrorMessage}
                  onChange={(e) => industryChange(e)}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="12.499"
                      height="16.008"
                      viewBox="0 0 12.499 16.008"
                    >
                      <g id="germany" transform="translate(0)">
                        <path
                          id="Path_654"
                          data-name="Path 654"
                          d="M215.5,352h.5v.47h-.5Z"
                          transform="translate(-210.504 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_655"
                          data-name="Path 655"
                          d="M247.5,352h.5v.47h-.5Z"
                          transform="translate(-241.501 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_656"
                          data-name="Path 656"
                          d="M279.5,352h.5v.47h-.5Z"
                          transform="translate(-272.499 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_657"
                          data-name="Path 657"
                          d="M67.788,6.861A6.3,6.3,0,0,0,65.9,4.682l-.274.381a5.839,5.839,0,0,1,1.413,1.453H65.457a6.863,6.863,0,0,0-.542-1.16,4.631,4.631,0,0,0,.577-2.114,3.242,3.242,0,1,0-6.485,0,4.631,4.631,0,0,0,.577,2.114,6.865,6.865,0,0,0-.542,1.16H57.465a5.839,5.839,0,0,1,1.413-1.453L58.6,4.682a6.248,6.248,0,1,0,9.185,2.179Zm-5.774,8.662a2.958,2.958,0,0,1-2.043-1.63A6.32,6.32,0,0,1,59.544,13h2.47Zm.47,0V13h2.47a6.318,6.318,0,0,1-.427.892A2.958,2.958,0,0,1,62.485,15.523Zm0-2.992v-.517h-.47v.517H59.38a9.23,9.23,0,0,1-.4-2.537h3.036v.517h.47V9.993H65.52a9.23,9.23,0,0,1-.4,2.537ZM56.475,9.993h2.034a9.791,9.791,0,0,0,.38,2.537H57.18a5.74,5.74,0,0,1-.7-2.537Zm4.21-3.007a12.5,12.5,0,0,0,1.329,1.376V9.523H58.979a9.321,9.321,0,0,1,.4-2.537Zm3.129,0h1.3a9.321,9.321,0,0,1,.4,2.537H62.485V8.362A12.5,12.5,0,0,0,63.814,6.986ZM65.99,9.993h2.034a5.74,5.74,0,0,1-.7,2.537H65.61A9.791,9.791,0,0,0,65.99,9.993Zm1.331-3.007c.017.031.034.062.051.093a5.79,5.79,0,0,1,.652,2.444H65.99a9.772,9.772,0,0,0-.379-2.537Zm-2.369-.47h-.77q.256-.345.462-.681A6.478,6.478,0,0,1,64.953,6.516ZM62.25.47a2.776,2.776,0,0,1,2.772,2.772c0,2.051-2.175,4.159-2.772,4.7-.6-.54-2.772-2.646-2.772-4.7A2.776,2.776,0,0,1,62.25.47ZM59.855,5.835q.206.336.462.681h-.77A6.477,6.477,0,0,1,59.855,5.835ZM57.127,7.079c.016-.031.034-.062.051-.093h1.71a9.773,9.773,0,0,0-.379,2.537H56.475A5.789,5.789,0,0,1,57.127,7.079ZM57.467,13h1.576a6.9,6.9,0,0,0,.523,1.129,4.727,4.727,0,0,0,.884,1.121A5.806,5.806,0,0,1,57.467,13Zm6.583,2.25a4.728,4.728,0,0,0,.884-1.121A6.9,6.9,0,0,0,65.456,13h1.576A5.806,5.806,0,0,1,64.05,15.25Z"
                          transform="translate(-56)"
                          fill="#374957"
                        />
                        <path
                          id="Path_658"
                          data-name="Path 658"
                          d="M188.48,34.24a2.24,2.24,0,1,0-2.24,2.24A2.242,2.242,0,0,0,188.48,34.24Zm-2.24-1.77a1.77,1.77,0,0,1,1.469.783h-2.937A1.77,1.77,0,0,1,186.24,32.47Zm-1.693,1.253h3.386a1.769,1.769,0,0,1,0,1.034h-3.386a1.769,1.769,0,0,1,0-1.034Zm.224,1.5h2.937a1.769,1.769,0,0,1-2.937,0Z"
                          transform="translate(-179.991 -30.998)"
                          fill="#374957"
                        />
                      </g>
                    </svg>
                  }
                />
              ) : null}

              <InputBox
                variant="location"
                style={{ minWidth: "100% " }}
                placeholder="Location"
                type="text"
                error={locationError}
                errorMessage={locationErrorMessage}
                svg={
                  <svg
                    id="pin"
                    xmlns="http://www.w3.org/2000/svg"
                    width="7.983"
                    height="11.61"
                    viewBox="0 0 7.983 11.61"
                  >
                    <path
                      id="Path_280"
                      data-name="Path 280"
                      d="M87.4,1.988A3.94,3.94,0,0,0,84.049,0c-.059,0-.119,0-.179,0a3.94,3.94,0,0,0-3.348,1.987,4.042,4.042,0,0,0-.053,3.994l2.879,5.269,0,.007a.7.7,0,0,0,1.214,0l0-.007,2.879-5.269A4.042,4.042,0,0,0,87.4,1.988ZM83.959,5.26a1.633,1.633,0,1,1,1.633-1.633A1.634,1.634,0,0,1,83.959,5.26Z"
                      transform="translate(-79.968 0)"
                      fill="#374957"
                    />
                  </svg>
                }
                value={address}
                onChange={(e) => handleChangelocation(e, true)}
                onSelected={(e) => handleChangelocation(e, false)}
              />
              <InputBox
                placeholder="Password"
                type="password"
                secureKey={true}
                error={passwordError}
                errorMessage={passwordErrorMessage}
                svg={
                  <svg
                    id="padlock"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.192"
                    height="14.922"
                    viewBox="0 0 11.192 14.922"
                  >
                    <path
                      id="Path_293"
                      data-name="Path 293"
                      d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                      transform="translate(-3 -3.404)"
                      fill="#374957"
                    />
                    <path
                      id="Path_294"
                      data-name="Path 294"
                      d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                      transform="translate(-4.135)"
                      fill="#374957"
                    />
                    <path
                      id="Path_295"
                      data-name="Path 295"
                      d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                      transform="translate(-5.648 -4.917)"
                      fill="#374957"
                    />
                    <path
                      id="Path_296"
                      data-name="Path 296"
                      d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                      transform="translate(-6.12 -5.957)"
                      fill="#374957"
                    />
                  </svg>
                }
                value={password}
                name="password"
                onChange={onPasswordTextChangeListener}
              />
              <InputBox
                placeholder="Confirm Password"
                type="password"
                value={confirmPassword}
                secureKey={true}
                error={confirmPasswordError}
                errorMessage={confirmPasswordErrorMessage}
                svg={
                  <svg
                    id="padlock"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.192"
                    height="14.922"
                    viewBox="0 0 11.192 14.922"
                  >
                    <path
                      id="Path_293"
                      data-name="Path 293"
                      d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                      transform="translate(-3 -3.404)"
                      fill="#374957"
                    />
                    <path
                      id="Path_294"
                      data-name="Path 294"
                      d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                      transform="translate(-4.135)"
                      fill="#374957"
                    />
                    <path
                      id="Path_295"
                      data-name="Path 295"
                      d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                      transform="translate(-5.648 -4.917)"
                      fill="#374957"
                    />
                    <path
                      id="Path_296"
                      data-name="Path 296"
                      d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                      transform="translate(-6.12 -5.957)"
                      fill="#374957"
                    />
                  </svg>
                }
                name="confirmPassword"
                onChange={(event) =>
                  onConfirmPasswordTextChangeListener(event.currentTarget.value)
                }
              />

              <div
                className="homepage__container__jumbotron__form__filters__role"
                style={{ marginLeft: 10 }}
              >
                <input
                  className="styled-checkbox"
                  id="styled-checkbox-freelancer-role"
                  type="checkbox"
                  checked={rememberMe}
                  onChange={(e) => {
                    setRememberMe(!rememberMe);
                  }}
                  name="Remember"
                />
                <label htmlFor="styled-checkbox-freelancer-role">
                  I agree to AIDApro’s{" "}
                  <Link
                    to="/terms-conditions"
                    className="homepage__container__jumbotron__form__filters__role__link"
                    style={{ color: "#0ca69d" }}
                  >
                    Terms & Conditions
                  </Link>{" "}
                  &{" "}
                  <Link
                    to="/privacy-policy"
                    className="homepage__container__jumbotron__form__filters__role__link"
                    style={{ color: "#0ca69d" }}
                  >
                    Privacy Policy
                  </Link>
                </label>
              </div>
              <div className="homepage__container__jumbotron__signup__button">
                <button
                  onClick={() => signUpUser()}
                  disabled={isLoading ? true : false}
                  className="header__nav__btn btn__primary"
                  style={{ width: 150, height: 45, marginRight: 0 }}
                  title="sign up"
                >
                  {isLoading ? "Loading..." : "Create account"}
                </button>
              </div>
            </div>
            {redirectedFromRole === "company" ? (
              <div style={{ marginBottom: -10 }}></div>
            ) : (
              <>
                <div
                  className="homepage__container__jumbotron__left__border"
                  style={{ color: "#0ca69d" }}
                >
                  Or sign up with
                </div>

                <div
                  className="homepage__container__jumotron__left__links"
                  style={{ marginBottom: -30 }}
                >
                  <FacebookLogin
                    appId="4732603883483692"
                    autoLoad={false}
                    fields="name,email,picture"
                    scope="public_profile, email"
                    callback={signupWithFacebook}
                    buttonStyle={{
                      backgroundColor: "transparent",
                      borderWidth: 0,
                      marginRight: -20,
                      marginLeft: -20,
                    }}
                    textButton=""
                    icon={
                      <SocialLinkCard
                        svg={
                          <svg
                            id="facebook_2_"
                            data-name="facebook (2)"
                            xmlns="http://www.w3.org/2000/svg"
                            width="25.99"
                            height="25.99"
                            viewBox="0 0 25.99 25.99"
                          >
                            <path
                              id="Path_659"
                              data-name="Path 659"
                              d="M22.741,0H3.249A3.252,3.252,0,0,0,0,3.249V22.741A3.252,3.252,0,0,0,3.249,25.99H22.741a3.252,3.252,0,0,0,3.249-3.249V3.249A3.252,3.252,0,0,0,22.741,0Z"
                              fill="#1976d2"
                            />
                            <path
                              id="Path_660"
                              data-name="Path 660"
                              d="M204.183,104.122h-4.061v-3.249c0-.9.728-.812,1.624-.812h1.624V96h-3.249a4.873,4.873,0,0,0-4.873,4.873v3.249H192v4.061h3.249v8.934h4.873v-8.934h2.437Z"
                              transform="translate(-182.254 -91.127)"
                              fill="#fafafa"
                            />
                          </svg>
                        }
                      />
                    }
                  />
                  <GoogleLogin
                    clientId={
                      process.env.REACT_APP_NAME == "production"
                        ? "380324339331-b1jcpu3op2mrk96n53pltv4jpe469l7o.apps.googleusercontent.com"
                        : "380324339331-e6hhkf07flvdfvn2u83a80hq0nohh0at.apps.googleusercontent.com"
                    }
                    render={(renderProps) => {
                      return (
                        <SocialLinkCard
                          svg={
                            <svg
                              id="google"
                              xmlns="http://www.w3.org/2000/svg"
                              width="25.99"
                              height="25.99"
                              viewBox="0 0 25.99 25.99"
                            >
                              <path
                                id="Path_661"
                                data-name="Path 661"
                                d="M278.579,211.48h-10.6a.847.847,0,0,0-.848.847v3.387a.848.848,0,0,0,.848.848h5.97a7.97,7.97,0,0,1-3.43,4.02l2.546,4.407a12.742,12.742,0,0,0,6.5-11.144,8.59,8.59,0,0,0-.146-1.664A.851.851,0,0,0,278.579,211.48Z"
                                transform="translate(-253.571 -200.744)"
                                fill="#167ee6"
                              />
                              <path
                                id="Path_662"
                                data-name="Path 662"
                                d="M45.659,337.805a7.908,7.908,0,0,1-6.842-3.958l-4.406,2.54a12.985,12.985,0,0,0,17.745,4.763v-.006l-2.546-4.407A7.851,7.851,0,0,1,45.659,337.805Z"
                                transform="translate(-32.664 -316.901)"
                                fill="#12b347"
                              />
                              <path
                                id="Path_663"
                                data-name="Path 663"
                                d="M262.5,395.192v-.006l-2.546-4.407A7.851,7.851,0,0,1,256,391.847v5.085A12.973,12.973,0,0,0,262.5,395.192Z"
                                transform="translate(-243.005 -370.943)"
                                fill="#0f993e"
                              />
                              <path
                                id="Path_664"
                                data-name="Path 664"
                                d="M5.085,134.61a7.851,7.851,0,0,1,1.068-3.952l-4.406-2.54a12.935,12.935,0,0,0,0,12.983l4.406-2.54A7.851,7.851,0,0,1,5.085,134.61Z"
                                transform="translate(0 -121.616)"
                                fill="#ffd500"
                              />
                              <path
                                id="Path_665"
                                data-name="Path 665"
                                d="M45.659,5.085a7.873,7.873,0,0,1,5.022,1.8.844.844,0,0,0,1.136-.051l2.4-2.4a.854.854,0,0,0-.049-1.249A12.966,12.966,0,0,0,34.411,6.5l4.406,2.54A7.908,7.908,0,0,1,45.659,5.085Z"
                                transform="translate(-32.664 0)"
                                fill="#ff4b26"
                              />
                              <path
                                id="Path_666"
                                data-name="Path 666"
                                d="M261.022,6.888a.844.844,0,0,0,1.136-.051l2.4-2.4a.854.854,0,0,0-.049-1.249A12.956,12.956,0,0,0,256,0V5.085A7.873,7.873,0,0,1,261.022,6.888Z"
                                transform="translate(-243.005 0)"
                                fill="#d93f21"
                              />
                            </svg>
                          }
                        />
                      );
                    }}
                    // onSuccess={signUpWithGoogle}
                    // onFailure={signUpWithGoogle}
                    isSignedIn={false}
                    cookiePolicy={"single_host_origin"}
                  />

                  {/* <LinkedIn
                        clientId="772mw1ch0ax915"
                        callback={linkedInLogin}
                        // className={styles.linkedin}
                        scope={["r_liteprofile", "r_emailaddress"]}
                        text="Login With LinkedIn"
                      /> */}

                  {/* <SocialLinkCard
                        onClick={() => linkedInLogin()}
                        svg={
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="25.99"
                            height="25.99"
                            viewBox="0 0 25.99 25.99"
                          >
                            <path
                              id="linkedin"
                              d="M23.639,0H2.351A2.351,2.351,0,0,0,0,2.351V23.639A2.351,2.351,0,0,0,2.351,25.99H23.639a2.351,2.351,0,0,0,2.351-2.351V2.351A2.351,2.351,0,0,0,23.639,0ZM8.042,22.441a.684.684,0,0,1-.684.684H4.446a.684.684,0,0,1-.684-.684V10.233a.684.684,0,0,1,.684-.684H7.358a.684.684,0,0,1,.684.684ZM5.9,8.4A2.767,2.767,0,1,1,8.669,5.631,2.767,2.767,0,0,1,5.9,8.4Zm17.36,14.1a.629.629,0,0,1-.629.629H19.508a.629.629,0,0,1-.629-.629V16.77c0-.854.251-3.743-2.232-3.743-1.926,0-2.317,1.978-2.4,2.865v6.6a.629.629,0,0,1-.629.629H10.6a.629.629,0,0,1-.629-.629V10.178a.629.629,0,0,1,.629-.629h3.023a.629.629,0,0,1,.629.629v1.065a4.29,4.29,0,0,1,4.035-1.9c5,0,4.976,4.675,4.976,7.244V22.5Z"
                              fill="#0077b7"
                            />
                          </svg>
                        }
                      /> */}
                </div>
              </>
            )}
            <div
              className="homepage__container__jumbotron__left__border"
              style={{
                color: "#242424",
                marginBottom: redirectedFromRole === "company" ? 150 : 50,
              }}
            >
              Already have an account?
              <a
                style={{
                  color: "#0ca69d",
                  cursor: "pointer",
                  marginLeft: ".3em",
                }}
                onClick={() => {
                  setIsLoginOpen(true);
                }}
              >
                Login
              </a>
            </div>
          </div>
          <Img
            loading="lazy"
            src={HomeSignupSvg}
            alt="HomeSignupSvg"
            style={{ height: "50%", marginTop: "3em" }}
            className="homepage__container__jumbotron__right animate__animated animate__fadeInRight"
          />
        </div>
      </div>
    </>
  );
};
export default SignUp;
