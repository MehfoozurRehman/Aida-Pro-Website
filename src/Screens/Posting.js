import React, { useState, useEffect } from "react";
import Pagination from "react-js-pagination";
import { companyGetAllJobProject } from "../API/CompanyAPI";
import { useSelector } from "react-redux";
import { postingSelected } from "Assets";
import { Head, DashboardHeading, PostingList } from "Components";
import NoData from "Components/NoData";
import PostingHeader from "Components/PostingHeader";
import PostingTableHeader from "Components/PostingTableHeader";

const Posting = ({ setDontShowSidebar }) => {
  useEffect(() => {
    localStorage.removeItem("isApplicant");
    localStorage.removeItem("isInterested");
    localStorage.removeItem("isVisitor");
    localStorage.removeItem("JobId");
    localStorage.removeItem("JobType");
    setDontShowSidebar(false);
  }, []);

  const [postingFilterStatus, setPostingFilterStatus] = useState("All");
  let [postingTableData, setPostingTableData] = useState([]);
  let [postingAllTableData, setPostingAllTableData] = useState([]);
  const [activeTab, setActiveTab] = useState("All");
  const [isLoading, setIsLoading] = useState(false);
  const [searchByjobSeeker, setSearchByJobSeeker] = useState("Jobs");
  const [searchByFreelancer, setSearchByFreelancer] = useState("Projects");
  const [filterJobSeeker, setFilterJobSeeker] = useState(true);
  const [filterFreelancer, setFilterFreelancer] = useState(true);
  const [reloadData, setReloadData] = useState(true);
  const { company } = useSelector((state) => state.company);
  const [pageLimit, setPageLimit] = useState(6);
  const [pageNo, setPageNo] = useState(1);
  const [totalRecords, setTotalRecords] = useState(0);
  const [isJobsShow, setIsJobsShow] = useState(true);
  const [isJProjectsShow, setIsJProjectsShow] = useState(true);

  const handleReloadData = () => {
    setReloadData(!reloadData);
    setActiveTab(4);
  };

  const companyGetAllJobProjectAPI = (compId, isJob, isProject, pageNumber) => {
    setIsLoading(true);
    let jobs;
    let projects;
    if (isJob) {
      jobs = searchByjobSeeker;
    }
    if (isProject) {
      projects = searchByFreelancer;
    }
    companyGetAllJobProject(compId, jobs, projects, pageLimit, pageNumber)
      .then(({ data }) => {
        if (data.success) {
          setPostingTableData([]);
          setPostingTableData((postingTableData = data.result));
          setPostingAllTableData([]);
          setPostingAllTableData((postingAllTableData = data.result));
          setPageNo(pageNumber);
          if (postingFilterStatus === "All") {
            filterPostingData(postingFilterStatus);
          } else {
            filterPostingData(postingFilterStatus);
          }
          setTotalRecords(data.totalRecords);
        } else {
          setPostingTableData([]);
          setPostingAllTableData([]);
        }
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    handlePageChange(1, isJobsShow, isJProjectsShow);
  }, [company, filterJobSeeker, filterFreelancer, reloadData]);

  const filterPostingData = (postingFilterStatuss) => {
    const sourceTableData = postingAllTableData;

    let tableData;
    if (postingFilterStatuss === "All") {
      tableData = postingAllTableData;
    } else {
      tableData = sourceTableData.filter((e) => {
        const includeData = () => {
          if (
            e &&
            e.StatusTypeLookupDetail &&
            e.StatusTypeLookupDetail.Title === postingFilterStatuss
          ) {
            return true;
          }
          return false;
        };
        return includeData();
      });
    }
    setPostingTableData(tableData);
  };

  function handleTabChange(postingFilterStatuss) {
    if (postingFilterStatus === postingFilterStatuss) return;
    setPostingFilterStatus(postingFilterStatuss);
    // filterPostingData(postingFilterStatuss);
    setReloadData(!reloadData);
  }

  const handlePageChange = (pageNumber, isJob, isProject) => {
    let compId = company.CompanyProfileId;
    if (compId) {
      companyGetAllJobProjectAPI(compId, isJob, isProject, pageNumber);
    }
  };

  const changeCheckBox = (event) => {
    if (event.target.name === "job") {
      if (!event.target.checked) {
        setIsJProjectsShow(true);
        setIsJobsShow(event.target.checked);
        handlePageChange(pageNo, event.target.checked, true);
      } else {
        setIsJobsShow(event.target.checked);
        handlePageChange(pageNo, event.target.checked, isJProjectsShow);
      }
    } else if (event.target.name === "project") {
      if (!event.target.checked) {
        setIsJobsShow(true);
        setIsJProjectsShow(event.target.checked);
        handlePageChange(pageNo, true, event.target.checked);
      } else {
        setIsJProjectsShow(event.target.checked);
        handlePageChange(pageNo, isJobsShow, event.target.checked);
      }
    }
  };

  return (
    <>
      <Head title="AIDApro | Posting" description="Posting" />
      <div className="posting__container">
        <DashboardHeading heading="Postings" svg={postingSelected} />
        <PostingHeader
          handleTabChange={handleTabChange}
          isJobsShow={isJobsShow}
          changeCheckBox={changeCheckBox}
          isJProjectsShow={isJProjectsShow}
        />
        <div className="posting__container__table">
          <PostingTableHeader />
          {postingTableData.length > 0 ? (
            <>
              <div className="posting__container__content">
                <PostingList
                  data={postingTableData}
                  handleReloadData={handleReloadData}
                  postingFilterStatus={postingFilterStatus}
                />
              </div>
              <div className="posting__container__pagination">
                <Pagination
                  activePage={pageNo}
                  itemsCountPerPage={pageLimit}
                  totalItemsCount={totalRecords}
                  pageRangeDisplayed={5}
                  onChange={(e) =>
                    handlePageChange(e, isJobsShow, isJProjectsShow)
                  }
                />
              </div>
            </>
          ) : (
            <NoData />
          )}
        </div>
      </div>
    </>
  );
};

export default Posting;
