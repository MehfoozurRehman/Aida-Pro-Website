import React from "react";
import Card from "../../Shared/Card/Card.component";
import { faMicrophoneSlash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Participant.css";

export const Participant = (props) => {
  const {
    curentIndex,
    currentParticipant,
    hideVideo,
    videoRef,
    showAvatar,
    currentUser,
    userImage,
  } = props;
  if (!currentParticipant) return null;

  return (
    <div className={`participant ${hideVideo ? "hide" : ""}`}>
      <Card>
        <video
          ref={videoRef}
          className="video"
          id={`participantVideo${curentIndex}`}
          autoPlay
          playsInline
        ></video>
        {!currentParticipant.audio && (
          <FontAwesomeIcon
            className="muted"
            icon={faMicrophoneSlash}
            title="Muted"
          />
        )}
        {showAvatar && (
          <div
            style={{ background: currentParticipant.avatarColor }}
            className="avatar"
          >
            {currentParticipant.name[0]}
          </div>
          // (userImage != null ?
          //   <Img loading="lazy"
          //     style={{ background: currentParticipant.avatarColor }}
          //     className="avatar"
          //     src={process.env.REACT_APP_BASEURL.concat(userImage)}
          //   />
          //   :
          //   <div
          //     style={{ background: currentParticipant.avatarColor }}
          //     className="avatar"
          //   >
          //     {currentParticipant.name[0]}
          //   </div>
          // )
        )}
        <div className="name">
          {currentParticipant.name}
          {currentUser ? "(You)" : ""}
        </div>
      </Card>
    </div>
  );
};
