import React, { useEffect } from "react";
import { Head, InputBox } from "Components";
import { crossSvg } from "Assets";

export default function EditCertifications({
  setIsEditCertificationsOpen,
  editCertificationData,
}) {

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <>
      <Head
        title="AIDApro | Edit Certification"
        description="Edit Certification"
      />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsEditCertificationsOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="86.122"
                height="98.621"
                viewBox="0 0 86.122 98.621"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                </defs>
                <g
                  id="Page-1"
                  transform="matrix(0.574, -0.819, 0.819, 0.574, -25.423, 57.352)"
                >
                  <g
                    id="_001---Degree"
                    data-name="001---Degree"
                    transform="translate(-0.018 31.049)"
                  >
                    <path
                      id="Shape"
                      d="M52.125,34.2a34.5,34.5,0,0,1,1.181,7.662,11.368,11.368,0,0,0-11.4,1.06,44.259,44.259,0,0,0-1.125-8.735q2.977.062,6.106.062C48.673,34.245,50.412,34.226,52.125,34.2Zm-3.6,26.261a8.191,8.191,0,1,1,8.191-8.191A8.191,8.191,0,0,1,48.521,60.457ZM1.012,45.713A45.948,45.948,0,0,1,2.65,31.788,45.948,45.948,0,0,1,4.288,45.713,45.948,45.948,0,0,1,2.65,59.638,45.948,45.948,0,0,1,1.012,45.713ZM5.964,59.918a54.329,54.329,0,0,0,1.6-14.205,54.341,54.341,0,0,0-1.6-14.207A287.439,287.439,0,0,0,37.355,34.1a42.172,42.172,0,0,1,1.337,11.614c0,.238,0,.464-.01.691a11.35,11.35,0,0,0-.528,10.734l-.072.174A285.5,285.5,0,0,0,5.964,59.918ZM42.635,71.925l-1.2-2.752a1.638,1.638,0,0,0-2.148-.852L36.534,69.54l3.842-9.21a11.456,11.456,0,0,0,5.786,3.162Zm15.116-3.6a1.638,1.638,0,0,0-2.148.852L54.391,71.9l-3.512-8.409a11.468,11.468,0,0,0,5.791-3.163L60.5,69.5Zm33.037-7.946a265.314,265.314,0,0,0-31.782-2.949l-.116-.282a11.4,11.4,0,0,0-2.208-12.919,45.6,45.6,0,0,0-1.17-10.093,273.463,273.463,0,0,0,35.277-3.083c.76,1.291,1.966,6.4,1.966,14.664S91.54,59.084,90.788,60.375Z"
                      transform="translate(-0.982 -31.049)"
                      fill="url(#linear-gradient)"
                    />
                  </g>
                </g>
              </svg>

              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Edit Certifications
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  Edit Your Certifications History here
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox placeholder="Certification Name" />
                <InputBox placeholder="University / Institute" />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox placeholder="Certificate ID" />
                <InputBox placeholder="Certificate Verification URL" />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <div
                  className="homepage__container__jumbotron__form__filters__role"
                  style={{ marginLeft: 10 }}
                >
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-credential-does-not-expire"
                    type="checkbox"
                    value="Remember"
                    name="Remember"
                  />
                  <label htmlFor="styled-checkbox-credential-does-not-expire">
                    This certificate does not expire
                  </label>
                </div>
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox variant="date" placeholder="Issue Date" />
                <InputBox variant="date" placeholder="Expiration Date" />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  setIsEditCertificationsOpen(false);
                }}
                title="edit certification"
              >
                Edit Certification
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
