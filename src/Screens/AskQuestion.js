import React, { useState, useContext, useEffect } from "react";
import { Head, InputBox } from "Components";
import Img from "react-cool-img";
import { questionPost } from "API/Question";
import UserContext from "Context/UserContext";
import moment from "moment";
import { CustomError } from "./Toasts";
import { getTags } from "API/Tags";
import { askQuestion, crossSvg } from "Assets";

export default function AskQuestion({
  setIsAskQuestionOpen,
  getAskQuestionData,
}) {
  const user = useContext(UserContext);

  const [questionStatement, setQuestionStatement] = useState("");
  const [description, setDescription] = useState("");
  const [isSelected, setIsSelected] = useState(false);
  const [tagsList, setTagsList] = useState([]);
  let [selectedTags, setSelectedTags] = useState([]);
  // validation
  const [questionStatementError, setQuetionStatementError] = useState(false);
  const [questionStatementErrorMessage, setQuetionStatementErrorMessage] =
    useState("");
  const [questionMessageError, setQuetionMessageError] = useState(false);
  const [questionMessageErrorMessage, setQuetionMessageErrorMessage] =
    useState("");
  const [tagsError, setTagsError] = useState(false);
  const [tagsErrorMessage, setTagsErrorMessage] = useState("");
  useEffect(() => {
    getTagsList();
  }, []);

  const getTagsList = () => {
    getTags(10)
      .then(({ data }) => {
        let options = data.result.map((e) => ({
          label: e.Title,
          value: e.TagId,
          TagId: e.TagId,
          Title: e.Title,
          Description: e.Description,
          ActionTypeId: 0,
          CreatedById: null,
          CreatedOn: null,
          ForumTags: e.ForumTags,
        }));

        setTagsList(options);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
  };

  const onClickPress = () => {
    getAskQuestionData(false);
    if (
      questionStatement === "" ||
      description.length === 0 ||
      selectedTags.length == 0
    ) {
      if (questionStatement === "") {
        setQuetionStatementError(true);
        setQuetionStatementErrorMessage("Please enter question");
      } else if (description.length === 0) {
        setQuetionMessageError(true);
        setQuetionMessageErrorMessage("Please enter description");
      } else if (selectedTags.length === 0) {
        setTagsError(true);
        setTagsErrorMessage("Please select atleast one tag");
      }
    } else {
      let currentDate = new Date();
      let askQuestionObject = {
        Subject: questionStatement,
        Description: description,
        CreatedById: user.Id,
        PostedOn: moment(currentDate).format("YYYY-MM-DD"),
        PostedBy: user.Id,
        CreatedOn: moment(currentDate).format("YYYY-MM-DD"),
        ForumTags: selectedTags,
      };

      if (user.Id) {
        questionPost(askQuestionObject)
          .then(({ data }) => {
            if (data.success) {
              getAskQuestionData(true);
              setIsAskQuestionOpen(false);
            }
          })
          .catch((err) => {
            // console.log("Err", err);
          });
      } else CustomError("You are not logged-in to ask a question.");
    }
  };

  const addRemoveTags = (item) => {
    const newTagsList = [...tagsList];
    const targetIndex = newTagsList.findIndex((e) => e.TagId === item.TagId);
    if (!item.isSelected) {
      selectedTags.push(item);
      newTagsList[targetIndex].isSelected = true;
      setTagsList(newTagsList);
    } else {
      const newArray = selectedTags.filter((x) => x.TagId != item.TagId);
      setSelectedTags(newArray);
      newTagsList[targetIndex].isSelected = false;
      setTagsList(newTagsList);
    }
  };

  const handleSelectTags = (event) => {
    setSelectedTags(event);
    setTagsError(false);
    setTagsErrorMessage("");
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Ask Question" description="Ask Question" />
      <div className="pupup__container">
        <div className="pupup__container__from animate__animated animate__slideInDown">
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsAskQuestionOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div className="pupup__container__from__top__left">
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px" }}
              >
                Ask questions
              </div>
              <InputBox
                placeholder="Question Statement Here"
                type="text"
                error={questionStatementError}
                errorMessage={questionStatementErrorMessage}
                onChange={(e) => {
                  if (e.target.value === "") {
                    setQuetionStatementError(true);
                    setQuetionStatementErrorMessage("Please enter question");
                  } else {
                    setQuetionStatementError(false);
                    setQuetionStatementErrorMessage("");
                    setQuestionStatement(e.currentTarget.value);
                  }
                }}
              />
              <InputBox
                variant="select"
                placeholder="Question tags"
                options={tagsList}
                value={selectedTags}
                isMulti={true}
                onChange={handleSelectTags}
                error={tagsError}
                errorMessage={tagsErrorMessage}
              />
              <InputBox
                placeholder="Description"
                variant="textarea"
                error={questionMessageError}
                errorMessage={questionMessageErrorMessage}
                onChange={(e) => {
                  if (e.length === 0) {
                    setQuetionMessageError(true);
                    setQuetionMessageErrorMessage("Please enter description");
                  } else {
                    setQuetionMessageError(false);
                    setQuetionMessageErrorMessage("");
                    setDescription(e);
                  }
                }}
              />

              <button
                type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  marginTop: "2em",
                  marginLeft: "auto",
                  marginRight: "auto",
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  onClickPress();
                }}
                title="post question"
              >
                Post your question
              </button>
            </div>
            <Img
              loading="lazy"
              src={askQuestion}
              alt="askQuestion"
              className="pupup__container__from__top__right"
            />
          </div>
        </div>
      </div>
    </>
  );
}
