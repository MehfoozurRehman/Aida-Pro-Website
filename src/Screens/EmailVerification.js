import React, { useEffect, useState } from "react";
import { CustomError, CustomInfo } from "./Toasts";
import { verifyUser, validateCode, forgetPassword } from "../API/Users";
import { setCurrentUser } from "../Redux/Actions/AppActions";
import { useDispatch } from "react-redux";
import { Head, InputBox } from "Components";
import { isNullOrEmpty } from "Utils/TextUtils";
import { crossSvg } from "Assets";

export default function EmailVerification({
  setIsEmailVerificationOpen,
  setIsChangePasswordOpen,
  setIsForgotPasswordOpen,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
  setIsNewPasswordOpen,
  isFromForgotPassword,
  setIsFromForgotPassword,
}) {
  const [verifyCode, setVerifyCode] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [resendCodeLoading, setResendCodeLoading] = useState(false);

  // validation
  var [verificationCodeError, setVerificationCodeError] = useState(false);
  const [verificationCodeErrorMessage, setVerificationCodeErrorMessage] =
    useState("");

  let dispatch = useDispatch();
  const email = localStorage.getItem("emailTobeVerify");
  const handleChangeValues = (event) => {
    if (isCodeInValid(event.currentTarget.value))
      setCodeErrorMessageAndVisibility(
        "Please enter 6 digit verification code"
      );
    else setCodeErrorMessageAndVisibility("");
    setVerifyCode(event.currentTarget.value);
  };

  const setCodeErrorMessageAndVisibility = (text) => {
    setVerificationCodeError((verificationCodeError = !isNullOrEmpty(text)));
    setVerificationCodeErrorMessage(text);
  };

  const isCodeInValid = (code) => {
    return code.length !== 6;
  };

  const verify = () => {
    if (isCodeInValid(verifyCode))
      setCodeErrorMessageAndVisibility(
        "Please enter 6 digit verification code"
      );
    if (!verificationCodeError) {
      setIsLoading(true);

      let payload = {
        email: email,
        code: verifyCode,
      };

      verifyUser(payload)
        .then(({ data }) => {
          if (data.success) {
            localStorage.setItem("userId", data.result.Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result));
            setIsLoading(false);
            setIsEmailVerificationOpen(false);
            // CustomInfo("Login Successfully...");
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            CustomError("Verification code is wrong, Please try again.");
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          if (err.status == 553) {
            CustomError(
              "Failed to verify user because verification code is expired, Please try again later."
            );
          } else {
            CustomError("Failed to verify user. Please try again later.");
            setAlertDialogVisibility(true);
          }
          setIsLoading(false);
        });
    }
  };

  const verifyUserValidation = () => {
    if (isFromForgotPassword) verificationCodeApi();
    else verify();
  };

  const verificationCodeApi = () => {
    if (isCodeInValid(verifyCode))
      setCodeErrorMessageAndVisibility(
        "Please enter 6 digit verification code."
      );
    else {
      let object = {
        email: email,
        code: verifyCode,
      };
      setIsLoading(true);
      validateCode(object)
        .then(({ data }) => {
          if (data.success) {
            setIsNewPasswordOpen(true);
            setIsEmailVerificationOpen(false);
          } else CustomError("Verification code is wrong, Please try again.");
          setIsLoading(false);
        })
        .catch((err) => {
          console.log("err", err);
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  const resendCode = () => {
    setResendCodeLoading(true);
    forgetPassword(email).then(({ data }) => {
      console.log("data", data)
      setResendCodeLoading(false);
    }).catch((err) => {
      console.log("err", err)
      setResendCodeLoading(false);
    })
  }

  return (
    <>
      <Head
        title="AIDApro | Email Verification"
        description="Email Verification"
      />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsEmailVerificationOpen(false);
              setIsFromForgotPassword(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px", textAlign: "center" }}
              >
                We sent a code to your email
              </div>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "16px",
                  textAlign: "center",
                  marginTop: "0em",
                  marginBottom: "2em",
                }}
              >
                Enter the 6-digit verification code sent to <br />
                <a
                  href={email}
                  style={{ color: "#242424", fontWeight: "bold" }}
                >
                  {email}
                </a>{" "}
                {/* <a
                  href="#"
                  onClick={() => {
                    setIsEmailVerificationOpen(false);
                    setIsForgotPasswordOpen(true);
                  }}
                  style={{ color: "#0ca69d" }}
                >
                  Change
                </a> */}
              </div>
              <InputBox
                placeholder="Code"
                name="code"
                error={verificationCodeError}
                errorMessage={verificationCodeErrorMessage}
                value={verifyCode}
                maxValue={6}
                onChange={(e) => {
                  handleChangeValues(e);
                }}
              // onKeyDown={(event) => {
              //   if (event.key === "Enter") verifyUserValidation();
              // }}
              />
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "16px",
                  textAlign: "center",
                  marginTop: "-.5em",
                  marginBottom: "-1em",
                }}
              ></div>
              <button
                // type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  margin: "0em auto",
                  marginTop: "2em",
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  verifyUserValidation();
                }}
                disabled={isLoading ? true : false}
                title="verify"
              >
                {isLoading ? "Loading..." : "Submit"}
              </button>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "16px",
                  textAlign: "center",
                  marginTop: "1em",
                }}
              >
                If you don't see the email in your inbox, check your spam
                folder.
                <br />
                <div
                  onClick={() => resendCode()}
                  style={{ color: "#0ca69d" }}>
                  {resendCodeLoading ? "Processing..." : "Resend code"}
                </div>
                {/* <a href="#" onClick={() => {}} style={{ color: "#0ca69d" }}>
                  Can't access this email?
                </a> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
