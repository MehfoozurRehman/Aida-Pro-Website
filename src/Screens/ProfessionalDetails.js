import React, { useState, useEffect, useContext } from "react";
import UserContext from "Context/UserContext";
import { useDispatch, useSelector } from "react-redux";
import Img from "react-cool-img";
import { CustomError } from "./Toasts";
import { PART_TIME } from "Utils/Constants";
import {
  jobseekerQualificationDelete,
  jobseekerCertificateDelete,
  jobSekkerProfessionalDetailsUpdate,
  jobseekerSkillDelete,
  jobSekkerExperienceDelete,
} from "../API/EmploymentAPI";
import {
  freelancerCertificateDelete,
  freelancerExperienceDelete,
  freelancerQualificationDelete,
  freelancerSkillDelete,
  freelacerProfessionalDetailsUpdate,
} from "../API/FreelancerApi";
import {
  Head,
  PostingDetailsEducationCertificateCard,
  PostingDetailsEducationDegreeCard,
  PostingDetailsExperianceCard,
  ProfessionalsTags,
} from "Components";
import { HourlyRateSvg, PlaneSvg, postingSelected } from "Assets";
import moment from "moment";

const ProfessionalDetails = ({
  type,
  setIsAddDegreeOpen,
  setEditDegree,
  setIsAddCertificationsOpen,
  setIsAddSkillsOpen,
  setIsAddWorkExperianceOpen,
  setAddWorkExperianceEditData,
  setEditCertificationData,
  setIsAddSocialsOpen,
  setSelectedSkills,
  setIsSetHourlyRateOpen,
  setIsSelectWorkTimeOpen,
  setSelectedHourlyRate,
}) => {
  const isOn = window.localStorage.getItem("isOn");

  const [isWorkOnlyOfficeChecked, setIsWorkOnlyOfficeChecked] = useState(false);
  const [isSocialsUploaded, setIsSocialsUploaded] = useState(false);
  useEffect(() => {
    setIsSocialsUploaded(localStorage.getItem("uploaded"));
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  const user = useContext(UserContext);
  let dispatch = useDispatch();

  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  const [linkedinLink, setLinkedinLink] = useState("");
  const [googleLink, setGoogleLink] = useState("");
  const [facebookLink, setFacebookLink] = useState("");
  const [hourlyRate, setHourlyRate] = useState(0);
  const [workOffice, setWorkOffice] = useState(0);
  const [workRemote, setWorkRemote] = useState(true);
  const [professionalEducation, setProfessionalEducation] = useState([]);
  const [professionalCertifications, setProfessionalCertifications] = useState(
    []
  );
  const [professionalSkills, setProfessionalSkills] = useState([]);
  const [professionalExperience, setProfessionalExperience] = useState([]);
  const [showCanRelocate, setShowCanRelocate] = useState(false);
  const [canRelocate, setCanRelocate] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [fileString, setFileString] = useState(null);
  var [file, setFile] = useState(null);
  const [joiningAvailbilty, setJoiningAvailbilty] = useState(null);
  const [jobTypeDropDownSelected, setJobTypeDropDownSelected] = useState(null);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    setSelectedSkills([]);
    setEditDegree(null);
    setEditCertificationData(null);
    setAddWorkExperianceEditData(null);
    setLinkedinLink(jobsekker.LinkedInProfile);
    setGoogleLink(jobsekker.GoogleProfile);
    setFacebookLink(jobsekker.FacebookProfile);
    setJobTypeDropDownSelected(jobsekker.JobTypeLookupDetail);

    let JobSeekerQualifications = [];
    if (jobsekker && jobsekker.qualification) {
      jobsekker.qualification &&
        jobsekker.qualification.map((e) => {
          JobSeekerQualifications.push({
            Id: e.Id,
            JobSeekerId: user.JobSeekerId,
            dateFrom: e.StartDate,
            dateTo: e.EndDate,
            EndMonth: e.EndMonth,
            EndYear: e.EndYear,
            FieldOfStudy: e.FieldOfStudy,
            StartMonth: e.StartMonth,
            StartYear: e.StartYear,
            institueName: e.Institute,
            educationType: {
              label: e.LookupDetail.Title,
              value: e.LookupDetail.Id,
            },
          });
        });
      JobSeekerQualifications.sort(function (a, b) {
        return moment(b.dateTo) - moment(a.dateFrom);
      });
      setProfessionalEducation(JobSeekerQualifications);
    }
    setProfessionalSkills([]);
    if (jobsekker && jobsekker.skills) {
      jobsekker.skills &&
        jobsekker.skills.map((e) => {
          setProfessionalSkills((preValue) => [
            ...preValue,
            {
              label: e.Skill && e.Skill.Title,
              value: e.Skill && e.SkillId,
              Id: e.Id,
            },
          ]);
        });
    }

    setProfessionalExperience([]);
    if (jobsekker && jobsekker.experience) {
      if (jobsekker.experience.length > 0) {
        jobsekker.experience.sort(
          (a, b) => moment(b.StartDate) - moment(a.StartDate)
        );
        jobsekker.experience.map((e) => {
          setProfessionalExperience((preValue) => [
            ...preValue,
            {
              Id: e.Id,
              jobCompany: e.CompanyName,
              jobDescription: e.Description,
              jobEmployementType: {
                value: e.LookupDetail.Id,
                label: e.LookupDetail.Title,
              },
              jobIndustryType: {
                value: e.Industry.Id,
                label: e.Industry.Title,
              },
              jobLocation: e.Location,
              jobTitle: e.Title,
              jobFrom: e.StartDate,
              jobTo: e.EndDate,
              EndMonth: e.EndMonth,
              EndYear: e.EndYear,
              StartMonth: e.StartMonth,
              StartYear: e.StartYear,
            },
          ]);
        });
      }
    }

    setProfessionalCertifications([]);
    if (jobsekker && jobsekker.certificates) {
      if (jobsekker.certificates != null && jobsekker.certificates.length > 0) {
        console.log("jobsekker.certificates", jobsekker.certificates);
        jobsekker.certificates.sort(function (a, b) {
          return (
            moment(b.EndDate != null ? b.EndDate : new Date()) -
            moment(a.StartDate)
          );
        });
        jobsekker.certificates.reverse().map((e) =>
          setProfessionalCertifications((preValue) => [
            ...preValue,
            {
              Id: e.Id,
              certificateName: e.CertificateName,
              JobSeekerId: user.JobSeekerId,
              dateFrom: e.StartDate,
              dateTo: e.EndDate,
              institueName: e.Institute,
              EndMonth: e.EndMonth,
              EndYear: e.EndYear,
              StartMonth: e.StartMonth,
              StartYear: e.StartYear,
            },
          ])
        );
      }
    }
    if (jobsekker) {
      setShowCanRelocate(jobsekker.canRelocate);
      setCanRelocate(jobsekker.canRelocate);
      setWorkRemote(jobsekker.workRemote);
      setWorkOffice(jobsekker.workOffice);
      setHourlyRate(jobsekker.expectedSalary);
      setJoiningAvailbilty(jobsekker.Availbility);

      if (jobsekker.linkedInProfile) {
        let linkedInProfile = jobsekker.linkedInProfile.split("/");
        setLinkedinLink(linkedInProfile[4]);
      }

      if (jobsekker.googleProfile) {
        let twitterProfile = jobsekker.googleProfile.split("/");
        setGoogleLink(twitterProfile[3]);
      }

      if (jobsekker.facebookProfile) {
        let facebookProfile = jobsekker.facebookProfile.split("/");
        setFacebookLink(facebookProfile[3]);
      }

      if (jobsekker.JobSeekerCV) {
        setFile(process.env.REACT_APP_BASEURL.concat(jobsekker.JobSeekerCV));
      }

      if (jobsekker.Availbility) {
        setJoiningAvailbilty(jobsekker.Availbility);
      }
    }
  }, [jobsekker]);

  const removeSkill = (index, Id) => {
    setIsLoading(true);

    if (type === "freelancer") {
      let skillObject = {
        FreelancerId: user.FreelancerId,
        Id: Id,
      };
      freelancerSkillDelete(skillObject)
        .then(({ data }) => {
          setProfessionalSkills(
            professionalSkills.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Skills Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Skills ");
        });
    } else {
      let skillObject = {
        jobseekerId: user.JobSeekerId,
        Id: Id,
      };
      jobseekerSkillDelete(skillObject)
        .then(({ data }) => {
          setProfessionalSkills(
            professionalSkills.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Skills Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Skills ");
        });
    }
  };

  const removeQualification = (index, Id) => {
    setIsLoading(true);
    if (type === "freelancer") {
      let qualificationObject = {
        FreelancerId: user.FreelancerId,
        Id: Id,
      };
      freelancerQualificationDelete(qualificationObject)
        .then(({ data }) => {
          let professionalEducationList = professionalEducation;
          professionalEducationList = professionalEducation.filter(
            (e, i) => i !== index
          );
          setProfessionalEducation(professionalEducationList);
          setIsLoading(false);
          //CustomSuccess("Qualification Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Qualification ");
        });
    } else {
      let qualificationObject = {
        jobseekerId: user.JobSeekerId,
        Id: Id,
      };
      jobseekerQualificationDelete(qualificationObject)
        .then(({ data }) => {
          setProfessionalEducation(
            professionalEducation.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Qualification Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Qualification ");
        });
    }
  };

  const removeCertification = (index, Id) => {
    setIsLoading(true);
    if (type === "freelancer") {
      let certificateObject = {
        FreelancerId: user.FreelancerId,
        Id: Id,
      };
      freelancerCertificateDelete(certificateObject)
        .then(({ data }) => {
          setProfessionalCertifications(
            professionalCertifications.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Certificate Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Certificate ");
        });
    } else {
      let certificateObject = {
        jobseekerId: user.JobSeekerId,
        Id: Id,
      };
      jobseekerCertificateDelete(certificateObject)
        .then(({ data }) => {
          setProfessionalCertifications(
            professionalCertifications.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Certificate Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Certificate ");
        });
    }
  };

  const removeExperience = (index, Id) => {
    setIsLoading(true);
    if (type === "freelancer") {
      let experienceObject = {
        FreelancerId: user.FreelancerId,
        Id: Id,
      };
      freelancerExperienceDelete(experienceObject)
        .then(({ data }) => {
          setProfessionalExperience(
            professionalExperience.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Experience Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Experience ");
        });
    } else {
      let experienceObject = {
        jobseekerId: user.JobSeekerId,
        Id: Id,
      };
      jobSekkerExperienceDelete(experienceObject)
        .then(({ data }) => {
          setProfessionalExperience(
            professionalExperience.filter((e, i) => i !== index)
          );
          setIsLoading(false);
          //CustomSuccess("Experience Deleted Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Deleted Experience ");
        });
    }
  };

  const saveProfessionalDetailData = () => {
    setIsLoading(true);

    let relocate = canRelocate;
    let officeWork = workOffice;
    if (jobTypeDropDownSelected != null) {
      if (
        jobTypeDropDownSelected.value == PART_TIME ||
        jobTypeDropDownSelected.value == PART_TIME
      ) {
        relocate = false;
        officeWork = false;
      }
    }
    if (user.JobSeekerId) {
      let data = {
        Id: user.JobSeekerId,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        ExpectedSalary: hourlyRate,
        WorkOffice: officeWork ? 1 : 0,
        CanRelocate: relocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        JobSeekerCV:
          file != null ? (file.includes("/api") ? null : file) : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      jobSekkerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          //CustomSuccess("Professional Detail Update Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Update Professional Detail ");
        });
    } else {
      let data = {
        Id: user.FreelancerId,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        HourlyRate: hourlyRate,
        WorkOffice: workOffice ? 1 : 0,
        CanRelocate: canRelocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        FreelancerCV:
          file != null ? (file.includes("/api") ? null : file) : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      freelacerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          window.location.reload();
          //CustomSuccess("Professional Detail Update Successfully...");
        })
        .catch((err) => {
          setIsLoading(false);
          CustomError("Failed to Update Professional Detail ");
        });
    }
    setIsLoading(false);
  };

  function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (err) {
      // console.log("err ", err);
    };
  }

  const fileSelectedHandler = async (event) => {
    if (event.target.files.length > 0) {
      const element = event.target.files[0];
      const elementName = element.name;
      await getBase64(element, (result) => {
        setFileString(elementName);
        setFile((file = result));
        saveProfessionalDetailData();
      });
    }
  };

  const removeCV = () => {
    setFile(null);
  };

  const onClickEditDegree = (value) => {
    setEditDegree(value);
    setIsAddDegreeOpen(true);
  };

  return (
    <>
      <Head
        title={
          type === "freelancer"
            ? "AIDApro | Freelancers Professional Details"
            : "AIDApro | Professionals Professional Details"
        }
        description={
          type === "freelancer"
            ? "Freelancers Professional Details"
            : "Professionals Professional Details"
        }
      />
      <div className="professional__details__container">
        <div className="professional__details__container__header">
          <div className="dashboard__container__heading">
            <Img
              loading="lazy"
              src={postingSelected}
              alt="postingSelected"
              style={{ marginRight: 12, width: 30, height: 30 }}
            />
            Professional details
          </div>
        </div>
        <div className="professional__details__container__content">
          <div className="professional__details__container__content__col">
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="43.49"
                    height="34"
                    viewBox="0 0 43.49 34"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-6"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#fff" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                    </defs>
                    <g id="degrees" transform="translate(-19.758 -25.832)">
                      <g
                        id="Group_1490"
                        data-name="Group 1490"
                        transform="translate(19.758 25.832)"
                      >
                        <g
                          id="Group_1489"
                          data-name="Group 1489"
                          transform="translate(0 0)"
                        >
                          <g id="Group_1488" data-name="Group 1488">
                            <g
                              id="Group_1472"
                              data-name="Group 1472"
                              transform="translate(7.121 15.525)"
                            >
                              <g id="Group_1471" data-name="Group 1471">
                                <g id="Group_1470" data-name="Group 1470">
                                  <path
                                    id="Path_21312"
                                    data-name="Path 21312"
                                    d="M82.923,116.121l-12.081,5.253a2.543,2.543,0,0,1-2.028,0l-12.081-5.253a2.543,2.543,0,0,1-1.529-2.333V103.11H84.452v10.679a2.544,2.544,0,0,1-1.529,2.333Z"
                                    transform="translate(-55.203 -103.11)"
                                    fill="url(#linear-gradient)"
                                  />
                                </g>
                              </g>
                            </g>
                            <g
                              id="Group_1473"
                              data-name="Group 1473"
                              transform="translate(7.121 24.194)"
                            >
                              <path
                                id="Path_21313"
                                data-name="Path 21313"
                                d="M82.923,148.6l-12.081,5.253a2.543,2.543,0,0,1-2.028,0L56.732,148.6a2.543,2.543,0,0,1-1.529-2.333v2.009a2.544,2.544,0,0,0,1.529,2.333l12.081,5.253a2.543,2.543,0,0,0,2.028,0l12.081-5.253a2.543,2.543,0,0,0,1.529-2.333v-2.009A2.544,2.544,0,0,1,82.923,148.6Z"
                                transform="translate(-55.203 -146.266)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                            <g id="Group_1476" data-name="Group 1476">
                              <g id="Group_1475" data-name="Group 1475">
                                <g id="Group_1474" data-name="Group 1474">
                                  <path
                                    id="Path_21314"
                                    data-name="Path 21314"
                                    d="M40.465,47.863l-19.2-8.583a2.543,2.543,0,0,1,0-4.644l19.2-8.583a2.543,2.543,0,0,1,2.076,0l19.2,8.583a2.543,2.543,0,0,1,0,4.644l-19.2,8.583a2.543,2.543,0,0,1-2.076,0Z"
                                    transform="translate(-19.758 -25.832)"
                                    fill="url(#linear-gradient)"
                                  />
                                </g>
                              </g>
                            </g>
                            <g
                              id="Group_1478"
                              data-name="Group 1478"
                              transform="translate(6.718 0.803)"
                            >
                              <g id="Group_1477" data-name="Group 1477">
                                <path
                                  id="Path_21315"
                                  data-name="Path 21315"
                                  d="M53.6,37.044a.4.4,0,0,1-.163-.769l14.079-6.292a1.716,1.716,0,0,1,.707-.153h0a1.716,1.716,0,0,1,.707.153l3.236,1.444a.4.4,0,0,1,.2.532.415.415,0,0,1-.532.2l-3.234-1.446a.926.926,0,0,0-.763,0L53.769,37.008h0a.386.386,0,0,1-.165.036Zm19.872-4.193a.418.418,0,0,1-.163-.034.4.4,0,0,1-.211-.223.389.389,0,0,1,.008-.307.408.408,0,0,1,.53-.2.4.4,0,0,1,.2.53.4.4,0,0,1-.368.237Z"
                                  transform="translate(-53.2 -29.83)"
                                  fill="url(#linear-gradient)"
                                />
                              </g>
                            </g>
                            <g
                              id="Group_1479"
                              data-name="Group 1479"
                              transform="translate(0.001 9.067)"
                            >
                              <path
                                id="Path_21316"
                                data-name="Path 21316"
                                d="M62.2,70.963a2.627,2.627,0,0,1-.456.262l-19.2,8.583a2.543,2.543,0,0,1-2.076,0l-19.2-8.583a2.64,2.64,0,0,1-.456-.262,2.545,2.545,0,0,0,.456,4.382l19.2,8.583a2.543,2.543,0,0,0,2.076,0l19.2-8.583A2.545,2.545,0,0,0,62.2,70.963Z"
                                transform="translate(-19.762 -70.963)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                            <g
                              id="Group_1482"
                              data-name="Group 1482"
                              transform="translate(2.306 8.636)"
                            >
                              <g id="Group_1481" data-name="Group 1481">
                                <g id="Group_1480" data-name="Group 1480">
                                  <path
                                    id="Path_21317"
                                    data-name="Path 21317"
                                    d="M31.871,86.094a.636.636,0,0,1-.636-.636V74.274a.635.635,0,0,1,.478-.616l18.8-4.819a.636.636,0,0,1,.316,1.232l-18.325,4.7v10.69a.636.636,0,0,1-.636.636Z"
                                    transform="translate(-31.235 -68.819)"
                                    fill="url(#linear-gradient-6)"
                                  />
                                </g>
                              </g>
                            </g>
                            <g
                              id="Group_1485"
                              data-name="Group 1485"
                              transform="translate(0.948 21.194)"
                            >
                              <g
                                id="Group_1484"
                                data-name="Group 1484"
                                transform="translate(0 0)"
                              >
                                <g id="Group_1483" data-name="Group 1483">
                                  <path
                                    id="Path_21318"
                                    data-name="Path 21318"
                                    d="M27.641,137.056H25.3a.824.824,0,0,1-.824-.824v-2.906a1.993,1.993,0,1,1,3.986,0v2.906A.824.824,0,0,1,27.641,137.056Z"
                                    transform="translate(-24.479 -131.333)"
                                    fill="url(#linear-gradient)"
                                  />
                                </g>
                              </g>
                            </g>
                            <g
                              id="Group_1486"
                              data-name="Group 1486"
                              transform="translate(0.948 21.194)"
                            >
                              <path
                                id="Path_21319"
                                data-name="Path 21319"
                                d="M26.471,131.333h-.016v2.889a.824.824,0,0,1-.824.824H24.478v1.185a.824.824,0,0,0,.824.824H27.64a.824.824,0,0,0,.824-.824v-2.906A1.993,1.993,0,0,0,26.471,131.333Z"
                                transform="translate(-24.478 -131.333)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                            <g
                              id="Group_1487"
                              data-name="Group 1487"
                              transform="translate(0.948 21.369)"
                            >
                              <path
                                id="Path_21320"
                                data-name="Path 21320"
                                d="M27.286,132.2a1.983,1.983,0,0,1,.175.814v2.906a.824.824,0,0,1-.824.824H24.479v.18a.824.824,0,0,0,.824.824h2.338a.824.824,0,0,0,.824-.824v-2.906A1.992,1.992,0,0,0,27.286,132.2Z"
                                transform="translate(-24.479 -132.204)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                  Degrees
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setEditDegree(null);
                    setIsAddDegreeOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="add degrees"
                >
                  Add
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                {jobsekker
                  ? jobsekker.qualification
                    ? professionalEducation.map(
                        (qualification, qualificationIndex) => (
                          <>
                            <PostingDetailsEducationDegreeCard
                              removeEducation={() =>
                                removeQualification(
                                  qualificationIndex,
                                  qualification.Id
                                )
                              }
                              data={qualification ? qualification : null}
                              onClick={onClickEditDegree}
                            />
                            <div className="professional__details__container__content__entry__content__divider"></div>
                          </>
                        )
                      )
                    : null
                  : null}
              </div>
            </div>
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="41.749"
                    height="41.749"
                    viewBox="0 0 41.749 41.749"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#0ca69d" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-2"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#007e77" />
                      </linearGradient>
                    </defs>
                    <g id="tools" transform="translate(-3.323 -3.323)">
                      <path
                        id="Path_21160"
                        data-name="Path 21160"
                        d="M44.416,10.182l-5.328,5.328L33.964,14.48,32.935,9.356l5.328-5.328a8.7,8.7,0,0,0-10.894,9.4,2.932,2.932,0,0,1-.877,2.377L17.7,24.6l-1.892,1.892a2.932,2.932,0,0,1-2.377.877,8.7,8.7,0,0,0-9.4,10.894l5.328-5.328,5.125,1.029,1.029,5.125-5.328,5.328a8.7,8.7,0,0,0,10.894-9.4,2.932,2.932,0,0,1,.877-2.377L32.637,21.953a2.932,2.932,0,0,1,2.377-.877,8.7,8.7,0,0,0,9.4-10.894Z"
                        transform="translate(-0.113 -0.113)"
                        fill="url(#linear-gradient)"
                      />
                      <path
                        id="Path_21161"
                        data-name="Path 21161"
                        d="M7.732,30.586a8.689,8.689,0,0,1,7.147-2.493,2.932,2.932,0,0,0,2.377-.877l1.892-1.892,8.792-8.792a2.932,2.932,0,0,0,.877-2.377,8.685,8.685,0,0,1,8.994-9.676l.45-.45a8.7,8.7,0,0,0-10.894,9.4,2.932,2.932,0,0,1-.877,2.377L17.7,24.6l-1.892,1.892a2.932,2.932,0,0,1-2.377.877,8.7,8.7,0,0,0-9.4,10.894L5.2,37.087a8.647,8.647,0,0,1,2.529-6.5Z"
                        transform="translate(-0.113 -0.113)"
                        fill="url(#linear-gradient-2)"
                      />
                      <path
                        id="Path_21162"
                        data-name="Path 21162"
                        d="M55.1,58.263l4.219,1.055L58.263,55.1A4.335,4.335,0,0,0,55.1,58.263Z"
                        transform="translate(-14.247 -14.247)"
                        fill="#58595b"
                      />
                      <path
                        id="Path_21163"
                        data-name="Path 21163"
                        d="M52.169,52.17l-2.562-.512v4.1l3.982,1a4.335,4.335,0,0,1,3.163-3.163l-1-3.979h-4.1Z"
                        transform="translate(-12.736 -12.737)"
                        fill="#d8d8d8"
                      />
                      <path
                        id="Path_21164"
                        data-name="Path 21164"
                        d="M0,0H0Z"
                        transform="translate(11.769 13.822) rotate(-45)"
                        fill="#d1e7f8"
                      />
                      <path
                        id="Path_21165"
                        data-name="Path 21165"
                        d="M17.134,10.983,14.058,7.908l-6.15,6.15,3.075,3.075,2.05-2.05,2.051-2.051Z"
                        transform="translate(-1.262 -1.262)"
                        fill="#303043"
                      />
                      <path
                        id="Path_21166"
                        data-name="Path 21166"
                        d="M10.747,4.6a4.349,4.349,0,1,0-6.15,6.15l2.05,2.05,6.15-6.15Z"
                        fill="#d1d3d4"
                      />
                      <path
                        id="Path_21167"
                        data-name="Path 21167"
                        d="M36.318,27.056l-1.025-1.025L38.4,22.919a.725.725,0,0,1,1.025,1.025Z"
                        transform="translate(-8.797 -5.334)"
                        fill="#fff"
                      />
                      <path
                        id="Path_21168"
                        data-name="Path 21168"
                        d="M22.919,39.43a.725.725,0,0,1,0-1.025l3.112-3.112,1.025,1.025L23.944,39.43A.725.725,0,0,1,22.919,39.43Z"
                        transform="translate(-5.334 -8.797)"
                        fill="#fff"
                      />
                      <path
                        id="Path_21169"
                        data-name="Path 21169"
                        d="M14.2,17.809,12.15,19.861,20.391,28.1l6.147,6.147L39.3,47v-4.1Z"
                        transform="translate(-2.429 -3.986)"
                        fill="#ff7f23"
                      />
                      <path
                        id="Path_21170"
                        data-name="Path 21170"
                        d="M47,39.3,34.242,26.538,28.1,20.391,19.861,12.15,17.809,14.2l25.1,25.1Z"
                        transform="translate(-3.986 -2.429)"
                        fill="#f6a039"
                      />
                      <path
                        id="Path_21171"
                        data-name="Path 21171"
                        d="M40.079,42.129l2.562.512-.512-2.562-25.1-25.1-2.05,2.05Z"
                        transform="translate(-3.208 -3.208)"
                        fill="#f58e3b"
                      />
                    </g>
                  </svg>
                  Skills
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setSelectedSkills(
                      jobsekker.skills ? professionalSkills : []
                    );
                    setIsAddSkillsOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="add skills"
                >
                  Add
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                <button
                  className="homepage__container__jobs__projects__penel__container__details__content__profile__card__entry__container"
                  // onClick={() => {
                  //   setIsEditSkillsOpen(true);
                  //   window.scrollTo({ top: 0, behavior: "smooth" });
                  // }}
                  // title="skills edit"
                >
                  {jobsekker
                    ? jobsekker.skills
                      ? professionalSkills.map((skill, i) => (
                          <ProfessionalsTags
                            key={i}
                            heading={skill.label}
                            removeSkillMethod={() => removeSkill(i, skill.Id)}
                          />
                        ))
                      : null
                    : null}
                </button>
              </div>
            </div>
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <Img
                    loading="lazy"
                    src={HourlyRateSvg}
                    alt="HourlyRateSvg"
                    style={{ width: 40, height: 40, marginRight: ".5em" }}
                  />
                  Annual Salary
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setSelectedHourlyRate(hourlyRate);
                    setIsSetHourlyRateOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="Set annual salary"
                >
                  Set
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                <div className="professional__details__container__content__entry__content__social__card">
                  <div className="professional__details__container__content__entry__content__social__card__text">
                    <b>€</b> {hourlyRate}
                  </div>
                </div>
              </div>
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <Img
                    loading="lazy"
                    src={PlaneSvg}
                    alt="PlaneSvg"
                    style={{ width: 50, height: 50, marginRight: "0em" }}
                  />
                  When are you normally able to start?
                </div>
              </div>
              <div className="professional__details__container__content__entry__content">
                <div className="professional__details__container__content__entry__content__social__card">
                  <div className="professional__details__container__content__entry__content__social__card__text">
                    {joiningAvailbilty != null
                      ? joiningAvailbilty.label
                      : "Not specified"}
                  </div>
                </div>
              </div>
            </div>
            <div
              className="professional__details__container__content__entry"
              style={{ backgroundColor: "#303043" }}
            >
              <div className="professional__details__container__content__entry__header">
                <div
                  className="professional__details__container__content__entry__header__heading"
                  style={{ color: "#ffffff" }}
                >
                  Upload CV
                </div>
                <div style={{ display: "flex" }}>
                  <button
                    className="header__nav__btn btn__primary"
                    title="upload cv"
                  >
                    <input
                      type="file"
                      // accept=".xlsx,.xls,.doc,.docx,.pdf"
                      accept=".pdf"
                      onClick={(event) => (event.target.value = null)}
                      onChange={fileSelectedHandler}
                    />
                    Upload
                  </button>
                  {/* <button
                    className="header__nav__btn btn__secondary"
                    onClick={() => {
                      saveProfessionalDetailData();
                    }}
                  >
                    Save
                  </button> */}
                </div>
              </div>
              <div className="professional__details__container__content__entry__content">
                {file != null ? (
                  <div
                    className="pupup__container__from__wrapper__cv"
                    style={{ marginTop: 20 }}
                  >
                    <div
                      className="pupup__container__from__wrapper__cv__file"
                      style={{ borderColor: "white" }}
                    >
                      <button
                        className="pupup__container__from__wrapper__cv__file__btn"
                        onClick={() => removeCV()}
                        title="remove cv"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={15}
                          height={15}
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth="2"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="feather feather-x"
                        >
                          <line x1="18" y1="6" x2="6" y2="18"></line>
                          <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg>
                      </button>
                      <div className="pupup__container__from__wrapper__cv__file__icon">
                        PDF
                      </div>
                      <div className="pupup__container__from__wrapper__cv__file__content">
                        <div //className="pupup__container__from__wrapper__cv__file__content__time"
                          style={{ color: "white" }}
                        >
                          {fileString != null ? fileString : "Already Uploaded"}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
          <div className="professional__details__container__content__col">
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="49.049"
                    height="41.167"
                    viewBox="0 0 49.049 56.167"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                    </defs>
                    <g
                      id="Page-1"
                      transform="matrix(0.574, -0.819, 0.819, 0.574, -25.423, 24.99)"
                    >
                      <g
                        id="_001---Degree"
                        data-name="001---Degree"
                        transform="translate(-0.018 31.049)"
                      >
                        <path
                          id="Shape"
                          d="M30.109,32.841a19.65,19.65,0,0,1,.673,4.364,6.474,6.474,0,0,0-6.495.6,25.207,25.207,0,0,0-.641-4.975q1.7.035,3.477.035C28.143,32.869,29.133,32.858,30.109,32.841ZM28.057,47.8a4.665,4.665,0,1,1,4.665-4.665A4.665,4.665,0,0,1,28.057,47.8ZM1,39.4a26.169,26.169,0,0,1,.933-7.931A26.169,26.169,0,0,1,2.865,39.4a26.169,26.169,0,0,1-.933,7.931A26.168,26.168,0,0,1,1,39.4Zm2.821,8.09a30.942,30.942,0,0,0,.912-8.09,30.949,30.949,0,0,0-.912-8.091A163.7,163.7,0,0,0,21.7,32.786a24.018,24.018,0,0,1,.761,6.614c0,.135,0,.264-.006.394a6.464,6.464,0,0,0-.3,6.113l-.041.1A162.6,162.6,0,0,0,3.819,47.491ZM24.7,54.329l-.681-1.567a.933.933,0,0,0-1.223-.485l-1.57.694,2.188-5.245a6.525,6.525,0,0,0,3.3,1.8Zm8.609-2.053a.933.933,0,0,0-1.223.485l-.69,1.552-2-4.789a6.531,6.531,0,0,0,3.3-1.8l2.183,5.225Zm18.815-4.525a151.1,151.1,0,0,0-18.1-1.679l-.066-.16A6.493,6.493,0,0,0,32.7,38.553a25.97,25.97,0,0,0-.666-5.748,155.744,155.744,0,0,0,20.091-1.756c.433.735,1.12,3.643,1.12,8.352S52.557,47.016,52.129,47.751Z"
                          transform="translate(-0.982 -31.049)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                    </g>
                  </svg>
                  Certifications
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setEditCertificationData(null);
                    setIsAddCertificationsOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="add certifications"
                >
                  Add
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                {jobsekker
                  ? jobsekker.certificates
                    ? professionalCertifications.map(
                        (certificate, certificateIndex) => (
                          <>
                            <PostingDetailsEducationCertificateCard
                              onClick={(value) => {
                                setEditCertificationData(value);
                                setIsAddCertificationsOpen(true);
                                window.scrollTo({ top: 0, behavior: "smooth" });
                              }}
                              data={certificate ? certificate : null}
                              removeCertification={() =>
                                removeCertification(
                                  certificateIndex,
                                  certificate.Id
                                )
                              }
                            />
                            <div className="professional__details__container__content__entry__content__divider"></div>
                          </>
                        )
                      )
                    : null
                  : null}
              </div>
            </div>
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="42"
                    height="58"
                    viewBox="0 0 42 58"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-3"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#0ca69d" />
                      </linearGradient>
                    </defs>
                    <g id="Work" transform="translate(-11 -3)">
                      <path
                        id="Path_21366"
                        data-name="Path 21366"
                        d="M18,0c9.941,0,18,2.239,18,5s-8.059,5-18,5S0,7.761,0,5,8.059,0,18,0Z"
                        transform="translate(14 51)"
                        fill="url(#linear-gradient)"
                      />
                      <path
                        id="Path_21359"
                        data-name="Path 21359"
                        d="M53,24c0,7.8-9.5,20.03-15.72,27.21C34.25,54.7,32,57,32,57s-2.25-2.3-5.28-5.79C20.5,44.03,11,31.8,11,24a21,21,0,0,1,42,0Z"
                        fill="url(#linear-gradient)"
                      />
                      <circle
                        id="Ellipse_383"
                        data-name="Ellipse 383"
                        cx="16"
                        cy="16"
                        r="16"
                        transform="translate(16 8)"
                        fill="#d1e7f8"
                      />
                      <path
                        id="Path_21360"
                        data-name="Path 21360"
                        d="M48,24A15.98,15.98,0,0,1,21.265,35.839q-2.214,1.242-4.533,2.309A122.307,122.307,0,0,0,26.72,51.21C29.75,54.7,32,57,32,57s2.25-2.3,5.28-5.79C43.5,44.03,53,31.8,53,24A20.928,20.928,0,0,0,46.927,9.235q-1.206,2.343-2.592,4.575A15.932,15.932,0,0,1,48,24Z"
                        fill="#eb8a2d"
                      />
                      <path
                        id="Path_21361"
                        data-name="Path 21361"
                        d="M48,24a15.932,15.932,0,0,0-3.665-10.19,64.29,64.29,0,0,1-23.07,22.029A15.98,15.98,0,0,0,48,24Z"
                        fill="#b7cad9"
                      />
                      <path
                        id="Path_21362"
                        data-name="Path 21362"
                        d="M41,24v8a2.006,2.006,0,0,1-2,2H25a2.006,2.006,0,0,1-2-2V24Z"
                        fill="url(#linear-gradient-3)"
                      />
                      <path
                        id="Path_21363"
                        data-name="Path 21363"
                        d="M43,19v5H21V19a2.006,2.006,0,0,1,2-2H41a2.006,2.006,0,0,1,2,2Z"
                        fill="url(#linear-gradient)"
                      />
                      <path
                        id="Path_21364"
                        data-name="Path 21364"
                        d="M34,24v2a1,1,0,0,1-1,1H31a1,1,0,0,1-1-1V23a1,1,0,0,1,1-1h2a1,1,0,0,1,1,1Z"
                        fill="#ff9811"
                      />
                      <path
                        id="Path_21365"
                        data-name="Path 21365"
                        d="M35,12H29a2,2,0,0,0-2,2v3h2V14h6v3h2V14A2,2,0,0,0,35,12Z"
                        fill="#603913"
                      />
                    </g>
                  </svg>
                  Job type
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setIsSelectWorkTimeOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="set job type"
                >
                  Set
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                {/* {jobTypeDropDownData.length > 0 ? (
                   <div className="professional__details__container__content__entry__content__col">
                  <InputBox
                    style={{
                      backgroundColor: "#ffffff",
                      maxWidth: "80%",
                      maxHeight: 50,
                    }}
                    variant="select"
                    placeholder={"Select Job Type"}
                    name={"jobType"}
                    value={jobTypeDropDownSelected}
                    options={jobTypeDropDownData}
                    onChange={(e) => {
                      setWorkOffice(false);
                      setWorkRemote(false);
                      setCanRelocate(false);
                      setJobTypeDropDownSelected(e);
                    }}
                  />

                </div>
                ) : 
                null} */}
                <div className="professional__details__container__content__entry__content__social__card">
                  <div className="professional__details__container__content__entry__content__social__card__text">
                    {jobTypeDropDownSelected != null
                      ? jobTypeDropDownSelected.label
                      : "Not specified"}
                  </div>
                </div>
                {/* {jobTypeDropDownSelected != null ? (
                  jobTypeDropDownSelected.value == PART_TIME ||
                  jobTypeDropDownSelected.value == FULL_TIME ? ( */}
                <div className="professional__details__container__content__entry__content__col">
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-remote"
                    type="checkbox"
                    value="Remember"
                    name="remoteOnly"
                    checked={workRemote}
                    disabled
                    // onClick={(e) => changeWorkStatus(e)}
                  />
                  <label htmlFor="styled-checkbox-remote">
                    {/* Work only remote */}
                    Remote
                  </label>
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-office"
                    type="checkbox"
                    checked={workOffice}
                    value="Remember"
                    // onClick={(e) => changeWorkStatus(e)}
                    name="officeOnly"
                    disabled
                  />
                  <label htmlFor="styled-checkbox-office">
                    {/* Work only office */}
                    Office
                  </label>
                  <>
                    <input
                      className="styled-checkbox"
                      id="styled-checkbox-relocate"
                      type="checkbox"
                      value="Remember"
                      name="willingToRelocate"
                      checked={canRelocate}
                      // onClick={(e) => changeWorkStatus(e)}
                      disabled
                    />
                    <label htmlFor="styled-checkbox-relocate">
                      Willing to Locate
                    </label>
                  </>
                </div>
              </div>
            </div>
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="45.148"
                    height="39.488"
                    viewBox="0 0 45.148 39.488"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-2"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#fff4ee" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-4"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#0ca69d" />
                      </linearGradient>
                    </defs>
                    <g
                      id="suitcase_1_"
                      data-name="suitcase (1)"
                      transform="translate(-0.001)"
                    >
                      <g
                        id="Group_1096"
                        data-name="Group 1096"
                        transform="translate(0.001 0)"
                      >
                        <path
                          id="Path_3425"
                          data-name="Path 3425"
                          d="M183.373,34.091a8.882,8.882,0,0,0-1.961-1.047,14.434,14.434,0,0,0-10.335,0,8.882,8.882,0,0,0-1.961,1.047,1.484,1.484,0,0,0-.584,1.18V37.6l1.484.794L171.5,37.6V36.111a11.188,11.188,0,0,1,9.489,0V37.6l1.484.794,1.484-.794V35.271A1.483,1.483,0,0,0,183.373,34.091Z"
                          transform="translate(-153.671 -32.088)"
                          fill="url(#linear-gradient)"
                        />
                        <g
                          id="Group_1095"
                          data-name="Group 1095"
                          transform="translate(14.861 5.511)"
                        >
                          <path
                            id="Path_3426"
                            data-name="Path 3426"
                            d="M168.533,94.589v1.638l1.484.794,1.484-.794V94.589Z"
                            transform="translate(-168.533 -94.589)"
                            fill="url(#linear-gradient-2)"
                          />
                          <path
                            id="Path_3427"
                            data-name="Path 3427"
                            d="M312.773,94.589H309.8v1.638l1.484.794,1.484-.794Z"
                            transform="translate(-297.348 -94.589)"
                            fill="url(#linear-gradient-2)"
                          />
                        </g>
                        <path
                          id="Path_3428"
                          data-name="Path 3428"
                          d="M13.247,298.286v13.182a2.834,2.834,0,0,0,2.834,2.834H53.225a2.834,2.834,0,0,0,2.834-2.834V298.286Z"
                          transform="translate(-12.079 -274.813)"
                          fill="url(#linear-gradient-4)"
                        />
                        <path
                          id="Path_3429"
                          data-name="Path 3429"
                          d="M13.247,279.705v1.638c2.354,1.869,8.576,5.5,21.406,5.5s19.052-3.628,21.406-5.5V279.7H13.247Z"
                          transform="translate(-12.079 -257.871)"
                          fill="#f7983d"
                        />
                        <path
                          id="Path_3430"
                          data-name="Path 3430"
                          d="M1.045,126.91c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a4.733,4.733,0,0,0,1.045-2.992V116a2.834,2.834,0,0,0-2.834-2.833H2.834A2.834,2.834,0,0,0,0,116v7.916a4.733,4.733,0,0,0,1.045,2.992Z"
                          transform="translate(0 -106.019)"
                          fill="url(#linear-gradient-4)"
                        />
                        <path
                          id="Path_3431"
                          data-name="Path 3431"
                          d="M44.1,237.276c-2.256,1.837-8.469,5.6-21.529,5.6s-19.273-3.759-21.529-5.6A2.834,2.834,0,0,1,0,235.078v1.638a2.834,2.834,0,0,0,1.045,2.2c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a2.834,2.834,0,0,0,1.045-2.2v-1.638A2.834,2.834,0,0,1,44.1,237.276Z"
                          transform="translate(0 -217.179)"
                          fill="url(#linear-gradient)"
                        />
                        <path
                          id="Path_3432"
                          data-name="Path 3432"
                          d="M228.551,310.914c1.466,0,2.654-1.089,2.654-3.448v-1.609a.944.944,0,0,0-.944-.944h-3.419a.944.944,0,0,0-.945.944v1.61C225.9,309.826,227.085,310.914,228.551,310.914Z"
                          transform="translate(-205.978 -280.856)"
                          fill="#fff"
                        />
                        <path
                          id="Path_3433"
                          data-name="Path 3433"
                          d="M228.551,336.528a2.654,2.654,0,0,1-2.654-2.654v1.639a2.654,2.654,0,0,0,5.308,0v-1.639A2.654,2.654,0,0,1,228.551,336.528Z"
                          transform="translate(-205.978 -307.263)"
                          fill="url(#linear-gradient-4)"
                        />
                      </g>
                    </g>
                  </svg>
                  Work experience
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setAddWorkExperianceEditData(null);
                    setIsAddWorkExperianceOpen(true);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="add work experiance"
                >
                  Add
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                {jobsekker
                  ? jobsekker.experience
                    ? professionalExperience.map(
                        (experienceObject, experienceObjectIndex) => (
                          <>
                            <PostingDetailsExperianceCard
                              data={experienceObject ? experienceObject : null}
                              onClick={(value) => {
                                setIsAddWorkExperianceOpen(true);
                                setAddWorkExperianceEditData(value);
                              }}
                              // setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
                              removeExperience={() =>
                                removeExperience(
                                  experienceObjectIndex,
                                  experienceObject.Id
                                )
                              }
                            />
                            <div className="professional__details__container__content__entry__content__divider"></div>
                          </>
                        )
                      )
                    : null
                  : null}
              </div>
            </div>
            <div className="professional__details__container__content__entry">
              <div className="professional__details__container__content__entry__header">
                <div className="professional__details__container__content__entry__header__heading">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="45.148"
                    height="39.488"
                    viewBox="0 0 45.148 39.488"
                  >
                    <defs>
                      <linearGradient
                        id="linear-gradient"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#f5833c" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-2"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#f6a938" />
                        <stop offset="1" stopColor="#fff4ee" />
                      </linearGradient>
                      <linearGradient
                        id="linear-gradient-4"
                        x1="0.5"
                        x2="0.5"
                        y2="1"
                        gradientUnits="objectBoundingBox"
                      >
                        <stop offset="0" stopColor="#0ee1a3" />
                        <stop offset="1" stopColor="#0ca69d" />
                      </linearGradient>
                    </defs>
                    <g
                      id="suitcase_1_"
                      data-name="suitcase (1)"
                      transform="translate(-0.001)"
                    >
                      <g
                        id="Group_1096"
                        data-name="Group 1096"
                        transform="translate(0.001 0)"
                      >
                        <path
                          id="Path_3425"
                          data-name="Path 3425"
                          d="M183.373,34.091a8.882,8.882,0,0,0-1.961-1.047,14.434,14.434,0,0,0-10.335,0,8.882,8.882,0,0,0-1.961,1.047,1.484,1.484,0,0,0-.584,1.18V37.6l1.484.794L171.5,37.6V36.111a11.188,11.188,0,0,1,9.489,0V37.6l1.484.794,1.484-.794V35.271A1.483,1.483,0,0,0,183.373,34.091Z"
                          transform="translate(-153.671 -32.088)"
                          fill="url(#linear-gradient)"
                        />
                        <g
                          id="Group_1095"
                          data-name="Group 1095"
                          transform="translate(14.861 5.511)"
                        >
                          <path
                            id="Path_3426"
                            data-name="Path 3426"
                            d="M168.533,94.589v1.638l1.484.794,1.484-.794V94.589Z"
                            transform="translate(-168.533 -94.589)"
                            fill="url(#linear-gradient-2)"
                          />
                          <path
                            id="Path_3427"
                            data-name="Path 3427"
                            d="M312.773,94.589H309.8v1.638l1.484.794,1.484-.794Z"
                            transform="translate(-297.348 -94.589)"
                            fill="url(#linear-gradient-2)"
                          />
                        </g>
                        <path
                          id="Path_3428"
                          data-name="Path 3428"
                          d="M13.247,298.286v13.182a2.834,2.834,0,0,0,2.834,2.834H53.225a2.834,2.834,0,0,0,2.834-2.834V298.286Z"
                          transform="translate(-12.079 -274.813)"
                          fill="url(#linear-gradient-4)"
                        />
                        <path
                          id="Path_3429"
                          data-name="Path 3429"
                          d="M13.247,279.705v1.638c2.354,1.869,8.576,5.5,21.406,5.5s19.052-3.628,21.406-5.5V279.7H13.247Z"
                          transform="translate(-12.079 -257.871)"
                          fill="#f7983d"
                        />
                        <path
                          id="Path_3430"
                          data-name="Path 3430"
                          d="M1.045,126.91c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a4.733,4.733,0,0,0,1.045-2.992V116a2.834,2.834,0,0,0-2.834-2.833H2.834A2.834,2.834,0,0,0,0,116v7.916a4.733,4.733,0,0,0,1.045,2.992Z"
                          transform="translate(0 -106.019)"
                          fill="url(#linear-gradient-4)"
                        />
                        <path
                          id="Path_3431"
                          data-name="Path 3431"
                          d="M44.1,237.276c-2.256,1.837-8.469,5.6-21.529,5.6s-19.273-3.759-21.529-5.6A2.834,2.834,0,0,1,0,235.078v1.638a2.834,2.834,0,0,0,1.045,2.2c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a2.834,2.834,0,0,0,1.045-2.2v-1.638A2.834,2.834,0,0,1,44.1,237.276Z"
                          transform="translate(0 -217.179)"
                          fill="url(#linear-gradient)"
                        />
                        <path
                          id="Path_3432"
                          data-name="Path 3432"
                          d="M228.551,310.914c1.466,0,2.654-1.089,2.654-3.448v-1.609a.944.944,0,0,0-.944-.944h-3.419a.944.944,0,0,0-.945.944v1.61C225.9,309.826,227.085,310.914,228.551,310.914Z"
                          transform="translate(-205.978 -280.856)"
                          fill="#fff"
                        />
                        <path
                          id="Path_3433"
                          data-name="Path 3433"
                          d="M228.551,336.528a2.654,2.654,0,0,1-2.654-2.654v1.639a2.654,2.654,0,0,0,5.308,0v-1.639A2.654,2.654,0,0,1,228.551,336.528Z"
                          transform="translate(-205.978 -307.263)"
                          fill="url(#linear-gradient-4)"
                        />
                      </g>
                    </g>
                  </svg>
                  Socials
                </div>
                <button
                  className="header__nav__btn btn__secondary"
                  onClick={() => {
                    setIsAddSocialsOpen(true);
                    localStorage.setItem("uploaded", false);
                    window.scrollTo({ top: 0, behavior: "smooth" });
                  }}
                  title="add socials"
                >
                  Add
                </button>
              </div>
              <div className="professional__details__container__content__entry__content">
                {facebookLink != null ? (
                  <div className="professional__details__container__content__entry__content__social__card">
                    <svg
                      id="facebook_2_"
                      data-name="facebook (2)"
                      xmlns="http://www.w3.org/2000/svg"
                      width="25.99"
                      height="25.99"
                      viewBox="0 0 25.99 25.99"
                    >
                      <path
                        id="Path_659"
                        data-name="Path 659"
                        d="M22.741,0H3.249A3.252,3.252,0,0,0,0,3.249V22.741A3.252,3.252,0,0,0,3.249,25.99H22.741a3.252,3.252,0,0,0,3.249-3.249V3.249A3.252,3.252,0,0,0,22.741,0Z"
                        fill="#1976d2"
                      />
                      <path
                        id="Path_660"
                        data-name="Path 660"
                        d="M204.183,104.122h-4.061v-3.249c0-.9.728-.812,1.624-.812h1.624V96h-3.249a4.873,4.873,0,0,0-4.873,4.873v3.249H192v4.061h3.249v8.934h4.873v-8.934h2.437Z"
                        transform="translate(-182.254 -91.127)"
                        fill="#fafafa"
                      />
                    </svg>
                    <div className="professional__details__container__content__entry__content__social__card__text">
                      {facebookLink}
                    </div>
                  </div>
                ) : null}

                {googleLink != null ? (
                  <div className="professional__details__container__content__entry__content__social__card">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25.99"
                      height="25.99"
                      viewBox="0 0 13.189 13.19"
                    >
                      <g
                        id="Group_1746"
                        data-name="Group 1746"
                        transform="translate(408.739 -103.684)"
                      >
                        <path
                          id="Path_21771"
                          data-name="Path 21771"
                          d="M-408.739,115.986V104.559a1.007,1.007,0,0,1,.16-.489.869.869,0,0,1,.641-.373c.025,0,.052.007.073-.013h11.425a.945.945,0,0,1,.574.222.97.97,0,0,1,.314.758c0,.352,0,.7,0,1.055q0,5.07,0,10.14a.985.985,0,0,1-.43.869.971.971,0,0,1-.588.146q-4.741,0-9.482,0c-.57,0-1.141,0-1.711,0a.972.972,0,0,1-.758-.314A.945.945,0,0,1-408.739,115.986Z"
                          fill="#1795d6"
                        />
                        <path
                          id="Path_21772"
                          data-name="Path 21772"
                          d="M-330.722,194.67a1.778,1.778,0,0,1-.731.924,2.073,2.073,0,0,0,.5-.092c.15-.043.3-.1.467-.151a3.282,3.282,0,0,1-.763.817.2.2,0,0,0-.088.181,5.041,5.041,0,0,1-1.589,3.787,4.574,4.574,0,0,1-2.572,1.243,4.983,4.983,0,0,1-3.307-.607c-.038-.022-.087-.035-.115-.1a3.564,3.564,0,0,0,2.5-.726,1.818,1.818,0,0,1-1.624-1.215,1.94,1.94,0,0,0,.771-.042,1.8,1.8,0,0,1-.945-.562,1.692,1.692,0,0,1-.427-1.011c-.01-.149-.007-.149.126-.092a1.77,1.77,0,0,0,.639.146,2.494,2.494,0,0,1-.359-.34,1.735,1.735,0,0,1-.227-1.883c.045-.1.07-.108.148-.019a4.9,4.9,0,0,0,2.883,1.657c.168.031.34.038.509.067.1.017.129-.009.114-.111a1.726,1.726,0,0,1,.315-1.314,1.744,1.744,0,0,1,2.591-.25.223.223,0,0,0,.238.059,3.756,3.756,0,0,0,.826-.315A.224.224,0,0,1-330.722,194.67Z"
                          transform="translate(-67.417 -87.703)"
                          fill="#fff"
                        />
                      </g>
                    </svg>
                    <div className="professional__details__container__content__entry__content__social__card__text">
                      {googleLink}
                    </div>
                  </div>
                ) : null}

                {linkedinLink != null ? (
                  <div className="professional__details__container__content__entry__content__social__card">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="25.99"
                      height="25.99"
                      viewBox="0 0 25.99 25.99"
                    >
                      <path
                        id="linkedin"
                        d="M23.639,0H2.351A2.351,2.351,0,0,0,0,2.351V23.639A2.351,2.351,0,0,0,2.351,25.99H23.639a2.351,2.351,0,0,0,2.351-2.351V2.351A2.351,2.351,0,0,0,23.639,0ZM8.042,22.441a.684.684,0,0,1-.684.684H4.446a.684.684,0,0,1-.684-.684V10.233a.684.684,0,0,1,.684-.684H7.358a.684.684,0,0,1,.684.684ZM5.9,8.4A2.767,2.767,0,1,1,8.669,5.631,2.767,2.767,0,0,1,5.9,8.4Zm17.36,14.1a.629.629,0,0,1-.629.629H19.508a.629.629,0,0,1-.629-.629V16.77c0-.854.251-3.743-2.232-3.743-1.926,0-2.317,1.978-2.4,2.865v6.6a.629.629,0,0,1-.629.629H10.6a.629.629,0,0,1-.629-.629V10.178a.629.629,0,0,1,.629-.629h3.023a.629.629,0,0,1,.629.629v1.065a4.29,4.29,0,0,1,4.035-1.9c5,0,4.976,4.675,4.976,7.244V22.5Z"
                        fill="#0077b7"
                      />
                    </svg>
                    <div className="professional__details__container__content__entry__content__social__card__text">
                      {linkedinLink != null ? linkedinLink : "Linkedin link"}
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfessionalDetails;
