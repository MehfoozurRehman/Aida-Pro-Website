import React from "react";
import Header from "Components/Header";
import { Link } from "@reach/router";
import { Head } from "Components";

export default function TermsConditions({ setIsLoginOpen }) {
  return (
    <>
      <Head
        title="AIDApro | Terms & Conditions"
        description="Terms & Conditions"
      />
      <Header isOnHomeCompany={false} setIsLoginOpen={setIsLoginOpen} />
      <div className="homepage__container__jumbotron">
        <div className="homepage__container__jumbotron__terms__conditions__headings">
          <Link
            to="/"
            style={{
              display: "flex",

              marginRight: "auto",
              marginBottom: -30,
            }}
          >
            <div className="homepage__freelancer__project__details__title__container__svg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="7.639"
                height="13.363"
                style={{ marginRight: 5 }}
                viewBox="0 0 7.639 13.363"
              >
                <path
                  id="Icon_ionic-ios-arrow-back"
                  data-name="Icon ionic-ios-arrow-back"
                  d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                  transform="translate(-11.251 -6.194)"
                />
              </svg>
            </div>
            <div className="homepage__freelancer__project__details__title__container__heading">
              Back
            </div>
          </Link>
          <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
            <span>Terms & Conditions</span>
          </div>
          <div
            className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft"
            style={{ textAlign: "justify" }}
          >
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </div>
          <div className="homepage__container__jumbotron__terms__conditions__sub__headings">
            <div className="homepage__container__jumbotron__terms__conditions__sub__heading animate__animated animate__fadeInDown">
              Introduction
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__info animate__animated animate__fadeInRight">
              It was popularised in the 1960s with the release of Letraset
              sheets containing Lorem Ipsum passages, and more recently with
              desktop publishing software like Aldus PageMaker including
              versions of Lorem Ipsum.It was popularised in the 1960s with the
              release of Letraset sheets containing Lorem Ipsum passages, and
              more recently with desktop publishing software like Aldus
              PageMaker including versions of Lorem Ipsum.
            </div>

            <div className="homepage__container__jumbotron__terms__conditions__sub__heading animate__animated animate__fadeInDowm">
              Restrictions
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__info animate__animated animate__fadeInLeft">
              Lorem Ipsum has been the industry's standard dummy text ever since
              the 1500s, when an unknown printer took a galley of type and
              scrambled it to make a type specimen book. It has survived not
              only five centuries, but also the leap into electronic
              typesetting, remaining essentially unchanged. It was popularised
              in the 1960s with the release of Letraset sheets containing Lorem
              Ipsum passages, and more recently with desktop publishing software
              like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__sub__heading animate__animated animate__fadeInDown">
              Do I need terms & conditions?
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__info animate__animated animate__fadeInRight">
              Unlike a privacy policy, terms & conditions are not a legal
              requirement on your website however it is still recommended that
              every website has one. Terms & conditions are designed to limit
              your liability in the event of a claim by one of your users. The
              courts will use your terms and conditions as a basis for assessing
              the validity of any potential complaint so it should still be
              considered a very important document. Unlike a privacy policy,
              terms & conditions are not a legal requirement on your website
              however it is still recommended that every website has one. Terms
              & conditions are designed to limit your liability in the event of
              a claim by one of your users. The courts will use your terms and
              conditions as a basis for assessing the validity of any potential
              complaint so it should still be considered a very important
              document.
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
