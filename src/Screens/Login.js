import React, { useEffect, useState } from "react";
import { login } from "../API/Api";
import { setCurrentUser } from "../Redux/Actions/AppActions";
import { useDispatch } from "react-redux";
import { navigate } from "@reach/router";
import { Head, InputBox } from "Components";
import Img from "react-cool-img";
import { isNullOrEmpty } from "../Utils/TextUtils";
import { isInvalidEmail } from "Utils/Validations";
import { crossSvg, screeningByExperts } from "Assets";
import { getUser } from "Redux/Actions/AppActions";

const Login = ({
  setIsLoginOpen,
  redirectURLFromLogin,
  setIsForgotPasswordOpen,
  setIsEmailVerificationOpen,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) => {
  let dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const firebaseToken = localStorage.getItem("ft");

  // validation
  const [userNameError, setUserNameError] = useState(false);
  const [userNameErrorMessage, setUserNameErrorMessage] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");

  const onEmailTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value)) {
      setUserNameError(true);
      setUserNameErrorMessage("Please enter an email address");
    } else if (isInvalidEmail(event.currentTarget.value)) {
      setUserNameError(true);
      setUserNameErrorMessage("Please enter a valid email address");
    } else {
      setUserNameError(false);
      setUserNameErrorMessage("");
    }
    setUserName(event.currentTarget.value);
  };

  const onPasswordTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value)) {
      setPasswordError(true);
      setPasswordErrorMessage("Please enter a password");
    } else {
      setPasswordError(false);
      setPasswordErrorMessage("");
    }
    setPassword(event.currentTarget.value);
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (isNullOrEmpty(userName)) {
      setUserNameError(true);
      setUserNameErrorMessage("Please enter an email address");
    } else if (isNullOrEmpty(password)) {
      setPasswordError(true);
      setPasswordErrorMessage("Please enter an Password");
    } else {
      setIsLoading(true);
      login({
        UserName: userName,
        LoginPassword: password,
        WebToken: firebaseToken,
      })
        .then(({ data }) => {
          if (data.success) {
            localStorage.setItem("userId", data.result.Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result));
            // let userId = null
            // if (data.result.FreelancerId != null) userId = data.result.FreelancerId;
            // else if (data.result.JobSeekerId != null) userId = data.result.JobSeekerId;
            // else if (data.result.CompanyId != null) userId = data.result.CompanyId;
            // dispatch(getUser(userId))
            window.location.reload();
            // window.location.href = "/login";
            // setTimeout(() => {
            //   setIsLoading(false);
            //   setIsLoginOpen(false);
            //   if (data.result.FreelancerId != null) navigate("home-freelancer");
            //   else if (data.result.JobSeekerId != null) navigate("home-professional");
            //   else if (data.result.CompanyId != null) navigate("home-company");
            // }, 2000);
          } else {
            if (data.status === 534) {
              localStorage.setItem("emailTobeVerify", userName);
              setIsEmailVerificationOpen(true);
            } else {
              setHeadingAndTextForAlertDialog(data.errors);
              setAlertDialogVisibility(true);
            }
            setIsLoading(false);
          }
        })
        .catch((err) => {
          setHeadingAndTextForAlertDialog("Failed to Login");
          setAlertDialogVisibility(true);
          setIsLoading(false);
        });
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Login" description="Login" />
      <div className="pupup__container">
        <form
          onSubmit={handleFormSubmit}
          className="pupup__container__from animate__animated animate__slideInDown"
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => setIsLoginOpen(false)}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div className="pupup__container__from__top__left">
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px" }}
              >
                Welcome to <span>AIDApro</span>
              </div>
              <InputBox
                placeholder="Email"
                type="email"
                error={userNameError}
                errorMessage={userNameErrorMessage}
                onChange={onEmailTextChangeListener}
                name="userName"
                svg={
                  <svg
                    id="mail"
                    xmlns="http://www.w3.org/2000/svg"
                    width="17.919"
                    height="11.946"
                    viewBox="0 0 17.919 11.946"
                  >
                    <g id="Group_202" data-name="Group 202">
                      <path
                        id="Path_292"
                        data-name="Path 292"
                        d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                        transform="translate(0 -85.333)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
              />
              <InputBox
                placeholder="Password"
                type="password"
                secureKey={true}
                error={passwordError}
                errorMessage={passwordErrorMessage}
                name="password"
                onChange={onPasswordTextChangeListener}
                svg={
                  <svg
                    id="padlock"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.192"
                    height="14.922"
                    viewBox="0 0 11.192 14.922"
                  >
                    <path
                      id="Path_293"
                      data-name="Path 293"
                      d="M12.793,18.327H4.4a1.4,1.4,0,0,1-1.4-1.4V10.4A1.4,1.4,0,0,1,4.4,9h8.394a1.4,1.4,0,0,1,1.4,1.4v6.529A1.4,1.4,0,0,1,12.793,18.327ZM4.4,9.933a.467.467,0,0,0-.466.466v6.529a.467.467,0,0,0,.466.466h8.394a.467.467,0,0,0,.466-.466V10.4a.467.467,0,0,0-.466-.466Z"
                      transform="translate(-3 -3.404)"
                      fill="#374957"
                    />
                    <path
                      id="Path_294"
                      data-name="Path 294"
                      d="M12.995,6.529a.466.466,0,0,1-.466-.466V3.731a2.8,2.8,0,1,0-5.6,0V6.062a.466.466,0,0,1-.933,0V3.731a3.731,3.731,0,1,1,7.461,0V6.062A.466.466,0,0,1,12.995,6.529Z"
                      transform="translate(-4.135)"
                      fill="#374957"
                    />
                    <path
                      id="Path_295"
                      data-name="Path 295"
                      d="M11.244,15.487a1.244,1.244,0,1,1,1.244-1.244A1.245,1.245,0,0,1,11.244,15.487Zm0-1.554a.311.311,0,1,0,.311.311A.311.311,0,0,0,11.244,13.933Z"
                      transform="translate(-5.648 -4.917)"
                      fill="#374957"
                    />
                    <path
                      id="Path_296"
                      data-name="Path 296"
                      d="M11.716,18.393a.466.466,0,0,1-.466-.466v-1.71a.466.466,0,1,1,.933,0v1.71A.466.466,0,0,1,11.716,18.393Z"
                      transform="translate(-6.12 -5.957)"
                      fill="#374957"
                    />
                  </svg>
                }
              />
              <div className="pupup__container__from__top__left__row">
                <input
                  className="styled-checkbox"
                  id="styled-checkbox-remember-me"
                  type="checkbox"
                  value="Remember"
                  name="Remember"
                />
                <label htmlFor="styled-checkbox-remember-me">Remember Me</label>
                <button
                  type="button"
                  className="pupup__container__from__top__left__row__link"
                  onClick={() => {
                    setIsLoginOpen(false);
                    setIsForgotPasswordOpen(true);
                  }}
                  title="forgot password?"
                >
                  Forgot Password?
                </button>
              </div>

              <button
                type="submit"
                className="header__nav__btn btn__primary"
                onClick={(e) => handleFormSubmit(e)}
                disabled={isLoading ? true : false}
                style={{
                  width: "80%",
                  margin: "1em auto",
                  height: "50px",
                  marginTop: "2em",
                  marginBottom: "2em",
                }}
                title="login"
              >
                {isLoading ? "Processing..." : "Login"}
              </button>

              <button
                type="button"
                className="pupup__container__from__top__left__row__link"
                style={{
                  marginLeft: "25%",
                  marginTop: "1.5em",
                  width: "50%",
                }}
                title="sign up"
                onClick={() => {
                  setIsLoginOpen(false);
                  navigate(
                    redirectURLFromLogin === "/home-company"
                      ? "/sign-up"
                      : redirectURLFromLogin === "/home-professional"
                      ? "/sign-up"
                      : redirectURLFromLogin === "/home-freelancer"
                      ? "/sign-up"
                      : "/"
                  );
                }}
              >
                Create an account?{" "}
                <span style={{ color: "#0ca69d" }}>Sign Up</span>
              </button>
            </div>
            <Img
              loading="lazy"
              src={screeningByExperts}
              alt="screeningByExperts"
              className="pupup__container__from__top__right"
            />
          </div>
        </form>
      </div>
    </>
  );
};

export default Login;
