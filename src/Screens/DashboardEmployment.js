import React, { useContext, useEffect, useState } from "react";
import { DashboardGoogleMap } from "./DashboardGoogleMap";
import DashboardCompanySearchFilter from "Components/DashboardCompanySearchFilter";
import { Head } from "Components";
import { useDispatch, useSelector } from "react-redux";
import UserContext from "Context/UserContext";
import { getJobSekkerById } from "Redux/Actions/AppActions";
import DashboardEmploymentProfileCompletion from "Components/DashboardEmploymentProfileCompletion";

const DashboardEmployment = ({
  setIsFilterOpen,
  isFilterOpen,
  filterCall,
  title,
  keywords,
  userName,
  latitude,
  longitude,
  mapLocation,
  selectedRangeValue,
}) => {
  localStorage.setItem("isOn", "professional");

  let { jobsekker } = useSelector((state) => state.jobsekker);
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(getJobSekkerById(user.JobSeekerId));
  }, []);

  const [isLoading, setIsLoading] = useState(false);
  const [titleLocal, setTitleLocal] = useState(title);
  const [jobLocation, setJobLocation] = useState(mapLocation);
  const [lat, setLat] = useState(latitude);
  const [lng, setLng] = useState(longitude);
  let [profilePercentage, setProfilePercentage] = useState(0);
  const [showProfilePercentageScreen, setShowProfilePercentageScreen] =
    useState(false);

  let [radius, setRadius] = useState(
    selectedRangeValue ? selectedRangeValue : 500
  );

  const apiCall = () => {
    filterCall(titleLocal, jobLocation, radius, lat, lng);
  };

  const searchDataByTitle = (data) => {
    setTitleLocal(data);
  };

  const searchByLocation = (data, isInput) => {
    if (isInput == false) {
      if (data.name) {
        // setLocationError(true);
        // setLocationErrorMessage("Please enter your correct location");
      } else {
        setJobLocation(data.formatted_address);
        setLat(data.geometry.location.lat());
        setLng(data.geometry.location.lng());
      }
    } else {
      if (data.currentTarget.value === "" && data.name) {
        // setLocationError(true);
        // setLocationErrorMessage("Please enter your location");
      } else {
        // setLocationError(false);
        // setLocationErrorMessage("");
        setJobLocation(data.currentTarget.value);
      }
    }
  };

  const searchByKeyword = (data) => {
    setTitleLocal(data);
  };

  const searchByRange = (value) => {
    setRadius((radius = value));
  };

  const isSignUpProfileCompleted = () => {
    if (
      jobsekker.emailAddress != "" ||
      jobsekker.firstName != "" ||
      jobsekker.lastName != "" ||
      jobsekker.lat != "" ||
      jobsekker.lng != "" ||
      jobsekker.primaryAddress ||
      jobsekker.phoneNo != ""
    )
      return true;
    else return false;
  };

  const isPersonelDetailCompleted = () => {
    if (
      jobsekker.age != null &&
      jobsekker.gender != undefined &&
      jobsekker.language.length > 0 &&
      jobsekker.primaryZipCode != null &&
      isSignUpProfileCompleted()
    )
      return true;
    else return false;
  };

  const isProfessionalDetailCompleted = () => {
    if (jobsekker.qualification.length > 0 && jobsekker.skills.length > 0)
      return true;
    else return false;
  };

  const isPortfolioCompleted = () => {
    if (jobsekker.portfolios.length > 0) return true;
    else return false;
  };

  useEffect(() => {
    if (Object.keys(jobsekker).length > 0) {
      let signUpProfileCompletedPercentage = 0;
      let personelDetailCompletedPercentage = 0;
      let professionalDetailCompletedPercentage = 0;
      let portfolioCompletedPercentage = 0;
      if (isSignUpProfileCompleted()) {
        signUpProfileCompletedPercentage = 25;
      }
      if (isPersonelDetailCompleted()) {
        personelDetailCompletedPercentage = 25;
      }
      if (isProfessionalDetailCompleted()) {
        professionalDetailCompletedPercentage = 25;
      }
      if (isPortfolioCompleted()) {
        portfolioCompletedPercentage = 25;
      }
      let percentage =
        signUpProfileCompletedPercentage +
        personelDetailCompletedPercentage +
        professionalDetailCompletedPercentage +
        portfolioCompletedPercentage;
      setProfilePercentage(percentage);
      if (percentage != 100) {
        setShowProfilePercentageScreen(true);
      }
    }
  }, [jobsekker]);

  return (
    <>
      <Head
        title="AIDApro | Professional Dashboard"
        description="Professional Dashboard"
      />
      {showProfilePercentageScreen ? (
        <DashboardEmploymentProfileCompletion
          profilePercentage={profilePercentage}
          isPersonelDetailCompleted={isPersonelDetailCompleted}
          isProfessionalDetailCompleted={isProfessionalDetailCompleted}
          isPortfolioCompleted={isPortfolioCompleted}
        />
      ) : null}
      <div className="dashboard__company__container">
        <div className="dashboard__company__container__left">
          <div
            className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
            style={{ textAlign: "left", fontSize: 25 }}
          >
            Welcome <b>{userName}</b>
            <br />
            <span>Find your next challenge</span>
          </div>
          <DashboardCompanySearchFilter
            setIsFilterOpen={setIsFilterOpen}
            isFilterOpen={isFilterOpen}
            isMain={true}
            keywords={keywords}
            searchByTitle={(e) => searchDataByTitle(e)}
            searchByFilter={() => apiCall()}
            searchByLocation={searchByLocation}
            searchByKeyword={(e) => searchByKeyword(e)}
            searchByRange={searchByRange}
            jobLocation={jobLocation}
            title={titleLocal}
            radius={radius}
          />
        </div>
        <div className="dashboard__company__container__right">
          <DashboardGoogleMap lat={lat} lng={lng} jobLocation={jobLocation} />
        </div>
      </div>
    </>
  );
};
export default DashboardEmployment;
