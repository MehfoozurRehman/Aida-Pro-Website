import React, { useEffect, useState, useContext } from "react";
import HomepageFreelancerJumbotron from "Components/HomepageFreelancerJumbotron";
import FreelancerResultCard from "Components/HomePageFreelancerResultCard";
import { freelancerProjectsForHomePage } from "../API/FreelancerApi";
import { getAllKeywords } from "../API/Api";
import UserContext from "Context/UserContext";
import { Head, MobileAppAdvert, Testmonials } from "Components";
import { navigate } from "@reach/router";
import { isUserLoggedIn } from "Utils/common";
import Header from "Components/Header";
import Img from "react-cool-img";
import axios from "axios";
import _ from "lodash";
import {
  filteredSkills,
  hassleFreeJobRecruitment,
  mainsvgcolored3,
} from "Assets";

const HomepageFreelancer = ({
  setIsLoginOpen,
  setRedirectURLFromLogin,
  setIsApplyForJob,
}) => {
  localStorage.setItem("isOn", "freelancer");
  const [projects, setProjects] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentJobId, setCurrentJobId] = useState();
  const [isShowJobCard, setIsShowJobCard] = useState(false);
  const [title, setTitle] = useState("");
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [keywords, setKeywords] = useState([]);
  const user = useContext(UserContext);
  const [languages, setLanguages] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://gist.githubusercontent.com/MehfoozurRehman/b41d62e2f419c8212b196bab53826528/raw/c68dc51538002598245f18d4113c77d620348cda/languages.json"
      )
      .then((res) => {
        setLanguages(res.data);
      });
  }, []);

  useEffect(() => {
    let response = isUserLoggedIn(user);
    if (response) {
      if (user.Freelancer != null) navigate("/home-freelancer");
      else if (user.JobSeeker != null) navigate("/home-professional");
      else if (user.CompanyProfile != null) navigate("/home-company");
    }
  }, [user]);

  useEffect(() => {
    setRedirectURLFromLogin("/home-freelancer");
  }, []);

  const freelancerProjectApi = () => {
    setIsLoading(true);
    freelancerProjectsForHomePage(page, limit, title)
      .then(({ data }) => {
        const filteredResult = _.slice(data.result, [0], [6]);
        setProjects(filteredResult);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const getAllKeywordsApi = () => {
    setIsLoading(true);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    freelancerProjectApi();
  }, [page]);

  // useEffect(() => {
  //   // freelancerProjectApi();
  //   getAllKeywordsApi();
  // }, []);

  // useEffect(() => {
  //   if (title != "") {
  //     freelancerProjectApi();
  //   }
  // }, [title]);

  const handleShowJobCard = (selectedJobData) => {
    setCurrentJobId(selectedJobData);
    setIsShowJobCard(!isShowJobCard);
    freelancerProjectApi();
  };

  const loadMore = () => {
    if (user.Id !== undefined) {
      window.location.href = "/home-freelancer";
    } else {
      window.location.href = "/login";
    }
  };

  const navigateToDetail = (dataObject) => {
    if (user.Id !== undefined) {
      navigate("/project-details", { state: dataObject });
    } else {
      setIsLoginOpen(true);
    }
  };

  return (
    <>
      <Head title="AIDApro | Freelancer" description="Freelancer" />
      <Header
        isOnHomeCompany={false}
        setIsLoginOpen={setIsLoginOpen}
        redirectURLSignUp="/sign-up"
      />
      <HomepageFreelancerJumbotron
        user={user}
        setIsLoginOpen={setIsLoginOpen}
        mainsvgcolored3={mainsvgcolored3}
      />
      <div className="homepage__container">
        <div
          className="homepage__container__results"
          style={{ marginBottom: "3em" }}
        >
          {isLoading
            ? null
            : projects &&
              projects
                .slice(0, 3)
                .map((e, i) => (
                  <FreelancerResultCard
                    key={i}
                    isFreelancer={true}
                    data={e}
                    moveTo={navigateToDetail}
                  />
                ))}
        </div>

        <div className="homepage__container__vacancy">
          <div className="homepage__container__vacancy__content">
            <div className="homepage__container__vacancy__content__heading">
              The platform to <span>cash using your skills</span>
            </div>
            <div className="homepage__container__vacancy__content__info">
              Sign up and offer your talent. Explore opportunities and start
              working. AIDApro is your platform to grow your portfolio and
              knowledge.
            </div>
          </div>
          <Img
            loading="lazy"
            src={hassleFreeJobRecruitment}
            alt="hassleFreeJobRecruitment"
            className="homepage__container__vacancy__img"
            style={{ height: 420 }}
          />
        </div>
        <div className="homepage__container__tagline">
          <Img
            loading="lazy"
            src={filteredSkills}
            alt="filteredSkills"
            className="homepage__container__tagline__background"
          />
          <div className="homepage__container__tagline__content">
            <div className="homepage__container__tagline__heading">
              Projects and jobs in <span>100+ different skills</span>.
            </div>
            <div
              className="homepage__container__tagline__info"
              style={{ flexWrap: "wrap" }}
            >
              {languages
                .filter((item, i) => i < 20)
                .map((item, i) => (
                  <span key={i} style={{ marginLeft: 10 }}>
                    {item}
                  </span>
                ))}
            </div>
            <div
              onClick={() => {
                if (user.Id !== undefined)
                  window.location.href = "/home-freelancer";
                else setIsLoginOpen(true);
              }}
              className="header__nav__btn btn__secondary"
              style={{ width: 180 }}
            >
              Find projects
            </div>
          </div>
        </div>
      </div>
      <Testmonials />
      <MobileAppAdvert />
      <div className="homepage__container"></div>
    </>
  );
};

export default HomepageFreelancer;
