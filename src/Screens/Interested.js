import React, { useContext, useState, useEffect } from "react";
import { Head } from "Components";
import InterestedCard from "Components/InterestedCard";
import Pagination from "react-js-pagination";
import { getAllInterestedProjectByFreelancerId } from "API/FreelancerApi";
import UserContext from "Context/UserContext";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "@reach/router";
import { getFreelancerById, getJobSekkerById } from "Redux/Actions/AppActions";
import { getAllInterestedJobByJobseekerId } from "API/EmploymentAPI";
import { interestedSelected } from "Assets";
import noData from "../Assets/noInterested.svg";
import Img from "react-cool-img";

export default function Interested() {
  const isOn = window.localStorage.getItem("isOn");
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  const [totalCards, setTotalCards] = useState();
  const [interestedData, setInterestedData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const user = useContext(UserContext);
  let dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "freelancer") {
      dispatch(getFreelancerById(user.FreelancerId));
    }
  }, []);

  let { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    getAllinterested();
  }, [page]);

  const getAllinterested = () => {
    setIsLoading(true);
    if (isOn === "professional") {
      getAllInterestedJobByJobseekerId(jobsekker.Id, page, limit)
        .then(({ data }) => {
          if (data.success) {
            setInterestedData(data.result);
            setTotalCards(data.totalRecords);
            setPage(data.page);
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
        });
    } else {
      getAllInterestedProjectByFreelancerId(jobsekker.Id, page, limit)
        .then(({ data }) => {
          if (data.success) {
            setInterestedData(data.result);
            setTotalCards(data.totalRecords);
            setPage(data.page);
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
        });
    }
  };

  const handlePageChange = (pageNumber) => {
    setPage(pageNumber);
  };

  const navigateToDetail = (dataObject) => {
    if (interestedData[0].Project != undefined)
      navigate("/project-details", { state: dataObject });
    else navigate("/job-details", { state: dataObject });
  };

  return (
    <>
      <Head
        title="AIDApro | Freelancer Projects"
        description="Freelancer Projects"
      />
      <div className="freelancer__project__container">
        <div className="freelancer__project__container__header">
          <div className="freelancer__project__container__header__heading">
            <Img
              loading="lazy"
              src={interestedSelected}
              alt=""
              style={{ marginRight: 10 }}
            />
            Interesting {isOn == "professional" ? "vacancies" : "projects"}
          </div>
        </div>
        {interestedData.length > 0 ? (
          <>
            <div
              className="homepage__container__results"
              style={{ marginTop: "0em" }}
            >
              {interestedData.map((item, i) => (
                <InterestedCard key={i} item={item} moveTo={navigateToDetail} />
              ))}
            </div>
            <div className="posting__container__pagination">
              <Pagination
                activePage={page}
                itemsCountPerPage={limit}
                totalItemsCount={totalCards}
                pageRangeDisplayed={10}
                onChange={(e) => handlePageChange(e)}
              />
            </div>
          </>
        ) : (
          <div className="no__data__container" style={{ height: 750 }}>
            <Img
              loading="lazy"
              src={noData}
              className="no__data__container__img"
            />
            <div className="no__data__container__text">No interests yet</div>
          </div>
        )}
      </div>
    </>
  );
}
