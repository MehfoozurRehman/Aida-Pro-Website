import React, { useEffect } from "react";
import { Head } from "Components";
import { Offline } from "react-detect-offline";
import Img from "react-cool-img";
import { offlineSvg } from "Assets";

export default function NoInternet() {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Offline" description="Offline" />
      <Offline>
        <div
          className="pupup__container"
          style={{
            flexDirection: "column",
            fontSize: 30,
            textAlign: "center",
            display: "none",
          }}
        >
          <Img
            loading="lazy"
            src={offlineSvg}
            alt="offlineSvg"
            style={{ width: 400, marginBottom: 40 }}
          />
          You're offline right now. Check your connection.
          <button
            onClick={() => {
              window.location.reload();
            }}
            className="header__nav__btn btn__secondary"
            style={{ width: 180, marginTop: "1em" }}
            title="retry"
          >
            Retry
          </button>
        </div>
      </Offline>
    </>
  );
}
