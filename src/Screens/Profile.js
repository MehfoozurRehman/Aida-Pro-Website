import React, { useContext, useState } from "react";
import { navigate } from "@reach/router";
import { Head } from "Components";
import Img from "react-cool-img";
import { CustomError, CustomSuccess } from "./Toasts";
import { changePassword } from "API/Api";
import UserContext from "Context/UserContext";
import { isNullOrEmpty } from "Utils/TextUtils";
import { isInvalidPassword } from "Utils/Validations";
import { ProfileSvg } from "Assets";
import ProfileCompany from "Components/ProfileCompany";
import ProfileOther from "Components/ProfileOther";

export default function Profile({ setIsLoginOpen }) {
  const isOn = window.localStorage.getItem("isOn");
  const user = useContext(UserContext);
  const [isOnCompany, setIsOnCompany] = useState(false);
  const [isOnOther, setIsOnOther] = useState(true);
  const [oldPassword, setOldPassword] = useState("");
  let [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  // validation
  let [oldPasswordError, setOldPasswordError] = useState(false);
  let [oldPasswordErrorMessage, setOldPasswordErrorMessage] = useState("");
  let [newPasswordError, setNewPasswordError] = useState(false);
  let [newPasswordErrorMessage, setNewPasswordErrorMessage] = useState("");
  let [confrimPasswordError, setConfirmPasswordError] = useState(false);
  let [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
    useState("");

  const onOldPasswordTextChangeListener = (event) => {
    if (event.currentTarget.value === "") {
      setOldPasswordError(true);
      setOldPasswordErrorMessage("Please enter old password");
    } else {
      setOldPasswordError(false);
      setOldPasswordErrorMessage("");
      setOldPassword(event.currentTarget.value);
    }
  };

  const onPasswordTextChangeListener = (event) => {
    if (isNullOrEmpty(event.currentTarget.value))
      setNewPasswordErrorMessageAndVisibility("Please enter new password");
    else if (isInvalidPassword(event.currentTarget.value))
      setNewPasswordErrorMessageAndVisibility("Min 8 characters");
    else setNewPasswordErrorMessageAndVisibility("");
    setNewPassword((newPassword = event.currentTarget.value));
    if (!isNullOrEmpty(confirmPassword))
      onConfirmPasswordTextChangeListener(confirmPassword);
  };

  const onConfirmPasswordTextChangeListener = (enteredValue) => {
    if (isNullOrEmpty(enteredValue))
      setConfirmPasswordErrorMessageAndVisibility(
        "Please enter confirm password"
      );
    else if (isInvalidPassword(enteredValue))
      setConfirmPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (enteredValue !== newPassword)
      setConfirmPasswordErrorMessageAndVisibility("Password do not match!");
    else setConfirmPasswordErrorMessageAndVisibility("");
    setConfirmPassword(enteredValue);
  };

  const setNewPasswordErrorMessageAndVisibility = (text) => {
    setNewPasswordError((newPasswordError = !isNullOrEmpty(text)));
    setNewPasswordErrorMessage(text);
  };

  const setConfirmPasswordErrorMessageAndVisibility = (text) => {
    setConfirmPasswordError((confrimPasswordError = !isNullOrEmpty(text)));
    setConfirmPasswordErrorMessage(text);
  };

  const viewIsValid = () => {
    if (isNullOrEmpty(oldPassword))
      setOldPasswordErrorMessage("Please enter old password");
    else if (isNullOrEmpty(newPassword))
      setNewPasswordErrorMessageAndVisibility("Please enter new password");
    else if (isInvalidPassword(newPassword))
      setNewPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (isNullOrEmpty(confirmPassword))
      setConfirmPasswordErrorMessageAndVisibility(
        "Please enter confirm password"
      );
    else if (isInvalidPassword(confirmPassword))
      setConfirmPasswordErrorMessageAndVisibility("Min 8 characters");
    else if (newPassword != confirmPassword)
      setConfirmPasswordErrorMessageAndVisibility("Password donot match");
    else return true;
    return false;
  };

  const onChangePassword = () => {
    if (viewIsValid()) {
      if (newPassword == confirmPassword) {
        let changePasswordObject = {
          Id: user.Id,
          OldPassword: oldPassword,
          NewPassword: newPassword,
        };

        changePassword(changePasswordObject)
          .then((data) => {
            if (data.data.success) {
              //CustomSuccess("Your password has been changed successfully.");
              if (isOn == "professional")
                navigate("/home-professional/profile");
              else if (isOn == "freelancer")
                navigate("/home-freelancer/profile");
            } else {
              CustomError(data.data.errors);
            }
          })
          .catch((err) => {
            CustomError(err);
          });
      } else {
        CustomError("New and old password aren't same.");
      }
    }
  };

  return (
    <>
      <Head
        title={isOnOther ? "AIDApro | Change Password" : "AIDApro | Profile"}
        description={isOnOther ? "Change Password" : "Profile"}
      />
      <div style={{ width: "20%", fontWeight: "bold", fontSize: 22 }}>
        Change Password
      </div>
      <div className="homepage__container__profile__wrapper">
        <div className="homepage__container__profile__wrapper__left">
          {isOnCompany ? (
            <ProfileCompany />
          ) : isOnOther ? (
            <ProfileOther
              oldPasswordError={oldPasswordError}
              oldPasswordErrorMessage={oldPasswordErrorMessage}
              onOldPasswordTextChangeListener={onOldPasswordTextChangeListener}
              newPasswordError={newPasswordError}
              newPasswordErrorMessage={newPasswordErrorMessage}
              onPasswordTextChangeListener={onPasswordTextChangeListener}
              confrimPasswordError={confrimPasswordError}
              confirmPasswordErrorMessage={confirmPasswordErrorMessage}
              onConfirmPasswordTextChangeListener={
                onConfirmPasswordTextChangeListener
              }
              onChangePassword={onChangePassword}
            />
          ) : null}
        </div>
        <div className="homepage__container__profile__wrapper__right">
          <Img
            loading="lazy"
            style={{ width: "90%", marginLeft: "1em" }}
            src={ProfileSvg}
            alt="ProfileSvg"
          />
        </div>
      </div>
    </>
  );
}
