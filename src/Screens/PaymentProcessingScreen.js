import React, { useState, useEffect } from "react";
import { Link } from "@reach/router";
import { updatePaymentStatus } from "../API/Plans";
import { Head } from "Components";

export default function PaymentProcessingScreen() {
  const paymentDone = true;
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    callUpdatePaymentStatusAPI();
  }, []);

  const callUpdatePaymentStatusAPI = () => {
    const transactionId = localStorage.getItem("TransactionId");
    let requestData = {
      TransactionId: transactionId,
      Status: 2,
    };

    updatePaymentStatus(requestData)
      .then(({ data }) => {
        setIsLoading(false);
        localStorage.removeItem("TransactionId");
      })
      .catch((e) => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <Head
        title="AIDApro | Payment Processing"
        description="Payment Processing"
      />

      <div className="payment__container">
        {paymentDone ? (
          <div className="payment__container__done">
            <div className="payment__container__svg">
              <svg
                id="Capa_1"
                enable-background="new 0 0 512 512"
                viewBox="0 0 512 512"
                xmlns="http://www.w3.org/2000/svg"
              >
                <linearGradient
                  id="SVGID_1_"
                  gradientUnits="userSpaceOnUse"
                  x1="248.5"
                  x2="248.5"
                  y1="496"
                  y2="31"
                >
                  <stop offset="0" stopColor="#00b59c" />
                  <stop offset="1" stopColor="#9cffac" />
                </linearGradient>
                <linearGradient
                  id="SVGID_2_"
                  gradientUnits="userSpaceOnUse"
                  x1="286.5"
                  x2="286.5"
                  y1="436"
                  y2="16"
                >
                  <stop offset="0" stopColor="#c3ffe8" />
                  <stop offset=".9973" stopColor="#f0fff4" />
                </linearGradient>
                <g>
                  <g>
                    <g>
                      <path
                        d="m376 31c-66.167 0-120 53.833-120 120 0 15.93 3.265 31.079 8.926 45h-249.926c-8.284 0-15 6.716-15 15v270c0 8.284 6.716 15 15 15h421c8.284 0 15-6.716 15-15v-237.089c27.217-22.017 46-55.26 46-92.911 0-66.167-54.833-120-121-120z"
                        fill="url(#SVGID_1_)"
                      />
                    </g>
                  </g>
                  <g>
                    <g>
                      <path
                        d="m106 406h-30c-8.291 0-15 6.709-15 15s6.709 15 15 15h30c8.291 0 15-6.709 15-15s-6.709-15-15-15zm90 0h-30c-8.291 0-15 6.709-15 15s6.709 15 15 15h30c8.291 0 15-6.709 15-15s-6.709-15-15-15zm0-150h-120c-8.291 0-15 6.709-15 15v30c0 8.291 6.709 15 15 15h120c8.291 0 15-6.709 15-15v-30c0-8.291-6.709-15-15-15zm90 150h-30c-8.291 0-15 6.709-15 15s6.709 15 15 15h30c8.291 0 15-6.709 15-15s-6.709-15-15-15zm90 0h-30c-8.291 0-15 6.709-15 15s6.709 15 15 15h30c8.291 0 15-6.709 15-15s-6.709-15-15-15zm0-390c-74.443 0-135 60.557-135 135s60.557 135 135 135 136-60.557 136-135-61.557-135-136-135zm60.527 110.684-72.422 72.422c-2.93 2.93-6.768 4.395-10.605 4.395s-7.676-1.465-10.605-4.395l-34.922-34.922c-5.859-5.859-5.859-15.352 0-21.211s15.352-5.859 21.211 0l24.316 24.316 61.816-61.816c5.859-5.859 15.352-5.859 21.211 0s5.86 15.351 0 21.211z"
                        fill="url(#SVGID_2_)"
                      />
                    </g>
                  </g>
                </g>
              </svg>
            </div>
            <div className="payment__container__heading">
              Great payment sucessful thanks for working with us!
            </div>
            <Link
              to="#"
              className="payment__container__button"
              onClick={() => window.close()}
            >
              Back to Home
            </Link>
          </div>
        ) : (
          <div className="payment__container__not__done">
            <div className="payment__container__svg">
              <svg
                id="Layer_3"
                fill="#cc2f32"
                viewBox="0 0 64 64"
                xmlns="http://www.w3.org/2000/svg"
                data-name="Layer 3"
              >
                <path d="m25 28v-8a1 1 0 0 0 -1-1h-3v2h2v2h-2v6h3a1 1 0 0 0 1-1z" />
                <path d="m16 29h3v-2h-2v-2h2v-6h-3a1 1 0 0 0 -1 1v8a1 1 0 0 0 1 1z" />
                <path d="m53 11v-1a3 3 0 0 0 -3-3h-44a3 3 0 0 0 -3 3v5h4.1a5.009 5.009 0 0 1 4.9-4z" />
                <path d="m41.03 32.757 1.94.486-.555 2.221a12.927 12.927 0 0 1 16.585 1.177v-20.641a3 3 0 0 0 -3-3h-44a3 3 0 0 0 -3 3v26a3 3 0 0 0 3 3h25.051a12.913 12.913 0 0 1 2.641-6.89zm13.97-1.757h-2v-2h2zm-24-14h24v2h-24zm0 4h24v2h-24zm0 4h24v2h-24zm-18-5a3 3 0 0 1 3-3h8a3 3 0 0 1 3 3v8a3 3 0 0 1 -3 3h-8a3 3 0 0 1 -3-3zm23 21h-23v-2h23zm-5-10v-2h20v2z" />
                <path d="m3 17h4v4h-4z" />
                <path d="m3 36a3 3 0 0 0 3 3h1v-16h-4z" />
                <path d="m50 35a11 11 0 1 0 11 11 11.013 11.013 0 0 0 -11-11zm7 15-3 3-4-4-4 4-3-3 4-4-4-4 3-3 4 4 4-4 3 3-4 4z" />
              </svg>
            </div>
            <div className="payment__container__heading">
              There was some problem
            </div>
            <Link to="/contact" className="payment__container__button">
              Let us help you
            </Link>
          </div>
        )}
      </div>
    </>
  );
}
