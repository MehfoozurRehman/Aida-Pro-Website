import React, { useEffect } from "react";
import { Head } from "Components";
import { crossSvg } from "Assets";

export default function PaymentConfirmation({
  onClose,
  heading,
  text,
  onConfirm,
  onDecline,
}) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Email Verification"
        description="Email Verification"
      />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              onClose(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  textAlign: "center",
                  marginTop: 0,
                }}
              >
                {heading}
              </div>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "14px", textAlign: "center", marginTop: 0 }}
              >
                {text}
              </div>
              <div
                style={{
                  display: "flex",
                  width: 250,
                  margin: "0em auto",
                }}
              >
                <button
                  type="submit"
                  className="header__nav__btn btn__primary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "35px",
                    width: "120px",
                    marginRight: "1em",
                    fontSize: 12,
                  }}
                  onClick={onConfirm}
                  title="confirm"
                >
                  Continue
                </button>
                <button
                  type="submit"
                  className="header__nav__btn btn__secondary"
                  style={{
                    margin: "0em auto",
                    marginTop: "2em",
                    height: "35px",
                    width: "120px",
                    fontSize: 12,
                  }}
                  onClick={onDecline}
                  title="decline"
                >
                  No
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
