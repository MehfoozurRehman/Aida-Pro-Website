import React from "react";
import { Link } from "@reach/router";
import Header from "Components/Header";
import { Head } from "Components";

export default function PrivacyPolicy({ setIsLoginOpen }) {
  return (
    <>
      <Head title="AIDApro | Privacy Policy" description="Privacy Policy" />
      <Header isOnHomeCompany={false} setIsLoginOpen={setIsLoginOpen} />
      <div className="homepage__container__jumbotron">
        <div className="homepage__container__jumbotron__terms__conditions__headings">
          <Link
            to="/"
            style={{
              display: "flex",

              marginRight: "auto",
              marginBottom: -30,
            }}
          >
            <div className="homepage__freelancer__project__details__title__container__svg">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="7.639"
                height="13.363"
                style={{ marginRight: 5 }}
                viewBox="0 0 7.639 13.363"
              >
                <path
                  id="Icon_ionic-ios-arrow-back"
                  data-name="Icon ionic-ios-arrow-back"
                  d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                  transform="translate(-11.251 -6.194)"
                />
              </svg>
            </div>
            <div className="homepage__freelancer__project__details__title__container__heading">
              Back
            </div>
          </Link>
          <div className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown">
            <span>Privacy Policy</span>
          </div>
          <div
            className="homepage__container__jumbotron__info animate__animated animate__fadeInLeft"
            style={{ textAlign: "justify" }}
          >
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam leo
            leo, laoreet eu rutrum nec, pretium quis libero. Maecenas et nisi eu
            tortor luctus pharetra. Ut ullamcorper a dui malesuada egestas.
            Nulla eu turpis id nisl tempor tincidunt sit amet eu justo. Ut
            volutpat, sapien eu euismod venenatis, neque felis luctus nunc, non
            suscipit dui purus non tortor. Aliquam at eros vitae tortor mollis
            egestas. Vestibulum molestie nisl nec rutrum varius. Fusce lorem
            velit, suscipit id dictum eu, convallis pretium enim. Ut ligula
            orci, blandit elementum metus ut, commodo vulputate leo. Proin
            ullamcorper volutpat nunc, non aliquam erat accumsan eu. Phasellus
            aliquam ipsum nec mollis tempor. Vivamus efficitur est sit amet ex
            aliquet cursus. Nunc et leo sem. Maecenas ut commodo metus, vel
            pharetra lorem. Cras id dui eleifend, tempus arcu sed, ultrices
            enim.
          </div>
          <div className="homepage__container__jumbotron__terms__conditions__sub__headings">
            <div className="homepage__container__jumbotron__terms__conditions__sub__heading animate__animated animate__fadeInDown">
              Restrictions
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__info animate__animated animate__fadeInRight">
              Aenean eu lectus eu nulla fermentum efficitur. Vestibulum vel
              eleifend quam. Phasellus tincidunt viverra felis, sit amet
              tincidunt felis fermentum eu. Donec laoreet malesuada ligula sit
              amet maximus. Pellentesque sit amet justo est. Proin sollicitudin,
              metus in placerat tincidunt, tortor libero tempus sem, non
              pellentesque est arcu vitae nunc. Donec suscipit eleifend sapien
              eleifend tincidunt. Pellentesque vel nunc id lectus auctor
              feugiat. Maecenas tempor rutrum nunc interdum finibus. Etiam erat
              enim, consequat id condimentum a, tristique vitae tellus. Mauris
              eu orci sit amet enim rutrum dignissim. Integer tristique massa at
              sagittis tristique. Sed blandit tortor consectetur odio sodales
              tincidunt.
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__sub__heading animate__animated animate__fadeInDown">
              Do I need Privacy Policy?
            </div>
            <div className="homepage__container__jumbotron__terms__conditions__info animate__animated animate__fadeInLeft">
              Unlike a privacy policy, terms & conditions are not a legal
              requirement on your website however it is still recommended that
              every website has one. Terms & conditions are designed to limit
              your liability in the event of a claim by one of your users. The
              courts will use your terms and conditions as a basis for assessing
              the validity of any potential complaint so it should still be
              considered a very important document. Unlike a privacy policy,
              terms & conditions are not a legal requirement on your website
              however it is still recommended that every website has one. Terms
              & conditions are designed to limit your liability in the event of
              a claim by one of your users. The courts will use your terms and
              conditions as a basis for assessing the validity of any potential
              complaint so it should still be considered a very important
              document.
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
