import React, { useEffect, useRef, useState } from "react";
import MessangerSidebar from "Components/MessangerSidebar";
import MessangerContent from "Components/MessangerContent";
import { chat, chatSelected, noChat } from "Assets";
import { Head } from "Components";
import { CustomError } from "./Toasts";
import { useSelector } from "react-redux";
import firebase from "firebase/app";
import { useNavigate } from "@reach/router";
import {
  approveChat,
  deleteChat,
  getChatUsersOfCompany,
  uploadFile,
} from "API/Chat";

export default function Messenger({
  location,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
  setVideoCallUserData,
  setIsRequestVideoCallOpen,
  setIsDeleteConfirmationResponse,
}) {
  const [chatId, setChatId] = useState(
    location.state.ChatNodeName != undefined
      ? location.state.ChatNodeName
      : null
  );

  const isOn = window.localStorage.getItem("isOn");
  const [isOnline, setIsOnline] = useState(false);
  let [chatUsers, setChatUsers] = useState(null);
  let [searchedUsers, setSearchedUsers] = useState(null);
  let [file, setFile] = useState(null);
  const [messages, setMessages] = useState(null);
  let [chatNodeName, setChatNodeName] = useState("");
  const [newMessage, setNewMessage] = useState("");
  const [requestApproved, setRequestApproved] = useState(false);
  let [selectedChat, setSelectedChat] = useState(null);
  const [isSelectedUserOnline, setIsSelectedUserOnline] = useState(false);
  const navigate = useNavigate();
  const { user } = useSelector((state) => state.user);
  let numberOfMessages = requestApproved ? Infinity : 1;

  useEffect(() => {
    setIsDeleteConfirmationResponse(false);
    getChatUsersList();
  }, []);

  const getChatUsersList = async () => {
    let response = await getChatUsersOfCompany(user);
    if (response.data != undefined) {
      if (response.data.success) {
        if (response.data.result.length > 0) {
          const found = response.data.result.find(
            (element) => element.ChatNodeName == chatId
          );

          if (found != undefined) {
            setSelectedChat(found);
            setChatNodeName((chatNodeName = found.ChatNodeName));
          } else {
            setSelectedChat(response.data.result[0]);
            setChatNodeName(
              (chatNodeName = response.data.result[0].ChatNodeName)
            );
          }
          firebase
            .database()
            .ref(
              `/status/${
                found != undefined
                  ? found.FirebaseId
                  : response.data.result[0].FirebaseId
              }`
            )
            .on("value", (snapshot) => {
              if (snapshot.val() != undefined) {
                if (snapshot.val().state == "online")
                  setIsSelectedUserOnline(true);
                else setIsSelectedUserOnline(false);
              } else setIsSelectedUserOnline(false);
            });
          setChatUsers((chatUsers = response.data.result));
          fetchMessages();
        }
      } else CustomError(response.data.errors);
    } else CustomError("Some error occured");
  };

  const fetchMessages = async () => {
    const messageListener = firebase
      .firestore()
      .collection("Chat")
      .doc(chatNodeName)
      .collection("Message")
      .orderBy("createdAt", "asc")
      .onSnapshot((querySnapShot) => {
        const firebaseMessages = querySnapShot.docs.map((doc) => {
          const firebaseData = doc.data();
          const data = {
            _id: doc.id,
            message: "",
            ...firebaseData,
          };
          return data;
        });
        setMessages(firebaseMessages);
      });
    return () => messageListener();
  };

  const onClickChatNode = (e) => {
    setFile(null);
    setSelectedChat(e);
    setChatNodeName((chatNodeName = e.ChatNodeName));

    firebase
      .database()
      .ref(`/status/${e.FirebaseId}`)
      .on("value", (snapshot) => {
        if (snapshot.val() != undefined) {
          if (snapshot.val().state == "online") setIsSelectedUserOnline(true);
          else setIsSelectedUserOnline(false);
        } else setIsSelectedUserOnline(false);
      });

    fetchMessages();
  };

  const onClickSend = async (type) => {
    if (newMessage != "" || file != null) {
      setNewMessage("");
      var db = firebase.firestore();
      let fromId = 0;
      let loggedInFrom = "";
      if (user.CompanyId != null) {
        fromId = user.CompanyId;
        loggedInFrom = "company";
      } else if (user.FreelancerId != null) {
        fromId = user.FreelancerId;
        loggedInFrom = "freelancer";
      } else {
        fromId = user.JobSeekerId;
        loggedInFrom = "job_seeker";
      }

      let toId = null;
      if (loggedInFrom != "company" && selectedChat.CompanyId != null) {
        toId = selectedChat.CompanyId;
      } else if (
        loggedInFrom != "freelancer" &&
        selectedChat.FreelancerId != null
      ) {
        toId = selectedChat.FreelancerId;
      } else if (
        loggedInFrom != "job_seeker" &&
        selectedChat.JobSeekerId != null
      ) {
        toId = selectedChat.JobSeekerId;
      }

      let data = {
        from: fromId,
        to: toId,
        date: new Date(),
        type:
          loggedInFrom == "company"
            ? "Company"
            : loggedInFrom == "job_seeker"
            ? "Employment"
            : "Freelancer",
        createdAt: new Date().getTime(),
        message: newMessage,
        attachment: file,
        attachment_type: type,
        ChatNodeName: selectedChat.ChatNodeName,
      };

      let notificationData = {
        from: fromId,
        to: toId,
        senderName: user.LoginName,
        date: new Date(),
        createdAt: new Date().getTime(),
        isRead: false,
        message: newMessage,
        ChatNodeName: selectedChat.ChatNodeName,
      };
      let docID = "";
      if (loggedInFrom == "company") {
        if (loggedInFrom != "freelancer" && selectedChat.FreelancerId != null) {
          docID = "Freelancer_" + selectedChat.FreelancerId;
        } else if (
          loggedInFrom != "job_seeker" &&
          selectedChat.JobSeekerId != null
        ) {
          docID = "Jobseeker_" + selectedChat.JobSeekerId;
        }
      } else docID = "Company_" + selectedChat.CompanyId;
      setFile(null);
      await db
        .collection("Chat")
        .doc(selectedChat.ChatNodeName)
        .collection("Message")
        .add(data)
        .then(async (res) => {
          fetchMessages();
          await firebase
            .firestore()
            .collection("Notification")
            .doc(docID)
            .collection("Message")
            .add(notificationData)
            .then((res) => {})
            .catch((err) => {
              // console.log("notification failed to send error", err);
            });
        })
        .catch((err) => {
          // console.log("chat message send err", err);
        });
    } else {
      CustomError("Please type something");
    }
  };

  const onClickApprove = () => {
    let chatApproveObject = {
      CompanyId:
        selectedChat.CompanyId != undefined ? selectedChat.CompanyId : null,
      ChatId: selectedChat.ChatId,
      JobSeekerId:
        selectedChat.JobSeekerId != undefined ? selectedChat.JobSeekerId : null,
      FreelancerId:
        selectedChat.FreelancerId != undefined
          ? selectedChat.FreelancerId
          : null,
      ChatNodeName: selectedChat.ChatNodeName,
      IsVideo: false,
      VideoNodeName: null,
      IsChatRequestAccepted: true,
    };

    approveChat(chatApproveObject)
      .then((data) => {
        getChatUsersList();
      })
      .catch((err) => {
        CustomError(err);
      });
  };

  const onClickDecline = () => {
    setIsDeleteConfirmation(true);
  };

  useEffect(() => {
    if (isDeleteConfirmationResponse) deleteChatRequest();
  }, [isDeleteConfirmationResponse]);

  const deleteChatRequest = () => {
    debugger;
    var object = {
      Id: selectedChat.ChatId,
    };
    deleteChat(object)
      .then((data) => {
        setChatUsers(null);
        setMessages(null);
        setSelectedChat(null);
        setChatNodeName("");
        setTimeout(() => {
          getChatUsersList();
        }, 800);
        setIsDeleteConfirmationResponse(false);
      })
      .catch((err) => {
        CustomError(err);
        setIsDeleteConfirmationResponse(false);
      });
  };

  const AlwaysScrollToBottom = () => {
    const elementRef = useRef();
    useEffect(() => elementRef.current.scrollIntoView());
    return <div ref={elementRef} />;
  };

  const onSearchUser = (value) => {
    let filterData = chatUsers.filter((data) =>
      data.FirstName.toUpperCase().includes(value.toUpperCase())
    );
    setSearchedUsers(filterData);
  };

  const showName = (data) => {
    let name = "";
    let firstName = data.FirstName;
    name = name + firstName;
    let lastName = data.LastName;
    if (data.LastName != null) name = name + " " + lastName;

    return (
      <>
        {name.length > 15 ? (
          <div>{name.substring(0, 15) + "..."}</div>
        ) : (
          <div>{name}</div>
        )}
      </>
    );
  };

  const fileSelectedHandler = (img, type) => {
    let fromId = 0;
    let loggedInFrom = "";
    if (user.CompanyId != null) {
      fromId = user.CompanyId;
      loggedInFrom = "company";
    } else if (user.FreelancerId != null) {
      fromId = user.FreelancerId;
      loggedInFrom = "freelancer";
    } else {
      fromId = user.JobSeekerId;
      loggedInFrom = "job_seeker";
    }

    let toId = null;
    if (loggedInFrom != "company" && selectedChat.CompanyId != null) {
      toId = selectedChat.CompanyId;
    } else if (
      loggedInFrom != "freelancer" &&
      selectedChat.FreelancerId != null
    ) {
      toId = selectedChat.FreelancerId;
    } else if (
      loggedInFrom != "job_seeker" &&
      selectedChat.JobSeekerId != null
    ) {
      toId = selectedChat.JobSeekerId;
    }

    let object = {
      Id: 0,
      ChatNodeName: selectedChat.ChatNodeName,
      fromId: parseInt(fromId),
      toId: parseInt(toId),
    };

    var tmppath = URL.createObjectURL(img[0]);
    setFile(tmppath);

    uploadFile(object, img)
      .then((data) => {
        if (data.data.success) {
          setFile(
            (file = process.env.REACT_APP_BASEURL.concat(
              data.data.result[0].media
            ))
          );
          onClickSend(type);
        }
      })
      .catch((err) => {
        // console.log("err", err);
      });
  };

  useEffect(() => {
    document.querySelector(".footer").style.display = "none";
  }, []);

  useEffect(() => {
    return () => (document.querySelector(".footer").style.display = "flex");
  }, []);

  return (
    <>
      <Head title="AIDAPro | Messenger" description="Messenger" />
      <div className="messenger__container">
        <MessangerSidebar
          chatSelected={chatSelected}
          onSearchUser={onSearchUser}
          chatUsers={chatUsers}
          searchedUsers={searchedUsers}
          onClickChatNode={onClickChatNode}
          selectedChat={selectedChat}
          showName={showName}
          chat={chat}
          onClickDecline={onClickDecline}
          setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
          setVideoCallUserData={setVideoCallUserData}
        />
        <MessangerContent
          messages={messages}
          chatNodeName={chatNodeName}
          isOn={isOn}
          navigate={navigate}
          selectedChat={selectedChat}
          showName={showName}
          isSelectedUserOnline={isSelectedUserOnline}
          onClickDecline={onClickDecline}
          onClickApprove={onClickApprove}
          user={user}
          file={file}
          AlwaysScrollToBottom={AlwaysScrollToBottom}
          newMessage={newMessage}
          setNewMessage={setNewMessage}
          CustomError={CustomError}
          onClickSend={onClickSend}
          fileSelectedHandler={fileSelectedHandler}
          noChat={noChat}
          chatUsers={chatUsers}
        />
      </div>
    </>
  );
}
