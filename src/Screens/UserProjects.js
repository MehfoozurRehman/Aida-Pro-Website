import React, { useContext, useState, useEffect } from "react";
import { freelancerProjectPortfoliosList } from "../API/FreelancerApi";
import { jobSekkerRecentJobs } from "../API/EmploymentAPI";
import UserContext from "Context/UserContext";
import { Head, ProjectsCard } from "Components";
import NoData from "Components/NoData";
import Img from "react-cool-img";
import { postProjectSelected } from "Assets";
import { useDispatch } from "react-redux";
import { getFreelancerById, getJobSekkerById } from "Redux/Actions/AppActions";

export default function UserProjects({
  setIsUploadProjectOpen,
  getSelectedProjectData,
}) {
  const isOn = window.localStorage.getItem("isOn");
  const [projects, setProjects] = useState([]);
  const [isFreelancer, setIsFreelancer] = useState(false);
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "freelancer") {
      dispatch(getFreelancerById(user.FreelancerId));
    }
    handleProjectShow();
  }, []);

  const handleProjectShow = () => {
    if (user.Role.Title === "JobSekker") {
      employeeProjectPortfoliosListAPI(user.JobSeekerId);
    } else if (user.Role.Title === "Freelancer") {
      freelancerProjectPortfoliosListAPI(user.FreelancerId);
    }
  };

  const freelancerProjectPortfoliosListAPI = (Id) => {
    freelancerProjectPortfoliosList(Id).then(({ data }) => {
      setIsFreelancer(true);
      setProjects(data.result);
    });
  };

  const employeeProjectPortfoliosListAPI = (Id) => {
    jobSekkerRecentJobs(Id, 1, 100).then(({ data }) => {
      setIsFreelancer(false);
      setProjects(data.result);
    });
  };

  return (
    <>
      <Head
        title="AIDApro | Freelancer Projects"
        description="Freelancer Projects"
      />
      <div className="freelancer__project__container">
        <div className="freelancer__project__container__header">
          <div className="freelancer__project__container__header__heading">
            <Img
              loading="lazy"
              src={postProjectSelected}
              alt="postProjectSelected"
              style={{ marginRight: 10 }}
            />
            My Projects
          </div>
          <button
            className="header__nav__btn btn__secondary"
            onClick={() => {
              setIsUploadProjectOpen(true);
            }}
            style={{ width: 150 }}
            title="upload project"
          >
            Upload
          </button>
        </div>
        <div
          className="homepage__container__results"
          style={{ marginTop: "0em" }}
        >
          {projects.length === 0 ? (
            <NoData text="No Projects" />
          ) : (
            projects.map((project, i) => (
              <ProjectsCard
                isOnFreelancer={isFreelancer}
                key={i}
                data={project}
                getSelectedProjectData={getSelectedProjectData}
              />
            ))
          )}
        </div>
      </div>
    </>
  );
}
