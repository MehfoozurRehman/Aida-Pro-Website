import React, { useContext, useState, useEffect } from "react";
import Header from "Components/Header";
import { useNavigate } from "@reach/router";
import { getForumbyId, answerToQuestionPost } from "API/Question";
import UserContext from "Context/UserContext";
import { useDispatch } from "react-redux";
import moment from "moment";
import { CustomError } from "./Toasts";
import { getAllJobsProjectsCoffeCorner } from "API/CompanyAPI";
import IndustrySvg from "../Assets/IndustrySvg.svg";
import experienceSvg from "../Assets/experienceSvg.svg";
import GetStartedBanner from "Components/GetStartedBanner";
import { getText } from "Utils/functions";
import Img from "react-cool-img";
import { Head, Avatar, ContactTab, MobileAppAdvert } from "Components";
import {
  getCompanyById,
  getFreelancerById,
  getJobSekkerById,
} from "Redux/Actions/AppActions";
import CoffeeCornerDiscussionComments from "Components/CoffeeCornerDiscussionComments";

export default function CoffeeCorner(props) {
  const isOn = window.localStorage.getItem("isOn");
  const navigate = useNavigate();

  let forumIdFromUser = 0;
  if (props.location.state) {
    forumIdFromUser = props.location.state.forumId;
  } else {
    const url = window.location.href;
    forumIdFromUser = url.substring(url.lastIndexOf("/") + 1);
  }

  const [questions, setQuestions] = useState(null);
  const [forumId, setForumId] = useState(forumIdFromUser);
  const [jobs, setJobs] = useState("Jobs");
  const [projects, setProjects] = useState(null);
  const [showJobs, setShowJobs] = useState(true);
  const [showProjects, setShowProjects] = useState(false);
  const [postingTableData, setPostingTableData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);

  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    if (props.askQuestionSuccess) getForumbyId(forumId);
  }, [props.askQuestionSuccess]);

  useEffect(() => {
    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "company") {
      dispatch(getCompanyById(user.CompanyId));
    } else {
      dispatch(getFreelancerById(user.FreelancerId));
    }
    getQuestionsBySearch(forumId);
    // getTagsList();
  }, []);

  useEffect(() => {
    setPostingTableData([]);
    getJobsOrProject();
  }, [showJobs, showProjects]);

  const getQuestionsBySearch = (forumId) => {
    getForumbyId(forumId)
      .then(({ data }) => {
        setQuestions(data.result);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
  };

  const getJobsOrProject = () => {
    getAllJobsProjectsCoffeCorner(null, jobs, projects, 3, pageNumber)
      .then(({ data }) => {
        if (data.success) setPostingTableData(data.result);
        else setPostingTableData([]);
      })
      .catch((err) => {
        // console.log("err", err);
      });
  };

  const addComment = (comment, id, element) => {
    let currentDate = new Date();
    let answerObject = {
      ForumId: id,
      Description: comment,
      AnswerById: user.Id,
      AnswerOn: moment(currentDate).format("YYYY-MM-DD"),
      AllowEdit: 0,
      CreatedById: user.Id,
    };
    answerToQuestionPost(answerObject)
      .then(({ data }) => {
        if (data.success) {
          setQuestions(null);
          element.ForumAnswers.splice(0, 0, data.result);
          setQuestions(element);
        }
      })
      .catch((err) => {
        // console.log("Err", err);
      });
  };

  const getWriteCommentAndIdAndData = (comment, id, element) => {
    if (user.Id) addComment(comment, id, element);
    else CustomError("You need to login first to comment.");
  };
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <>
      <Head title="AIDApro | Coffee Corner" description="Coffee Corner" />
      <Header setIsLoginOpen={props.setIsLoginOpen} />
      <div
        className="homepage__container__jumbotron"
        style={{ minHeight: 120 }}
      ></div>
      <div className="homepage__container">
        <div className="homepage__container__descussions__panel">
          <div className="homepage__container__descussions__panel__side">
            {/* <div
              className="homepage__container__heading"
              style={{ marginBottom: "1em" }}
            >
              Trending Topics
            </div>
            {tags.length > 0 ? (
              <div className="homepage__container__descussions__panel__side__content">
                {tags.map((data, index) => (
                  <TrendingTopicBadge
                    data={data}
                    key={index}
                    setSelectedTagId={setSelectedTagId}
                    tagId={tagId}
                  />
                ))}
              </div>
            ) : null} */}
            <div
              className="homepage__container__heading"
              style={{ marginBottom: "1em" }}
            >
              Trending Jobs & Projects
            </div>
            <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                margin: "1em auto",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              <ContactTab
                defaultChecked={true}
                label="Trending Jobs"
                onClick={(event) => {
                  setShowProjects(false);
                  setShowJobs(true);
                  setProjects(null);
                  setJobs("Jobs");
                }}
              />
              <ContactTab
                label="Trending Projects"
                onClick={() => {
                  setShowJobs(false);
                  setShowProjects(true);
                  setJobs(null);
                  setProjects("Projects");
                }}
              />
            </div>

            <div className="homepage__container__descussions__panel__side__links">
              {postingTableData.length > 0
                ? postingTableData.map((item, index) => (
                    <div
                      key={index}
                      className="homepage__container__result__card__wrapper"
                      style={{
                        marginBottom: ".5em",
                      }}
                      onClick={() => {
                        const requestData = {
                          redirectFrom: item.Type,
                          objectData: item,
                          jobType: item.Type,
                        };
                        navigate("/job-project-details", {
                          state: requestData,
                        });
                      }}
                    >
                      <div className="homepage__container__result__card">
                        <div className="homepage__container__result__card__header">
                          <div className="homepage__container__result__card__header__icon">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="20.4"
                              height="17"
                              viewBox="0 0 20.4 17"
                              fill="currentColor"
                            >
                              <g id="suitcase" transform="translate(0 -2)">
                                <path
                                  id="Path_2267"
                                  data-name="Path 2267"
                                  d="M13.95,5.825a.85.85,0,0,1-.85-.85V3.7H9.7V4.975a.85.85,0,1,1-1.7,0V3.7A1.7,1.7,0,0,1,9.7,2h3.4a1.7,1.7,0,0,1,1.7,1.7V4.975A.85.85,0,0,1,13.95,5.825Z"
                                  transform="translate(-1.2)"
                                />
                                <path
                                  id="Path_2268"
                                  data-name="Path 2268"
                                  d="M10.8,14.816a1.751,1.751,0,0,1-.6.1,1.863,1.863,0,0,1-.655-.119L0,11.62v6.486a2.336,2.336,0,0,0,2.338,2.337H18.063A2.336,2.336,0,0,0,20.4,18.106V11.62Z"
                                  transform="translate(0 -1.443)"
                                />
                                <path
                                  id="Path_2269"
                                  data-name="Path 2269"
                                  d="M20.4,7.338V9.284l-10,3.332a.629.629,0,0,1-.408,0L0,9.284V7.338A2.336,2.336,0,0,1,2.338,5H18.063A2.336,2.336,0,0,1,20.4,7.338Z"
                                  transform="translate(0 -0.45)"
                                />
                              </g>
                            </svg>
                          </div>
                          <div className="homepage__container__result__card__header__content">
                            <div
                              className="homepage__container__result__card__header__designation"
                              style={{ marginBottom: "0.3em" }}
                            />
                            <div className="homepage__container__result__card__header__name">
                              {item.Title ? item.Title : null}
                            </div>
                          </div>
                        </div>
                        <div className="homepage__container__result__card__content__info">
                          {getText(item.Description).length > 150 ? (
                            <>{`${getText(item.Description).substring(
                              0,
                              150
                            )}...`}</>
                          ) : (
                            <>{getText(item.Description)}</>
                          )}
                        </div>
                        <div
                          className="homepage__container__result__card__content"
                          style={{ marginBottom: -20 }}
                        >
                          <div className="homepage__container__result__card__content__entry">
                            <Img
                              loading="lazy"
                              src={IndustrySvg}
                              alt="IndustrySvg"
                              className="homepage__container__result__card__content__entry__svg"
                            />

                            <div className="homepage__container__result__card__content__entry__text">
                              {item.Industry != null
                                ? item.Industry.Title
                                : "Not specified"}
                            </div>
                          </div>
                          <div className="homepage__container__result__card__content__entry">
                            <Img
                              loading="lazy"
                              src={experienceSvg}
                              alt="experienceSvg"
                              className="homepage__container__result__card__content__entry__svg"
                            />
                            <div className="homepage__container__result__card__content__entry__text">
                              {item.CompanyDetail != null &&
                              item.CompanyDetail != undefined ? (
                                getText(item.CompanyDetail.NoOfEmployees)
                                  .length > 8 ? (
                                  <>{`${getText(
                                    item.CompanyDetail.NoOfEmployees
                                  ).substring(0, 8)}... employees`}</>
                                ) : (
                                  <>
                                    {getText(item.CompanyDetail.NoOfEmployees)}{" "}
                                    employees
                                  </>
                                )
                              ) : (
                                "Not specified"
                              )}
                            </div>
                          </div>
                        </div>
                        <div
                          className="homepage__container__result__card__badges"
                          style={{
                            paddingBottom: "0em",
                            overflow: "hidden",
                            marginTop: 15,
                          }}
                        >
                          {item.Type == "Jobs"
                            ? item.JobSkills.map((skill, i) => (
                                <div
                                  className="homepage__container__result__card__badge"
                                  key={i}
                                >
                                  {skill.Title}
                                </div>
                              ))
                            : item.ProjectSkills.map((skill, i) => (
                                <div
                                  className="homepage__container__result__card__badge"
                                  key={i}
                                >
                                  {skill.Title}
                                </div>
                              ))}
                        </div>
                      </div>
                    </div>
                  ))
                : null}
            </div>
          </div>
          <div className="homepage__container__discussions__panel__content">
            {questions != null ? (
              <div className="coffee__corner__new__discussion__card">
                {/* <div className="coffee__corner__new__discussion__card__left">
                  <button
                    className="coffee__corner__new__discussion__card__left__vote__up"
                    title="vote up"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-chevron-up"
                    >
                      <polyline points="18 15 12 9 6 15"></polyline>
                    </svg>
                  </button>
                  <div className="coffee__corner__new__discussion__card__left__no__of__vote">
                    2323
                  </div>
                  <button
                    className="coffee__corner__new__discussion__card__left__vote__down"
                    title="vote down"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-chevron-down"
                    >
                      <polyline points="6 9 12 15 18 9"></polyline>
                    </svg>
                  </button>
                </div> */}

                <div className="coffee__corner__new__discussion__card__right">
                  <div className="coffee__corner__new__discussion__card__right__header">
                    <Avatar
                      onClick={() => {}}
                      userPic={
                        questions.User != null
                          ? questions.User.Freelancer != null
                            ? questions.User.Freelancer.ProfilePicture != ""
                              ? process.env.REACT_APP_BASEURL.concat(
                                  questions.User.Freelancer.ProfilePicture
                                )
                              : null
                            : questions.User.JobSeeker != null
                            ? questions.User.JobSeeker.ProfilePicture != ""
                              ? process.env.REACT_APP_BASEURL.concat(
                                  questions.User.JobSeeker.ProfilePicture
                                )
                              : null
                            : questions.User.CompanyProfile != null
                            ? questions.User.CompanyProfile.LogoUrl != ""
                              ? process.env.REACT_APP_BASEURL.concat(
                                  questions.User.CompanyProfile.LogoUrl
                                )
                              : null
                            : null
                          : null
                      }
                      dontShowHover={true}
                    />
                    <div className="coffee__corner__new__discussion__card__right__header__name">
                      {questions.User != null
                        ? questions.User.LoginName
                        : "Not specified"}
                    </div>
                    <div className="coffee__corner__new__discussion__card__right__header__posted">
                      Posted by{" "}
                      <span>
                        {questions.User != null
                          ? questions.User.LoginName
                          : "Not specified"}
                      </span>
                    </div>
                    <div className="coffee__corner__new__discussion__card__right__header__time">
                      <span>
                        {moment(questions.PostedOn).startOf("day").fromNow()}
                      </span>
                    </div>
                    <button
                      className="coffee__corner__new__discussion__card__right__header__buttons__btn"
                      style={{ marginLeft: "auto" }}
                      onClick={() => {
                        localStorage.setItem("forumId", questions.ForumId);
                        props.setIsShareDiscusssion(true);
                      }}
                      title="share"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="currentColor"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="feather feather-share-2"
                      >
                        <circle cx="18" cy="5" r="3"></circle>
                        <circle cx="6" cy="12" r="3"></circle>
                        <circle cx="18" cy="19" r="3"></circle>
                        <line x1="8.59" y1="13.51" x2="15.42" y2="17.49"></line>
                        <line x1="15.41" y1="6.51" x2="8.59" y2="10.49"></line>
                      </svg>
                      Share
                    </button>
                  </div>
                  <div className="coffee__corner__new__discussion__card__right__heading">
                    {questions.Subject}
                  </div>
                  <div className="coffee__corner__new__discussion__card__right__header__info">
                    {getText(questions.Description)}
                  </div>
                  <div className="homepage__container__result__card__badges">
                    {questions.ForumTags.map((forum, i) => (
                      <div
                        className="coffee__corner__discussions__skills"
                        key={i}
                      >
                        {forum.Tag.Title}
                      </div>
                    ))}
                  </div>
                  <div className="coffee__corner__new__discussion__card__right__header__buttons"></div>
                  <CoffeeCornerDiscussionComments
                    getWriteCommentAndIdAndData={getWriteCommentAndIdAndData}
                    questions={questions}
                    getText={getText}
                  />
                </div>
              </div>
            ) : null}
          </div>
        </div>
        {!user.Id ? <GetStartedBanner /> : null}
        <MobileAppAdvert />
      </div>
    </>
  );
}
