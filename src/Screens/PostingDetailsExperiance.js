import React from "react";
import { Head, PostingDetailsExperianceCard } from "Components";

export default function PostingDetailsExperiance({ data }) {
  return (
    <>
      <Head
        title="AIDApro | Posting Details - Experiance"
        description="Posting Details - Experiance"
      />
      <div className="homepage__container__jobs__projects__penel__container__details__content__work__experiance">
        {data.map((experience, i) => (
          <PostingDetailsExperianceCard key={i} data={experience} />
        ))}
      </div>
    </>
  );
}
