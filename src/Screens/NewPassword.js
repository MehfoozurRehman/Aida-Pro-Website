import React, { useEffect, useState } from "react";
import { CustomError } from "./Toasts";
import { resetPassword, verifyUser } from "../API/Users";
import { setCurrentUser } from "../Redux/Actions/AppActions";
import { useDispatch } from "react-redux";
import { Head, InputBox } from "Components";
import { isNullOrEmpty } from "Utils/TextUtils";
import { crossSvg } from "Assets";
import { isInvalidPassword } from "Utils/Validations";

export default function NewPassword({
  setIsNewPasswordOpen,
  setIsPasswordChangedOpen,
  setIsChangePasswordOpen,
  setIsForgotPasswordOpen,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) {
  const email = localStorage.getItem("emailTobeVerify");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  //#region Validation
  var [newPasswordError, setNewPasswordError] = useState(false);
  const [newPasswordErrorMessage, setNewPasswordErrorMessage] = useState("");
  var [confirmPasswordError, setConfirmPasswordError] = useState(false);
  const [confirmPasswordErrorMessage, setConfirmPasswordErrorMessage] =
    useState("");
  //#endregion
  let dispatch = useDispatch();

  const handleNewPassword = (value) => {
    if (isNullOrEmpty(value))
      setNewPasswordErrorMessageAndText("Please enter new password");
    else if (isInvalidPassword(value))
      setNewPasswordErrorMessageAndText("Min 8 characters.");
    else setNewPasswordErrorMessageAndText("");
    setNewPassword(value);
  };

  const handleConfirmPassword = (value) => {
    if (isNullOrEmpty(value))
      setConfirmPasswordErrorMessageAndText("Please enter confirm password");
    else if (isInvalidPassword(value))
      setConfirmPasswordErrorMessageAndText("Min 8 characters.");
    else if (value != newPassword)
      setConfirmPasswordErrorMessageAndText("Password do not match!");
    else setConfirmPasswordErrorMessageAndText("");
    setConfirmPassword(value);
  };

  const setNewPasswordErrorMessageAndText = (text) => {
    setNewPasswordError((newPasswordError = !isNullOrEmpty(text)));
    setNewPasswordErrorMessage(text);
  };

  const setConfirmPasswordErrorMessageAndText = (text) => {
    setConfirmPasswordError((confirmPasswordError = !isNullOrEmpty(text)));
    setConfirmPasswordErrorMessage(text);
  };

  const isViewValid = () => {
    if (isNullOrEmpty(newPassword))
      setNewPasswordErrorMessageAndText("Please enter new password.");
    else if (isInvalidPassword(newPassword))
      setNewPasswordErrorMessageAndText("Min 8 characters.");
    else if (isNullOrEmpty(confirmPassword))
      setNewPasswordErrorMessageAndText("Please enter confirm password.");
    else if (isInvalidPassword(confirmPassword))
      setNewPasswordErrorMessageAndText("Min 8 characters.");
    else if (newPassword != confirmPassword)
      setConfirmPasswordErrorMessageAndText("Password do not match!");
    else return true;
  };

  const handleSubmit = () => {
    if (isViewValid()) {
      let resetPasswordObject = {
        email: email,
        NewPassword: newPassword,
      };
      setIsLoading(true);

      resetPassword(resetPasswordObject)
        .then(({ data }) => {
          if (data.success) {
            localStorage.setItem("userId", data.result.Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result));
            setIsLoading(false);
            setIsNewPasswordOpen(false);
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            CustomError("Verification code is wrong, Please try again.");
          }
          setIsLoading(false);
        })
        .catch((err) => {
          console.log("err", err);
          setIsLoading(true);
        });
    }
  };

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <>
      <Head
        title="AIDApro | Email Verification"
        description="Email Verification"
      />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsNewPasswordOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px", textAlign: "center" }}
              >
                Choose New Password
              </div>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "16px",
                  textAlign: "center",
                  marginTop: "0em",
                  marginBottom: "2em",
                }}
              >
                Create a new password that is at least 8 characters long.
                <br />
                {/* <a href="#" onClick={() => {}} style={{ color: "#0ca69d" }}>
                  What makes a strong password?
                </a> */}
              </div>
              <InputBox
                placeholder="New Password"
                type="password"
                name="code"
                error={newPasswordError}
                errorMessage={newPasswordErrorMessage}
                value={newPassword}
                secureKey={true}
                onChange={(e) => handleNewPassword(e.currentTarget.value)}
              // onKeyDown={(event) => {
              //   if (event.key === "Enter") verifyUserValidation();
              // }}
              />
              <InputBox
                placeholder="Retype New Password"
                type="password"
                name="code"
                error={confirmPasswordError}
                errorMessage={confirmPasswordErrorMessage}
                value={confirmPassword}
                secureKey={true}
                onChange={(e) => handleConfirmPassword(e.currentTarget.value)}
                onKeyDown={(event) => {
                  if (event.key === "Enter") handleSubmit();
                }}
              />
              {/* <div className="pupup__container__from__top__left__row">
                <input
                  className="styled-checkbox"
                  id="styled-checkbox-remember-me"
                  type="checkbox"
                  value="Remember"
                  name="Remember"
                />
                <label htmlFor="styled-checkbox-remember-me">
                  Require all devices to sign in with new password
                </label>
              </div> */}
              <button
                // type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  margin: "0em auto",
                  marginTop: "2em",
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  handleSubmit();
                }}
                disabled={isLoading ? true : false}
                title="verify"
              >
                {isLoading ? "Loading..." : "Submit"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
