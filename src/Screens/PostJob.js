import React, { useContext, useState, useEffect } from "react";
import UserContext from "Context/UserContext";
import { jobPost, jobUpdate, saveJobDraftPost } from "../API/Job";
import { CustomError, CustomSuccess } from "./Toasts";
import { Head, InputBox, DashboardHeading } from "Components";
import { getAllSkills, getLookUpByPrefix } from "../API/Api";
import moment from "moment";
import { isNullOrEmpty, isNullOrEmptyArray } from "Utils/TextUtils";
import { postJobSvg } from "Assets";

const PostJob = ({ setIsJobPreviewOpen, previewData, location }) => {
  const isUpdateFromObject = location.state.jobData;
  let jobSkillsLocal = [];
  let jobTypeLocal = [];
  let jobEducationLocal = [];
  let jobSalaryTypeLocal = [];
  let jobStatusLookupDetailIdLocal = null;
  let isShowCompanyDetail = false;

  if (isUpdateFromObject) {
    let jobSkillList = [];
    isUpdateFromObject.JobSkills.map((jobSk, jobSkIndex) =>
      jobSkillList.push({
        label: jobSk.Title,
        value: jobSk.SkillId,
        Id: jobSk.Id,
      })
    );

    jobSkillsLocal = jobSkillList;

    // if (isUpdateFromObject.JobTypeLookupDetail) {
    //   jobTypeLocal = {
    //     label: isUpdateFromObject.JobTypeLookupDetail.Title,
    //     value: isUpdateFromObject.JobTypeLookupDetail.Id,
    //   };
    // }

    let jobTypeData = [];
    isUpdateFromObject.JobTypes.map((jobType, jobSkIndex) => {
      jobTypeData.push({
        label: jobType.JobTypeDetailTitle,
        value: jobType.JobTypeLookupDetailId,
        Id: jobType.Id,
      });
    });
    jobTypeLocal = jobTypeData;

    let educationList = [];
    isUpdateFromObject.JobDegrees.map((jobEd, jobSkIndex) => {
      educationList.push({
        label: jobEd.DegreeLookupDetailTitle,
        value: jobEd.DegreeLookupDetailId,
        Id: jobEd.Id,
      });
    });
    jobEducationLocal = educationList;

    if (isUpdateFromObject.IsShowCompanyDetail) {
      isShowCompanyDetail = isUpdateFromObject.IsShowCompanyDetail;
    }

    if (isUpdateFromObject.SalaryTypeLookup) {
      jobSalaryTypeLocal = {
        label: isUpdateFromObject.SalaryTypeLookup.Title,
        value: isUpdateFromObject.SalaryTypeLookup.Id,
      };
    }

    if (isUpdateFromObject.StatusTypeLookupDetail) {
      jobStatusLookupDetailIdLocal = {
        label: isUpdateFromObject.StatusTypeLookupDetail.Title,
        value: isUpdateFromObject.StatusTypeLookupDetail.Id,
      };
    }
  }

  const [isUpdate, setIsUpdate] = useState(isUpdateFromObject ? true : false);
  const [jobId, setJobId] = useState(
    isUpdateFromObject ? isUpdateFromObject.Id : 0
  );
  const [jobTitle, setJobTitle] = useState(
    isUpdateFromObject ? isUpdateFromObject.Title : ""
  );

  const [jobLocation, setJobLocation] = useState(
    isUpdateFromObject ? isUpdateFromObject.Location : ""
  );
  const [jobLat, setJobLat] = useState(
    isUpdateFromObject ? isUpdateFromObject.Latitude : null
  );
  const [jobLng, setJobLng] = useState(
    isUpdateFromObject ? isUpdateFromObject.Longitude : null
  );
  const [postingDate, setPostingDate] = useState(
    isUpdateFromObject
      ? moment(isUpdateFromObject.StartDate).format("YYYY-MM-DD")
      : ""
  );
  const [salaryType, setSalaryType] = useState(
    jobSalaryTypeLocal.length === 0 ? "" : jobSalaryTypeLocal.label
  );
  const [salaryMin, setSalaryMin] = useState(
    isUpdateFromObject ? isUpdateFromObject.SalaryFrom : ""
  );
  const [salaryMax, setSalaryMax] = useState(
    isUpdateFromObject ? isUpdateFromObject.SalaryTo : ""
  );
  const user = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [salaryTypeDropDownData, setSalaryTypeDropDownData] = useState([]);
  const [skillsDropDownData, setSkillsDropDownData] = useState([]);
  const [jobTypeDropDownData, setJobTypeDropDownData] = useState([]);
  const [educationDropDownData, setEducationDropDownData] = useState([]);
  const [jobEducation, setJobEducation] = useState(jobEducationLocal);
  const [jobSkills, setJobSkills] = useState(jobSkillsLocal);
  const [jobType, setJobType] = useState(jobTypeLocal);
  const [jobSalaryType, setJobSalaryType] = useState(jobSalaryTypeLocal);
  const [jobDescription, setJobDescription] = useState(
    isUpdateFromObject ? isUpdateFromObject.Description : ""
  );
  const [jobRequirements, setJobRequirements] = useState(
    isUpdateFromObject ? isUpdateFromObject.Requirements : ""
  );
  const [isCompanyDetailShow, setIsCompanyDetailShow] =
    useState(isShowCompanyDetail);

  //validations
  var [jobTitleError, setJobTitleError] = useState(false);
  const [jobTitleErrorMessage, setJobTitleErrorMessage] = useState("");
  var [dateError, setDateError] = useState(false);
  const [dateErrorMessage, setDateErrorMessage] = useState("");
  var [locationError, setLocationError] = useState(false);
  const [locationErrorMessage, setLocationErrorMessage] = useState("");
  var [educationError, setEducationError] = useState(false);
  const [educationErrorMessage, setEducationErrorMessage] = useState("");
  var [skillsError, setSkillsError] = useState(false);
  const [skillsErrorMessage, setSkillsErrorMessage] = useState("");
  var [salaryTypeError, setSalaryTypeError] = useState(false);
  const [salaryTypeErrorMessage, setSalaryTypeErrorMessage] = useState("");
  var [jobTypeError, setJobTypeError] = useState(false);
  const [jobTypeErrorMessage, setJobTypeErrorMessage] = useState("");
  var [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");
  var [requirementError, setRequirementError] = useState(false);
  const [requirementErrorMessage, setRequirementErrorMessage] = useState("");
  var [minSalaryError, setMinSalaryError] = useState(false);
  const [minSalaryErrorMessage, setMinSalaryErrorMessage] = useState("");
  var [maxSalaryError, setMaxSalaryError] = useState(false);
  const [maxSalaryErrorMessage, setMaxSalaryErrorMessage] = useState("");
  var [fixSalaryError, setFixSalaryError] = useState(false);
  const [fixSalaryErrorMessage, setFixSalaryErrorMessage] = useState("");

  const handleChangeValues = (event) => {
    if (event.currentTarget.name === "title") {
      if (isNullOrEmpty(event.currentTarget.value))
        setJobTitleErrorMessageAndVisibility("Please enter job title");
      else setJobTitleErrorMessageAndVisibility("");
      setJobTitle(event.currentTarget.value);
    } else if (event.currentTarget.name === "minSalary") {
      if (salaryType === "Range") {
        if (isNullOrEmpty(event.currentTarget.value))
          setMinSalaryErrorMessageAndVisibility("Enter range");
        else setMinSalaryErrorMessageAndVisibility("");
      } else {
        if (isNullOrEmpty(event.currentTarget.value))
          setFixSalaryErrorMessageAndVisibility("Please enter salary");
        else setFixSalaryErrorMessageAndVisibility("");
      }

      setSalaryMin(event.currentTarget.value);
    } else if (event.currentTarget.name === "maxSalary") {
      setSalaryMax(event.currentTarget.value);
      if (isNullOrEmpty(event.currentTarget.value))
        setMaxSalaryErrorMessageAndVisibility("Enter range");
      else if (parseInt(salaryMin) > parseInt(event.currentTarget.value))
        setMaxSalaryErrorMessageAndVisibility("Min can't > max");
      else setMaxSalaryErrorMessageAndVisibility("");
    } else if (event.currentTarget.name === "isCompanyDetailShow")
      setIsCompanyDetailShow(event.currentTarget.checked);
  };

  const handleChangeDateValues = (event) => {
    if (event.currentTarget.value === "")
      setPostingDateErrorMessageAndVisibility("Please select date");
    else setPostingDateErrorMessageAndVisibility("");
    setPostingDate(event.currentTarget.value);
  };

  const handleChangelocation = (data, isInput) => {
    if (isNullOrEmpty(data.geometry))
      setLocationErrorMessageAndVisibility("Please enter your location");
    else {
      setLocationErrorMessageAndVisibility("");
      if (isInput) {
        setJobLocation(data.currentTarget.value);
      } else {
        setJobLocation(data.formatted_address);
        setJobLat(data.geometry.location.lat());
        setJobLng(data.geometry.location.lng());
      }
    }
    setJobLocation(data.formatted_address);
  };

  const handleChangeDescriptionValues = (data) => {
    if (isNullOrEmptyArray(data))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else setDescriptionErrorMessageAndVisibility("");
    setJobDescription(data);
  };

  const handleChangeRequirementsValues = (data) => {
    if (isNullOrEmptyArray(data))
      setRequirementErrorMessageAndVisibility("Please enter requirements");
    else setRequirementErrorMessageAndVisibility("");
    setJobRequirements(data);
  };

  useEffect(() => {
    getLookUpByPrefix("EDUTY")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setEducationDropDownData(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
    getLookUpByPrefix("SALTYP")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setSalaryTypeDropDownData(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
    getLookUpByPrefix("JOBT")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setJobTypeDropDownData(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getAllSkills()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setSkillsDropDownData(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    setIsLoading(false);
  }, []);

  const saveDraftClicked = () => {
    if (viewIsValid()) {
      setIsLoading(true);
      saveDraftJob(getRequestData());
    }
  };
  const save = () => {
    if (viewIsValid()) {
      setIsLoading(true);
      if (jobId === 0) {
        saveJob(getRequestData());
      } else {
        updateJob(getRequestData());
      }
    }
  };
  const getRequestData = () => {
    let projectSkillTempArr = [];
    jobSkills.map((e) => {
      projectSkillTempArr.push({ SkillId: e.value });
    });
    let degreeLookUp = [];
    jobEducation.map((e) => {
      // degreeLookUp.push({ DegreeLookupDetailId: e.value, Id: e.Id != undefined ? e.Id : 0 });
      degreeLookUp.push({ DegreeLookupDetailId: e.value });
    });
    let jobtypeLookUp = [];
    jobType.map((e) => {
      // jobtypeLookUp.push({ JobTypeLookupDetailId: e.value, Id: e.Id != undefined ? e.Id : 0 });
      jobtypeLookUp.push({ JobTypeLookupDetailId: e.value });
    });
    let data = {
      Id: jobId,
      JobTitle: jobTitle,
      Location: jobLocation,
      Latitude: jobLat,
      Longitude: jobLng,
      JobSkills: projectSkillTempArr,
      // JobTypeLookupDetailId: jobType.value,
      // DegreeLookupDetailId:
      //   typeof jobEducation === "object"
      //     ? jobEducation.value
      //     : jobEducation[0].value,
      // DegreeLookupDetailId: jobEducation.value,
      JobDegrees: degreeLookUp,
      JobTypes: jobtypeLookUp,
      SalaryTypeLookupDetailId: jobSalaryType.value,
      Description: jobDescription,
      Requirements: jobRequirements,
      IsShowCompanyDetail: isCompanyDetailShow ? 1 : 0,
      SalaryFrom: salaryMin,
      SalaryTo: salaryMax,
      CompanyProfileId: user.CompanyId,
      PostingDate: postingDate,
      StatusTypeLookupDetailId: jobStatusLookupDetailIdLocal
        ? jobStatusLookupDetailIdLocal.value
        : jobStatusLookupDetailIdLocal,
    };
    return data;
  };
  const viewIsValid = () => {
    if (isNullOrEmpty(jobTitle))
      setJobTitleErrorMessageAndVisibility("Please enter job title");
    else if (isNullOrEmpty(postingDate))
      setPostingDateErrorMessageAndVisibility("Please select date");
    else if (isNullOrEmpty(jobLat) && isNullOrEmpty(jobLng))
      setLocationErrorMessageAndVisibility("Please enter your location");
    else if (isNullOrEmptyArray(jobEducation))
      setEducationErrorMessageAndVisibility("Please select your education");
    else if (isNullOrEmptyArray(jobSkills))
      setSkillsErrorMessageAndVisibility("Please select at least on skill");
    else if (isNullOrEmptyArray(jobType))
      setJobTypeErrorMessageAndVisibility("Please select job type");
    else if (isNullOrEmptyArray(salaryType))
      setSalaryTypeErrorMessageAndVisibility("Please select salary type");
    else if (salaryType === "Fixed" && isNullOrEmpty(salaryMin)) {
      setFixSalaryErrorMessageAndVisibility("Please enter salary");
    } else if (
      salaryType === "Range" &&
      (isNullOrEmpty(salaryMin) || isNullOrEmpty(salaryMax))
    ) {
      if (isNullOrEmpty(salaryMin))
        setMinSalaryErrorMessageAndVisibility("Enter range");
      else if (isNullOrEmpty(salaryMax))
        setMaxSalaryErrorMessageAndVisibility("Enter range");
      else if (parseInt(salaryMin) > parseInt(salaryMax))
        setMaxSalaryErrorMessageAndVisibility("Min can't > max");
    } else if (isNullOrEmpty(jobDescription))
      setDescriptionErrorMessageAndVisibility("Please enter description");
    else if (isNullOrEmpty(jobRequirements))
      setRequirementErrorMessageAndVisibility("Please enter requirements");
    else return true;
    return false;
  };
  const setMinSalaryErrorMessageAndVisibility = (text) => {
    setMinSalaryError((minSalaryError = !isNullOrEmpty(text)));
    setMinSalaryErrorMessage(text);
  };
  const setMaxSalaryErrorMessageAndVisibility = (text) => {
    setMaxSalaryError((maxSalaryError = !isNullOrEmpty(text)));
    setMaxSalaryErrorMessage(text);
  };
  const setFixSalaryErrorMessageAndVisibility = (text) => {
    setFixSalaryError((fixSalaryError = !isNullOrEmpty(text)));
    setFixSalaryErrorMessage(text);
  };
  const setJobTitleErrorMessageAndVisibility = (text) => {
    setJobTitleError((jobTitleError = !isNullOrEmpty(text)));
    setJobTitleErrorMessage(text);
  };
  const setPostingDateErrorMessageAndVisibility = (text) => {
    setDateError((dateError = !isNullOrEmpty(text)));
    setDateErrorMessage(text);
  };
  const setLocationErrorMessageAndVisibility = (text) => {
    setLocationError((locationError = !isNullOrEmpty(text)));
    setLocationErrorMessage(text);
  };
  const setDescriptionErrorMessageAndVisibility = (text) => {
    setDescriptionError((locationError = !isNullOrEmpty(text)));
    setDescriptionErrorMessage(text);
  };
  const setRequirementErrorMessageAndVisibility = (text) => {
    setRequirementError((locationError = !isNullOrEmpty(text)));
    setRequirementErrorMessage(text);
  };
  const saveJob = (data) => {
    jobPost({ data })
      .then(({ data }) => {
        //CustomSuccess("Job Post Successfully");
        setIsLoading(false);
        window.location.href = "/home-company/posting";
        // setTimeout(() => {
        // }, 1000);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Failed to Post a Job");
        CustomError(err);
      });
  };
  const saveDraftJob = (data) => {
    saveJobDraftPost({ data })
      .then(({ data }) => {
        //CustomSuccess("Job saved as draft successfully");
        setIsLoading(false);
        setTimeout(() => {
          window.location.href = "/home-company/posting";
        }, 1000);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Job failed to save as draft");
        CustomError(err);
      });
  };

  const updateJob = (data) => {
    jobUpdate({ data })
      .then(({ data }) => {
        //CustomSuccess("Job Update Successfully");
        setIsLoading(false);
        setTimeout(() => {
          window.location.href = "/home-company/posting";
        }, 1000);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Failed to Update a Job");
        CustomError(err);
      });
  };

  const handleEducation = (data) => {
    setJobEducation(data);
    if (isNullOrEmptyArray(data)) {
      setEducationErrorMessageAndVisibility("Please select your education");
      setJobEducation([]);
    } else {
      setEducationErrorMessageAndVisibility("");
      setJobEducation(data);
    }
  };
  const setEducationErrorMessageAndVisibility = (text) => {
    setEducationError((educationError = !isNullOrEmpty(text)));
    setEducationErrorMessage(text);
  };
  const handleSkills = (data) => {
    if (isNullOrEmptyArray(data))
      setSkillsErrorMessageAndVisibility("Please select at least one skills");
    else setSkillsErrorMessageAndVisibility("");
    setJobSkills(data);
  };
  const setSkillsErrorMessageAndVisibility = (text) => {
    setSkillsError((skillsError = !isNullOrEmpty(text)));
    setSkillsErrorMessage(text);
  };
  const handleJobType = (data) => {
    if (isNullOrEmptyArray(data)) {
      setJobTypeErrorMessageAndVisibility("Please select job type");
      setJobType([]);
    } else {
      setJobTypeErrorMessageAndVisibility("");
      setJobType(data);
    }
  };
  const setJobTypeErrorMessageAndVisibility = (text) => {
    setJobTypeError((jobTypeError = !isNullOrEmpty(text)));
    setJobTypeErrorMessage(text);
  };
  const handleSalaryType = (data) => {
    if (isNullOrEmptyArray(data)) {
      setSalaryTypeErrorMessageAndVisibility("Please select salary type");
      setSalaryType("");
    } else {
      setSalaryTypeErrorMessageAndVisibility("");
      setSalaryType(data.label);
    }
    setJobSalaryType(data);
  };
  const setSalaryTypeErrorMessageAndVisibility = (text) => {
    setSalaryTypeError((salaryTypeError = !isNullOrEmpty(text)));
    setSalaryTypeErrorMessage(text);
  };

  const showPreviewData = () => {
    let previewDTO = {
      type: "job",
      title: jobTitle,
      startDate: postingDate,
      endDate: null,
      location: jobLocation,
      education: jobEducation,
      skill: jobSkills,
      jobType,
      salaryType,
      salaryMin,
      salaryMax,
      description: jobDescription,
      requirements: jobRequirements,
    };
    previewData(previewDTO);

    setIsJobPreviewOpen(true);
  };

  // const getDescription = (data) => { };

  return (
    <>
      <Head title="AIDApro | Post a job" description="Post a job" />
      <div className="post__job__container">
        <DashboardHeading heading="Post a job" svg={postJobSvg} />
        <div className="post__job__container__input__container">
          <div className="post__job__container__input__container__wrapper__top">
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="simple"
                placeholder="Job title"
                name="title"
                error={jobTitleError}
                errorMessage={jobTitleErrorMessage}
                onChange={(e) => handleChangeValues(e)}
                value={jobTitle}
                svg={
                  <svg
                    id="keyboard"
                    xmlns="http://www.w3.org/2000/svg"
                    width="15.431"
                    height="9.428"
                    viewBox="0 0 15.431 9.428"
                  >
                    <g id="Group_161" data-name="Group 161">
                      <path
                        id="Path_276"
                        data-name="Path 276"
                        d="M26.431,114.888v-7.747a.869.869,0,0,0-.891-.841H11.891a.869.869,0,0,0-.891.841v7.747a.869.869,0,0,0,.891.841H25.539A.867.867,0,0,0,26.431,114.888Zm-14.659,0v-7.747c0-.028.047-.069.12-.069H25.539c.076,0,.12.044.12.069v7.747c0,.028-.047.069-.12.069H11.891C11.819,114.957,11.772,114.916,11.772,114.888Z"
                        transform="translate(-11 -106.3)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_275"
                        data-name="Ellipse 275"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(2.299 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_276"
                        data-name="Ellipse 276"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(4.273 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_277"
                        data-name="Ellipse 277"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(6.245 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_278"
                        data-name="Ellipse 278"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(8.219 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_279"
                        data-name="Ellipse 279"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(10.194 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_280"
                        data-name="Ellipse 280"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(12.168 2.189)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_281"
                        data-name="Ellipse 281"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(3.287 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_282"
                        data-name="Ellipse 282"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(5.259 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_283"
                        data-name="Ellipse 283"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(7.233 4.232)"
                        fill="#374957"
                      />
                      <ellipse
                        id="Ellipse_284"
                        data-name="Ellipse 284"
                        cx="0.482"
                        cy="0.482"
                        rx="0.482"
                        ry="0.482"
                        transform="translate(9.205 4.232)"
                        fill="#374957"
                      />
                      <path
                        id="Path_277"
                        data-name="Path 277"
                        d="M366.482,240.7h0a.482.482,0,0,0,0,.964h0a.482.482,0,1,0,0-.964Z"
                        transform="translate(-354.82 -236.468)"
                        fill="#374957"
                      />
                      <path
                        id="Path_278"
                        data-name="Path 278"
                        d="M141.671,308.7h-6.884a.387.387,0,1,0,0,.775h6.884a.387.387,0,1,0,0-.775Z"
                        transform="translate(-130.514 -302.326)"
                        fill="#374957"
                      />
                    </g>
                  </svg>
                }
              />
              <InputBox
                variant="date"
                placeholder="Deadline"
                style={{ minWidth: "32% " }}
                name="date"
                error={dateError}
                errorMessage={dateErrorMessage}
                value={postingDate}
                minDate={moment(new Date()).format("YYYY-MM-DD")}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="12.019"
                    height="12.011"
                    viewBox="0 0 12.019 12.011"
                  >
                    <g id="deadline" transform="translate(0.1 -0.076)">
                      <g
                        id="Group_1396"
                        data-name="Group 1396"
                        transform="translate(7.626 6.295)"
                      >
                        <g id="Group_1395" data-name="Group 1395">
                          <path
                            id="Path_21141"
                            data-name="Path 21141"
                            d="M331.053,266.95l-.006-1.7-.691,0,.007,1.987,1.753,1.724.485-.493Z"
                            transform="translate(-330.356 -265.251)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_1398"
                        data-name="Group 1398"
                        transform="translate(0 0.176)"
                      >
                        <g
                          id="Group_1397"
                          data-name="Group 1397"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_21142"
                            data-name="Path 21142"
                            d="M11.8,7.718v-6.5H10.368V.176H9.677V1.213H8.986V.176H8.295V1.213H7.6V.176H6.912V1.213H6.221V.176H5.53V1.213H4.839V.176H4.147V1.213H3.456V.176H2.765V1.213H2.074V.176H1.382V1.213H0v9.78H5.39A3.848,3.848,0,0,0,11.819,8.14,3.92,3.92,0,0,0,11.8,7.718ZM.691,10.3h0V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9H5.53v.806h.691V1.9h.691v.806H7.6V1.9h.691v.806h.691V1.9h.691v.806h.691V1.9h.737v4a3.894,3.894,0,0,0-.413-.487A3.85,3.85,0,0,0,5.753,5H2.079v.691H5.007a3.834,3.834,0,0,0-.458.691H2.079v.691h2.2a3.839,3.839,0,0,0-.132.689H2.079v.691H4.137A3.81,3.81,0,0,0,4.788,10.3H.691Zm9.512.07a3.132,3.132,0,0,1-1.886.906v-.629H7.626v.629a3.157,3.157,0,0,1-2.8-2.828h.561V7.758H4.838A3.158,3.158,0,0,1,7.626,5v.557h.691V5a3.158,3.158,0,0,1,2.788,2.756h-.542v.691h.55A3.133,3.133,0,0,1,10.2,10.372Z"
                            transform="translate(0 -0.176)"
                            fill="#374957"
                            stroke="#f6f6f6"
                            strokeWidth="0.2"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                }
                onChange={(e) => handleChangeDateValues(e)}
              />
              <InputBox
                variant="location"
                style={{ minWidth: "32% " }}
                placeholder="Location"
                error={locationError}
                errorMessage={locationErrorMessage}
                value={jobLocation}
                svg={
                  <svg
                    id="pin"
                    xmlns="http://www.w3.org/2000/svg"
                    width="7.983"
                    height="11.61"
                    viewBox="0 0 7.983 11.61"
                  >
                    <path
                      id="Path_280"
                      data-name="Path 280"
                      d="M87.4,1.988A3.94,3.94,0,0,0,84.049,0c-.059,0-.119,0-.179,0a3.94,3.94,0,0,0-3.348,1.987,4.042,4.042,0,0,0-.053,3.994l2.879,5.269,0,.007a.7.7,0,0,0,1.214,0l0-.007,2.879-5.269A4.042,4.042,0,0,0,87.4,1.988ZM83.959,5.26a1.633,1.633,0,1,1,1.633-1.633A1.634,1.634,0,0,1,83.959,5.26Z"
                      transform="translate(-79.968 0)"
                      fill="#374957"
                    />
                  </svg>
                }
                onChange={(e) => handleChangelocation(e, true)}
                onSelected={(e) => handleChangelocation(e, false)}
              />
            </div>
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="select"
                placeholder="Education"
                options={educationDropDownData}
                error={educationError}
                errorMessage={educationErrorMessage}
                isMulti={true}
                style={{ minWidth: "32%", height: "fit-content" }}
                value={jobEducation}
                onChange={(e) => handleEducation(e)}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="14.818"
                    height="14.818"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#374957"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-database"
                  >
                    <ellipse cx="12" cy="5" rx="9" ry="3"></ellipse>
                    <path d="M21 12c0 1.66-4 3-9 3s-9-1.34-9-3"></path>
                    <path d="M3 5v14c0 1.66 4 3 9 3s9-1.34 9-3V5"></path>
                  </svg>
                }
              />
              <InputBox
                variant="select"
                placeholder="Skills required"
                options={skillsDropDownData}
                error={skillsError}
                errorMessage={skillsErrorMessage}
                isMulti={true}
                style={{ minWidth: "32%", height: "fit-content" }}
                onChange={(e) => handleSkills(e)}
                value={jobSkills}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.818"
                    height="11.818"
                    viewBox="0 0 11.818 11.818"
                  >
                    <g id="pencil" transform="translate(-0.001)">
                      <g
                        id="Group_334"
                        data-name="Group 334"
                        transform="translate(0.001 0)"
                      >
                        <path
                          id="Path_381"
                          data-name="Path 381"
                          d="M11.113,7.737a2.4,2.4,0,0,0-2.395-.594L7.874,6.3,11.56,2.613a.883.883,0,0,0,0-1.248L10.454.259a.883.883,0,0,0-1.248,0L5.52,3.945,4.676,3.1A2.391,2.391,0,0,0,1.427.209a.231.231,0,0,0-.07.374l1.1,1.1-.779.778-1.1-1.1a.231.231,0,0,0-.374.07A2.391,2.391,0,0,0,3.1,4.681l.844.844L1.117,8.348a.233.233,0,0,0-.056.091L.055,11.472a.231.231,0,0,0,.292.292l3.03-1a.278.278,0,0,0,.094-.057L6.295,7.879l.844.844a2.391,2.391,0,0,0,3.249,2.892.231.231,0,0,0,.07-.374l-1.1-1.1.779-.779,1.1,1.1a.231.231,0,0,0,.374-.07,2.391,2.391,0,0,0-.5-2.654ZM9.532.585a.421.421,0,0,1,.6,0l1.106,1.106a.421.421,0,0,1,0,.6l-.749.749-1.7-1.7ZM3.321,4.253A.231.231,0,0,0,3.076,4.2,1.93,1.93,0,0,1,.514,1.95l1,1a.231.231,0,0,0,.326,0l1.1-1.1a.231.231,0,0,0,0-.326l-1-1A1.93,1.93,0,0,1,4.2,3.081a.231.231,0,0,0,.053.245l.945.945L4.266,5.2ZM.638,11.181l.334-1.007.674.674Zm1.5-.5-1-1,.247-.744,1.5,1.5Zm1.172-.472L2.62,9.525,4.88,7.266a.231.231,0,0,0-.326-.326L2.294,9.2l-.688-.688L8.457,1.66l.019.019.669.669L6.029,5.464a.231.231,0,1,0,.326.326L9.471,2.674l.688.688ZM11.3,9.873l-1-1a.231.231,0,0,0-.326,0l-1.1,1.1a.231.231,0,0,0,0,.326l1,1a1.93,1.93,0,0,1-2.25-2.562A.231.231,0,0,0,7.566,8.5l-.945-.945.927-.927.945.945a.231.231,0,0,0,.245.053A1.93,1.93,0,0,1,11.3,9.873Z"
                          transform="translate(-0.001 0)"
                          fill="#374957"
                        />
                        <g
                          id="Group_333"
                          data-name="Group 333"
                          transform="translate(5.226 6.132)"
                        >
                          <path
                            id="Path_382"
                            data-name="Path 382"
                            d="M226.63,266.112a.231.231,0,1,1,.226-.186A.233.233,0,0,1,226.63,266.112Z"
                            transform="translate(-226.399 -265.651)"
                            fill="#374957"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                }
              />

              <InputBox
                variant="select"
                placeholder="Job type"
                options={jobTypeDropDownData}
                isMulti={true}
                error={jobTypeError}
                errorMessage={jobTypeErrorMessage}
                onChange={(e) => handleJobType(e)}
                style={{ minWidth: "32%", height: "fit-content" }}
                value={jobType}
                svg={
                  <svg
                    id="checklist"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.169"
                    height="13.488"
                    viewBox="0 0 11.169 13.488"
                  >
                    <g
                      id="Group_304"
                      data-name="Group 304"
                      transform="translate(5.321 4.004)"
                    >
                      <g
                        id="Group_303"
                        data-name="Group 303"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_363"
                          data-name="Path 363"
                          d="M250.058,152h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -152)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_306"
                      data-name="Group 306"
                      transform="translate(8.007 5.269)"
                    >
                      <g
                        id="Group_305"
                        data-name="Group 305"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_364"
                          data-name="Path 364"
                          d="M348.391,200.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,200.078Z"
                          transform="translate(-347.941 -200.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_308"
                      data-name="Group 308"
                      transform="translate(5.321 1.133)"
                    >
                      <g id="Group_307" data-name="Group 307">
                        <path
                          id="Path_365"
                          data-name="Path 365"
                          d="M246.441,43.078a.263.263,0,1,0,.077.186A.265.265,0,0,0,246.441,43.078Z"
                          transform="translate(-245.991 -43.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_310"
                      data-name="Group 310"
                      transform="translate(5.321 5.269)"
                    >
                      <g
                        id="Group_309"
                        data-name="Group 309"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_366"
                          data-name="Path 366"
                          d="M247.842,200h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -200)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_312"
                      data-name="Group 312"
                      transform="translate(5.321 6.849)"
                    >
                      <g
                        id="Group_311"
                        data-name="Group 311"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_367"
                          data-name="Path 367"
                          d="M250.058,260h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_314"
                      data-name="Group 314"
                      transform="translate(8.007 8.114)"
                    >
                      <g
                        id="Group_313"
                        data-name="Group 313"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_368"
                          data-name="Path 368"
                          d="M348.391,308.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,308.078Z"
                          transform="translate(-347.941 -308.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_316"
                      data-name="Group 316"
                      transform="translate(5.321 8.114)"
                    >
                      <g
                        id="Group_315"
                        data-name="Group 315"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_369"
                          data-name="Path 369"
                          d="M247.842,308h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -308)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_318"
                      data-name="Group 318"
                      transform="translate(5.321 9.694)"
                    >
                      <g
                        id="Group_317"
                        data-name="Group 317"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_370"
                          data-name="Path 370"
                          d="M250.058,368h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_320"
                      data-name="Group 320"
                      transform="translate(8.007 10.959)"
                    >
                      <g
                        id="Group_319"
                        data-name="Group 319"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_371"
                          data-name="Path 371"
                          d="M348.391,416.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,416.078Z"
                          transform="translate(-347.941 -416.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_322"
                      data-name="Group 322"
                      transform="translate(5.321 10.959)"
                    >
                      <g
                        id="Group_321"
                        data-name="Group 321"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_372"
                          data-name="Path 372"
                          d="M247.842,416h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -416)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_324"
                      data-name="Group 324"
                      transform="translate(0)"
                    >
                      <g id="Group_323" data-name="Group 323">
                        <path
                          id="Path_373"
                          data-name="Path 373"
                          d="M53.886,1.027H51.874A1.614,1.614,0,0,0,50.757.58h-.231a1.054,1.054,0,0,0-1.882,0h-.231a1.614,1.614,0,0,0-1.117.448H45.285A1.286,1.286,0,0,0,44,2.312V12.2a1.286,1.286,0,0,0,1.284,1.284h8.6A1.286,1.286,0,0,0,55.17,12.2V2.312A1.286,1.286,0,0,0,53.886,1.027Zm-5.473.079h.411a.263.263,0,0,0,.254-.193.527.527,0,0,1,1.015,0,.263.263,0,0,0,.254.193h.41a1.1,1.1,0,0,1,1.089,1H47.324A1.1,1.1,0,0,1,48.413,1.106Zm6.23,11.1a.758.758,0,0,1-.758.758h-8.6a.758.758,0,0,1-.758-.758V2.312a.758.758,0,0,1,.758-.758h1.642a1.61,1.61,0,0,0-.134.645v.171a.263.263,0,0,0,.263.263h5.057a.263.263,0,0,0,.263-.263V2.2a1.61,1.61,0,0,0-.134-.645h1.643a.758.758,0,0,1,.758.758Z"
                          transform="translate(-44.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_326"
                      data-name="Group 326"
                      transform="translate(1.614 3.864)"
                    >
                      <g
                        id="Group_325"
                        data-name="Group 325"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_374"
                          data-name="Path 374"
                          d="M107.557,146.744a.263.263,0,0,0-.373,0l-1.126,1.127-.347-.347a.263.263,0,0,0-.373.373l.533.533a.263.263,0,0,0,.373,0l1.313-1.313A.263.263,0,0,0,107.557,146.744Z"
                          transform="translate(-105.261 -146.667)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_328"
                      data-name="Group 328"
                      transform="translate(1.739 9.694)"
                    >
                      <g
                        id="Group_327"
                        data-name="Group 327"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_375"
                          data-name="Path 375"
                          d="M111.529,368h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,368Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_330"
                      data-name="Group 330"
                      transform="translate(1.739 6.849)"
                    >
                      <g
                        id="Group_329"
                        data-name="Group 329"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_376"
                          data-name="Path 376"
                          d="M111.529,260h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,260Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                  </svg>
                }
              />
            </div>
            <div className="post__job__container__input__container__wrapper__top__row">
              <InputBox
                variant="select"
                placeholder="Salary type"
                options={salaryTypeDropDownData}
                isMulti={false}
                error={salaryTypeError}
                errorMessage={salaryTypeErrorMessage}
                onChange={(e) => handleSalaryType(e)}
                value={jobSalaryType}
                style={{
                  minWidth: "50%",
                  height: "fit-content",
                }}
                svg={
                  <svg
                    id="checklist"
                    xmlns="http://www.w3.org/2000/svg"
                    width="11.169"
                    height="13.488"
                    viewBox="0 0 11.169 13.488"
                  >
                    <g
                      id="Group_304"
                      data-name="Group 304"
                      transform="translate(5.321 4.004)"
                    >
                      <g
                        id="Group_303"
                        data-name="Group 303"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_363"
                          data-name="Path 363"
                          d="M250.058,152h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -152)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_306"
                      data-name="Group 306"
                      transform="translate(8.007 5.269)"
                    >
                      <g
                        id="Group_305"
                        data-name="Group 305"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_364"
                          data-name="Path 364"
                          d="M348.391,200.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,200.078Z"
                          transform="translate(-347.941 -200.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_308"
                      data-name="Group 308"
                      transform="translate(5.321 1.133)"
                    >
                      <g id="Group_307" data-name="Group 307">
                        <path
                          id="Path_365"
                          data-name="Path 365"
                          d="M246.441,43.078a.263.263,0,1,0,.077.186A.265.265,0,0,0,246.441,43.078Z"
                          transform="translate(-245.991 -43.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_310"
                      data-name="Group 310"
                      transform="translate(5.321 5.269)"
                    >
                      <g
                        id="Group_309"
                        data-name="Group 309"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_366"
                          data-name="Path 366"
                          d="M247.842,200h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -200)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_312"
                      data-name="Group 312"
                      transform="translate(5.321 6.849)"
                    >
                      <g
                        id="Group_311"
                        data-name="Group 311"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_367"
                          data-name="Path 367"
                          d="M250.058,260h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_314"
                      data-name="Group 314"
                      transform="translate(8.007 8.114)"
                    >
                      <g
                        id="Group_313"
                        data-name="Group 313"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_368"
                          data-name="Path 368"
                          d="M348.391,308.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,308.078Z"
                          transform="translate(-347.941 -308.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_316"
                      data-name="Group 316"
                      transform="translate(5.321 8.114)"
                    >
                      <g
                        id="Group_315"
                        data-name="Group 315"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_369"
                          data-name="Path 369"
                          d="M247.842,308h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -308)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_318"
                      data-name="Group 318"
                      transform="translate(5.321 9.694)"
                    >
                      <g
                        id="Group_317"
                        data-name="Group 317"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_370"
                          data-name="Path 370"
                          d="M250.058,368h-3.793a.263.263,0,1,0,0,.527h3.793a.263.263,0,1,0,0-.527Z"
                          transform="translate(-246.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_320"
                      data-name="Group 320"
                      transform="translate(8.007 10.959)"
                    >
                      <g
                        id="Group_319"
                        data-name="Group 319"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_371"
                          data-name="Path 371"
                          d="M348.391,416.078a.263.263,0,1,0,.077.186A.266.266,0,0,0,348.391,416.078Z"
                          transform="translate(-347.941 -416.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_322"
                      data-name="Group 322"
                      transform="translate(5.321 10.959)"
                    >
                      <g
                        id="Group_321"
                        data-name="Group 321"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_372"
                          data-name="Path 372"
                          d="M247.842,416h-1.577a.263.263,0,0,0,0,.527h1.577a.263.263,0,0,0,0-.527Z"
                          transform="translate(-246.001 -416)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_324"
                      data-name="Group 324"
                      transform="translate(0)"
                    >
                      <g id="Group_323" data-name="Group 323">
                        <path
                          id="Path_373"
                          data-name="Path 373"
                          d="M53.886,1.027H51.874A1.614,1.614,0,0,0,50.757.58h-.231a1.054,1.054,0,0,0-1.882,0h-.231a1.614,1.614,0,0,0-1.117.448H45.285A1.286,1.286,0,0,0,44,2.312V12.2a1.286,1.286,0,0,0,1.284,1.284h8.6A1.286,1.286,0,0,0,55.17,12.2V2.312A1.286,1.286,0,0,0,53.886,1.027Zm-5.473.079h.411a.263.263,0,0,0,.254-.193.527.527,0,0,1,1.015,0,.263.263,0,0,0,.254.193h.41a1.1,1.1,0,0,1,1.089,1H47.324A1.1,1.1,0,0,1,48.413,1.106Zm6.23,11.1a.758.758,0,0,1-.758.758h-8.6a.758.758,0,0,1-.758-.758V2.312a.758.758,0,0,1,.758-.758h1.642a1.61,1.61,0,0,0-.134.645v.171a.263.263,0,0,0,.263.263h5.057a.263.263,0,0,0,.263-.263V2.2a1.61,1.61,0,0,0-.134-.645h1.643a.758.758,0,0,1,.758.758Z"
                          transform="translate(-44.001)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_326"
                      data-name="Group 326"
                      transform="translate(1.614 3.864)"
                    >
                      <g
                        id="Group_325"
                        data-name="Group 325"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_374"
                          data-name="Path 374"
                          d="M107.557,146.744a.263.263,0,0,0-.373,0l-1.126,1.127-.347-.347a.263.263,0,0,0-.373.373l.533.533a.263.263,0,0,0,.373,0l1.313-1.313A.263.263,0,0,0,107.557,146.744Z"
                          transform="translate(-105.261 -146.667)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_328"
                      data-name="Group 328"
                      transform="translate(1.739 9.694)"
                    >
                      <g
                        id="Group_327"
                        data-name="Group 327"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_375"
                          data-name="Path 375"
                          d="M111.529,368h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,368Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -368)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                    <g
                      id="Group_330"
                      data-name="Group 330"
                      transform="translate(1.739 6.849)"
                    >
                      <g
                        id="Group_329"
                        data-name="Group 329"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_376"
                          data-name="Path 376"
                          d="M111.529,260h-1.264a.263.263,0,0,0-.263.263v1.264a.263.263,0,0,0,.263.263h1.264a.263.263,0,0,0,.263-.263v-1.264A.263.263,0,0,0,111.529,260Zm-.263,1.264h-.738v-.738h.738Z"
                          transform="translate(-110.001 -260)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                  </svg>
                }
              />
              <div style={{ minWidth: "50%" }}></div>
            </div>
            <div className="post__job__container__input__container__wrapper__top__row">
              {salaryType === "Range" ? (
                <div className="post__job__container__input__container__wrapper__top__sub">
                  <div className="post__job__container__input__container__wrapper__top__label">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      style={{ marginRight: ".5em" }}
                      width="17.158"
                      height="14.745"
                      viewBox="0 0 17.158 14.745"
                    >
                      <g id="salary" transform="translate(0 -36)">
                        <path
                          id="Path_383"
                          data-name="Path 383"
                          d="M13.437,43.943a3.976,3.976,0,0,0-.2-7.943h-.046a3.975,3.975,0,0,0-2.529.907.251.251,0,1,0,.32.388,3.476,3.476,0,1,1,.417,5.657A3.51,3.51,0,0,1,10.1,41.58a3.461,3.461,0,0,1,.117-3.413.251.251,0,0,0-.429-.262A3.962,3.962,0,0,0,9.274,40.7a3.971,3.971,0,0,0-4.906,4.326L2.8,45.508a.661.661,0,0,0-.6-.38H.66a.661.661,0,0,0-.66.661v3.784a.661.661,0,0,0,.66.66H2.2a.661.661,0,0,0,.624-.445l2.021.351a.269.269,0,0,0,.043,0,.251.251,0,0,0,.043-.5l-2.07-.359v-3.27l1.6-.493.39-.121.09-.028a8.523,8.523,0,0,1,2.879-.481h.815a.988.988,0,0,1,.27.088.779.779,0,0,1,.429.549.7.7,0,0,1,.01.188c-.024.3-.255.616-.83.616H7.879A.817.817,0,0,0,7,47.028a.833.833,0,0,0,.7.934l.016,0L8,48.013l1.67.279.062.01.022,0c.119.009.235.023.346.037a2.874,2.874,0,0,0,1.062-.012l3.609-.843h0c.422-.106.636.089.7.306a.521.521,0,0,1-.406.64L9.4,50.126a2.711,2.711,0,0,1-1.345.06l-.013,0-1.968-.341a.251.251,0,1,0-.086.5l1.961.34a3,3,0,0,0,.649.068,3.3,3.3,0,0,0,.946-.138l5.661-1.692a1.026,1.026,0,0,0,.747-1.255,1,1,0,0,0-1.3-.662l-3.607.843a1.8,1.8,0,0,1-.549.037,3.992,3.992,0,0,0,.8-.7,3.948,3.948,0,0,0,.984-2.879,4.015,4.015,0,0,0-.057-.468,3.971,3.971,0,0,0,.958.117q.128,0,.257-.008ZM2.36,49.573a.16.16,0,0,1-.158.157H.66A.159.159,0,0,1,.5,49.573V45.789a.158.158,0,0,1,.157-.158H2.2a.16.16,0,0,1,.158.158Zm8.558-2.723a3.456,3.456,0,0,1-1.3.925l-1.816-.3c-.219-.054-.334-.191-.307-.366a.317.317,0,0,1,.358-.269h.662a1.213,1.213,0,0,0,1.331-1.078,1.256,1.256,0,0,0-.6-1.162.811.811,0,0,0-.624-.293H8a.318.318,0,0,1,0-.636H9.191a.251.251,0,1,0,0-.5H8.564V42.74a.251.251,0,0,0-.5,0v.425H8A.82.82,0,0,0,7.3,44.4a9.271,9.271,0,0,0-2.439.473,3.461,3.461,0,0,1,.85-2.613h0a3.477,3.477,0,0,1,3.716-.995,4,4,0,0,0,2.243,2.386,3.515,3.515,0,0,1,.112.684A3.448,3.448,0,0,1,10.918,46.85Z"
                          fill="#0b2239"
                        />
                        <path
                          id="Path_384"
                          data-name="Path 384"
                          d="M361.2,95.945h-1.187a.251.251,0,1,0,0,.5h.626v.424a.251.251,0,0,0,.5,0v-.424h.058a.821.821,0,0,0,0-1.642h-.621a.318.318,0,0,1,0-.636h1.187a.251.251,0,1,0,0-.5h-.627v-.424a.251.251,0,0,0-.5,0v.424h-.057a.821.821,0,0,0,0,1.641h.621a.318.318,0,0,1,0,.637Z"
                          transform="translate(-347.708 -55.082)"
                          fill="#0b2239"
                        />
                      </g>
                    </svg>
                    Salary Range (monthly){" "}
                    <b style={{ marginLeft: ".5em" }}>€</b>
                  </div>
                  <InputBox
                    variant="simple"
                    placeholder="Min"
                    name="minSalary"
                    error={minSalaryError}
                    errorMessage={minSalaryErrorMessage}
                    onChange={(event) => {
                      const re = /^[0-9\b]+$/;
                      if (
                        event.currentTarget.value === "" ||
                        re.test(event.currentTarget.value)
                      ) {
                        handleChangeValues(event);
                      }
                    }}
                    maxValue={15}
                    value={salaryMin}
                  />
                  <InputBox
                    variant="simple"
                    placeholder="Max"
                    name="maxSalary"
                    error={maxSalaryError}
                    errorMessage={maxSalaryErrorMessage}
                    onChange={(event) => {
                      const re = /^[0-9\b]+$/;
                      if (
                        event.currentTarget.value === "" ||
                        re.test(event.currentTarget.value)
                      ) {
                        handleChangeValues(event);
                      }
                    }}
                    maxValue={15}
                    value={salaryMax}
                  />
                </div>
              ) : salaryType === "Fixed" ? (
                <div className="post__job__container__input__container__wrapper__top__sub">
                  <div className="post__job__container__input__container__wrapper__top__label">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      style={{ marginRight: ".5em" }}
                      width="17.158"
                      height="14.745"
                      viewBox="0 0 17.158 14.745"
                    >
                      <g id="salary" transform="translate(0 -36)">
                        <path
                          id="Path_383"
                          data-name="Path 383"
                          d="M13.437,43.943a3.976,3.976,0,0,0-.2-7.943h-.046a3.975,3.975,0,0,0-2.529.907.251.251,0,1,0,.32.388,3.476,3.476,0,1,1,.417,5.657A3.51,3.51,0,0,1,10.1,41.58a3.461,3.461,0,0,1,.117-3.413.251.251,0,0,0-.429-.262A3.962,3.962,0,0,0,9.274,40.7a3.971,3.971,0,0,0-4.906,4.326L2.8,45.508a.661.661,0,0,0-.6-.38H.66a.661.661,0,0,0-.66.661v3.784a.661.661,0,0,0,.66.66H2.2a.661.661,0,0,0,.624-.445l2.021.351a.269.269,0,0,0,.043,0,.251.251,0,0,0,.043-.5l-2.07-.359v-3.27l1.6-.493.39-.121.09-.028a8.523,8.523,0,0,1,2.879-.481h.815a.988.988,0,0,1,.27.088.779.779,0,0,1,.429.549.7.7,0,0,1,.01.188c-.024.3-.255.616-.83.616H7.879A.817.817,0,0,0,7,47.028a.833.833,0,0,0,.7.934l.016,0L8,48.013l1.67.279.062.01.022,0c.119.009.235.023.346.037a2.874,2.874,0,0,0,1.062-.012l3.609-.843h0c.422-.106.636.089.7.306a.521.521,0,0,1-.406.64L9.4,50.126a2.711,2.711,0,0,1-1.345.06l-.013,0-1.968-.341a.251.251,0,1,0-.086.5l1.961.34a3,3,0,0,0,.649.068,3.3,3.3,0,0,0,.946-.138l5.661-1.692a1.026,1.026,0,0,0,.747-1.255,1,1,0,0,0-1.3-.662l-3.607.843a1.8,1.8,0,0,1-.549.037,3.992,3.992,0,0,0,.8-.7,3.948,3.948,0,0,0,.984-2.879,4.015,4.015,0,0,0-.057-.468,3.971,3.971,0,0,0,.958.117q.128,0,.257-.008ZM2.36,49.573a.16.16,0,0,1-.158.157H.66A.159.159,0,0,1,.5,49.573V45.789a.158.158,0,0,1,.157-.158H2.2a.16.16,0,0,1,.158.158Zm8.558-2.723a3.456,3.456,0,0,1-1.3.925l-1.816-.3c-.219-.054-.334-.191-.307-.366a.317.317,0,0,1,.358-.269h.662a1.213,1.213,0,0,0,1.331-1.078,1.256,1.256,0,0,0-.6-1.162.811.811,0,0,0-.624-.293H8a.318.318,0,0,1,0-.636H9.191a.251.251,0,1,0,0-.5H8.564V42.74a.251.251,0,0,0-.5,0v.425H8A.82.82,0,0,0,7.3,44.4a9.271,9.271,0,0,0-2.439.473,3.461,3.461,0,0,1,.85-2.613h0a3.477,3.477,0,0,1,3.716-.995,4,4,0,0,0,2.243,2.386,3.515,3.515,0,0,1,.112.684A3.448,3.448,0,0,1,10.918,46.85Z"
                          fill="#0b2239"
                        />
                        <path
                          id="Path_384"
                          data-name="Path 384"
                          d="M361.2,95.945h-1.187a.251.251,0,1,0,0,.5h.626v.424a.251.251,0,0,0,.5,0v-.424h.058a.821.821,0,0,0,0-1.642h-.621a.318.318,0,0,1,0-.636h1.187a.251.251,0,1,0,0-.5h-.627v-.424a.251.251,0,0,0-.5,0v.424h-.057a.821.821,0,0,0,0,1.641h.621a.318.318,0,0,1,0,.637Z"
                          transform="translate(-347.708 -55.082)"
                          fill="#0b2239"
                        />
                      </g>
                    </svg>
                    Salary Fixed (monthly)
                    <b style={{ marginLeft: ".5em" }}>€</b>
                  </div>
                  <InputBox
                    placeholder="Amount"
                    name="minSalary"
                    error={fixSalaryError}
                    errorMessage={fixSalaryErrorMessage}
                    onChange={(event) => {
                      const re = /^[0-9\b]+$/;
                      if (
                        event.currentTarget.value === "" ||
                        re.test(event.currentTarget.value)
                      ) {
                        handleChangeValues(event);
                      }
                    }}
                    maxValue={15}
                    value={salaryMin}
                  />
                </div>
              ) : null}
            </div>
          </div>
          <div className="post__job__container__input__container__wrapper__bottom">
            <InputBox
              variant="textarea"
              placeholder="Job description"
              name="description"
              error={descriptionError}
              errorMessage={descriptionErrorMessage}
              onChange={(e) => handleChangeDescriptionValues(e)}
              value={jobDescription}
            />
            <InputBox
              variant="textarea"
              placeholder="Job requirements"
              name="requirements"
              error={requirementError}
              errorMessage={requirementErrorMessage}
              onChange={(e) => handleChangeRequirementsValues(e)}
              value={jobRequirements}
            />
          </div>
          <div
            className="homepage__container__jumbotron__form__filters__role"
            style={{ justifyContent: "flex-start" }}
          >
            <input
              className="styled-checkbox"
              id="styled-checkbox-company-details-show"
              type="checkbox"
              name="isCompanyDetailShow"
              checked={isCompanyDetailShow}
              onChange={(e) => handleChangeValues(e)}
            />
            <label htmlFor="styled-checkbox-company-details-show">
              Do you want to show your company details?
            </label>
          </div>
          <div className="post__job__container__input__container__wrapper__bottom__btns">
            <button
              className="post__job__container__input__container__wrapper__bottom__btns__preview"
              onClick={() => {
                showPreviewData();
              }}
              title="preview data"
            >
              Preview
            </button>
            <button
              className="homepage__container__result__card__content__right__buttons__drafts"
              onClick={() => saveDraftClicked()}
              title="save draft"
              disabled={isLoading ? true : false}
            >
              {isLoading ? "Processing..." : "Save Draft"}
            </button>
            <button
              className="header__nav__btn btn__secondary"
              style={{
                height: "40px",
              }}
              onClick={() => save()}
              title={isUpdate ? "Update" : "Post"}
              disabled={isLoading ? true : false}
            >
              {isUpdate
                ? isLoading
                  ? "Processing..."
                  : "Update"
                : isLoading
                ? "Processing..."
                : "Post"}
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default PostJob;
