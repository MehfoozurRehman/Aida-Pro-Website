import React, { useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { forgetPassword } from "API/Users";
import { CustomError } from "./Toasts";
import { crossSvg } from "Assets";

export default function ForgotPassword({
  setIsForgotPasswordOpen,
  setIsEmailVerificationOpen,
  setIsLoginOpen,
  setIsFromForgotPassword,
}) {
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  //validation
  const [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState(false);

  const handleChange = (event) => {
    if (event.currentTarget.value === "") {
      setEmailError(true);
      setEmailErrorMessage("Please enter email address");
    } else if (
      !event.currentTarget.value.match(
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/
      )
    ) {
      setEmailError(true);
      setEmailErrorMessage("Please enter valid email");
    } else {
      setEmailError(false);
      setEmailErrorMessage("");
      setEmail(event.currentTarget.value);
    }
  };

  const forgetPasswordCheckEmail = () => {
    if (email === "") {
      setEmailError(true);
      setEmailErrorMessage("Please enter email address");
    } else {
      setIsLoading(true);
      const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
      if (emailRegex.test(email)) {
        forgetPassword(email)
          .then(({ data }) => {
            if (data.success) {
              setIsForgotPasswordOpen(false);
              localStorage.setItem("emailTobeVerify", email);
              setIsFromForgotPassword(true);
              setIsEmailVerificationOpen(true);
            } else
              CustomError(data.errors);
            setIsLoading(false);
          })
          .catch((err) => {
            setIsLoading(false);
          });
      } else {
        setEmailError(true);
        setEmailErrorMessage("Please enter correct Email");
      }
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Forgot Password" description="Forgot Password" />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsForgotPasswordOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px", textAlign: "center" }}
              >
                Forgot password?
              </div>
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "16px",
                  textAlign: "center",
                  marginTop: "0em",
                  marginBottom: "2em",
                }}
              >
                Reset password in two quick steps
              </div>
              <InputBox
                placeholder="Email"
                type="text"
                error={emailError}
                errorMessage={emailErrorMessage}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="17.919"
                    height="11.946"
                    viewBox="0 0 17.919 11.946"
                  >
                    <g id="mail" transform="translate(0)">
                      <g
                        id="Group_202"
                        data-name="Group 202"
                        transform="translate(0)"
                      >
                        <path
                          id="Path_292"
                          data-name="Path 292"
                          d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                          transform="translate(0 -85.333)"
                          fill="#374957"
                        />
                      </g>
                    </g>
                  </svg>
                }
                onChange={(e) => handleChange(e)}
              />
              <button
                type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  margin: "0em auto",
                  marginTop: "2em",
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => forgetPasswordCheckEmail()}
                title="forget Password Check Email"
              >
                Reset password
              </button>
              {/* <button
                type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  margin: "0em auto",
                  height: "50px",
                  width: "180px",
                  background: "transparent",
                  color: "#242424",
                  boxShadow: "none",
                }}
                onClick={() => {
                  setIsForgotPasswordOpen(false);
                  setIsLoginOpen(true);
                }}
                title="back"
              >
                Back
              </button> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
