import React, { useState, useEffect, useContext } from "react";
import { Head, InputBox } from "Components";
import {
  jobSekkerExperienceEdit,
  jobSekkerExperienceUpdate,
} from "../API/EmploymentAPI";
import {
  freelancerExperienceUpdate,
  freelancerExperienceEdit,
} from "../API/FreelancerApi";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { getAllIndustry, getLookUpByPrefix } from "../API/Api";
import { isNullOrEmpty } from "Utils/TextUtils";
import { months, years } from "Constants/Constants";
import { crossSvg } from "Assets";

export default function AddWorkExperiance({
  setIsAddWorkExperianceOpen,
  addWorkExperianceEditData,
}) {

  const [jobType, setJobType] = useState([]);
  const [industryType, setIndustryType] = useState([]);
  const [experienceDescription, setExperienceDescription] = useState(
    addWorkExperianceEditData != null
      ? addWorkExperianceEditData.jobDescription
      : ""
  );
  const [experienceLocation, setExperienceLocation] = useState(
    "Test location for experience"
  );
  const [experienceTitle, setExperienceTitle] = useState(
    addWorkExperianceEditData != null ? addWorkExperianceEditData.jobTitle : ""
  );
  const [experienceCompanyName, setExperienceCompanyName] = useState(
    addWorkExperianceEditData != null
      ? addWorkExperianceEditData.jobCompany
      : ""
  );
  const [jobTypeId, setJobTypeId] = useState(
    addWorkExperianceEditData != null
      ? addWorkExperianceEditData.jobEmployementType
      : null
  );
  const [industryTypeId, setIndustryTypeId] = useState(
    addWorkExperianceEditData != null
      ? addWorkExperianceEditData.jobIndustryType
      : null
  );
  const [isLoading, setIsLoading] = useState(false);
  const [startMonth, setStartMonth] = useState("");
  const [startYear, setStartYear] = useState("");
  let [endMonth, setEndMonth] = useState("");
  let [endYear, setEndYear] = useState("");

  // validation
  let [titleError, setTitleError] = useState(false);
  let [titleErrorMessage, setTitleErrorMessage] = useState("");
  let [companyNameError, setCompanyNameError] = useState(false);
  let [companyNameErrorMessage, setCompanyNameErrorMessage] = useState("");
  let [industryTypeError, setIndustryTypeError] = useState(false);
  let [industryTypeErrorMessage, setIndustryTypeErrorMessage] = useState("");
  let [employmentTypeError, setEmploymentTypeError] = useState(false);
  let [employmentTypeErrorMessage, setEmploymentTypeErrorMessage] =
    useState("");
  let [descriptionError, setDescriptionError] = useState(false);
  let [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");
  let [startingYearError, setStartingYearError] = useState(false);
  let [startingYearErrorMessage, setStartingYearErrorMessage] = useState("");
  let [endingYearError, setEndingYearError] = useState(false);
  let [endingYearErrorMessage, setEndingYearErrorMessage] = useState("");
  const [isExpirationDate, setIsExpirationDate] = useState(
    addWorkExperianceEditData != null
      ? addWorkExperianceEditData.jobTo == null
        ? true
        : false
      : false
  );

  const [startMonthError, setStartMonthError] = useState(false);
  const [startMonthErrorMessage, setStartMonthErrorMessage] = useState("");
  const [startYearError, setStartYearError] = useState(false);
  const [startYearErrorMessage, setStartYearErrorMessage] = useState("");

  const [endMonthError, setEndMonthError] = useState(false);
  const [endMonthErrorMessage, setEndMonthErrorMessage] = useState("");
  const [endYearError, setEndYearError] = useState(false);
  const [endYearErrorMessage, setEndYearErrorMessage] = useState("");

  const user = useContext(UserContext);
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    // setIsLoading(true);
    //#region dates
    if (addWorkExperianceEditData != null) {
      const start_month = months.find(
        (element) => element.label == addWorkExperianceEditData.StartMonth
      );
      setStartMonth(start_month);
      const start_year = years.find(
        (element) => element.label == addWorkExperianceEditData.StartYear
      );
      setStartYear(start_year);
      if (isExpirationDate == false) {
        const end_month = months.find(
          (element) => element.label == addWorkExperianceEditData.EndMonth
        );
        setEndMonth(end_month);
        const end_year = years.find(
          (element) => element.label == addWorkExperianceEditData.EndYear
        );
        setEndYear(end_year);
      }
    }
    //#endregion

    getLookUpByPrefix("JOBT")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setJobType(formattedData);
      })
      .catch((err) => {
        // setIsLoading(false);
      });

    getAllIndustry()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });

        setIndustryType(formattedData);
        // setIsLoading(false);
      })
      .catch(() => {
        // setIsLoading(false);
      });
  }, []);

  const handleChange = (event) => {
    if (event.currentTarget.name === "title") {
      if (event.currentTarget.value === "") {
        setTitleError(true);
        setTitleErrorMessage("Please enter title");
      } else {
        setTitleError(false);
        setTitleErrorMessage("");
      }
      setExperienceTitle(event.currentTarget.value);
    } else {
      if (event.currentTarget.value === "") {
        setCompanyNameError(true);
        setCompanyNameErrorMessage("Please enter Company Name");
      } else {
        setCompanyNameError(false);
        setCompanyNameErrorMessage("");
      }
      setExperienceCompanyName(event.currentTarget.value);
    }
  };

  const startDateChange = (event) => {
    if (event.target.value === "") {
      setStartingYearError(true);
      setStartingYearErrorMessage("Please enter starting year");
    } else {
      setStartingYearError(false);
      setStartingYearErrorMessage("");
    }
    setStartYear(event.target.value);
  };

  const endDateChange = (event) => {
    if (event.target.value === "") {
      setEndingYearError(true);
      setEndingYearErrorMessage("Please enter ending year.");
    } else if (event.target.value < startYear) {
      setEndingYearError(true);
      setEndingYearErrorMessage("End date can't be less than start date.");
    } else {
      setEndingYearError(false);
      setEndingYearErrorMessage("");
    }
    setEndYear(event.target.value);
  };

  const handleChangeJobType = (e) => {
    if (e.length === 0) {
      setEmploymentTypeError(true);
      setEmploymentTypeErrorMessage("Please select employment type");
    } else {
      setEmploymentTypeError(false);
      setEmploymentTypeErrorMessage("");
    }
    setJobTypeId(e);
  };

  const handleChangeIndustryType = (e) => {
    if (e.length === 0) {
      setIndustryTypeError(true);
      setIndustryTypeErrorMessage("Please select industry type");
    } else {
      setIndustryTypeError(false);
      setIndustryTypeErrorMessage("");
    }
    setIndustryTypeId(e);
  };

  const handleChangeDescriptionValues = (data) => {
    if (data === "") {
      setDescriptionError(true);
      setDescriptionErrorMessage("Please enter description");
    } else {
      setDescriptionError(false);
      setDescriptionErrorMessage("");
    }
    setExperienceDescription(data);
  };

  const viewIsValid = () => {
    if (isNullOrEmpty(experienceTitle))
      setTitleErrorMessageAndVisibility("Please enter title");
    else if (isNullOrEmpty(experienceCompanyName))
      setExperienceCompanyNameErrorMessageAndVisibility(
        "Please enter company name"
      );
    else if (industryTypeId == null)
      setIndustryTypeIdErrorMessageAndVisibility("Please select industry type");
    else if (jobTypeId == null)
      setJobTypeIdErrorMessageAndVisibility("Please select employment type");
    else if (isNullOrEmpty(startMonth)) {
      setStartMonthError(true);
      setStartMonthErrorMessage("Please select starting month.");
    } else if (isNullOrEmpty(startYear)) {
      setStartYearError(true);
      setStartYearErrorMessage("Please select starting year");
    } else if (isExpirationDate == false) {
      if (isNullOrEmpty(endMonth)) {
        setEndMonthError(true);
        setEndMonthErrorMessage("Please select ending month");
      } else if (isNullOrEmpty(endYear)) {
        setEndYearError(true);
        setEndYearErrorMessage("Please select ending year");
      } else if (endYear.label < startYear.label) {
        setEndYearError(true);
        setEndYearErrorMessage("Ending year can't be less than starting year.");
      } else return true;
    } else if (isNullOrEmpty(experienceDescription))
      setExperienceDescriptionErrorMessageAndVisibility(
        "Please enter description"
      );
    else return true;
    return false;
  };

  const setTitleErrorMessageAndVisibility = (text) => {
    setTitleError((titleError = !isNullOrEmpty(text)));
    setTitleErrorMessage(text);
  };
  const setExperienceCompanyNameErrorMessageAndVisibility = (text) => {
    setCompanyNameError((companyNameError = !isNullOrEmpty(text)));
    setCompanyNameErrorMessage(text);
  };
  const setIndustryTypeIdErrorMessageAndVisibility = (text) => {
    setIndustryTypeError((industryTypeError = !isNullOrEmpty(text)));
    setIndustryTypeErrorMessage(text);
  };
  const setJobTypeIdErrorMessageAndVisibility = (text) => {
    setEmploymentTypeError((employmentTypeError = !isNullOrEmpty(text)));
    setEmploymentTypeErrorMessage(text);
  };
  const setExperienceDescriptionErrorMessageAndVisibility = (text) => {
    setDescriptionError((descriptionError = !isNullOrEmpty(text)));
    setDescriptionErrorMessage(text);
  };
  const setStartYearErrorMessageAndVisibility = (text) => {
    setStartingYearError((startingYearError = !isNullOrEmpty(text)));
    setStartingYearErrorMessage(text);
  };
  const setEndYearErrorMessageAndVisibility = (text) => {
    setEndingYearError((endingYearError = !isNullOrEmpty(text)));
    setEndingYearErrorMessage(text);
  };

  const saveExperience = (e) => {
    e.preventDefault();
    if (viewIsValid()) {
      if (isExpirationDate) {
        setEndMonth((endMonth = ""));
        setEndYear((endYear = ""));
      }
      setIsLoading(true);
      if (user.Role.Title !== "JobSekker") {
        let experienceObject = {
          Id:
            addWorkExperianceEditData != null
              ? addWorkExperianceEditData.Id
              : 0,
          FreelancerId: user.FreelancerId,
          CompanyName: experienceCompanyName,
          Description: experienceDescription,
          EmploymentTypeLookupDetailId: jobTypeId.value,
          IndustryId: industryTypeId.value,
          Location: experienceLocation,
          Title: experienceTitle,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
        };
        if (addWorkExperianceEditData != null) {
          freelancerExperienceEdit(experienceObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Experience Edited Successfully...");
              setIsAddWorkExperianceOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Edit Experience ");
              setIsAddWorkExperianceOpen(false);
            });
        } else {
          freelancerExperienceUpdate(experienceObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Experience Update Successfully...");
              setIsAddWorkExperianceOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Experience ");
              setIsAddWorkExperianceOpen(false);
            });
        }
      } else {
        let experienceObject = {
          Id:
            addWorkExperianceEditData != null
              ? addWorkExperianceEditData.Id
              : 0,
          jobseekerId: user.JobSeekerId,
          CompanyName: experienceCompanyName,
          Description: experienceDescription,
          EmploymentTypeLookupDetailId: jobTypeId.value,
          IndustryId: industryTypeId.value,
          Location: experienceLocation,
          Title: experienceTitle,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
        };

        if (addWorkExperianceEditData != null) {
          jobSekkerExperienceEdit(experienceObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Experience Edited Successfully...");
              setIsAddWorkExperianceOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Edit Experience ");
              setIsAddWorkExperianceOpen(false);
            });
        } else {
          jobSekkerExperienceUpdate(experienceObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Experience Update Successfully...");
              setIsAddWorkExperianceOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Experience ");
              setIsAddWorkExperianceOpen(false);
            });
        }
      }
    }
  };
  const setExpirationDisabled = (e) => {
    setIsExpirationDate(e.currentTarget.checked);
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Add Work Experiance"
        description="Add work experiance"
      />
      <div className="pupup__container">
        <form
          onSubmit={saveExperience}
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsAddWorkExperianceOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="45.148"
                height="39.488"
                viewBox="0 0 45.148 39.488"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-2"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#fff4ee" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-4"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                </defs>
                <g
                  id="suitcase_1_"
                  data-name="suitcase (1)"
                  transform="translate(-0.001)"
                >
                  <g
                    id="Group_1096"
                    data-name="Group 1096"
                    transform="translate(0.001 0)"
                  >
                    <path
                      id="Path_3425"
                      data-name="Path 3425"
                      d="M183.373,34.091a8.882,8.882,0,0,0-1.961-1.047,14.434,14.434,0,0,0-10.335,0,8.882,8.882,0,0,0-1.961,1.047,1.484,1.484,0,0,0-.584,1.18V37.6l1.484.794L171.5,37.6V36.111a11.188,11.188,0,0,1,9.489,0V37.6l1.484.794,1.484-.794V35.271A1.483,1.483,0,0,0,183.373,34.091Z"
                      transform="translate(-153.671 -32.088)"
                      fill="url(#linear-gradient)"
                    />
                    <g
                      id="Group_1095"
                      data-name="Group 1095"
                      transform="translate(14.861 5.511)"
                    >
                      <path
                        id="Path_3426"
                        data-name="Path 3426"
                        d="M168.533,94.589v1.638l1.484.794,1.484-.794V94.589Z"
                        transform="translate(-168.533 -94.589)"
                        fill="url(#linear-gradient-2)"
                      />
                      <path
                        id="Path_3427"
                        data-name="Path 3427"
                        d="M312.773,94.589H309.8v1.638l1.484.794,1.484-.794Z"
                        transform="translate(-297.348 -94.589)"
                        fill="url(#linear-gradient-2)"
                      />
                    </g>
                    <path
                      id="Path_3428"
                      data-name="Path 3428"
                      d="M13.247,298.286v13.182a2.834,2.834,0,0,0,2.834,2.834H53.225a2.834,2.834,0,0,0,2.834-2.834V298.286Z"
                      transform="translate(-12.079 -274.813)"
                      fill="url(#linear-gradient-4)"
                    />
                    <path
                      id="Path_3429"
                      data-name="Path 3429"
                      d="M13.247,279.705v1.638c2.354,1.869,8.576,5.5,21.406,5.5s19.052-3.628,21.406-5.5V279.7H13.247Z"
                      transform="translate(-12.079 -257.871)"
                      fill="#f7983d"
                    />
                    <path
                      id="Path_3430"
                      data-name="Path 3430"
                      d="M1.045,126.91c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a4.733,4.733,0,0,0,1.045-2.992V116a2.834,2.834,0,0,0-2.834-2.833H2.834A2.834,2.834,0,0,0,0,116v7.916a4.733,4.733,0,0,0,1.045,2.992Z"
                      transform="translate(0 -106.019)"
                      fill="url(#linear-gradient-4)"
                    />
                    <path
                      id="Path_3431"
                      data-name="Path 3431"
                      d="M44.1,237.276c-2.256,1.837-8.469,5.6-21.529,5.6s-19.273-3.759-21.529-5.6A2.834,2.834,0,0,1,0,235.078v1.638a2.834,2.834,0,0,0,1.045,2.2c2.256,1.837,8.469,5.6,21.529,5.6s19.273-3.759,21.529-5.6a2.834,2.834,0,0,0,1.045-2.2v-1.638A2.834,2.834,0,0,1,44.1,237.276Z"
                      transform="translate(0 -217.179)"
                      fill="url(#linear-gradient)"
                    />
                    <path
                      id="Path_3432"
                      data-name="Path 3432"
                      d="M228.551,310.914c1.466,0,2.654-1.089,2.654-3.448v-1.609a.944.944,0,0,0-.944-.944h-3.419a.944.944,0,0,0-.945.944v1.61C225.9,309.826,227.085,310.914,228.551,310.914Z"
                      transform="translate(-205.978 -280.856)"
                      fill="#fff"
                    />
                    <path
                      id="Path_3433"
                      data-name="Path 3433"
                      d="M228.551,336.528a2.654,2.654,0,0,1-2.654-2.654v1.639a2.654,2.654,0,0,0,5.308,0v-1.639A2.654,2.654,0,0,1,228.551,336.528Z"
                      transform="translate(-205.978 -307.263)"
                      fill="url(#linear-gradient-4)"
                    />
                  </g>
                </g>
              </svg>

              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  {addWorkExperianceEditData != null ? "Edit " : "Add "}work
                  experience
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  {addWorkExperianceEditData != null ? "Edit " : "Add "}your
                  work experience history here
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="simple"
                  placeholder="Title"
                  name="title"
                  value={experienceTitle}
                  error={titleError}
                  errorMessage={titleErrorMessage}
                  onChange={(e) => handleChange(e)}
                />
                <InputBox
                  variant="simple"
                  placeholder="Company name"
                  name="companyName"
                  value={experienceCompanyName}
                  error={companyNameError}
                  errorMessage={companyNameErrorMessage}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Industry type"
                  options={industryType}
                  value={industryTypeId}
                  error={industryTypeError}
                  errorMessage={industryTypeErrorMessage}
                  onChange={(e) => handleChangeIndustryType(e)}
                />
                <InputBox
                  variant="select"
                  placeholder="Employment type"
                  options={jobType}
                  value={jobTypeId}
                  error={employmentTypeError}
                  errorMessage={employmentTypeErrorMessage}
                  onChange={(e) => handleChangeJobType(e)}
                />
              </div>

              <div className="pupup__container__from__wrapper__form__row">
                <div
                  className="homepage__container__jumbotron__form__filters__role"
                  style={{ marginLeft: 10 }}
                >
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-credential-does-not-expire"
                    type="checkbox"
                    value="Remember"
                    defaultChecked={isExpirationDate}
                    defaultValue={isExpirationDate}
                    name="Remember"
                    onClick={(e) => setExpirationDisabled(e)}
                  />
                  <label htmlFor="styled-checkbox-credential-does-not-expire">
                    Still working in this role
                  </label>
                </div>
              </div>
              <div className="pupup__container__from__wrapper__form__row__wrapper">
                <div className="pupup__container__from__wrapper__form__row__wrapper__entry">
                  <div
                    className="pupup__container__from__wrapper__header__content__info"
                    style={{ marginLeft: 5, marginBottom: 10, marginTop: -10 }}
                  >
                    Start Date
                  </div>
                  <div className="pupup__container__from__wrapper__form__row">
                    <InputBox
                      variant="select"
                      placeholder="Month"
                      options={months}
                      error={startMonthError}
                      errorMessage={startMonthErrorMessage}
                      value={startMonth}
                      onChange={(e) => {
                        setStartMonthError(false);
                        setStartMonth(e);
                      }}
                    />
                    <InputBox
                      variant="select"
                      placeholder="Year"
                      options={years}
                      error={startYearError}
                      errorMessage={startYearErrorMessage}
                      value={startYear}
                      onChange={(e) => {
                        setStartYearError(false);
                        setStartYear(e);
                      }}
                    />
                  </div>
                </div>
                <div className="pupup__container__from__wrapper__form__row__wrapper__entry">
                  {isExpirationDate ? null : (
                    <>
                      <div
                        className="pupup__container__from__wrapper__header__content__info"
                        style={{
                          marginLeft: 5,
                          marginBottom: 10,
                          marginTop: -10,
                        }}
                      >
                        End Date
                      </div>
                      <div className="pupup__container__from__wrapper__form__row">
                        <InputBox
                          variant="select"
                          placeholder="Month"
                          options={months}
                          error={endMonthError}
                          errorMessage={endMonthErrorMessage}
                          value={endMonth}
                          onChange={(e) => {
                            setEndMonthError(false);
                            setEndMonth(e);
                          }}
                        />
                        <InputBox
                          variant="select"
                          placeholder="Year"
                          options={years}
                          error={endYearError}
                          errorMessage={endYearErrorMessage}
                          value={endYear}
                          onChange={(e) => {
                            setEndYearError(false);
                            setEndYear(e);
                          }}
                        />
                      </div>
                    </>
                  )}
                </div>
              </div>

              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="textarea"
                  placeholder="Description"
                  name="description"
                  value={experienceDescription}
                  error={descriptionError}
                  errorMessage={descriptionErrorMessage}
                  onChange={(e) => handleChangeDescriptionValues(e)}
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                disabled={isLoading ? true : false}
                title="add work experiance"
              >
                {isLoading ? "Loading..." : "Save"}
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
