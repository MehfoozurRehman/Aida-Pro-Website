import React, { useEffect, useState, useContext } from "react";
import HomepageResearcherJumbotron from "Components/HomepageResearcherJumbotron";
import EmploymentResultCard from "Components/HomePageEmploymentResultCard";
import { employmentDefaultListing } from "Constants/defaultData";
import { jobSekkerJobsForHomePage } from "../API/EmploymentAPI";
import { isUserLoggedIn } from "Utils/common";
import UserContext from "Context/UserContext";
import { navigate } from "@reach/router";
import Header from "Components/Header";
import Img from "react-cool-img";
import _ from "lodash";
import {
  applyForJob,
  bestIdea,
  filteredSkills,
  homePageEmploymentSvg,
} from "Assets";
import {
  Head,
  JobProjectPanel,
  Testmonials,
  MobileAppAdvert,
} from "Components";

const HomepageResearcher = ({
  setIsLoginOpen,
  setRedirectURLFromLogin,
  setIsApplyForJob,
}) => {
  localStorage.setItem("isOn", "professional");
  const [isLoading, setIsLoading] = useState(false);
  const [jobs, setJobs] = useState(employmentDefaultListing);
  let [title, setTitle] = useState("");
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(10);
  let [location, setJobLocation] = useState("");
  let [range, setRange] = useState(500);
  const user = useContext(UserContext);

  useEffect(() => {
    let response = isUserLoggedIn(user);
    if (response) {
      if (user.Freelancer != null) navigate("/home-freelancer");
      else if (user.JobSeeker != null) navigate("/home-professional");
      else if (user.CompanyProfile != null) navigate("/home-company");
    }
  }, [user]);

  useEffect(() => {
    jobSellerJobsApi();
  }, [page]);

  const jobSellerJobsApi = () => {
    setIsLoading(true);
    jobSekkerJobsForHomePage(page, limit, title, location, range)
      .then(({ data }) => {
        const filteredResult = _.slice(data.result, [0], [6]);
        setJobs(filteredResult);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    setRedirectURLFromLogin("/home-professional");
  }, []);

  const navigateToDetail = (dataObject) => {
    if (user.Id !== undefined) {
      navigate("/job-details", { state: dataObject });
    } else {
      setIsLoginOpen(true);
    }
  };

  const searchByFilter = (keywords, rangeValue, jobLocation) => {
    setTitle((title = keywords));
    setJobLocation((location = jobLocation));
    setRange((range = rangeValue));
    jobSellerJobsApi();
  };

  const resetFilter = () => {
    setTitle((title = ""));
    setJobLocation((location = ""));
    setRange((range = 500));
    setPage(1);
    jobSellerJobsApi();
  };

  return (
    <>
      <Head title="AIDApro | Professional" description="Professional" />
      <Header
        isOnHomeCompany={false}
        setIsLoginOpen={setIsLoginOpen}
        redirectURLSignUp="/sign-up"
      />
      <HomepageResearcherJumbotron
        homePageEmploymentSvg={homePageEmploymentSvg}
      />
      <div className="homepage__container">
        <div
          className="homepage__container__results"
          style={{ marginBottom: "3em" }}
        >
          {jobs.slice(0, 3).map((e, i) => (
            <EmploymentResultCard key={i} data={e} moveTo={navigateToDetail} />
          ))}
        </div>
        <section id="homeemploymentsearchpanel" style={{ paddingTop: "2em" }}>
          <JobProjectPanel
            descriptionHeading="Job description"
            responsibilitesHeading="Job responsibility"
            jobsList={jobs}
            setIsLoginOpen={setIsLoginOpen}
            setIsApplyForJob={setIsApplyForJob}
            searchByFilter={searchByFilter}
            resetFilter={resetFilter}
            keyword={title}
            selectedLocation={location}
            selectedRange={range}
            isFreelancer={false}
          />
        </section>
        <div className="homepage__container__vacancy">
          <div className="homepage__container__vacancy__content">
            <div className="homepage__container__vacancy__content__heading">
              Apply for your <span>favourite job</span>
            </div>
            <div className="homepage__container__vacancy__content__info">
              Create a profile and let companies find you or apply for your
              favourite job. Looking for employment or project challenges? Our
              platform offers you flexibility and a variety of options to find
              your dream job. Login today and explore AIDApro’s job offerings.
            </div>
          </div>
          <Img
            loading="lazy"
            src={applyForJob}
            alt="applyForJob"
            className="homepage__container__vacancy__img"
            style={{ height: 420 }}
          />
        </div>
        <div className="homepage__container__qualification">
          <Img
            loading="lazy"
            src={bestIdea}
            alt="bestIdea"
            className="homepage__container__qualification__left"
          />
          <div className="homepage__container__qualification__right">
            <div className="homepage__container__qualification__heading">
              Grow with the best platform for{" "}
              <span>AI and Data professionals</span>
            </div>
            <div className="homepage__container__qualification__info">
              Our platform is dedicated to AI and Data with companies looking
              for talented candidates in these fields. Our pool of AI and data
              professionals is also available for any question or problem on
              your mind. Or read blogs and subscribe to dedicated and relevant
              newsletters. All these and more for free as AI and data is our
              true passion and life dedication. Come on board and help our
              passionate community and yourself to grow.
            </div>
            <a
              href="#homeemploymentsearchpanel"
              to="/"
              className="header__nav__btn btn__primary"
              style={{ width: 150 }}
            >
              Find job
            </a>
          </div>
        </div>
        <div className="homepage__container__tagline">
          <Img
            loading="lazy"
            src={filteredSkills}
            alt="filteredSkills"
            className="homepage__container__tagline__background"
          />
          <div className="homepage__container__tagline__content">
            <div className="homepage__container__tagline__heading">
              Get jobs in <span>100+ skills</span> across seniorities
            </div>
            <div className="homepage__container__tagline__info">
              Connect with companies selecting you. Or let companies looking for
              your skills and experience find you instead. Our platform is
              dedicated to help you. Assisting where possible in your search for
              great jobs. Anytime, anywhere.
            </div>
            <a
              href="#homeemploymentsearchpanel"
              to="/"
              className="header__nav__btn btn__secondary"
              style={{ width: 180, height: 50 }}
            >
              Find job
            </a>
          </div>
        </div>
      </div>
      <Testmonials />
      <MobileAppAdvert />
      <div className="homepage__container"></div>
    </>
  );
};

export default HomepageResearcher;
