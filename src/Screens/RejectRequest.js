import React, { useEffect } from "react";
import { Head, InputBox } from "Components";
import { crossSvg } from "Assets";

export default function RejectRequest({ setIsRejectRequest }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Request Video Call"
        description="Request Video Call"
      />
      <div className="pupup__container">
        <form
          onSubmit={() => {}}
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsRejectRequest(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="33.195"
                height="33.203"
                viewBox="0 0 33.195 33.203"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stop-color="#0ee1a3" />
                    <stop offset="1" stop-color="#0ca69d" />
                  </linearGradient>
                </defs>
                <g
                  id="Group_1768"
                  data-name="Group 1768"
                  transform="translate(-179.677 -142.963)"
                >
                  <path
                    id="Path_22079"
                    data-name="Path 22079"
                    d="M373.374,328.286c-.108.526-.172,1.066-.331,1.576a8.745,8.745,0,0,1-7.386,6.494,8.992,8.992,0,1,1,7.593-10.382c.035.21.083.418.125.626Zm-8.968,1.066a3.419,3.419,0,0,0,.25.33c.411.414.815.836,1.245,1.23a1.364,1.364,0,0,0,2.211-1.422,1.972,1.972,0,0,0-.433-.668c-.433-.46-.9-.892-1.365-1.353.512-.508.983-.968,1.445-1.436a1.375,1.375,0,1,0-1.944-1.946c-.475.468-.941.945-1.453,1.46-.5-.521-.964-1.039-1.47-1.507a1.373,1.373,0,1,0-1.853,2.028c.465.461.927.924,1.4,1.4-.5.495-.965.958-1.429,1.426a1.329,1.329,0,0,0-.376,1.337,1.364,1.364,0,0,0,2.288.637C363.412,330.4,363.873,329.9,364.405,329.352Z"
                    transform="translate(-160.502 -160.298)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_22080"
                    data-name="Path 22080"
                    d="M199.97,142.962c.192.06.385.118.576.179a3.8,3.8,0,0,1,2.652,3.58c.015,2.55.007,5.1,0,7.65a1.048,1.048,0,0,1-1.045,1.12,1.029,1.029,0,0,1-1.042-1.116c-.007-2.42,0-4.84,0-7.261,0-.162,0-.325-.016-.486a1.719,1.719,0,0,0-1.666-1.591c-.1,0-.194,0-.292,0h-15.4a1.781,1.781,0,0,0-1.989,2q0,10.453,0,20.907a1.782,1.782,0,0,0,2,1.99q3.663,0,7.325,0a1.035,1.035,0,0,1,.3,2.03,1.263,1.263,0,0,1-.351.04c-2.55,0-5.1.008-7.65,0a3.787,3.787,0,0,1-3.7-3.712q-.007-10.81,0-21.62a3.8,3.8,0,0,1,3.031-3.64,1.891,1.891,0,0,0,.207-.07Z"
                    transform="translate(0)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_22081"
                    data-name="Path 22081"
                    d="M256.98,276.005q-2.186,0-4.372,0a1.044,1.044,0,0,1-1.139-1.146,4.426,4.426,0,0,1,.407-2.3,3.616,3.616,0,0,1,3.113-2.062c1.335-.065,2.678-.066,4.013,0a3.769,3.769,0,0,1,3.543,3.664c.009.259.009.518,0,.777a1.042,1.042,0,0,1-1.1,1.065Q259.215,276.008,256.98,276.005Z"
                    transform="translate(-65.57 -116.447)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_22082"
                    data-name="Path 22082"
                    d="M286.2,196.422a2.766,2.766,0,1,1,2.808-2.607A2.758,2.758,0,0,1,286.2,196.422Z"
                    transform="translate(-94.814 -43.779)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_22083"
                    data-name="Path 22083"
                    d="M247.282,360.618c-.917,0-1.834,0-2.751,0a1.037,1.037,0,1,1-.016-2.071q2.783,0,5.566,0a1.038,1.038,0,1,1-.017,2.071C249.138,360.62,248.21,360.618,247.282,360.618Z"
                    transform="translate(-58.232 -196.911)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_22084"
                    data-name="Path 22084"
                    d="M246.892,406.449c.8,0,1.6,0,2.4,0a1.036,1.036,0,1,1,0,2.07q-2.4,0-4.795,0a1.035,1.035,0,1,1,0-2.07C245.293,406.446,246.093,406.449,246.892,406.449Z"
                    transform="translate(-58.232 -240.664)"
                    fill="url(#linear-gradient)"
                  />
                </g>
              </svg>

              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Reject Profile
                </div>
              </div>
            </div>
            <InputBox
              variant="select"
              placeholder="Subject"
              type="text"
              options={[
                {
                  value: "Reasons",
                  label: "Reasons",
                },
                {
                  value: "Reasons 2",
                  label: "Reasons 2",
                },
                {
                  value: "Reasons 3",
                  label: "Reasons 3",
                },
              ]}
              isMulti={true}
            />
            <InputBox placeholder="Description" variant="textarea" />
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  setIsRejectRequest(false);
                }}
                title="close popup"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
