import React, { useEffect, useState } from "react";
import { CustomError } from "./Toasts";
import { verifyUser } from "../API/Users";
import { setCurrentUser } from "../Redux/Actions/AppActions";
import { useDispatch } from "react-redux";
import { Head } from "Components";
import { isNullOrEmpty } from "Utils/TextUtils";
import { crossSvg } from "Assets";
import passwordChanged from "../Assets/passwordChanged.png";
import Img from "react-cool-img";

export default function PasswordChanged({
  setIsPasswordChangedOpen,
  setAlertDialogVisibility,
}) {
  const [verificationCode, setVerificationCode] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  // validation
  var [verificationCodeError, setVerificationCodeError] = useState(false);
  const [verificationCodeErrorMessage, setVerificationCodeErrorMessage] =
    useState("");

  let dispatch = useDispatch();
  const email = localStorage.getItem("emailTobeVerify");
  const handleChangeValues = (event) => {
    if (isCodeInValid(event.currentTarget.value))
      setLocationErrorMessageAndVisibility(
        "Please enter 6 digit verification code"
      );
    else setLocationErrorMessageAndVisibility("");
    setVerificationCode(event.currentTarget.value);
  };

  const setLocationErrorMessageAndVisibility = (text) => {
    setVerificationCodeError((verificationCodeError = !isNullOrEmpty(text)));
    setVerificationCodeErrorMessage(text);
  };

  const isCodeInValid = (code) => {
    return code.length !== 6;
  };

  const verify = () => {
    if (isCodeInValid(verificationCode))
      setLocationErrorMessageAndVisibility(
        "Please enter 6 digit verification code"
      );
    if (!verificationCodeError) {
      setIsLoading(true);

      let payload = {
        email: email,
        code: verificationCode,
      };

      verifyUser(payload)
        .then(({ data }) => {
          if (data.success) {
            localStorage.setItem("userId", data.result.Id);
            window.localStorage.removeItem("token");
            localStorage.setItem("token", data.token);
            dispatch(setCurrentUser(data.result));
            setIsLoading(false);
            setIsPasswordChangedOpen(false);
            window.localStorage.removeItem("emailTobeVerify");
            window.location.href = "/login";
          } else {
            CustomError("Verification code is wrong, Please try again.");
          }
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          if (err.status == 553) {
            CustomError(
              "Failed to verify user because verification code is expired, Please try again later."
            );
          } else {
            CustomError("Failed to verify user. Please try again later.");
            setAlertDialogVisibility(true);
          }
          setIsLoading(false);
        });
    }
  };

  const verifyUserValidation = () => {
    verify();
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Email Verification"
        description="Email Verification"
      />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "600px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsPasswordChangedOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{ fontSize: "30px", textAlign: "center" }}
              >
                Your Password has been changed
              </div>
              <Img
                loading="lazy"
                src={passwordChanged}
                alt=""
                style={{
                  width: 150,
                  marginLeft: 195,
                }}
              />
              <button
                type="submit"
                className="header__nav__btn btn__primary"
                style={{
                  margin: "0em auto",
                  marginTop: "2em",
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  setIsPasswordChangedOpen(false);
                }}
                title="verify"
              >
                Go to AIDApro
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
