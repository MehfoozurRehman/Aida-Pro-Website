import React, { useContext, useEffect } from "react";
import { Link, navigate } from "@reach/router";
import Header from "Components/Header";
import { Head } from "Components";
import Img from "react-cool-img";
import { isUserLoggedIn } from "Utils/common";
import UserContext from "Context/UserContext";
import { mainsvgcolored1, mainsvgcolored2, mainsvgcolored3 } from "Assets";

const Homepage = ({ setIsLoginOpen }) => {
  const user = useContext(UserContext);

  useEffect(() => {
    let response = isUserLoggedIn(user);
    if (response) {
      if (user.Freelancer != null) navigate("home-freelancer");
      else if (user.JobSeeker != null) navigate("home-professional");
      else if (user.CompanyProfile != null) navigate("home-company");
    }
  }, [user]);

  return (
    <div className="home__container">
      <Head title="AIDApro" description="Home Page" />
      <Header isWelcomeScreen={true} setIsLoginOpen={setIsLoginOpen} />
      <div className="home__container__text animate__animated animate__fadeInLeft">
        Welcome to the <span>AIDApro</span> community where AI & Data
        professionals and companies meet. <br /> The place to find{" "}
        <span>professionals, vacancies and projects.</span>
      </div>
      <div className="homepage__container__wrapper">
        <Link
          to="/company"
          className="homepage__container__link homepage__container__link__1 animate__animated animate__fadeInLeft"
        >
          <div className="homepage__container__link__img__box">
            <div className="homepage__container__link__button">Company</div>
            <Img
              loading="lazy"
              src={mainsvgcolored1}
              alt="homepage__container__link__img"
              className="homepage__container__link__img"
            />
          </div>
          <div className="homepage__container__link__heading">Company</div>
        </Link>
        <Link
          to="/professional"
          className="homepage__container__link homepage__container__link__1 animate__animated animate__fadeInDown"
        >
          <div className="homepage__container__link__img__box">
            <div className="homepage__container__link__button">
              Professional
            </div>
            <Img
              loading="lazy"
              src={mainsvgcolored2}
              alt="homepage__container__link__img"
              className="homepage__container__link__img"
            />
          </div>
          <div className="homepage__container__link__heading">Professional</div>
        </Link>
        <Link
          to="/freelancer"
          className="homepage__container__link homepage__container__link__1 animate__animated animate__fadeInRight"
        >
          <div className="homepage__container__link__img__box">
            <div className="homepage__container__link__button">Freelancer</div>
            <Img
              loading="lazy"
              src={mainsvgcolored3}
              alt="homepage__container__link__img"
              className="homepage__container__link__img"
            />
          </div>
          <div className="homepage__container__link__heading">Freelancer</div>
        </Link>
      </div>
    </div>
  );
};
export default Homepage;
