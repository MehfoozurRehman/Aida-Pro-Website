import React, { useState, useEffect, useContext } from "react";
import { Link } from "@reach/router";
import { Head, ProfilePreviewCard, Avatar } from "Components";
import { useSelector, useDispatch } from "react-redux";
import UserContext from "Context/UserContext";
import { LoadingMask } from "Screens/LoadingMask";
import {
  getJobSekkerById,
  getFreelancerById,
  getCompanyById,
} from "../Redux/Actions/AppActions";
import Img from "react-cool-img";
import Logout from "Components/Logout";
import {
  FreelancerProfileDetailsSvg,
  profilePreviewAvatarSvg,
  profilePreviewEmailSvg,
  profileSelected,
} from "Assets";
import { calculateAge } from "Utils/common";

export default function PersonalDetailsPreview({ isOn, setDontShowSidebar }) {
  let { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);
  const { company } = useSelector((state) => state.company);

  const [isLoading, setIsLoading] = useState(true);
  const [initialValues, setInitialValues] = useState();
  const [img, setImg] = useState(null);
  useEffect(() => {
    // setDontShowSidebar(false);
    setImg(
      isOn === "professional"
        ? jobsekker.personalDetailsJobSeekerImage
        : isOn === "freelancer"
        ? freelancer.personalDetailsFreelancerImage
        : company.LogoUrl
    );
  });
  const [fileString, setFileString] = useState();
  const [file, setFile] = useState();
  const [readyToSave, setReadyToSave] = useState();
  const [autocomplete, setAutocomplete] = useState();
  const [map, setMap] = useState();
  const [zoom, setZoom] = useState(8);
  const [center, setCenter] = useState({ lat: 33.596041, lng: 73.005094 });

  const user = useContext(UserContext);
  let dispatch = useDispatch();
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
    setIsLoading(true);

    if (isOn === "professional") {
      dispatch(getJobSekkerById(user.JobSeekerId));
    } else if (isOn === "company") {
      dispatch(getCompanyById(user.CompanyId));
    } else {
      dispatch(getFreelancerById(user.FreelancerId));
    }
  }, []);

  const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (error) {
      // console.log("Error: ", error);
    };
  };

  const fileSelectedHandler = () => {
    try {
      getBase64(img, (result) => {
        setFileString(result);
        setFile(result);
        setReadyToSave(true);
      });
    } catch (err) {
      // console.log(err);
    }
  };

  useEffect(() => {
    if (img) {
      fileSelectedHandler();
    }
  }, [img]);

  useEffect(() => {
    setInitialValues(jobsekker);
    setIsLoading(false);
  }, [jobsekker]);

  useEffect(() => {
    setInitialValues(freelancer);
    setIsLoading(false);
  }, [freelancer]);

  useEffect(() => {
    setInitialValues(company);
    setIsLoading(false);
  }, [company]);

  const onPlaceChanged = () => {
    if (autocomplete !== null || autocomplete !== undefined) {
      let place = autocomplete.getPlace();
      if (place.geometry) {
        let lat = place.geometry.location.lat();
        let lng = place.geometry.location.lng();
        setCenter({ lat: lat, lng: lng });
        setZoom(12);
      }
    } else {
      // console.log("Autocomplete is not loaded yet!");
    }
  };

  const handleMapOnLoad = (map) => {
    setMap(map);
  };

  const handleCenterChange = (setFieldValue) => {
    if (map) {
      setFieldValue("lat", map.getCenter().lat());
      setFieldValue("lng", map.getCenter().lng());
      setFieldValue("zoom", map.getZoom());
    }
  };

  return (
    <>
      {!isLoading ? (
        <>
          <Head
            title={
              isOn === "company"
                ? "AIDApro | Company Personal Details"
                : isOn === "freelancer"
                ? "AIDApro | Freealncer Personal Details"
                : "AIDApro | Professional Personal Details"
            }
            description={
              isOn === "company"
                ? "Company Personal Details"
                : isOn === "freelancer"
                ? "Freealncer Personal Details"
                : "Professional Personal Details"
            }
          />
          <div className="dashboard__company__container__profile__preview">
            <div className="dashboard__company__container__profile__preview__left">
              <div
                className="dashboard__company__container__heading__wrapper"
                style={
                  isOn === "company"
                    ? { marginBottom: 10 }
                    : { marginBottom: 10 }
                }
              >
                <Img
                  loading="lazy"
                  src={profileSelected}
                  alt=""
                  style={{
                    width: 30,
                    height: 30,
                    marginRight: 10,
                    marginBottom: 10,
                  }}
                />
                <div className="homepage__container__jumbotron__sub__heading">
                  {isOn === "company" ? "Company" : null}
                  <div>{isOn === "company" ? null : "Personal"} details</div>
                </div>
                <div className="homepage__container__jumbotron__button">
                  <Link
                    to={
                      isOn === "company"
                        ? "/home-company/profile-edit"
                        : isOn === "professional"
                        ? "/home-professional/personal-details"
                        : "/home-freelancer/personal-details"
                    }
                    state={{ userData: initialValues }}
                    style={{ width: 100 }}
                    className="header__nav__btn btn__secondary"
                  >
                    Edit
                  </Link>
                </div>
              </div>
              <div className="dashboard__company__container__profile__preview__wrapper">
                <div className="dashboard__company__container__heading__wrapper__content__container">
                  <div className="dashboard__company__container__heading__wrapper__content">
                    <div className="dashboard__company__container__heading__wrapper__content__left">
                      <div className="dashboard__company__container__heading__wrapper__content__left__img">
                        <Avatar
                          // onClick={(event) => {
                          //   if (img != null && img != undefined && img != "") {
                          //     window.open(
                          //       process.env.REACT_APP_BASEURL.concat(img),
                          //       "_Blank"
                          //     );
                          //   }
                          // }}
                          onClick={() => {}}
                          userPic={
                            img != null && img != undefined && img != ""
                              ? process.env.REACT_APP_BASEURL.concat(img) +
                                "?" +
                                new Date()
                              : null
                          }
                        />
                      </div>
                      <Img
                        loading="lazy"
                        src={profilePreviewAvatarSvg}
                        alt="profilePreviewAvatarSvg"
                        className="dashboard__company__container__heading__wrapper__content__left__svg"
                      />
                    </div>
                    <div className="dashboard__company__container__heading__wrapper__content__right">
                      <div className="dashboard__company__container__heading__wrapper__content__left__headings">
                        <ProfilePreviewCard
                          svg={
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="13.419"
                              height="17.918"
                              viewBox="0 0 13.419 17.918"
                            >
                              <g
                                id="Group_1699"
                                data-name="Group 1699"
                                transform="translate(-514.783 -232.859)"
                              >
                                <path
                                  id="Path_285"
                                  data-name="Path 285"
                                  d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                                  transform="translate(376.159 66.29)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_286"
                                  data-name="Path 286"
                                  d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                                  transform="translate(409.691 -81.998)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_287"
                                  data-name="Path 287"
                                  d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                                  transform="translate(519.365 232.859)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_288"
                                  data-name="Path 288"
                                  d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                                  transform="translate(376.159 175.843)"
                                  fill="#374957"
                                />
                              </g>
                            </svg>
                          }
                          heading={
                            isOn === "company" ? "Company Name" : "First Name"
                          }
                          subHeading={
                            isOn === "company"
                              ? initialValues.companyName
                              : initialValues
                              ? initialValues.firstName
                              : ""
                          }
                          maxLength={10}
                        />
                        <ProfilePreviewCard
                          heading={
                            isOn === "company"
                              ? "Industry / Branch"
                              : isOn === "freelancer"
                              ? "Gender"
                              : "Gender"
                          }
                          svg={
                            isOn === "company" ? (
                              <svg
                                id="Group_204"
                                data-name="Group 204"
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.43"
                                height="18.373"
                                viewBox="0 0 18.43 18.373"
                              >
                                <g id="Group_203" data-name="Group 203">
                                  <path
                                    id="Path_304"
                                    data-name="Path 304"
                                    d="M18.157,14.664h-1.8V12.427a.274.274,0,1,0-.547,0v6.194h-4.7V10.49h4.7v.66a.274.274,0,1,0,.547,0v-.934a.274.274,0,0,0-.274-.274H12.34V1.07A.274.274,0,0,0,12.066.8H6.373A.274.274,0,0,0,6.1,1.07V5.164H2.558a.274.274,0,0,0-.274.274v6.73H.274A.274.274,0,0,0,0,12.442V18.9a.274.274,0,0,0,.274.274H18.157a.274.274,0,0,0,.274-.274V14.938A.274.274,0,0,0,18.157,14.664ZM2.285,18.622H.547V12.715H2.285Zm5.693,0H2.832V17.108H7.978Zm0-3.9H5.511a.274.274,0,0,0,0,.547H7.978v1.3H2.832v-1.3h1.4a.274.274,0,0,0,0-.547h-1.4v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832V5.712H7.978Zm2.583,2.87v8.406H8.525V5.438a.274.274,0,0,0-.274-.274H7.878V4.617a.274.274,0,1,0-.547,0v.547H6.647V1.343h5.146v8.6h-.958A.274.274,0,0,0,10.561,10.216Zm7.322,8.405h-1.53v-3.41h1.53Z"
                                    transform="translate(0 -0.796)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            ) : isOn === "freelancer" ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="17.064"
                                height="17.078"
                                viewBox="0 0 17.064 17.078"
                              >
                                <path
                                  id="Path_297"
                                  data-name="Path 297"
                                  d="M13.581,0V1h1.981L12.99,3.572a4.51,4.51,0,0,0-5.543-.077,4.519,4.519,0,1,0-3.222,8.1V14.4H2.876v1H4.225v1.675h1V15.4H6.576v-1H5.226V11.593a4.5,4.5,0,0,0,2.222-.886A4.517,4.517,0,0,0,13.7,4.28L16.27,1.708V3.69h1V0ZM7.448,9.329a3.512,3.512,0,0,1,0-4.456,3.512,3.512,0,0,1,0,4.456ZM1.207,7.1a3.517,3.517,0,0,1,5.5-2.9,4.51,4.51,0,0,0,0,5.81,3.517,3.517,0,0,1-5.5-2.9Zm8.963,3.519a3.5,3.5,0,0,1-1.984-.614,4.51,4.51,0,0,0,0-5.81,3.519,3.519,0,1,1,1.984,6.424Z"
                                  transform="translate(-0.206 0)"
                                  fill="#374957"
                                />
                              </svg>
                            ) : (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="17.064"
                                height="17.078"
                                viewBox="0 0 17.064 17.078"
                              >
                                <path
                                  id="Path_297"
                                  data-name="Path 297"
                                  d="M13.581,0V1h1.981L12.99,3.572a4.51,4.51,0,0,0-5.543-.077,4.519,4.519,0,1,0-3.222,8.1V14.4H2.876v1H4.225v1.675h1V15.4H6.576v-1H5.226V11.593a4.5,4.5,0,0,0,2.222-.886A4.517,4.517,0,0,0,13.7,4.28L16.27,1.708V3.69h1V0ZM7.448,9.329a3.512,3.512,0,0,1,0-4.456,3.512,3.512,0,0,1,0,4.456ZM1.207,7.1a3.517,3.517,0,0,1,5.5-2.9,4.51,4.51,0,0,0,0,5.81,3.517,3.517,0,0,1-5.5-2.9Zm8.963,3.519a3.5,3.5,0,0,1-1.984-.614,4.51,4.51,0,0,0,0-5.81,3.519,3.519,0,1,1,1.984,6.424Z"
                                  transform="translate(-0.206 0)"
                                  fill="#374957"
                                />
                              </svg>
                            )
                          }
                          subHeading={
                            isOn === "company"
                              ? initialValues.companyBranch != null
                                ? initialValues.companyBranch.label
                                : "Not specified"
                              : initialValues
                              ? initialValues.gender
                                ? initialValues.gender.label
                                : "Not specified"
                              : ""
                          }
                          maxLength={18}
                        />
                      </div>
                      <div className="dashboard__company__container__heading__wrapper__content__right__headings">
                        <ProfilePreviewCard
                          svg={
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="13.419"
                              height="17.918"
                              viewBox="0 0 13.419 17.918"
                            >
                              <g id="name" transform="translate(3.582 -1)">
                                <path
                                  id="Path_285"
                                  data-name="Path 285"
                                  d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                                  transform="translate(-142.206 -165.569)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_286"
                                  data-name="Path 286"
                                  d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                                  transform="translate(-108.674 -313.857)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_287"
                                  data-name="Path 287"
                                  d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                                  transform="translate(1 1)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_288"
                                  data-name="Path 288"
                                  d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                                  transform="translate(-142.206 -56.016)"
                                  fill="#374957"
                                />
                              </g>
                            </svg>
                          }
                          heading={
                            isOn === "company" ? "Contact Person" : "Last Name"
                          }
                          subHeading={
                            isOn === "company"
                              ? initialValues.companyContantPerson
                              : initialValues
                              ? initialValues.lastName
                              : ""
                          }
                          maxLength={17}
                        />
                        <ProfilePreviewCard
                          heading={
                            isOn === "company"
                              ? "No. of Employee"
                              : isOn === "freelancer"
                              ? "Age"
                              : "Age"
                          }
                          svg={
                            isOn === "company" ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="13.419"
                                height="17.918"
                                viewBox="0 0 13.419 17.918"
                              >
                                <g id="name" transform="translate(3.582 -1)">
                                  <path
                                    id="Path_285"
                                    data-name="Path 285"
                                    d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                                    transform="translate(-142.206 -165.569)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_286"
                                    data-name="Path 286"
                                    d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                                    transform="translate(-108.674 -313.857)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_287"
                                    data-name="Path 287"
                                    d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                                    transform="translate(1 1)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_288"
                                    data-name="Path 288"
                                    d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                                    transform="translate(-142.206 -56.016)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            ) : isOn === "freelancer" ? (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.471"
                                height="17.888"
                                viewBox="0 0 18.471 17.888"
                              >
                                <g
                                  id="Group_1748"
                                  data-name="Group 1748"
                                  transform="translate(-977.281 -502.999)"
                                >
                                  <path
                                    id="Path_298"
                                    data-name="Path 298"
                                    d="M15.118,22.72A8.4,8.4,0,1,1,13.389,9.892a.271.271,0,0,0,.287-.46,8.949,8.949,0,1,0,1.839,13.656.271.271,0,0,0-.4-.368Z"
                                    transform="translate(977.281 494.92)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_299"
                                    data-name="Path 299"
                                    d="M59.516,57.132a.271.271,0,0,0-.249-.163h-.382a8.932,8.932,0,0,0-3.231-5.817.271.271,0,1,0-.342.42,8.388,8.388,0,0,1,3.061,5.694.271.271,0,0,0,.268.245l-.864.92-.864-.92H57.1a.273.273,0,0,0,.269-.3,7.407,7.407,0,1,0-13.658,4.774.271.271,0,1,0,.461-.285,6.865,6.865,0,1,1,12.616-4.728h-.5a.273.273,0,0,0-.2.457l1.491,1.587a.273.273,0,0,0,.4,0l1.491-1.587A.271.271,0,0,0,59.516,57.132Z"
                                    transform="translate(936.213 454.316)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_300"
                                    data-name="Path 300"
                                    d="M103.795,312.093h-1.607a.271.271,0,0,0-.257.184,6.867,6.867,0,0,1-11.732,2.24.271.271,0,1,0-.413.351,7.409,7.409,0,0,0,12.593-2.233h1.052a8.346,8.346,0,0,1-1.163,2.315.271.271,0,1,0,.441.315,8.884,8.884,0,0,0,1.347-2.83.271.271,0,0,0-.262-.343Z"
                                    transform="translate(890.391 201.643)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_301"
                                    data-name="Path 301"
                                    d="M100.938,206.011a.271.271,0,0,0,.35-.157l.259-.68h1.726l.256.679a.271.271,0,1,0,.507-.191l-1.3-3.432,0-.007a.349.349,0,0,0-.324-.217h0a.349.349,0,0,0-.324.217l0,.006-1.307,3.432A.271.271,0,0,0,100.938,206.011Zm1.476-3.113.655,1.734h-1.315Z"
                                    transform="translate(880.153 306.995)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_302"
                                    data-name="Path 302"
                                    d="M318.21,202.988a.271.271,0,1,0,0-.542h-1.453a.271.271,0,0,0-.271.271v3.449a.271.271,0,0,0,.271.271h1.453a.271.271,0,1,0,0-.542h-1.182v-1.183H318.1a.271.271,0,1,0,0-.542h-1.075v-1.183h1.182Z"
                                    transform="translate(672.214 306.555)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_303"
                                    data-name="Path 303"
                                    d="M198.346,202.548a1.462,1.462,0,0,1,.821.251.271.271,0,1,0,.3-.449,2.012,2.012,0,1,0-1.125,3.68,1.821,1.821,0,0,0,1.774-2.012.271.271,0,0,0-.271-.271h-1.041a.271.271,0,0,0,0,.542h.755a1.216,1.216,0,0,1-1.217,1.2,1.47,1.47,0,1,1,0-2.939Z"
                                    transform="translate(788.03 306.994)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            ) : (
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.471"
                                height="17.888"
                                viewBox="0 0 18.471 17.888"
                              >
                                <g
                                  id="Group_1748"
                                  data-name="Group 1748"
                                  transform="translate(-977.281 -502.999)"
                                >
                                  <path
                                    id="Path_298"
                                    data-name="Path 298"
                                    d="M15.118,22.72A8.4,8.4,0,1,1,13.389,9.892a.271.271,0,0,0,.287-.46,8.949,8.949,0,1,0,1.839,13.656.271.271,0,0,0-.4-.368Z"
                                    transform="translate(977.281 494.92)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_299"
                                    data-name="Path 299"
                                    d="M59.516,57.132a.271.271,0,0,0-.249-.163h-.382a8.932,8.932,0,0,0-3.231-5.817.271.271,0,1,0-.342.42,8.388,8.388,0,0,1,3.061,5.694.271.271,0,0,0,.268.245l-.864.92-.864-.92H57.1a.273.273,0,0,0,.269-.3,7.407,7.407,0,1,0-13.658,4.774.271.271,0,1,0,.461-.285,6.865,6.865,0,1,1,12.616-4.728h-.5a.273.273,0,0,0-.2.457l1.491,1.587a.273.273,0,0,0,.4,0l1.491-1.587A.271.271,0,0,0,59.516,57.132Z"
                                    transform="translate(936.213 454.316)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_300"
                                    data-name="Path 300"
                                    d="M103.795,312.093h-1.607a.271.271,0,0,0-.257.184,6.867,6.867,0,0,1-11.732,2.24.271.271,0,1,0-.413.351,7.409,7.409,0,0,0,12.593-2.233h1.052a8.346,8.346,0,0,1-1.163,2.315.271.271,0,1,0,.441.315,8.884,8.884,0,0,0,1.347-2.83.271.271,0,0,0-.262-.343Z"
                                    transform="translate(890.391 201.643)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_301"
                                    data-name="Path 301"
                                    d="M100.938,206.011a.271.271,0,0,0,.35-.157l.259-.68h1.726l.256.679a.271.271,0,1,0,.507-.191l-1.3-3.432,0-.007a.349.349,0,0,0-.324-.217h0a.349.349,0,0,0-.324.217l0,.006-1.307,3.432A.271.271,0,0,0,100.938,206.011Zm1.476-3.113.655,1.734h-1.315Z"
                                    transform="translate(880.153 306.995)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_302"
                                    data-name="Path 302"
                                    d="M318.21,202.988a.271.271,0,1,0,0-.542h-1.453a.271.271,0,0,0-.271.271v3.449a.271.271,0,0,0,.271.271h1.453a.271.271,0,1,0,0-.542h-1.182v-1.183H318.1a.271.271,0,1,0,0-.542h-1.075v-1.183h1.182Z"
                                    transform="translate(672.214 306.555)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_303"
                                    data-name="Path 303"
                                    d="M198.346,202.548a1.462,1.462,0,0,1,.821.251.271.271,0,1,0,.3-.449,2.012,2.012,0,1,0-1.125,3.68,1.821,1.821,0,0,0,1.774-2.012.271.271,0,0,0-.271-.271h-1.041a.271.271,0,0,0,0,.542h.755a1.216,1.216,0,0,1-1.217,1.2,1.47,1.47,0,1,1,0-2.939Z"
                                    transform="translate(788.03 306.994)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            )
                          }
                          subHeading={
                            isOn === "company"
                              ? initialValues.companyNoOfEmp != null
                                ? initialValues.companyNoOfEmp
                                : "Not specified"
                              : initialValues
                              ? initialValues.age
                                ? calculateAge(initialValues.age)
                                : "Not specified"
                              : ""
                          }
                          maxLength={10}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="dashboard__company__container__profile__preview__center"
                  style={{
                    backgroundColor: "#ffffff",
                    boxShadow: "0px 0px 30px rgba(0,0,0,.05)",
                  }}
                >
                  <div className="dashboard__company__container__profile__preview__left__content__wrapper__blue">
                    <Img
                      loading="lazy"
                      src={profilePreviewEmailSvg}
                      alt="profilePreviewEmailSvg"
                      className="dashboard__company__container__profile__preview__left__content__wrapper__svg"
                    />
                    <ProfilePreviewCard
                      // is="whiteText"
                      svg={
                        <svg
                          id="Group_202"
                          data-name="Group 202"
                          xmlns="http://www.w3.org/2000/svg"
                          width="17.919"
                          height="11.946"
                          viewBox="0 0 17.919 11.946"
                        >
                          <path
                            id="Path_292"
                            data-name="Path 292"
                            d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                            transform="translate(0 -85.333)"
                            fill="#303043"
                          />
                        </svg>
                      }
                      heading="Email"
                      subHeading={
                        isOn === "company"
                          ? initialValues.companyEmailAddress
                          : initialValues
                          ? initialValues.emailAddress
                            ? initialValues.emailAddress
                            : "Not specified"
                          : ""
                      }
                      maxLength={50}
                    />

                    <ProfilePreviewCard
                      // is="whiteText"
                      svg={
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="15.428"
                          height="15.45"
                          viewBox="0 0 15.428 15.45"
                        >
                          <g id="phone-call" transform="translate(-0.344 0)">
                            <g
                              id="Group_201"
                              data-name="Group 201"
                              transform="translate(0.344 0)"
                            >
                              <path
                                id="Path_289"
                                data-name="Path 289"
                                d="M12.544,36.083a1.52,1.52,0,0,0-1.1-.505,1.571,1.571,0,0,0-1.115.5L9.3,37.107c-.085-.046-.17-.088-.251-.13-.117-.059-.228-.114-.323-.173a11.2,11.2,0,0,1-2.684-2.446,6.607,6.607,0,0,1-.88-1.389c.267-.245.515-.5.757-.743.091-.091.183-.186.274-.277a1.5,1.5,0,0,0,0-2.257L5.3,28.8c-.1-.1-.205-.205-.3-.31-.2-.2-.4-.411-.613-.607a1.544,1.544,0,0,0-1.092-.479,1.6,1.6,0,0,0-1.109.479l-.007.007L1.066,29.01a2.386,2.386,0,0,0-.708,1.516,5.716,5.716,0,0,0,.417,2.42A14.041,14.041,0,0,0,3.27,37.107a15.346,15.346,0,0,0,5.11,4,7.961,7.961,0,0,0,2.87.848c.068,0,.14.007.205.007a2.457,2.457,0,0,0,1.882-.809c0-.007.01-.01.013-.016a7.413,7.413,0,0,1,.571-.59c.14-.134.284-.274.424-.421A1.627,1.627,0,0,0,14.836,39a1.567,1.567,0,0,0-.5-1.118Zm1.167,3.434s0,0,0,0c-.127.137-.258.261-.4.4a8.574,8.574,0,0,0-.629.652,1.572,1.572,0,0,1-1.226.518c-.049,0-.1,0-.15,0a7.073,7.073,0,0,1-2.544-.763,14.485,14.485,0,0,1-4.813-3.77A13.239,13.239,0,0,1,1.6,32.64,4.656,4.656,0,0,1,1.238,30.6a1.5,1.5,0,0,1,.45-.968L2.8,28.518a.741.741,0,0,1,.5-.232.7.7,0,0,1,.476.228l.01.01c.2.186.388.378.587.584.1.1.205.209.31.316l.89.89a.622.622,0,0,1,0,1.011c-.095.095-.186.189-.28.28-.274.28-.535.541-.818.8-.007.007-.013.01-.016.016a.665.665,0,0,0-.17.74l.01.029a7.147,7.147,0,0,0,1.053,1.719l0,0a11.969,11.969,0,0,0,2.9,2.635,4.451,4.451,0,0,0,.4.218c.117.059.228.114.323.173.013.007.026.016.039.023a.707.707,0,0,0,.323.082.7.7,0,0,0,.5-.225L10.943,36.7a.738.738,0,0,1,.492-.245.664.664,0,0,1,.47.238l.007.007,1.8,1.8A.645.645,0,0,1,13.711,39.517Z"
                                transform="translate(-0.344 -26.512)"
                                fill="#303043"
                              />
                              <path
                                id="Path_290"
                                data-name="Path 290"
                                d="M245.306,86.8a4.2,4.2,0,0,1,3.417,3.417.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007.441.441,0,0,0,.362-.509,5.075,5.075,0,0,0-4.135-4.135.443.443,0,0,0-.509.359A.435.435,0,0,0,245.306,86.8Z"
                                transform="translate(-236.968 -83.123)"
                                fill="#303043"
                              />
                              <path
                                id="Path_291"
                                data-name="Path 291"
                                d="M256.1,6.816A8.356,8.356,0,0,0,249.287.007a.44.44,0,1,0-.143.867,7.464,7.464,0,0,1,6.085,6.085.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007A.432.432,0,0,0,256.1,6.816Z"
                                transform="translate(-240.674 0)"
                                fill="#303043"
                              />
                            </g>
                          </g>
                        </svg>
                      }
                      heading="Phone No"
                      subHeading={
                        isOn === "company"
                          ? initialValues.companyCellNo != null &&
                            initialValues.companyCellNo != ""
                            ? "+" + initialValues.companyCellNo
                            : "Not specified"
                          : initialValues
                          ? initialValues.phoneNo
                            ? "+ " + initialValues.phoneNo
                            : "Not specified"
                          : ""
                      }
                      maxLength={20}
                    />
                  </div>
                </div>
              </div>
              <div style={{ height: "1em" }}></div>
              <div
                className="dashboard__company__container__profile__preview__bottom"
                style={{
                  backgroundColor: "#ffffff",
                  boxShadow: "0px 0px 30px rgba(0,0,0,.05)",
                  flexDirection: "column",
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="42"
                  height="58"
                  viewBox="0 0 42 58"
                  style={{ marginTop: 60 }}
                >
                  <defs>
                    <linearGradient
                      id="linear-gradient"
                      x1="0.5"
                      x2="0.5"
                      y2="1"
                      gradientUnits="objectBoundingBox"
                    >
                      <stop offset="0" stopColor="#f6a938" />
                      <stop offset="1" stopColor="#f5833c" />
                    </linearGradient>
                    <linearGradient
                      id="linear-gradient-3"
                      x1="0.5"
                      x2="0.5"
                      y2="1"
                      gradientUnits="objectBoundingBox"
                    >
                      <stop offset="0" stopColor="#0ee1a3" />
                      <stop offset="1" stopColor="#0ca69d" />
                    </linearGradient>
                  </defs>
                  <g id="Work" transform="translate(-11 -3)">
                    <path
                      id="Path_21366"
                      data-name="Path 21366"
                      d="M18,0c9.941,0,18,2.239,18,5s-8.059,5-18,5S0,7.761,0,5,8.059,0,18,0Z"
                      transform="translate(14 51)"
                      fill="url(#linear-gradient)"
                    />
                    <path
                      id="Path_21359"
                      data-name="Path 21359"
                      d="M53,24c0,7.8-9.5,20.03-15.72,27.21C34.25,54.7,32,57,32,57s-2.25-2.3-5.28-5.79C20.5,44.03,11,31.8,11,24a21,21,0,0,1,42,0Z"
                      fill="url(#linear-gradient)"
                    />
                    <circle
                      id="Ellipse_383"
                      data-name="Ellipse 383"
                      cx="16"
                      cy="16"
                      r="16"
                      transform="translate(16 8)"
                      fill="#d1e7f8"
                    />
                    <path
                      id="Path_21360"
                      data-name="Path 21360"
                      d="M48,24A15.98,15.98,0,0,1,21.265,35.839q-2.214,1.242-4.533,2.309A122.307,122.307,0,0,0,26.72,51.21C29.75,54.7,32,57,32,57s2.25-2.3,5.28-5.79C43.5,44.03,53,31.8,53,24A20.928,20.928,0,0,0,46.927,9.235q-1.206,2.343-2.592,4.575A15.932,15.932,0,0,1,48,24Z"
                      fill="#eb8a2d"
                    />
                    <path
                      id="Path_21361"
                      data-name="Path 21361"
                      d="M48,24a15.932,15.932,0,0,0-3.665-10.19,64.29,64.29,0,0,1-23.07,22.029A15.98,15.98,0,0,0,48,24Z"
                      fill="#b7cad9"
                    />
                    <path
                      id="Path_21362"
                      data-name="Path 21362"
                      d="M41,24v8a2.006,2.006,0,0,1-2,2H25a2.006,2.006,0,0,1-2-2V24Z"
                      fill="url(#linear-gradient-3)"
                    />
                    <path
                      id="Path_21363"
                      data-name="Path 21363"
                      d="M43,19v5H21V19a2.006,2.006,0,0,1,2-2H41a2.006,2.006,0,0,1,2,2Z"
                      fill="url(#linear-gradient)"
                    />
                    <path
                      id="Path_21364"
                      data-name="Path 21364"
                      d="M34,24v2a1,1,0,0,1-1,1H31a1,1,0,0,1-1-1V23a1,1,0,0,1,1-1h2a1,1,0,0,1,1,1Z"
                      fill="#ff9811"
                    />
                    <path
                      id="Path_21365"
                      data-name="Path 21365"
                      d="M35,12H29a2,2,0,0,0-2,2v3h2V14h6v3h2V14A2,2,0,0,0,35,12Z"
                      fill="#603913"
                    />
                  </g>
                </svg>

                <div
                  className="dashboard__company__container__profile__preview__left__content__wrapper"
                  style={{ paddingTop: "0em" }}
                >
                  <div className="dashboard__company__container__profile__preview__left__content__wrapper__left">
                    {isOn === "company" ? null : isOn === "freelancer" ? (
                      <div
                        style={{ marginTop: "1em" }}
                        className="dashboard__company__container__profile__preview__content"
                      >
                        <div className="dashboard__company__container__profile__preview__content">
                          <div className="dashboard__company__container__profile__preview__content__svg">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="18.052"
                              height="18.051"
                              viewBox="0 0 18.052 18.051"
                            >
                              <g id="translation" transform="translate(0 0)">
                                <path
                                  id="Path_281"
                                  data-name="Path 281"
                                  d="M16.571,6.346H11.612c.016-.174.024-.35.024-.528A5.82,5.82,0,0,0,2.911.777a.353.353,0,1,0,.353.611,5.113,5.113,0,1,1,2.554,9.543A5.056,5.056,0,0,1,2.612,9.8a.353.353,0,0,0-.325-.062l-.936.245.367-.806a.352.352,0,0,0-.036-.354,5.116,5.116,0,0,1-.291-5.566A.353.353,0,0,0,.781,2.9,5.822,5.822,0,0,0,.992,9.068l-.59,1.3a.352.352,0,0,0,.41.487l1.5-.392a5.817,5.817,0,0,0,4.317,1.119v3.087a1.482,1.482,0,0,0,1.481,1.481H13.1l2.23,1.824A.353.353,0,0,0,15.9,17.7V16.147h.67a1.482,1.482,0,0,0,1.481-1.481V7.827a1.482,1.482,0,0,0-1.481-1.481Zm.776,8.321a.777.777,0,0,1-.776.776H15.548a.353.353,0,0,0-.353.353v1.16L13.454,15.53c-.1-.088-.18-.088-.369-.088H8.109a.777.777,0,0,1-.776-.776V11.435A5.838,5.838,0,0,0,11.5,7.051h5.067a.776.776,0,0,1,.776.776Zm0,0"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_282"
                                  data-name="Path 282"
                                  d="M291.348,253.555a.352.352,0,0,0-.15-.671h-.144l-1.137-2.591a.353.353,0,0,0-.646,0l-1.137,2.591h-.144a.352.352,0,0,0-.15.671l-.423.965a.353.353,0,1,0,.646.283l.533-1.215h2l.533,1.215a.353.353,0,1,0,.646-.283Zm-2.444-.671.69-1.572.69,1.572Zm0,0"
                                  transform="translate(-277.254 -241.265)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_283"
                                  data-name="Path 283"
                                  d="M45.755,46.1a.353.353,0,1,0-.249-.1A.356.356,0,0,0,45.755,46.1Zm0,0"
                                  transform="translate(-43.802 -43.798)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_284"
                                  data-name="Path 284"
                                  d="M100.316,90.348a.353.353,0,1,0,.339.618,4.038,4.038,0,0,0,1.745-1.893,4.038,4.038,0,0,0,1.745,1.893.353.353,0,1,0,.339-.618,3.334,3.334,0,0,1-1.694-2.426h1.565a.353.353,0,1,0,0-.705h-1.6V85.747a.353.353,0,1,0-.705,0v1.469h-1.274a.353.353,0,0,0,0,.705h1.236A3.334,3.334,0,0,1,100.316,90.348Zm0,0"
                                  transform="translate(-96.603 -82.384)"
                                  fill="#374957"
                                />
                              </g>
                            </svg>
                          </div>
                          <div className="dashboard__company__container__profile__preview__content__text">
                            <div className="dashboard__company__container__profile__preview__content__text__upper">
                              {"Languages"}
                            </div>
                            <div className="dashboard__company__container__profile__preview__content__text__bottom">
                              {initialValues.language != null
                                ? initialValues.language
                                    .map((item) => item.label)
                                    .join(", ")
                                : "Not specified"}
                              {/* {initialValues.language != null
                                ? initialValues.language.map(
                                    (element, index) => (
                                      <div>{element.label}</div>
                                    )
                                  )
                                : "Not specified"} */}
                            </div>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div
                        style={{ marginTop: "1em" }}
                        className="dashboard__company__container__profile__preview__content"
                      >
                        <div className="dashboard__company__container__profile__preview__content">
                          <div className="dashboard__company__container__profile__preview__content__svg">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="18.052"
                              height="18.051"
                              viewBox="0 0 18.052 18.051"
                            >
                              <g id="translation" transform="translate(0 0)">
                                <path
                                  id="Path_281"
                                  data-name="Path 281"
                                  d="M16.571,6.346H11.612c.016-.174.024-.35.024-.528A5.82,5.82,0,0,0,2.911.777a.353.353,0,1,0,.353.611,5.113,5.113,0,1,1,2.554,9.543A5.056,5.056,0,0,1,2.612,9.8a.353.353,0,0,0-.325-.062l-.936.245.367-.806a.352.352,0,0,0-.036-.354,5.116,5.116,0,0,1-.291-5.566A.353.353,0,0,0,.781,2.9,5.822,5.822,0,0,0,.992,9.068l-.59,1.3a.352.352,0,0,0,.41.487l1.5-.392a5.817,5.817,0,0,0,4.317,1.119v3.087a1.482,1.482,0,0,0,1.481,1.481H13.1l2.23,1.824A.353.353,0,0,0,15.9,17.7V16.147h.67a1.482,1.482,0,0,0,1.481-1.481V7.827a1.482,1.482,0,0,0-1.481-1.481Zm.776,8.321a.777.777,0,0,1-.776.776H15.548a.353.353,0,0,0-.353.353v1.16L13.454,15.53c-.1-.088-.18-.088-.369-.088H8.109a.777.777,0,0,1-.776-.776V11.435A5.838,5.838,0,0,0,11.5,7.051h5.067a.776.776,0,0,1,.776.776Zm0,0"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_282"
                                  data-name="Path 282"
                                  d="M291.348,253.555a.352.352,0,0,0-.15-.671h-.144l-1.137-2.591a.353.353,0,0,0-.646,0l-1.137,2.591h-.144a.352.352,0,0,0-.15.671l-.423.965a.353.353,0,1,0,.646.283l.533-1.215h2l.533,1.215a.353.353,0,1,0,.646-.283Zm-2.444-.671.69-1.572.69,1.572Zm0,0"
                                  transform="translate(-277.254 -241.265)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_283"
                                  data-name="Path 283"
                                  d="M45.755,46.1a.353.353,0,1,0-.249-.1A.356.356,0,0,0,45.755,46.1Zm0,0"
                                  transform="translate(-43.802 -43.798)"
                                  fill="#374957"
                                />
                                <path
                                  id="Path_284"
                                  data-name="Path 284"
                                  d="M100.316,90.348a.353.353,0,1,0,.339.618,4.038,4.038,0,0,0,1.745-1.893,4.038,4.038,0,0,0,1.745,1.893.353.353,0,1,0,.339-.618,3.334,3.334,0,0,1-1.694-2.426h1.565a.353.353,0,1,0,0-.705h-1.6V85.747a.353.353,0,1,0-.705,0v1.469h-1.274a.353.353,0,0,0,0,.705h1.236A3.334,3.334,0,0,1,100.316,90.348Zm0,0"
                                  transform="translate(-96.603 -82.384)"
                                  fill="#374957"
                                />
                              </g>
                            </svg>
                          </div>
                          <div className="dashboard__company__container__profile__preview__content__text">
                            <div className="dashboard__company__container__profile__preview__content__text__upper">
                              {"Languages"}
                            </div>
                            <div className="dashboard__company__container__profile__preview__content__text__bottom">
                              {initialValues.language != null
                                ? initialValues.language
                                    .map((item) => item.label)
                                    .join(", ")
                                : "Not specified"}
                              {/* {initialValues.language != null
                                ? initialValues.language.map(
                                  (element, index) => (
                                    <div>{element.label.concat(", ")}</div>
                                  )
                                )
                                : "Not specified"} */}
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                    <div
                      style={{ marginTop: "1em" }}
                      className="dashboard__company__container__profile__preview__content"
                    >
                      <div className="dashboard__company__container__profile__preview__content">
                        <div className="dashboard__company__container__profile__preview__content__svg">
                          <svg
                            id="Group_204"
                            data-name="Group 204"
                            xmlns="http://www.w3.org/2000/svg"
                            width="18.43"
                            height="18.373"
                            viewBox="0 0 18.43 18.373"
                          >
                            <g id="Group_203" data-name="Group 203">
                              <path
                                id="Path_304"
                                data-name="Path 304"
                                d="M18.157,14.664h-1.8V12.427a.274.274,0,1,0-.547,0v6.194h-4.7V10.49h4.7v.66a.274.274,0,1,0,.547,0v-.934a.274.274,0,0,0-.274-.274H12.34V1.07A.274.274,0,0,0,12.066.8H6.373A.274.274,0,0,0,6.1,1.07V5.164H2.558a.274.274,0,0,0-.274.274v6.73H.274A.274.274,0,0,0,0,12.442V18.9a.274.274,0,0,0,.274.274H18.157a.274.274,0,0,0,.274-.274V14.938A.274.274,0,0,0,18.157,14.664ZM2.285,18.622H.547V12.715H2.285Zm5.693,0H2.832V17.108H7.978Zm0-3.9H5.511a.274.274,0,0,0,0,.547H7.978v1.3H2.832v-1.3h1.4a.274.274,0,0,0,0-.547h-1.4v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832V5.712H7.978Zm2.583,2.87v8.406H8.525V5.438a.274.274,0,0,0-.274-.274H7.878V4.617a.274.274,0,1,0-.547,0v.547H6.647V1.343h5.146v8.6h-.958A.274.274,0,0,0,10.561,10.216Zm7.322,8.405h-1.53v-3.41h1.53Z"
                                transform="translate(0 -0.796)"
                                fill="#374957"
                              />
                            </g>
                          </svg>
                        </div>
                        <div className="dashboard__company__container__profile__preview__content__text">
                          <div className="dashboard__company__container__profile__preview__content__text__upper">
                            Address
                          </div>
                          <div className="dashboard__company__container__profile__preview__content__text__bottom">
                            <div>
                              {isOn === "company"
                                ? initialValues.companyAddress != null
                                  ? initialValues.companyAddress
                                  : "Not specified"
                                : initialValues
                                ? initialValues.primaryAddress
                                  ? initialValues.primaryAddress
                                  : ""
                                : ""}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div
                      style={{ marginTop: "1em" }}
                      className="dashboard__company__container__profile__preview__content"
                    >
                      <div className="dashboard__company__container__profile__preview__content">
                        <div className="dashboard__company__container__profile__preview__content__svg">
                          <svg
                            id="Group_204"
                            data-name="Group 204"
                            xmlns="http://www.w3.org/2000/svg"
                            width="18.43"
                            height="18.373"
                            viewBox="0 0 18.43 18.373"
                          >
                            <g id="Group_203" data-name="Group 203">
                              <path
                                id="Path_304"
                                data-name="Path 304"
                                d="M18.157,14.664h-1.8V12.427a.274.274,0,1,0-.547,0v6.194h-4.7V10.49h4.7v.66a.274.274,0,1,0,.547,0v-.934a.274.274,0,0,0-.274-.274H12.34V1.07A.274.274,0,0,0,12.066.8H6.373A.274.274,0,0,0,6.1,1.07V5.164H2.558a.274.274,0,0,0-.274.274v6.73H.274A.274.274,0,0,0,0,12.442V18.9a.274.274,0,0,0,.274.274H18.157a.274.274,0,0,0,.274-.274V14.938A.274.274,0,0,0,18.157,14.664ZM2.285,18.622H.547V12.715H2.285Zm5.693,0H2.832V17.108H7.978Zm0-3.9H5.511a.274.274,0,0,0,0,.547H7.978v1.3H2.832v-1.3h1.4a.274.274,0,0,0,0-.547h-1.4v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832V5.712H7.978Zm2.583,2.87v8.406H8.525V5.438a.274.274,0,0,0-.274-.274H7.878V4.617a.274.274,0,1,0-.547,0v.547H6.647V1.343h5.146v8.6h-.958A.274.274,0,0,0,10.561,10.216Zm7.322,8.405h-1.53v-3.41h1.53Z"
                                transform="translate(0 -0.796)"
                                fill="#374957"
                              />
                            </g>
                          </svg>
                        </div>
                        <div className="dashboard__company__container__profile__preview__content__text">
                          <div className="dashboard__company__container__profile__preview__content__text__upper">
                            Zipcode
                          </div>
                          <div className="dashboard__company__container__profile__preview__content__text__bottom">
                            <div>
                              {isOn === "company"
                                ? initialValues.companyAddressZipCode != null
                                  ? initialValues.companyAddressZipCode
                                  : "Not specified"
                                : initialValues
                                ? initialValues.primaryZipCode
                                  ? initialValues.primaryZipCode
                                  : "Not specified"
                                : ""}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {initialValues.Description != null &&
                  initialValues.Description != undefined ? (
                    <div className="dashboard__company__container__profile__preview__left__content__wrapper__right">
                      <ProfilePreviewCard
                        svg={
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="#374957"
                            strokeWidth="1"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            className="feather feather-book"
                          >
                            <path d="M4 19.5A2.5 2.5 0 0 1 6.5 17H20"></path>
                            <path d="M6.5 2H20v20H6.5A2.5 2.5 0 0 1 4 19.5v-15A2.5 2.5 0 0 1 6.5 2z"></path>
                          </svg>
                        }
                        heading="Description"
                        subHeading={initialValues.Description}
                        isDescription={true}
                      />
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
            <div className="dashboard__company__container__profile__preview__right">
              <Img
                loading="lazy"
                src={FreelancerProfileDetailsSvg}
                alt="FreelancerProfileDetailsSvg"
                style={{ width: "70%", marginLeft: "4em", marginTop: "4em" }}
              />
            </div>
          </div>
          {isOn === "company" ? (
            <div className="dashboard__company__container__profile__preview__bottom__bar">
              <Logout />
              <Link
                to="/home-company/invoices"
                className="header__nav__btn btn__primary"
                onClick={() => {
                  setTimeout(() => {
                    window.scrollTo({
                      top: 0,
                      behavior: "smooth",
                    });
                  }, 300);
                }}
                style={{
                  background: "#A7B8C3",
                  boxShadow: "none",
                  fontSize: 16.5,
                  marginRight: 0,
                }}
              >
                Invoices
              </Link>
            </div>
          ) : null}
        </>
      ) : (
        <LoadingMask />
      )}
    </>
  );
}
