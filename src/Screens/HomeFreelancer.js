import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { HomeFreelancerRouter } from "../Routes/HomeFreelancerRouter";
import { getFreelancerById } from "../Redux/Actions/AppActions";
import Header from "Components/Header";
import HomePageFreelancerResultCard from "Components/HomePageFreelancerResultCard";
import { getAllSkills } from "API/Api";
import { freelancerProjects } from "API/FreelancerApi";
import { Head } from "Components";
import Pagination from "react-js-pagination";
import Img from "react-cool-img";
import { getAllKeywords } from "../API/Api";
import { navigate } from "@reach/router";
import { homePageBackground } from "Assets";
import { checkOnlineOffline } from "Utils/common";
import HomeFreelancerSidebar from "Routes/HomeFreelancerSidebar";
import HomeFreelancerFilters from "Components/HomeFreelancerFilters";

export default function HomeFreelancer({
  setIsAddDegreeOpen,
  setIsAddCertificationsOpen,
  setIsAddSkillsOpen,
  setIsAddWorkExperianceOpen,
  setAddWorkExperianceEditData,
  setIsEditDegreeOpen,
  setIsEditCertificationsOpen,
  setEditCertificationData,
  setIsEditSkillsOpen,
  setIsEditWorkExperianceOpen,
  setIsUploadProjectOpen,
  setIsEditProjectOpen,
  getSelectedProjectData,
  setIsAddSocialsOpen,
  setIsEditSocialsOpen,
  setSelectedSkills,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
  setIsSetHourlyRateOpen,
  setIsSelectWorkTimeOpen,
  setSelectedHourlyRate,
  setEditDegree,
  setIsMessageOpen,
  setUserData,
  setVideoCallUpdate,
}) {
  localStorage.setItem("isOn", "freelancer");
  const [activePage, setActivePage] = useState(1);
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  const { user } = useSelector((state) => state.user);
  let dispatch = useDispatch();

  const [projects, setProjects] = useState([]);
  let [currentDate, setDate] = useState(null);
  const [skills, getSkills] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  let [skillId, setSkillsId] = useState([]);
  let [dateOfPosting, setDateOfPosting] = useState();
  let [title, setTitle] = useState("");
  let [jobLocation, setJobLocation] = useState("");
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(9);
  const [totalCards, setTotalCards] = useState();
  const [keywords, setKeywords] = useState([]);
  let [mapLocation, setMapLocation] = useState("");
  let [latitude, setLatitude] = useState(null);
  let [longitude, setLongitude] = useState(null);
  let [selectedRangeValue, setSelectedRangeValue] = useState(500);
  const isProjectView = window.localStorage.getItem("isProjectView");

  useEffect(() => {
    checkOnlineOffline(user.FirebaseId);
  }, []);

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });

    if (Object.keys(user).length !== 0) {
      dispatch(getFreelancerById(user.FreelancerId));
      getAllKeywordsApi();
      setIsLoading(true);
      getAllSkills()
        .then(({ data }) => {
          let formattedData = [];
          data.result.map((e) => {
            formattedData.push({ label: e.Title, value: e.Id });
          });
          getSkills(formattedData);
          setIsLoading(false);
        })
        .catch(() => {
          setIsLoading(false);
        });
      // freelancerProjectApi();
    } else {
      window.location.href = "/freelancer";
    }
  }, []);

  useEffect(() => {
    freelancerProjectApi();
  }, [page]);

  const freelancerProjectApi = (
    jobTitle,
    location,
    selectedRadius,
    lat,
    lng
  ) => {
    setIsLoading(true);

    let jobTitleCustom = "";
    let jobLocationCustom = "";
    let jobLocationLat = null;
    let jobLocationLng = null;
    let radius = null;

    if (jobTitle) {
      jobTitleCustom = jobTitle;
    } else {
      if (jobTitle !== "") jobTitleCustom = title;
    }
    if (location) {
      jobLocationCustom = location;
    } else {
      jobLocationCustom = jobLocation;
    }
    if (lat) {
      jobLocationLat = lat;
    }
    if (lng) {
      jobLocationLng = lng;
    }

    let skill =
      skillId &&
      skillId.map((e) => {
        return e.value;
      });

    if (selectedRadius != 0) {
      radius = selectedRadius;
    }

    freelancerProjects(
      page,
      limit,
      jobTitleCustom,
      skill,
      jobLocationCustom,
      jobLocationLat,
      jobLocationLng,
      dateOfPosting ? dateOfPosting.value : "",
      currentDate,
      radius
    )
      .then(({ data }) => {
        setProjects(data.result ? data.result : []);
        setTotalCards(data.totalRecords);
        setTitle(jobTitleCustom);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const handlePageChange = (pageNumber) => {
    setPage(pageNumber);
  };

  const filteredDataCall = (title, location, selectedRadius, lat, lng) => {
    setLatitude((latitude = lat));
    setLongitude((longitude = lng));
    setMapLocation(location);
    setSelectedRangeValue(selectedRadius);
    freelancerProjectApi(title, location, selectedRadius, lat, lng);
  };

  const dateOfPostingChange = (data) => {
    setDateOfPosting(data);
  };

  const skillSetChange = (data) => {
    setSkillsId(data);
  };

  const handleChangeDate = (data) => {
    setDate(data.currentTarget.value);
  };

  const getAllKeywordsApi = () => {
    // setIsLoading(true);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  const clearAllFilter = () => {
    setKeywords([]);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
      })
      .catch((err) => {
        // console.log(err);
      });
    setTitle((title = ""));
    setDateOfPosting((dateOfPosting = null));
    setSkillsId((skillId = []));
    setDate((currentDate = ""));
    setLatitude((latitude = null));
    setLongitude((longitude = null));
    setMapLocation((mapLocation = ""));
    setPage(1);
    freelancerProjectApi();
  };

  const navigateToDetail = (dataObject) => {
    navigate("/project-details", { state: dataObject });
  };

  return (
    <>
      <Head title="AIDApro | Freelancer" description="Freelancer" />
      <Header isOnDashboard={true} dashboard="freelancer" />
      <div className="dashboard__container__img">
        <Img loading="lazy" src={homePageBackground} alt="homePageBackground" />
        <div className="dashboard__container__img__gradiant"></div>
      </div>
      <div className="dashboard__container">
        <HomeFreelancerSidebar />
        <div className="dashboard__container__content">
          {keywords.length > 0 ? (
            <HomeFreelancerRouter
              setIsFilterOpen={setIsFilterOpen}
              isFilterOpen={isFilterOpen}
              setIsAddDegreeOpen={setIsAddDegreeOpen}
              setIsAddCertificationsOpen={setIsAddCertificationsOpen}
              setIsAddSkillsOpen={setIsAddSkillsOpen}
              setIsAddWorkExperianceOpen={setIsAddWorkExperianceOpen}
              setAddWorkExperianceEditData={setAddWorkExperianceEditData}
              setIsEditDegreeOpen={setIsEditDegreeOpen}
              setIsEditCertificationsOpen={setIsEditCertificationsOpen}
              setEditCertificationData={setEditCertificationData}
              setIsEditSkillsOpen={setIsEditSkillsOpen}
              setIsEditWorkExperianceOpen={setIsEditWorkExperianceOpen}
              setIsUploadProjectOpen={setIsUploadProjectOpen}
              setIsEditProjectOpen={setIsEditProjectOpen}
              getSelectedProjectData={getSelectedProjectData}
              setIsAddSocialsOpen={setIsAddSocialsOpen}
              setIsEditSocialsOpen={setIsEditSocialsOpen}
              filteredDataCall={filteredDataCall}
              latitude={latitude}
              longitude={longitude}
              mapLocation={mapLocation}
              title={title}
              keywords={keywords.length > 0 ? keywords : []}
              userName={user ? user.LoginName : ""}
              setSelectedSkills={setSelectedSkills}
              setIsDeleteConfirmation={setIsDeleteConfirmation}
              isDeleteConfirmationResponse={isDeleteConfirmationResponse}
              setIsSetHourlyRateOpen={setIsSetHourlyRateOpen}
              setIsSelectWorkTimeOpen={setIsSelectWorkTimeOpen}
              selectedRangeValue={selectedRangeValue}
              setSelectedHourlyRate={setSelectedHourlyRate}
              setEditDegree={setEditDegree}
              setIsMessageOpen={setIsMessageOpen}
              setUserData={setUserData}
              setVideoCallUpdate={setVideoCallUpdate}
            />
          ) : null}
        </div>
      </div>
      {window.location.pathname === "/home-freelancer" ? (
        <div className="dashboard__container__main">
          {isFilterOpen ? (
            <HomeFreelancerFilters
              dateOfPosting={dateOfPosting}
              dateOfPostingChange={dateOfPostingChange}
              skills={skills}
              skillId={skillId}
              skillSetChange={skillSetChange}
              currentDate={currentDate}
              handleChangeDate={handleChangeDate}
              freelancerProjectApi={freelancerProjectApi}
              clearAllFilter={clearAllFilter}
            />
          ) : null}
          <div
            className="homepage__container__results"
            style={{ marginTop: "0em" }}
          >
            {projects.map((project, i) => (
              <HomePageFreelancerResultCard
                key={i}
                data={project}
                moveTo={navigateToDetail}
              />
            ))}
          </div>
          <div className="posting__container__pagination">
            <Pagination
              activePage={page}
              itemsCountPerPage={limit}
              totalItemsCount={totalCards}
              pageRangeDisplayed={5}
              onChange={(e) => handlePageChange(e)}
            />
          </div>
        </div>
      ) : null}
    </>
  );
}
