import React, { useState, useEffect } from "react";
import { HomeCompanyAdminRouter } from "../Routes/HomeCompanyRouter";
import Header from "Components/Header";
import CompanyResultCard from "Components/HomePageCompanyResultCard";
import _ from "lodash";
import { getAllKeywords } from "../API/Api";
import { jobSekkerRecentJobsForCompany } from "API/CompanyAPI";
import { getAllSkills, getLookUpByPrefix, getAllIndustry } from "API/Api";
import { getCompanyById } from "../Redux/Actions/AppActions";
import { useSelector, useDispatch } from "react-redux";
import { Head } from "Components";
import Pagination from "react-js-pagination";
import Img from "react-cool-img";
import { homePageBackground } from "Assets";
import { checkOnlineOffline } from "Utils/common";
import HomeCompanySidebar from "Routes/HomeCompanySidebar";
import HomeCompanyFilters from "Components/HomeCompanyFilters";
import { companyDefaultListing } from "Constants/defaultData";

export default function HomeCompany({
  setIsJobPreviewOpen,
  setIsJobPreviewData,
  isJobPreviewOpen,
  setIsRequestVideoCallOpen,
  setVideoCallUserData,
  setIsRejectRequest,
  setIsMessageOpen,
  setIsCustomizedRequirmentsOpen,
  setIsInvoiceOpen,
  setUserData,
  setIsPaymentConfirmation,
  setDataAfterPaymentConfirmation,
  setVideoCallUpdate,
  videoCallUpdate,
  videoCallUpdateSuccess,
  setIsDeleteConfirmation,
  isDeleteConfirmationResponse,
  setIsDeleteConfirmationResponse,
  dataAfterPaymentConfirmation,
}) {
  localStorage.setItem("isOn", "company");
  const { user } = useSelector((state) => state.user);

  let dispatch = useDispatch();
  useEffect(() => {
    if (Object.keys(user).length !== 0) {
      dispatch(getCompanyById(user.CompanyId));
    } else {
      window.location.href = "/sign-up";
    }
  }, []);

  useEffect(() => {
    checkOnlineOffline(user.FirebaseId);
  }, []);

  const [companyName, setCompanyName] = useState("");
  const [isFilterOpen, setIsFilterOpen] = useState(false);
  let [dontShowSidebar, setDontShowSidebar] = useState(false);

  useEffect(() => {
    if (window.location.pathname == "/home-company")
      setDontShowSidebar((dontShowSidebar = false));
  });

  const [searchBy, setSearchBy] = useState("Employment");
  let [searchByFreelancer, setSearchByFreelancer] = useState("");

  let [title, setTitle] = useState("");
  let [jobLocation, setJobLocation] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [jobSeekers, setJobSeekers] = useState(companyDefaultListing);
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(12);
  const [keywords, setKeywords] = useState([]);

  let [skills, setSkills] = useState([]);
  let [industryType, setIndustryType] = useState([]);
  let [jobType, setJobType] = useState([]);
  let [experience, setExperience] = useState([]);
  let [education, setEducation] = useState([]);
  let [availbility, setAvailbility] = useState([]);
  let [languages, setLanguages] = useState([]);
  let [sortBy, setSortBy] = useState("");
  let [salaryFrom, setSalaryFrom] = useState(null);
  let [salaryTo, setSalaryTo] = useState(null);
  const [skillsDropDownData, setSkillsDropDownData] = useState([]);
  const [industryTypeDropDownData, setIndustryTypeDropDownData] = useState([]);
  const [experienceTypeDropDownData, setExperienceTypeDropDownData] = useState(
    []
  );
  const [jobTypeDropDownData, setJobTypeDropDownData] = useState([]);
  const [educationTypeDropDownData, setEducationTypeDropDownData] = useState(
    []
  );
  const [availbilityTypeDropDownData, setAvailbilityTypeDropDownData] =
    useState([]);
  const [languageTypeDropDownData, setLanguageTypeDropDownData] = useState([]);
  const [totalCards, setTotalCards] = useState();
  let [latitude, setLatitude] = useState(
    Object.keys(user).length > 0 ? user.CompanyProfile.Latitude : null
  );
  let [longitude, setLongitude] = useState(
    Object.keys(user).length > 0 ? user.CompanyProfile.Longitude : null
  );
  let [selectedRangeValue, setSelectedRangeValue] = useState(500);

  const isUserProfileView = window.localStorage.getItem("isUserProfile");

  const jobSekkerRecentJobsForCompanyAPI = (
    jobTitle,
    location,
    selectedRadius,
    lat,
    lng
  ) => {
    setIsLoading(true);
    let skillId = skills.map((e) => {
      return e.value;
    });

    let industryId = industryType.map((e) => {
      return e.value;
    });

    // let experienceLevel = experience.label;
    let experienceLevel = experience.map((item) => item.label).join(", ");

    let jobTypeId = jobType.map((e) => {
      return e.value;
    });

    let educationId = education.map((e) => {
      return e.value;
    });

    let availabilityId = availbility.map((e) => {
      return e.value;
    });

    let languageId = languages.map((e) => {
      return e.value;
    });

    let jobTitleCustom = "";
    let jobLocationCustom = "";
    let jobLocationLat = null;
    let jobLocationLng = null;
    let radius = null;

    if (jobTitle) {
      jobTitleCustom = jobTitle;
    } else {
      jobTitleCustom = title;
    }

    if (location) {
      jobLocationCustom = location;
    } else {
      jobLocationCustom = jobLocation;
    }

    if (lat) jobLocationLat = lat;
    else jobLocationLat = latitude;
    if (lng) jobLocationLng = lng;
    else jobLocationLng = longitude;

    if (selectedRadius != 0) {
      radius = selectedRadius;
    }
    jobSekkerRecentJobsForCompany({
      page,
      limit,
      title: jobTitleCustom,
      jobLocation: jobLocationCustom,
      jobLocationLat,
      jobLocationLng,
      skillId,
      industryId,
      experienceLevel,
      jobTypeId,
      educationId,
      availabilityId,
      languageId,
      sortBy,
      searchBy,
      searchByFreelancer,
      salaryFrom,
      salaryTo,
      selectedRadius,
    })
      .then(({ data }) => {
        let finalList = [];
        if (data && data.result) {
          // if (data.result.freelancer) {
          //   let fList = data.result.freelancer;
          //   let jList = data.result.jobseeker;
          //   finalList = [...fList, ...jList];
          // } else {
          finalList = data.result;
          // }
        }
        setJobSeekers(finalList);
        setTotalCards(data.totalRecords);
        setPage(data.page == 0 ? page : data.page);

        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const loadDropDownDataFromAI = () => {
    setIsLoading(true);

    getAllSkills()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setSkillsDropDownData(formattedData);
      })
      .catch((err) => {
        setIsLoading(false);
      });

    getAllIndustry()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setIndustryTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    getLookUpByPrefix("EXPLVL")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setExperienceTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    getLookUpByPrefix("JOBT")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setJobTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    getLookUpByPrefix("EDUTY")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setEducationTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    getLookUpByPrefix("JOINAVAIL")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setAvailbilityTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });

    getLookUpByPrefix("LANG")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setLanguageTypeDropDownData(formattedData);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  const getAllKeywordsApi = () => {
    setIsLoading(true);
    getAllKeywords()
      .then(({ data }) => {
        setKeywords(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    setCompanyName(user.LoginName);
    loadDropDownDataFromAI();
  }, []);

  useEffect(() => {
    getAllKeywordsApi();
  }, [searchBy]);

  useEffect(() => {
    jobSekkerRecentJobsForCompanyAPI();
  }, [page, searchByFreelancer, searchBy]);

  const checkedBy = (data) => {
    if (data === "emp") {
      if (searchBy === "") {
        setSearchBy("Employment");
      } else {
        setSearchBy("");
      }
    } else {
      if (searchByFreelancer === "") {
        setSearchByFreelancer("Freelancer");
      } else {
        setSearchByFreelancer("");
      }
    }
  };

  const loadMore = () => {
    if (user.Id !== undefined) {
      window.location.href = "/home-company";
    } else {
      window.location.href = "/login";
    }
  };

  const handlePageChange = (pageNumber) => {
    setPage(pageNumber);
  };

  const filteredDataCall = (title, location, selectedRadius, lat, lng) => {
    setTitle(title);
    setJobLocation((jobLocation = location));
    setLatitude((latitude = lat));
    setLongitude((longitude = lng));
    setSelectedRangeValue(selectedRadius);
    jobSekkerRecentJobsForCompanyAPI(title, location, selectedRadius, lat, lng);
  };

  const handleChangeInput = (event) => {
    if (event.currentTarget.name === "minSalary") {
      setSalaryFrom(event.currentTarget.value);
    } else {
      setSalaryTo(event.currentTarget.value);
    }
  };

  const skillChange = (event) => {
    setSkills(event);
  };

  const industryChange = (event) => {
    setIndustryType(event);
  };

  const experienceChange = (event) => {
    setExperience(event);
  };

  const jobTypeChange = (event) => {
    setJobType(event);
  };

  const educationChange = (event) => {
    setEducation(event);
  };

  const availabilityChange = (event) => {
    setAvailbility(event);
  };

  const languageChange = (event) => {
    setLanguages(event);
  };

  const searchByUser = (freelancer, employee) => {
    setSearchBy(employee ? "Employment" : "");
    setSearchByFreelancer(freelancer ? "Freelancer" : "");
  };

  const resetFilters = () => {
    setTitle((title = ""));
    setSkills((skills = []));
    setIndustryType((industryType = []));
    setExperience((experience = []));
    setJobType((jobType = []));
    setSalaryFrom(0);
    setSalaryTo(0);
    setEducation((education = []));
    setAvailbility((availbility = []));
    setLanguages((languages = []));
    setSelectedRangeValue((selectedRangeValue = 500));
    // setSearchByFreelancer((searchByFreelancer = ""));
    setTimeout(() => {
      jobSekkerRecentJobsForCompanyAPI();
    }, 800);
  };

  return (
    <>
      <Head title="AIDApro | Company" description="Company" />
      <Header isOnDashboard={true} dashboard="company" />
      <div className="dashboard__container__img">
        <Img loading="lazy" src={homePageBackground} alt="homePageBackground" />
        <div className="dashboard__container__img__gradiant"></div>
      </div>
      <div className="dashboard__container">
        {dontShowSidebar ? null : <HomeCompanySidebar />}
        <div
          className="dashboard__container__content"
          style={dontShowSidebar ? { width: "100%" } : null}
        >
          {keywords.length > 0 ? (
            <HomeCompanyAdminRouter
              setIsFilterOpen={setIsFilterOpen}
              isFilterOpen={isFilterOpen}
              setDontShowSidebar={setDontShowSidebar}
              setIsJobPreviewOpen={setIsJobPreviewOpen}
              setIsJobPreviewData={setIsJobPreviewData}
              isJobPreviewOpen={isJobPreviewOpen}
              setIsRequestVideoCallOpen={setIsRequestVideoCallOpen}
              setVideoCallUserData={setVideoCallUserData}
              setIsRejectRequest={setIsRejectRequest}
              setIsMessageOpen={setIsMessageOpen}
              setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
              filteredDataCall={filteredDataCall}
              latitude={latitude}
              longitude={longitude}
              mapLocation={jobLocation}
              setIsInvoiceOpen={setIsInvoiceOpen}
              title={title}
              searchBy={searchBy}
              searchByFreelancer={searchByFreelancer}
              searchByUser={searchByUser}
              keywords={keywords.length > 0 ? keywords : []}
              setUserData={setUserData}
              selectedRangeValue={selectedRangeValue}
              setIsPaymentConfirmation={setIsPaymentConfirmation}
              setDataAfterPaymentConfirmation={setDataAfterPaymentConfirmation}
              setVideoCallUpdate={setVideoCallUpdate}
              videoCallUpdate={videoCallUpdate}
              videoCallUpdateSuccess={videoCallUpdateSuccess}
              setIsDeleteConfirmation={setIsDeleteConfirmation}
              isDeleteConfirmationResponse={isDeleteConfirmationResponse}
              setIsDeleteConfirmationResponse={setIsDeleteConfirmationResponse}
              jobSeekers={jobSeekers}
              dataAfterPaymentConfirmation={dataAfterPaymentConfirmation}
            />
          ) : null}
        </div>
      </div>
      {window.location.pathname === "/home-company" ? (
        <div className="dashboard__container__main">
          {isFilterOpen ? (
            <HomeCompanyFilters
              skillsDropDownData={skillsDropDownData}
              skills={skills}
              skillChange={skillChange}
              industryTypeDropDownData={industryTypeDropDownData}
              industryChange={industryChange}
              industryType={industryType}
              experienceTypeDropDownData={experienceTypeDropDownData}
              experienceChange={experienceChange}
              experience={experience}
              jobTypeDropDownData={jobTypeDropDownData}
              jobTypeChange={jobTypeChange}
              jobType={jobType}
              handleChangeInput={handleChangeInput}
              salaryFrom={salaryFrom}
              salaryTo={salaryTo}
              educationTypeDropDownData={educationTypeDropDownData}
              educationChange={educationChange}
              education={education}
              availbilityTypeDropDownData={availbilityTypeDropDownData}
              availabilityChange={availabilityChange}
              availbility={availbility}
              languageTypeDropDownData={languageTypeDropDownData}
              languageChange={languageChange}
              languages={languages}
              jobSekkerRecentJobsForCompanyAPI={
                jobSekkerRecentJobsForCompanyAPI
              }
              resetFilters={resetFilters}
            />
          ) : null}
          {jobSeekers.length > 0 ? (
            <>
              <div
                className="homepage__container__results"
                style={{ marginTop: "0em" }}
              >
                {jobSeekers.map((e, i) => (
                  <CompanyResultCard
                    key={i}
                    data={e}
                    allRecords={jobSeekers}
                    // isFreelancer={i % i === 0 ? true : false}
                    isFreelancer={e.Type == "Freelancer" ? true : false}
                    setIsPaymentConfirmation={setIsPaymentConfirmation}
                  />
                ))}
              </div>
              <div className="posting__container__pagination">
                <Pagination
                  activePage={page}
                  itemsCountPerPage={limit}
                  totalItemsCount={totalCards}
                  pageRangeDisplayed={5}
                  onChange={(e) => handlePageChange(e)}
                />
              </div>
            </>
          ) : null}
        </div>
      ) : null}
    </>
  );
}
