import React, { useState, useEffect, useContext } from "react";
import { Head, InputBox } from "Components";
import { getLookUpByPrefix } from "../API/Api";
import {
  jobSekkerQualificationUpdate,
  jobSekkerQualificationEdit,
} from "../API/EmploymentAPI";
import {
  freelancerQualificationEdit,
  freelancerQualificationUpdate,
} from "../API/FreelancerApi";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { crossSvg } from "Assets";

export default function AddDegree({ setIsAddDegreeOpen, editDegree }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  const [education, setEducation] = useState([]);
  const [selectedDegree, setSelectedDegree] = useState(
    editDegree != null ? editDegree.educationType : null
  );
  const [university, setUniversity] = useState(
    editDegree != null ? editDegree.institueName : ""
  );
  const [fieldOfStudy, setFieldOfStudy] = useState(
    editDegree != null ? editDegree.FieldOfStudy : ""
  );
  const [grades, setGrades] = useState("");

  const [months, setMonths] = useState([]);
  const [years, setYears] = useState([]);
  const [startMonth, setStartMonth] = useState("");
  const [startYear, setStartYear] = useState("");
  const [endMonth, setEndMonth] = useState("");
  const [endYear, setEndYear] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  //validation
  const [degreeError, setDegreeError] = useState(false);
  const [degreeErrorMessage, setDegreeErrorMessage] = useState("");
  const [universityError, setUniversityError] = useState(false);
  const [universityErrorMessage, setUniversityErrorMessage] = useState("");
  const [fieldOfStudyError, setFieldOfStudyError] = useState(false);
  const [fieldOfStudyErrorMessage, setFieldOfStudyErrorMessage] = useState("");
  const [gradesError, setGradesError] = useState(false);
  const [gradesErrorMessage, setGradesErrorMessage] = useState("");
  const [startDateError, setStartDateError] = useState(false);
  const [startDateErrorMessage, setStartDateErrorMessage] = useState("");
  const [endDateError, setEndDateError] = useState(false);
  const [endDateErrorMessage, setEndDateErrorMessage] = useState("");

  const [startMonthError, setStartMonthError] = useState(false);
  const [startMonthErrorMessage, setStartMonthErrorMessage] = useState("");
  const [startYearError, setStartYearError] = useState(false);
  const [startYearErrorMessage, setStartYearErrorMessage] = useState("");

  const [endMonthError, setEndMonthError] = useState(false);
  const [endMonthErrorMessage, setEndMonthErrorMessage] = useState("");
  const [endYearError, setEndYearError] = useState(false);
  const [endYearErrorMessage, setEndYearErrorMessage] = useState("");

  const user = useContext(UserContext);
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    getLookUpByPrefix("EDUTY")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id, Id: e.Id });
        });
        setEducation(formattedData);
      })
      .catch((err) => {
        // console.log("err", err);
      });
    let monthsData = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    let formattedMonthsData = [];
    let formattedYearsData = [];
    monthsData.map((e, index) => {
      formattedMonthsData.push({ label: e, value: index + 1 });
    });
    setMonths(formattedMonthsData);
    if (editDegree != null) {
      const start_month = formattedMonthsData.find(
        (element) => element.label == editDegree.StartMonth
      );
      setStartMonth(start_month);
    }
    if (editDegree != null) {
      const end_month = formattedMonthsData.find(
        (element) => element.label == editDegree.EndMonth
      );
      setEndMonth(end_month);
    }

    let currentYear = new Date().getFullYear();
    for (let index = 1960; index <= currentYear; index++) {
      formattedYearsData.push({ label: index, value: index });
    }
    setYears(formattedYearsData.reverse());
    if (editDegree != null) {
      const start_year = formattedYearsData.find(
        (element) => element.label == editDegree.StartYear
      );
      setStartYear(start_year);
    }
    if (editDegree != null) {
      const end_year = formattedYearsData.find(
        (element) => element.label == editDegree.EndYear
      );
      setEndYear(end_year);
    }
  }, []);

  const degreeChange = (e) => {
    if (e) {
      setDegreeError(false);
      setDegreeErrorMessage("");
      setSelectedDegree(e);
    } else {
      setDegreeError(true);
      setDegreeErrorMessage("Please select degree");
    }
  };

  const handleChange = (event) => {
    if (event.currentTarget.name === "university") {
      setUniversity(event.currentTarget.value);
      if (event.currentTarget.value === "") {
        setUniversityError(true);
        setUniversityErrorMessage("Please enter your university - institute");
      } else {
        setUniversityError(false);
        setUniversityErrorMessage("");
      }
    } else if (event.currentTarget.name === "field") {
      setFieldOfStudy(event.currentTarget.value);
      if (event.currentTarget.value === "") {
        setFieldOfStudyError(true);
        setFieldOfStudyErrorMessage("Please enter your Field of study");
      } else {
        setFieldOfStudyError(false);
        setFieldOfStudyErrorMessage("");
      }
    } else if (event.currentTarget.name === "grades") {
      if (event.currentTarget.value === "") {
        setGradesError(true);
        setGradesErrorMessage("Please enter your grades");
      } else {
        setGradesError(false);
        setGradesErrorMessage("");
        setGrades(event.currentTarget.value);
      }
    }
  };

  const startDateChange = (e) => {
    if (e.target.value === "") {
      setStartDateError(true);
      setStartDateErrorMessage("Please select a start date");
    } else {
      setStartDateError(false);
      setStartDateErrorMessage("");
      setStartYear(e.target.value);
    }
  };

  const endDateChange = (e) => {
    if (e.target.value === "") {
      setEndDateError(true);
      setEndDateErrorMessage("Please select a end date.");
    } else if (e.target.value < startYear) {
      setEndDateError(true);
      setEndDateErrorMessage("End date can't be less than start date.");
    } else {
      setEndDateError(false);
      setEndDateErrorMessage("");
    }
    setEndYear(e.target.value);
  };

  const saveEducation = () => {
    if (
      !selectedDegree ||
      university === "" ||
      fieldOfStudy === "" ||
      // grades === "" ||
      startMonth == "" ||
      startYear == "" ||
      endMonth == "" ||
      endYear == "" ||
      startYear.label > endYear.label
      // startYear === "" ||
      // endYear === "" ||
      // startYear > endYear
    ) {
      if (!selectedDegree) {
        setDegreeError(true);
        setDegreeErrorMessage("Please select degree");
      }
      if (university === "") {
        setUniversityError(true);
        setUniversityErrorMessage("Please enter your university - institute");
      }
      if (fieldOfStudy === "") {
        setFieldOfStudyError(true);
        setFieldOfStudyErrorMessage("Please enter your Field of study");
      }
      // if (grades === "") {
      //   setGradesError(true);
      //   setGradesErrorMessage("Please enter your grades");
      // }
      if (startMonth == "") {
        setStartMonthError(true);
        setStartMonthErrorMessage("Please select starting month");
      }
      if (startYear === "") {
        setStartYearError(true);
        setStartYearErrorMessage("Please select starting year");
      }
      if (endMonth === "") {
        setEndMonthError(true);
        setEndMonthErrorMessage("Please select ending month");
      }
      if (endYear === "") {
        setEndYearError(true);
        setEndYearErrorMessage("Please select ending year");
      }
      if (startYear.label > endYear.label) {
        setStartYearError(true);
        setStartYearErrorMessage(
          "Starting year can't be greater than ending year"
        );
      }
    } else {
      setIsLoading(true);
      if (user.Role.Title !== "JobSekker") {
        let qualificationObject = {
          Id: editDegree != null ? editDegree.Id : 0,
          DegreeLookupDetailId: selectedDegree.value,
          FreelancerId: user.FreelancerId,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
          Institute: university,
          // Grades: grades,
          FieldOfStudy: fieldOfStudy,
        };

        if (editDegree != null) {
          freelancerQualificationEdit(qualificationObject)
            .then(({ data }) => {
              setIsLoading(false);

              //CustomSuccess("Qualification edited successfully...");
              setIsAddDegreeOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to update qualification ");
              setIsAddDegreeOpen(false);
            });
        } else {
          freelancerQualificationUpdate(qualificationObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Qualification updated successfully...");
              setIsAddDegreeOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to update qualification ");
              setIsAddDegreeOpen(false);
            });
        }
      } else {
        let qualificationObject = {
          Id: editDegree != null ? editDegree.Id : 0,
          DegreeLookupDetailId: selectedDegree.value,
          jobseekerId: user.JobSeekerId,
          StartDate: `${JSON.stringify(startYear.label)}-${startMonth.value}-1`,
          EndDate:
            endYear.label != undefined
              ? `${JSON.stringify(endYear.label)}-${endMonth.value}-1`
              : null,
          StartMonth: startMonth.label,
          StartYear: JSON.stringify(startYear.label),
          EndMonth: endMonth.label,
          EndYear: JSON.stringify(endYear.label),
          Institute: university,
          // Grades: grades,
          FieldOfStudy: fieldOfStudy,
        };

        if (editDegree != null) {
          jobSekkerQualificationEdit(qualificationObject)
            .then(({ data }) => {
              setIsLoading(false);

              //CustomSuccess("Qualification Edit Successfully...");
              setIsAddDegreeOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Qualification ");
              setIsAddDegreeOpen(false);
            });
        } else {
          jobSekkerQualificationUpdate(qualificationObject)
            .then(({ data }) => {
              setIsLoading(false);
              //CustomSuccess("Qualification Update Successfully...");
              setIsAddDegreeOpen(false);
              window.location.reload();
            })
            .catch((err) => {
              setIsLoading(false);
              CustomError("Failed to Update Qualification ");
              setIsAddDegreeOpen(false);
            });
        }
      }
    }
  };

  return (
    <>
      <Head title="AIDApro | Add degree" description={"Add degree"} />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsAddDegreeOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="110.348"
                height="86.268"
                viewBox="0 0 110.348 86.268"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-6"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#fff" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                </defs>
                <g id="degrees" transform="translate(-19.758 -25.831)">
                  <g
                    id="Group_1490"
                    data-name="Group 1490"
                    transform="translate(19.758 25.832)"
                  >
                    <g
                      id="Group_1489"
                      data-name="Group 1489"
                      transform="translate(0 0)"
                    >
                      <g id="Group_1488" data-name="Group 1488">
                        <g
                          id="Group_1472"
                          data-name="Group 1472"
                          transform="translate(18.067 39.391)"
                        >
                          <g id="Group_1471" data-name="Group 1471">
                            <g id="Group_1470" data-name="Group 1470">
                              <path
                                id="Path_21312"
                                data-name="Path 21312"
                                d="M125.536,136.123,94.883,149.451a6.453,6.453,0,0,1-5.146,0L59.083,136.123A6.453,6.453,0,0,1,55.2,130.2V103.11h74.214V130.2a6.455,6.455,0,0,1-3.881,5.918Z"
                                transform="translate(-55.203 -103.11)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1473"
                          data-name="Group 1473"
                          transform="translate(18.067 61.389)"
                        >
                          <path
                            id="Path_21313"
                            data-name="Path 21313"
                            d="M125.536,152.184,94.883,165.512a6.453,6.453,0,0,1-5.146,0L59.083,152.184a6.453,6.453,0,0,1-3.88-5.918v5.1a6.454,6.454,0,0,0,3.88,5.918l30.653,13.328a6.453,6.453,0,0,0,5.146,0l30.654-13.328a6.453,6.453,0,0,0,3.881-5.918v-5.1A6.455,6.455,0,0,1,125.536,152.184Z"
                            transform="translate(-55.203 -146.266)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g id="Group_1476" data-name="Group 1476">
                          <g id="Group_1475" data-name="Group 1475">
                            <g id="Group_1474" data-name="Group 1474">
                              <path
                                id="Path_21314"
                                data-name="Path 21314"
                                d="M72.3,81.733,23.578,59.955a6.453,6.453,0,0,1,0-11.783L72.3,26.393a6.451,6.451,0,0,1,5.267,0l48.721,21.779a6.453,6.453,0,0,1,0,11.783L77.565,81.733a6.453,6.453,0,0,1-5.268,0Z"
                                transform="translate(-19.758 -25.832)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1478"
                          data-name="Group 1478"
                          transform="translate(17.046 2.038)"
                        >
                          <g id="Group_1477" data-name="Group 1477">
                            <path
                              id="Path_21315"
                              data-name="Path 21315"
                              d="M54.22,48.134a1.019,1.019,0,0,1-.413-1.952L89.528,30.217a4.353,4.353,0,0,1,1.794-.387h.01a4.353,4.353,0,0,1,1.794.387l8.212,3.665a1.027,1.027,0,0,1,.515,1.35,1.052,1.052,0,0,1-1.351.515L92.3,32.078a2.348,2.348,0,0,0-1.937,0L54.643,48.042h-.005A.98.98,0,0,1,54.22,48.134ZM104.642,37.5a1.061,1.061,0,0,1-.413-.087,1.007,1.007,0,0,1-.535-.566.987.987,0,0,1,.02-.78,1.036,1.036,0,0,1,1.346-.515,1.02,1.02,0,0,1,.515,1.345,1.019,1.019,0,0,1-.933.6Z"
                              transform="translate(-53.2 -29.83)"
                              fill="url(#linear-gradient)"
                            />
                          </g>
                        </g>
                        <g
                          id="Group_1479"
                          data-name="Group 1479"
                          transform="translate(0.002 23.005)"
                        >
                          <path
                            id="Path_21316"
                            data-name="Path 21316"
                            d="M127.445,70.963a6.665,6.665,0,0,1-1.157.664L77.568,93.405a6.451,6.451,0,0,1-5.267,0L23.58,71.628a6.7,6.7,0,0,1-1.157-.664A6.458,6.458,0,0,0,23.58,82.082L72.3,103.86a6.452,6.452,0,0,0,5.267,0l48.721-21.778A6.458,6.458,0,0,0,127.445,70.963Z"
                            transform="translate(-19.762 -70.963)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g
                          id="Group_1482"
                          data-name="Group 1482"
                          transform="translate(5.85 21.912)"
                        >
                          <g id="Group_1481" data-name="Group 1481">
                            <g id="Group_1480" data-name="Group 1480">
                              <path
                                id="Path_21317"
                                data-name="Path 21317"
                                d="M32.849,112.65a1.614,1.614,0,0,1-1.614-1.614V82.66A1.612,1.612,0,0,1,32.448,81.1l47.71-12.226a1.613,1.613,0,0,1,.8,3.125l-46.5,11.916v27.125a1.613,1.613,0,0,1-1.613,1.614Z"
                                transform="translate(-31.235 -68.819)"
                                fill="url(#linear-gradient-6)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1485"
                          data-name="Group 1485"
                          transform="translate(2.406 53.777)"
                        >
                          <g
                            id="Group_1484"
                            data-name="Group 1484"
                            transform="translate(0 0)"
                          >
                            <g id="Group_1483" data-name="Group 1483">
                              <path
                                id="Path_21318"
                                data-name="Path 21318"
                                d="M32.5,145.853H26.57a2.091,2.091,0,0,1-2.091-2.091V136.39a5.057,5.057,0,1,1,10.114,0v7.372A2.091,2.091,0,0,1,32.5,145.853Z"
                                transform="translate(-24.479 -131.333)"
                                fill="url(#linear-gradient)"
                              />
                            </g>
                          </g>
                        </g>
                        <g
                          id="Group_1486"
                          data-name="Group 1486"
                          transform="translate(2.406 53.777)"
                        >
                          <path
                            id="Path_21319"
                            data-name="Path 21319"
                            d="M29.535,131.333l-.04,0v7.33a2.091,2.091,0,0,1-2.091,2.091H24.478v3.006a2.091,2.091,0,0,0,2.091,2.091H32.5a2.091,2.091,0,0,0,2.091-2.091V136.39A5.057,5.057,0,0,0,29.535,131.333Z"
                            transform="translate(-24.478 -131.333)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                        <g
                          id="Group_1487"
                          data-name="Group 1487"
                          transform="translate(2.406 54.221)"
                        >
                          <path
                            id="Path_21320"
                            data-name="Path 21320"
                            d="M31.6,132.2a5.032,5.032,0,0,1,.444,2.065v7.372a2.091,2.091,0,0,1-2.091,2.091H24.479v.458a2.091,2.091,0,0,0,2.091,2.091H32.5a2.091,2.091,0,0,0,2.091-2.091v-7.372A5.055,5.055,0,0,0,31.6,132.2Z"
                            transform="translate(-24.479 -132.204)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Education
                  {/* {editDegree != null ? "Edit " : "Add "}degree */}
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  {/* {editDegree != null ? "Edit " : "Add "}your educational
                  history here */}
                  Your educational history here
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Degree"
                  value={selectedDegree}
                  options={education}
                  error={degreeError}
                  errorMessage={degreeErrorMessage}
                  onChange={(e) => degreeChange(e)}
                />
                <InputBox
                  variant="simple"
                  placeholder="Field of study"
                  name="field"
                  value={fieldOfStudy}
                  error={fieldOfStudyError}
                  errorMessage={fieldOfStudyErrorMessage}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <InputBox
                variant="simple"
                placeholder="University - Institute"
                name="university"
                value={university}
                error={universityError}
                errorMessage={universityErrorMessage}
                onChange={(e) => handleChange(e)}
              />
              <div
                className="pupup__container__from__wrapper__header__content__info"
                style={{ marginLeft: 5, marginBottom: 10, marginTop: -10 }}
              >
                Start Date
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Month"
                  name="endDate"
                  options={months}
                  error={startMonthError}
                  errorMessage={startMonthErrorMessage}
                  type="month"
                  value={startMonth}
                  onChange={(e) => {
                    setStartMonthError(false);
                    setStartMonth(e);
                  }}
                />
                <InputBox
                  variant="select"
                  placeholder="Year"
                  name="startDate"
                  options={years}
                  error={startYearError}
                  errorMessage={startYearErrorMessage}
                  type="year"
                  value={startYear}
                  onChange={(e) => {
                    setStartYearError(false);
                    setStartYear(e);
                  }}
                />
              </div>
              <div
                className="pupup__container__from__wrapper__header__content__info"
                style={{ marginLeft: 5, marginBottom: 10, marginTop: -10 }}
              >
                End Date
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Month"
                  name="endDate"
                  error={endMonthError}
                  errorMessage={endMonthErrorMessage}
                  minDate={startYear}
                  options={months}
                  type="month"
                  value={endMonth}
                  onChange={(e) => {
                    setEndMonthError(false);
                    setEndMonth(e);
                  }}
                  disabled={startYear == "" ? true : false}
                />
                <InputBox
                  variant="select"
                  placeholder="Year"
                  name="startDate"
                  options={years}
                  error={endYearError}
                  errorMessage={endYearErrorMessage}
                  type="year"
                  value={endYear}
                  onChange={(e) => {
                    setEndYearError(false);
                    setEndYear(e);
                  }}
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="button"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => saveEducation()}
                disabled={isLoading ? true : false}
                title="add degree"
              >
                {isLoading
                  ? "Loading..."
                  : editDegree != null
                    ? "Edit degree"
                    : "Add degree"}
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
