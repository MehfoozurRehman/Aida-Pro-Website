import React, { useContext, useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { appliedJob, appliedProject, getCountry } from "../API/Api";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import UserContext from "Context/UserContext";
import { getBase64 } from "Utils/common";
import { useDispatch, useSelector } from "react-redux";
import { getFreelancerById, getJobSekkerById } from "Redux/Actions/AppActions";
import { isNullOrEmpty } from "Utils/TextUtils";
import { isInvalidPhoneNumber } from "Utils/Validations";
import { crossSvg } from "Assets";

export default function ApplyForJob({
  setIsApplyForJob,
  setIsApplyForJobProject,
}) {
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  useEffect(() => {
    if (Object.keys(freelancer).length > 0)
      dispatch(getFreelancerById(user.FreelancerId));
    else dispatch(getJobSekkerById(user.JobSeekerId));
  }, []);

  const { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);

  let [userData, setUserData] = useState({})
  const [isLoading, setIsLoading] = useState(false);
  const [email, setEmail] = useState(user.UserName);
  const [country, setCountry] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState([]);

  const [phone, setPhone] = useState("");
  const [notes, setNotes] = useState("");
  const [jobId, setJobId] = useState(localStorage.getItem("jobId"));

  // validation
  let [phoneError, setPhoneError] = useState(false);
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  const [emailAddressError, setEmailAddressError] = useState(false);
  const [emailAddressErrorMessage, setEmailAddressErrorMessage] = useState("");
  const [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");
  const [cvError, setCvError] = useState(false);
  const [cvErrorMessage, setCvErrorMessage] = useState("");

  let cvFile = "";
  let cvFileName = "";

  if (jobsekker) {
    cvFile = jobsekker.JobSeekerCV;
    if (cvFile) {
      cvFileName = "Already uploaded";
    }
  }

  if (freelancer) {
    cvFile = freelancer.JobSeekerCV;
    if (cvFile) {
      cvFileName = "Already uploaded";
    }
  }

  const [fileString, setFileString] = useState(cvFile);
  const [file, setFile] = useState(cvFileName);
  const [cv, setCv] = useState(jobsekker ? jobsekker.JobSeekerCV : null);

  useEffect(() => {
    if (Object.keys(jobsekker).length > 0) setUserData(userData = jobsekker)
    else if (Object.keys(freelancer).length > 0) setUserData(userData = freelancer)

    setPhone(userData.phoneNo)
    setIsLoading(true);
    getCountry()
      .then(({ data }) => {
        let options = data.result.map((e) => ({ value: e.Id, label: e.Title }));
        setCountry(options);
        setIsLoading(false);
      })
      .catch((err) => { });
  }, []);

  const handleChange = (event) => {
    if (event.currentTarget.name === "email") {
      if (event.currentTarget.value === "") {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter an email address");
      } else if (
        !event.currentTarget.value.match(
          /^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w\w+)+$/
        )
      ) {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter a valid email address");
      } else {
        setEmailAddressError(false);
        setEmailAddressErrorMessage("");
        setEmail(event.currentTarget.value);
      }
    }
  };
  const onPhoneTextChangeListener = (enteredValue) => {
    setPhone(enteredValue);
    if (isNullOrEmpty(enteredValue))
      setPhoneNumberErrorMessageAndVisibility("Please enter phone number");
    else if (isInvalidPhoneNumber(enteredValue))
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter valid phone number"
      );
    else {
      setPhoneNumberErrorMessageAndVisibility("");
      return true;
    }
  };
  const setPhoneNumberErrorMessageAndVisibility = (text) => {
    setPhoneError((phoneError = !isNullOrEmpty(text)));
    setPhoneErrorMessage(text);
  };
  const handleChangeDescriptionValues = (data) => {
    if (data.length === 0) {
      setDescriptionError(true);
      setDescriptionErrorMessage("Please enter description");
    } else {
      setDescriptionError(false);
      setDescriptionErrorMessage("");
      setNotes(data);
    }
  };
  const handleChangeCountry = (event) => {
    setSelectedCountry(event);
  };

  const applyNow = () => {
    if (
      email === "" ||
      phone === "" ||
      notes.length === 0 ||
      isInvalidPhoneNumber(phone)
    ) {
      if (email === "") {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter an email address");
      }
      if (phone === "") {
        setPhoneError(true);
        setPhoneErrorMessage("Please enter phone no");
      }
      if (notes.length === 0) {
        setDescriptionError(true);
        setDescriptionErrorMessage("Please enter description");
      }
    } else {
      // if (file) {
      if (user.Role.Title === "Freelancer") applyForProjectSubmit();
      else applyForJobSubmit();
      // } else {
      //   setCvError(true);
      //   setCvErrorMessage("Please upload CV.");
      // }
    }
  };

  const removeCV = () => {
    setFile("");
  };

  const applyForJobSubmit = () => {
    setIsLoading(true);
    let data = {
      JobId: parseInt(jobId),
      JobSeekerId: user.JobSeekerId,
      IsApplied: true,
      IsInterested: null,
      Email: email,
      Phone: phone,
      AdditionalNotes: notes,
      UploadCv: fileString,
    };
    appliedJob(data)
      .then(({ data }) => {
        //CustomSuccess("Applied Successfully...");
        setIsLoading(false);
        localStorage.removeItem("jobId");
        setIsApplyForJobProject(true);
        setIsApplyForJob(false);
      })
      .catch((err) => {
        CustomError("Failed to Apply the Job.");
        setIsLoading(false);
      });
  };

  const applyForProjectSubmit = () => {
    setIsLoading(true);
    let data = {
      ProjectId: parseInt(jobId),
      FreelancerId: user.FreelancerId,
      IsApplied: true,
      IsInterested: null,
      Email: email,
      Phone: phone,
      AdditionalNotes: notes,
      UploadCv: fileString,
    };
    appliedProject(data)
      .then(({ data }) => {
        //CustomSuccess("Successfully Applied !");
        setIsApplyForJob(false);
        localStorage.removeItem("jobId");
        setIsApplyForJobProject(true);
        setIsLoading(false);
      })
      .catch((err) => {
        CustomError("Failed to Apply the Project.");
        setIsLoading(false);
      });
  };

  const fileSelectedHandler = async (img) => {
    if (img[0].size > 5242880) {
      CustomError("Your file size is to large. Kindly upload below 4mb.");
    } else {
      const imageFile = img[0];
      const imageFileName = imageFile.name;
      await getBase64(imageFile, (result) => {
        setFileString(result);
        setFile(imageFileName);
      });
      setCvError(false);
      setCvErrorMessage("");
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Apply For Job" description="Apply For Job" />
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => setIsApplyForJob(false)}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Apply For Job
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Email Address"
                  onChange={(e) => handleChange(e)}
                  name="email"
                  value={email}
                  error={emailAddressError}
                  errorMessage={emailAddressErrorMessage}
                  svg={
                    <svg
                      id="Group_202"
                      data-name="Group 202"
                      xmlns="http://www.w3.org/2000/svg"
                      width="17.919"
                      height="11.946"
                      viewBox="0 0 17.919 11.946"
                    >
                      <path
                        id="Path_292"
                        data-name="Path 292"
                        d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                        transform="translate(0 -85.333)"
                        fill="#374957"
                      />
                    </svg>
                  }
                />
                <InputBox
                  variant="simple"
                  placeholder="Phone"
                  name="phone"
                  type="tel"
                  error={phoneError}
                  errorMessage={phoneErrorMessage}
                  style={{ backgroundColor: "#ffffff" }}
                  value={phone}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      onPhoneTextChangeListener(event.currentTarget.value);
                    }
                  }}
                  maxValue={20}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15.428"
                      height="15.45"
                      viewBox="0 0 15.428 15.45"
                    >
                      <g id="phone-call" transform="translate(-0.344 0)">
                        <g
                          id="Group_201"
                          data-name="Group 201"
                          transform="translate(0.344 0)"
                        >
                          <path
                            id="Path_289"
                            data-name="Path 289"
                            d="M12.544,36.083a1.52,1.52,0,0,0-1.1-.505,1.571,1.571,0,0,0-1.115.5L9.3,37.107c-.085-.046-.17-.088-.251-.13-.117-.059-.228-.114-.323-.173a11.2,11.2,0,0,1-2.684-2.446,6.607,6.607,0,0,1-.88-1.389c.267-.245.515-.5.757-.743.091-.091.183-.186.274-.277a1.5,1.5,0,0,0,0-2.257L5.3,28.8c-.1-.1-.205-.205-.3-.31-.2-.2-.4-.411-.613-.607a1.544,1.544,0,0,0-1.092-.479,1.6,1.6,0,0,0-1.109.479l-.007.007L1.066,29.01a2.386,2.386,0,0,0-.708,1.516,5.716,5.716,0,0,0,.417,2.42A14.041,14.041,0,0,0,3.27,37.107a15.346,15.346,0,0,0,5.11,4,7.961,7.961,0,0,0,2.87.848c.068,0,.14.007.205.007a2.457,2.457,0,0,0,1.882-.809c0-.007.01-.01.013-.016a7.413,7.413,0,0,1,.571-.59c.14-.134.284-.274.424-.421A1.627,1.627,0,0,0,14.836,39a1.567,1.567,0,0,0-.5-1.118Zm1.167,3.434s0,0,0,0c-.127.137-.258.261-.4.4a8.574,8.574,0,0,0-.629.652,1.572,1.572,0,0,1-1.226.518c-.049,0-.1,0-.15,0a7.073,7.073,0,0,1-2.544-.763,14.485,14.485,0,0,1-4.813-3.77A13.239,13.239,0,0,1,1.6,32.64,4.656,4.656,0,0,1,1.238,30.6a1.5,1.5,0,0,1,.45-.968L2.8,28.518a.741.741,0,0,1,.5-.232.7.7,0,0,1,.476.228l.01.01c.2.186.388.378.587.584.1.1.205.209.31.316l.89.89a.622.622,0,0,1,0,1.011c-.095.095-.186.189-.28.28-.274.28-.535.541-.818.8-.007.007-.013.01-.016.016a.665.665,0,0,0-.17.74l.01.029a7.147,7.147,0,0,0,1.053,1.719l0,0a11.969,11.969,0,0,0,2.9,2.635,4.451,4.451,0,0,0,.4.218c.117.059.228.114.323.173.013.007.026.016.039.023a.707.707,0,0,0,.323.082.7.7,0,0,0,.5-.225L10.943,36.7a.738.738,0,0,1,.492-.245.664.664,0,0,1,.47.238l.007.007,1.8,1.8A.645.645,0,0,1,13.711,39.517Z"
                            transform="translate(-0.344 -26.512)"
                            fill="#374957"
                          />
                          <path
                            id="Path_290"
                            data-name="Path 290"
                            d="M245.306,86.8a4.2,4.2,0,0,1,3.417,3.417.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007.441.441,0,0,0,.362-.509,5.075,5.075,0,0,0-4.135-4.135.443.443,0,0,0-.509.359A.435.435,0,0,0,245.306,86.8Z"
                            transform="translate(-236.968 -83.123)"
                            fill="#374957"
                          />
                          <path
                            id="Path_291"
                            data-name="Path 291"
                            d="M256.1,6.816A8.356,8.356,0,0,0,249.287.007a.44.44,0,1,0-.143.867,7.464,7.464,0,0,1,6.085,6.085.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007A.432.432,0,0,0,256.1,6.816Z"
                            transform="translate(-240.674 0)"
                            fill="#374957"
                          />
                        </g>
                      </g>
                    </svg>
                  }
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                {/* <InputBox
                  variant="select"
                  placeholder="Country"
                  options={country}
                  name="description"
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="12.499"
                      height="16.008"
                      viewBox="0 0 12.499 16.008"
                    >
                      <g id="germany" transform="translate(-56)">
                        <path
                          id="Path_654"
                          data-name="Path 654"
                          d="M215.5,352h.5v.47h-.5Z"
                          transform="translate(-154.503 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_655"
                          data-name="Path 655"
                          d="M247.5,352h.5v.47h-.5Z"
                          transform="translate(-185.501 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_656"
                          data-name="Path 656"
                          d="M279.5,352h.5v.47h-.5Z"
                          transform="translate(-216.499 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_657"
                          data-name="Path 657"
                          d="M67.788,6.861A6.3,6.3,0,0,0,65.9,4.682l-.274.381a5.839,5.839,0,0,1,1.413,1.453H65.457a6.863,6.863,0,0,0-.542-1.16,4.631,4.631,0,0,0,.577-2.114,3.242,3.242,0,1,0-6.485,0,4.631,4.631,0,0,0,.577,2.114,6.865,6.865,0,0,0-.542,1.16H57.465a5.839,5.839,0,0,1,1.413-1.453L58.6,4.682a6.248,6.248,0,1,0,9.185,2.179Zm-5.774,8.662a2.958,2.958,0,0,1-2.043-1.63A6.32,6.32,0,0,1,59.544,13h2.47Zm.47,0V13h2.47a6.318,6.318,0,0,1-.427.892A2.958,2.958,0,0,1,62.485,15.523Zm0-2.992v-.517h-.47v.517H59.38a9.23,9.23,0,0,1-.4-2.537h3.036v.517h.47V9.993H65.52a9.23,9.23,0,0,1-.4,2.537ZM56.475,9.993h2.034a9.791,9.791,0,0,0,.38,2.537H57.18a5.74,5.74,0,0,1-.7-2.537Zm4.21-3.007a12.5,12.5,0,0,0,1.329,1.376V9.523H58.979a9.321,9.321,0,0,1,.4-2.537Zm3.129,0h1.3a9.321,9.321,0,0,1,.4,2.537H62.485V8.362A12.5,12.5,0,0,0,63.814,6.986ZM65.99,9.993h2.034a5.74,5.74,0,0,1-.7,2.537H65.61A9.791,9.791,0,0,0,65.99,9.993Zm1.331-3.007c.017.031.034.062.051.093a5.79,5.79,0,0,1,.652,2.444H65.99a9.772,9.772,0,0,0-.379-2.537Zm-2.369-.47h-.77q.256-.345.462-.681A6.478,6.478,0,0,1,64.953,6.516ZM62.25.47a2.776,2.776,0,0,1,2.772,2.772c0,2.051-2.175,4.159-2.772,4.7-.6-.54-2.772-2.646-2.772-4.7A2.776,2.776,0,0,1,62.25.47ZM59.855,5.835q.206.336.462.681h-.77A6.477,6.477,0,0,1,59.855,5.835ZM57.127,7.079c.016-.031.034-.062.051-.093h1.71a9.773,9.773,0,0,0-.379,2.537H56.475A5.789,5.789,0,0,1,57.127,7.079ZM57.467,13h1.576a6.9,6.9,0,0,0,.523,1.129,4.727,4.727,0,0,0,.884,1.121A5.806,5.806,0,0,1,57.467,13Zm6.583,2.25a4.728,4.728,0,0,0,.884-1.121A6.9,6.9,0,0,0,65.456,13h1.576A5.806,5.806,0,0,1,64.05,15.25Z"
                          transform="translate(0)"
                          fill="#374957"
                        />
                        <path
                          id="Path_658"
                          data-name="Path 658"
                          d="M188.48,34.24a2.24,2.24,0,1,0-2.24,2.24A2.242,2.242,0,0,0,188.48,34.24Zm-2.24-1.77a1.77,1.77,0,0,1,1.469.783h-2.937A1.77,1.77,0,0,1,186.24,32.47Zm-1.693,1.253h3.386a1.769,1.769,0,0,1,0,1.034h-3.386a1.769,1.769,0,0,1,0-1.034Zm.224,1.5h2.937a1.769,1.769,0,0,1-2.937,0Z"
                          transform="translate(-123.99 -30.998)"
                          fill="#374957"
                        />
                      </g>
                    </svg>
                  }
                  onChange={(e) => handleChangeCountry(e)}
                /> */}
              </div>
              <InputBox
                variant="textarea"
                placeholder="Motivation to apply"
                error={descriptionError}
                errorMessage={descriptionErrorMessage}
                style={{
                  width: "100%",
                  maxWidth: "100%",
                  marginBottom: "1em",
                }}
                name="description"
                onChange={(e) => handleChangeDescriptionValues(e)}
              />
            </div>
            {file ? (
              <div
                className="pupup__container__from__wrapper__cv"
                style={{ marginTop: 10 }}
                onClick={() => { }}
              >
                <div className="pupup__container__from__wrapper__cv__file">
                  <button
                    className="pupup__container__from__wrapper__cv__file__btn"
                    onClick={() => removeCV()}
                    title="remove cv"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width={15}
                      height={15}
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-x"
                    >
                      <line x1="18" y1="6" x2="6" y2="18"></line>
                      <line x1="6" y1="6" x2="18" y2="18"></line>
                    </svg>
                  </button>
                  <div className="pupup__container__from__wrapper__cv__file__icon">
                    PDF
                  </div>
                  <div className="pupup__container__from__wrapper__cv__file__content">
                    <div className="pupup__container__from__wrapper__cv__file__content__heading">
                      {file ? file : ""}
                    </div>
                  </div>
                </div>
              </div>
            ) : null}

            <div
              className="pupup__container__from__wrapper__cta"
              style={{ marginTop: "1em" }}
            >
              <button
                className="header__nav__btn btn__primary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                title="upload cv"
              >
                <input
                  type="file"
                  accept=".pdf"
                  siz
                  onClick={(event) => (event.target.value = null)}
                  onChange={(e) => fileSelectedHandler(e.currentTarget.files)}
                />
                Upload CV
              </button>
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => applyNow()}
                title="apply now"
              >
                Apply
              </button>
            </div>

            {cvError ? (
              <div className="input__form__error__message">
                {cvErrorMessage}
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}
