import React, { useState, useEffect, useContext } from "react";
import { Head, InputBox } from "Components";
import Img from "react-cool-img";
import { jobSekkerPersonalDetailsUpdate } from "../API/EmploymentAPI";
import { freelancerPersonalDetailsUpdate } from "../API/FreelancerApi";
import { companyUpdate } from "../API/CompanyAPI";
import UserContext from "Context/UserContext";
import { CustomError } from "./Toasts";
import { getCountry, getLookUpByPrefix, getAllIndustry } from "../API/Api";
import { useSelector, useDispatch } from "react-redux";
import { getBase64, getDate10YearsAgo } from "Utils/common";
import { isNullOrEmpty } from "Utils/TextUtils";
import { notValidNumber, isInvalidPhoneNumber } from "Utils/Validations";
import {
  FreelancerProfileDetailsSvg,
  pictureplaceholder,
  profileSelected,
} from "Assets";
import moment from "moment";

const PersonalDetails = ({ location, isOn }) => {
  let userData = location.state.userData;

  const [isDifferentBillingAddress, setIsDifferentBillingAddress] =
    useState(false);
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  // const [userDataObject, setUserDataObject] = useState(userData);
  const [firstName, setFirstName] = useState(
    isOn === "company" ? userData.companyName : userData.firstName
  );
  // const [companyName, setCompanyName] = useState(userData.companyName);
  const [lastName, setLastName] = useState(
    isOn === "company" ? userData.companyContantPerson : userData.lastName
  );
  const [email, setEmail] = useState(
    isOn === "company" ? userData.companyEmailAddress : userData.emailAddress
  );
  const [phone, setPhone] = useState(
    isOn === "company"
      ? userData.companyCellNo != null && userData.companyCellNo != undefined
        ? userData.companyCellNo.substring(2)
        : ""
      : userData.phoneNo != null && userData.phoneNo != undefined
      ? userData.phoneNo.substring(2)
      : ""
  );
  const [age, setAge] = useState(userData.age);
  const [address, setAddress] = useState(
    isOn === "company" ? userData.companyAddress : userData.primaryAddress
  );
  const [postalCode, setPostalCode] = useState(
    isOn === "company"
      ? userData.companyAddressZipCode
      : userData.primaryZipCode
  );
  // const [contactPerson, setContactPerson] = useState("");
  // const [fileString, setFileString] = useState("");
  const [lat, setLat] = useState(userData.lat);
  const [lng, setLng] = useState(userData.lng);
  const [billingLat, setBillingLat] = useState(
    isOn == "company" ? userData.billingLat : null
  );
  const [billingLng, setBillingLng] = useState(
    isOn == "company" ? userData.billingLng : null
  );
  const [isLoading, setIsLoading] = useState(false);
  const [password, setPassword] = useState("");
  const [country, setCountry] = useState([]);
  const [gender, setGender] = useState([]);
  const [language, setLanguage] = useState([]);
  const [selectedLanguage, setSelectedLanguage] = useState(null);
  const [countryCodes, setCountryCode] = useState([]);
  const [selectedCountryCode, setSelectedCountryCode] = useState(null);
  const [phoneMinLength, setPhoneMinLength] = useState(null);
  const [phoneMaxLength, setPhoneMaxLength] = useState(null);
  // const [postalCodeRegex, setPostalCodeRegex] = useState(null);
  // const [countryId, setCountryId] = useState(

  //   isOn === "company" && userData !== null && userData.companyAddressCountry !== null ? userData.companyAddressCountry.value : null
  // );
  // const [cityId, setCityId] = useState();
  // const [city, setCity] = useState([]);
  // const [selectedCountry, setSelectedCountry] = useState(
  //   isOn === "company"
  //     ? userData.companyAddressCountry
  //     : userData.primaryCountry
  // );
  // const [selectedCity, setSelectedCity] = useState(
  //   isOn === "company" ? userData.companyAddressCity : userData.primaryCity
  // );
  const [selectedGender, setSelectedGender] = useState(userData.gender);

  const [industryType, setIndustryType] = useState([]);
  const [selectedIndustry, setSelectedIndustry] = useState(
    userData.companyBranch
  );
  // const [representativeName, setRepresentativeName] = useState(
  //   userData.companyContantPerson
  // );
  const [noOfEmployee, setNoOfEmployee] = useState(userData.companyNoOfEmp);
  const [description, setDescription] = useState(
    isOn === "company" ? userData.Description : null
  );

  //#region billing address company
  // let [billingAddressCountry, setBillingAddressCountry] = useState(
  //   userData.companyBillingAddressCountry != undefined
  //     ? userData.companyBillingAddressCountry
  //     : ""
  // );
  // let [billingAddressCity, setBillingAddressCity] = useState(
  //   userData.companyBillingAddressCity != undefined
  //     ? userData.companyBillingAddressCity
  //     : ""
  // );
  let [billingPostalCode, setBillingPostalCode] = useState(
    userData.companyBillingAddressZipCode != undefined
      ? userData.companyBillingAddressZipCode
      : ""
  );
  let [billingAddress, setBillingAddress] = useState(
    userData.companyBillingAddress != undefined
      ? userData.companyBillingAddress
      : ""
  );
  const [addressTypeArrayPRMY, setAddressTypeArrayPRMY] = useState([]);
  const [addressTypeArrayBIL, setAddressTypeArrayBIL] = useState([]);
  //#endregion

  const [imageToUpload, setImageToUpload] = useState(null);

  const [urlImage, setUrlImage] = useState(
    isOn === "company"
      ? userData.LogoUrl
      : isOn === "professional"
      ? userData.personalDetailsJobSeekerImage
      : isOn === "freelancer"
      ? userData.personalDetailsFreelancerImage
      : null
  );
  const user = useContext(UserContext);
  let dispatch = useDispatch();

  const { jobsekker } = useSelector((state) => state.jobsekker);
  const { freelancer } = useSelector((state) => state.freelancer);
  const { company } = useSelector((state) => state.company);

  // validation
  // const [companyNameError, setCompanyNameError] = useState(false);
  // const [companyNameErrorMessage, setCompanyNameErrorMessage] = useState("");
  // const [representativeNameError, setRepresentativeNameError] = useState(false);
  // const [representativeNameErrorMessage, setRepresentativeNameErrorMessage] =
  //   useState("");
  var [firstNameError, setFirstNameError] = useState(false);
  const [firstNameErrorMessage, setFirstNameErrorMessage] = useState("");
  var [lastNameError, setLastNameError] = useState(false);
  const [lastNameErrorMessage, setLastNameErrorMessage] = useState("");
  var [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  var [phoneError, setPhoneError] = useState(false);
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  var [genderError, setGenderError] = useState(false);
  const [genderErrorMessage, setGenderErrorMessage] = useState("");
  var [ageError, setAgeError] = useState(false);
  const [ageErrorMessage, setAgeErrorMessage] = useState(false);
  // const [countryError, setCountryError] = useState(false);
  // const [countryErrorMessage, setCountryErrorMessage] = useState("");
  // const [cityError, setCityError] = useState(false);
  // const [cityErrorMessage, setCityErrorMessage] = useState("");
  var [postalCodeError, setPostalCodeError] = useState(false);
  const [postalCodeErrorMessage, setPostalCodeErrorMessage] = useState("");
  var [addressError, setAddressError] = useState(false);
  const [addressErrorMessage, setAddressErrorMessage] = useState("");
  var [industryTypeError, setIndustryTypeError] = useState(false);
  const [industryTypeErrorMessage, setIndustryTypeErrorMessage] = useState("");
  var [noOfEmployeeError, setNoOfEmployeeError] = useState(false);
  const [noOfEmployeeErrorMessage, setNoOfEmployeeErrorMessage] = useState("");
  var [billingPostalCodeError, setBillingPostalCodeError] = useState(false);
  const [billingPostalCodeErrorMessage, setBillingPostalCodeErrorMessage] =
    useState("");

  var [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");

  var [billingAddressError, setBillingAddressError] = useState(false);
  const [billingAddressErrorMessage, setBillingAddressErrorMessage] =
    useState("");

  useEffect(() => {
    setIsLoading(true);
    setSelectedLanguage(
      userData.language != undefined && userData.language.length > 0
        ? userData.language
        : null
    );
    getAllIndustry()
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setIndustryType(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getCountry()
      .then(({ data }) => {
        let options = data.result.map((e) => ({
          value: e.Id,
          label: e.digit_name + " - " + e.phone_country_code,
          capital_city: e.capital_city,
          long_name: e.long_name,
          digit_name: e.digit_name,
          phone_country_code: e.phone_country_code,
          phone_number_length_min: e.phone_number_length_min,
          phone_number_length_max: e.phone_number_length_max,
          postal_validation_regex: e.postal_validation_regex,
          postal_code_char_set: e.postal_code_char_set,
          postal_code_length_min: e.postal_code_length_min,
          postal_code_length_max: e.postal_code_length_max,
        }));

        if (isOn == "company") {
          if (userData.companyAddressCountry != null) {
            const found = options.find(
              (element) =>
                element.phone_country_code ==
                userData.companyAddressCountry.phone_country_code
            );
            if (found != undefined) {
              setSelectedCountryCode(found);
              setPhoneMinLength(parseInt(found.phone_number_length_min));
              setPhoneMaxLength(parseInt(found.phone_number_length_max));
              // setPostalCodeRegex(new RegExp(found.postal_validation_regex))
            }
          }
        } else {
          if (userData.primaryCountry != null) {
            const found = options.find(
              (element) =>
                element.phone_country_code ==
                userData.primaryCountry.phone_country_code
            );
            if (found != undefined) {
              setSelectedCountryCode(found);
              setPhoneMinLength(parseInt(found.phone_number_length_min));
              setPhoneMaxLength(parseInt(found.phone_number_length_max));
              // setPostalCodeRegex(new RegExp(found.postal_validation_regex))
            }
          }
        }
        setCountryCode(options);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getLookUpByPrefix("GEND")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setGender(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getLookUpByPrefix("LANG")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setLanguage(formattedData);
      })
      .catch((err) => {
        // console.log("Err", err);
      });

    getLookUpByPrefix("ADRSTYP")
      .then(({ data }) => {
        setAddressTypeArrayPRMY(data.result[0]);
        setAddressTypeArrayBIL(data.result[1]);
      })
      .catch((err) => {
        // console.log("Err", err);
      });
    setIsLoading(false);
  }, []);

  const handleTabBtnClick = (e) => {
    if (activeTabIndex === e.target.tabIndex) {
      return;
    }
    setActiveTabIndex(e.target.tabIndex);
  };

  const updateFreelancer = () => {
    let langData = [];
    if (selectedLanguage != null) {
      selectedLanguage.map((e) => {
        langData.push({
          LanguageLookupDetailId: e.value,
          // Id: e.Id != undefined ? e.Id : 0,
          Id: 0,
        });
      });
    }
    let data = {
      Age: age,
      FirstName: firstName,
      GenderLookupDetailId: selectedGender.value,
      ProfilePicture: imageToUpload != null ? imageToUpload : urlImage,
      Id: user.FreelancerId,
      LastName: lastName,
      FreelancerLanguages: langData,
      Latitude: lat,
      Longitude: lng,
      FreelancerAddresses: [
        {
          ZipCode: postalCode,
          PhoneNo: selectedCountryCode.phone_country_code + phone,
          Id: freelancer.FreelancerAddressesId,
          FreelancerId: user.FreelancerId,
          CountryId: selectedCountryCode.value,
          // CityId: selectedCity.value,
          Email: email,
          CreatedById: user.Id,
          Latitude: lat,
          Longitude: lng,
          Zoom: 1,
          AddressDetail: address,
        },
      ],
    };
    setIsLoading(true);
    freelancerPersonalDetailsUpdate(data)
      .then(({ data }) => {
        setIsLoading(false);
        //CustomSuccess("Personal detail updated successfully");
        // setTimeout(() => {
        window.location.href = "/home-freelancer/personal-details-preview";
        // }, 800);
      })
      .catch((err) => {
        setIsLoading(false);
        CustomError("Failed to update");
      });
  };

  const updateJobseeker = () => {
    let langData = [];
    if (selectedLanguage != null) {
      selectedLanguage.map((e) => {
        langData.push({
          LanguageLookupDetailId: e.value,
          Id: e.Id != undefined ? e.Id : 0,
        });
      });
    }

    let data = {
      Age: age,
      FirstName: firstName,
      GenderLookupDetailId: selectedGender.value,
      ProfilePicture: imageToUpload != null ? imageToUpload : urlImage,
      Id: user.JobSeekerId,
      LastName: lastName,
      JobSeekerLanguages: langData,
      Latitude: lat,
      Longitude: lng,
      JobSeekerAddresses: [
        {
          ZipCode: postalCode,
          PhoneNo: selectedCountryCode.phone_country_code + phone,
          Id: jobsekker.JobSeekerAddressesId,
          JobSeekerId: user.JobSeekerId,
          CountryId: selectedCountryCode.value,
          // CityId: selectedCity.value,
          Email: email,
          CreatedById: user.Id,
          Latitude: lat,
          Longitude: lng,
          Zoom: 1,
          AddressDetail: address,
        },
      ],
    };
    setIsLoading(true);
    jobSekkerPersonalDetailsUpdate(data)
      .then(({ data }) => {
        //CustomSuccess("Personal Detail Update Successfully");
        setIsLoading(false);
        window.location.href = "/home-professional/personal-details-preview";
      })
      .catch((err) => {
        CustomError("Failed to Update Personal Detail");
        setIsLoading(false);
      });
  };

  const updateCompany = () => {
    let companyAddress = [];

    let personalAddressObject = {
      Id: userData.Id,
      AddressDetail: address,
      AddressTypeLookupDetailId: addressTypeArrayPRMY.Id,
      // CityId: selectedCity.value,
      CompanyProfileId: userData.CompanyProfileId,
      CreatedById: userData.Id,
      CountryId: selectedCountryCode.value,
      Email: email,
      PhoneNo: selectedCountryCode.phone_country_code + phone,
      Latitude: lat,
      Longitude: lng,
      ZipCode: postalCode,
    };

    let billingAddressObject = {
      AddressDetail: billingAddress,
      AddressTypeLookupDetailId: addressTypeArrayBIL.Id,
      // CityId: billingAddressCity.value,
      // CountryId: selectedCountryCode.value,
      CreatedById: userData.Id,
      Latitude: billingLat,
      Longitude: billingLng,
      ZipCode: billingPostalCode,
    };

    companyAddress.push(personalAddressObject);
    companyAddress.push(billingAddressObject);
    // return editObject;

    let data = {
      Id: userData.CompanyProfileId,
      CreatedById: userData.Id,
      CompanyName: firstName,
      ContactPerson: lastName,
      LogoUrl: imageToUpload != null ? imageToUpload : urlImage,
      IndustryId: selectedIndustry.value,
      Description: description,
      NoOfEmployees: noOfEmployee,
      Latitude: lat,
      Longitude: lng,
      CompanyAddresses: companyAddress,
    };
    setIsLoading(true);
    companyUpdate(data)
      .then(({ data }) => {
        //CustomSuccess("Personal Detail Update Successfully");
        window.location.href = "/home-company/profile";
        setIsLoading(false);
      })
      .catch((err) => {
        CustomError("Failed to Update Personal Detail");
        setIsLoading(false);
      });
  };

  const updateProfile = () => {
    if (isViewValid()) {
      if (isOn === "professional") updateJobseeker();
      else if (isOn === "freelancer") updateFreelancer();
      else updateCompany();
    }
  };

  const isCompany = () => {
    return isOn === "company";
  };
  const isViewValid = () => {
    if (
      setFirstNameErrorMessageAndVisibilityAndSetText(firstName) ||
      setLastNameErrorMessageAndVisibilityAndSetText(lastName) ||
      setEmailErrorMessageAndVisibilityAndSetText(email) ||
      setPhoneErrorMessageAndVisibilityAndSetText(phone) ||
      (isCompany() &&
        setIndustryTypeErrorMessageAndVisibility(selectedIndustry)) ||
      (isCompany() &&
        setNoOfEmployeeErrorMessageAndVisibilityAndSetText(noOfEmployee)) ||
      (!isCompany() &&
        setGenderErrorMessageAndVisibilityAndSetText(selectedGender)) ||
      (!isCompany() && setAgeErrorMessageAndVisibilityAndSetText(age)) ||
      setPostalCodeErrorMessageAndVisibilityAndSetText(postalCode) ||
      setAddressErrorMessageAndVisibilityAndSetText(address, lat, lng) ||
      (isCompany() &&
        setDescriptionErrorMessageAndVisibilityAndSetText(description))
    )
      return false;
    if (isCompany() && isDifferentBillingAddress) {
      if (
        setBillingPostalCodeErrorMessageAndVisibilityAndSetText(
          billingPostalCode
        ) ||
        setBillingAddressErrorMessageAndVisibilityAndSetText(billingAddress)
      )
        return false;
    }
    return true;
  };

  const setFirstNameErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setFirstNameError((firstNameError = isEmpty));
    setFirstNameErrorMessage(
      isEmpty
        ? isOn === "company"
          ? "Please enter company name"
          : "Please enter first name"
        : ""
    );
    setFirstName(enteredText);
    return isEmpty;
  };
  const setDescriptionErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setDescriptionError((descriptionError = isEmpty));
    setDescriptionErrorMessage(isEmpty ? "Please enter description" : "");
    setDescription(enteredText);

    return isEmpty;
  };
  const setLastNameErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setLastNameError((lastNameError = isEmpty));
    setLastNameErrorMessage(
      isEmpty
        ? isOn === "company"
          ? "Please enter representative name"
          : "Please enter last name"
        : ""
    );
    setLastName(enteredText);
    return isEmpty;
  };

  const setEmailErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setEmailError((emailError = isEmpty));
    setEmailErrorMessage(isEmpty ? "Please enter email address" : "");
    setEmail(enteredText);
    return isEmpty;
  };
  const setPhoneErrorMessageAndVisibilityAndSetText = (enteredText) => {
    setPhone(enteredText);
    if (isNullOrEmpty(enteredText)) {
      setPhoneNumberErrorMessageAndVisibility("Please enter phone number");
      return true;
    } else if (selectedCountryCode == null) {
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter country code first"
      );
      return true;
    } else if (isInvalidPhoneNumber(enteredText)) {
      setPhoneNumberErrorMessageAndVisibility(
        "Please enter valid phone number"
      );
      return true;
    } else if (enteredText.length < phoneMaxLength) {
      setPhoneNumberErrorMessageAndVisibility(
        `Please enter ${phoneMaxLength} digit phone number`
      );
      return true;
    } else if (enteredText.length > phoneMaxLength) {
      setPhoneNumberErrorMessageAndVisibility(
        `Phone number can't be greater than ${phoneMaxLength} digits`
      );
      return true;
    } else {
      setPhoneNumberErrorMessageAndVisibility("");
      return false;
    }
  };
  const setPhoneNumberErrorMessageAndVisibility = (text) => {
    setPhoneError((phoneError = !isNullOrEmpty(text)));
    setPhoneErrorMessage(text);
  };
  const setAgeErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    if (isEmpty) {
      setAgeError((ageError = isEmpty));
      setAgeErrorMessage(isEmpty ? "Please enter age" : "");
    }
    setAge(enteredText);
    return isEmpty;
  };
  const setGenderErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setGenderError((genderError = isEmpty));
    setGenderErrorMessage(isEmpty ? "Please select gender" : "");
    setSelectedGender(enteredText);

    return isEmpty;
  };
  const setNoOfEmployeeErrorMessageAndVisibilityAndSetText = (enteredText) => {
    let isEmpty = isNullOrEmpty(enteredText);
    let isInvalidNumber = notValidNumber(enteredText);
    setNoOfEmployeeError((noOfEmployeeError = isEmpty));
    setNoOfEmployeeErrorMessage(isEmpty ? "Please enter no of employees" : "");
    if (isEmpty) {
      setNoOfEmployeeError((noOfEmployeeError = isEmpty));
      setNoOfEmployeeErrorMessage(
        isEmpty ? "Please enter no of employees" : ""
      );
    } else if (isCompany()) {
      if (isInvalidNumber) {
        setNoOfEmployeeError(true);
        setNoOfEmployeeErrorMessage("Can't contain special characters.");
      }
    }
    setNoOfEmployee(enteredText);
    if (isCompany()) {
      return isInvalidNumber;
    } else return isEmpty;
  };
  const setPostalCodeErrorMessageAndVisibilityAndSetText = (enteredText) => {
    setPostalCode(enteredText);
    if (isNullOrEmpty(enteredText)) {
      setPostalCodeErrorMessageAndVisibility("Please enter postal code");
      return true;
    }
    // else if (!postalCodeRegex.test(enteredText)) {
    //   setPostalCodeErrorMessageAndVisibility("Please type correct postal code");
    //   return true;
    // }
    else {
      setPostalCodeErrorMessageAndVisibility("");
      return false;
    }
  };
  const setPostalCodeErrorMessageAndVisibility = (text) => {
    setPostalCodeError((phoneError = !isNullOrEmpty(text)));
    setPostalCodeErrorMessage(text);
  };
  const setAddressErrorMessageAndVisibilityAndSetText = (
    enteredText,
    lat,
    long
  ) => {
    let isEmpty = !(lat !== null && long !== null);
    setAddressError((addressError = isEmpty));
    setAddressErrorMessage(isEmpty ? "Please enter address" : "");
    setAddress(enteredText);
    setLat(lat);
    setLng(long);

    return isEmpty;
  };
  const setIndustryTypeErrorMessageAndVisibility = (industryType) => {
    let isEmpty = isNullOrEmpty(industryType);
    setIndustryTypeError((industryTypeError = isEmpty));
    setIndustryTypeErrorMessage(isEmpty ? "Please select industry" : "");
    setSelectedIndustry(industryType);
    return isEmpty;
  };

  const setBillingPostalCodeErrorMessageAndVisibilityAndSetText = (
    enteredText
  ) => {
    let isEmpty = isNullOrEmpty(enteredText);
    setBillingPostalCodeError((postalCodeError = isEmpty));
    setBillingPostalCodeErrorMessage(isEmpty ? "Please enter postal code" : "");
    setBillingPostalCode(enteredText);

    return isEmpty;
  };
  const setBillingAddressErrorMessageAndVisibilityAndSetText = (
    enteredText,
    billingLat,
    billingLng
  ) => {
    let isEmpty = !(billingLat !== null && billingLng !== null);
    setBillingAddressError((billingAddressError = isEmpty));
    setBillingAddressErrorMessage(
      isEmpty ? "Please enter billing address" : ""
    );
    setBillingAddress(enteredText);
    setBillingLat(billingLat);
    setBillingLng(billingLng);
    return isEmpty;
  };

  const onNameChangeListener = (event) => {
    setFirstNameErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };
  const onLastNameChangeListener = (event) => {
    setLastNameErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };
  const onEmailChangeListener = (event) => {
    setEmailErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };
  const onPhoneNumberChangeListener = (event) => {
    setPhoneErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };
  const onAgeChangeListener = (event) => {
    setAgeErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };
  const onNoOfEmployeeChangeListener = (event) => {
    setNoOfEmployeeErrorMessageAndVisibilityAndSetText(
      event.currentTarget.value
    );
  };
  const onPostalCodeChangeListener = (event) => {
    setPostalCodeErrorMessageAndVisibilityAndSetText(event.currentTarget.value);
  };

  const handleChangeGender = (event) => {
    setGenderErrorMessageAndVisibilityAndSetText(event);
  };

  const handleChangeIndustry = (event) => {
    setIndustryTypeErrorMessageAndVisibility(event);
  };

  const fetchImage = async (img) => {
    let imageFile = img[0];
    await getBase64(imageFile, (result) => {
      setImageToUpload(result);
    });
  };

  const handleChangelocation = (data, isInput) => {
    if (isNullOrEmpty(data.geometry))
      setAddressErrorMessageAndVisibilityAndSetText(
        data.currentTarget.value,
        null,
        null
      );
    else {
      if (isInput) {
        setAddressErrorMessageAndVisibilityAndSetText(
          data.currentTarget.value,
          null,
          null
        );
      } else {
        setAddressErrorMessageAndVisibilityAndSetText(
          data.formatted_address,
          data.geometry.location.lat(),
          data.geometry.location.lng()
        );
        setAddress(data.formatted_address);
      }
    }
  };

  const handleChangelocationBilling = (data, isInput) => {
    if (isNullOrEmpty(data.geometry))
      setBillingAddressErrorMessageAndVisibilityAndSetText(
        data.currentTarget.value,
        null,
        null
      );
    else {
      if (isInput) {
        setBillingAddressErrorMessageAndVisibilityAndSetText(
          data.currentTarget.value,
          null,
          null
        );
      } else {
        setBillingAddressErrorMessageAndVisibilityAndSetText(
          data.formatted_address,
          data.geometry.location.lat(),
          data.geometry.location.lng()
        );
        setBillingAddress(data.formatted_address);
      }
    }
  };

  const onChangeCountryCode = (event) => {
    setSelectedCountryCode(event);
    setPhoneMinLength(parseInt(event.phone_number_length_min));
    setPhoneMaxLength(parseInt(event.phone_number_length_max));
    setPhoneNumberErrorMessageAndVisibility("");
    // setPostalCodeRegex(new RegExp(event.postal_validation_regex));
  };

  return (
    <>
      <Head
        title={
          isOn === "company"
            ? "AIDApro | Company Personal Details"
            : isOn === "freelancer"
            ? "AIDApro | Freealncer Personal Details"
            : "AIDApro | Professional Personal Details"
        }
        description={
          isOn === "company"
            ? "Company Personal Details"
            : isOn === "freelancer"
            ? "Freealncer Personal Details"
            : "Professional Personal Details"
        }
      />
      <div className="dashboard__company__container">
        <div className="dashboard__company__container__profile__left">
          <div
            className="dashboard__company__container__heading__wrapper"
            style={
              isOn === "company"
                ? { marginBottom: 10, marginTop: 10 }
                : { marginBottom: 10, marginTop: 10 }
            }
          >
            <Img
              loading="lazy"
              src={profileSelected}
              alt=""
              style={{
                width: 30,
                height: 30,
                marginRight: 10,
                marginBottom: 10,
              }}
            />
            <div className="homepage__container__jumbotron__sub__heading">
              {isOn === "company" ? "Company" : null}
              <div>{isOn === "company" ? null : "Personal"} details</div>
            </div>
            <div className="homepage__container__jumbotron__button">
              <button
                style={{ width: 150 }}
                className="header__nav__btn btn__secondary"
                onClick={() => updateProfile()}
                title="Save changings"
              >
                Save Changes
              </button>
            </div>
          </div>
          <div className="dashboard__company__container__profile__left__container">
            <div className="dashboard__company__container__profile__left__container__wrapper">
              <div className="dashboard__company__container__profile__left__container__left">
                <InputBox
                  variant="white"
                  placeholder={
                    isOn === "company" ? "Company Name" : "First Name"
                  }
                  error={firstNameError}
                  errorMessage={firstNameErrorMessage}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="13.419"
                      height="17.918"
                      viewBox="0 0 13.419 17.918"
                    >
                      <g
                        id="Group_448"
                        data-name="Group 448"
                        transform="translate(-514.783 -232.859)"
                      >
                        <path
                          id="Path_285"
                          data-name="Path 285"
                          d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                          transform="translate(376.159 66.29)"
                          fill="#374957"
                        />
                        <path
                          id="Path_286"
                          data-name="Path 286"
                          d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                          transform="translate(409.691 -81.998)"
                          fill="#374957"
                        />
                        <path
                          id="Path_287"
                          data-name="Path 287"
                          d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                          transform="translate(519.365 232.859)"
                          fill="#374957"
                        />
                        <path
                          id="Path_288"
                          data-name="Path 288"
                          d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                          transform="translate(376.159 175.843)"
                          fill="#374957"
                        />
                      </g>
                    </svg>
                  }
                  name={isOn === "company" ? "companyName" : "name"}
                  disabled={false}
                  value={firstName}
                  onChange={(e) => onNameChangeListener(e)}
                />
              </div>
              <div className="dashboard__company__container__profile__left__container__right">
                <InputBox
                  variant="white"
                  placeholder={
                    isOn === "company" ? "Representative Name" : "Last Name"
                  }
                  error={lastNameError}
                  errorMessage={lastNameErrorMessage}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="13.419"
                      height="17.918"
                      viewBox="0 0 13.419 17.918"
                    >
                      <g id="name" transform="translate(3.582 -1)">
                        <path
                          id="Path_285"
                          data-name="Path 285"
                          d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                          transform="translate(-142.206 -165.569)"
                          fill="#374957"
                        />
                        <path
                          id="Path_286"
                          data-name="Path 286"
                          d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                          transform="translate(-108.674 -313.857)"
                          fill="#374957"
                        />
                        <path
                          id="Path_287"
                          data-name="Path 287"
                          d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                          transform="translate(1 1)"
                          fill="#374957"
                        />
                        <path
                          id="Path_288"
                          data-name="Path 288"
                          d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                          transform="translate(-142.206 -56.016)"
                          fill="#374957"
                        />
                      </g>
                    </svg>
                  }
                  name={isOn === "company" ? "representativeName" : "lastName"}
                  value={lastName}
                  onChange={(e) => onLastNameChangeListener(e)}
                />
              </div>
              <div className="dashboard__company__container__profile__left__container__left">
                <InputBox
                  variant="white"
                  placeholder="Email"
                  error={emailError}
                  errorMessage={emailErrorMessage}
                  svg={
                    <svg
                      id="Group_202"
                      data-name="Group 202"
                      xmlns="http://www.w3.org/2000/svg"
                      width="17.919"
                      height="11.946"
                      viewBox="0 0 17.919 11.946"
                    >
                      <path
                        id="Path_292"
                        data-name="Path 292"
                        d="M17,85.333H.919A.922.922,0,0,0,0,86.252V96.36a.922.922,0,0,0,.919.919H17a.922.922,0,0,0,.919-.919V86.252A.922.922,0,0,0,17,85.333Zm-.345.689L9.488,91.4a.961.961,0,0,1-1.057,0L1.264,86.022Zm-3.828,5.73,3.905,4.824.013.013H1.174l.013-.013,3.905-4.824a.345.345,0,0,0-.536-.434L.689,96.1V86.453l7.328,5.5a1.645,1.645,0,0,0,1.884,0l7.328-5.5V96.1l-3.867-4.777a.345.345,0,0,0-.536.434Z"
                        transform="translate(0 -85.333)"
                        fill="#374957"
                      />
                    </svg>
                  }
                  name="email"
                  value={email}
                  disabled={true}
                  onChange={(e) => onEmailChangeListener(e)}
                />
              </div>
              <div className="dashboard__company__container__profile__left__container__right">
                <InputBox
                  variant="phone"
                  placeholder="Phone"
                  type="tel"
                  options={countryCodes}
                  selectedCountryCode={selectedCountryCode}
                  onChangeCountryCode={onChangeCountryCode}
                  style={{ backgroundColor: "#ffffff" }}
                  width="65%"
                  error={phoneError}
                  errorMessage={phoneErrorMessage}
                  phoneMinLength={phoneMinLength}
                  phoneMaxLength={phoneMaxLength}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="15.428"
                      height="15.45"
                      viewBox="0 0 15.428 15.45"
                    >
                      <g id="phone-call" transform="translate(-0.344 0)">
                        <g
                          id="Group_201"
                          data-name="Group 201"
                          transform="translate(0.344 0)"
                        >
                          <path
                            id="Path_289"
                            data-name="Path 289"
                            d="M12.544,36.083a1.52,1.52,0,0,0-1.1-.505,1.571,1.571,0,0,0-1.115.5L9.3,37.107c-.085-.046-.17-.088-.251-.13-.117-.059-.228-.114-.323-.173a11.2,11.2,0,0,1-2.684-2.446,6.607,6.607,0,0,1-.88-1.389c.267-.245.515-.5.757-.743.091-.091.183-.186.274-.277a1.5,1.5,0,0,0,0-2.257L5.3,28.8c-.1-.1-.205-.205-.3-.31-.2-.2-.4-.411-.613-.607a1.544,1.544,0,0,0-1.092-.479,1.6,1.6,0,0,0-1.109.479l-.007.007L1.066,29.01a2.386,2.386,0,0,0-.708,1.516,5.716,5.716,0,0,0,.417,2.42A14.041,14.041,0,0,0,3.27,37.107a15.346,15.346,0,0,0,5.11,4,7.961,7.961,0,0,0,2.87.848c.068,0,.14.007.205.007a2.457,2.457,0,0,0,1.882-.809c0-.007.01-.01.013-.016a7.413,7.413,0,0,1,.571-.59c.14-.134.284-.274.424-.421A1.627,1.627,0,0,0,14.836,39a1.567,1.567,0,0,0-.5-1.118Zm1.167,3.434s0,0,0,0c-.127.137-.258.261-.4.4a8.574,8.574,0,0,0-.629.652,1.572,1.572,0,0,1-1.226.518c-.049,0-.1,0-.15,0a7.073,7.073,0,0,1-2.544-.763,14.485,14.485,0,0,1-4.813-3.77A13.239,13.239,0,0,1,1.6,32.64,4.656,4.656,0,0,1,1.238,30.6a1.5,1.5,0,0,1,.45-.968L2.8,28.518a.741.741,0,0,1,.5-.232.7.7,0,0,1,.476.228l.01.01c.2.186.388.378.587.584.1.1.205.209.31.316l.89.89a.622.622,0,0,1,0,1.011c-.095.095-.186.189-.28.28-.274.28-.535.541-.818.8-.007.007-.013.01-.016.016a.665.665,0,0,0-.17.74l.01.029a7.147,7.147,0,0,0,1.053,1.719l0,0a11.969,11.969,0,0,0,2.9,2.635,4.451,4.451,0,0,0,.4.218c.117.059.228.114.323.173.013.007.026.016.039.023a.707.707,0,0,0,.323.082.7.7,0,0,0,.5-.225L10.943,36.7a.738.738,0,0,1,.492-.245.664.664,0,0,1,.47.238l.007.007,1.8,1.8A.645.645,0,0,1,13.711,39.517Z"
                            transform="translate(-0.344 -26.512)"
                            fill="#374957"
                          />
                          <path
                            id="Path_290"
                            data-name="Path 290"
                            d="M245.306,86.8a4.2,4.2,0,0,1,3.417,3.417.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007.441.441,0,0,0,.362-.509,5.075,5.075,0,0,0-4.135-4.135.443.443,0,0,0-.509.359A.435.435,0,0,0,245.306,86.8Z"
                            transform="translate(-236.968 -83.123)"
                            fill="#374957"
                          />
                          <path
                            id="Path_291"
                            data-name="Path 291"
                            d="M256.1,6.816A8.356,8.356,0,0,0,249.287.007a.44.44,0,1,0-.143.867,7.464,7.464,0,0,1,6.085,6.085.438.438,0,0,0,.434.365.582.582,0,0,0,.075-.007A.432.432,0,0,0,256.1,6.816Z"
                            transform="translate(-240.674 0)"
                            fill="#374957"
                          />
                        </g>
                      </g>
                    </svg>
                  }
                  name="phone"
                  value={phone}
                  onChange={(event) => {
                    const re = /^[0-9\b]+$/;
                    if (
                      event.currentTarget.value === "" ||
                      re.test(event.currentTarget.value)
                    ) {
                      onPhoneNumberChangeListener(event);
                    }
                  }}
                />
              </div>
              <div className="dashboard__company__container__profile__left__container__left">
                <InputBox
                  style={{ backgroundColor: "#ffffff" }}
                  variant="select"
                  placeholder={
                    isOn === "company" ? "Select Industry / Branch" : "Gender"
                  }
                  error={isOn === "company" ? industryTypeError : genderError}
                  errorMessage={
                    isOn === "company"
                      ? industryTypeErrorMessage
                      : genderErrorMessage
                  }
                  svg={
                    isOn === "company" ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="17.064"
                        height="17.078"
                        viewBox="0 0 17.064 17.078"
                      >
                        <path
                          id="Path_21767"
                          data-name="Path 21767"
                          d="M13.581,0V1h1.981L12.99,3.572a4.51,4.51,0,0,0-5.543-.077,4.519,4.519,0,1,0-3.222,8.1V14.4H2.876v1H4.225v1.675h1V15.4H6.576v-1H5.226V11.593a4.5,4.5,0,0,0,2.222-.886A4.517,4.517,0,0,0,13.7,4.28L16.27,1.708V3.69h1V0ZM7.448,9.329a3.512,3.512,0,0,1,0-4.456,3.512,3.512,0,0,1,0,4.456ZM1.207,7.1a3.517,3.517,0,0,1,5.5-2.9,4.51,4.51,0,0,0,0,5.81,3.517,3.517,0,0,1-5.5-2.9Zm8.963,3.519a3.5,3.5,0,0,1-1.984-.614,4.51,4.51,0,0,0,0-5.81,3.519,3.519,0,1,1,1.984,6.424Z"
                          transform="translate(-0.206)"
                          fill="#374957"
                        />
                      </svg>
                    ) : isOn === "freelancer" ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="17.064"
                        height="17.078"
                        viewBox="0 0 17.064 17.078"
                      >
                        <path
                          id="Path_297"
                          data-name="Path 297"
                          d="M13.581,0V1h1.981L12.99,3.572a4.51,4.51,0,0,0-5.543-.077,4.519,4.519,0,1,0-3.222,8.1V14.4H2.876v1H4.225v1.675h1V15.4H6.576v-1H5.226V11.593a4.5,4.5,0,0,0,2.222-.886A4.517,4.517,0,0,0,13.7,4.28L16.27,1.708V3.69h1V0ZM7.448,9.329a3.512,3.512,0,0,1,0-4.456,3.512,3.512,0,0,1,0,4.456ZM1.207,7.1a3.517,3.517,0,0,1,5.5-2.9,4.51,4.51,0,0,0,0,5.81,3.517,3.517,0,0,1-5.5-2.9Zm8.963,3.519a3.5,3.5,0,0,1-1.984-.614,4.51,4.51,0,0,0,0-5.81,3.519,3.519,0,1,1,1.984,6.424Z"
                          transform="translate(-0.206 0)"
                          fill="#374957"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="17.064"
                        height="17.078"
                        viewBox="0 0 17.064 17.078"
                      >
                        <path
                          id="Path_297"
                          data-name="Path 297"
                          d="M13.581,0V1h1.981L12.99,3.572a4.51,4.51,0,0,0-5.543-.077,4.519,4.519,0,1,0-3.222,8.1V14.4H2.876v1H4.225v1.675h1V15.4H6.576v-1H5.226V11.593a4.5,4.5,0,0,0,2.222-.886A4.517,4.517,0,0,0,13.7,4.28L16.27,1.708V3.69h1V0ZM7.448,9.329a3.512,3.512,0,0,1,0-4.456,3.512,3.512,0,0,1,0,4.456ZM1.207,7.1a3.517,3.517,0,0,1,5.5-2.9,4.51,4.51,0,0,0,0,5.81,3.517,3.517,0,0,1-5.5-2.9Zm8.963,3.519a3.5,3.5,0,0,1-1.984-.614,4.51,4.51,0,0,0,0-5.81,3.519,3.519,0,1,1,1.984,6.424Z"
                          transform="translate(-0.206 0)"
                          fill="#374957"
                        />
                      </svg>
                    )
                  }
                  name={isOn === "company" ? "industry" : "gender"}
                  value={isOn === "company" ? selectedIndustry : selectedGender}
                  options={isOn === "company" ? industryType : gender}
                  onChange={(e) =>
                    isOn === "company"
                      ? handleChangeIndustry(e)
                      : handleChangeGender(e)
                  }
                />
              </div>
              <div className="dashboard__company__container__profile__left__container__right">
                {isOn == "company" ? (
                  <InputBox
                    variant="white"
                    placeholder={"No. of Employee"}
                    minValue={0}
                    maxValue={7}
                    error={noOfEmployeeError}
                    errorMessage={noOfEmployeeErrorMessage}
                    svg={
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="13.419"
                        height="17.918"
                        viewBox="0 0 13.419 17.918"
                      >
                        <g id="name" transform="translate(3.582 -1)">
                          <path
                            id="Path_285"
                            data-name="Path 285"
                            d="M145.333,176.081a2.04,2.04,0,1,0-2.04-2.04A2.04,2.04,0,0,0,145.333,176.081Zm0-3.288a1.251,1.251,0,1,1-1.251,1.251A1.251,1.251,0,0,1,145.333,172.793Zm0,0"
                            transform="translate(-142.206 -165.569)"
                            fill="#374957"
                          />
                          <path
                            id="Path_286"
                            data-name="Path 286"
                            d="M111.8,325.122a3.062,3.062,0,0,0-2.218.927,3.273,3.273,0,0,0-.916,2.317.4.4,0,0,0,.395.395h5.478a.4.4,0,0,0,.395-.395,3.273,3.273,0,0,0-.916-2.317A3.062,3.062,0,0,0,111.8,325.122Zm-2.317,2.85a2.414,2.414,0,0,1,.663-1.37,2.325,2.325,0,0,1,3.307,0,2.426,2.426,0,0,1,.663,1.37Zm0,0"
                            transform="translate(-108.674 -313.857)"
                            fill="#374957"
                          />
                          <path
                            id="Path_287"
                            data-name="Path 287"
                            d="M6.864,0H-2.609A1.974,1.974,0,0,0-4.582,1.973V15.945a1.974,1.974,0,0,0,1.973,1.973H6.864a1.974,1.974,0,0,0,1.973-1.973V1.973A1.974,1.974,0,0,0,6.864,0ZM8.048,15.945a1.188,1.188,0,0,1-1.184,1.184H-2.609a1.188,1.188,0,0,1-1.184-1.184V1.973A1.188,1.188,0,0,1-2.609.789H6.864A1.188,1.188,0,0,1,8.048,1.973Zm0,0"
                            transform="translate(1 1)"
                            fill="#374957"
                          />
                          <path
                            id="Path_288"
                            data-name="Path 288"
                            d="M143.688,59.664h3.157a.395.395,0,1,0,0-.789h-3.157a.395.395,0,1,0,0,.789Zm0,0"
                            transform="translate(-142.206 -56.016)"
                            fill="#374957"
                          />
                        </g>
                      </svg>
                    }
                    name={"noOfEmployee"}
                    value={noOfEmployee}
                    onChange={(e) => onNoOfEmployeeChangeListener(e)}
                  />
                ) : (
                  <InputBox
                    variant="date"
                    placeholder="DOB"
                    name="age"
                    error={ageError}
                    errorMessage={ageErrorMessage}
                    value={age}
                    style={{ backgroundColor: "white" }}
                    maxDate={moment(getDate10YearsAgo()).format("YYYY-MM-DD")}
                    svg={
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18.471"
                        height="17.888"
                        viewBox="0 0 18.471 17.888"
                      >
                        <g
                          id="Group_1748"
                          data-name="Group 1748"
                          transform="translate(-977.281 -502.999)"
                        >
                          <path
                            id="Path_298"
                            data-name="Path 298"
                            d="M15.118,22.72A8.4,8.4,0,1,1,13.389,9.892a.271.271,0,0,0,.287-.46,8.949,8.949,0,1,0,1.839,13.656.271.271,0,0,0-.4-.368Z"
                            transform="translate(977.281 494.92)"
                            fill="#374957"
                          />
                          <path
                            id="Path_299"
                            data-name="Path 299"
                            d="M59.516,57.132a.271.271,0,0,0-.249-.163h-.382a8.932,8.932,0,0,0-3.231-5.817.271.271,0,1,0-.342.42,8.388,8.388,0,0,1,3.061,5.694.271.271,0,0,0,.268.245l-.864.92-.864-.92H57.1a.273.273,0,0,0,.269-.3,7.407,7.407,0,1,0-13.658,4.774.271.271,0,1,0,.461-.285,6.865,6.865,0,1,1,12.616-4.728h-.5a.273.273,0,0,0-.2.457l1.491,1.587a.273.273,0,0,0,.4,0l1.491-1.587A.271.271,0,0,0,59.516,57.132Z"
                            transform="translate(936.213 454.316)"
                            fill="#374957"
                          />
                          <path
                            id="Path_300"
                            data-name="Path 300"
                            d="M103.795,312.093h-1.607a.271.271,0,0,0-.257.184,6.867,6.867,0,0,1-11.732,2.24.271.271,0,1,0-.413.351,7.409,7.409,0,0,0,12.593-2.233h1.052a8.346,8.346,0,0,1-1.163,2.315.271.271,0,1,0,.441.315,8.884,8.884,0,0,0,1.347-2.83.271.271,0,0,0-.262-.343Z"
                            transform="translate(890.391 201.643)"
                            fill="#374957"
                          />
                          <path
                            id="Path_301"
                            data-name="Path 301"
                            d="M100.938,206.011a.271.271,0,0,0,.35-.157l.259-.68h1.726l.256.679a.271.271,0,1,0,.507-.191l-1.3-3.432,0-.007a.349.349,0,0,0-.324-.217h0a.349.349,0,0,0-.324.217l0,.006-1.307,3.432A.271.271,0,0,0,100.938,206.011Zm1.476-3.113.655,1.734h-1.315Z"
                            transform="translate(880.153 306.995)"
                            fill="#374957"
                          />
                          <path
                            id="Path_302"
                            data-name="Path 302"
                            d="M318.21,202.988a.271.271,0,1,0,0-.542h-1.453a.271.271,0,0,0-.271.271v3.449a.271.271,0,0,0,.271.271h1.453a.271.271,0,1,0,0-.542h-1.182v-1.183H318.1a.271.271,0,1,0,0-.542h-1.075v-1.183h1.182Z"
                            transform="translate(672.214 306.555)"
                            fill="#374957"
                          />
                          <path
                            id="Path_303"
                            data-name="Path 303"
                            d="M198.346,202.548a1.462,1.462,0,0,1,.821.251.271.271,0,1,0,.3-.449,2.012,2.012,0,1,0-1.125,3.68,1.821,1.821,0,0,0,1.774-2.012.271.271,0,0,0-.271-.271h-1.041a.271.271,0,0,0,0,.542h.755a1.216,1.216,0,0,1-1.217,1.2,1.47,1.47,0,1,1,0-2.939Z"
                            transform="translate(788.03 306.994)"
                            fill="#374957"
                          />
                        </g>
                      </svg>
                    }
                    onChange={(e) => onAgeChangeListener(e)}
                  />
                )}
              </div>
            </div>
            <div className="dashboard__company__container__profile__left__container__wrapper">
              <div className="dashboard__company__container__profile__left__container__left">
                <InputBox
                  variant="white"
                  placeholder="Postal Code"
                  error={postalCodeError}
                  errorMessage={postalCodeErrorMessage}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="9.581"
                      height="13.36"
                      viewBox="0 0 9.581 13.36"
                    >
                      <g id="pin" transform="translate(0.541 0.5)">
                        <path
                          id="Path_280"
                          data-name="Path 280"
                          d="M87.876,2.116A4.194,4.194,0,0,0,84.312,0c-.063,0-.127,0-.19,0a4.2,4.2,0,0,0-3.564,2.116A4.3,4.3,0,0,0,80.5,6.368l3.065,5.61,0,.007a.745.745,0,0,0,1.292,0l0-.007,3.065-5.61a4.3,4.3,0,0,0-.056-4.252ZM84.217,5.6a1.738,1.738,0,1,1,1.738-1.738A1.74,1.74,0,0,1,84.217,5.6Z"
                          transform="translate(-79.968 0)"
                          fill="none"
                          stroke="#374957"
                          strokeWidth="1"
                        />
                      </g>
                    </svg>
                  }
                  name="postalCode"
                  maxValue={10}
                  value={postalCode}
                  onChange={(e) => onPostalCodeChangeListener(e)}
                />
              </div>

              <InputBox
                variant="location"
                style={{ minWidth: "32%", backgroundColor: "#ffffff" }}
                placeholder="Location"
                error={addressError}
                errorMessage={addressErrorMessage}
                svg={
                  <svg
                    id="pin"
                    xmlns="http://www.w3.org/2000/svg"
                    width="7.983"
                    height="11.61"
                    viewBox="0 0 7.983 11.61"
                  >
                    <path
                      id="Path_280"
                      data-name="Path 280"
                      d="M87.4,1.988A3.94,3.94,0,0,0,84.049,0c-.059,0-.119,0-.179,0a3.94,3.94,0,0,0-3.348,1.987,4.042,4.042,0,0,0-.053,3.994l2.879,5.269,0,.007a.7.7,0,0,0,1.214,0l0-.007,2.879-5.269A4.042,4.042,0,0,0,87.4,1.988ZM83.959,5.26a1.633,1.633,0,1,1,1.633-1.633A1.634,1.634,0,0,1,83.959,5.26Z"
                      transform="translate(-79.968 0)"
                      fill="#374957"
                    />
                  </svg>
                }
                value={address}
                onChange={(e) => handleChangelocation(e, true)}
                onSelected={(e) => handleChangelocation(e, false)}
              />

              {isOn != "company" ? (
                <InputBox
                  variant="select"
                  placeholder="Language"
                  style={{ backgroundColor: "white", height: "fit-content" }}
                  error={false}
                  errorMessage="hello"
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="12.499"
                      height="16.008"
                      viewBox="0 0 12.499 16.008"
                    >
                      <g id="germany" transform="translate(-56)">
                        <path
                          id="Path_654"
                          data-name="Path 654"
                          d="M215.5,352h.5v.47h-.5Z"
                          transform="translate(-154.503 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_655"
                          data-name="Path 655"
                          d="M247.5,352h.5v.47h-.5Z"
                          transform="translate(-185.501 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_656"
                          data-name="Path 656"
                          d="M279.5,352h.5v.47h-.5Z"
                          transform="translate(-216.499 -340.973)"
                          fill="#374957"
                        />
                        <path
                          id="Path_657"
                          data-name="Path 657"
                          d="M67.788,6.861A6.3,6.3,0,0,0,65.9,4.682l-.274.381a5.839,5.839,0,0,1,1.413,1.453H65.457a6.863,6.863,0,0,0-.542-1.16,4.631,4.631,0,0,0,.577-2.114,3.242,3.242,0,1,0-6.485,0,4.631,4.631,0,0,0,.577,2.114,6.865,6.865,0,0,0-.542,1.16H57.465a5.839,5.839,0,0,1,1.413-1.453L58.6,4.682a6.248,6.248,0,1,0,9.185,2.179Zm-5.774,8.662a2.958,2.958,0,0,1-2.043-1.63A6.32,6.32,0,0,1,59.544,13h2.47Zm.47,0V13h2.47a6.318,6.318,0,0,1-.427.892A2.958,2.958,0,0,1,62.485,15.523Zm0-2.992v-.517h-.47v.517H59.38a9.23,9.23,0,0,1-.4-2.537h3.036v.517h.47V9.993H65.52a9.23,9.23,0,0,1-.4,2.537ZM56.475,9.993h2.034a9.791,9.791,0,0,0,.38,2.537H57.18a5.74,5.74,0,0,1-.7-2.537Zm4.21-3.007a12.5,12.5,0,0,0,1.329,1.376V9.523H58.979a9.321,9.321,0,0,1,.4-2.537Zm3.129,0h1.3a9.321,9.321,0,0,1,.4,2.537H62.485V8.362A12.5,12.5,0,0,0,63.814,6.986ZM65.99,9.993h2.034a5.74,5.74,0,0,1-.7,2.537H65.61A9.791,9.791,0,0,0,65.99,9.993Zm1.331-3.007c.017.031.034.062.051.093a5.79,5.79,0,0,1,.652,2.444H65.99a9.772,9.772,0,0,0-.379-2.537Zm-2.369-.47h-.77q.256-.345.462-.681A6.478,6.478,0,0,1,64.953,6.516ZM62.25.47a2.776,2.776,0,0,1,2.772,2.772c0,2.051-2.175,4.159-2.772,4.7-.6-.54-2.772-2.646-2.772-4.7A2.776,2.776,0,0,1,62.25.47ZM59.855,5.835q.206.336.462.681h-.77A6.477,6.477,0,0,1,59.855,5.835ZM57.127,7.079c.016-.031.034-.062.051-.093h1.71a9.773,9.773,0,0,0-.379,2.537H56.475A5.789,5.789,0,0,1,57.127,7.079ZM57.467,13h1.576a6.9,6.9,0,0,0,.523,1.129,4.727,4.727,0,0,0,.884,1.121A5.806,5.806,0,0,1,57.467,13Zm6.583,2.25a4.728,4.728,0,0,0,.884-1.121A6.9,6.9,0,0,0,65.456,13h1.576A5.806,5.806,0,0,1,64.05,15.25Z"
                          transform="translate(0)"
                          fill="#374957"
                        />
                        <path
                          id="Path_658"
                          data-name="Path 658"
                          d="M188.48,34.24a2.24,2.24,0,1,0-2.24,2.24A2.242,2.242,0,0,0,188.48,34.24Zm-2.24-1.77a1.77,1.77,0,0,1,1.469.783h-2.937A1.77,1.77,0,0,1,186.24,32.47Zm-1.693,1.253h3.386a1.769,1.769,0,0,1,0,1.034h-3.386a1.769,1.769,0,0,1,0-1.034Zm.224,1.5h2.937a1.769,1.769,0,0,1-2.937,0Z"
                          transform="translate(-123.99 -30.998)"
                          fill="#374957"
                        />
                      </g>
                    </svg>
                  }
                  options={language}
                  value={selectedLanguage}
                  isMulti
                  onChange={(event) => {
                    setSelectedLanguage(event);
                  }}
                />
              ) : null}

              <div className="dashboard__company__container__profile__left__container__right">
                <button
                  className="header__nav__btn btn__primary"
                  style={{ height: 50, width: "100%", marginBottom: 20 }}
                  title="upload"
                >
                  <input
                    type="file"
                    accept="image/png, image/jpg, image/jpeg, image/heic"
                    multiple={false}
                    onChange={(e) => fetchImage(e.currentTarget.files)}
                  />
                  {isOn === "company" ? "Upload Logo" : " Upload Photo"}
                </button>
              </div>

              {/* <InputBox
                variant="white"
                placeholder="Address"
                error={addressError}
                errorMessage={addressErrorMessage}
                svg={
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="9.581"
                    height="13.36"
                    viewBox="0 0 9.581 13.36"
                  >
                    <g id="pin" transform="translate(0.541 0.5)">
                      <path
                        id="Path_280"
                        data-name="Path 280"
                        d="M87.876,2.116A4.194,4.194,0,0,0,84.312,0c-.063,0-.127,0-.19,0a4.2,4.2,0,0,0-3.564,2.116A4.3,4.3,0,0,0,80.5,6.368l3.065,5.61,0,.007a.745.745,0,0,0,1.292,0l0-.007,3.065-5.61a4.3,4.3,0,0,0-.056-4.252ZM84.217,5.6a1.738,1.738,0,1,1,1.738-1.738A1.74,1.74,0,0,1,84.217,5.6Z"
                        transform="translate(-79.968 0)"
                        fill="none"
                        stroke="#374957"
                        strokeWidth="1"
                      />
                    </g>
                  </svg>
                }
                name="address"
                onChange={(e) => handleValueChange(e)}
              /> */}
              {isOn === "company" ? (
                <InputBox
                  variant="textarea-white"
                  placeholder="Write Company Description"
                  error={descriptionError}
                  errorMessage={descriptionErrorMessage}
                  svg={
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="9.581"
                      height="13.36"
                      viewBox="0 0 9.581 13.36"
                    >
                      <g id="pin" transform="translate(0.541 0.5)">
                        <path
                          id="Path_280"
                          data-name="Path 280"
                          d="M87.876,2.116A4.194,4.194,0,0,0,84.312,0c-.063,0-.127,0-.19,0a4.2,4.2,0,0,0-3.564,2.116A4.3,4.3,0,0,0,80.5,6.368l3.065,5.61,0,.007a.745.745,0,0,0,1.292,0l0-.007,3.065-5.61a4.3,4.3,0,0,0-.056-4.252ZM84.217,5.6a1.738,1.738,0,1,1,1.738-1.738A1.74,1.74,0,0,1,84.217,5.6Z"
                          transform="translate(-79.968 0)"
                          fill="none"
                          stroke="#374957"
                          strokeWidth="1"
                        />
                      </g>
                    </svg>
                  }
                  value={description}
                  name="description"
                  onChange={(value) =>
                    setDescriptionErrorMessageAndVisibilityAndSetText(value)
                  }
                />
              ) : null}
            </div>

            {isOn === "company" ? (
              <>
                <div>
                  <div
                    className="homepage__container__jumbotron__form__filters__role homepage__container__jumbotron__form__filters__role__white"
                    style={{ justifyContent: "flex-start", marginLeft: ".5em" }}
                  >
                    <input
                      className="styled-checkbox"
                      id="styled-checkbox-different-billing-address"
                      type="checkbox"
                      value="Remember"
                      name="Remember"
                      onChange={(e) => {
                        e.target.checked
                          ? setIsDifferentBillingAddress(true)
                          : setIsDifferentBillingAddress(false);
                      }}
                    />
                    <label htmlFor="styled-checkbox-different-billing-address">
                      Different Billing Address
                    </label>
                  </div>
                  {isDifferentBillingAddress ? (
                    <>
                      <div className="dashboard__company__container__profile__left__container__wrapper">
                        <div className="dashboard__company__container__profile__left__container__left">
                          {/* <InputBox
                            variant="select"
                            placeholder="Country"
                            style={{ backgroundColor: "white" }}
                            svg={
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="12.499"
                                height="16.008"
                                viewBox="0 0 12.499 16.008"
                              >
                                <g id="germany" transform="translate(-56)">
                                  <path
                                    id="Path_654"
                                    data-name="Path 654"
                                    d="M215.5,352h.5v.47h-.5Z"
                                    transform="translate(-154.503 -340.973)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_655"
                                    data-name="Path 655"
                                    d="M247.5,352h.5v.47h-.5Z"
                                    transform="translate(-185.501 -340.973)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_656"
                                    data-name="Path 656"
                                    d="M279.5,352h.5v.47h-.5Z"
                                    transform="translate(-216.499 -340.973)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_657"
                                    data-name="Path 657"
                                    d="M67.788,6.861A6.3,6.3,0,0,0,65.9,4.682l-.274.381a5.839,5.839,0,0,1,1.413,1.453H65.457a6.863,6.863,0,0,0-.542-1.16,4.631,4.631,0,0,0,.577-2.114,3.242,3.242,0,1,0-6.485,0,4.631,4.631,0,0,0,.577,2.114,6.865,6.865,0,0,0-.542,1.16H57.465a5.839,5.839,0,0,1,1.413-1.453L58.6,4.682a6.248,6.248,0,1,0,9.185,2.179Zm-5.774,8.662a2.958,2.958,0,0,1-2.043-1.63A6.32,6.32,0,0,1,59.544,13h2.47Zm.47,0V13h2.47a6.318,6.318,0,0,1-.427.892A2.958,2.958,0,0,1,62.485,15.523Zm0-2.992v-.517h-.47v.517H59.38a9.23,9.23,0,0,1-.4-2.537h3.036v.517h.47V9.993H65.52a9.23,9.23,0,0,1-.4,2.537ZM56.475,9.993h2.034a9.791,9.791,0,0,0,.38,2.537H57.18a5.74,5.74,0,0,1-.7-2.537Zm4.21-3.007a12.5,12.5,0,0,0,1.329,1.376V9.523H58.979a9.321,9.321,0,0,1,.4-2.537Zm3.129,0h1.3a9.321,9.321,0,0,1,.4,2.537H62.485V8.362A12.5,12.5,0,0,0,63.814,6.986ZM65.99,9.993h2.034a5.74,5.74,0,0,1-.7,2.537H65.61A9.791,9.791,0,0,0,65.99,9.993Zm1.331-3.007c.017.031.034.062.051.093a5.79,5.79,0,0,1,.652,2.444H65.99a9.772,9.772,0,0,0-.379-2.537Zm-2.369-.47h-.77q.256-.345.462-.681A6.478,6.478,0,0,1,64.953,6.516ZM62.25.47a2.776,2.776,0,0,1,2.772,2.772c0,2.051-2.175,4.159-2.772,4.7-.6-.54-2.772-2.646-2.772-4.7A2.776,2.776,0,0,1,62.25.47ZM59.855,5.835q.206.336.462.681h-.77A6.477,6.477,0,0,1,59.855,5.835ZM57.127,7.079c.016-.031.034-.062.051-.093h1.71a9.773,9.773,0,0,0-.379,2.537H56.475A5.789,5.789,0,0,1,57.127,7.079ZM57.467,13h1.576a6.9,6.9,0,0,0,.523,1.129,4.727,4.727,0,0,0,.884,1.121A5.806,5.806,0,0,1,57.467,13Zm6.583,2.25a4.728,4.728,0,0,0,.884-1.121A6.9,6.9,0,0,0,65.456,13h1.576A5.806,5.806,0,0,1,64.05,15.25Z"
                                    transform="translate(0)"
                                    fill="#374957"
                                  />
                                  <path
                                    id="Path_658"
                                    data-name="Path 658"
                                    d="M188.48,34.24a2.24,2.24,0,1,0-2.24,2.24A2.242,2.242,0,0,0,188.48,34.24Zm-2.24-1.77a1.77,1.77,0,0,1,1.469.783h-2.937A1.77,1.77,0,0,1,186.24,32.47Zm-1.693,1.253h3.386a1.769,1.769,0,0,1,0,1.034h-3.386a1.769,1.769,0,0,1,0-1.034Zm.224,1.5h2.937a1.769,1.769,0,0,1-2.937,0Z"
                                    transform="translate(-123.99 -30.998)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            }
                            options={country}
                            value={billingAddressCountry}
                            onChange={(e) => {
                              setBillingAddressCountry(e);
                              setCountryId(e.value);
                              setBillingAddressCity("");
                            }}
                          /> */}
                          <InputBox
                            variant="white"
                            placeholder="Postal code"
                            error={billingPostalCodeError}
                            errorMessage={billingPostalCodeErrorMessage}
                            svg={
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="9.581"
                                height="13.36"
                                viewBox="0 0 9.581 13.36"
                              >
                                <g id="pin" transform="translate(0.541 0.5)">
                                  <path
                                    id="Path_280"
                                    data-name="Path 280"
                                    d="M87.876,2.116A4.194,4.194,0,0,0,84.312,0c-.063,0-.127,0-.19,0a4.2,4.2,0,0,0-3.564,2.116A4.3,4.3,0,0,0,80.5,6.368l3.065,5.61,0,.007a.745.745,0,0,0,1.292,0l0-.007,3.065-5.61a4.3,4.3,0,0,0-.056-4.252ZM84.217,5.6a1.738,1.738,0,1,1,1.738-1.738A1.74,1.74,0,0,1,84.217,5.6Z"
                                    transform="translate(-79.968 0)"
                                    fill="none"
                                    stroke="#374957"
                                    strokeWidth="1"
                                  />
                                </g>
                              </svg>
                            }
                            name="billingPostalCode"
                            value={billingPostalCode}
                            onChange={(e) =>
                              setBillingPostalCodeErrorMessageAndVisibilityAndSetText(
                                e.currentTarget.value
                              )
                            }
                          />
                        </div>
                        {/* <div className="dashboard__company__container__profile__left__container__right">
                          <InputBox
                            variant="select"
                            placeholder="City"
                            svg={
                              <svg
                                id="Group_204"
                                data-name="Group 204"
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.43"
                                height="18.373"
                                viewBox="0 0 18.43 18.373"
                              >
                                <g id="Group_203" data-name="Group 203">
                                  <path
                                    id="Path_304"
                                    data-name="Path 304"
                                    d="M18.157,14.664h-1.8V12.427a.274.274,0,1,0-.547,0v6.194h-4.7V10.49h4.7v.66a.274.274,0,1,0,.547,0v-.934a.274.274,0,0,0-.274-.274H12.34V1.07A.274.274,0,0,0,12.066.8H6.373A.274.274,0,0,0,6.1,1.07V5.164H2.558a.274.274,0,0,0-.274.274v6.73H.274A.274.274,0,0,0,0,12.442V18.9a.274.274,0,0,0,.274.274H18.157a.274.274,0,0,0,.274-.274V14.938A.274.274,0,0,0,18.157,14.664ZM2.285,18.622H.547V12.715H2.285Zm5.693,0H2.832V17.108H7.978Zm0-3.9H5.511a.274.274,0,0,0,0,.547H7.978v1.3H2.832v-1.3h1.4a.274.274,0,0,0,0-.547h-1.4v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832v-1.3H7.978Zm0-1.843H2.832V5.712H7.978Zm2.583,2.87v8.406H8.525V5.438a.274.274,0,0,0-.274-.274H7.878V4.617a.274.274,0,1,0-.547,0v.547H6.647V1.343h5.146v8.6h-.958A.274.274,0,0,0,10.561,10.216Zm7.322,8.405h-1.53v-3.41h1.53Z"
                                    transform="translate(0 -0.796)"
                                    fill="#374957"
                                  />
                                </g>
                              </svg>
                            }
                            options={city}
                            value={billingAddressCity}
                            onChange={(e) => setBillingAddressCity(e)}
                          />
                        </div> */}
                      </div>

                      <InputBox
                        variant="location"
                        style={{ minWidth: "32%", backgroundColor: "white" }}
                        placeholder="Location"
                        error={billingAddressError}
                        errorMessage={billingAddressErrorMessage}
                        svg={
                          <svg
                            id="pin"
                            xmlns="http://www.w3.org/2000/svg"
                            width="7.983"
                            height="11.61"
                            viewBox="0 0 7.983 11.61"
                          >
                            <path
                              id="Path_280"
                              data-name="Path 280"
                              d="M87.4,1.988A3.94,3.94,0,0,0,84.049,0c-.059,0-.119,0-.179,0a3.94,3.94,0,0,0-3.348,1.987,4.042,4.042,0,0,0-.053,3.994l2.879,5.269,0,.007a.7.7,0,0,0,1.214,0l0-.007,2.879-5.269A4.042,4.042,0,0,0,87.4,1.988ZM83.959,5.26a1.633,1.633,0,1,1,1.633-1.633A1.634,1.634,0,0,1,83.959,5.26Z"
                              transform="translate(-79.968 0)"
                              fill="#374957"
                            />
                          </svg>
                        }
                        value={billingAddress}
                        onChange={(e) => handleChangelocationBilling(e, true)}
                        onSelected={(e) =>
                          handleChangelocationBilling(e, false)
                        }
                      />

                      {/* <InputBox
                        variant="white"
                        placeholder="Address"
                        svg={
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="9.581"
                            height="13.36"
                            viewBox="0 0 9.581 13.36"
                          >
                            <g id="pin" transform="translate(0.541 0.5)">
                              <path
                                id="Path_280"
                                data-name="Path 280"
                                d="M87.876,2.116A4.194,4.194,0,0,0,84.312,0c-.063,0-.127,0-.19,0a4.2,4.2,0,0,0-3.564,2.116A4.3,4.3,0,0,0,80.5,6.368l3.065,5.61,0,.007a.745.745,0,0,0,1.292,0l0-.007,3.065-5.61a4.3,4.3,0,0,0-.056-4.252ZM84.217,5.6a1.738,1.738,0,1,1,1.738-1.738A1.74,1.74,0,0,1,84.217,5.6Z"
                                transform="translate(-79.968 0)"
                                fill="none"
                                stroke="#374957"
                                strokeWidth="1"
                              />
                            </g>
                          </svg>
                        }
                        name="billingAddress"
                        onChange={(e) =>
                          setBillingAddress(e.currentTarget.value)
                        }
                      /> */}
                    </>
                  ) : null}
                </div>
              </>
            ) : (
              <div style={{ marginTop: "0em" }}></div>
            )}

            {imageToUpload != null || urlImage != null ? (
              <div className="pupup__container__from__wrapper__form__col__upload__btn__active__wrapper">
                <div
                  style={{
                    borderRadius: 10,
                    marginTop: 20,
                    marginBottom: 20,
                    maxWidth: 170,
                    maxHeight: 170,
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <Img
                    loading="lazy"
                    src={
                      imageToUpload != null
                        ? imageToUpload
                        : urlImage != null
                        ? process.env.REACT_APP_BASEURL.concat(urlImage) +
                          "?" +
                          new Date()
                        : pictureplaceholder
                    }
                    alt="project picture"
                    className="pupup__container__from__wrapper__form__col__upload__btn__active__img"
                  />
                  {/* <button
                    className="header__nav__btn btn__secondary"
                    style={{
                      width: 100,
                      minHeight: 40,
                      marginTop: 10,
                      marginRight: 0,
                      background: "linear-gradient(#DB1414,#FF0E0E)",
                    }}
                  >
                    Delete
                  </button> */}
                </div>
              </div>
            ) : null}
          </div>
        </div>
        <div className="dashboard__company__container__right">
          <Img
            loading="lazy"
            src={FreelancerProfileDetailsSvg}
            alt="FreelancerProfileDetailsSvg"
            style={{ width: "70%", marginLeft: "4em", marginTop: "4em" }}
          />
        </div>
      </div>
    </>
  );
};

export default PersonalDetails;
