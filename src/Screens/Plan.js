import React, { useState, useEffect } from "react";
import { Head } from "Components";
import { createPayment, getPlans } from "../API/Plans";
import { useSelector } from "react-redux";
import { planSvg } from "Assets";
import moment from "moment";
import Img from "react-cool-img";
import Plan4 from "Components/Plan4";
import Plan3 from "Components/Plan3";
import Plan2 from "Components/Plan2";
import Plan1 from "Components/Plan1";

export default function Plan({ setIsCustomizedRequirmentsOpen }) {
  const { company } = useSelector((state) => state.company);
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedData, setSelectedData] = useState();
  const [apiData, setApiData] = useState();
  const [noCredit, setNoCredit] = useState(false);
  const [noSubscription, setNoSubscription] = useState(false);

  useEffect(() => {
    getPlans()
      .then(({ data }) => {
        setData(data.result);
        setIsLoading(false);
      })
      .catch((e) => {
        setIsLoading(false);
      });
  }, []);

  const handlePlanData = (data) => {
    setIsLoading(true);
    setSelectedData(data);
    callCreatePaymentAPI(data);
  };
  const callCreatePaymentAPI = (data) => {
    let data1 = {
      currency: "EUR",
      value: data.Price,
      paymentMode: "test",
      description: `${data.Title}-${data.Description}`,
      // redirectUrl: "http://localhost:3000/PaymentProcessing",
      redirectUrl:
        process.env.REACT_APP_NAME == "production"
          ? "https://aidapro.com/home-company/plan"
          : "http://localhost:3000/home-company/plan",
      Status: 1,
      companyProfileId: company.Id,
      PlanDetailId: data.Id,
    };

    createPayment(data1)
      .then(({ data }) => {
        setApiData(data);
        setIsLoading(false);
        if (data.status === 200) {
          localStorage.setItem("TransactionId", data.result.Id);
          let url =
            data &&
            data.result &&
            data.result._links &&
            data.result._links.Checkout &&
            data.result._links.Checkout.Href &&
            data.result._links.Checkout.Href;
          window.open(url, "_blank");
          //window.location.href = url;
        }
        if (data.status === 521) {
          setNoSubscription(true);
        }
        if (data.status === 522) {
          setNoCredit(true);
        }
      })
      .catch((e) => {
        setIsLoading(false);
      });
  };

  return (
    <>
      <Head title="AIDApro | Plans & Pricing" description="Plans & Pricing" />
      <section className="plans__container">
        <div
          className="plans__container__row"
          style={{ flexDirection: "column" }}
        >
          <div className="plans__container__row__header">
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <Img
                loading="lazy"
                style={{ marginRight: 12, marginBottom: 15, height: 30 }}
                src={planSvg}
                alt="planSvg"
              />
              <div className="plans__container__row__header__heading">
                <span>Plans & Pricing</span>
              </div>
            </div>
            <div
              className="plans__container__row__header__heading"
              style={{
                color: "#0ca69d",
                fontWeight: "bold",
              }}
            >
              {moment(new Date()).format("YYYY")} Promotion
            </div>
          </div>
          <div className="plans__container__row__main">
            <Plan1 data={data} handlePlanData={handlePlanData} />
            <Plan2 data={data} handlePlanData={handlePlanData} />
            <div className="plans__container__row__main__col">
              <Plan3 />
              <Plan4
                setIsCustomizedRequirmentsOpen={setIsCustomizedRequirmentsOpen}
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
