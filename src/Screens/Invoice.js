import React, { useEffect } from "react";
import { Head } from "Components";
import InvoiceTemplate from "Components/InvoiceTemplate";

export default function Invoice({ setIsInvoiceOpen }) {
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Invoice" description="Invoice" />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "1200px", padding: ".5em", height: "90vh" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsInvoiceOpen(false);
            }}
            title="close popup"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="14.372"
              height="14.372"
              viewBox="0 0 14.372 14.372"
            >
              <defs>
                <linearGradient
                  id="linear-gradient"
                  x1="0.5"
                  x2="0.5"
                  y2="1"
                  gradientUnits="objectBoundingBox"
                >
                  <stop offset="0" stopColor="#0ee1a3" />
                  <stop offset="1" stopColor="#0DCBA0" />
                </linearGradient>
              </defs>
              <path
                id="Icon_metro-cross"
                data-name="Icon metro-cross"
                d="M16.812,13.474h0l-4.36-4.36,4.36-4.36h0a.45.45,0,0,0,0-.635l-2.06-2.06a.45.45,0,0,0-.635,0h0l-4.36,4.36L5.4,2.059h0a.45.45,0,0,0-.635,0L2.7,4.119a.45.45,0,0,0,0,.635h0l4.36,4.36L2.7,13.474h0a.45.45,0,0,0,0,.635l2.06,2.06a.45.45,0,0,0,.635,0h0l4.36-4.36,4.36,4.36h0a.45.45,0,0,0,.635,0l2.06-2.06a.45.45,0,0,0,0-.635Z"
                transform="translate(-2.571 -1.928)"
                fill="url(#linear-gradient)"
              />
            </svg>
          </button>
          <InvoiceTemplate />
        </form>
      </div>
    </>
  );
}
