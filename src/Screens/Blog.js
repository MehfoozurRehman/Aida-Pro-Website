import React, { useState, useEffect, useContext } from "react";
import Header from "Components/Header";
import { navigate } from "@reach/router";
import { Head } from "Components";
import { getBlog } from "../API/Blogs";
import UserContext from "Context/UserContext";
import GetStartedBanner from "Components/GetStartedBanner";
import BlogJumbotron from "Components/BlogJumbotron";
import BlogListing from "Components/BlogListing";

export default function Blog({ setIsLoginOpen, selectedBlogData }) {
  const user = useContext(UserContext);
  //#region Variables
  const [pageNumber, setPageNumber] = useState(1);
  const [limit, setLimit] = useState(9);
  const [totalRecords, setTotalRecords] = useState();
  const [blogApiData, setBlogApiData] = useState([]);
  const [isLogin, setIsLogin] = useState(false);
  //#endregion
  //#region API Calls
  useEffect(() => {
    getBlogsDataApiCall();
    if (Object.keys(user).length > 0) setIsLogin(true);
  }, []);

  const getBlogsDataApiCall = () => {
    getBlog(limit, pageNumber).then((res) => {
      if (res.data.errors === null) {
        setBlogApiData(res.data.result);
        setTotalRecords(res.data.totalRecords);
        setPageNumber(res.data.page);
      }
    });
  };
  //#endregion

  const navigateToBlogArticle = (data) => {
    selectedBlogData(data);
    navigate("/blog-artical/");
  };

  return (
    <>
      <Head title="AIDApro | Blog" description="Blog" />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <BlogJumbotron />
      <div className="homepage__container">
        <BlogListing
          blogApiData={blogApiData}
          navigateToBlogArticle={navigateToBlogArticle}
          pageNumber={pageNumber}
          limit={limit}
          totalRecords={totalRecords}
          setPageNumber={setPageNumber}
        />
        {!isLogin ? <GetStartedBanner /> : null}
      </div>
    </>
  );
}
