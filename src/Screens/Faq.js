import React, { useEffect, useState } from "react";
import Header from "Components/Header";
import { Head } from "Components";
import { Link } from "@reach/router";
import FAQQuestions from "Components/FAQQuestions";
import FAQTab from "Components/FAQTab";

export default function Faq({ setIsLoginOpen, location }) {
  let [isCompany, setIsCompany] = useState(false);
  let [isProfessional, setIsProfessional] = useState(false);
  let [isFreelancer, setIsFreelancer] = useState(false);

  const [loggedInUser, setLoggedInUser] = useState(location.state.loggedInUser);

  const faqCompany = [
    {
      question: "How to get started with AIDApro",
      title: "Get started",
      info: [
        {
          paragraph:
            "Welcome to AIDApro, your dedicated community of AI or data professionals and freelancers. Connecting you to the right AI and data professionals/freelancers is our main goal.",
        },
        {
          paragraph:
            "Registration is simple, as is placing a job or project for your search. We also simplified the option to just browse through our platform for candidates. Search filters and mapping can help you to get better results. Of course, there is also an overview of candidates who applied or are interested in your job or project. Simply message or video with the ones you like most. Optionally, you can let us assist you in screening or recruiting candidates.",
        },
      ],
    },
    {
      question: "How to register with AIDApro",
      title: "How to register",
      info: [
        {
          paragraph:
            "•	Fill in a short signup form and create your username and password. Please provide a valid email address and confirm that you read our terms and conditions.",
        },
        {
          paragraph:
            "•	When you submit the registration form, we will send a verification email to the email address you provided. Follow the link in the email to activate your account.",
        },
        {
          paragraph:
            "•	Once logged in you can finish your profile. Complete your profile to let the professionals and freelancers know who you are and what you're looking for. ",
        },
      ],
    },
    {
      question: "How to communicate with candidates",
      title: "Communicate with candidates",
      info: [
        {
          paragraph:
            "With AIDApro you can message and videocall professionals or freelancers. Our own secured platform allows you upon your convenience to contact interesting candidates. Profiles of all our candidates also feature contact details such as phone, email and social media.",
        },
      ],
    },
    {
      question: "How to get screening or recruitment assistance?",
      title: "Need screening or recruitment assistance?",
      info: [
        {
          paragraph:
            "Optionally, AIDApro can assist you in your search for professionals or freelancers. With literally decades of screening and recruitment experiences we can assist you in either screening a candidate as per your requirements. Or even perform a full recruitment search for your needs.",
        },
        {
          paragraph:
            "Once logged in simply go to Plans and click on screening or recruitment to receive an offer without any obligations.",
        },
      ],
    },
    {
      question: "Why AIDApro",
      title: "Why AIDApro",
      info: [
        {
          paragraph:
            "For all your AI and data needs, AIDApro is THE platform to match you with the right professionals or freelancers.",
        },
      ],
    },
    {
      question: "Fees",
      title: "Fees",
      info: [
        {
          paragraph:
            "Our normal fees are clear and without any hidden costs mentioned under Plans, clickable once you signed in. As recruitment services are fully customized, please click under Plans on Recruitment and fill in your needs. AIDApro will get back to you, free of charge, with a customized offer.",
        },
        {
          paragraph:
            "Kindly note our introduction offer in 2022 which allows you to use all platform features, search, job postings for free. For a limited number of companies and period only.",
        },
      ],
    },
  ];
  useEffect(() => {
    const testimonials = document.getElementById(
      "homepage__container__jobs__projects__penel__container__details__tabs"
    );
    testimonials.addEventListener("wheel", (e) => {
      e.preventDefault();
      testimonials.scrollLeft += e.deltaY;
    });
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Frequently Asked Questions"
        description="Frequently Asked Questions"
      />
      <Header setIsLoginOpen={setIsLoginOpen} />
      <div className="faq__container">
        <div className="faq__container__content">
          <div className="faq__container__content__left">
            <div className="faq__container__content__heading">
              Frequently Asked Questions
            </div>
            <div
              className="homepage__container__jobs__projects__penel__container__details__tabs"
              style={{
                width: "fit-content",
                marginTop: "0em",
              }}
              id="homepage__container__jobs__projects__penel__container__details__tabs"
            >
              <FAQTab
                label="Company"
                onClick={() => {
                  setIsProfessional(false);
                  setIsCompany(true);
                  setIsFreelancer(false);
                }}
                defaultChecked={loggedInUser == "Company" ? true : false}
              />
              <FAQTab
                label="Professional"
                onClick={() => {
                  setIsProfessional(true);
                  setIsCompany(false);
                  setIsFreelancer(false);
                }}
                defaultChecked={loggedInUser == "Professional" ? true : false}
              />
              <FAQTab
                label="Freelancer"
                onClick={() => {
                  setIsFreelancer(true);
                  setIsProfessional(false);
                  setIsCompany(false);
                }}
                defaultChecked={loggedInUser == "Freelancer" ? true : false}
              />
            </div>
            {isCompany ? (
              <div className="faq__container__content__questions__container">
                <div className="faq__container__content__questions__container__card">
                  {faqCompany.map((item, i) => (
                    <FAQQuestions key={i} data={item} />
                  ))}
                </div>
              </div>
            ) : isProfessional ? (
              <div className="faq__container__content__questions__container">
                <div className="faq__container__content__questions__container__card">
                  {faqCompany.map((item, i) => (
                    <FAQQuestions key={i} data={item} />
                  ))}
                </div>
              </div>
            ) : isFreelancer ? (
              <div className="faq__container__content__questions__container">
                <div className="faq__container__content__questions__container__card">
                  {faqCompany.map((item, i) => (
                    <FAQQuestions key={i} data={item} />
                  ))}
                </div>
              </div>
            ) : (
              <div
                className="faq__container__content__left__main__heading"
                style={{
                  width: "100%",
                  height: 300,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontWeight: "normal",
                  textAlign: "center",
                }}
              >
                Please select role for frequently asked questions
              </div>
            )}
          </div>
          <div className="faq__container__content__right">
            <div className="faq__container__content__right__content__heading">
              Other questions?
            </div>
            <div className="faq__container__content__right__content__text">
              Simply fill in the contact form. We’ll get back to you asap.
            </div>
            <Link
              to="/contact"
              className="header__nav__btn btn__secondary"
              style={{
                width: "100%",
                height: 45,
                fontSize: 15,
                margin: "0px auto",
              }}
            >
              Contact Us
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
