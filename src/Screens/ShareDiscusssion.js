import { crossSvg } from "Assets";
import React, { useEffect, useState } from "react";
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  WhatsappShareButton,
  WhatsappIcon,
} from "react-share";

export default function ShareDiscusssion({ onClose, onConfirm, onDecline }) {
  const [copied, setCopied] = useState(false);
  const [forumURL, setForumURL] = useState(
    window.location.origin +
      "/coffee-corner-discussion/" +
      localStorage.getItem("forumId")
  );

  const copyLink = () => {
    /* Get the text field */
    var copyText = document.getElementById("forumURLInput");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyText.value);
    setCopied(true);
  };

  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  return (
    <>
      <input type="text" value={forumURL} id="forumURLInput"></input>
      <div className="pupup__container">
        <div
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "450px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              onClose(false);
              localStorage.removeItem("forumId");
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__top">
            <div
              className="pupup__container__from__top__left"
              style={{ width: "100%" }}
            >
              <div
                className="homepage__container__jumbotron__heading animate__animated animate__fadeInDown"
                style={{
                  fontSize: "25px",
                  textAlign: "center",
                  marginTop: 0,
                }}
              >
                Share this discussion
              </div>
              <div className="coffee__corner__new__discussion__share__links">
                <FacebookShareButton
                  url={
                    "https://aida-pro.web.app/coffee-corner-discussion/" +
                    window.localStorage.getItem("forumId")
                  }
                >
                  <button
                    className="coffee__corner__new__discussion__share__links__link"
                    title="facebook link"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-facebook"
                    >
                      <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                    </svg>
                  </button>
                </FacebookShareButton>

                {/* <button
                  className="coffee__corner__new__discussion__share__links__link"
                  title="instagram link"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-instagram"
                  >
                    <rect
                      x="2"
                      y="2"
                      width="20"
                      height="20"
                      rx="5"
                      ry="5"
                    ></rect>
                    <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                    <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                  </svg>
                </button> */}

                <LinkedinShareButton
                  url={
                    "https://aida-pro.web.app/coffee-corner-discussion/" +
                    window.localStorage.getItem("forumId")
                  }
                >
                  <button
                    className="coffee__corner__new__discussion__share__links__link"
                    title="linkedin link"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-linkedin"
                    >
                      <path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path>
                      <rect x="2" y="9" width="4" height="12"></rect>
                      <circle cx="4" cy="4" r="2"></circle>
                    </svg>
                  </button>
                </LinkedinShareButton>

                <TwitterShareButton
                  url={
                    "https://aida-pro.web.app/coffee-corner-discussion/" +
                    window.localStorage.getItem("forumId")
                  }
                >
                  <button
                    className="coffee__corner__new__discussion__share__links__link"
                    title="twitter link"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-twitter"
                    >
                      <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
                    </svg>
                  </button>
                </TwitterShareButton>

                <WhatsappShareButton
                  url={
                    "https://aida-pro.web.app/coffee-corner-discussion/" +
                    window.localStorage.getItem("forumId")
                  }
                >
                  <button
                    className="coffee__corner__new__discussion__share__links__link"
                    title="whatsapp link"
                  >
                    <WhatsappIcon size={32} round={true} />
                  </button>
                </WhatsappShareButton>

                {/* <button
                  className="coffee__corner__new__discussion__share__links__link"
                  title="twitch link"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-twitch"
                  >
                    <path d="M21 2H3v16h5v4l4-4h5l4-4V2zm-10 9V7m5 4V7"></path>
                  </svg>
                </button> */}
              </div>
              <div className="coffee__corner__new__discussion__share__text__link">
                <input
                  type="text"
                  // value="https://dribbble.com/shots/7093873-Share-Post-UI-Design"
                  value={
                    "https://aida-pro.web.app/coffee-corner-discussion/" +
                    window.localStorage.getItem("forumId")
                  }
                  className="coffee__corner__new__discussion__share__text__link__input"
                />
                <button
                  className="coffee__corner__new__discussion__share__text__link__btn"
                  onClick={() => copyLink()}
                  title="copy link"
                >
                  {copied ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-check"
                    >
                      <polyline points="20 6 9 17 4 12"></polyline>
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="2"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="feather feather-clipboard"
                    >
                      <path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path>
                      <rect
                        x="8"
                        y="2"
                        width="8"
                        height="4"
                        rx="1"
                        ry="1"
                      ></rect>
                    </svg>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
