import React, { useState, useContext, useEffect } from "react";
import { Head, InputBox } from "Components";
import UserContext from "Context/UserContext";
import { useSelector } from "react-redux";
import { getLookUpByPrefix } from "API/Api";
import { jobSekkerProfessionalDetailsUpdate } from "../API/EmploymentAPI";
import { freelacerProfessionalDetailsUpdate } from "../API/FreelancerApi";
import {
  CustomError, CustomSuccess
} from "./Toasts";
import { PART_TIME } from "Utils/Constants";
import { crossSvg } from "Assets";

export default function SelectWorkTime({ setIsSelectWorkTimeOpen }) {
  const [jobTypeDropDownSelected, setJobTypeDropDownSelected] = useState(null);
  const [jobTypeDropDownData, setJobTypeDropDownData] = useState([]);
  const [canRelocate, setCanRelocate] = useState(0);
  const [workOffice, setWorkOffice] = useState(0);
  const [workRemote, setWorkRemote] = useState(false);
  const [joiningAvailbilty, setJoiningAvailbilty] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const user = useContext(UserContext);
  let { jobsekker } = useSelector((state) => state.jobsekker);
  let { freelancer } = useSelector((state) => state.freelancer);

  if (jobsekker.Id === undefined) {
    jobsekker = freelancer;
  }

  useEffect(() => {
    setCanRelocate(jobsekker.canRelocate);
    setWorkRemote(jobsekker.workRemote);
    setWorkOffice(jobsekker.workOffice);
    setJobTypeDropDownSelected(jobsekker.JobTypeLookupDetail);
    setJoiningAvailbilty(jobsekker.Availbility);
    getLookUpByPrefix("JOBT")
      .then(({ data }) => {
        let formattedData = [];
        data.result.map((e) => {
          formattedData.push({ label: e.Title, value: e.Id });
        });
        setJobTypeDropDownData(formattedData);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  }, []);

  const saveProfessionalDetailData = () => {
    let relocate = canRelocate;
    let officeWork = workOffice;
    if (jobTypeDropDownSelected != null) {
      if (
        jobTypeDropDownSelected.value == PART_TIME ||
        jobTypeDropDownSelected.value == PART_TIME
      ) {
        relocate = false;
        officeWork = false;
      }
    }

    if (user.JobSeekerId) {
      let data = {
        Id: user.JobSeekerId,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        ExpectedSalary: jobsekker.expectedSalary,
        WorkOffice: officeWork ? 1 : 0,
        CanRelocate: relocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      setIsLoading(true);
      jobSekkerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          window.location.reload();
          // //CustomSuccess("Professional Detail Update Successfully...");
          setIsLoading(false);
        })
        .catch((err) => {
          setIsLoading(false);
          // CustomError("Failed to Update Professional Detail ");
        });
    } else {
      let data = {
        Id: user.FreelancerId,
        HourlyRate: jobsekker.expectedSalary,
        AvailabilityLookupDetailId:
          joiningAvailbilty && joiningAvailbilty.value,
        WorkOffice: officeWork ? 1 : 0,
        CanRelocate: relocate ? 1 : 0,
        WorkRemote: workRemote ? 1 : 0,
        JobTypeLookupDetailId:
          jobTypeDropDownSelected != null
            ? jobTypeDropDownSelected.value
            : null,
        LinkedInProfile: jobsekker.linkedInProfile,
        GoogleProfile: jobsekker.googleProfile,
        FacebookProfile: jobsekker.facebookProfile,
      };
      setIsLoading(true);
      freelacerProfessionalDetailsUpdate(data)
        .then(({ data }) => {
          window.location.reload();
          // //CustomSuccess("Professional Detail Update Successfully...");
          setIsLoading(false);
        })
        .catch((err) => {
          // CustomError("Failed to Update Professional Detail ");
          setIsLoading(false);
        });
    }
  };

  const changeWorkStatus = (event) => {
    if (event.target.name === "remoteOnly") {
      if (event.target.checked) {
        // setWorkOffice(false);
        setWorkRemote(true);
      } else {
        setWorkOffice(false);
        setWorkRemote(false);
      }
      setCanRelocate(false);
    } else if (event.target.name === "officeOnly") {
      if (event.target.checked) {
        setWorkOffice(true);
        // setWorkRemote(false);
      } else {
        setWorkOffice(false);
        setWorkRemote(false);
      }
    } else if (event.target.name === "willingToRelocate") {
      // setWorkRemote(false);
      setCanRelocate(event.target.checked);
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Add Skills" description="Add skills" />
      <div className="pupup__container">
        <form
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
          onSubmit={() => {
            setIsSelectWorkTimeOpen(false);
          }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={(e) => {
              e.preventDefault();
              setIsSelectWorkTimeOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="42"
                height="58"
                viewBox="0 0 42 58"
              >
                <defs>
                  <linearGradient
                    id="linear-gradient"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#f6a938" />
                    <stop offset="1" stopColor="#f5833c" />
                  </linearGradient>
                  <linearGradient
                    id="linear-gradient-3"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                  >
                    <stop offset="0" stopColor="#0ee1a3" />
                    <stop offset="1" stopColor="#0ca69d" />
                  </linearGradient>
                </defs>
                <g id="Work" transform="translate(-11 -3)">
                  <path
                    id="Path_21366"
                    data-name="Path 21366"
                    d="M18,0c9.941,0,18,2.239,18,5s-8.059,5-18,5S0,7.761,0,5,8.059,0,18,0Z"
                    transform="translate(14 51)"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_21359"
                    data-name="Path 21359"
                    d="M53,24c0,7.8-9.5,20.03-15.72,27.21C34.25,54.7,32,57,32,57s-2.25-2.3-5.28-5.79C20.5,44.03,11,31.8,11,24a21,21,0,0,1,42,0Z"
                    fill="url(#linear-gradient)"
                  />
                  <circle
                    id="Ellipse_383"
                    data-name="Ellipse 383"
                    cx="16"
                    cy="16"
                    r="16"
                    transform="translate(16 8)"
                    fill="#d1e7f8"
                  />
                  <path
                    id="Path_21360"
                    data-name="Path 21360"
                    d="M48,24A15.98,15.98,0,0,1,21.265,35.839q-2.214,1.242-4.533,2.309A122.307,122.307,0,0,0,26.72,51.21C29.75,54.7,32,57,32,57s2.25-2.3,5.28-5.79C43.5,44.03,53,31.8,53,24A20.928,20.928,0,0,0,46.927,9.235q-1.206,2.343-2.592,4.575A15.932,15.932,0,0,1,48,24Z"
                    fill="#eb8a2d"
                  />
                  <path
                    id="Path_21361"
                    data-name="Path 21361"
                    d="M48,24a15.932,15.932,0,0,0-3.665-10.19,64.29,64.29,0,0,1-23.07,22.029A15.98,15.98,0,0,0,48,24Z"
                    fill="#b7cad9"
                  />
                  <path
                    id="Path_21362"
                    data-name="Path 21362"
                    d="M41,24v8a2.006,2.006,0,0,1-2,2H25a2.006,2.006,0,0,1-2-2V24Z"
                    fill="url(#linear-gradient-3)"
                  />
                  <path
                    id="Path_21363"
                    data-name="Path 21363"
                    d="M43,19v5H21V19a2.006,2.006,0,0,1,2-2H41a2.006,2.006,0,0,1,2,2Z"
                    fill="url(#linear-gradient)"
                  />
                  <path
                    id="Path_21364"
                    data-name="Path 21364"
                    d="M34,24v2a1,1,0,0,1-1,1H31a1,1,0,0,1-1-1V23a1,1,0,0,1,1-1h2a1,1,0,0,1,1,1Z"
                    fill="#ff9811"
                  />
                  <path
                    id="Path_21365"
                    data-name="Path 21365"
                    d="M35,12H29a2,2,0,0,0-2,2v3h2V14h6v3h2V14A2,2,0,0,0,35,12Z"
                    fill="#603913"
                  />
                </g>
              </svg>
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Job type
                </div>
                <div className="pupup__container__from__wrapper__header__content__info">
                  Tell us about how you want to work
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="select"
                  placeholder="Select"
                  options={jobTypeDropDownData}
                  value={jobTypeDropDownSelected}
                  style={{
                    height: "fit-content",
                    minHeight: 45,
                  }}
                  onChange={(event) => setJobTypeDropDownSelected(event)}
                />
              </div>
              <div
                className="pupup__container__from__wrapper__form__row"
                style={{ alignItems: "flex-start" }}
              >
                <div
                  className="professional__details__container__content__entry__content__col"
                  style={{
                    backgroundColor: "#f6f6f6",
                    padding: 15,
                    borderRadius: 10,
                    paddingBottom: 0,
                    marginBottom: 20,
                    marginRight: 10,
                  }}
                >
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-remote"
                    type="checkbox"
                    value="Remember"
                    name="remoteOnly"
                    checked={workRemote}
                    onClick={(e) => changeWorkStatus(e)}
                  />
                  <label htmlFor="styled-checkbox-remote">
                    {/* Work only remote */}
                    Remote
                  </label>
                </div>
                <div
                  className="professional__details__container__content__entry__content__col"
                  style={{
                    backgroundColor: "#f6f6f6",
                    padding: 15,
                    borderRadius: 10,
                    paddingBottom: 0,
                    marginBottom: 20,
                  }}
                >
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-office"
                    type="checkbox"
                    checked={workOffice}
                    value="Remember"
                    onClick={(e) => changeWorkStatus(e)}
                    name="officeOnly"
                  />
                  <label htmlFor="styled-checkbox-office">
                    {/* Work only office */}
                    Office
                  </label>
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-relocate"
                    type="checkbox"
                    value="Remember"
                    name="willingToRelocate"
                    checked={canRelocate}
                    onClick={(e) => changeWorkStatus(e)}
                  />
                  <label htmlFor="styled-checkbox-relocate">
                    Willing to Locate
                  </label>
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => {
                  saveProfessionalDetailData();
                }}
                title="set professional details"
                disabled={isLoading}
              >
                {isLoading ? "Processing..." : "Save"}
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
