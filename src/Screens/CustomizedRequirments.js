import React, { useEffect, useState } from "react";
import { Head, InputBox } from "Components";
import { crossSvg } from "Assets";

export default function CustomizedRequirments({
  setIsCustomizedRequirmentsOpen,
}) {
  const [contactName, setContactName] = useState("");
  const [phone, setPhone] = useState("");
  const [emailAddress, setEmailAddress] = useState("");
  const [description, setDescription] = useState("");

  //validation
  const [contactNameError, setContactNameError] = useState(false);
  const [contactNameErrorMessage, setContactNameErrorMessage] = useState("");
  const [phoneError, setPhoneError] = useState(false);
  const [phoneErrorMessage, setPhoneErrorMessage] = useState("");
  const [emailAddressError, setEmailAddressError] = useState(false);
  const [emailAddressErrorMessage, setEmailAddressErrorMessage] = useState("");
  const [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");

  function handleChange(e) {
    if (e.currentTarget.name === "name") {
      if (e.currentTarget.value === "") {
        setContactNameError(true);
        setContactNameErrorMessage("Please enter contact name");
      } else {
        setContactNameError(false);
        setContactNameErrorMessage("");
        setContactName(e.currentTarget.value);
      }
    } else if (e.currentTarget.name === "phone") {
      if (e.currentTarget.value === "") {
        setPhoneError(true);
        setPhoneErrorMessage("Please enter phone no");
      } else {
        setPhoneError(false);
        setPhoneErrorMessage("");
        setPhone(e.currentTarget.value);
      }
    } else if (e.currentTarget.name === "email") {
      if (e.currentTarget.value === "") {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter an email address");
      } else if (
        !e.currentTarget.value.match(
          /^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w\w+)+$/
        )
      ) {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter a valid email address");
      } else {
        setEmailAddressError(false);
        setEmailAddressErrorMessage("");
        setEmailAddress(e.currentTarget.value);
      }
    }
  }
  function handleChangeDescription(data) {
    if (data.length === 0) {
      setDescriptionError(true);
      setDescriptionErrorMessage("Please enter description");
    } else {
      setDescriptionError(false);
      setDescriptionErrorMessage("");
      setDescription(data);
    }
  }
  function handleSubmit(e) {
    e.preventDefault();
    if (
      contactName === "" ||
      emailAddress === "" ||
      phone === "" ||
      description.length === 0
    ) {
      if (contactName === "") {
        setContactNameError(true);
        setContactNameErrorMessage("Please enter contact name");
      }
      if (emailAddress === "") {
        setEmailAddressError(true);
        setEmailAddressErrorMessage("Please enter an email address");
      }
      if (phone === "") {
        setPhoneError(true);
        setPhoneErrorMessage("Please enter phone no");
      }
      if (description.length === 0) {
        setDescriptionError(true);
        setDescriptionErrorMessage("Please enter description");
      }
    } else {
      setIsCustomizedRequirmentsOpen(false);
    }
  }
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head
        title="AIDApro | Customized Requirments"
        description="Customized Requirments"
      />
      <div className="pupup__container">
        <form
          onSubmit={handleSubmit}
          className="pupup__container__from animate__animated animate__slideInDown"
          style={{ maxWidth: "700px" }}
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsCustomizedRequirmentsOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Customize Requirments
                </div>
              </div>
            </div>
            <div className="pupup__container__from__wrapper__form">
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Contact name"
                  style={{
                    height: "fit-content",
                  }}
                  error={contactNameError}
                  errorMessage={contactNameErrorMessage}
                  onChange={handleChange}
                  name="name"
                />
                <InputBox
                  placeholder="Phone"
                  type="tel"
                  style={{
                    height: "fit-content",
                  }}
                  error={phoneError}
                  errorMessage={phoneErrorMessage}
                  onChange={handleChange}
                  name="phone"
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  placeholder="Email address"
                  type="email"
                  style={{
                    height: "fit-content",
                  }}
                  error={emailAddressError}
                  errorMessage={emailAddressErrorMessage}
                  onChange={handleChange}
                  name="email"
                />
              </div>
              <div className="pupup__container__from__wrapper__form__row">
                <InputBox
                  variant="textarea"
                  placeholder="Recruitment details or questions"
                  style={{
                    height: "fit-content",
                  }}
                  error={descriptionError}
                  errorMessage={descriptionErrorMessage}
                  onChange={handleChangeDescription}
                  name="description"
                />
              </div>
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                title="submit customized requirements"
              >
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
