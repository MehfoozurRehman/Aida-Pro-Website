import React from "react";
import { Head, PostingDetailsProtfolioCard } from "Components";
import NoData from "Components/NoData";

export default function PostingDetailsProtfolio({ Portfolios }) {
  return (
    <>
      <Head
        title="AIDApro | Posting Details - Portfolio"
        description="Posting Details - Portfolio"
      />
      <div className="homepage__container__jobs__projects__penel__container__details__content__project__portfolio">
        {Portfolios.length === 0 ? (
          <NoData text="No Project" />
        ) : (
          Portfolios.map((item, i) => (
            <PostingDetailsProtfolioCard key={i} item={item} />
          ))
        )}
      </div>
    </>
  );
}
