import React, { useState, useContext, useEffect } from "react";
import { freelancerProjectPortfolio } from "../API/FreelancerApi";
import { jobSekkerProjectPortfolio } from "../API/EmploymentAPI";
import { CustomError } from "./Toasts";
import UserContext from "Context/UserContext";
import { Head, InputBox } from "Components";
import { getBase64 } from "Utils/common";
import { Plus, X } from "react-feather";
import { crossSvg } from "Assets";
import moment from "moment";
import Img from "react-cool-img";

export default function UploadProject({ setIsUploadProjectOpen }) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const user = useContext(UserContext);
  const [fileString, setFileString] = useState([]);

  window.scrollTo({ top: 0, behavior: "smooth" });

  const [projectNameError, setProjectNameError] = useState(false);
  const [projectNameErrorMessage, setProjectNameErrorMessage] = useState("");
  const [descriptionError, setDescriptionError] = useState(false);
  const [descriptionErrorMessage, setDescriptionErrorMessage] = useState("");

  const handleTitle = (event) => {
    if (event.currentTarget.value === "") {
      setProjectNameError(true);
      setProjectNameErrorMessage("Please enter project name");
    } else {
      setProjectNameError(false);
      setProjectNameErrorMessage("");
      setName(event.currentTarget.value);
    }
  };

  const handleDescription = (data) => {
    if (data.length === 0) {
      setDescriptionError(true);
      setDescriptionErrorMessage("Please enter description");
    } else {
      setDescriptionError(false);
      setDescriptionErrorMessage("");
      setDescription(data);
    }
  };

  const handleFormSubmit = () => {
    // e.preventDefault();
    if (name === "" || description.length <= 0) {
      if (name === "") {
        setProjectNameError(true);
        setProjectNameErrorMessage("Please enter project name");
      }
      if (description.length <= 0) {
        setDescriptionError(true);
        setDescriptionErrorMessage("Please enter description");
      }
      // if (fileString === "") {
      //   setUploadImageError(true);
      //   setUploadImageErrorMessage("Please upload image");
      // }
    } else {
      if (user.Role.Title === "Freelancer") {
        let data = {
          ProjectDescription: description,
          ProjectName: name,
          FreelancerId: user.FreelancerId,
          FreelancerUploads: fileString,
          CreatedOn: moment().format("YYYY-MM-DD HH:mm:ss"),
        };
        freelancerProjectPortfolio(data)
          .then(({ data }) => {
            //CustomSuccess("Project Post Successfully");
            window.location.reload();
          })
          .catch((error) => {
            CustomError("Failed to Post a Project");
          });
      } else if (user.Role.Title === "JobSekker") {
        let data = {
          ProjectDescription: description,
          ProjectName: name,
          JobSeekerId: user.JobSeekerId,
          JobSeekerUploads: fileString,
          CreatedOn: moment().format("YYYY-MM-DD HH:mm:ss"),
        };
        jobSekkerProjectPortfolio(data)
          .then(({ data }) => {
            //CustomSuccess("Project Post Successfully");

            window.location.reload();
          })
          .catch(() => {
            CustomError("Failed to Post a Project");
          });
      }
    }
  };

  const uploadedImagesArray = async (data) => {
    let imageObjectArray = [];
    let base64Images = [];
    for (let index = 0; index < data.length; index++) {
      let imageFile = data[index];
      await getBase64(imageFile, (result) => {
        let newObject = {
          imageFile: imageFile,
          base64: result,
        };
        base64Images.push(newObject);
      });
    }
    setTimeout(() => {
      for (let index = 0; index < base64Images.length; index++) {
        const element = base64Images[index];
        if (user.Role.Title === "JobSekker") {
          imageObjectArray.push({
            JobSeekerId: user.JobSeekerId,
            JobSeekerPortfolioId: "",
            Type: "Employement",
            JobSeekerPortfolio: "JobSeekerPortfolio",
            UploadFilePath: element.base64,
            url: URL.createObjectURL(element.imageFile),
          });
        } else {
          imageObjectArray.push({
            FreelancerId: user.FreelancerId,
            FreelancerPortfolioId: "",
            Type: "Freelancer",
            FreelancerPortfolio: "FreelancerPortfolio",
            UploadFilePath: element.base64,
            url: URL.createObjectURL(element.imageFile),
          });
        }
      }
      setFileString(imageObjectArray);
    }, 1000);
  };

  const removeImage = (data, index) => {
    var array = [...fileString]; // make a separate copy of the array
    if (index !== -1) {
      array.splice(index, 1);
      setFileString(array);
    }
  };
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);
  return (
    <>
      <Head title="AIDApro | Upload Project" description="Upload Project" />
      <div className="pupup__container">
        <div
          // onSubmit={handleFormSubmit}
          className="pupup__container__from animate__animated animate__slideInDown"
        >
          <button
            className="pupup__container__from__button"
            type="button"
            onClick={() => {
              setIsUploadProjectOpen(false);
            }}
            title="close popup"
          >
            {crossSvg}
          </button>
          <div className="pupup__container__from__wrapper">
            <div className="pupup__container__from__wrapper__header">
              <div className="pupup__container__from__wrapper__header__content">
                <div className="pupup__container__from__wrapper__header__content__heading">
                  Upload Project
                </div>
              </div>
            </div>

            <div className="pupup__container__from__wrapper__form__reverse">
              <div
                className="pupup__container__from__wrapper__form__col"
                style={{
                  flexDirection: "column",
                  alignItems: "flex-start",
                  width: "100%",
                }}
              >
                <InputBox
                  variant="simple"
                  placeholder="Project Name"
                  name="title"
                  error={projectNameError}
                  errorMessage={projectNameErrorMessage}
                  onChange={(e) => handleTitle(e)}
                />
                <InputBox
                  variant="textarea"
                  placeholder="Description"
                  error={descriptionError}
                  errorMessage={descriptionErrorMessage}
                  style={{
                    width: "100%",
                    maxWidth: "880px",
                    marginBottom: "1em",
                  }}
                  name="description"
                  onChange={(e) => handleDescription(e)}
                />
              </div>
              {/* <UploadImgInput
                fileString={fileString}
                uploadedImagesArray={uploadedImagesArray}
              /> */}
            </div>
            <div
              className="pupup__container__from__wrapper__form__col"
              style={{ marginBottom: "1em" }}
            >
              <div className="pupup__container__from__wrapper__form__col__image__uploader">
                <input
                  type="file"
                  accept=".png, .jpg, .jpeg"
                  multiple
                  className="pupup__container__from__wrapper__form__col__image__uploader__input"
                  onClick={(event) => (event.target.value = null)}
                  onChange={(e) => uploadedImagesArray(e.currentTarget.files)}
                />
                <Plus size={30} color="currentColor" />
              </div>
              {fileString.length > 0
                ? fileString.map((item, i) => (
                    <div
                      key={i}
                      className="pupup__container__from__wrapper__form__col__image__uploader__img__wrapper"
                    >
                      <button
                        className="pupup__container__from__wrapper__form__col__image__uploader__img__wrapper__button"
                        onClick={() => removeImage(item, i)}
                      >
                        <X size={20} color="currentColor" />
                      </button>
                      <Img
                        loading="lazy"
                        src={item.url}
                        // alt="https://www.timeoutdubai.com/cloud/timeoutdubai/2021/09/11/hfpqyV7B-IMG-Dubai-UAE-1200x900.jpg"
                        className="pupup__container__from__wrapper__form__col__image__uploader__img"
                      />
                    </div>
                  ))
                : null}
            </div>
            <div className="pupup__container__from__wrapper__cta">
              <button
                type="submit"
                className="header__nav__btn btn__secondary"
                style={{
                  height: "50px",
                  width: "180px",
                }}
                onClick={() => handleFormSubmit()}
                title="upload project"
              >
                Upload Project
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
