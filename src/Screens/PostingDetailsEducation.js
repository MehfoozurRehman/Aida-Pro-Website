import React from "react";
import {
  Head,
  PostingDetailsEducationCertificateCard,
  PostingDetailsEducationDegreeCard,
} from "Components";
import NoData from "Components/NoData";

export default function PostingDetailsEducation({ data, certificatesData }) {
  return (
    <>
      <Head
        title="AIDApro | Posting Details - Education"
        description="Posting Details - Education"
      />
      <div className="homepage__container__jobs__projects__penel__container__details__content__education">
        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry">
          <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__header">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="43.49"
              height="34"
              style={{ marginRight: "1em" }}
              viewBox="0 0 43.49 34"
            >
              <defs>
                <linearGradient
                  id="linear-gradient"
                  x1="0.5"
                  x2="0.5"
                  y2="1"
                  gradientUnits="objectBoundingBox"
                >
                  <stop offset="0" stopColor="#f6a938" />
                  <stop offset="1" stopColor="#f5833c" />
                </linearGradient>
                <linearGradient
                  id="linear-gradient-6"
                  x1="0.5"
                  x2="0.5"
                  y2="1"
                  gradientUnits="objectBoundingBox"
                >
                  <stop offset="0" stopColor="#fff" />
                  <stop offset="1" stopColor="#f5833c" />
                </linearGradient>
              </defs>
              <g id="degrees" transform="translate(-19.758 -25.832)">
                <g
                  id="Group_1490"
                  data-name="Group 1490"
                  transform="translate(19.758 25.832)"
                >
                  <g
                    id="Group_1489"
                    data-name="Group 1489"
                    transform="translate(0 0)"
                  >
                    <g id="Group_1488" data-name="Group 1488">
                      <g
                        id="Group_1472"
                        data-name="Group 1472"
                        transform="translate(7.121 15.525)"
                      >
                        <g id="Group_1471" data-name="Group 1471">
                          <g id="Group_1470" data-name="Group 1470">
                            <path
                              id="Path_21312"
                              data-name="Path 21312"
                              d="M82.923,116.121l-12.081,5.253a2.543,2.543,0,0,1-2.028,0l-12.081-5.253a2.543,2.543,0,0,1-1.529-2.333V103.11H84.452v10.679a2.544,2.544,0,0,1-1.529,2.333Z"
                              transform="translate(-55.203 -103.11)"
                              fill="url(#linear-gradient)"
                            />
                          </g>
                        </g>
                      </g>
                      <g
                        id="Group_1473"
                        data-name="Group 1473"
                        transform="translate(7.121 24.194)"
                      >
                        <path
                          id="Path_21313"
                          data-name="Path 21313"
                          d="M82.923,148.6l-12.081,5.253a2.543,2.543,0,0,1-2.028,0L56.732,148.6a2.543,2.543,0,0,1-1.529-2.333v2.009a2.544,2.544,0,0,0,1.529,2.333l12.081,5.253a2.543,2.543,0,0,0,2.028,0l12.081-5.253a2.543,2.543,0,0,0,1.529-2.333v-2.009A2.544,2.544,0,0,1,82.923,148.6Z"
                          transform="translate(-55.203 -146.266)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                      <g id="Group_1476" data-name="Group 1476">
                        <g id="Group_1475" data-name="Group 1475">
                          <g id="Group_1474" data-name="Group 1474">
                            <path
                              id="Path_21314"
                              data-name="Path 21314"
                              d="M40.465,47.863l-19.2-8.583a2.543,2.543,0,0,1,0-4.644l19.2-8.583a2.543,2.543,0,0,1,2.076,0l19.2,8.583a2.543,2.543,0,0,1,0,4.644l-19.2,8.583a2.543,2.543,0,0,1-2.076,0Z"
                              transform="translate(-19.758 -25.832)"
                              fill="url(#linear-gradient)"
                            />
                          </g>
                        </g>
                      </g>
                      <g
                        id="Group_1478"
                        data-name="Group 1478"
                        transform="translate(6.718 0.803)"
                      >
                        <g id="Group_1477" data-name="Group 1477">
                          <path
                            id="Path_21315"
                            data-name="Path 21315"
                            d="M53.6,37.044a.4.4,0,0,1-.163-.769l14.079-6.292a1.716,1.716,0,0,1,.707-.153h0a1.716,1.716,0,0,1,.707.153l3.236,1.444a.4.4,0,0,1,.2.532.415.415,0,0,1-.532.2l-3.234-1.446a.926.926,0,0,0-.763,0L53.769,37.008h0a.386.386,0,0,1-.165.036Zm19.872-4.193a.418.418,0,0,1-.163-.034.4.4,0,0,1-.211-.223.389.389,0,0,1,.008-.307.408.408,0,0,1,.53-.2.4.4,0,0,1,.2.53.4.4,0,0,1-.368.237Z"
                            transform="translate(-53.2 -29.83)"
                            fill="url(#linear-gradient)"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_1479"
                        data-name="Group 1479"
                        transform="translate(0.001 9.067)"
                      >
                        <path
                          id="Path_21316"
                          data-name="Path 21316"
                          d="M62.2,70.963a2.627,2.627,0,0,1-.456.262l-19.2,8.583a2.543,2.543,0,0,1-2.076,0l-19.2-8.583a2.64,2.64,0,0,1-.456-.262,2.545,2.545,0,0,0,.456,4.382l19.2,8.583a2.543,2.543,0,0,0,2.076,0l19.2-8.583A2.545,2.545,0,0,0,62.2,70.963Z"
                          transform="translate(-19.762 -70.963)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                      <g
                        id="Group_1482"
                        data-name="Group 1482"
                        transform="translate(2.306 8.636)"
                      >
                        <g id="Group_1481" data-name="Group 1481">
                          <g id="Group_1480" data-name="Group 1480">
                            <path
                              id="Path_21317"
                              data-name="Path 21317"
                              d="M31.871,86.094a.636.636,0,0,1-.636-.636V74.274a.635.635,0,0,1,.478-.616l18.8-4.819a.636.636,0,0,1,.316,1.232l-18.325,4.7v10.69a.636.636,0,0,1-.636.636Z"
                              transform="translate(-31.235 -68.819)"
                              fill="url(#linear-gradient-6)"
                            />
                          </g>
                        </g>
                      </g>
                      <g
                        id="Group_1485"
                        data-name="Group 1485"
                        transform="translate(0.948 21.194)"
                      >
                        <g
                          id="Group_1484"
                          data-name="Group 1484"
                          transform="translate(0 0)"
                        >
                          <g id="Group_1483" data-name="Group 1483">
                            <path
                              id="Path_21318"
                              data-name="Path 21318"
                              d="M27.641,137.056H25.3a.824.824,0,0,1-.824-.824v-2.906a1.993,1.993,0,1,1,3.986,0v2.906A.824.824,0,0,1,27.641,137.056Z"
                              transform="translate(-24.479 -131.333)"
                              fill="url(#linear-gradient)"
                            />
                          </g>
                        </g>
                      </g>
                      <g
                        id="Group_1486"
                        data-name="Group 1486"
                        transform="translate(0.948 21.194)"
                      >
                        <path
                          id="Path_21319"
                          data-name="Path 21319"
                          d="M26.471,131.333h-.016v2.889a.824.824,0,0,1-.824.824H24.478v1.185a.824.824,0,0,0,.824.824H27.64a.824.824,0,0,0,.824-.824v-2.906A1.993,1.993,0,0,0,26.471,131.333Z"
                          transform="translate(-24.478 -131.333)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                      <g
                        id="Group_1487"
                        data-name="Group 1487"
                        transform="translate(0.948 21.369)"
                      >
                        <path
                          id="Path_21320"
                          data-name="Path 21320"
                          d="M27.286,132.2a1.983,1.983,0,0,1,.175.814v2.906a.824.824,0,0,1-.824.824H24.479v.18a.824.824,0,0,0,.824.824h2.338a.824.824,0,0,0,.824-.824v-2.906A1.992,1.992,0,0,0,27.286,132.2Z"
                          transform="translate(-24.479 -132.204)"
                          fill="url(#linear-gradient)"
                        />
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
            Degrees
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content">
            {data.length === 0 ? (
              <NoData text="No Degrees" />
            ) : (
              data.map((education, i) => (
                <PostingDetailsEducationDegreeCard data={education} key={i} />
              ))
            )}
          </div>
        </div>
        <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry">
          <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__header">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{ marginRight: "1em" }}
              width="42.586"
              height="48.766"
              viewBox="0 0 42.586 48.766"
            >
              <defs>
                <linearGradient
                  id="linear-gradient"
                  x1="0.5"
                  x2="0.5"
                  y2="1"
                  gradientUnits="objectBoundingBox"
                >
                  <stop offset="0" stopColor="#f6a938" />
                  <stop offset="1" stopColor="#f5833c" />
                </linearGradient>
              </defs>
              <g
                id="Page-1"
                transform="matrix(0.574, -0.819, 0.819, 0.574, -25.423, 19.349)"
              >
                <g
                  id="_001---Degree"
                  data-name="001---Degree"
                  transform="translate(-0.018 31.049)"
                >
                  <path
                    id="Shape"
                    d="M26.271,32.605a17.061,17.061,0,0,1,.584,3.789,5.621,5.621,0,0,0-5.639.524A21.885,21.885,0,0,0,20.66,32.6q1.472.031,3.019.031C24.565,32.629,25.424,32.62,26.271,32.605ZM24.489,45.591a4.05,4.05,0,1,1,4.05-4.05A4.05,4.05,0,0,1,24.489,45.591ZM1,38.3a22.721,22.721,0,0,1,.81-6.886,22.721,22.721,0,0,1,.81,6.886,22.721,22.721,0,0,1-.81,6.886A22.72,22.72,0,0,1,1,38.3Zm2.449,7.024A26.865,26.865,0,0,0,4.237,38.3a26.871,26.871,0,0,0-.791-7.025,142.134,142.134,0,0,0,15.522,1.282,20.853,20.853,0,0,1,.661,5.743c0,.117,0,.229,0,.342a5.612,5.612,0,0,0-.261,5.308l-.036.086A141.177,141.177,0,0,0,3.445,45.324Zm18.133,5.937L20.987,49.9a.81.81,0,0,0-1.062-.421l-1.363.6,1.9-4.554a5.665,5.665,0,0,0,2.861,1.563Zm7.475-1.782a.81.81,0,0,0-1.062.421l-.6,1.347L25.655,47.09a5.671,5.671,0,0,0,2.864-1.564l1.9,4.537ZM45.39,45.55a131.2,131.2,0,0,0-15.716-1.458l-.058-.139a5.637,5.637,0,0,0-1.092-6.388,22.548,22.548,0,0,0-.578-4.991A135.222,135.222,0,0,0,45.39,31.049c.376.638.972,3.163.972,7.251S45.761,44.912,45.39,45.55Z"
                    transform="translate(-0.982 -31.049)"
                    fill="url(#linear-gradient)"
                  />
                </g>
              </g>
            </svg>
            Certifications
          </div>
          <div className="homepage__container__jobs__projects__penel__container__details__content__education__entry__content">
            {certificatesData.length === 0 ? (
              <NoData text="No Certificates" />
            ) : (
              certificatesData.map((certificate, i) => (
                <PostingDetailsEducationCertificateCard
                  data={certificate}
                  key={i}
                />
              ))
            )}
          </div>
        </div>
      </div>
    </>
  );
}
