import React, { useContext, useEffect, useState } from "react";
import Header from "Components/Header";
import { Head, JobProjectDetailCard } from "Components";
import Img from "react-cool-img";
import { jobStatus } from "../API/Job";
import UserContext from "Context/UserContext";
import { homeSelected } from "Assets";

export default function JobsProjectsDetailsCoffeeCorner({
  setIsApplyForJob,
  setIsLoginOpen,
  isRandom,
  location,
  setIsApplyForJobProject,
  isApplyForJobProject,
  setAlertDialogVisibility,
  setHeadingAndTextForAlertDialog,
}) {
  const isOn = window.localStorage.getItem("isOn");
  const [jobStatusApiData, setJobStatusApiData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const user = useContext(UserContext);
  const dataComingFrom = location.state.redirectFrom;
  const dataJobType = location.state.jobType;
  const data = location.state.objectData;

  if (user.Id === undefined) {
    window.location.href = "/login";
  }

  useEffect(() => {
    if (isOn != "company") {
      if (isOn == "freelancer") jobStatusAPI();
      else jobStatusAPI();
    }
  }, []);

  const jobStatusAPI = () => {
    setIsLoading(true);
    jobStatus(dataJobType, user.Id, data.Id)
      .then(({ data }) => {
        setJobStatusApiData(data.result);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
      });
  };

  const goBack = () => {
    window.history.go(-1);
  };

  const callBackToGetInterested = () => {
    setJobStatusApiData(null);
    jobStatusAPI();
  };

  return (
    <>
      <Head title="AIDApro | Details" description="Details" />
      <Header setIsLoginOpen={setIsLoginOpen} />

      <div className="homepage__container__jumbotron">
        <div className="" style={{ width: "90vw", maxWidth: "1440px" }}>
          <div className="homepage__freelancer__project__details__container">
            <div
              className="homepage__freelancer__project__details__title__container"
              onClick={() => goBack()}
            >
              <div className="homepage__freelancer__project__details__title__container__svg">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="7.639"
                  height="13.363"
                  viewBox="0 0 7.639 13.363"
                >
                  <path
                    id="Icon_ionic-ios-arrow-back"
                    data-name="Icon ionic-ios-arrow-back"
                    d="M13.554,12.873,18.61,7.821a.955.955,0,0,0-1.353-1.349L11.529,12.2a.953.953,0,0,0-.028,1.317l5.752,5.764a.955.955,0,0,0,1.353-1.349Z"
                    transform="translate(-11.251 -6.194)"
                  />
                </svg>
              </div>
              <div className="homepage__freelancer__project__details__title__container__heading__container">
                <Img loading="lazy" src={homeSelected} alt="homeSelected" />
                <div className="homepage__freelancer__project__details__title__container__heading">
                  Back
                </div>
              </div>
            </div>
            {data ? (
              <JobProjectDetailCard
                setIsApplyForJob={setIsApplyForJob}
                isRandom={isRandom}
                data={data}
                isFreelancer={dataComingFrom == "Projects" ? true : false}
                callBackToGetInterested={callBackToGetInterested}
                setIsApplyForJobProject={setIsApplyForJobProject}
                isApplyForJobProject={isApplyForJobProject}
                jobStatusApiData={jobStatusApiData}
                setAlertDialogVisibility={setAlertDialogVisibility}
                setHeadingAndTextForAlertDialog={
                  setHeadingAndTextForAlertDialog
                }
              />
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}
