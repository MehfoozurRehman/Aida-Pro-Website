// Scripts for firebase and firebase messaging
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js");

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("./firebase-messaging-sw.js")
    .then(function (registration) {
      // console.log("Registration successful, scope is:", registration.scope);
    })
    .catch(function (err) {
      console.log("Service worker registration failed, error:", err);
    });
}

//For Listning background notification
// Initialize the Firebase app in the service worker by passing the generated config
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDYso5KV4oK0diHno1Q5oSKDA8dWknOKaM",
  authDomain: "aida-dev-chat.firebaseapp.com",
  databaseURL: "https://aida-dev-chat-default-rtdb.firebaseio.com",
  projectId: "aida-dev-chat",
  storageBucket: "aida-dev-chat.appspot.com",
  messagingSenderId: "882477990543",
  appId: "1:882477990543:web:bdc3912d7afee1bbf366fa",
  measurementId: "G-0T59ECX61E",
};
firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();
  messaging.onBackgroundMessage(function (payload) {
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
      body: payload.notification.body,
    };
    return self.registration.showNotification(
      notificationTitle,
      notificationOptions
    );
  });
}

self.addEventListener("notificationclick", function (event) {
  var url = "https://aidapro.com/";
  event.notification.close();
  self.clients.openWindow(url).then(function (clientsArr) {
    // If a Window tab matching the targeted URL already exists, focus that;
    const hadWindowToFocus = clientsArr.some((windowClient) =>
      windowClient.url === event.notification.data.url
        ? (windowClient.focus(), true)
        : false
    );
    // Otherwise, open a new tab to the applicable URL and focus it.
    if (!hadWindowToFocus)
      clients
        .openWindow(event.notification.data.url)
        .then((windowClient) => (windowClient ? windowClient.focus() : null));
  });
});
